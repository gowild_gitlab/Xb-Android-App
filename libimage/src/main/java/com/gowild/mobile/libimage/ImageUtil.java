/**
 * Copyright (C)  2016 深圳市狗尾草智能科技有限公司
 * GowildNewXB
 * ImageUtil.java
 */
package com.gowild.mobile.libimage;

import android.app.Application;
import android.graphics.Bitmap;
import android.view.View;
import android.widget.ImageView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.imageaware.ImageAware;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

/**
 * @author Ji.Li
 * @version 1.0
 *          <p><strong>Features draft description.主要功能介绍</strong></p>
 * @since 2016/7/11 17:04
 */
@SuppressWarnings("unused")
public class ImageUtil {

    // ===========================================================
    // Constants
    // ===========================================================

    private static final String TAG = ImageUtil.class.getSimpleName();

    private static final int DEFAULT_DRAWABLE_ID = android.R.color.transparent;

    // ===========================================================
    // Static Fields
    // ===========================================================

    private static volatile ImageUtil sInstance = null;
    private static Application sContext = null;

    // ===========================================================
    // Fields
    // ===========================================================

    private final ImageLoader mImageLoader;
    private final DisplayImageOptions mDisplayOptions;

    // ===========================================================
    // Constructors
    // ===========================================================

    private ImageUtil() {
        this.mImageLoader = ImageLoader.getInstance();
        this.mImageLoader.init(ImageLoaderConfiguration.createDefault(sContext));
        DisplayImageOptions.Builder displayBuilder = new DisplayImageOptions.Builder();
        this.mDisplayOptions = displayBuilder.cacheInMemory(true)
                .cacheOnDisk(true)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .imageScaleType(ImageScaleType.IN_SAMPLE_POWER_OF_2)
                .resetViewBeforeLoading(false)
                .showImageOnFail(DEFAULT_DRAWABLE_ID)
                .showImageOnLoading(DEFAULT_DRAWABLE_ID)
                .build();
    }

    public static ImageUtil getInstance() {
        if (sInstance == null) {
            synchronized (ImageUtil.class) {
                if (sInstance == null) {
                    sInstance = new ImageUtil();
                }
            }
        }
        return sInstance;
    }

    ;

    // ===========================================================
    // Getter or Setter
    // ===========================================================


    // ===========================================================
    // Methods for/from SuperClass/Interfaces
    // ===========================================================


    // ===========================================================
    // Methods
    // ===========================================================

    public static void initContext(Application context) {
        sContext = context;
    }

    public void displayImage(String uri, ImageView iv) {
        this.mImageLoader.displayImage(uri, iv, mDisplayOptions);
    }

    public void loadImage(String uri, ImageLoadingListener listener) {
        this.mImageLoader.loadImage(uri, listener);
    }

    public void displayImageBg(String uri, View view) {
        ImageAware imageAware = new BackgroundViewAware(view);
        this.mImageLoader.displayImage(uri, imageAware);
    }

    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================

}
