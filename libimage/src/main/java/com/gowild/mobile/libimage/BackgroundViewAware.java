/**
 * Copyright (C)  2016 深圳市狗尾草智能科技有限公司
 * GowildNewXB
 * BackgroundViewAware.java
 */
package com.gowild.mobile.libimage;

import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.view.View;

import com.gowild.mobile.libcommon.utils.APILevelUtil;
import com.nostra13.universalimageloader.core.imageaware.ViewAware;

/**
 * @author Ji.Li
 * @since 2016/7/11 18:05
 * @version 1.0
 * <p><strong>Features draft description.主要功能介绍</strong></p>
 */
@SuppressWarnings("unused")
public class BackgroundViewAware extends ViewAware{

    // ===========================================================
    // Constants
    // ===========================================================

    private static final String TAG = BackgroundViewAware.class.getSimpleName();

    // ===========================================================
    // Static Fields
    // ===========================================================

    // ===========================================================
    // Fields
    // ===========================================================

    // ===========================================================
    // Constructors
    // ===========================================================

    public BackgroundViewAware(View view) {
        super(view);
    }

    public BackgroundViewAware(View view, boolean checkActualViewSize) {
        super(view, checkActualViewSize);
    }

    // ===========================================================
    // Getter or Setter
    // ===========================================================


    // ===========================================================
    // Methods for/from SuperClass/Interfaces
    // ===========================================================

    @SuppressLint("NewApi")
    @Override
    protected void setImageDrawableInto(Drawable drawable, View view) {
        if (view != null && drawable != null) {
            if (APILevelUtil.hasJellyBean()) {
                view.setBackground(drawable);
            } else {
                view.setBackgroundDrawable(drawable);
            }
        }
    }

    @Override
    protected void setImageBitmapInto(Bitmap bitmap, View view) {
        if (view != null && bitmap != null && !bitmap.isRecycled()) {
            BitmapDrawable bmpDrawable = new BitmapDrawable(bitmap);
            setImageDrawableInto(bmpDrawable, view);
        }
    }

    // ===========================================================
    // Methods
    // ===========================================================


    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================

}
