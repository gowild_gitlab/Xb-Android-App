package com.gowild.mobile.libcommon.utils;

import android.content.Context;
import android.content.res.Resources;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;

/**
 * @author Ji.Li
 */
public final class DimenUtil {
	
	@SuppressWarnings("unused")
	private static final String TAG = DimenUtil.class.getSimpleName();

	public static int getStatusBarHeight(Context context) {
		int result = 0;
		Resources res = context.getResources();
		int resourceId = res.getIdentifier("status_bar_height", "dimen",
				"android");
		if (resourceId > 0) {
			result = res.getDimensionPixelSize(resourceId);
		}
		Log.d(TAG, "getStatusBarHeight() result = " + result);
		return result;
	}

	public static void setMargins(View v, int l, int t, int r, int b) {
		if (v.getLayoutParams() instanceof ViewGroup.MarginLayoutParams) {
			ViewGroup.MarginLayoutParams p = (ViewGroup.MarginLayoutParams) v.getLayoutParams();
			p.setMargins(l, t, r, b);
			v.requestLayout();
		}
	}

	public static void setMarginTop(View v, int t) {
		if (v.getLayoutParams() instanceof ViewGroup.MarginLayoutParams) {
			ViewGroup.MarginLayoutParams p = (ViewGroup.MarginLayoutParams) v.getLayoutParams();
			setMargins(v, p.leftMargin, t, p.rightMargin, p.bottomMargin);
		}
	}

	/**
	 * 获得窗体宽度
	 * @param context
	 * @return
     */
	public static int getWindowWidth(Context context){
		DisplayMetrics display = getWindowPixels(context);
		return display.widthPixels;
	}

	/**
	 * 获得窗体高度
	 * @param context
	 * @return
     */
	public static int getWindowHeight(Context context){
		DisplayMetrics display = getWindowPixels(context);
		return display.heightPixels;
	}

	public static DisplayMetrics getWindowPixels(Context context){
		DisplayMetrics display = new DisplayMetrics();
		WindowManager manager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
		manager.getDefaultDisplay().getMetrics(display);
		return  display;
	}
	
}
