package com.gowild.mobile.libcommon.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Administrator on 2016/5/11.
 */
public final class CheckContentUtil {

    private static Pattern pattern = Pattern.compile("[0-9a-zA-Z\\.%\\&\\|_@]{6,16}");

    public static boolean containersSpecialChar(String source) {
//        密码6到16位字符，仅包含（大小写字母、阿拉伯数字、.<点>、%<百分号>、&<与符号>、|<或符号>、_<下划线>、@<艾特符>）；
        String malReg = "[0-9a-zA-Z\\.%\\&\\|_@]{6,16}";

        return source.matches(malReg);
    }


    public static boolean isCorrectPhone(String phone) {
//		Pattern pattern = Pattern.compile("^((13[0-9])|(15[^4,\\D])|(18[0,5-9]))\\d{8}$");
//		Matcher matcher = pattern.matcher(phone);
        Pattern pattern = Pattern.compile("^1\\d{10}$");
        Matcher matcher = pattern.matcher(phone);//根据需求文档写的但是不科学
        System.out.println(matcher.matches());
        return matcher.matches();
    }

    public static boolean containersNumChar(String source) {
        String malReg = "[0-9a-zA-Z\\u4e00-\\u9fa5]{3,10}";
        return source.matches(malReg);
    }

//    public static boolean containsEmoji(String source) {
//        int len = source.length();
//        for (int i = 0; i < len; i++) {
//            char codePoint = source.charAt(i);
//            if (!isEmojiCharacter(codePoint)) { //如果不能匹配,则该字符是Emoji表情
//                return true;
//            }
//        }
//        return false;
//    }
//
//    /**
//     * 判断是否是Emoji
//     *
//     * @param codePoint 比较的单个字符
//     * @return
//     */
//    private static boolean isEmojiCharacter(char codePoint) {
//        return (codePoint == 0x0) || (codePoint == 0x9) || (codePoint == 0xA) ||
//                (codePoint == 0xD) || ((codePoint >= 0x20) && (codePoint <= 0xD7FF)) ||
//                ((codePoint >= 0xE000) && (codePoint <= 0xFFFD)) || ((codePoint >= 0x10000)
//                && (codePoint <= 0x10FFFF));
//    }

    public static boolean containsEmail(String source) {
        String malReg = "\\w+@\\w+\\.com";
        return source.matches(malReg);
    }


    /**
     * 04	     * 检测是否有emoji字符
     * 05	     * @param source
     * 06	     * @return 一旦含有就抛出
     * 07
     */
    public static boolean containsEmoji(String source) {
        int len = source.length();
        boolean isEmoji = false;
        for (int i = 0; i < len; i++) {
            char hs = source.charAt(i);
            if (0xd800 <= hs && hs <= 0xdbff) {
                if (source.length() > 1) {
                    char ls = source.charAt(i+1);
                    int uc = ((hs - 0xd800) * 0x400) + (ls - 0xdc00) + 0x10000;
                    if (0x1d000 <= uc && uc <= 0x1f77f) {
                        return true;
                    }
                }
            } else {
                // non surrogate
                if (0x2100 <= hs && hs <= 0x27ff && hs != 0x263b) {
                    return true;
                } else if (0x2B05 <= hs && hs <= 0x2b07) {
                    return true;
                } else if (0x2934 <= hs && hs <= 0x2935) {
                    return true;
                } else if (0x3297 <= hs && hs <= 0x3299) {
                    return true;
                } else if (hs == 0xa9 || hs == 0xae || hs == 0x303d || hs == 0x3030 || hs == 0x2b55 || hs == 0x2b1c || hs == 0x2b1b || hs == 0x2b50|| hs == 0x231a ) {
                    return true;
                }
                if (!isEmoji && source.length() > 1 && i < source.length() -1) {
                    char ls = source.charAt(i+1);
                    if (ls == 0x20e3) {
                        return true;
                    }
                }
            }
        }
        return  isEmoji;
    }
    private static boolean isEmojiCharacter(char codePoint) {

        return (codePoint == 0x0) ||

                (codePoint == 0x9) ||

                (codePoint == 0xA) ||


                (codePoint == 0xD) ||

                ((codePoint >= 0x20) && (codePoint <= 0xD7FF)) ||

                ((codePoint >= 0xE000) && (codePoint <= 0xFFFD)) ||

                ((codePoint >= 0x10000) && (codePoint <= 0x10FFFF));
    }


    /**
     * 过滤emoji 或者 其他非文字类型的字符
     *
     * @param source
     * @return
     */

    public static String filterEmoji(String source) {


        if (!containsEmoji(source)) {

            return source;//如果不包含，直接返回

        }

        //到这里铁定包含

        StringBuilder buf = null;


        int len = source.length();


        for (int i = 0; i < len; i++) {

            char codePoint = source.charAt(i);


            if (isEmojiCharacter(codePoint)) {


                if (buf == null) {


                    buf = new StringBuilder(source.length());


                }


                buf.append(codePoint);

            } else {

            }
        }

        if (buf == null) {

            return source;//如果没有找到 emoji表情，则返回源字符串

        } else {

            if (buf.length() == len) {//这里的意义在于尽可能少的toString，因为会重新生成字符串

                buf = null;

                return source;

            } else {

                return buf.toString();
            }

        }
    }

    public static boolean isContainChar(String str){
        char[] chars = str.toCharArray();
        String pa = "[`~!@#$%^&*()+=|{}':;',\\[\\].<>/?~！@#￥%……& amp;*（）——+|{}【】‘；：”“’。，、？|-]";
        for (char ch: chars) {
            if ((ch+"").matches(pa)){
                return true;
            }
        }
        return false;
    }
}

