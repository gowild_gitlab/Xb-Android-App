package com.gowild.mobile.libcommon.utils;

import android.content.Context;

public final class UnitUtils {
    public static int dip2px(Context context, float dpValue) {
        final float scale = context.getResources().getDisplayMetrics().density;
        return (int) (dpValue * scale + 0.5f);
    }
}
