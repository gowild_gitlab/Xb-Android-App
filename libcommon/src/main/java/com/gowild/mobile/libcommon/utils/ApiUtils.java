package com.gowild.mobile.libcommon.utils;

import android.os.Build;

/**
 * Created by Ji.Li on 2016/2/23.
 */
@SuppressWarnings("unused")
public final class ApiUtils {

    private static final String TAG = ApiUtils.class.getSimpleName();

    private ApiUtils(){}

    public static final boolean hasKitkat(){
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;
    }
}
