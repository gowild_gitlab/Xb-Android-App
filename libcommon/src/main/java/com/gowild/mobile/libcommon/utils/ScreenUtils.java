
package com.gowild.mobile.libcommon.utils;

import android.content.Context;
import android.os.Build;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.WindowManager;

/**
 * 注意。该方法里面屏幕高肯定大于宽，如需单独横屏处理，请自身转换
 */
public class ScreenUtils {
    private static int sRealScreenWidth;
    private static int sRealScreenHeight;
    private static float sRealScreenDensity;
 
    private static int sScreenWidth;
    private static int sScreenHeight;
    private static float sScreenDensity;
    
    /**
     * 初始化屏幕宽高值
     * @param context
     */
    public static void initScreenWidthAndHeight(Context context) {
        DisplayMetrics dm = new DisplayMetrics();
        Display display= ((WindowManager) context.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            display.getRealMetrics(dm);
        } else {
            display.getMetrics(dm);
        }
        if (dm.widthPixels > dm.heightPixels) {
            sRealScreenWidth = dm.heightPixels;
            sRealScreenHeight = dm.widthPixels;
        } else {
            sRealScreenWidth = dm.widthPixels;
            sRealScreenHeight = dm.heightPixels;
        }
        sRealScreenDensity = dm.density;

        display.getMetrics(dm);
        if (dm.widthPixels > dm.heightPixels) {
            sScreenWidth = dm.heightPixels;
            sScreenHeight = dm.widthPixels;
        } else {
            sScreenWidth = dm.widthPixels;
            sScreenHeight = dm.heightPixels;
        }
        sScreenDensity = dm.density;
//        Log.e("ScreenUtils", sScreenWidth + "   " + sScreenHeight + "  " + sScreenDensity);
    }

    public static int getRealScreenWidth() {
        return sRealScreenWidth;
    }

    public static int getRealScreenHeight() {
        return sRealScreenHeight;
    }

    public static float getRealScreenDensity() {
        return sRealScreenDensity;
    }

    public static int getScreenWidth() {
        return sScreenWidth;
    }

    public static int getScreenHeight() {
        return sScreenHeight;
    }

    public static float getScreenDensity() {
        return sScreenDensity;
    }

    public static int dp2px(float dp) {
        final float scale = sScreenDensity;
        return (int) (dp * scale + 0.5f);
    }
}
