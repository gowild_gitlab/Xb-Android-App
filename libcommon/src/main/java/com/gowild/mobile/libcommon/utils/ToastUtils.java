package com.gowild.mobile.libcommon.utils;

import android.content.Context;
import android.view.Gravity;
import android.widget.Toast;

/**
 * Created by Ji.Li on 2016/2/23.
 */
@SuppressWarnings("unused")
public final class ToastUtils {

    private static final String TAG = ToastUtils.class.getSimpleName();

    private static final int BASE_FONT_SIZE = 30;
    private static final int BASE_SCREEN_WIDTH = 720;
    private static final int BASE_PADDING_LEFT = 50;
    private static final int BASE_PADDING_TOP = 30;
    private static Toast toast;

    private ToastUtils() {
    }

    public static void showToast(Context context, int textResID, int len, int gravity) {
        Toast t = Toast.makeText(context.getApplicationContext(), textResID, len);
        t.setGravity(gravity, 0, 0);
        t.show();
    }

    public static void showToast(Context context, int textResID, int len) {
        Toast t = Toast.makeText(context.getApplicationContext(), textResID, len);
        t.show();
    }

    public static final void showToast(Context context, String text,
                                       boolean lengthLong) {
        Toast toast = null;
        if (lengthLong) {
            toast = Toast.makeText(context.getApplicationContext(), text, Toast.LENGTH_LONG);
        } else {
            toast = Toast.makeText(context.getApplicationContext(), text, Toast.LENGTH_SHORT);
        }
        toast.show();
    }

    public static final void showCustom(Context context, CharSequence text, boolean lengthLong) {
        int duration;
        if (lengthLong) {
            duration = Toast.LENGTH_LONG;
        } else {
            duration = Toast.LENGTH_SHORT;
        }
        if (toast == null) {
            toast = Toast.makeText(context.getApplicationContext(), text, duration);
            toast.setGravity(Gravity.CENTER, 0, 0);
        } else {
            toast.setText(text);
        }
        toast.show();
    }

    public static final void showCustom(Context context, int stringId, boolean lenghLong) {
        CharSequence text = context.getApplicationContext().getText(stringId);
        showCustom(context, text, lenghLong);
    }
}
