package com.gowild.mobile.libcommon.utils;

import android.os.Environment;
import android.os.Process;
import android.util.Log;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public final class Logger {
	// public static int LOG_LEVEL = 0; // 全局的总开关
	public static int LOWEST_LOG_LEVEL = 7;// 0隐藏 7全部显示   最低日志显示级别
	private static int SYSTEM = 1;
	private static int VERBOS = 2;
	private static int DEBUG = 3;
	private static int INFO = 4;
	private static int WARN = 5;
	private static int ERROR = 6;

	private static boolean          bSaveLog           = false;
	private static String           LOG_FILE_PATH      = "/log/com.gowild.mobileclient/";
	private static SimpleDateFormat dateFormatFileName = (SimpleDateFormat) SimpleDateFormat.getDateInstance();
	private static SimpleDateFormat dateFormat         = (SimpleDateFormat) SimpleDateFormat.getDateInstance();
	static {
		dateFormatFileName.applyPattern("yyyy-MM-dd");
		dateFormat.applyPattern("yyyy-MM-dd  HH:mm:ss.SSS");
	}

	public static void setSaveLog(boolean save) {
		bSaveLog = save;
	}

	private static File checkFileExist() {
		String environment = Environment.getExternalStorageDirectory().getAbsolutePath();
		Calendar calendar = Calendar.getInstance();
		String filePath  = environment + LOG_FILE_PATH + dateFormatFileName.format(calendar.getTime()) + "/";
		File file = new File(filePath);
		if (!file.exists() || !file.isDirectory()) {
			file.delete();
			file.mkdirs();
		}
		String fileName = "log.txt";
		file = new File(filePath + fileName);
		if (!file.exists()) {
			try {
				file.createNewFile();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return file;
	}

	public static final void writeTextToFile(String text) {
		if (!bSaveLog) {
			return;
		}
		try {
			File file = checkFileExist();
			FileWriter fw = new FileWriter(file.getAbsoluteFile(), true);
			BufferedWriter bw = new BufferedWriter(fw);
			fw.write(dateFormat.format(new Date()) + "  " + Process.myPid() + "  " + text + "\n");
			bw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void i(String tag, String message) {
		if(message==null){
			return;
		}
		if (LOWEST_LOG_LEVEL <= INFO) {//
			Log.i(tag, message);
			writeTextToFile(tag + " " + message);
		}
	}

	public static void e(String tag, String message) {
		if(message==null){
			return;
		}
		if (LOWEST_LOG_LEVEL <= ERROR) {
			Log.e(tag, message);
			writeTextToFile("ERROR" + tag + " " + message);
		}
	}

	public static void d(String tag, String message) {
		if(message==null){
			return;
		}
		if (LOWEST_LOG_LEVEL <= DEBUG) {
			Log.d(tag, message);
			writeTextToFile(tag + " " + message);
		}
	}

	public static void w(String tag, String message) {
		if(message==null){
			return;
		}
		if (LOWEST_LOG_LEVEL <= WARN) {
			Log.w(tag, message);
			writeTextToFile(tag + " " + message);
		}
	}

	public static void v(String tag, String message) {
		if(message==null){
			return;
		}
		if (LOWEST_LOG_LEVEL <= VERBOS) {
			Log.v(tag, message);
			writeTextToFile(tag + " " + message);
		}
	}

	public static void s(String message) {
		if(message==null){
			return;
		}
		if (LOWEST_LOG_LEVEL <= SYSTEM) {
			System.out.println(message);
		}
	}
}
