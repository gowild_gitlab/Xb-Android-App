/**
 * Copyright (C)  2016 深圳市狗尾草智能科技有限公司
 * GowildXB
 * SpUtils.java
 */
package com.gowild.mobile.libcommon.utils;

import android.content.Context;

/**
 * @author shuai. wang
 * @version 1.0
 *          <p><strong>Features draft description.主要功能介绍</strong></p>
 * @since 2016/7/29 16:53
 */
public class SpUtils {

    // ===========================================================
    // Constants
    // ===========================================================

    private static final String TAG  = SpUtils.class.getSimpleName();
    private static final String NAME = "gowildxb";

    public static void putString(String name, Context context, String key, String value) {
        if (name != null) {
            context.getSharedPreferences(name, 0).edit().putString(key, value).apply();
        } else {
            putString(context, key, value);
        }
    }

    public static void putString(Context context, String key, String value) {
        context.getSharedPreferences(NAME, 0).edit().putString(key, value).apply();
    }


    public static String getString(String name, Context context, String key) {
        if (name != null) {
            return context.getSharedPreferences(name, 0).getString(key, "");
        } else {
            return getString(context, key);
        }
    }
    public static String getString(Context context, String key) {
        return context.getSharedPreferences(NAME, 0).getString(key, "");
    }


    public static void putInt(String name, Context context, String key, int value) {
        if (name != null) {
            context.getSharedPreferences(name, 0).edit().putInt(key, value).apply();
        } else {
            putInt(context, key, value);
        }
    }

    public static void putInt(Context context, String key, int value) {
        context.getSharedPreferences(NAME, 0).edit().putInt(key, value).apply();
    }


    public static int getInt(String name, Context context, String key) {
        if (name != null) {
            return context.getSharedPreferences(name, 0).getInt(key, 0);
        } else {
            return getInt(context, key);
        }
    }

    public static int getInt(Context context, String key) {
        return context.getSharedPreferences(NAME, 0).getInt(key, 0);
    }


    public static void putLong(Context context, String key, long value) {
        context.getSharedPreferences(NAME, 0).edit().putLong(key, value).apply();
    }

    public static long getLong(Context context, String key) {
        return context.getSharedPreferences(NAME, 0).getLong(key, 0);
    }

    public static void putBoolean(Context context, String key, boolean value) {
        context.getSharedPreferences(NAME, 0).edit().putBoolean(key, value).apply();
    }

    public static boolean getBoolean(Context context, String key) {
        return context.getSharedPreferences(NAME, 0).getBoolean(key, false);
    }
}
