/**
 * Copyright (C)  2016 深圳市狗尾草智能科技有限公司
 * GowildXB
 * FileUtil.java
 */
package com.gowild.mobile.libcommon.utils;

import android.text.TextUtils;

import java.io.File;
import java.io.IOException;

/**
 * @author Ji.Li
 * @since 2016/8/1 10:00
 * @version 1.0
 * <p><strong>Features draft description.主要功能介绍</strong></p>
 */
@SuppressWarnings("unused")
public final class FileUtil {

    // ===========================================================
    // Constants
    // ===========================================================

    private static final String TAG = FileUtil.class.getSimpleName();
    private static final String MSG_CREATE_DIR_FAIL        = "Create directory fail.";
    private static final String MSG_CREATE_FILE_FAIL       = "Create file fail.";
    private static final String MSG_CREATE_PARENT_DIR_FAIL = "Create parent directory fail.";

    // ===========================================================
    // Static Fields
    // ===========================================================

    // ===========================================================
    // Fields
    // ===========================================================


    // ===========================================================
    // Constructors
    // ===========================================================

    private FileUtil() {}

    // ===========================================================
    // Getter or Setter
    // ===========================================================


    // ===========================================================
    // Methods for/from SuperClass/Interfaces
    // ===========================================================

    public static File createFile(String filePath, boolean writable, boolean isDirectory) throws IOException {
        if (TextUtils.isEmpty(filePath)){
            return null;
        }
        File file = new File(filePath);
        if (file.exists()){
            return file;
        }
        file.setWritable(writable);
        boolean isSuccess = false;
        if (isDirectory) {
            isSuccess = file.mkdirs();
            if (!isSuccess){
                throw new IOException(MSG_CREATE_DIR_FAIL);
            }
        } else {
            File parentFile = file.getParentFile();
            if (parentFile.exists()) {
                isSuccess = file.createNewFile();
                if (!isSuccess) {
                    throw new IOException(MSG_CREATE_FILE_FAIL);
                }
            } else {
                isSuccess = parentFile.mkdirs();
                if (!isSuccess) {
                    throw new IOException(MSG_CREATE_PARENT_DIR_FAIL);
                }
                isSuccess = file.createNewFile();
                if (!isSuccess) {
                    throw new IOException(MSG_CREATE_FILE_FAIL);
                }
            }
        }
        return file;
    }

    public static boolean deleteFile(File file) {
        if (file == null || !file.exists()) {
            return false;
        }
        if (file.isDirectory()) {
            File[] list = file.listFiles();
            for (File f : list) {
                deleteFile(f);
            }
            return file.delete();
        } else {
            return file.delete();
        }
    }

    public static boolean deleteFile(String filePath) {
        if (!isValidFilePath(filePath)) {
            return false;
        }
        File file = new File(filePath);
        if (file.isDirectory()) {
            File[] list = file.listFiles();
            for (File f : list) {
                deleteFile(f);
            }
            return file.delete();
        } else {
            return file.delete();
        }
    }

    public static boolean isValidFilePath(String filePath) {
        return true;
    }

    // ===========================================================
    // Methods
    // ===========================================================


    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================

}
