package com.gowild.mobile.libcommon.utils;

import android.content.Context;
import android.os.Environment;
import android.text.TextUtils;


public class PathUtil {

	@SuppressWarnings("unused")
	private static final String TAG = PathUtil.class.getSimpleName();
	private PathUtil(){}
	
	public static final String getSdcardPath(Context context) {
		StringBuilder sb = new StringBuilder();
		//优先选择SDCard,如果没有SDCard选择应用的安装路径下
		String state = Environment.getExternalStorageState();
		if (TextUtils.equals(state, Environment.MEDIA_MOUNTED)){
			//SDCard可写
			sb.append(Environment.getExternalStorageDirectory());
		}else{
			//存储在应用程序file下
			sb.append(context.getFilesDir().toString());
		}
		return sb.toString();
	}
}
