/**
 * Copyright (C)  2016 深圳市狗尾草智能科技有限公司
 * GowildXB
 * NumberUtils.java
 */
package com.gowild.mobile.libcommon.utils;

/**
 * @author shuai. wang
 * @version 1.0
 *          <p><strong>Features draft description.主要功能介绍</strong></p>
 * @since 2016/8/11 16:10
 */
public class NumberUtils {

	// ===========================================================
	// Constants
	// ===========================================================

	private static final String TAG = NumberUtils.class.getSimpleName();


	// 阿拉伯数字转中文小写
	public static String transition(int num) {
		String si = num + "";
		String s = null;
		String[] aa = { "", "十", "百", "千", "万", "十万", "百万", "千万", "亿", "十亿" };
		String[] bb = { "一", "二", "三", "四", "五", "六", "七", "八", "九" };
		char[] ch = si.toCharArray();
		int maxindex = ch.length;
		// 字符的转换
		// 两位数的特殊转换
		if (maxindex == 2) {
			for (int i = maxindex - 1, j = 0; i >= 0; i--, j++) {
				if (ch[j] != 48) {
					if (j == 0 && ch[j] == 49) {
						s = aa[i].toString();
					} else {
						s = s + (bb[ch[j] - 49] + aa[i]).toString();
					}
				}
			}
			// 其他位数的特殊转换，使用的是int类型最大的位数为十亿
		} else {
			for (int i = maxindex - 1, j = 0; i >= 0; i--, j++) {
				if (ch[j] != 48) {
					s = (bb[ch[j] - 49] + aa[i]).toString();
				}
			}
		}

		return s.replaceAll("null","");
	}


	/**
	 * 检查是否输入了特殊字符，若后期需要屏蔽新字符可直接添加
	 * @param tag
	 * @return
	 */
	public static boolean containsUnChar(String tag) {
		boolean isOk = false;
		String regex = "[-_——`~!@#$%^&*()+=|{}':;',\\[\\].<>/?~！@#￥%……&*（）——+|{}【】‘；：”“’。，、？\\s]";
        /*if (!TextUtils.isEmpty(tag)) {
            int length = tag.length();
            for (int i = 0; i < length; i++) {
                String ch = tag.substring(i, i + 1);
                if (ch.matches(regex)) {
                    isOk = true;
                    break;
                }
            }
        }*/
		if (tag.matches(regex)) {
			isOk = true;
		}
		return isOk;
	}

	/**
	 * 检测是否有emoji表情
	 *
	 * @param source
	 * @return
	 */
	public static boolean containsEmoji(String source) {
		int len = source.length();
		for (int i = 0; i < len; i++) {
			char codePoint = source.charAt(i);
			if (!isEmojiCharacter(codePoint)) { //如果不能匹配,则该字符是Emoji表情
				return true;
			}
		}
		return false;
	}

	/**
	 * 判断是否是Emoji
	 *
	 * @param codePoint 比较的单个字符
	 * @return
	 */
	private static boolean isEmojiCharacter(char codePoint) {
		return (codePoint == 0x0) || (codePoint == 0x9) || (codePoint == 0xA) ||
				(codePoint == 0xD) || ((codePoint >= 0x20) && (codePoint <= 0xD7FF)) ||
				((codePoint >= 0xE000) && (codePoint <= 0xFFFD)) || ((codePoint >= 0x10000)
				&& (codePoint <= 0x10FFFF));
	}

}


