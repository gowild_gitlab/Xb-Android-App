/**
 * Copyright(c) 2014 ShenZhen Gowild Intelligent Technology Co., Ltd.
 * All rights reserved.
 * Created on  2014-3-17  下午4:54:42
 */
package com.gowild.mobile.libcommon.utils;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * 时间辅助类
 */
public final class TimeUtil {

    /** */
    private static final String COMMON_DATE = "yyyy-MM-dd";
    private static final String COMMOD_DATE_TIME = "yyyyMMdd";

    /** */
    private static final String COMMON_DATE_TIME = "yyyy-MM-dd HH:mm:ss";

    private static final String COMMON_DATE_HOUR_MINUTE = "yyyy-MM-dd HH:mm";

    /** */
    private static final String COMMON_TIME = "HH:mm:ss";

    private static final String COMMON_DEFAULT_TIME = "yyyyMMdd E HH:mm";

    private static final String COMMON_DEFAULT_TIME_SUPER = "yyyyM月dd日  E HH:mm";

    private static final String COMMON_DATE_WEEK = "MMdd E";

    private static final String COMMON_DATE_WEEK_SUPER = "M月dd日  E";

    private static final String COMMON_DATE_WEEK_TIME = "MMdd E HH:mm";

    /**
     *
     */

    private TimeUtil() {
    }

    /**
     * 获取系统距1970年1月1日总毫秒
     *
     * @return
     */
    public static long getSysCurTimeMillis() {
        return TimeUtil.getCalendar().getTimeInMillis();
    }

    /**
     * 获取系统距1970年1月1日总秒
     *
     * @return
     */
    public static long getSysCurSeconds() {
        return TimeUtil.getCalendar().getTimeInMillis() / 1000;
    }

    /**
     * 获取系统当前时间
     *
     * @return
     */
    public static Timestamp getSysteCurTime() {
        Timestamp ts = new Timestamp(TimeUtil.getCalendar().getTimeInMillis());
        return ts;
    }

    /**
     * @return
     */
    public static Timestamp getSysMonth() {
        Calendar now = TimeUtil.getCalendar();
        now.set(Calendar.DAY_OF_MONTH, 1);
        now.set(Calendar.HOUR_OF_DAY, 0);
        now.set(Calendar.MINUTE, 0);
        now.set(Calendar.SECOND, 0);
        now.set(Calendar.MILLISECOND, 0);
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        formatter.format(now.getTime());
        return new Timestamp(now.getTimeInMillis());
    }

    /**
     * 获取指定日期距1970年1月1日总秒
     *
     * @param date
     * @return
     */
    public static long getDateToSeconds(final Date date) {
        return TimeUtil.getCalendar(date).getTimeInMillis() / 1000;
    }

    /**
     * 获取当前时间的秒
     *
     * @return
     */
    public static int getSysTimeSeconds() {
        Calendar cal = TimeUtil.getCalendar();
        return cal.get(Calendar.HOUR_OF_DAY) * 3600
                + cal.get(Calendar.MINUTE) * 60
                + cal.get(Calendar.SECOND);
    }

    /**
     * 获取指定日期距1970年1月1日总毫秒
     *
     * @param date
     * @return
     */
    public static long getDateToMillis(final Date date) {
        if (date == null) {
            return getSysCurTimeMillis();
        }
        return getCalendar(date).getTimeInMillis();
    }

    /**
     * 获取当前小时
     *
     * @return
     */
    public static int getCurrentHour() {
        return TimeUtil.getCalendar().get(Calendar.HOUR_OF_DAY);
    }

    /**
     * 获 取当前分钟
     *
     * @return
     */
    public static int getCurrentMinute() {
        return TimeUtil.getCalendar().get(Calendar.MINUTE);
    }

    /**
     * 获取当前分钟
     *
     * @return
     */
    public static int getCurrentSecond() {
        return TimeUtil.getCalendar().get(Calendar.SECOND);
    }

    /**
     * 获取当前天
     */
    public static int getCurrentDay() {
        return TimeUtil.getCalendar().get(Calendar.DAY_OF_MONTH);
    }

    /**
     * 指定的毫秒long值转成Timestamp类型
     *
     * @param value
     * @return
     */
    public static Timestamp getMillisToDate(final long value) {
        return new Timestamp(value);
    }

    /**
     * 当前系统时间增加值
     *
     * @param type
     * @param value
     * @return
     */
    public static Date addSystemCurTime(final int type, final int value) {
        Calendar cal = TimeUtil.getCalendar();
        switch (type) {
            case Calendar.DATE:// 增加天数
                cal.add(Calendar.DATE, value);
                break;
            case Calendar.HOUR:// 增加小时
                cal.add(Calendar.HOUR, value);
                break;
            case Calendar.MINUTE:// 增加分钟
                cal.add(Calendar.MINUTE, value);
                break;
            case Calendar.SECOND:// 增加秒
                cal.add(Calendar.SECOND, value);
                break;
            case Calendar.MILLISECOND:// 增加毫秒
                cal.add(Calendar.MILLISECOND, value);
                break;
            default:
                break;
        }
        return new Date(cal.getTimeInMillis());
    }

    /**
     * @return
     */
    public static Date getNextDate() {
        Calendar cal = TimeUtil.getCalendar();
        cal.add(Calendar.DATE, 1);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.add(Calendar.MILLISECOND, 0);
        return new Date(cal.getTimeInMillis());
    }

    /**
     * 解析日期
     *
     * @param
     * @return
     */
    public static Date parseDate(final String dateStr) {
        try {
            Date date = new SimpleDateFormat(COMMON_DATE).parse(dateStr);
            return date;
        } catch (ParseException e) {
            return null;
        }
    }

    public static Date parseDate2(final String dateStr) {
        try {
            Date date = new SimpleDateFormat(COMMOD_DATE_TIME).parse(dateStr);
            return date;
        } catch (ParseException e) {
            return null;
        }
    }

    /**
     * 解析日期
     */
    public static Date parseDateANDWeekANDTime(final String dateStr) {
        try {
            Date date = new SimpleDateFormat(COMMON_DATE_WEEK_TIME).parse(dateStr);
            return date;
        } catch (ParseException e) {
            return null;
        }
    }

    /**
     * 解析日期
     */
    public static Date parseYearAndDateAndWeekAndTime(final String dateStr) {
        try {
            Date date = new SimpleDateFormat(COMMON_DEFAULT_TIME_SUPER).parse(dateStr);
            return date;
        } catch (ParseException e) {
            return null;
        }
    }

    /**
     * 解析日期
     *
     * @param
     * @return
     */
    public static Date parseDateAndTime(final String dateStr) {
        try {
            Date date = new SimpleDateFormat(COMMON_DATE_TIME).parse(dateStr);
            return date;
        } catch (ParseException e) {
            return null;
        }
    }

    /**
     * 格式化日期
     *
     * @param date
     * @return
     */
    public static String getDateFormat(final Date date) {
        return new SimpleDateFormat(COMMON_DATE).format(date);
    }

    public static String getDateHourMinute(final Date date) {
        return new SimpleDateFormat(COMMON_DATE_HOUR_MINUTE).format(date);
    }

    /**
     * 格式化日期
     *
     * @param date
     * @return
     */
    public static String getDateAndTimeFormat(final Date date) {
        return new SimpleDateFormat(COMMON_DATE_TIME).format(date);
    }

    /**
     * 格式化日期
     */
    public static String getDateAndWeekAndTimeFormat(final Date date) {
        return new SimpleDateFormat(COMMON_DATE_WEEK_TIME).format(date);
    }

    /**
     * 格式化日期
     *
     * @param date
     * @return
     */
    public static String getTimeFormat(final Date date) {
        return new SimpleDateFormat(COMMON_TIME).format(date);
    }

    public static String getTimeDateAndWeek(final Date date) {
        return new SimpleDateFormat(COMMON_DATE_WEEK).format(date);
    }

    public static final String getTimeDateAndWeekSuper(final Date date) {
        return new SimpleDateFormat(COMMON_DATE_WEEK_SUPER).format(date);
    }

    public static String getTimeFromDateAndTimeType(final Date date, String Type) {
        return new SimpleDateFormat(Type).format(date);
    }
    public static Date parseDateAndTimeFromType(final String dateStr,String type) {
        try {
            Date date = new SimpleDateFormat(type).parse(dateStr);
            return date;
        } catch (ParseException e) {
            return null;
        }
    }

    /**
     * 获取默认日期2000-01-01
     *
     * @return 返回默认起始时间
     */
    public static Date getDefaultDate() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(2000, 0, 1, 0, 0, 0);
        return calendar.getTime();
    }

    /**
     * 获取默认目上限日期2999-01-01
     *
     * @return 返回默认上限时间
     */
    public static Date getDefaultMaxDate() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(2999, 0, 1, 0, 0, 0);
        return calendar.getTime();
    }

    /**
     * <pre>
     * 比较日期是否同一天(注意：分界线为晚上 12 点)
     * </pre>
     *
     * @param date
     * @return
     */
    public static boolean dateCompare(final Date date) {
        if (date == null) {
            return false;
        }
        Calendar now = TimeUtil.getCalendar();
        Calendar other = TimeUtil.getCalendar(date);
        return TimeUtil.dateCompare(now, other) == 0 ? true : false;
    }

    /**
     * <pre>
     * 比较日期是否同一天(注意：分界线为晚上 12 点)
     * </pre>
     *
     * @param
     * @return
     */
    public static boolean dateCompare(final long date) {
        Calendar now = TimeUtil.getCalendar();
        Calendar other = TimeUtil.getCalendar(TimeUtil.getMillisToDate(date));
        return TimeUtil.dateCompare(now, other) == 0 ? true : false;
    }

    /**
     * <pre>
     * 比较是否为同一天(注意：分界线为凌晨 5 点)
     * </pre>
     *
     * @param date
     * @return
     */
    public static boolean dateCompare5(final Date date) {
        if (date == null) {
            return false;
        }
        Calendar now = TimeUtil.getCalendar();
        now.add(Calendar.HOUR_OF_DAY, -5);
        Calendar other = TimeUtil.getCalendar(date);
        other.add(Calendar.HOUR_OF_DAY, -5);
        if (TimeUtil.dateCompare(now, other) == 0) {
            return true;
        }
        return false;
    }

    /**
     * <pre>
     * 比较日期是否同一天(注意：分界线为晚上 12 点)
     * </pre>
     *
     * @param date1
     * @param date2
     * @return
     */
    public static boolean dataCompare(final Date date1, final Date date2) {
        if (date1 == null || date2 == null) {
            return false;
        }
        Calendar c1 = TimeUtil.getCalendar(date1);
        Calendar c2 = TimeUtil.getCalendar(date2);
        return TimeUtil.dateCompare(c1, c2) == 0 ? true : false;
    }

    /**
     * 比较是否是同一天的同一个小时
     *
     * @param date1
     * @param date2
     * @return
     */
    public static boolean hourCompare(Date date1, Date date2) {
        if (date1 == null || date2 == null) {
            return false;
        }
        Calendar c1 = TimeUtil.getCalendar(date1);
        Calendar c2 = TimeUtil.getCalendar(date2);
        return c1.get(Calendar.HOUR_OF_DAY) == c2.get(Calendar.HOUR_OF_DAY)
                && TimeUtil.dateCompare(c1, c2) == 0;
    }

    /**
     * 返回两个日期相差天数
     *
     * @param startDate 开始日期
     * @param endDate   结束日期
     * @return
     */
    public static int dateCompare(Calendar startDate, Calendar endDate) {
        startDate.set(Calendar.HOUR_OF_DAY, 0);
        startDate.set(Calendar.MINUTE, 0);
        startDate.set(Calendar.SECOND, 0);
        startDate.set(Calendar.MILLISECOND, 0);

        endDate.set(Calendar.HOUR_OF_DAY, 0);
        endDate.set(Calendar.MINUTE, 0);
        endDate.set(Calendar.SECOND, 0);
        endDate.set(Calendar.MILLISECOND, 0);

        int day = (int) (endDate.getTimeInMillis() / 1000 / 60 / 60 / 24
                - startDate.getTimeInMillis() / 1000 / 60 / 60 / 24);
        return day;
    }

    /**
     * <pre>
     * 返回两个日期相差天数(注意：分界线为晚上 12 点)
     * </pre>
     *
     * @param startDate
     * @param endDate
     * @return
     * @throws Exception
     */
    public static int dateCompare(final Date startDate, final Date endDate) {
        if (startDate == null || endDate == null) {
            return 0;
        }
        Calendar c1 = TimeUtil.getCalendar(startDate);
        Calendar c2 = TimeUtil.getCalendar(endDate);
        return TimeUtil.dateCompare(c1, c2);
    }

    /**
     * <pre>
     * 返回两个日期相差天数(注意：分界线为凌晨 5 点)
     * </pre>
     *
     * @param
     * @return
     */
    public static int dateCompare5(Date startDate, Date endDate) {
        if (startDate == null || endDate == null) {
            return 0;
        }
        Calendar c1 = TimeUtil.getCalendar(startDate);
        c1.add(Calendar.HOUR_OF_DAY, -5);
        Calendar c2 = TimeUtil.getCalendar(endDate);
        c2.add(Calendar.HOUR_OF_DAY, -5);
        return TimeUtil.dateCompare(c1, c2);
    }

    /**
     * 比较日期是否是同一个月份
     *
     * @param date 被比较的日期
     * @return
     */
    public static boolean monthCompare(final Date date) {
        if (date == null) {
            return false;
        }
        Calendar now = TimeUtil.getCalendar();
        Calendar other = TimeUtil.getCalendar(date);
        int nowMonth = now.get(Calendar.MONTH) + 1;
        int otherMonth = other.get(Calendar.MONTH) + 1;
        return (otherMonth - nowMonth) == 0 ? true : false;
    }

    /**
     * 比较两个时间的秒
     *
     * @param date1
     * @param date2
     * @return
     */
    public static boolean secondCompare(final long date1, final long date2) {
        Calendar d1Calendar = TimeUtil.getCalendar(new Date(date1));
        Calendar d2Calendar = TimeUtil.getCalendar(new Date(date2));
        return d1Calendar.get(Calendar.SECOND) == d2Calendar
                .get(Calendar.SECOND);

    }

    /**
     * 比较指定日期是否跟当前时间在同一年的同个月份
     *
     * @param date 被比较的日期
     * @return
     */
    public static boolean monthOfYearCompare(final Date date) {
        if (date == null) {
            return false;
        }
        Calendar now = TimeUtil.getCalendar();
        Calendar other = TimeUtil.getCalendar(date);
        int nowMonth = now.get(Calendar.MONTH) + 1;
        int nowYear = now.get(Calendar.YEAR);
        int otherMonth = other.get(Calendar.MONTH) + 1;
        int otherYear = other.get(Calendar.YEAR);
        return (nowMonth == otherMonth) && (nowYear == otherYear);
    }

    /**
     * 比较指定日期是否跟当前时间在同个周(注意设置星期几是一周中的第一天)
     *
     * @param date 被比较的日期
     * @return
     */
    public static boolean weekOfYearCompare(final Date date) {
        if (date == null) {
            return false;
        }
        Calendar now = TimeUtil.getCalendar();
        now.setFirstDayOfWeek(Calendar.MONDAY); // 设置周一为一周中的第一天
        Calendar other = TimeUtil.getCalendar(date);
        other.setFirstDayOfWeek(Calendar.MONDAY);

        //  此算法暂时注释
        //        now.set(Calendar.HOUR_OF_DAY, 0);
        //        now.set(Calendar.MINUTE, 0);
        //        now.set(Calendar.SECOND, 0);
        //        other.set(Calendar.HOUR_OF_DAY, 0);
        //        other.set(Calendar.MINUTE, 0);
        //        other.set(Calendar.SECOND, 0);
        //
        //        int nowWeek = now.get(Calendar.WEEK_OF_YEAR);
        //        int otherWeek = other.get(Calendar.WEEK_OF_YEAR);
        //
        //        if (nowWeek == otherWeek) { // 周数相同的并且相差在七天之内的
        //            if (Math.abs(now.getTimeInMillis() - other.getTimeInMillis()) < (7 * 24 * 3600 * 1000)) {
        //                return true;
        //            }
        //        }
        //        return false;

        // 国军算法
        int yearDistance = Math
                .abs(now.get(Calendar.YEAR) - other.get(Calendar.YEAR));
        int monthDistance = Math.abs(
                now.get(Calendar.MONTH) - other.get(Calendar.MONTH));
        //年差大于等于1就返回false
        if (yearDistance > 1) {
            return false;
        }
        //年差不等于0 并且  年差和月差的积不能小于等于1返回false
        if ((yearDistance != 0) && monthDistance <= 1) {
            return false;
        }
        if (now.get(Calendar.WEEK_OF_YEAR) != other
                .get(Calendar.WEEK_OF_YEAR)) {
            return false;
        }
        return true;

    }

    /**
     * 获取该月的天数
     *
     * @return
     */
    public static int monthDays() {
        Calendar now = TimeUtil.getCalendar();
        return now.getActualMaximum(Calendar.DAY_OF_MONTH);
    }

    /**
     * 获取当前是该月的第几天
     *
     * @return
     */
    public static int monthDay() {
        Calendar now = TimeUtil.getCalendar();
        return now.get(Calendar.DAY_OF_MONTH);
    }

    /**
     * @param startTime
     * @param endTime
     * @return
     */
    public static long calcDistanceMillis(final Date startTime,
                                          final Date endTime) {
        long startSecond = TimeUtil.getDateToSeconds(startTime);
        long endSecond = TimeUtil.getDateToSeconds(endTime);
        return (endSecond - startSecond) * 1000;
    }

    /**
     * @param secondTime
     * @return
     */
    public static int timeToFrame(final int secondTime) {
        return (secondTime * 25) / 1000;
    }

    /**
     *
     * @return
     */
//    public static String getSignStr() {
//        String[] strs = new String[] { "a", "b", "c", "d", "e", "f", "g", "h",
//                        "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s",
//                        "t", "u", "v", "w", "x", "y", "z", "0", "1", "2", "3",
//                        "4", "5", "6", "7", "8", "9" };
//
//        String signStr = "";
//        for (int i = 0; i < 6; i++) {
//            int j = RandomUtil.next(strs.length);
//            signStr += strs[j];
//
//        }
//        return signStr;
//    }

    /**
     * 获取系统时间
     *
     * @return
     */
    public static Calendar getCalendar() {
        return Calendar.getInstance();
    }

    /**
     * 获取指定的时间
     *
     * @param date
     * @return
     */
    public static Calendar getCalendar(final Date date) {
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(date);
        return calendar;
    }

    /**
     * @param calendar
     * @return
     */
    public static Timestamp getCalendarToDate(final Calendar calendar) {
        if (calendar != null) {
            return new Timestamp(TimeUtil.getCalendar().getTimeInMillis());
        }
        return null;
    }

    /**
     * @param date
     * @param value
     * @return
     */
    public static Date addDate(final Date date, final long value) {
        long time = date.getTime() + value;
        return new Date(time);
    }

    /**
     * 是否同一天
     *
     * @return
     */
    public static boolean isSameDay(Calendar calendar1, Calendar calendar2) {
        return ((calendar1.get(Calendar.YEAR) == calendar2.get(Calendar.YEAR)) //年是否相等
                && (calendar1.get(Calendar.MONTH) == calendar2
                .get(Calendar.MONTH)) //月是否相等
                && (calendar1.get(Calendar.DAY_OF_MONTH) == calendar2
                .get(Calendar.DAY_OF_MONTH)));

    }

    /**
     * 是否同一天
     *
     * @return
     */
    public static boolean isSameDay(Date date1, Date date2) {
        Calendar calendar1 = Calendar.getInstance();
        Calendar calendar2 = Calendar.getInstance();
        calendar1.setTime(date1);
        calendar2.setTime(date2);
        return isSameDay(calendar1, calendar2);
    }

    /**
     * 把日期类型转换为字节数组
     *
     * @param date
     * @return
     */
    public static byte[] dateToBytes(final Date date) {
        Calendar calendar = Calendar.getInstance();
        byte[] byteArray = new byte[7];
        calendar.setTime(date);
        short year = (short) calendar.get(Calendar.YEAR);
        byteArray[0] = (byte) ((year >>> 8) & 0xFF);
        byteArray[1] = (byte) (year & 0xFF);
        byteArray[2] = (byte) (calendar.get(Calendar.MONTH) + 1);
        byteArray[3] = (byte) calendar.get(Calendar.DATE);
        byteArray[4] = (byte) calendar.get(Calendar.HOUR_OF_DAY);
        byteArray[5] = (byte) calendar.get(Calendar.MINUTE);
        byteArray[6] = (byte) calendar.get(Calendar.SECOND);
        return byteArray;
    }

    /**
     * @return
     */
    public static boolean isBetweenHour(Calendar cal1, Calendar cal2) {
        Calendar calendar = TimeUtil.getCalendar();
        if (calendar.after(cal1) && calendar.before(cal2)) {
            return true;
        }
        return false;
    }
}
