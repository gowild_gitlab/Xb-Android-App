/**
 * Copyright (C)  2016 深圳市狗尾草智能科技有限公司
 * GowildXB
 * WBShareActivity.java
 */
package com.gowild.socialshare.wbapi;

import android.app.Activity;

import com.umeng.socialize.media.WBShareCallBackActivity;

/**
 * @author temp
 * @since 2016/9/6 16:29
 * @version 1.0
 * <p><strong>Features draft description.主要功能介绍</strong></p>
 */
public class WBShareActivity extends WBShareCallBackActivity {

    // ===========================================================
    // Constants
    // ===========================================================

    private static final String TAG = WBShareActivity.class.getSimpleName();

    // ===========================================================
    // Static Fields
    // ===========================================================

    // ===========================================================
    // Fields
    // ===========================================================


    // ===========================================================
    // Constructors
    // ===========================================================


    // ===========================================================
    // Getter or Setter
    // ===========================================================


    // ===========================================================
    // Methods for/from SuperClass/Interfaces
    // ===========================================================


    // ===========================================================
    // Methods
    // ===========================================================


    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================

}
