package com.gowild.socialshare;

import android.app.Activity;
import android.content.Context;

import com.tencent.mm.sdk.openapi.WXAPIFactory;
import com.tencent.open.utils.Util;
import com.umeng.socialize.Config;
import com.umeng.socialize.PlatformConfig;
import com.umeng.socialize.ShareAction;
import com.umeng.socialize.UMShareListener;
import com.umeng.socialize.bean.SHARE_MEDIA;
import com.umeng.socialize.media.UMImage;
import com.umeng.socialize.shareboard.SnsPlatform;
import com.umeng.socialize.utils.ShareBoardlistener;

/**
 * @Title: GowildShare.java
 * @Package com.gowild.share
 * @Description: TODO(添加描述)
 * @author 张帅  zhang_shuai@gowild.cn
 * @date 2015年7月22日 上午10:56:49
 * @version V1.0
 */
public class GowildShare implements UMShareListener {

	private static final String DESCRIPTOR = "com.umeng.share";

    public static Context mContext;


    /**
     * 根据不同的平台设置不同的分享内容</br>
     */
    public static void setShareContent(final Activity activity, final UMImage image, String url) {
        boolean isInstall = WXAPIFactory.createWXAPI(activity, "wx2235288151e5c73e").isWXAppInstalled();

        mContext = activity.getApplicationContext();
        final ShareAction action = new ShareAction(activity);

       boolean sMobileQQSupportShare = Util.isMobileQQSupportShare(activity);//false
        if (isInstall){
            if (sMobileQQSupportShare){
                action.setDisplayList(SHARE_MEDIA.WEIXIN_CIRCLE,  SHARE_MEDIA.WEIXIN, SHARE_MEDIA.QQ, SHARE_MEDIA.SINA);
            }else{
                action.setDisplayList(SHARE_MEDIA.WEIXIN_CIRCLE,  SHARE_MEDIA.WEIXIN, SHARE_MEDIA.SINA);
            }
        }else{
            if (sMobileQQSupportShare){
                action.setDisplayList(SHARE_MEDIA.QQ, SHARE_MEDIA.SINA);
            }else{
                action.setDisplayList( SHARE_MEDIA.SINA);
            }
        }
        action.setShareboardclickCallback(new ShareBoardlistener() {
                    @Override
                    public void onclick(SnsPlatform snsPlatform, SHARE_MEDIA share_media) {
                        action.withMedia(image).setCallback(new GowildShare()).setPlatform(share_media);
                        if (snsPlatform.mPlatform.equals(SHARE_MEDIA.SINA)) {
                            action.withText("来自未来的智能机器人-小白（狗尾草智能机器人） http://www.gowild.cn");
                        }
                        action.share();
                    }
                })
                .open();
    }

    public static void configPlatform() {
        PlatformConfig.setWeixin("wx2235288151e5c73e", "5985dab525979bb57e198c1ca143bd12");
        //微信 appid appsecret
        PlatformConfig.setSinaWeibo("898475828","373337fea86e53899741c7bd10831034");
        //新浪微博需要
        Config.REDIRECT_URL = "http://sns.whalecloud.com/sina2/callback";
        //新浪微博 appkey appsecret
        PlatformConfig.setQQZone("1104996944", "y3aY8WMlcaeAc78o");
        // QQ和Qzone appid appkey
    }

    @Override
    public void onResult(SHARE_MEDIA share_media) {
        //ToastUtils.showToast(mContext, mContext.getString(R.string.share_success), false);
    }

    @Override
    public void onError(SHARE_MEDIA share_media, Throwable throwable) {
        //ToastUtils.showToast(mContext, mContext.getString(R.string.share_fail), false);
    }

    @Override
    public void onCancel(SHARE_MEDIA share_media) {
        //ToastUtils.showToast(mContext, mContext.getString(R.string.share_cancel), false);
    }
}

