/**
 * Copyright (C)  2016 深圳市狗尾草智能科技有限公司
 * Sliding
 * HonorWealthFragment.java
 */
package com.gowild.mobileclient.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.gowild.mobileclient.R;
import com.gowild.mobileclient.event.TimeEvent;
import com.gowild.mobileclient.widget.loopview.LoopView;
import com.gowild.mobileclient.widget.loopview.OnItemSelectedListener;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;


/**
 * @author sirshuai
 * @version 1.0
 * @since 2016/7/23 15:26
 */
public class TimeFragment extends Fragment {
    // ===========================================================
    // Constructors
    // ===========================================================
    @Bind(R.id.min)
    LoopView mMin;
    @Bind(R.id.hour)
    LoopView mHour;

    // ===========================================================
    // Getter or Setter
    // ===========================================================


    // ===========================================================
    // Methods for/from SuperClass/Interfaces
    // ===========================================================


    protected List<String> hon      = new ArrayList<>();
    protected List<String> min      = new ArrayList<>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.gowild_time_fragment, null);
        ButterKnife.bind(this, view);
        getTime();
        setTime();
        setLensenter();
        return view;
    }

    private String hour;
    private String minutes;

    private void setLensenter() {
        final TimeEvent mEvent =new TimeEvent(hour, minutes, mIndex);
        mHour.setListener(new OnItemSelectedListener() {


            @Override
            public void onItemSelected(int index) {
                mEvent.hour = hon.get(index);
                EventBus.getDefault().post(mEvent);
            }
        });

        mMin.setListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(int index) {
                mEvent.minutes = min.get(index);
                EventBus.getDefault().post(mEvent);
            }
        });


    }

    private void setTime() {
        String[] split = title.split(":");
        hour = split[0].replace("从","").replace("至","");
        minutes = split[1];
        for (int i = 0; i < hon.size(); i++) {
            if (hon.get(i).equals(hour)) {
                mHour.setInitPosition(i);
            }
        }

        for (int i = 0; i < min.size(); i++) {
            if (min.get(i).equals(minutes)) {
                mMin.setInitPosition(i);
            }
        }
    }

    private void getTime() {
        for (int i = 0; i < 24; i++) {
            if (i < 10) {
                hon.add("0" + i);
            } else {
                hon.add(i + "");
            }
        }
        for (int i = 0; i < 60; i++) {
            if (i < 10) {
                min.add("0" + i);
            } else {
                min.add(i + "");
            }
        }
        mHour.setTextSize(18);
        mMin.setTextSize(18);
        mHour.setItems(hon);
        mMin.setItems(min);
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_confirm:
                showSelectedResult();
                break;
            default:
                break;
        }
    }

    private void showSelectedResult() {

    }


    private String title;
    private int mIndex;

    public void setTime(String title, int index) {
        this.title = title;
        this.mIndex = index;
    }
}
