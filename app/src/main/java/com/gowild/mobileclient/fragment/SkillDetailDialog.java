/**
 * Copyright (C)  2016 深圳市狗尾草智能科技有限公司
 * MyTool
 * SkillDetailDialog.java
 */
package com.gowild.mobileclient.fragment;

import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import com.gowild.mobile.libimage.ImageUtil;
import com.gowild.mobileclient.R;
import com.gowild.mobileclient.config.Constants;
import com.gowild.mobileclient.model.SkillBack;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * @author 王揆
 * @since 2016/7/20 20:06
 * @version 1.0
 * <p><strong>Features draft description.主要功能介绍</strong></p>
 */
public class SkillDetailDialog extends DialogFragment {

    private static final String TAG = SkillDetailDialog.class.getSimpleName();

    @Bind(R.id.tv_name)
    protected TextView mTvName;
    @Bind(R.id.iv_thumb)
    protected ImageView mIvThumb;
    @Bind(R.id.tv_status)
    protected TextView mTvStatus;
    @Bind(R.id.tv_skill_des_content)
    protected TextView mTvSkillDes;
    @Bind(R.id.tv_unlock_condition_content)
    protected TextView mTvUnlockCondition;
    @Bind(R.id.tv_need_gold_content)
    protected TextView mNeedGold;

    private SkillBack.SkillItem mInfo;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_gowild_skill_detail, container, false);
        //container.setBackgroundResource(R.drawable.gowild_gray_background);
        ButterKnife.bind(this, view);
        getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.transparent)));
        initView();
        return view;
    }

    private void initView() {
        mInfo = getArguments().getParcelable(Constants.INFO);
        mTvName.setText(mInfo.skillName);
        mTvStatus.setText(mInfo.isLock() ? getString(R.string.gowild_lock) : getString(R.string.gowild_unlock));
        mTvSkillDes.setText(mInfo.skillDesc);
        mTvUnlockCondition.setText(mInfo.conditions);
        mNeedGold.setText("" + mInfo.costCoin);
        ImageUtil.getInstance().displayImage(mInfo.skillIconDetail, mIvThumb);
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        if (!hidden) {
            initView();
        }
        super.onHiddenChanged(hidden);
    }

    @OnClick({R.id.iv_detail_close})
    public void onClick(View view) {
        dismiss();
    }
}
