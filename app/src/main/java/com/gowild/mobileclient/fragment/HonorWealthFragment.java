/**
 * Copyright (C)  2016 深圳市狗尾草智能科技有限公司
 * Sliding
 * HonorWealthFragment.java
 */
package com.gowild.mobileclient.fragment;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.gowild.mobile.libcommon.utils.SpUtils;
import com.gowild.mobileclient.R;
import com.gowild.mobileclient.adapter.HonorWealthAdapter;
import com.gowild.mobileclient.application.GowildApplication;
import com.gowild.mobileclient.config.Constants;
import com.gowild.mobileclient.event.Update;
import com.gowild.mobileclient.manager.GowildAsyncHttpResponseHandler;
import com.gowild.mobileclient.manager.GowildHttpManager;
import com.gowild.mobileclient.model.HonorWealthModel;
import com.gowild.mobileclient.vo.RequestWealthMine;
import com.gowild.mobileclient.vo.RequestWealthRank;
import com.gowild.mobileclient.vo.WealthMine;
import com.gowild.mobileclient.vo.WealthRank;
import com.gowild.mobileclient.vo.WealthRankPageInfo;
import com.gowild.mobileclient.widget.PullToRefreshLayout;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Administrator
 * @version 1.0
 *          <p><strong> 小编荣誉下的财富排名界面 </strong></p>
 * @since 2016/7/23 15:26
 */
public class HonorWealthFragment extends Fragment {

    // ===========================================================
    // HostConstants
    // ===========================================================

    private static final String TAG = HonorWealthFragment.class.getSimpleName();

    // ===========================================================
    // Static Fields
    // ===========================================================

    // ===========================================================
    // Fields
    // ===========================================================

    private ListView mGowildWealthList;
    private PullToRefreshLayout mGowildWealthPtrl;
    private boolean isFirstIn = true;
    private HonorWealthAdapter mAdapter;
    private TextView wealth_rank_tv;
    private TextView wealth_coin_tv;
    private List<HonorWealthModel> mWealthList = new ArrayList<HonorWealthModel>();

    //我的财富信息请求成功
    private final int MSG_CODE_MINE_SUCCESS = 0x001;
    //我的财富信息请求失败
    private final int MSG_CODE_MINE_FAIL = 0x002;
    //
    private final int MSG_CODE_RANK_SUCCESS = 0x003;

    private final int MSG_CODE_RANK_FAIL = 0x004;
    //刷新
    private final int MSG_CODE_REFRESH = 0x005;
    //加载更多
    private final int MSG_CODE_LOAD_MORE = 0x006;
    //首次初始化
    private final int MSG_CODE_INIT = 0x007;

    private int CURRENT_HANDLER = MSG_CODE_INIT;

    private int PAGE_SIZE = 20;

    private int PAGE_NUMB = 1;

    private RequestWealthMineCallback mWealthMineCallback = new RequestWealthMineCallback(GowildApplication.context);
    private RequestWealthRankCallback mWealthRankCallback = new RequestWealthRankCallback(GowildApplication.context);

    private Handler mHandler = new Handler() {

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case MSG_CODE_MINE_SUCCESS:
                    WealthMine response = (WealthMine) msg.obj;
                    //填充我的财富信息数据
                    wealth_rank_tv.setText("NO." + response.getPetRank());
                    wealth_coin_tv.setText(response.getCoinCnt());
                    break;
                case MSG_CODE_RANK_SUCCESS:
                    WealthRankPageInfo rankResponse = (WealthRankPageInfo) msg.obj;

                    if (CURRENT_HANDLER == MSG_CODE_REFRESH) {
                        mWealthList.clear();
                        mGowildWealthPtrl.refreshFinish(PullToRefreshLayout.SUCCEED);
                        EventBus.getDefault().post(new Update(true));
                    } else if (CURRENT_HANDLER == MSG_CODE_LOAD_MORE) {
                        mGowildWealthPtrl.loadmoreFinish(PullToRefreshLayout.SUCCEED);

                        //当返回的条数小于每页请求数的时候，证明服务器数据到尾了
                        if (rankResponse.getWealthRankList().size() < PAGE_SIZE) {
                            PAGE_NUMB--;
                            Toast.makeText(getActivity(), "没有更多了", Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(getActivity(), "加载完成", Toast.LENGTH_SHORT).show();
                        }
                    }

                    fillDatas(rankResponse);
                    mAdapter.notifyDataSetChanged();
                    break;
                case MSG_CODE_RANK_FAIL:
                    break;
                case MSG_CODE_MINE_FAIL:
                    break;
            }
        }
    };


    // ===========================================================
    // Constructors
    // ===========================================================


    // ===========================================================
    // Getter or Setter
    // ===========================================================


    // ===========================================================
    // Methods for/from SuperClass/Interfaces
    // ===========================================================
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.gowild_honor_wealth_fragment, null);
        mGowildWealthPtrl = ((PullToRefreshLayout) view.findViewById(R.id.refresh_view));
        mGowildWealthList = (ListView) view.findViewById(R.id.wealth_lv);
        wealth_rank_tv = (TextView) view.findViewById(R.id.wealth_rank_tv);
        wealth_coin_tv = (TextView) view.findViewById(R.id.wealth_coin_tv);
        mAdapter = new HonorWealthAdapter(getActivity(), mWealthList);
        mGowildWealthList.setAdapter(mAdapter);
        CURRENT_HANDLER = MSG_CODE_INIT;
        mGowildWealthPtrl.setOnRefreshListener(new PullToRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh(PullToRefreshLayout pullToRefreshLayout) {
                PAGE_NUMB = 1;
                CURRENT_HANDLER = MSG_CODE_REFRESH;
                loadWealthPetRank();
            }

            @Override
            public void onLoadMore(PullToRefreshLayout pullToRefreshLayout) {
                CURRENT_HANDLER = MSG_CODE_LOAD_MORE;
                loadWealthPetRank();
            }
        });
//        LoginMananger.getInstence().startCountPage(null, "honorwealth");
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        loadData();
    }


    // ===========================================================
    // Methods
    // ===========================================================

    private void loadData() {
        loadWealthMineData();
        loadWealthPetRank();
    }

    /**
     * 加载数据
     */
    private void loadWealthMineData() {
        RequestWealthMine requestWealthMine = new RequestWealthMine();
        String petid = SpUtils.getString(GowildApplication.context,Constants.PETID);
        requestWealthMine.put(Constants.PETID, petid);
        GowildHttpManager.requestWealthMine(getActivity(), requestWealthMine, mWealthMineCallback);
    }

    private void loadWealthPetRank() {
        RequestWealthRank requestWealthRank = new RequestWealthRank();
        requestWealthRank.put(Constants.PAGENUM, String.valueOf(PAGE_NUMB));
        requestWealthRank.put(Constants.PAGESIZE, String.valueOf(PAGE_SIZE));
        GowildHttpManager.requestWealthRank(getActivity(), requestWealthRank, mWealthRankCallback);
    }


    private void fillDatas(WealthRankPageInfo rankResponse) {
        List<WealthRank> achievementList = rankResponse.getWealthRankList();
        //判断是否已经存在相同数据
        HonorWealthModel tempModel = null;
        if (mWealthList.size() >= 1) {
            tempModel = mWealthList.get(mWealthList.size() - 1);
        }
        if (achievementList.size() >= 1) {
            //重复数据不添加到集合中
            WealthRank tempAchievement = achievementList.get(achievementList.size() - 1);
            if (tempModel != null && tempAchievement.getId().equals(tempModel.getId())) {
                return;
            } else {
                int baseNum = mWealthList.size();
                for (int i = 0; i < achievementList.size(); i++) {
                    HonorWealthModel data = new HonorWealthModel();
                    WealthRank vo = achievementList.get(i);
                    data.setIndex(String.valueOf(i + baseNum + 1));
                    data.setName(vo.getNickname());
                    data.setMoneyTotal(vo.getCoinCnt());
                    data.setId(vo.getId());
                    mWealthList.add(data);
                }
            }
        }
    }


    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================

    private class RequestWealthMineCallback extends GowildAsyncHttpResponseHandler<WealthMine> {

        /**
         * 构造方法
         *
         * @param context 用于处理授权过期消息，会被转成ApplicationContext
         */
        public RequestWealthMineCallback(Context context) {
            super(context);
        }

        @Override
        public void onSuccess(int statusCode, WealthMine responseBody) {
            Log.e("callback", "onResponseSuccess");
            Message msg = mHandler.obtainMessage();
            msg.what = MSG_CODE_MINE_SUCCESS;
            msg.obj = responseBody;
            mHandler.sendMessage(msg);
        }

        @Override
        public void onFailure(int statusCode, String responseBody) {
            Log.e("callback", "onResponseFailure");
            Message msg = mHandler.obtainMessage();
            msg.what = MSG_CODE_MINE_FAIL;
            mHandler.sendMessage(msg);
        }
    }

    private class RequestWealthRankCallback extends GowildAsyncHttpResponseHandler<WealthRankPageInfo> {

        /**
         * 构造方法
         *
         * @param context 用于处理授权过期消息，会被转成ApplicationContext
         */
        public RequestWealthRankCallback(Context context) {
            super(context);
        }

        @Override
        public void onSuccess(int statusCode, WealthRankPageInfo responseBody) {
            PAGE_NUMB++;
            Message msg = mHandler.obtainMessage();
            msg.what = MSG_CODE_RANK_SUCCESS;
            msg.obj = responseBody;
            mHandler.sendMessage(msg);
        }

        @Override
        public void onFailure(int statusCode, String responseBody) {
            Message msg = mHandler.obtainMessage();
            msg.what = MSG_CODE_RANK_FAIL;
            mHandler.sendMessage(msg);
        }
    }

}
