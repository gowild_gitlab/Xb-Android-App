/**
 * Copyright (C)  2016 深圳市狗尾草智能科技有限公司
 * GowildXB
 * GowildAlertListFragment.java
 */
package com.gowild.mobileclient.fragment;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.gowild.mobile.libcommon.utils.TimeUtil;
import com.gowild.mobileclient.R;
import com.gowild.mobileclient.activity.alertandclock.GowildAddOrUpdateAlertActivity;
import com.gowild.mobileclient.activity.alertandclock.GowildAlertOrClockActivity;
import com.gowild.mobileclient.callback.OnNormalDialogListener;
import com.gowild.mobileclient.config.Constants;
import com.gowild.mobileclient.manager.GowildAsyncHttpResponseHandler;
import com.gowild.mobileclient.manager.GowildHttpManager;
import com.gowild.mobileclient.vo.Alert;
import com.gowild.mobileclient.vo.RequestDeleteAlert;
import com.gowild.mobileclient.vo.ResponseDeleteAlert;

import java.util.ArrayList;
import java.util.Date;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * @author JunDong Wang
 * @version 1.0
 *          <p><strong>显示待办列表</strong></p>
 * @since 2016/7/23 11:20
 */
public class GowildAlertListFragment extends Fragment {

    // ===========================================================
    // HostConstants
    // ===========================================================

    private static final String TAG = GowildAlertListFragment.class.getSimpleName();


    // ===========================================================
    // Static Fields
    // ===========================================================

    // ===========================================================
    // Fields
    // ===========================================================
    private Context mContext;
    @Bind(R.id.list)
    ListView mListView;
    @Bind(R.id.noalert_tips)
    LinearLayout noalertTips;
    private ListAdapter mAdapter = new ListAdapter();
    private OnItemClick mOnItemClick = new OnItemClick();
    private RequestDeleteAlertCallback mRequestDeleteAlertCallback;
    // ===========================================================
    // Constructors
    // ===========================================================


    // ===========================================================
    // Getter or Setter
    // ===========================================================


    // ===========================================================
    // Methods for/from SuperClass/Interfaces
    // ===========================================================
    boolean isChangeUi = true;


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
        mRequestDeleteAlertCallback = new RequestDeleteAlertCallback(mContext);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_gowild_alertlist, container, false);
        ButterKnife.bind(this, view);
//        LoginMananger.getInstence().startCountPage(null,"alertlist");
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        isChangeUi = true;
        mListView.setAdapter(mAdapter);
        mListView.setOnItemClickListener(mOnItemClick);
    }

    @Override
    public void onPause() {
        super.onPause();
        isChangeUi = false;
    }

    // ===========================================================
    // Methods
    // ===========================================================
    public void setAlertList(ArrayList<Alert> alertList) {
        if (!isChangeUi) {
            return;
        }
        if (alertList.size() != 0) {
            noalertTips.setVisibility(View.GONE);
            mListView.setVisibility(View.VISIBLE);
            mAdapter.update(alertList);
        } else {
            noalertTips.setVisibility(View.VISIBLE);
            mListView.setVisibility(View.GONE);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================
    private class ListAdapter extends BaseAdapter {
        private ArrayList<Alert> mAlertList = new ArrayList<>();

        public void update(ArrayList<Alert> alertList) {
            mAlertList.clear();
            mAlertList.addAll(alertList);
            notifyDataSetChanged();
        }

        @Override
        public int getCount() {
            return mAlertList.size();
        }

        @Override
        public Alert getItem(int position) {
            return mAlertList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = LayoutInflater.from(mContext).inflate(R.layout.item_gowild_alert, parent, false);
            }
            final Alert alert = mAlertList.get(position);
            Date date = TimeUtil.parseDateAndTimeFromType(alert.mRemindTime, "yyyy-MM-dd HH:mm:ss");
            TextView tvDate = (TextView) convertView.findViewById(R.id.date);
            tvDate.setText(TimeUtil.getTimeFromDateAndTimeType(date, "yyyy.MM.dd"));
            TextView tvTime = (TextView) convertView.findViewById(R.id.time);
            tvTime.setText(TimeUtil.getTimeFromDateAndTimeType(date, "HH:mm"));
            TextView tvContent = (TextView) convertView.findViewById(R.id.content);
            tvContent.setText(alert.mContent);
            RelativeLayout rltDelete = (RelativeLayout) convertView.findViewById(R.id.delete);
            rltDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //要弹出一个dialog 确定才能删除
                    final AlertFragmentDialog dialog = new AlertFragmentDialog();
                    dialog.setTitle("您确定删除待办吗");
                    dialog.show(getFragmentManager(), null);
                    dialog.setDialogListener(new OnNormalDialogListener() {
                        @Override
                        public void onCancelListener() {
                            dialog.dismiss();
                        }

                        @Override
                        public void onConfirmListener() {
                            RequestDeleteAlert requestDeleteAlert = new RequestDeleteAlert();
                            requestDeleteAlert.put(Constants.REMIND_ID, Integer.toString(alert.mRemindId));
                            ((GowildAlertOrClockActivity) mContext).showLoading(R.string.loading);
                            GowildHttpManager.requestDeleteAlert(mContext, requestDeleteAlert, mRequestDeleteAlertCallback);

                        }
                    });
                }
            });
            return convertView;
        }

    }


    private class OnItemClick implements AdapterView.OnItemClickListener {

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            Alert alert = mAdapter.getItem(position);
            Intent intent = new Intent(mContext, GowildAddOrUpdateAlertActivity.class);
            intent.putExtra(Constants.TYPE, GowildAlertOrClockActivity.TYPE_UPDATE);
            intent.putExtra(Constants.CONTENT, alert.mContent);
            intent.putExtra(Constants.REMIND_ID, alert.mRemindId);
            intent.putExtra(Constants.REMIND_TIME, alert.mRemindTime);
            startActivity(intent);
        }
    }

    @OnClick({R.id.add_alert})
    public void onclick(View view) {
        switch (view.getId()) {
            case R.id.add_alert:
                Intent intent = new Intent(mContext, GowildAddOrUpdateAlertActivity.class);
                intent.putExtra(Constants.TYPE, GowildAlertOrClockActivity.TYPE_ADD);
                startActivity(intent);
                break;
        }
    }

    private class RequestDeleteAlertCallback extends GowildAsyncHttpResponseHandler<ResponseDeleteAlert> {


        /**
         * 构造方法
         *
         * @param context 用于处理授权过期消息，会被转成ApplicationContext
         */
        public RequestDeleteAlertCallback(Context context) {
            super(context);
        }

        @Override
        public void onSuccess(int statusCode, ResponseDeleteAlert responseBody) {
            ((GowildAlertOrClockActivity) mContext).hideLoading();
            if (statusCode == 100) {
                ((GowildAlertOrClockActivity) mContext).requestAlerts();
            }
        }

        @Override
        public void onFailure(int statusCode, String responseBody) {
            ((GowildAlertOrClockActivity) mContext).hideLoading();
//            ToastUtils.showCustom(mContext, R.string.gowild_network_error, false);
        }
    }


}
