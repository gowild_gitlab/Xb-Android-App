/**
 * Copyright (C)  2016 深圳市狗尾草智能科技有限公司
 * Sliding
 * HonorAchievementFragment.java
 */
package com.gowild.mobileclient.fragment;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import com.gowild.mobileclient.R;
import com.gowild.mobileclient.adapter.HonorAchievementAdapter;
import com.gowild.mobileclient.application.GowildApplication;
import com.gowild.mobileclient.config.Constants;
import com.gowild.mobileclient.event.Update;
import com.gowild.mobileclient.manager.GowildAsyncHttpResponseHandler;
import com.gowild.mobileclient.manager.GowildHttpManager;
import com.gowild.mobileclient.model.HonorAchievementModel;
import com.gowild.mobileclient.vo.Achievement;
import com.gowild.mobileclient.vo.AchievementPageInfo;
import com.gowild.mobileclient.vo.RequestAchievement;
import com.gowild.mobileclient.widget.PullToRefreshLayout;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Administrator
 * @version 1.0
 *          <p><strong> 小白荣誉下的勋章成就排行界面 </strong></p>
 * @since 2016/7/23 15:27
 */
public class HonorAchievementFragment extends Fragment {

    // ===========================================================
    // HostConstants
    // ===========================================================

    private static final String TAG = HonorAchievementFragment.class.getSimpleName();

    private HonorAchievementAdapter mAdapter = null;

    //请求成功
    private final int MSG_CODE_SUCCESS = 0x001;
    //请求失败
    private final int MSG_CODE_FAIL = 0x002;
    //刷新
    private final int MSG_CODE_REFRESH = 0x003;
    //加载更多
    private final int MSG_CODE_LOAD_MORE = 0x004;
    //首次初始化
    private final int MSG_CODE_INIT = 0x005;

    private int CURRENT_HANDLER = MSG_CODE_INIT;

    private int PAGE_SIZE = 20;

    private int PAGE_NUMB = 1;

    // ===========================================================
    // Static Fields
    // ===========================================================

    // ===========================================================
    // Fields
    // ===========================================================
    private ListView mGowildAchievementList;
    private PullToRefreshLayout mGowildAchievementPtrl;
    private List<HonorAchievementModel> mAchievementList = new ArrayList<HonorAchievementModel>();
    private RequestAchievementCallback mAchievementCallback = new RequestAchievementCallback(GowildApplication.context);

    private Handler mHandler = new Handler() {

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case MSG_CODE_SUCCESS:
                    AchievementPageInfo mResponse = (AchievementPageInfo) msg.obj;
                    if (null != mResponse) {
                        if (CURRENT_HANDLER == MSG_CODE_REFRESH) {
                            mAchievementList.clear();
                            mGowildAchievementPtrl.refreshFinish(PullToRefreshLayout.SUCCEED);
                            EventBus.getDefault().post(new Update(true));
                        } else if (CURRENT_HANDLER == MSG_CODE_LOAD_MORE) {
                            mGowildAchievementPtrl.loadmoreFinish(PullToRefreshLayout.SUCCEED);

                            //当返回的条数小于每页请求数的时候，证明服务器数据到尾了
                            if (mResponse.getAchievementList().size() < PAGE_SIZE) {
                                PAGE_NUMB--;
                                Toast.makeText(getActivity(), "没有更多了", Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(getActivity(), "加载完成", Toast.LENGTH_SHORT).show();
                            }
                        }

                        fillDatas(mResponse);
                        mAdapter.notifyDataSetChanged();
                    }
                    break;
                case MSG_CODE_FAIL:
                    if (CURRENT_HANDLER == MSG_CODE_REFRESH) {
                        mGowildAchievementPtrl.refreshFinish(PullToRefreshLayout.FAIL);
                    } else if (CURRENT_HANDLER == MSG_CODE_LOAD_MORE) {
                        mGowildAchievementPtrl.refreshFinish(PullToRefreshLayout.FAIL);
                    }
                    break;
            }
        }
    };



    // ===========================================================
    // Constructors
    // ===========================================================


    // ===========================================================
    // Getter or Setter
    // ===========================================================


    // ===========================================================
    // Methods for/from SuperClass/Interfaces
    // ===========================================================
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.gowild_honor_achievement_fragment, null);
        mGowildAchievementPtrl = ((PullToRefreshLayout) view.findViewById(R.id.refresh_view));
        mGowildAchievementList = (ListView) view.findViewById(R.id.content_view);
        mAdapter = new HonorAchievementAdapter(getActivity(), mAchievementList);
        mGowildAchievementList.setAdapter(mAdapter);
        CURRENT_HANDLER = MSG_CODE_INIT;
        mGowildAchievementPtrl.setOnRefreshListener(new PullToRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh(PullToRefreshLayout pullToRefreshLayout) {
                PAGE_NUMB = 1;
                CURRENT_HANDLER = MSG_CODE_REFRESH;
                loadAchievementData();
            }

            @Override
            public void onLoadMore(PullToRefreshLayout pullToRefreshLayout) {
                CURRENT_HANDLER = MSG_CODE_LOAD_MORE;
                loadAchievementData();
            }
        });
//        LoginMananger.getInstence().startCountPage(null, "honorachievement");
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        loadAchievementData();
    }

    // ===========================================================
    // Methods
    // ===========================================================

    /**
     * 加载数据
     */
    private void loadAchievementData() {
        RequestAchievement requestAchievement = new RequestAchievement();
        requestAchievement.put(Constants.PAGENUM, String.valueOf(PAGE_NUMB));
        requestAchievement.put(Constants.PAGESIZE, String.valueOf(PAGE_SIZE));
        GowildHttpManager.requestAchievement(GowildApplication.context, requestAchievement, mAchievementCallback);
    }

    private class RequestAchievementCallback extends GowildAsyncHttpResponseHandler<AchievementPageInfo> {

        /**
         * 构造方法
         *
         * @param context 用于处理授权过期消息，会被转成ApplicationContext
         */
        public RequestAchievementCallback(Context context) {
            super(context);
        }

        @Override
        public void onSuccess(int statusCode, AchievementPageInfo responseBody) {
            PAGE_NUMB++;
            Log.e("callback", "onResponseSuccess");
            Message msg = mHandler.obtainMessage();
            msg.what = MSG_CODE_SUCCESS;
            msg.obj = responseBody;
            mHandler.sendMessage(msg);
        }

        @Override
        public void onFailure(int statusCode, String responseBody) {
            Log.e("callback", "onResponseFailure");
            Message msg = mHandler.obtainMessage();
            msg.what = MSG_CODE_FAIL;
            mHandler.sendMessage(msg);
        }
    }

    private void fillDatas(AchievementPageInfo mResponse) {
        List<Achievement> achievementList = mResponse.getAchievementList();
        //判断是否已经存在相同数据
        HonorAchievementModel tempModel = null;
        if (mAchievementList.size() >= 1) {
            tempModel = mAchievementList.get(mAchievementList.size() - 1);
        }
        if (achievementList.size() >= 1) {
            //重复数据不添加到集合中
            Achievement tempAchievement = achievementList.get(achievementList.size() - 1);
            if (tempModel != null && tempAchievement.getId().equals(tempModel.getId())) {
                return;
            } else {
                for (int i = 0; i < achievementList.size(); i++) {
                    HonorAchievementModel data = new HonorAchievementModel();
                    Achievement vo = achievementList.get(i);
                    data.setId(vo.getId());
                    data.setCatalog(vo.getTypeName());
                    data.setIconUrl(vo.getPictureUrl());
                    data.setTitle(vo.getAchieveMedal());
                    data.setContent(vo.getAchieveContent());
                    data.setGoldCount(vo.getCoinCnt() + "金币");
                    data.setExperience(vo.getExperience() + "经验");
                    if (vo.getNumber() != 0) {
                        float per = vo.getAccountNumbe() / vo.getNumber() * 100;
                        data.setProgess((int) per);
                    }
                    mAchievementList.add(data);
                }
            }
        }

    }

    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================

}
