/**
 * Copyright (C)  2016 深圳市狗尾草智能科技有限公司
 * MyTool
 * PhotoListFragment.java
 */
package com.gowild.mobileclient.fragment;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.support.v7.widget.GridLayoutManager;

import com.gowild.mobile.libcommon.utils.PathUtil;
import com.gowild.mobile.libcommon.utils.ToastUtils;
import com.gowild.mobileclient.R;
import com.gowild.mobileclient.activity.PhotoAndMediaListActivity;
import com.gowild.mobileclient.adapter.PhotoRecyclerAdapter;
import com.gowild.mobileclient.base.baseactivity.GowildBaseActivity;
import com.gowild.mobileclient.config.Constants;
import com.gowild.mobileclient.db.DBHelper;
import com.gowild.mobileclient.db.DBUtil;
import com.gowild.mobileclient.manager.GowildAsyncHttpResponseHandler;
import com.gowild.mobileclient.manager.GowildHttpManager;
import com.gowild.mobileclient.model.PhotoContentInfo;
import com.gowild.mobileclient.model.PhotoListBack;
import com.gowild.mobileclient.model.PhotoVideoBaseContentInfo;
import com.gowild.mobileclient.model.TitleInfo;
import com.gowild.mobileclient.utils.CommonDataUtil;
import com.gowild.mobileclient.utils.VideoInfoUtil;
import com.gowild.mobileclient.widget.PullToRefreshLayout;

import java.io.File;
import java.util.ArrayList;

/**
 * @author 王揆
 * @since 2016/7/23 10:08
 * @version 1.0
 * <p><strong>Features draft description.主要功能介绍</strong></p>
 */
public class PhotoListFragment extends PhotoAndVideoBaseFrament {

    private static final String TAG = PhotoListFragment.class.getSimpleName();
    public static final String ISLOCAL = "islocal";

    public boolean bLocal = false;

    private int mPageNum = 1;

    public static final int CODE_RESULT_OK = 1000;
    @Override
    protected void initViews() {
        super.initViews();
    }

    public void initGridLayoutManager(GridLayoutManager manager) {
        manager.setSpanSizeLookup(mAdapter.lookup);
    }

    @Override
    public void initAdapter() {
        if (mAdapter == null) {
            mAdapter = new PhotoRecyclerAdapter(getContext(), this);
            mRecyclerView.setAdapter(mAdapter);
        }
    }

    @Override
    public void updateTitle() {
        Activity activity = getActivity();
        if (activity != null && activity instanceof PhotoAndMediaListActivity) {
            ((PhotoAndMediaListActivity) activity).setPhotoTabTitle(mContentInfos.size(), bLocal);
        }
    }

    @Override
    public void loadData() {
         bLocal = false;
//        LoginMananger.getInstence().startCountPage(null, "photolist");
        if (getArguments() != null) {
            bLocal = getArguments().getBoolean(ISLOCAL);
        }
        if (bLocal) {
            mPhotoBottomView.hideDownload();
            loadLocalPhoto();
        } else {
            new DBUtil().init(getContext());
            openPull();
            if (getActivity() != null && getActivity() instanceof PhotoAndMediaListActivity) {
                  ((GowildBaseActivity) getActivity()).showLoading(R.string.loading);
            }
            loadNetData(mPageNum);
        }
    }

    @Override
    public void  clearData() {
        mPageNum = 1;
        mTitleInfos.clear();
        mContentInfos.clear();
        if (mAdapter != null) {
            mAdapter.notifyDataSetChanged();
        }
    }

    private void openPull() {
        mPullRefreshLy.setPullUpToLoadEnable(true);
        mPullRefreshLy.setPullDownToRefreshEnable(true);
        mPullRefreshLy.setOnRefreshListener(new PullToRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh(PullToRefreshLayout pullToRefreshLayout) {
                loadNetData(1);
            }

            @Override
            public void onLoadMore(PullToRefreshLayout pullToRefreshLayout) {
                loadNetData(mPageNum);
            }
        });
    }

    private void loadLocalPhoto(){
        Thread th = new Thread(new Runnable() {
            @Override
            public void run() {
                String root = PathUtil.getSdcardPath(getContext()) + File.separator + "gowild";
                File file = new File(root + File.separator + "picture");
                ArrayList<VideoInfoUtil.PictureInfo> picList = new ArrayList<>();
                VideoInfoUtil.scanPictureDirectoryNoThrows(file, picList);
                VideoInfoUtil.sortList(picList);
                mTitleInfos.clear();
                mContentInfos.clear();
                TitleInfo titleInfo = null;
                for (VideoInfoUtil.PictureInfo pictureInfo : picList) {
                    titleInfo = reSortData(titleInfo, new PhotoContentInfo(pictureInfo));
                }
                if (getActivity() != null) {
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            updateView();
                        }
                    });
                }
            }
        });
        th.start();
    }

    private void loadNetData(final int pageNum) {
        loadNetData(pageNum, true);
    }

    private void loadNetData(final int pageNum, final boolean loadMore) {
        GowildHttpManager.requestPhotos(getContext(), pageNum, new GowildAsyncHttpResponseHandler<PhotoListBack>(getContext()) {
            @Override
            public void onSuccess(int statusCode, PhotoListBack responseBody) {
                if (getContext() == null) {
                    return;
                }
                if (pageNum == GowildHttpManager.DEFAULT_PAGE_NUM) {
                    clearData();
                }
                if (responseBody.total != 0) {
                    ArrayList<PhotoContentInfo> infos = responseBody.parcelInfo();
                    if (!infos.isEmpty()) {
                        mPageNum++;
                        parcelData(infos);
                    } else {
                        ToastUtils.showCustom(getContext(), R.string.gowild_no_more_photo, false);
                    }
                }
                ContentValues values = new ContentValues();
                Cursor cursor = null;
                for (PhotoVideoBaseContentInfo info : mContentInfos) {
                    cursor = DBUtil.query(null, "id = " + info.mId, null, null);
                    if (cursor != null){
                        if (!(cursor.getCount() > 0)){
                            values.put(DBHelper.COLUMN_ID, info.mId);
                            values.put(DBHelper.COLUMN_PHOTOURL, info.mFileOriginPath);
                            values.put(DBHelper.COLUMN_CREATETIME, info.mDate);
                            values.put(DBHelper.COLUMN_TIMESTAMP, info.mTimeStamp);
                            DBUtil.insert(values);
                        }
                    }
                }
                if (cursor!= null){
                 cursor.close();
                }
                loadComplete(loadMore);
            }

            @Override
            public void onFailure(int statusCode, String responseBody) {
                if (pageNum == GowildHttpManager.DEFAULT_PAGE_NUM) {
                    Cursor cursor = DBUtil.query(null, null, null, DBHelper.COLUMN_TIMESTAMP + " DESC");
                    if (cursor != null) {
                        ArrayList<PhotoVideoBaseContentInfo> infos = new ArrayList<PhotoVideoBaseContentInfo>();
                        while (cursor.moveToNext()) {
                            PhotoContentInfo info = new PhotoContentInfo();
                            info.mId = cursor.getInt(cursor.getColumnIndex(DBHelper.COLUMN_ID));
                            info.mTimeStamp = cursor.getLong(cursor.getColumnIndex(DBHelper.COLUMN_TIMESTAMP));
                            info.mDate = cursor.getString(cursor.getColumnIndex(DBHelper.COLUMN_CREATETIME));
                            info.mFileOriginPath = cursor.getString(cursor.getColumnIndex(DBHelper.COLUMN_PHOTOURL));
                            info.initThumbAndPreview();
                            infos.add(info);
                        }
                        resortAllInfos(infos);
                        cursor.close();
                    }
                }

                loadComplete(loadMore);
                mPullRefreshLy.setPullUpToLoadEnable(false);
                mPullRefreshLy.setPullDownToRefreshEnable(false);
            }
        });
    }

    private void loadComplete(boolean loadMore) {
        if (loadMore) {
            if (mPullRefreshLy != null) {
            mPullRefreshLy.loadmoreFinish(PullToRefreshLayout.SUCCEED);

            }
        } else {
            if (mPullRefreshLy != null) {
            mPullRefreshLy.refreshFinish(PullToRefreshLayout.SUCCEED);

            }
        }
        updateView();
        if (getActivity() != null && getActivity() instanceof PhotoAndMediaListActivity) {
           ((GowildBaseActivity) getActivity()).hideLoading();
        }
    }

    public void parcelData(ArrayList<PhotoContentInfo> infos) {
        TitleInfo titleInfo;
        if (mTitleInfos.isEmpty()) {
            titleInfo = null;
        } else {
            titleInfo = mTitleInfos.get(mTitleInfos.size() - 1);
        }

        for (PhotoContentInfo photoContentInfo : infos) {
            titleInfo = reSortData(titleInfo, photoContentInfo);
        }
    }

    @Override
    public String getEmptyTitle() {
        if (bLocal) {
            return getString(R.string.gowild_no_av_photo);
        }
        return CommonDataUtil.getCurrentRobotTitle() + "还没有拍摄任何照片哦！";
    }

    @Override
    public String getEmptyTip() {
        if (bLocal) {
            return "当你不在家里的时候，家里除了"+CommonDataUtil.getCurrentRobotTitle()+"外，还有熊孩子，宠物(狗猫或其他千奇百怪的宠物)，拿起手机客户端，就可以把家里的熊孩子装萌卖乖，或者宠物拆家的样子拍照留下来,或进行录制。";
        }
        return getString(R.string.gowild_take_photo_tip);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == CODE_RESULT_OK) {
            ArrayList<PhotoContentInfo> infos = data.getParcelableArrayListExtra(Constants.INFOS);
            resortAllInfos(infos);
        }
    }

    @Override
    public boolean setSelectMode(boolean selectMode) {
        if (super.setSelectMode(selectMode)) {
            if (selectMode) {
                mPullRefreshLy.setPullDownToRefreshEnable(false);
                mPullRefreshLy.setPullUpToLoadEnable(false);
            } else {
                mPullRefreshLy.setPullDownToRefreshEnable(true);
                mPullRefreshLy.setPullUpToLoadEnable(true);
            }
            return true;
        }
        return false;
    }
}
