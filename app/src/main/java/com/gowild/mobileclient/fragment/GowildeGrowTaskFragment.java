/**
 * Copyright (C)  2016 深圳市狗尾草智能科技有限公司
 * GowildXB
 * GowildeDailyTaskFragment.java
 */
package com.gowild.mobileclient.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.gowild.mobile.libcommon.utils.NumberUtils;
import com.gowild.mobile.libcommon.utils.ToastUtils;
import com.gowild.mobile.libimage.ImageUtil;
import com.gowild.mobileclient.R;
import com.gowild.mobileclient.application.GowildApplication;
import com.gowild.mobileclient.event.Update;
import com.gowild.mobileclient.manager.GowildAsyncHttpResponseHandler;
import com.gowild.mobileclient.manager.GowildHttpManager;
import com.gowild.mobileclient.model.GrowTaskEntry;
import com.gowild.mobileclient.model.TaskHonor;
import com.gowild.mobileclient.widget.CircleProgressBar;
import com.gowild.mobileclient.widget.PullToRefreshLayout;
import com.gowild.mobileclient.widget.PullableListView;
import com.loopj.android.http.RequestParams;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * @author shuai. wang
 * @version 1.0
 *          <p><strong>Features draft description.主要功能介绍</strong></p>
 * @since 2016/7/23 19:43
 */
public class GowildeGrowTaskFragment extends Fragment {

	// ===========================================================
	// HostConstants
	// ===========================================================

	private static final String TAG = GowildeGrowTaskFragment.class.getSimpleName();
	@Bind(R.id.pull_icon)
	ImageView mPullIcon;
	@Bind(R.id.refreshing_icon)
	ImageView mRefreshingIcon;
	@Bind(R.id.state_tv)
	TextView mStateTv;
	@Bind(R.id.state_iv)
	ImageView mStateIv;
	@Bind(R.id.head_view)
	RelativeLayout mHeadView;
	@Bind(R.id.gowild_lover_lv)
	PullableListView mListView;
	@Bind(R.id.pullup_icon)
	ImageView mPullupIcon;
	@Bind(R.id.loading_icon)
	ImageView mLoadingIcon;
	@Bind(R.id.loadstate_tv)
	TextView mLoadstateTv;
	@Bind(R.id.loadstate_iv)
	ImageView mLoadstateIv;
	@Bind(R.id.loadmore_view)
	RelativeLayout mLoadmoreView;
	@Bind(R.id.refresh_view)
	PullToRefreshLayout mRefreshView;
	//	private View mItemView;
	private MyAdapter mAdapter;
	private OnFreshLinsenter mlinsenter;
	private boolean hasNextPage = true;
	private int index = 2;
	private List<GrowTaskEntry> mList = new ArrayList<>();

	// ===========================================================
	// Static Fields
	// ===========================================================

	// ===========================================================
	// Fields
	// ===========================================================
	boolean isFinish = false;

	@Nullable
	@Override
	public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_gowild_listview_grow, null);
		ButterKnife.bind(this, view);
//		LoginMananger.getInstence().startCountPage(null,"growtask");
		return view;
	}

	@Override
	public void onActivityCreated(@Nullable Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		initData();
		initEvent();
	}




	private void initEvent() {
		if (mAdapter == null) {
			mAdapter = new MyAdapter();
		}
		mListView.setAdapter(mAdapter);
		mRefreshView.setOnRefreshListener(new PullToRefreshLayout.OnRefreshListener() {
			@Override
			public void onRefresh(PullToRefreshLayout pullToRefreshLayout) {
				//刷新数据
				loadDateFromNet(2);
				EventBus.getDefault().post(new Update(true));
			}

			@Override
			public void onLoadMore(PullToRefreshLayout pullToRefreshLayout) {
				//加载更多
				loadDateFromNet(0);
			}
		});
	}




	private class MyAdapter extends BaseAdapter {
		@Override
		public int getCount() {
			return mList.size();
		}

		@Override
		public Object getItem(int position) {
			return null;
		}


		@Override
		public long getItemId(int position) {
			return 0;
		}

		class DailyTaskItemHolder {
			ImageView mGowildTaskIvItemMedal;
			ImageView mGowildTaskIvItemComp;
			TextView mGowildTaskTvItemName;
			TextView mGowildTaskTvItemTask;
			TextView mGowildTaskTvItemExp;
			TextView mGowildTaskTvItemDes;
			TextView mGowildTaskTvItPercent;
			CircleProgressBar mGowildTaskCbItemCircle;

			public DailyTaskItemHolder(View view) {
				mGowildTaskIvItemMedal = (ImageView) view.findViewById(R.id.gowild_task_iv_item_medal);
				mGowildTaskIvItemComp = (ImageView) view.findViewById(R.id.gowild_task_cb_item_comp);
				mGowildTaskTvItemName = (TextView) view.findViewById(R.id.gowild_task_tv_item_name);
				mGowildTaskTvItemTask = (TextView) view.findViewById(R.id.gowild_task_tv_item_task);
				mGowildTaskTvItemExp = (TextView) view.findViewById(R.id.gowild_task_tv_item_exp);
				mGowildTaskTvItemDes = (TextView) view.findViewById(R.id.gowild_task_tv_item_des);
				mGowildTaskTvItPercent = (TextView) view.findViewById(R.id.gowild_task_cb_item_percent);
				mGowildTaskCbItemCircle = (CircleProgressBar) view.findViewById(R.id.gowild_task_cb_item_circle);
			}

		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			DailyTaskItemHolder holder = null;
			if (convertView == null) {
				convertView = View.inflate(GowildApplication.context, R.layout.fragment_gowild_listview_item, null);
				holder = new DailyTaskItemHolder(convertView);
				convertView.setTag(holder);
			} else {
				holder = (DailyTaskItemHolder) convertView.getTag();
			}
			GrowTaskEntry task = mList.get(position);
			ImageUtil.getInstance().displayImage(task.pictureUrl,holder.mGowildTaskIvItemMedal);

			holder.mGowildTaskTvItemTask.setText(String.format("任务%s", NumberUtils.transition(position + 1)));
			holder.mGowildTaskTvItemDes.setText(task.achieveContent);
			holder.mGowildTaskTvItemExp.setText(String.format("%s经验", task.experience));
			holder.mGowildTaskTvItemName.setText(task.achieveMedal);
			float per = Float.parseFloat(task.accountNumber) / Float.parseFloat(task.number);
			if (per >= 1) {
				per = 1;
				holder.mGowildTaskTvItPercent.setVisibility(View.GONE);
				holder.mGowildTaskIvItemComp.setVisibility(View.VISIBLE);
			} else {
				holder.mGowildTaskIvItemComp.setVisibility(View.GONE);
				holder.mGowildTaskTvItPercent.setVisibility(View.VISIBLE);
				holder.mGowildTaskTvItPercent.setText((int) (per * 100) + "%");
			}
			holder.mGowildTaskCbItemCircle.setProgress((int) (per * 100));
			return convertView;
		}
	}


	private void initData() {
		loadDateFromNet(1);
	}

	int type = 1;
	private void loadDateFromNet(int fresh) {

		RequestParams params = new RequestParams();
		if (fresh == 1) {
			type = 1;
			params.add("pageNum", 1 + "");
			mlinsenter.start();
			index = 2;
		} else if (fresh == 2) {
			type = 1;
			index = 2;
			params.add("pageNum", 1 + "");
		} else {
			type = 0;
			params.add("pageNum", index + "");//加载更多
			if (!hasNextPage) {
				ToastUtils.showCustom(GowildApplication.context, "没有更多数据", false);
				mRefreshView.refreshFinish(PullToRefreshLayout.SUCCEED);
				return;
			}
			index++;
		}
		params.add("pageSize", 10 + "");

		GowildHttpManager.getGrowTask(GowildApplication.context, params, new GowildAsyncHttpResponseHandler<TaskHonor>(GowildApplication.context) {

			@Override
			public void onSuccess(int statusCode, TaskHonor responseBody) {
				if (isFinish) {
					return;
				}

				if (responseBody.list == null || responseBody.list.size()< 10){
					hasNextPage = false;
				}else{
					hasNextPage = true;
				}

				if (responseBody.list == null){
					mRefreshView.loadmoreFinish(PullToRefreshLayout.SUCCEED);
					ToastUtils.showCustom(GowildApplication.context,"没有更多数据了",false);
					return;
				}

//				hasNextPage = responseBody.hasNextPage;//是否有下一页

				if (type == 1) {
					mList.clear();
				}
				for (GrowTaskEntry entry : responseBody.list) {
					mList.add(entry);
				}
				mAdapter.notifyDataSetChanged();

				if (type == 1) {
					mRefreshView.refreshFinish(PullToRefreshLayout.SUCCEED);
				}

				if (type == 0) {
					mRefreshView.loadmoreFinish(PullToRefreshLayout.SUCCEED);
				}
				mlinsenter.finish();
			}

			@Override
			public void onFailure(int statusCode, String responseBody) {
				mlinsenter.finish();
				if (TextUtils.isEmpty(responseBody)) {
					return;
				}
				if (type == 1) {
					mRefreshView.refreshFinish(PullToRefreshLayout.FAIL);
				}
				if (type == 0) {
					mRefreshView.loadmoreFinish(PullToRefreshLayout.FAIL);
				}
			}
		});
	}

	public interface OnFreshLinsenter {
		void start();

		void finish();
	}


	public void setOnFreshLinsenter(OnFreshLinsenter linsenter) {
		this.mlinsenter = linsenter;
	}


	@Override
	public void onDestroyView() {
		super.onDestroyView();
		isFinish = true;
		ButterKnife.unbind(this);
	}

	// ===========================================================
	// Constructors
	// ===========================================================


	// ===========================================================
	// Getter or Setter
	// ===========================================================


	// ===========================================================
	// Methods for/from SuperClass/Interfaces
	// ===========================================================


	// ===========================================================
	// Methods
	// ===========================================================


	// ===========================================================
	// Inner and Anonymous Classes
	// ===========================================================

}
