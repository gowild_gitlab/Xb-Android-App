/**
 * Copyright (C)  2016 深圳市狗尾草智能科技有限公司
 * MyTool
 * SkillDetailDialog.java
 */
package com.gowild.mobileclient.fragment;

import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import com.gowild.mobileclient.R;

import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * @author shuai .wang
 * @version 1.0
 *          <p><strong>Features draft description.主要功能介绍</strong></p>
 * @since 2016/7/20 20:06
 */
public class LunchDialog extends DialogFragment {


	private static final String TAG = LunchDialog.class.getSimpleName();


	@Nullable
	@Override
	public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.activity_gowild_lunch_dialog, container, false);
		//container.setBackgroundResource(R.drawable.gowild_gray_background);
		ButterKnife.bind(this, view);
		getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
		getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.transparent)));
		return view;
	}


	public interface OnClickChangeLinsenter {
		void startLogin();

		void changeAccount();
	}

	OnClickChangeLinsenter mLinsenter;


	public void setOnClickChangeLinsenter(OnClickChangeLinsenter mLinsenter){
		this.mLinsenter = mLinsenter;
	}

	@OnClick({R.id.iv_detail_close, R.id.gowild_chooselunch_tv_start, R.id.gowild_chooselunch_tv_end})
	public void onClick(View view) {
		switch (view.getId()) {
			case R.id.iv_detail_close:
				dismiss();
				break;
			case R.id.gowild_chooselunch_tv_start:
				mLinsenter.startLogin();
				break;
			case R.id.gowild_chooselunch_tv_end:
				mLinsenter.changeAccount();
				break;

			default:
				break;
		}
	}

	@Override
	public void onDestroyView() {
		super.onDestroyView();
		ButterKnife.unbind(this);
	}
}
