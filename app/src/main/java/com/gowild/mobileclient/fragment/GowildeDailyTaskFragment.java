/**
 * Copyright (C)  2016 深圳市狗尾草智能科技有限公司
 * GowildXB
 * GowildeDailyTaskFragment.java
 */
package com.gowild.mobileclient.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.gowild.mobile.libcommon.utils.NumberUtils;
import com.gowild.mobile.libcommon.utils.SpUtils;
import com.gowild.mobileclient.R;
import com.gowild.mobileclient.application.GowildApplication;
import com.gowild.mobileclient.config.Constants;
import com.gowild.mobileclient.event.Update;
import com.gowild.mobileclient.manager.GowildAsyncHttpResponseHandler;
import com.gowild.mobileclient.manager.GowildHttpManager;
import com.gowild.mobileclient.vo.DaliyTask;
import com.gowild.mobileclient.widget.CircleProgressBar;
import com.gowild.mobileclient.widget.PullToRefreshLayout;
import com.gowild.mobileclient.widget.PullableListView;
import com.loopj.android.http.RequestParams;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * @author shuai. wang
 * @version 1.0
 *          <p><strong>Features draft description.主要功能介绍</strong></p>
 * @since 2016/7/23 19:43
 */
public class GowildeDailyTaskFragment extends Fragment {

	// ===========================================================
	// HostConstants
	// ===========================================================
	boolean isFinish = false;
	private static final String TAG = GowildeDailyTaskFragment.class.getSimpleName();
	@Bind(R.id.pull_icon)
	ImageView mPullIcon;
	@Bind(R.id.refreshing_icon)
	ImageView mRefreshingIcon;
	@Bind(R.id.state_tv)
	TextView mStateTv;
	@Bind(R.id.state_iv)
	ImageView mStateIv;
	@Bind(R.id.head_view)
	RelativeLayout mHeadView;
	@Bind(R.id.gowild_lover_lv)
	PullableListView mListView;
	@Bind(R.id.pullup_icon)
	ImageView mPullupIcon;
	@Bind(R.id.loading_icon)
	ImageView mLoadingIcon;
	@Bind(R.id.loadstate_tv)
	TextView mLoadstateTv;
	@Bind(R.id.loadstate_iv)
	ImageView mLoadstateIv;
	@Bind(R.id.loadmore_view)
	RelativeLayout mLoadmoreView;
	@Bind(R.id.refresh_view)
	PullToRefreshLayout mRefreshView;
	//	private View mItemView;
	private ArrayList<DaliyTask> datas = new ArrayList<>();
	private MyAdapter mAdapter;
	private OnFreshLinsenter mlinsenter;

	// ===========================================================
	// Static Fields
	// ===========================================================

	// ===========================================================
	// Fields
	// ===========================================================

	@Nullable
	@Override
	public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_gowild_listview_grow,null);
		ButterKnife.bind(this, view);
//		LoginMananger.getInstence().startCountPage(null, "dailytask");
		return view;
	}

	@Override
	public void onActivityCreated(@Nullable Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		initData();
		initEvent();
	}

	private void initEvent() {
		if (mAdapter == null) {
			mAdapter = new MyAdapter();
		}
		mListView.setAdapter(mAdapter);
		mRefreshView.setPullUpToLoadEnable(false);
		mRefreshView.setOnRefreshListener(new PullToRefreshLayout.OnRefreshListener() {
			@Override
			public void onRefresh(PullToRefreshLayout pullToRefreshLayout) {
				//刷新数据
				loadDateFromNet();
				EventBus.getDefault().post(new Update(true));
			}

			@Override
			public void onLoadMore(PullToRefreshLayout pullToRefreshLayout) {
				//加载更多
			}
		});
	}

	@Override
	public void onDestroyView() {
		super.onDestroyView();
		ButterKnife.unbind(this);
		isFinish = true;
	}

	public void refreshData() {
		loadDateFromNet();
	}


	private class MyAdapter extends BaseAdapter {
		@Override
		public int getCount() {
			return datas.size();
		}

		@Override
		public Object getItem(int position) {
			return null;
		}




		@Override
		public long getItemId(int position) {
			return 0;
		}

		class DailyTaskItemHolder {
			TextView mGowildTaskTvItemTask;
			CircleProgressBar mGowildTaskCbItemCircle;
			TextView mGowildTaskTvItemTaskDes;
			TextView mGowildTaskTvItemGod;
			TextView mGowildTaskTvItemPer;
			TextView mGowildTaskTvItemTaskExp;
			ImageView mGowildTaskIvItemComp;

			public DailyTaskItemHolder(View view) {
				mGowildTaskIvItemComp = (ImageView) view.findViewById(R.id.gowild_task_tv_item_comp);
				mGowildTaskTvItemTask = (TextView) view.findViewById(R.id.gowild_task_tv_item_task);
				mGowildTaskCbItemCircle = (CircleProgressBar) view.findViewById(R.id.gowild_task_cb_item_circle);
				mGowildTaskTvItemTaskDes = (TextView) view.findViewById(R.id.gowild_task_tv_item_task_des);
				mGowildTaskTvItemGod = (TextView) view.findViewById(R.id.gowild_task_tv_item_god);
				mGowildTaskTvItemTaskExp = (TextView) view.findViewById(R.id.gowild_task_tv_item_task_exp);
				mGowildTaskTvItemPer = (TextView) view.findViewById(R.id.gowild_task_cb_item_percent);
			}

		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			DailyTaskItemHolder holder = null;
			if (convertView == null) {
				convertView = View.inflate(GowildApplication.context, R.layout.fragment_gowild_listview_item_daily, null);
				holder = new DailyTaskItemHolder(convertView);
				convertView.setTag(holder);
			} else {
				holder = (DailyTaskItemHolder) convertView.getTag();
			}
			DaliyTask daliyTask = datas.get(position);
			holder.mGowildTaskTvItemTask.setText(String.format("任务%s", NumberUtils.transition(position+1)));
			holder.mGowildTaskTvItemTaskDes.setText(daliyTask.name);
			holder.mGowildTaskTvItemGod.setText(daliyTask.coinCnt + "金币");
			holder.mGowildTaskTvItemTaskExp.setText(daliyTask.experience + "经验");
			float per = Float.parseFloat(daliyTask.accountCount)/Float.parseFloat(daliyTask.count);

			if (per >= 1) {
				per = 1;
				holder.mGowildTaskTvItemPer.setVisibility(View.GONE);
				holder.mGowildTaskIvItemComp.setVisibility(View.VISIBLE);
			} else {
				holder.mGowildTaskIvItemComp.setVisibility(View.GONE);
				holder.mGowildTaskTvItemPer.setVisibility(View.VISIBLE);
				holder.mGowildTaskTvItemPer.setText((int) (per * 100) + "%");
			}

			holder.mGowildTaskCbItemCircle.setProgress((int) (per * 100));
			return convertView;
		}
	}




	private void initData() {
		mlinsenter.start();
		loadDateFromNet();
	}

	private void loadDateFromNet() {
		RequestParams params = new RequestParams();
		params.add("petLevel", SpUtils.getString(GowildApplication.context, Constants.LEVEL));
		GowildHttpManager.requestDailyTask(GowildApplication.context, params, new GowildAsyncHttpResponseHandler<DaliyTask>(GowildApplication.context, true) {

			@Override
			public void onSuccess(int statusCode, DaliyTask responseBody) {
			}

			@Override
			public void onSuccess(int statusCode, ArrayList<DaliyTask> responseBody) {
				super.onSuccess(statusCode, responseBody);
				if (isFinish) {
					return;
				}
				mlinsenter.finish();
				datas.clear();
				datas.addAll(responseBody);
				mAdapter.notifyDataSetChanged();
				mRefreshView.refreshFinish(PullToRefreshLayout.SUCCEED);
			}

			@Override
			public void onFailure(int statusCode, String responseBody) {
				mlinsenter.finish();
				if (TextUtils.isEmpty(responseBody)) {
					return;
				}
				mRefreshView.refreshFinish(PullToRefreshLayout.FAIL);
			}
		});
	}

	public interface OnFreshLinsenter {
		void start();
		void finish();
	}


	public void setOnFreshLinsenter(OnFreshLinsenter linsenter) {
		this.mlinsenter = linsenter;
	}


	// ===========================================================
	// Constructors
	// ===========================================================


	// ===========================================================
	// Getter or Setter
	// ===========================================================


	// ===========================================================
	// Methods for/from SuperClass/Interfaces
	// ===========================================================


	// ===========================================================
	// Methods
	// ===========================================================


	// ===========================================================
	// Inner and Anonymous Classes
	// ===========================================================

}
