/**
 * Copyright (C)  2016 深圳市狗尾草智能科技有限公司
 * GowildXB
 * GowildClockListFragment.java
 */
package com.gowild.mobileclient.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.gowild.mobileclient.R;
import com.gowild.mobileclient.activity.alertandclock.GowildAddOrUpdateClockActivity;
import com.gowild.mobileclient.activity.alertandclock.GowildAlertOrClockActivity;
import com.gowild.mobileclient.callback.OnNormalDialogListener;
import com.gowild.mobileclient.config.Constants;
import com.gowild.mobileclient.manager.GowildAsyncHttpResponseHandler;
import com.gowild.mobileclient.manager.GowildHttpManager;
import com.gowild.mobileclient.vo.Clock;
import com.gowild.mobileclient.vo.RequestDeleteClock;
import com.gowild.mobileclient.vo.ResponseDeleteClock;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * @author JunDong Wang
 * @version 1.0
 *          <p><strong>显示闹钟列表</strong></p>
 * @since 2016/7/23 11:20
 */
public class GowildClockListFragment extends Fragment {

    // ===========================================================
    // HostConstants
    // ===========================================================

    private static final String TAG = GowildClockListFragment.class.getSimpleName();


    // ===========================================================
    // Static Fields
    // ===========================================================

    // ===========================================================
    // Fields
    // ===========================================================
    private Context mContext;
    @Bind(R.id.list)
    ListView mListView;
    @Bind(R.id.noclock_tips)
    LinearLayout noclockTips;
    private ListAdapter mAdapter = new ListAdapter();
    private OnItemClick mOnItemClick = new OnItemClick();
    private RequestDeleteClockCallback mRequestDeleteClockCallback;

    // ===========================================================
    // Constructors
    // ===========================================================


    // ===========================================================
    // Getter or Setter
    // ===========================================================


    // ===========================================================
    // Methods for/from SuperClass/Interfaces
    // ===========================================================


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
        mRequestDeleteClockCallback = new RequestDeleteClockCallback(mContext);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_gowild_clocklist, container, false);
        ButterKnife.bind(this, view);
//        LoginMananger.getInstence().startCountPage(null, "clocklist");
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        mListView.setAdapter(mAdapter);
        mListView.setOnItemClickListener(mOnItemClick);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    // ===========================================================
    // Methods
    // ===========================================================
    public void setClockList(ArrayList<Clock> clockList) {
        if (clockList.size() != 0) {
            noclockTips.setVisibility(View.GONE);
            mListView.setVisibility(View.VISIBLE);
            mAdapter.update(clockList);
        } else {
            noclockTips.setVisibility(View.VISIBLE);
            mListView.setVisibility(View.GONE);
        }
    }

    public String checkRepeat(int cycleIndex) {
        if(cycleIndex==0){
            return getResources().getString(R.string.gowild_alert_and_clock_never);
        }
        String repeat = "";
        String[] weekArr = mContext.getResources().getStringArray(R.array.week_type);
        for (int i = 1; i <= 7; i++) {
            if ((cycleIndex & (1 << (i % 7))) != 0) {
                repeat = repeat + weekArr[i - 1] + " ";
            }
        }
        return repeat;
    }


    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================
    private class ListAdapter extends BaseAdapter {
        private ArrayList<Clock> mClockList = new ArrayList<>();

        public void update(ArrayList<Clock> clockList) {
            mClockList.clear();
            mClockList.addAll(clockList);
            notifyDataSetChanged();
        }

        @Override
        public int getCount() {
            return mClockList.size();
        }

        @Override
        public Clock getItem(int position) {
            return mClockList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = LayoutInflater.from(mContext).inflate(R.layout.item_gowild_clock, parent, false);
            }
            final Clock clock = mClockList.get(position);
            TextView tvTime = (TextView) convertView.findViewById(R.id.time);
            tvTime.setText(clock.mHour + ":" + clock.mMinute);
            TextView tvRepead = (TextView) convertView.findViewById(R.id.repead);
            tvRepead.setText(checkRepeat(clock.mCycleIndex));
            TextView tvContent = (TextView) convertView.findViewById(R.id.content);
            tvContent.setText(clock.mAlarmFlag);
            RelativeLayout rltDelete = (RelativeLayout) convertView.findViewById(R.id.delete);
            rltDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    final AlertFragmentDialog dialog = new AlertFragmentDialog();
                    dialog.setTitle("您确定删除闹钟吗");
                    dialog.show(getFragmentManager(), null);
                    dialog.setDialogListener(new OnNormalDialogListener() {
                        @Override
                        public void onCancelListener() {
                            dialog.dismiss();
                        }

                        @Override
                        public void onConfirmListener() {
                            RequestDeleteClock mRequestDeleteClock = new RequestDeleteClock();
                            mRequestDeleteClock.put(Constants.ALARM_ID, Integer.toString(clock.mAlarmId));
                            ((GowildAlertOrClockActivity) mContext).showLoading(R.string.loading);
                            GowildHttpManager.requestDeleteClock(mContext, mRequestDeleteClock, mRequestDeleteClockCallback);
                        }
                    });
                }
            });
            return convertView;
        }

    }


    private class OnItemClick implements AdapterView.OnItemClickListener {

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            Clock clock = mAdapter.getItem(position);
            Intent intent = new Intent(mContext, GowildAddOrUpdateClockActivity.class);
            intent.putExtra(Constants.TYPE, GowildAlertOrClockActivity.TYPE_UPDATE);
            intent.putExtra(Constants.ALARM_ID, clock.mAlarmId);
            intent.putExtra(Constants.ALARM_FLAG, clock.mAlarmFlag);
            intent.putExtra(Constants.HOUR, clock.mHour);
            intent.putExtra(Constants.MINUTE, clock.mMinute);
            intent.putExtra(Constants.CYCLE_INDEX, clock.mCycleIndex);
            startActivity(intent);
        }
    }

    @OnClick({R.id.add_clock})
    public void onclick(View view) {
        switch (view.getId()) {
            case R.id.add_clock:
                Intent intent = new Intent(mContext, GowildAddOrUpdateClockActivity.class);
                intent.putExtra(Constants.TYPE, GowildAlertOrClockActivity.TYPE_ADD);
                startActivity(intent);
                break;
        }
    }

    private class RequestDeleteClockCallback extends GowildAsyncHttpResponseHandler<ResponseDeleteClock> {


        /**
         * 构造方法
         *
         * @param context 用于处理授权过期消息，会被转成ApplicationContext
         */
        public RequestDeleteClockCallback(Context context) {
            super(context);
        }

        @Override
        public void onSuccess(int statusCode, ResponseDeleteClock responseBody) {
            ((GowildAlertOrClockActivity) mContext).hideLoading();
            if (statusCode == 100) {
                ((GowildAlertOrClockActivity) mContext).requestClocks();
            }
        }

        @Override
        public void onFailure(int statusCode, String responseBody) {
            ((GowildAlertOrClockActivity) mContext).hideLoading();
//            ToastUtils.showCustom(mContext, R.string.gowild_network_error, false);
        }
    }
}
