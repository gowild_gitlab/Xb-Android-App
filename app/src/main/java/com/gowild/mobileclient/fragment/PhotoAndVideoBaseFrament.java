/**
 * Copyright (C)  2016 深圳市狗尾草智能科技有限公司
 * GowildXB
 * PhotoAndVideoBaseFrament.java
 */
package com.gowild.mobileclient.fragment;

import android.graphics.Rect;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.gowild.mobileclient.R;
import com.gowild.mobileclient.adapter.PhotoVideoBaseAdapter;
import com.gowild.mobileclient.model.PhotoContentInfo;
import com.gowild.mobileclient.model.PhotoVideoBaseContentInfo;
import com.gowild.mobileclient.model.TitleInfo;
import com.gowild.mobileclient.utils.VideoInfoUtil;
import com.gowild.mobileclient.widget.PhotoBottomView;
import com.gowild.mobileclient.widget.PullToRefreshLayout;
import com.gowild.mobileclient.widget.PullableRecyclerView;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * @author temp
 * @since 2016/8/3 21:07
 * @version 1.0
 * <p><strong>Features draft description.主要功能介绍</strong></p>
 */
public abstract class PhotoAndVideoBaseFrament extends Fragment  implements PhotoBottomView.OnPhotoDeleteCallback {

    // ===========================================================
    // HostConstants
    // ===========================================================

    private static final String TAG = PhotoAndVideoBaseFrament.class.getSimpleName();
    public static final int GRID_ROW = 4;

    @Bind(R.id.recycler)
    protected   RecyclerView         mRecyclerView;
    @Bind(R.id.refresh_view)
    protected   PullToRefreshLayout  mPullRefreshLy;
    @Bind(R.id.ly_no_photo)
    protected   View                 mVNoPhoto;
    @Bind(R.id.ly_photo_list_bottom)
    protected   PhotoBottomView      mPhotoBottomView;

    protected PhotoVideoBaseAdapter mAdapter;
    protected ArrayList<TitleInfo>                 mTitleInfos   = new ArrayList<>();
    protected ArrayList<PhotoVideoBaseContentInfo> mContentInfos = new ArrayList<>();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View contentView = LayoutInflater.from(container.getContext()).
                inflate(R.layout.fragment_gowild_photolist, container, false);
        ButterKnife.bind(this, contentView);
        mRecyclerView = (PullableRecyclerView) contentView.findViewById(R.id.recycler);
        mPhotoBottomView.setInfo(mContentInfos);
        mPhotoBottomView.setOnPhotoDeleteCallback(this);
        mPhotoBottomView.hideShare();
        initViews();
        loadData();
        return contentView;
    }

    public abstract void loadData();
    public void clearData(){};

    protected void initViews() {
        GridLayoutManager manager = new GridLayoutManager(getContext(), GRID_ROW);
        initAdapter();
        initGridLayoutManager(manager);
        mRecyclerView.setLayoutManager(manager);
        //int space = getResources().getDimensionPixelSize(R.dimen.gowild_photo_item_ver_padding);
        //mRecyclerView.addItemDecoration(new SpaceItemDecoration(space));
        int padding = getResources().getDimensionPixelSize(R.dimen.gowild_photo_item__hor_padding);
        int paddingEdge = getResources().getDimensionPixelSize(R.dimen.gowild_photo_list_padding_edge);
        mRecyclerView.setPadding(paddingEdge, 0, paddingEdge, 0);
        //mRecyclerView.addItemDecoration(new SpaceItemDecoration(padding));
        mPullRefreshLy.setPullUpToLoadEnable(false);
        mPullRefreshLy.setPullDownToRefreshEnable(false);
    }

    public void initGridLayoutManager(GridLayoutManager manager) {
    }

    public abstract void initAdapter();

    private class SpaceItemDecoration extends RecyclerView.ItemDecoration {
        private int space;

        public SpaceItemDecoration(int space) {
            this.space = space;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            int position = parent.getChildAdapterPosition(view);
            if(position / GRID_ROW != 0) {
                outRect.top = space;
            }

            if (position % GRID_ROW != 0) {
                outRect.left = space;
            }
        }
    }

    public TitleInfo reSortData(TitleInfo titleInfo, PhotoVideoBaseContentInfo contentInfo) {
        if (titleInfo == null || !titleInfo.mName.equals(contentInfo.mDate)) {
            if (mTitleInfos.size() > 0) {
                titleInfo = mTitleInfos.get(mTitleInfos.size() - 1);
                titleInfo.mEndContentPos = mContentInfos.size() - 1;
            }
            titleInfo = new TitleInfo(contentInfo.mDate);
            titleInfo.mStartContentPos = mContentInfos.size();
            mTitleInfos.add(titleInfo);
        }
        mContentInfos.add(contentInfo);
        titleInfo.mEndContentPos = mContentInfos.size() - 1;
        return titleInfo;
    }

    public Runnable mUpdateViewRunnable = new Runnable() {
        @Override
        public void run() {
            updateView();
        }
    };

    public void updateView() {
        if (mTitleInfos.isEmpty()) {
            mPullRefreshLy.setVisibility(View.GONE);
            mVNoPhoto.setVisibility(View.VISIBLE);
            TextView view = (TextView) mVNoPhoto.findViewById(R.id.tv_no_photo_title);
            view.setText(getEmptyTitle());
            view = (TextView) mVNoPhoto.findViewById(R.id.tv_take_photo_tip);
            view.setText(Html.fromHtml(getEmptyTip()));
            mPhotoBottomView.setVisibility(View.GONE);
        } else {
            mVNoPhoto.setVisibility(View.GONE);
            mPullRefreshLy.setVisibility(View.VISIBLE);
            mAdapter.setInfos(mTitleInfos, mContentInfos);
            mAdapter.notifyDataSetChanged();
            mPhotoBottomView.setInfo(mContentInfos);
            //mPhotoBottomView.setVisibility(View.VISIBLE);
        }

        updateTitle();
    }

    public abstract void updateTitle();

    public boolean setSelectMode(boolean selectMode) {
        if (mAdapter != null) {
            mAdapter.setSelectMode(selectMode);
            mAdapter.notifyDataSetChanged();
            if (selectMode) {
                mPhotoBottomView.setVisibility(View.VISIBLE);
            } else {
                mPhotoBottomView.setVisibility(View.GONE);
            }
            return true;
        }
        return false;
    }

    public abstract String getEmptyTip();
    public abstract String getEmptyTitle();

    @Override
    public void onDelete() {
        ArrayList<PhotoVideoBaseContentInfo> infos = new ArrayList<>();
        infos.addAll(mContentInfos);
        resortAllInfos(infos);
    }

    public void resortAllInfos(ArrayList<? extends PhotoVideoBaseContentInfo> infos) {
        mTitleInfos.clear();
        mContentInfos.clear();
        TitleInfo titleInfo = null;
        for (PhotoVideoBaseContentInfo info : infos) {
            titleInfo = reSortData(titleInfo, info);
        }
        updateView();
    }

    public int getInfoSize() {
        if (mContentInfos != null) {
            return mContentInfos.size();
        }
        return 0;
    }
}
