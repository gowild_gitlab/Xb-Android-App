/**
 * Copyright (C)  2016 深圳市狗尾草智能科技有限公司
 * MyTool
 * PhotoListFragment.java
 */
package com.gowild.mobileclient.fragment;

import android.app.Activity;
import android.support.v7.widget.GridLayoutManager;

import com.gowild.mobile.libcommon.utils.PathUtil;
import com.gowild.mobileclient.R;
import com.gowild.mobileclient.activity.PhotoAndMediaListActivity;
import com.gowild.mobileclient.adapter.VideoRecyclerAdapter;
import com.gowild.mobileclient.model.TitleInfo;
import com.gowild.mobileclient.utils.VideoInfoUtil;

import java.io.File;
import java.util.ArrayList;

/**
 * @author 王揆
 * @since 2016/7/23 10:08
 * @version 1.0
 * <p><strong>Features draft description.主要功能介绍</strong></p>
 */
public class VideoListFragment extends PhotoAndVideoBaseFrament {

    @Override
    public void loadData() {
//        LoginMananger.getInstence().startCountPage(null, "videolist");
        mPhotoBottomView.hideDownload();
        mPhotoBottomView.setIsVideo();
        loadDataAsyn();
    }

    public void initGridLayoutManager(GridLayoutManager manager) {
        manager.setSpanSizeLookup(mAdapter.lookup);
    }

    @Override
    public void initAdapter() {
        if (mAdapter == null) {
            mAdapter = new VideoRecyclerAdapter(getContext());
            mRecyclerView.setAdapter(mAdapter);
        }
    }

    @Override
    public void updateTitle() {
        Activity activity = getActivity();
        if (activity != null && activity instanceof PhotoAndMediaListActivity) {
            ((PhotoAndMediaListActivity) activity).setMediaTabTitle(mContentInfos.size());
        }
    }

    @Override
    public String getEmptyTitle() {
        return getString(R.string.gowild_no_av_video);
    }

    @Override
    public String getEmptyTip() {
        return getString(R.string.gowild_no_av_tip);
    }

    private void loadDataAsyn(){
        String saveFileDirectoryPath = PathUtil.getSdcardPath(getContext().getApplicationContext()) + File.separator
                + "gowild";
        final String saveVideoDirectoryPath = saveFileDirectoryPath + File.separator + "video";
        Thread th = new Thread(new Runnable() {
            @Override
            public void run() {
                File file = new File(saveVideoDirectoryPath);
                ArrayList<VideoInfoUtil.VideoInfo> videoList = new ArrayList<>();
                VideoInfoUtil.scanDirectoryNoThrows(file, videoList);
                VideoInfoUtil.sortList(videoList);
                mTitleInfos.clear();
                mContentInfos.clear();
                TitleInfo titleInfo = null;
                for (VideoInfoUtil.VideoInfo videoInfo : videoList) {
                    titleInfo = reSortData(titleInfo, videoInfo);
                }

                if (getActivity() != null) {
                    getActivity().runOnUiThread(mUpdateViewRunnable);
                }
            }
        });
        th.start();
    }
}
