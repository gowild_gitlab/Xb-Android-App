/**
 * Copyright (C)  2016 深圳市狗尾草智能科技有限公司
 * Sliding
 * HonorAchievementModel.java
 */
package com.gowild.mobileclient.model;

/**
 * @author Administrator
 * @since 2016/7/23 18:16
 * @version 1.0
 * <p><strong>Features draft description.主要功能介绍</strong></p>
 */
public class HonorAchievementModel {

    private String id;

    private String mCatalog = null;

    private String mIconUrl = null;

    private String mTitle = null;

    private String mContent = null;

    private String mGoldCount = null;

    private String mExperience = null;

    public String getIconUrl() {
        return mIconUrl;
    }

    public String getTitle() {
        return mTitle;
    }

    public String getContent() {
        return mContent;
    }

    public String getGoldCount() {
        return mGoldCount;
    }

    public String getExperience() {
        return mExperience;
    }

    public int getProgess() {
        return mProgess;
    }

    private int mProgess = 0;

    public void setIconUrl(String mIconUrl) {
        this.mIconUrl = mIconUrl;
    }

    public void setProgess(int mProgess) {
        this.mProgess = mProgess;
    }

    public void setExperience(String mExperience) {
        this.mExperience = mExperience;
    }

    public void setGoldCount(String mGoldCount) {
        this.mGoldCount = mGoldCount;
    }

    public void setContent(String mContent) {
        this.mContent = mContent;
    }

    public void setTitle(String mTitle) {
        this.mTitle = mTitle;
    }

    public String getCatalog() {
        return mCatalog;
    }

    public void setCatalog(String mCatalog) {
        this.mCatalog = mCatalog;
    }

    public HonorAchievementModel(String catalog, String mIconUrl, String mTitle, String mContent, String mGoldCount, String mExperience, int mProgess) {
        this.mCatalog = catalog;
        this.mIconUrl = mIconUrl;
        this.mTitle = mTitle;
        this.mContent = mContent;
        this.mGoldCount = mGoldCount;
        this.mExperience = mExperience;
        this.mProgess = mProgess;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public HonorAchievementModel(){

    }
}
