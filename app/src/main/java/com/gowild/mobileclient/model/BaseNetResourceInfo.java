/**
 * Copyright (C)  2016 深圳市狗尾草智能科技有限公司
 * GowildXB
 * BaseNetResourceInfo.java
 */
package com.gowild.mobileclient.model;

import com.alibaba.fastjson.annotation.JSONField;

/**
 * @author temp
 * @version 1.0
 *          <p><strong>Features draft description.主要功能介绍</strong></p>
 * @since 2016/7/27 14:37
 */
public class BaseNetResourceInfo {

    // ===========================================================
    // HostConstants
    // ===========================================================

    private static final String TAG = BaseNetResourceInfo.class.getSimpleName();

    //音乐时长
    @JSONField(name = "duration")
    public int duration;
    //音乐专属id
    @JSONField(name = "musicId")
    public int musicId;
    //音乐标题
    @JSONField(name = "title")
    public String title;
    //音乐专属文件地址
    @JSONField(name = "filePath")
    public String filePath;
    //故事专辑专用id
    @JSONField(name = "albumId")
    public int albumId;
    //故事专辑专用专辑名
    @JSONField(name = "albumName")
    public String albumName;
    //音乐故事通用作者名
    @JSONField(name = "announcerName")
    public String announcerName;
    //音乐故事通用属性
    @JSONField(name = "collect")
    public boolean collect;

    @Override
    public String toString() {
        return "BaseNetResourceInfo{" +
                "duration=" + duration +
                ", musicId=" + musicId +
                ", title='" + title + '\'' +
                ", filePath='" + filePath + '\'' +
                ", albumId=" + albumId +
                ", albumName='" + albumName + '\'' +
                ", announcerName='" + announcerName + '\'' +
                ", collect=" + collect +
                '}';
    }
}
