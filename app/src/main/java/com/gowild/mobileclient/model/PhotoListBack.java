/**
 * Copyright (C)  2016 深圳市狗尾草智能科技有限公司
 * GowildXB
 * PhotoListBack.java
 */
package com.gowild.mobileclient.model;

import com.gowild.mobileclient.utils.Utils;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * @author temp
 * @since 2016/8/4 11:28
 * @version 1.0
 * <p><strong>Features draft description.主要功能介绍</strong></p>
 */
public class PhotoListBack {

    // ===========================================================
    // HostConstants
    // ===========================================================

    private static final String TAG = PhotoListBack.class.getSimpleName();

    /**
     * pageNum : 1
     * pageSize : 10
     * size : 10
     * orderBy : null
     * startRow : 0
     * endRow : 9
     * total : 10
     * pages : 1
     * list : [{"photoUrl":"http://resource.gowild.cn/2016053118482755800790.jpg","createTime":"2016-07-26 14:02:59"},{"photoUrl":"http://resource.gowild.cn/2016053118482755800790.jpg","createTime":"2016-07-26 13:56:44"},{"photoUrl":"http://resource.gowild.cn/2016053118482755800790.jpg","createTime":"2016-07-26 13:55:15"},{"photoUrl":"http://resource.gowild.cn/2016053118482755800790.jpg","createTime":"2016-07-26 13:54:16"},{"photoUrl":"http://resource.gowild.cn/2016053118482755800790.jpg","createTime":"2016-07-26 13:53:15"},{"photoUrl":"http://resource.gowild.cn/2016053118482755800790.jpg","createTime":"2016-07-26 13:51:55"},{"photoUrl":"http://resource.gowild.cn/2016053118482755800790.jpg","createTime":"2016-07-26 13:48:49"},{"photoUrl":"http://resource.gowild.cn/2016053118482755800790.jpg","createTime":"2016-07-26 13:46:19"},{"photoUrl":"http://resource.gowild.cn/2016053118482755800790.jpg","createTime":"2016-07-26 13:40:09"},{"photoUrl":"http://resource.gowild.cn/2016053118482755800790.jpg","createTime":"2016-07-26 13:39:32"}]
     * firstPage : 1
     * prePage : 0
     * nextPage : 0
     * lastPage : 1
     * isFirstPage : true
     * isLastPage : true
     * hasPreviousPage : false
     * hasNextPage : false
     * navigatePages : 8
     * navigatepageNums : [1]
     */

    public int pageNum;
    public int     pageSize;
    public int     size;
    public Object  orderBy;
    public int     startRow;
    public int     endRow;
    public int     total;
    public int     pages;
    public int     firstPage;
    public int     prePage;
    public int     nextPage;
    public int     lastPage;
    public boolean isFirstPage;
    public boolean isLastPage;
    public boolean hasPreviousPage;
    public boolean hasNextPage;
    public int     navigatePages;

    public List<ListBean> list;
    public List<Integer> navigatepageNums;

    public static class ListBean {
        public int id;
        public String photoUrl;
        public String createTime;
    }

    public ArrayList<PhotoContentInfo> parcelInfo() {
        ArrayList<PhotoContentInfo> infos = new ArrayList<>();
        for (ListBean bean : list) {
            infos.add(new PhotoContentInfo(bean));
        }
        return infos;
    }
}
