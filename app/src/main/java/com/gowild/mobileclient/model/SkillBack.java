/**
 * Copyright (C)  2016 深圳市狗尾草智能科技有限公司
 * GowildXB
 * SkillBack.java
 */
package com.gowild.mobileclient.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * @author temp
 * @since 2016/7/27 15:15
 * @version 1.0
 * <p><strong>Features draft description.主要功能介绍</strong></p>
 */
public class SkillBack {

    // ===========================================================
    // HostConstants
    // ===========================================================
    private static final String TAG = SkillBack.class.getSimpleName();

    public ArrayList<SkillItem> newSkillList;
    public ArrayList<SkillItem> lockedSkillList;
    public ArrayList<SkillItem> unLockedSkillList;

    public static class SkillItem implements Parcelable {
        public static final int STATUS_LOCK = 1;
        public static final int STATUS_UNLOCK = 2;

        public static final int STATUS_NEW = 1;

        public int id;
        public String skillName;
        public int skillStatus;
        public String skillIcon;
        public String skillIconDetail;
        public int costCoin;
        public String skillDesc;
        public String conditions;
        public int skillOrder;
        public int typeId;
        public int isNew;

        public SkillItem() {

        }

        protected SkillItem(Parcel in) {
            id = in.readInt();
            skillName = in.readString();
            skillStatus = in.readInt();
            skillIcon = in.readString();
            skillIcon = in.readString();
            costCoin = in.readInt();
            skillDesc = in.readString();
            conditions = in.readString();
            skillOrder = in.readInt();
            typeId = in.readInt();
            isNew = in.readInt();
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(id);
            dest.writeString(skillName);
            dest.writeInt(skillStatus);
            dest.writeString(skillIcon);
            dest.writeString(skillIconDetail);
            dest.writeInt(costCoin);
            dest.writeString(skillDesc);
            dest.writeString(conditions);
            dest.writeInt(skillOrder);
            dest.writeInt(typeId);
            dest.writeInt(isNew);
        }

        @Override
        public int describeContents() {
            return 0;
        }

        public static final Creator<SkillItem> CREATOR = new Creator<SkillItem>() {
            @Override
            public SkillItem createFromParcel(Parcel in) {
                return new SkillItem(in);
            }

            @Override
            public SkillItem[] newArray(int size) {
                return new SkillItem[size];
            }
        };

        /**
         *
         * @return true 未解锁
         */
        public boolean isLock() {
            return skillStatus == STATUS_LOCK;
        }

        /**
         *
         * @return true 新技能
         */
        public boolean isNew() {
            return isNew == STATUS_NEW;
        }
    }
}
