/**
 * Copyright (C)  2016 深圳市狗尾草智能科技有限公司
 * GowildXB
 * ApplianceModel.java
 */
package com.gowild.mobileclient.model;

import java.io.Serializable;

/**
 * @author Zhangct
 * @since 2016/8/26 20:14
 * @version 1.0
 * <p> 家电信息 </p>
 */
public class ApplianceModel implements Serializable {
    Integer deviceType; // 设备类型
    String brandName; // 品牌名字
    Integer modelID;  // 品牌型号
    Integer sceneID; // 场景ID
    String displayName;//展示的名称

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public Integer getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(Integer deviceType) {
        this.deviceType = deviceType;
    }

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public Integer getModelID() {
        return modelID;
    }

    public void setModelID(Integer modelID) {
        this.modelID = modelID;
    }

    public Integer getSceneID() {
        return sceneID;
    }

    public void setSceneID(Integer sceneID) {
        this.sceneID = sceneID;
    }
}
