package com.gowild.mobileclient.model;

/**
 * Created by 王揆 on 2016/7/19.
 */
public class TitleInfo {
    public String mName;
    //对应分类list的开始index（=）
    public int mStartContentPos;
    //对应分类list的结束index（=）
    public int mEndContentPos;
    public boolean mCbChecked = false;
    public TitleInfo(String name) {
        mName = name;
    }

    @Override
    public String toString() {
        return "TitleInfo{" +
                "mName='" + mName + '\'' +
                ", mStartContentPos=" + mStartContentPos +
                ", mEndContentPos=" + mEndContentPos +
                ", mCbChecked=" + mCbChecked +
                '}';
    }
}
