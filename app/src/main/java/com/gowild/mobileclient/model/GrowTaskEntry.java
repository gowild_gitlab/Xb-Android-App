/**
 * Copyright (C)  2016 深圳市狗尾草智能科技有限公司
 * GowildXB
 * GrowTaskEntry.java
 */
package com.gowild.mobileclient.model;

/**
 * @author shuai. wang
 * @version 1.0
 *          <p><strong>Features draft description.主要功能介绍</strong></p>
 * @since 2016/8/12 18:41
 */
public class GrowTaskEntry {

	// ===========================================================
	// HostConstants
	// ===========================================================

	private static final String TAG = GrowTaskEntry.class.getSimpleName();
	public int id;
	public String achieveContent;
	public String experience;
	public String coinCnt;
	public String achieveMedal;
	public String type;
	public String surplus;
	public String countAll;
	public String number;
	public String accountNumber;
	public String pictureUrl;
	public String typeName;
	// ===========================================================
	// Static Fields
	// ===========================================================

	// ===========================================================
	// Fields
	// ===========================================================


	// ===========================================================
	// Constructors
	// ===========================================================


	// ===========================================================
	// Getter or Setter
	// ===========================================================


	// ===========================================================
	// Methods for/from SuperClass/Interfaces
	// ===========================================================


	// ===========================================================
	// Methods
	// ===========================================================


	// ===========================================================
	// Inner and Anonymous Classes
	// ===========================================================

}
