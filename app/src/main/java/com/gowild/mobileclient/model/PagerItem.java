package com.gowild.mobileclient.model;

import android.support.v4.app.Fragment;

/**
 *
 */
public class PagerItem {

    private final String mTitle;

    private Fragment mFragment;

    public PagerItem(String title, Fragment fragment) {
        this.mTitle = title;
        this.mFragment = fragment;
    }

    public Fragment getmFragment() {
        return mFragment;
    }

    public CharSequence getTitle() {
        return mTitle;
    }
}
