/**
 * Copyright (C)  2016 深圳市狗尾草智能科技有限公司
 * Sliding
 * HonorWealthModel.java
 */
package com.gowild.mobileclient.model;

/**
 * @author Administrator
 * @since 2016/7/25 9:33
 * @version 1.0
 * <p><strong>Features draft description.主要功能介绍</strong></p>
 */
public class HonorWealthModel {

    private String id;

    private String mIndex;

    private String mName;

    private String mMoneyTotal;

    public HonorWealthModel(){}

    public HonorWealthModel(String mIndex, String mName, String mMoneyTotal) {
        this.mIndex = mIndex;
        this.mName = mName;
        this.mMoneyTotal = mMoneyTotal;
    }

    public String getName() {
        return mName;
    }

    public void setName(String mName) {
        this.mName = mName;
    }

    public String getMoneyTotal() {
        return mMoneyTotal;
    }

    public void setMoneyTotal(String mMoneyTotal) {
        this.mMoneyTotal = mMoneyTotal;
    }

    public String getIndex() {
        return mIndex;
    }

    public void setIndex(String mIndex) {
        this.mIndex = mIndex;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }
}
