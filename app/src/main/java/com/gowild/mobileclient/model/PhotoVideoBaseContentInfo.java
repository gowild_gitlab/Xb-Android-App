/**
 * Copyright (C)  2016 深圳市狗尾草智能科技有限公司
 * GowildXB
 * PhotoVideoBaseContentInfo.java
 */
package com.gowild.mobileclient.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * @author temp
 * @since 2016/8/10 21:40
 * @version 1.0
 * <p><strong>Features draft description.主要功能介绍</strong></p>
 */
public class PhotoVideoBaseContentInfo implements Parcelable {
    private static final String TAG = PhotoVideoBaseContentInfo.class.getSimpleName();
    public String mFileOriginPath;
    public String mLocalPath;
    public String mDate;
    public boolean bChecked;
    public long   mTimeStamp;

    public int mId = -1;

    public PhotoVideoBaseContentInfo() {
    }

    public boolean isLocalRes() {
        return mFileOriginPath == null || !mFileOriginPath.startsWith("http");
    }

    protected PhotoVideoBaseContentInfo(Parcel in) {
        mFileOriginPath = in.readString();
        mDate = in.readString();
        bChecked = in.readByte() != 0;
        mId = in.readInt();
        mLocalPath = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mFileOriginPath);
        dest.writeString(mDate);
        dest.writeByte((byte) (bChecked ? 1 : 0));
        dest.writeInt(mId);
        dest.writeString(mLocalPath);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<PhotoVideoBaseContentInfo> CREATOR = new Creator<PhotoVideoBaseContentInfo>() {
        @Override
        public PhotoVideoBaseContentInfo createFromParcel(Parcel in) {
            return new PhotoVideoBaseContentInfo(in);
        }

        @Override
        public PhotoVideoBaseContentInfo[] newArray(int size) {
            return new PhotoVideoBaseContentInfo[size];
        }
    };

    @Override
    public String toString() {
        return "PhotoVideoBaseContentInfo{" +
                "mFileOriginPath='" + mFileOriginPath + '\'' +
                ", mLocalPath='" + mLocalPath + '\'' +
                ", mDate='" + mDate + '\'' +
                ", bChecked=" + bChecked +
                ", mTimeStamp=" + mTimeStamp +
                ", mId=" + mId +
                '}';
    }
}
