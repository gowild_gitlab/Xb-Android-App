package com.gowild.mobileclient.model;

import com.gowild.mobileclient.config.KeysContainer;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Administrator on 2016/4/5.
 */
public class AccountModel {
    private static final String TAG = "account";
    private String mUsername = null;
    private String mPassword = null;
    private String mNickName = null;
    private String mEmail = null;
    private int mSex = 0;
    private String mBirthday = null;
//    private String mPhone = null;
    private String mAddress = null;
    private String mHobby = null;

    public AccountModel() {

    }

    public AccountModel(JSONObject data) {
        try {
            if (data.has(KeysContainer.KEY_USERNAME)) {
                mUsername = data.getString(KeysContainer.KEY_USERNAME);
            }
            if (data.has(KeysContainer.KEY_PASSWORD)) {
                mPassword = data.getString(KeysContainer.KEY_PASSWORD);
            }
            if (data.has(KeysContainer.KEY_NICKNAME)) {
                mNickName = data.getString(KeysContainer.KEY_NICKNAME);
            }
            if (data.has(KeysContainer.KEY_EMAIL)) {
                mEmail = data.getString(KeysContainer.KEY_EMAIL);
            }
            if (data.has(KeysContainer.KEY_SEX)) {
                mSex = data.getInt(KeysContainer.KEY_SEX);
            }
//            if (data.has(KeysContainer.KEY_PHONE)) {
//                mPhone = data.getString(KeysContainer.KEY_PHONE);
//            }
            if (data.has(KeysContainer.KEY_ADDRESS)) {
                mAddress = data.getString(KeysContainer.KEY_ADDRESS);
            }
            if (data.has(KeysContainer.KEY_HOBBY)) {
                mHobby = data.getString(KeysContainer.KEY_HOBBY);
            }
            if (data.has(KeysContainer.KEY_BIRTHDAY)) {
                mBirthday = data.getString(KeysContainer.KEY_BIRTHDAY);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public JSONObject toJSON() {
        JSONObject data = new JSONObject();
        try {
            data.put(KeysContainer.KEY_PASSWORD, mPassword);
            data.put(KeysContainer.KEY_USERNAME, mUsername);
            data.put(KeysContainer.KEY_NICKNAME, mNickName);
            data.put(KeysContainer.KEY_EMAIL, mEmail);
            data.put(KeysContainer.KEY_SEX, mSex);
//            data.put(KeysContainer.KEY_PHONE, mPhone);
            data.put(KeysContainer.KEY_ADDRESS, mAddress);
            data.put(KeysContainer.KEY_HOBBY, mHobby);
            data.put(KeysContainer.KEY_BIRTHDAY, mBirthday);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return data;
    }



    public String getUsername() {
        return mUsername;
    }

    public String getPassword() {
        return mPassword;
    }

    public String getNickName() {
        return mNickName;
    }

    public String getEmail() {
        return mEmail;
    }

    public int getSex() {
        return mSex;
    }

    public String getBirthday() {
        return mBirthday;
    }

//    public String getPhone() {
//        return mPhone;
//    }

    public String getAddress() {
        return mAddress;
    }

    public String getHobby() {
        return mHobby;
    }


    public void setUsername(String username) {
        mUsername = username;
    }

    public void setPassword(String password) {
        mPassword = password;
    }

    public void setNickName(String nickName) {
        mNickName = nickName;
    }

    public void setEmail(String email) {
        mEmail = email;
    }

    public void setSex(int sex) {
        mSex = sex;
    }

    public void setBirthday(String birthday) {
        mBirthday = birthday;
    }

//    public void setPhone(String phone) {
//        mPhone = phone;
//    }

    public void setAddress(String address) {
        mAddress = address;
    }

    public void setHobby(String hobby) {
        mHobby = hobby;
    }

}
