/**
 * Copyright (C)  2016 深圳市狗尾草智能科技有限公司
 * GowildXB
 * MusicListBack.java
 */
package com.gowild.mobileclient.model;

import java.util.ArrayList;

/**
 * @author temp
 * @since 2016/8/5 16:46
 * @version 1.0
 * <p><strong>Features draft description.主要功能介绍</strong></p>
 */
public class MusicListBack {

    // ===========================================================
    // HostConstants
    // ===========================================================

    private static final String TAG = MusicListBack.class.getSimpleName();


    /**
     * pageNum : 1
     * pageSize : 10
     * size : 10
     * orderBy : null
     * startRow : 1
     * endRow : 10
     * total : 92
     * pages : 10
     */

    public int pageNum;
    public int    pageSize;
    public int    size;
    public Object orderBy;
    public int    startRow;
    public int    endRow;
    public int    total;
    public int    pages;
    public ArrayList<MusicItemInfoBack> list;

}
