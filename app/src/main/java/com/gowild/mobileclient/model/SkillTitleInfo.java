package com.gowild.mobileclient.model;

/**
 * Created by 王揆 on 2016/7/19.
 */
public class SkillTitleInfo {
    public String mName;
    public int mCount;
    public boolean bIsNew;

    public SkillTitleInfo(String name) {
        mName = name;
        bIsNew = false;
    }

    public SkillTitleInfo(String name, boolean isNew) {
        mName = name;
        bIsNew = isNew;
    }

    public void setCount(int count) {
        mCount = count;
    }
}
