package com.gowild.mobileclient.model;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.format.DateFormat;
import android.text.format.DateUtils;

import com.gowild.mobile.libcommon.utils.ScreenUtils;
import com.gowild.mobileclient.application.GowildApplication;
import com.gowild.mobileclient.config.IntContants;
import com.gowild.mobileclient.utils.Utils;
import com.gowild.mobileclient.utils.VideoInfoUtil;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.io.File;
import java.util.Date;

/**
 * Created by 王揆 on 2016/7/19.
 */
public class PhotoContentInfo extends PhotoVideoBaseContentInfo {
    private static final String IMAGEVIEW2 = "?imageView2";
    private static final String TYPE_0 = "/0";
    private static final String TYPE_1 = "/1";
    private static final String TYPE_2 = "/2";
    private static final String TYPE_3 = "/3";
    public String mTime;
    public String mThumbName;
    public String mPreviewName;

    public PhotoContentInfo() {
        super();
    }

    public PhotoContentInfo(PhotoListBack.ListBean bean) {
        super();
        mId = bean.id;
        mFileOriginPath = bean.photoUrl;
        initThumbAndPreview();
        mTime = bean.createTime;
        try {
            Date date = Utils.sFormatTime.parse(mTime);
            mTimeStamp = date.getTime();
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (mTime != null) {
            mDate = mTime.split(" ")[0];
        }
    }

    public void initThumbAndPreview() {
        if (isLocalRes()) {
            mThumbName = mPreviewName = Uri.fromFile(new File(mFileOriginPath)).toString();
        } else {
            setLocalPath();
            mThumbName = mFileOriginPath;
            mPreviewName = mFileOriginPath;
//            mThumbName = new StringBuilder(mFileOriginPath).append(IMAGEVIEW2)
//                    .append(TYPE_1).append("/w/" + IntContants.PHOTO_THUMB_WIDTH).
//                            append("/h/" + IntContants.PHOTO_THUMB_WIDTH).toString();
//            mPreviewName = new StringBuilder(mFileOriginPath).append(IMAGEVIEW2)
//                    .append(TYPE_2).append("/w/" + ScreenUtils.getRealScreenWidth()).toString();
        }
    }

    private void setLocalPath() {
        String[] strings = mFileOriginPath.split("/");
        String fileName  = strings[strings.length - 1];
        mLocalPath = VideoInfoUtil.getPhotoSavePath(GowildApplication.context) + fileName;
    }

    public PhotoContentInfo(VideoInfoUtil.PictureInfo info) {
        mFileOriginPath = info.mFileOriginPath;
        mDate = info.mDate;
        //mTime = "" + info.timeStamp;
        initThumbAndPreview();
    }

    protected PhotoContentInfo(Parcel in) {
        super(in);
        mThumbName = in.readString();
        mPreviewName = in.readString();
        mTime = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeString(mThumbName);
        dest.writeString(mPreviewName);
        dest.writeString(mTime);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<PhotoContentInfo> CREATOR = new Creator<PhotoContentInfo>() {
        @Override
        public PhotoContentInfo createFromParcel(Parcel in) {
            return new PhotoContentInfo(in);
        }

        @Override
        public PhotoContentInfo[] newArray(int size) {
            return new PhotoContentInfo[size];
        }
    };
}
