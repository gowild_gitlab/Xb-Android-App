/**
 * Copyright (C)  2016 深圳市狗尾草智能科技有限公司
 * GowildXB
 * TaskHonor.java
 */
package com.gowild.mobileclient.model;

import java.util.List;

/**
 * @author shuai. wang
 * @since 2016/8/11 20:30
 * @version 1.0
 * <p><strong>Features draft description.主要功能介绍</strong></p>
 */
public class TaskHonor {

	// ===========================================================
	// HostConstants
	// ===========================================================

	private static final String TAG = TaskHonor.class.getSimpleName();
	/**
	 * pageNum : 1
	 * pageSize : 10
	 * size : 10
	 * startRow : 1
	 * endRow : 10
	 * total : 13
	 * pages : 2
	 * list : [{"id":1,"achieveContent":"累积播放歌曲300分钟","experience":30,"coinCnt":0,"achieveMedal":"不同凡响","type":"music","surplus":4,"countAll":4,"number":300,"accountNumber":0,"pictureUrl":"http://resource.gowild.cn/2016053118482755800790.jpg","typeName":"音乐"},{"id":5,"achieveContent":"累积播放故事500分钟","experience":33,"coinCnt":0,"achieveMedal":"谈笑风声","type":"story","surplus":4,"countAll":4,"number":500,"accountNumber":0,"pictureUrl":"http://resource.gowild.cn/2016053118482755800790.jpg","typeName":"故事"},{"id":9,"achieveContent":"累积播放新闻10天","experience":10,"coinCnt":2,"achieveMedal":"博学多才","type":"news","surplus":4,"countAll":4,"number":10,"accountNumber":0,"pictureUrl":"http://resource.gowild.cn/2016053118482755800790.jpg","typeName":"新闻"},{"id":13,"achieveContent":"累积完成闹钟进程10个","experience":10,"coinCnt":2,"achieveMedal":"诚信守时","type":"alarm","surplus":4,"countAll":4,"number":10,"accountNumber":0,"pictureUrl":"http://resource.gowild.cn/2016053118482755800790.jpg","typeName":"闹钟"},{"id":17,"achieveContent":"累积完成提醒进程10个","experience":10,"coinCnt":2,"achieveMedal":"小试牛刀","type":"remind","surplus":4,"countAll":4,"number":10,"accountNumber":0,"pictureUrl":"http://resource.gowild.cn/2016053118482755800790.jpg","typeName":"提醒"},{"id":21,"achieveContent":"累积播报天气预报10个","experience":10,"coinCnt":2,"achieveMedal":"初出茅庐","type":"weather","surplus":4,"countAll":4,"number":10,"accountNumber":0,"pictureUrl":"http://resource.gowild.cn/2016053118482755800790.jpg","typeName":"天气"},{"id":25,"achieveContent":"累积拍摄照片50张","experience":50,"coinCnt":10,"achieveMedal":"欢乐一刻","type":"photo","surplus":4,"countAll":4,"number":50,"accountNumber":0,"pictureUrl":"http://resource.gowild.cn/2016053118482755800790.jpg","typeName":"拍照"},{"id":29,"achieveContent":"累积监控时间10小时","experience":10,"coinCnt":2,"achieveMedal":"风吹草动","type":"monitor","surplus":4,"countAll":4,"number":10,"accountNumber":0,"pictureUrl":"http://resource.gowild.cn/2016053118482755800790.jpg","typeName":"看护"},{"surplus":0,"countAll":0,"accountNumber":0},{"id":33,"achieveContent":"己方发送配对消息条数1条","experience":5,"coinCnt":1,"achieveMedal":"执子之手","type":"pair","surplus":4,"countAll":4,"number":1,"accountNumber":0,"pictureUrl":"http://resource.gowild.cn/2016053118482755800790.jpg","typeName":"配对"}]
	 * firstPage : 1
	 * prePage : 0
	 * nextPage : 2
	 * lastPage : 2
	 * isFirstPage : true
	 * isLastPage : false
	 * hasPreviousPage : false
	 * hasNextPage : true
	 * navigatePages : 2
	 * navigatepageNums : [1,2]
	 */

	public int pageNum;
	public int pageSize;
	public int size;
	public int startRow;
	public int endRow;
	public int total;
	public int pages;
	public int firstPage;
	public int prePage;
	public int nextPage;
	public int lastPage;
	public boolean isFirstPage;
	public boolean isLastPage;
	public boolean hasPreviousPage;
	public boolean hasNextPage;
	public int navigatePages;
	/**
	 * id : 1
	 * achieveContent : 累积播放歌曲300分钟
	 * experience : 30
	 * coinCnt : 0
	 * achieveMedal : 不同凡响
	 * type : music
	 * surplus : 4
	 * countAll : 4
	 * number : 300
	 * accountNumber : 0
	 * pictureUrl : http://resource.gowild.cn/2016053118482755800790.jpg
	 * typeName : 音乐
	 */

	public List<GrowTaskEntry> list;





	// ===========================================================
	// Static Fields
	// ===========================================================

	// ===========================================================
	// Fields
	// ===========================================================


	// ===========================================================
	// Constructors
	// ===========================================================


	// ===========================================================
	// Getter or Setter
	// ===========================================================


	// ===========================================================
	// Methods for/from SuperClass/Interfaces
	// ===========================================================


	// ===========================================================
	// Methods
	// ===========================================================


	// ===========================================================
	// Inner and Anonymous Classes
	// ===========================================================

}
