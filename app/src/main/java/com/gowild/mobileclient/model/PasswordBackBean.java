/**
 * Copyright (C)  2016 深圳市狗尾草智能科技有限公司
 * GowildXB
 * PasswordBackBean.java
 */
package com.gowild.mobileclient.model;

/**
 * @author shuai. wang
 * @since 2016/8/2 15:08
 * @version 1.0
 * <p><strong>Features draft description.主要功能介绍</strong></p>
 */
public class PasswordBackBean {

	// ===========================================================
	// HostConstants
	// ===========================================================

	private static final String TAG = PasswordBackBean.class.getSimpleName();


	/**
	 * access_token : 3ae02c15-70de-4904-9d00-4c39e2880b67
	 * token_type : bearer
	 * refresh_token : 98b94b9b-4f1d-4ccb-a8fc-a3baaefd6b93
	 * expires_in : 43199
	 */

	public String access_token;
	public String token_type;
	public String refresh_token;
	public int expires_in;
}
