package com.gowild.mobileclient.model;

/**
 * 家电设备信息
 */
public class ApplianceItem {
    public int mApplianceTag;
    public String mApplianceName;
    public int mApplianceDrawable;

    public int mSceneTypeId;

    public ApplianceItem(){

    }

    public ApplianceItem(int tag, int mApplianceDrawable) {
        this.mApplianceTag = tag;
        this.mApplianceDrawable = mApplianceDrawable;
    }

    public int getApplianceTag() {
        return mApplianceTag;
    }

    public void setApplianceTag(int mApplianceTag) {
        this.mApplianceTag = mApplianceTag;
    }

    public String getApplianceName() {
        return mApplianceName;
    }

    public void setApplianceName(String applianceName) {
        this.mApplianceName = applianceName;
    }

    public int getApplianceDrawable() {
        return mApplianceDrawable;
    }

    public void setApplianceDrawable(int mApplianceDrawable) {
        this.mApplianceDrawable = mApplianceDrawable;
    }

    public int getSceneTypeId() {
        return mSceneTypeId;
    }

    public void setSceneTypeId(int mSceneTypeId) {
        this.mSceneTypeId = mSceneTypeId;
    }
}
