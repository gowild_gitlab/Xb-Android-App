/**
 * Copyright (C)  2016 深圳市狗尾草智能科技有限公司
 * GowildXB
 * HomeBaseAdapter.java
 */
package com.gowild.mobileclient.base.baseadapter;

import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.BaseAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * @author shuai. wang
 * @since 2016/7/19 15:19
 * @version 1.0
 * <p><strong>Features draft description.adapter的基类,可能会增加更多的功能,先放在一边</strong></p>
 */
public abstract class HomeBaseAdapter<T> extends BaseAdapter implements AdapterView.OnItemClickListener {

	// ===========================================================
	// HostConstants
	// ===========================================================

	// ===========================================================
	// Static Fields
	// ===========================================================

	// ===========================================================
	// Fields
	// ===========================================================
	//new 的原因是避免空指针
	List<T> mlist = new ArrayList<>();
	private AbsListView mAbsListView;
	// ===========================================================
	// Constructors
	// ===========================================================

	public HomeBaseAdapter(AbsListView absListView, List datas) {
		mAbsListView = absListView;
		mlist = datas;
		mAbsListView.setOnItemClickListener(this);
	}
	// ===========================================================
	// Getter or Setter
	// ===========================================================


	// ===========================================================
	// Methods for/from SuperClass/Interfaces
	// ===========================================================

	@Override
	public long getItemId(int position) {
		return 0;
	}

	public void setDate(List<T> list){
		mlist = list;
		notifyDataSetChanged();
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder = null;
		if (convertView == null) {
			holder = getViewHolder();
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		holder.setDataAndRefreshHolderView(mlist.get(position));
		return holder.mHolderView;
	}

	/**
	 * 返回viewholder 这将决定子类长什么样子
	 * @return
	 */
	protected abstract ViewHolder getViewHolder();

	@Override
	public int getCount() {
		return mlist.size();
	}

	@Override
	public T getItem(int position) {
		return mlist.get(position);
	}

	/**
	 * 子类重写该方法,实现点击事件
	 */
	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

	}
	// ===========================================================
	// Inner and Anonymous Classes
	// ===========================================================



}
