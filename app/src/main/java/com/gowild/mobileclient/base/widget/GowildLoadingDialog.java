package com.gowild.mobileclient.base.widget;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import com.gowild.mobileclient.R;


/**
 * Created by Administrator on 2016/6/6.
 */
public class GowildLoadingDialog extends Dialog {

    private static final String TAG = GowildLoadingDialog.class.getSimpleName();

    private View mProgressView;
    private Animation mLoadingAnimation;
    private TextView mLoadingTv;
    private OnBackPressedListener mBackPressedListener;

    public GowildLoadingDialog(Context context) {
        this(context, R.style.LoadingDialogStyle);
//        init(context);
    }

    public GowildLoadingDialog(Context context, int theme) {
        super(context, theme);
        init(context);
    }

    private void startLoadingAnimation() {
        this.mLoadingAnimation.reset();
        this.mProgressView.startAnimation(mLoadingAnimation);
    }

    private void stopLoadingAnimation() {
        this.mLoadingAnimation.cancel();
        this.mProgressView.setAnimation(null);
    }

    @Override
    protected void onStart() {
        super.onStart();
        startLoadingAnimation();
    }

    @Override
    protected void onStop() {
        super.onStop();
        stopLoadingAnimation();
    }

    @Override
    public void onBackPressed() {
        if (mBackPressedListener != null) {
            mBackPressedListener.onDialogBackPressed();
        }
        super.onBackPressed();
    }

    private void init(Context context) {
        getWindow().setContentView(R.layout.gowild_loading_dialog);
        setCancelable(false);
        this.mProgressView = findViewById(R.id.gowild_loading_view);
        this.mLoadingAnimation = AnimationUtils.loadAnimation(getContext(), R.anim.gowild_loading_animation);
        this.mLoadingTv = (TextView) findViewById(R.id.gowild_loading_tv);
    }

    public void show(String loadingMessage) {
        if (this.mLoadingTv != null) {
            this.mLoadingTv.setText(loadingMessage);
        }
        show();
    }

    public void show(int resID) {
        CharSequence message = getContext().getText(resID);
        if (message != null) {
            show(message.toString());
        } else {
            show();
        }
    }

    public void setOnBackPressedListener(OnBackPressedListener listener) {
        this.mBackPressedListener = listener;
    }

    public interface OnBackPressedListener {
        void onDialogBackPressed();
    }
}
