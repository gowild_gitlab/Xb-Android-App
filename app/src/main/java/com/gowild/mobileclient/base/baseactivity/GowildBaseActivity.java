package com.gowild.mobileclient.base.baseactivity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;

import com.gowild.gwais.libstatstics.ReporterFacade;
import com.gowild.mobile.libcommon.utils.ApiUtils;
import com.gowild.mobile.libcommon.utils.DimenUtil;
import com.gowild.mobile.libcommon.utils.Logger;
import com.gowild.mobileclient.R;
import com.gowild.mobileclient.base.widget.GowildLoadingDialog;
import com.gowild.mobileclient.manager.LoginMananger;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

/**
 * @author Ji.Li
 *         修改描述 取消了网络请求的描述,增加了butterknife的注入
 */
@SuppressWarnings("unused")
public abstract class GowildBaseActivity extends FragmentActivity {

    private static final String TAG = GowildBaseActivity.class.getSimpleName();
    private LinearLayout mRootView;
    private View mTopbarView;
    private boolean mTranslucentNavigation = false;
    private boolean mTranslucentStatus = true;

    private int mTopbarPaddingTop;
    private int mFullTopbarHeight;

    private   boolean             mHasBlurBg        = false;
    protected GowildLoadingDialog mLoadingDialog    = null;
    protected boolean             bActivityFinished = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState != null){
            Logger.d(TAG,"savedInstanceState != null");
        }
        mLoadingDialog = new GowildLoadingDialog(GowildBaseActivity.this);
        mLoadingDialog.setOnBackPressedListener(new GowildLoadingDialog.OnBackPressedListener() {
            @Override
            public void onDialogBackPressed() {
                onBackPressed();
            }
        });
//        mLoadingDialog.setCanceledOnTouchOutside(false);
        initDecorView();
        bActivityFinished = false;
	    EventBus.getDefault().register(this);
        //// TODO: 2016/10/28 进行网络统计的代码
//        countPager();
    }

    private void countPager() {
        LoginMananger.getInstence().startCountPage(this,null);
    }


    @Subscribe(threadMode  = ThreadMode.MAIN)
	public void subEvent(String msg) {
		finish();
	}

    @Override
    protected void onDestroy() {
        if (this.mRootView != null) {
            this.mRootView.removeAllViews();
        }
        if (null != mLoadingDialog) {
            mLoadingDialog.dismiss();
        }
//        HttpUtils.getInstance().cancelRequest(this, true);
        bActivityFinished = true;
        super.onDestroy();
	    EventBus.getDefault().unregister(this);

    }

    @Override
    protected void onPause() {
        ReporterFacade.onPauseActivity(this);//友盟统计
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        ReporterFacade.onResumeActivity(this);
        if (!setLoadingDialogFocusable()) {
            mLoadingDialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE, WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE);
        }else{
            mLoadingDialog.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);


    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
    }

    protected boolean setLoadingDialogFocusable(){
        return true;
    }

    protected boolean hasBlurBgLayout() {
        return mHasBlurBg;
    }

    private void initDecorView() {
        boolean translucentStatus = false;
        if (ApiUtils.hasKitkat()) {//api检测
            Window wnd = getWindow();
            if (this.mTranslucentNavigation) {
                wnd.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION, WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
            }
            if (this.mTranslucentStatus) {
                wnd.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
                translucentStatus = true;
            }
        } else {
            translucentStatus = false;
        }
        if (hasBlurBgLayout()) {
            setContentView(R.layout.gowild_base2_activity);
        } else {
            setContentView(R.layout.gowild_base_activity);
        }
        LinearLayout parent = (LinearLayout) findViewById(R.id.gowild_base_layout);
        initTopbarView(parent, translucentStatus);
        initContentView(parent);
        this.mRootView = parent;
    }

    private void initTopbarView(LinearLayout layout, boolean translucentStatus) {
        int topBarViewID = onSetTopbarView();
        if (topBarViewID != 0) {
            LayoutInflater inflater = getLayoutInflater();
            View view = inflater.inflate(topBarViewID, layout, false);
            int height = onSetTopBarHeight();
            if (translucentStatus) {
                int statusHeight = DimenUtil.getStatusBarHeight(this);
                height += statusHeight;
                int left = view.getPaddingLeft();
                int right = view.getPaddingRight();
                int top = view.getPaddingTop();
                int bottom = view.getPaddingBottom();
                this.mTopbarPaddingTop += statusHeight;
                top += statusHeight;
                view.setPadding(left, top, right, bottom);
            }
            this.mFullTopbarHeight = height;
            ViewGroup.LayoutParams params = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                    height);
            this.mTopbarView = view;
            layout.addView(view, params);
        }
    }

    private void initContentView(LinearLayout parent) {
        int contentViewID = onSetContentView();
        if (contentViewID != 0) {
            ViewGroup.LayoutParams params = new ViewGroup.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT);
            LayoutInflater inflater = getLayoutInflater();
            View view = inflater.inflate(contentViewID, parent, true);
            //parent.addView(view);
        }
    }

    public void showTopbarView() {
        if (this.mTopbarView != null) {
            this.mTopbarView.setVisibility(View.VISIBLE);
        }
    }

    public void goneTopbarView() {
        if (this.mTopbarView != null) {
            this.mTopbarView.setVisibility(View.GONE);
        }
    }

    /**
     * 显示加载框
     *
     * @param resId
     */
    public void showLoading(int resId) {
        String str = getStringById(resId);
        showLoading(str);
    }

    /**
     * 显示加载框
     *
     * @param loadStr
     */
    public void showLoading(String loadStr) {
	    if (mLoadingDialog == null) {
		    mLoadingDialog = new GowildLoadingDialog(this);
	    }
        if (!mLoadingDialog.isShowing()) {
            mLoadingDialog.show(loadStr);
        }
    }

    /**
     * 隐藏加载框
     */
    public void hideLoading() {
        if (null != mLoadingDialog) {
            if (mLoadingDialog.isShowing()) {
//                mLoadingDialog.hide();
            mLoadingDialog.dismiss();
            }
        }
    }

    /**
     * 根据资源ID获得文本
     *
     * @param resId
     * @return
     */
    public String getStringById(int resId) {
        return getResources().getString(resId);
    }


    public LinearLayout getRootView() {
        return this.mRootView;
    }

    protected int getFullTopbarHeight() {
        return mFullTopbarHeight;
    }

    protected int getTopbarPaddingTop() {
        return mTopbarPaddingTop;
    }

    /**
     * 子类必须重写此方法
     *
     * @return toolbar的高度
     */
    protected abstract int onSetTopbarView();

    /**
     * 子类重写该方法
     *
     * @return 内容
     */
    protected abstract int onSetContentView();


    protected abstract int onSetTopBarHeight();

    protected boolean isActivityFinished() {
        return bActivityFinished;
    }
}
