/**
 * Copyright (C)  2016 深圳市狗尾草智能科技有限公司
 * GowildXB
 * ViewHolder.java
 */
package com.gowild.mobileclient.base.baseadapter;

import android.view.View;

/**
 * @author shuai. wang
 * @since 2016/7/23 18:10
 * @version 1.0
 * <p><strong>Features draft description.ViewHolder的基类</strong></p>
 */
public abstract class ViewHolder<T> {

	//泛型控制bean的数据类型
	public View mHolderView;
	public T mData;

	//创建ViewHolder
	public ViewHolder() {
		mHolderView = initHolderView();
		//初始化ViewHolder持有的对象,到时候子类自行实现
		mHolderView.setTag(this);
	}

	/**
	 * @des 数据和视图的绑定操作
	 * @called 刷新数据的时候操作
	 */
	public void setDataAndRefreshHolderView(T data) {
		//保存数据到成员变量
		mData = data;
		refreshHolderView(mData);
	}

	/**
	 * @return
	 * @des 决定视图长什么样子,子类去实现,视图与数据的绑定
	 * @des 外界已经得到了数据,绑定数据的时候调用
	 *
	 */
	public abstract View initHolderView();

	/**
	 * @param data 数据集
	 * @des 数据和视图的绑定操作,绑定数据的时候调用
	 */
	public abstract void refreshHolderView(T data);
}
