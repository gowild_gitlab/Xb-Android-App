package com.gowild.mobileclient.adapter;

import android.content.Intent;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.gowild.mobile.libimage.ImageUtil;
import com.gowild.mobileclient.R;
import com.gowild.mobileclient.activity.PhotoDetailActivity;
import com.gowild.mobileclient.config.Constants;
import com.gowild.mobileclient.model.PhotoContentInfo;

import java.util.ArrayList;

/**
 * Created by 王揆 on 2016/7/20.
 */
public class PhotoListAdapter extends RecyclerView.Adapter<PhotoListAdapter.PhotoItemViewHolder> {

    private ArrayList<PhotoContentInfo> mInfos = new ArrayList<>();
    private ArrayList<PhotoContentInfo> mAllInfos;
    private boolean bSelectMode;

    public PhotoListAdapter setInfos(ArrayList<PhotoContentInfo> infos, ArrayList<PhotoContentInfo> allInfos) {
        mInfos.clear();
        mInfos.addAll(infos);
        //不需要创建多份，增加引用即可
        mAllInfos = allInfos;
        return this;
    }
    @Override
    public PhotoItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).
                inflate(R.layout.view_gowild_photo_item, parent, false);
        return new PhotoItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(PhotoItemViewHolder holder, int position) {
        holder.setInfo(mInfos.get(position), mAllInfos);
    }

    @Override
    public int getItemCount() {
        return mInfos.size();
    }

    protected class PhotoItemViewHolder extends RecyclerView.ViewHolder {

        protected View      mCbSelect;
        protected ImageView mThumb;
        public PhotoItemViewHolder(View itemView) {
            super(itemView);
            mCbSelect = itemView.findViewById(R.id.iv_photo_selected);
            mThumb = (ImageView) itemView.findViewById(R.id.iv_thumb);
        }

        public void setInfo(final PhotoContentInfo info, final ArrayList<PhotoContentInfo> infos) {
            mCbSelect.setVisibility(info.bChecked ? View.VISIBLE : View.GONE);
            ImageUtil.getInstance().displayImage(info.mThumbName, mThumb);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!bSelectMode) {
                        Intent intent = new Intent(itemView.getContext(), PhotoDetailActivity.class);
                        intent.putParcelableArrayListExtra(Constants.INFOS, infos);
                        intent.putExtra(Constants.POS, infos.indexOf(info));
                        itemView.getContext().startActivity(intent);
                    } else {
                        info.bChecked = !info.bChecked;
                        mCbSelect.setVisibility(info.bChecked ? View.VISIBLE : View.GONE);
                    }
                }
            });
        }
    }

    /**
     * 切换选中模式和正常模式
     * @param selectMode true 选择照片模式 false 正常显示模式
     */
    public void setSelectMode(boolean selectMode) {
        bSelectMode = selectMode;
    }
}
