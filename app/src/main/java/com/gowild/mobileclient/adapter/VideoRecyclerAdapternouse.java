package com.gowild.mobileclient.adapter;

import android.content.Context;
import android.graphics.Rect;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.gowild.mobileclient.R;
import com.gowild.mobileclient.model.PhotoContentInfo;
import com.gowild.mobileclient.model.TitleInfo;
import com.gowild.mobileclient.utils.VideoInfoUtil;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by 王揆 on 2016/7/19.
 */
public class VideoRecyclerAdapternouse extends RecyclerView.Adapter {

    private static final int GRID_ROW = 4;

    private static final int TYPE_TILTE = 0;
    private static final int TYPE_CONTENT = 1;

    private ArrayList<TitleInfo>                          mTitleInfos      = new ArrayList<>();
    private ArrayList<ArrayList<VideoInfoUtil.VideoInfo>> mContentInfos    = new ArrayList<>();
    private LayoutInflater mInflater;

    private boolean bSelectMode;

    public VideoRecyclerAdapternouse(Context context) {
        mInflater = LayoutInflater.from(context);
    }

    public void setInfos(ArrayList<TitleInfo> titleInfos, ArrayList<ArrayList<VideoInfoUtil.VideoInfo>> contentInfos) {
        mTitleInfos.clear();
        mTitleInfos.addAll(titleInfos);
        mContentInfos.clear();
        mContentInfos.addAll(contentInfos);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == TYPE_TILTE) {
            return new TitleViewHolder(mInflater.inflate(R.layout.view_gowild_photo_title, parent, false));
        } else {
            return new ContentViewHolder(mInflater.inflate(R.layout.view_gowild_recycleview, parent, false));

        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof TitleViewHolder) {
            ((TitleViewHolder) holder).setInfo(position / 2);
            return;
        }
        if (holder instanceof ContentViewHolder) {
            ((ContentViewHolder) holder).setInfo(mContentInfos.get(position / 2));
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (position % 2 == 0) {
            return TYPE_TILTE;
        }
        return TYPE_CONTENT;
    }

    @Override
    public int getItemCount() {
        int count = mTitleInfos.size();
        return count * 2;
    }

    protected class TitleViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.tv_date)
        protected TextView mTvDate;
        @Bind(R.id.cb_select_all)
        protected CheckBox mCbSelect;
        private int mPos;

        public TitleViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            mCbSelect.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (bSelectMode) {
                        ArrayList<VideoInfoUtil.VideoInfo> infos = mContentInfos.get(mPos);
                        for (VideoInfoUtil.VideoInfo info : infos) {
                            info.bChecked = isChecked;
                        }
                        notifyDataSetChanged();
                    }
            }});
        }

        public void setInfo(int pos) {
            mPos = pos;
            if (bSelectMode && mCbSelect.getVisibility() != View.VISIBLE) {
                mCbSelect.setVisibility(View.VISIBLE);
                return;
            }

            if (!bSelectMode && mCbSelect.getVisibility() != View.GONE) {
                mCbSelect.setChecked(false);
                mCbSelect.setVisibility(View.GONE);
                return;
            }
        }
    }

    private class ContentViewHolder extends RecyclerView.ViewHolder {
        public ContentViewHolder(View itemView) {
            super(itemView);
            initRecyclerview((RecyclerView) itemView);
        }

        private void initRecyclerview( RecyclerView recyclerView) {
            recyclerView.setHasFixedSize(false);
            recyclerView.setLayoutManager(new GridLayoutManager(recyclerView.getContext(), GRID_ROW));
            int padding = recyclerView.getResources().getDimensionPixelSize(R.dimen.gowild_photo_item__hor_padding);
            int paddingEdge = recyclerView.getResources().getDimensionPixelSize(R.dimen.gowild_photo_list_padding_edge);
            itemView.setPadding(paddingEdge, 0, paddingEdge, 0);
            ((RecyclerView) itemView).addItemDecoration(new SpaceItemDecoration(padding));
        }

        private class SpaceItemDecoration extends RecyclerView.ItemDecoration {
            private int space;

            public SpaceItemDecoration(int space) {
                this.space = space;
            }

            @Override
            public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
                int position = parent.getChildAdapterPosition(view);
                if(position / GRID_ROW != 0) {
                    outRect.top = space;
                }

                if (position % GRID_ROW != 0) {
                    outRect.left = space;
                }
            }
        }

        public void setInfo(ArrayList<VideoInfoUtil.VideoInfo> infos) {
            if (itemView instanceof RecyclerView) {
                //VideoRecyclerAdapter adapter = new VideoRecyclerAdapter().setInfos(infos);
//                adapter.setSelectMode(bSelectMode);
//                ((RecyclerView) itemView).setAdapter(adapter);
//                adapter.notifyDataSetChanged();
            }
        }
    }

    /**
     * 切换选中模式和正常模式
     * @param selectMode true 选择照片模式 false 正常显示模式
     */
    public void setSelectMode(boolean selectMode) {
        bSelectMode = selectMode;
        if (!bSelectMode) {
            for (ArrayList<VideoInfoUtil.VideoInfo> infos : mContentInfos) {
                for (VideoInfoUtil.VideoInfo info : infos) {
                    info.bChecked = false;
                }
            }
            notifyDataSetChanged();
        }
    }
}
