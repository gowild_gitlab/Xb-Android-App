/**
 * Copyright (C)  2016 深圳市狗尾草智能科技有限公司
 * GowildXB
 * ApplianceListAdapter.java
 */
package com.gowild.mobileclient.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.gowild.mobileclient.R;
import com.gowild.mobileclient.model.ApplianceModel;

import java.util.List;

/**
 * @author Administrator
 * @since 2016/8/5 15:12
 * @version 1.0
 * <p><strong>Features draft description.主要功能介绍</strong></p>
 */
public class ApplianceListAdapter extends BaseAdapter {

    private static final String TAG = ApplianceListAdapter.class.getSimpleName();

    private List<ApplianceModel> mApplianceItems;
    private Context mContext;

    public ApplianceListAdapter(Context context,List<ApplianceModel> applianceItems){
        this.mContext =context;
        this.mApplianceItems = applianceItems;
    }

    @Override
    public int getCount() {
        return mApplianceItems.size();
    }

    @Override
    public Object getItem(int position) {
        return mApplianceItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        if(null == convertView){
            convertView = LayoutInflater.from(mContext).inflate(
                    R.layout.view_gowild_appliance_list_item, null);
            holder = new ViewHolder();
            holder.tv_appliance = (TextView) convertView.findViewById(R.id.tv_appliance);
            holder.iv_appliance = (ImageView) convertView.findViewById(R.id.iv_appliance);
            convertView.setTag(holder);
        }else{
            holder = (ViewHolder) convertView.getTag();
        }
        ApplianceModel data = mApplianceItems.get(position);
        holder.tv_appliance.setText(data.getDisplayName());
        return convertView;
    }

    class ViewHolder{
        TextView tv_appliance;
        ImageView iv_appliance;
    }

}
