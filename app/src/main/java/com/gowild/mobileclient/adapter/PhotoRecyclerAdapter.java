package com.gowild.mobileclient.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.widget.ImageView;

import com.gowild.mobile.libimage.ImageUtil;
import com.gowild.mobileclient.activity.PhotoDetailActivity;
import com.gowild.mobileclient.config.Constants;
import com.gowild.mobileclient.model.PhotoContentInfo;
import com.gowild.mobileclient.model.PhotoVideoBaseContentInfo;

/**
 * Created by 王揆 on 2016/7/19.
 */
public class PhotoRecyclerAdapter extends PhotoVideoBaseAdapter {
    private Fragment mFragment;
    public PhotoRecyclerAdapter(Context context, Fragment fragment) {
        super(context);
        mFragment = fragment;
    }

    @Override
    public void setThumb(PhotoVideoBaseContentInfo info, ImageView thumbView) {
        if (info instanceof PhotoContentInfo) {
            ImageUtil.getInstance().displayImage(((PhotoContentInfo)info).mThumbName, thumbView);
        }
    }

    @Override
    public void onItemClick(PhotoVideoBaseContentInfo info) {
        Intent intent = new Intent(mContext, PhotoDetailActivity.class);
        intent.putParcelableArrayListExtra(Constants.INFOS, mAllContentInfos);
        intent.putExtra(Constants.POS, mAllContentInfos.indexOf(info));
        mFragment.startActivityForResult(intent, 0);
    }
}
