/**
 * Copyright (C)  2016 深圳市狗尾草智能科技有限公司
 * Sliding
 * HonorWealthAdapter.java
 */
package com.gowild.mobileclient.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.gowild.mobileclient.R;
import com.gowild.mobileclient.model.HonorWealthModel;

import java.util.List;

/**
 * @author Administrator
 * @version 1.0
 *          <p><strong>Features draft description.主要功能介绍</strong></p>
 * @since 2016/7/25 9:35
 */
public class HonorWealthAdapter extends BaseAdapter {

    List<HonorWealthModel> mItems;
    Context mContext;

    public HonorWealthAdapter(Context context, List<HonorWealthModel> items) {
        this.mContext = context;
        this.mItems = items;
    }

    @Override
    public int getCount() {
        return mItems.size();
    }

    @Override
    public Object getItem(int position) {
        return mItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        final HonorWealthModel data = mItems.get(position);
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = LayoutInflater.from(mContext).inflate(
                    R.layout.gowild_honor_wealth_item, null);
            holder.mIndex = (TextView) convertView.findViewById(R.id.honor_wealth_index_tv);
            holder.mName = (TextView) convertView.findViewById(R.id.honor_wealth_name_tv);
            holder.mMoneyTotal = (TextView) convertView.findViewById(R.id.honor_wealth_money_total_tv);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.mIndex.setText(data.getIndex());
        holder.mName.setText(data.getName());
        holder.mMoneyTotal.setText(data.getMoneyTotal());
        String currentIndex = data.getIndex();
        if (currentIndex.equals("1") || currentIndex.equals("2") || currentIndex.equals("3")) {
            holder.mIndex.setTextAppearance(mContext, R.style.gowild_honor_wealth_ranking_bold);
        } else {
            holder.mIndex.setTextAppearance(mContext, R.style.gowild_honor_wealth_ranking_normal);
        }
        return convertView;
    }

    private class ViewHolder {
        public TextView mIndex;
        public TextView mName;
        public TextView mMoneyTotal;
    }

}
