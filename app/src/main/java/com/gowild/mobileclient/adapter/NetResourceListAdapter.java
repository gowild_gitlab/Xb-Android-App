/**
 * Copyright (C)  2016 深圳市狗尾草智能科技有限公司
 * GowildXB
 * NetResourceListAdapter.java
 */
package com.gowild.mobileclient.adapter;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.gowild.mobileclient.R;
import com.gowild.mobileclient.model.BaseNetResourceInfo;
import com.gowild.mobileclient.widget.NetResourceItemView;

import java.util.ArrayList;

/**
 * @author 王揆
 * @since 2016/7/27 14:42
 * @version 1.0
 * <p><strong>Features draft description.主要功能介绍</strong></p>
 */
public class NetResourceListAdapter extends RecyclerView.Adapter<NetResourceListAdapter.ItemViewHolder> {

    // ===========================================================
    // HostConstants
    // ===========================================================

    private ArrayList<BaseNetResourceInfo> mInfos;

    public void setInfos(ArrayList infos) {
        mInfos = infos;
    }

    private static final String TAG = NetResourceListAdapter.class.getSimpleName();

    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).
                inflate(R.layout.view_gowild_net_resource_item, parent, false);
        return new ItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ItemViewHolder holder, int position) {
        if (mInfos != null && position < mInfos.size()) {
            holder.setInfo(mInfos.get(position));
        }
    }

    @Override
    public int getItemCount() {
        if (mInfos != null) {
            return mInfos.size();
        }
        return 0;
    }

    protected class ItemViewHolder extends RecyclerView.ViewHolder {

        public NetResourceItemView mItemView;

        public ItemViewHolder(View itemView) {
            super(itemView);
            mItemView = (NetResourceItemView) itemView;
        }

        public void setInfo(BaseNetResourceInfo info) {
            mItemView.setInfo(info);
        }
    }
}
