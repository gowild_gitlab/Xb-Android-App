/**
 * Copyright (C)  2016 深圳市狗尾草智能科技有限公司
 * Sliding
 * HonorAchievementAdapter.java
 */
package com.gowild.mobileclient.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.gowild.mobile.libcommon.utils.DimenUtil;
import com.gowild.mobile.libimage.ImageUtil;
import com.gowild.mobileclient.R;
import com.gowild.mobileclient.model.HonorAchievementModel;
import com.gowild.mobileclient.widget.CircleProgressBar;

import java.util.List;

/**
 * @author Administrator
 * @version 1.0
 *          <p><strong>Features draft description.主要功能介绍</strong></p>
 * @since 2016/7/23 18:35
 */
public class HonorAchievementAdapter extends BaseAdapter {

    List<HonorAchievementModel> mItems;
    Context mContext;

    public HonorAchievementAdapter(Context context, List<HonorAchievementModel> items) {
        this.mContext = context;
        this.mItems = items;
    }

    @Override
    public int getCount() {
        return mItems.size();
    }

    @Override
    public Object getItem(int position) {
        return mItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        final HonorAchievementModel data = mItems.get(position);
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = LayoutInflater.from(mContext).inflate(
                    R.layout.gowild_honor_achievement_item, null);
            holder.mAchievementCatalogTv = (TextView) convertView.findViewById(R.id.achievement_catalog_tv);
            holder.mAchievementTitleTv = (TextView) convertView.findViewById(R.id.achievement_title_tv);
            holder.mAchievementCoinTv = (TextView) convertView.findViewById(R.id.achievement_coin_tv);
            holder.mAchievementExperienceTv = (TextView) convertView.findViewById(R.id.achievement_experience);
            holder.mAchievementContentTv = (TextView) convertView.findViewById(R.id.achievement_content_tv);
            holder.mAchievementProgressTv = (TextView) convertView.findViewById(R.id.achievement_progress_tv);
            holder.mAchievementProgressCircle = (CircleProgressBar) convertView.findViewById(R.id.achievement_progress_circle);
            holder.mLLyt = (LinearLayout) convertView.findViewById(R.id.achievement_lLyt);
            holder.mIconIv = (ImageView) convertView.findViewById(R.id.achievement_icon_iv);
            holder.mGowildAchievementIv = (ImageView) convertView.findViewById(R.id.gowild_task_cb_item_comp);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        //根据有没有标题栏显示不同的样式
        int section = getSectionForPosition(data.getCatalog());
        if (position == section) {
            holder.mAchievementCatalogTv.setVisibility(View.VISIBLE);
            holder.mAchievementCatalogTv.setText(data.getCatalog());
            DimenUtil.setMarginTop(holder.mLLyt, (int) mContext.getResources().getDimension(R.dimen.gowild_24px));
        } else {
            holder.mAchievementCatalogTv.setVisibility(View.GONE);
            DimenUtil.setMarginTop(holder.mLLyt, (int) mContext.getResources().getDimension(R.dimen.gowild_28px));
        }
        ImageUtil.getInstance().displayImage(data.getIconUrl(), holder.mIconIv);
        holder.mAchievementTitleTv.setText(data.getTitle());
        holder.mAchievementCoinTv.setText(data.getGoldCount());
        holder.mAchievementExperienceTv.setText(data.getExperience());
        holder.mAchievementContentTv.setText(data.getContent());
        float per = data.getProgess();
        if (per >= 100) {
            per = 100;
            holder.mAchievementProgressTv.setVisibility(View.GONE);
            holder.mGowildAchievementIv.setVisibility(View.VISIBLE);
        } else {
            holder.mGowildAchievementIv.setVisibility(View.GONE);
            holder.mAchievementProgressTv.setVisibility(View.VISIBLE);
        }
        holder.mAchievementProgressTv.setText((int) (per) + "%");
        holder.mAchievementProgressCircle.setProgress((int) (per));
        return convertView;
    }

    private class ViewHolder {
        public TextView mAchievementCatalogTv;
        public LinearLayout mLLyt;
        public TextView mAchievementTitleTv;
        public TextView mAchievementContentTv;
        public TextView mAchievementCoinTv;
        public TextView mAchievementExperienceTv;
        public TextView mAchievementProgressTv;
        public CircleProgressBar mAchievementProgressCircle;
        public ImageView mIconIv;
        ImageView mGowildAchievementIv;

    }

    /**
     * 获得每一个在数组中的第一个位置
     *
     * @param catalog
     * @return
     */
    private int getSectionForPosition(String catalog) {
        int result = -1;
        int count = mItems.size();
        for (int i = 0; i < count; i++) {
            HonorAchievementModel temp = mItems.get(i);
            try {
                if (null != catalog && temp.getCatalog().equals(catalog)) {
                    result = i;
                    return result;
                }
            }catch (Exception e){
                Log.e("getSectionForPosition",e.toString());
            }

        }
        return result;
    }


}
