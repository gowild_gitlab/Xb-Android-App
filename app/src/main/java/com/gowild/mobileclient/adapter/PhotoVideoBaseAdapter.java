/**
 * Copyright (C)  2016 深圳市狗尾草智能科技有限公司
 * GowildXB
 * PhotoVideoBaseAdapter.java
 */
package com.gowild.mobileclient.adapter;

import android.content.Context;
import android.os.Handler;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.gowild.mobileclient.R;
import com.gowild.mobileclient.fragment.PhotoAndVideoBaseFrament;
import com.gowild.mobileclient.model.PhotoContentInfo;
import com.gowild.mobileclient.model.PhotoVideoBaseContentInfo;
import com.gowild.mobileclient.model.TitleInfo;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * @author temp
 * @since 2016/8/10 21:53
 * @version 1.0
 * <p><strong>Features draft description.主要功能介绍</strong></p>
 */
public abstract class PhotoVideoBaseAdapter extends RecyclerView.Adapter {

    private static final int TYPE_TILTE = 0;
    private static final int TYPE_CONTENT = 1;

    protected ArrayList<PhotoVideoBaseContentInfo> mAllContentInfos = new ArrayList<>();
    protected ArrayList<Object>           mAllInfos        = new ArrayList<>();
    ArrayList<TitleInfo> mTitleInfos;
    private LayoutInflater mInflater;
    private boolean        bSelectMode;
    protected Context mContext;

    public PhotoVideoBaseAdapter(Context context) {
        mInflater = LayoutInflater.from(context);
        mContext = context;
    }

    public void setInfos(ArrayList<TitleInfo> titleInfos, ArrayList<PhotoVideoBaseContentInfo> contentInfos) {
        mAllContentInfos = contentInfos;
        mTitleInfos = titleInfos;
        mAllInfos.clear();
        for (TitleInfo titleInfo : titleInfos) {
            mAllInfos.add(titleInfo);
            for (int pos = titleInfo.mStartContentPos; pos <= titleInfo.mEndContentPos; pos++) {
                mAllInfos.add(mAllContentInfos.get(pos));
            }
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == TYPE_TILTE) {
            return new TitleViewHolder(mInflater.inflate(R.layout.view_gowild_photo_title, parent, false));
        } else {
            View view = LayoutInflater.from(parent.getContext()).
                    inflate(R.layout.view_gowild_photo_item, parent, false);
            return new PhotoItemViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof TitleViewHolder) {
            ((TitleViewHolder) holder).setInfo((TitleInfo) mAllInfos.get(position), position);
            return;
        }
        if (holder instanceof PhotoItemViewHolder) {
            ((PhotoItemViewHolder) holder).setInfo((PhotoVideoBaseContentInfo) mAllInfos.get(position), position);
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (mAllInfos.get(position) instanceof TitleInfo) {
            return TYPE_TILTE;
        }
        return TYPE_CONTENT;
    }

    @Override
    public int getItemCount() {
        return mAllInfos.size();
    }

    protected class TitleViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.tv_date)
        protected TextView  mTvDate;
        @Bind(R.id.cb_select_all)
        protected CheckBox  mCbSelect;
        private   TitleInfo mTitleInfo;
        private int mPos;

        public TitleViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            mCbSelect.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (bSelectMode) {
                        mTitleInfo.mCbChecked = mCbSelect.isChecked();
                        for (int pos = mTitleInfo.mStartContentPos; pos <= mTitleInfo.mEndContentPos;
                             pos++) {
                            PhotoVideoBaseContentInfo info = mAllContentInfos.get(pos);
                            info.bChecked = mTitleInfo.mCbChecked;
                        }
                        notifyChange();
                    }
                }
            });
        }

        public void setInfo(TitleInfo info, int pos) {
            mTitleInfo = info;
            mPos = pos;
            mTvDate.setText(mTitleInfo.mName);
            if (!bSelectMode && mCbSelect.getVisibility() != View.GONE) {
                mCbSelect.setChecked(false);
                mCbSelect.setVisibility(View.GONE);
                return;
            }

            if (bSelectMode && mCbSelect.getVisibility() != View.VISIBLE) {
                mCbSelect.setVisibility(View.VISIBLE);
            }
            mCbSelect.setChecked(info.mCbChecked);
        }
    }

    private void notifyChange() {
        new Handler().post(new Runnable() {
            @Override
            public void run() {
                notifyDataSetChanged();
            }
        });
    }

    /**
     * 切换选中模式和正常模式
     * @param selectMode true 选择照片模式 false 正常显示模式
     */
    public void setSelectMode(boolean selectMode) {
        bSelectMode = selectMode;
        if (!bSelectMode) {
            for (PhotoVideoBaseContentInfo info : mAllContentInfos) {
                info.bChecked = false;
            }
            if (mTitleInfos != null) {
                for (TitleInfo titleInfo : mTitleInfos) {
                    titleInfo.mCbChecked = false;
                }
            }
        }
    }

    protected class PhotoItemViewHolder extends RecyclerView.ViewHolder {
        protected View      mCbSelect;
        protected ImageView mThumb;
        private int mPos;

        public PhotoItemViewHolder(View itemView) {
            super(itemView);
            mCbSelect = itemView.findViewById(R.id.iv_photo_selected);
            mThumb = (ImageView) itemView.findViewById(R.id.iv_thumb);
        }

        public void setInfo(final PhotoVideoBaseContentInfo info, int position) {
            mCbSelect.setVisibility(info.bChecked ? View.VISIBLE : View.GONE);
            setThumb(info, mThumb);
            mPos = position;
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!bSelectMode) {
                        onItemClick(info);
                    } else {
                        info.bChecked = !info.bChecked;
                        mCbSelect.setVisibility(info.bChecked ? View.VISIBLE : View.GONE);
                        checkIsAllSelect(info);
                    }
                }
            });
        }
    }

    public void checkIsAllSelect(final PhotoVideoBaseContentInfo info) {
        if (mTitleInfos == null) {
            return;
        }
        for (TitleInfo titleInfo : mTitleInfos) {
            if (!TextUtils.isEmpty(titleInfo.mName) && titleInfo.mName.equals(info.mDate)) {
                boolean lastStatus = titleInfo.mCbChecked;
                int i;
                for (i = titleInfo.mStartContentPos; i <= titleInfo.mEndContentPos; i++) {
                    if (!mAllContentInfos.get(i).bChecked) {
                        titleInfo.mCbChecked = false;
                        i = i - 1;
                        break;
                    }
                }

                if (i == titleInfo.mEndContentPos + 1) {
                    titleInfo.mCbChecked = true;
                }
                if (lastStatus != titleInfo.mCbChecked) {
                    notifyChange();
                }
                return;
            }
        }
        //info.mDate
    }

    public abstract void setThumb(final PhotoVideoBaseContentInfo info, ImageView thumbView);
    public abstract void onItemClick(final PhotoVideoBaseContentInfo info);
    public GridLayoutManager.SpanSizeLookup lookup = new GridLayoutManager.SpanSizeLookup() {
        @Override
        public int getSpanSize(int position) {
            if (mAllInfos.get(position) instanceof TitleInfo) {
                return PhotoAndVideoBaseFrament.GRID_ROW;
            } else {
                return 1;
            }
        }
    };

}
