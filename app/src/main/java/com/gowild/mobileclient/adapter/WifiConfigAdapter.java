/**
 * Copyright (C)  2016 深圳市狗尾草智能科技有限公司
 * GowildXB
 * WifiConfigAdapter.java
 */
package com.gowild.mobileclient.adapter;

import android.content.Context;
import android.net.wifi.ScanResult;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.gowild.mobileclient.R;

import java.util.List;

/**
 * @author Zhangct
 * @version 1.0
 *          <p> 主要功能介绍 </p>
 * @since 2016/8/6 16:54
 */
public class WifiConfigAdapter extends BaseAdapter {

    // ===========================================================
    // HostConstants
    // ===========================================================

    private static final String TAG = WifiConfigAdapter.class.getSimpleName();

    @Override
    public int getCount() {
        return mScanResults.size();
    }

    @Override
    public Object getItem(int position) {
        return mScanResults.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        if (null == convertView) {
            holder = new ViewHolder();
            convertView = LayoutInflater.from(mContext).inflate(R.layout.view_gowild_wifi_item, parent, false);
            holder.mSsidTv = (TextView) convertView.findViewById(R.id.gowild_wifi_item_tv);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        ScanResult result = mScanResults.get(position);
        holder.mSsidTv.setText(result.SSID);
        return convertView;
    }

    // ===========================================================
    // Static Fields
    // ===========================================================

    // ===========================================================
    // Fields
    // ===========================================================
    private List<ScanResult> mScanResults;
    private Context mContext;

    // ===========================================================
    // Constructors
    // ===========================================================
    public WifiConfigAdapter(Context context, List<ScanResult> scanResults) {
        this.mScanResults = scanResults;
        this.mContext = context;
    }

    // ===========================================================
    // Getter or Setter
    // ===========================================================


    // ===========================================================
    // Methods for/from SuperClass/Interfaces
    // ===========================================================


    // ===========================================================
    // Methods
    // ===========================================================


    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================
    private class ViewHolder {
        private TextView mSsidTv;
        private String ssid;
    }


}
