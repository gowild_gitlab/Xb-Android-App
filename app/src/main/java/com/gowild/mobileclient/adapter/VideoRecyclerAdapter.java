package com.gowild.mobileclient.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.gowild.mobile.libimage.ImageUtil;
import com.gowild.mobileclient.R;
import com.gowild.mobileclient.activity.PhotoDetailActivity;
import com.gowild.mobileclient.config.Constants;
import com.gowild.mobileclient.model.PhotoContentInfo;
import com.gowild.mobileclient.model.PhotoVideoBaseContentInfo;
import com.gowild.mobileclient.utils.SystemMediaUtil;
import com.gowild.mobileclient.utils.VideoInfoUtil;

import java.io.File;
import java.util.ArrayList;

/**
 * Created by 王揆 on 2016/7/20.
 */
public class VideoRecyclerAdapter extends PhotoVideoBaseAdapter {

    public VideoRecyclerAdapter(Context context) {
        super(context);
    }

    @Override
    public void setThumb(PhotoVideoBaseContentInfo info, ImageView thumbView) {
        ImageUtil.getInstance().displayImage(Uri.fromFile(new File(info.mFileOriginPath)).toString(), thumbView);
    }

    @Override
    public void onItemClick(PhotoVideoBaseContentInfo info) {
        SystemMediaUtil.startVideoPlayerActivity(mContext, info.mFileOriginPath);
    }
}
