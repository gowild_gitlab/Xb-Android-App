package com.gowild.mobileclient.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.gowild.mobileclient.R;
import com.gowild.mobileclient.model.SkillBack;
import com.gowild.mobileclient.widget.SkillItemView;

import java.util.ArrayList;

/**
 * Created by 王揆 on 2016/7/20.
 */
public class SkillListAdapter extends RecyclerView.Adapter<SkillListAdapter.SkillItemViewHolder> {

    private ArrayList<SkillBack.SkillItem> mInfos = new ArrayList<>();

    public SkillListAdapter setInfos(ArrayList<SkillBack.SkillItem> infos) {
        mInfos.clear();
        mInfos.addAll(infos);
        return this;
    }
    @Override
    public SkillItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).
                inflate(R.layout.view_gowild_skill_item, parent, false);

        return new SkillItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(SkillItemViewHolder holder, int position) {
        holder.setInfo(mInfos.get(position));
    }

    @Override
    public int getItemCount() {
        return mInfos.size();
    }

    protected static class SkillItemViewHolder extends RecyclerView.ViewHolder {
        public SkillItemViewHolder(View itemView) {
            super(itemView);
        }

        public void setInfo(SkillBack.SkillItem info) {
            if (itemView instanceof SkillItemView) {
                ((SkillItemView) itemView).setInfo(info);
            }
        }
    }
}
