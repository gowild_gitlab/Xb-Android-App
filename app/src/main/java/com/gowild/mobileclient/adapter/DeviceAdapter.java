/**
 * Copyright (C)  2016 深圳市狗尾草智能科技有限公司
 * GowildXB
 * DeviceAdapter.java
 */
package com.gowild.mobileclient.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.gowild.mobileclient.R;
import com.gowild.mobileclient.model.ApplianceItem;

import java.util.List;

/**
 * @author zhangct
 * @version 1.0
 *          <p><strong>家电设备适配器</strong></p>
 * @since 2016/8/3 15:09
 */
public class DeviceAdapter extends BaseAdapter {

    private static final String TAG = "DateAdapter";
    private Context context;
    private List<ApplianceItem> lstDate;

    public DeviceAdapter(Context mContext, List<ApplianceItem> list) {
        this.context = mContext;
        lstDate = list;
    }

    @Override
    public int getCount() {
        return lstDate.size();
    }

    @Override
    public Object getItem(int position) {
        return lstDate.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder item = null;
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(
                    R.layout.view_gowild_appliance_item, null);
            item = new ViewHolder();
            item.liner = (ImageView) convertView.findViewById(R.id.iv_item);
            convertView.setTag(item);
        } else {
            item = (ViewHolder) convertView.getTag();
        }
        ApplianceItem m_time = lstDate.get(position);
        item.liner.setBackgroundResource(m_time.mApplianceDrawable);
        return convertView;
    }

    class ViewHolder {
        ImageView liner;
        public ViewHolder() {
        }
    }

}
