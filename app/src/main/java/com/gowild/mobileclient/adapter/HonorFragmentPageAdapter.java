/**
 * Copyright (C)  2016 深圳市狗尾草智能科技有限公司
 * Sliding
 * HonorFragmentPageAdapter.java
 */
package com.gowild.mobileclient.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.gowild.mobileclient.model.PagerItem;

import java.util.List;

/**
 * @author Administrator
 * @since 2016/7/23 15:45
 * @version 1.0
 * <p><strong>Features draft description.主要功能介绍</strong></p>
 */
public class HonorFragmentPageAdapter extends FragmentPagerAdapter {

    // ===========================================================
    // HostConstants
    // ===========================================================

    private static final String TAG = HonorFragmentPageAdapter.class.getSimpleName();

    // ===========================================================
    // Static Fields
    // ===========================================================

    // ===========================================================
    // Fields
    // ===========================================================
    private Fragment mContentFragment;
    private List<PagerItem> mPagerItems;

    // ===========================================================
    // Constructors
    // ===========================================================


    // ===========================================================
    // Getter or Setter
    // ===========================================================


    // ===========================================================
    // Methods for/from SuperClass/Interfaces
    // ===========================================================

    public HonorFragmentPageAdapter(FragmentManager fm, List<PagerItem> pagerItems) {
        super(fm);
        mPagerItems = pagerItems;
    }

    @Override
    public int getCount() {
        return mPagerItems.size();
    }

    @Override
    public Fragment getItem(int position) {
        mContentFragment = mPagerItems.get(position).getmFragment();
        return mContentFragment;
    }

    @Override
    public CharSequence getPageTitle(int position) {

        return mPagerItems.get(position).getTitle();
    }

    // ===========================================================
    // Methods
    // ===========================================================


    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================

}
