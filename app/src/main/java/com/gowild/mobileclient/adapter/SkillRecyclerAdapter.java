package com.gowild.mobileclient.adapter;

import android.content.Context;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.gowild.mobileclient.R;
import com.gowild.mobileclient.fragment.PhotoAndVideoBaseFrament;
import com.gowild.mobileclient.model.SkillBack;
import com.gowild.mobileclient.model.SkillTitleInfo;
import com.gowild.mobileclient.model.TitleInfo;
import com.gowild.mobileclient.widget.SkillItemView;

import java.util.ArrayList;

/**
 * Created by 王揆 on 2016/7/19.
 */
public class SkillRecyclerAdapter extends RecyclerView.Adapter {

    public static final int GRID_ROW = 3;

    private static final int TYPE_TILTE = 0;
    private static final int TYPE_CONTENT = 1;


    private ArrayList<SkillTitleInfo>              mTitleInfos   = new ArrayList<>();
    private ArrayList<ArrayList<SkillBack.SkillItem>> mContentInfos = new ArrayList<>();
    private ArrayList<Object> mAllInfos = new ArrayList<>();

    private LayoutInflater mInflater;

    public SkillRecyclerAdapter(Context context) {
        mInflater = LayoutInflater.from(context);
    }

    public void setInfos(ArrayList<SkillTitleInfo> titleInfos, ArrayList<ArrayList<SkillBack.SkillItem>> contentInfos) {
        mTitleInfos.clear();
        mTitleInfos.addAll(titleInfos);
        mContentInfos.clear();
        mContentInfos.addAll(contentInfos);
        mAllInfos.clear();
        for (int i = 0; i < mTitleInfos.size(); i++) {
            ArrayList<SkillBack.SkillItem> infos = mContentInfos.get(i);
            if (infos == null || infos.isEmpty()) {
                continue;
            }
            mAllInfos.add(mTitleInfos.get(i));
            mAllInfos.addAll(infos);
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == TYPE_TILTE) {
            return new TitleViewHolder(mInflater.inflate(R.layout.view_gowild_skill_title, parent, false));
        } else {
            View view = LayoutInflater.from(parent.getContext()).
                    inflate(R.layout.view_gowild_skill_item, parent, false);

            return new SkillItemViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof TitleViewHolder) {
            ((TitleViewHolder) holder).setInfo((SkillTitleInfo) mAllInfos.get(position));
            return;
        }
        if (holder instanceof SkillItemViewHolder) {
            ((SkillItemViewHolder) holder).setInfo((SkillBack.SkillItem)mAllInfos.get(position));
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (position < mAllInfos.size()) {
            if (mAllInfos.get(position) instanceof SkillTitleInfo) {
                return TYPE_TILTE;
            } else {
                return TYPE_CONTENT;
            }
        }
        return super.getItemViewType(position);
    }

    @Override
    public int getItemCount() {
        return mAllInfos.size();
    }

    protected  class TitleViewHolder extends RecyclerView.ViewHolder {
        protected TextView mTvTitle;
        protected View mVNewTag;

        public TitleViewHolder(View itemView) {
            super(itemView);
            mTvTitle = (TextView) itemView.findViewById(R.id.tv_skill_title);
            mVNewTag = itemView.findViewById(R.id.iv_new_tag);
        }

        public void setInfo(SkillTitleInfo info) {
            mTvTitle.setText(String.format(info.mName, info.mCount));
            //mVNewTag.setVisibility(info.bIsNew ? View.VISIBLE : View.GONE);
        }
    }

    protected static class SkillItemViewHolder extends RecyclerView.ViewHolder {
        public SkillItemViewHolder(View itemView) {
            super(itemView);
        }

        public void setInfo(SkillBack.SkillItem info) {
            if (itemView instanceof SkillItemView) {
                ((SkillItemView) itemView).setInfo(info);
            }
        }
    }

    public GridLayoutManager.SpanSizeLookup lookup = new GridLayoutManager.SpanSizeLookup() {
        @Override
        public int getSpanSize(int position) {
            if (mAllInfos.get(position) instanceof SkillTitleInfo) {
                return GRID_ROW;
            } else {
                return 1;
            }
        }
    };

}
