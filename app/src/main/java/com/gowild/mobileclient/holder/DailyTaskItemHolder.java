/**
 * Copyright (C)  2016 深圳市狗尾草智能科技有限公司
 * GowildXB
 * DailyTaskItemHolder.java
 */
package com.gowild.mobileclient.holder;

import android.view.View;
import android.widget.TextView;

import com.gowild.mobileclient.R;
import com.gowild.mobileclient.application.GowildApplication;
import com.gowild.mobileclient.base.baseadapter.ViewHolder;
import com.gowild.mobileclient.vo.DaliyTask;
import com.gowild.mobileclient.widget.CircleProgressBar;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * @author shuai. wang
 * @version 1.0
 *          <p><strong>Features draft description.主要功能介绍</strong></p>
 * @since 2016/7/25 9:40
 */
public class DailyTaskItemHolder extends ViewHolder<DaliyTask> {

	// ===========================================================
	// HostConstants
	// ===========================================================

	private static final String TAG = DailyTaskItemHolder.class.getSimpleName();
	@Bind(R.id.gowild_task_tv_item_task)
	TextView mGowildTaskTvItemTask;
	@Bind(R.id.gowild_task_cb_item_circle)
	CircleProgressBar mGowildTaskCbItemCircle;
	@Bind(R.id.gowild_task_tv_item_task_des)
	TextView mGowildTaskTvItemTaskDes;
	@Bind(R.id.gowild_task_tv_item_god)
	TextView mGowildTaskTvItemGod;
	@Bind(R.id.gowild_task_tv_item_task_exp)
	TextView mGowildTaskTvItemTaskExp;

	@Override
	public View initHolderView() {
		View view = View.inflate(GowildApplication.context, R.layout.fragment_gowild_listview_item_daily, null);
		ButterKnife.bind(this, view);
		return view;
	}

	@Override
	public void refreshHolderView(DaliyTask data) {
//		id
//				Integer
//		任务id
//		1
//		count	Integer	任务总次数	1
//		coinCnt
//				Integer
//		任务金币
//		50
//		experience	Integer	任务经验	10
//		name	String	任务名称	完成1个闹钟进程
//		type	Integer	任务类型	clock
//		accountCount
//		Integer	用户完成数量	0
		mGowildTaskTvItemGod.setText(String.valueOf(data.coinCnt));
		mGowildTaskTvItemTaskExp.setText(String.valueOf(data.experience));
		mGowildTaskTvItemTaskDes.setText(data.name);
		mGowildTaskTvItemTask.setText("任务一");
		mGowildTaskCbItemCircle.setProgress((int) (Float.parseFloat(data.accountCount)/ Float.parseFloat(data.count)));
	}


	// ===========================================================
	// Static Fields
	// ===========================================================

	// ===========================================================
	// Fields
	// ===========================================================


	// ===========================================================
	// Constructors
	// ===========================================================


	// ===========================================================
	// Getter or Setter
	// ===========================================================


	// ===========================================================
	// Methods for/from SuperClass/Interfaces
	// ===========================================================


	// ===========================================================
	// Methods
	// ===========================================================


	// ===========================================================
	// Inner and Anonymous Classes
	// ===========================================================

}
