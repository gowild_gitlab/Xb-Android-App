/**
 * Copyright (C)  2016 深圳市狗尾草智能科技有限公司
 * GowildXB
 * DailyTaskItemHolder.java
 */
package com.gowild.mobileclient.holder;

import android.view.View;

import com.gowild.mobileclient.R;
import com.gowild.mobileclient.application.GowildApplication;
import com.gowild.mobileclient.base.baseadapter.ViewHolder;

import butterknife.ButterKnife;

/**
 * @author shuai. wang
 * @version 1.0
 *          <p><strong>Features draft description.主要功能介绍</strong></p>
 * @since 2016/7/25 9:40
 */
public class GrowTaskItemHolder extends ViewHolder {

	// ===========================================================
	// HostConstants
	// ===========================================================

	private static final String TAG = GrowTaskItemHolder.class.getSimpleName();

	@Override
	public View initHolderView() {
		View view = View.inflate(GowildApplication.context, R.layout.fragment_gowild_listview_item_daily, null);
		ButterKnife.bind(this, view);
		return view;
	}

	/**
	 * 数据和视图的绑定
	 * 因为数据没有,只好先放着
	 * @param data
	 */
	@Override
	public void refreshHolderView(Object data) {

	}


	// ===========================================================
	// Static Fields
	// ===========================================================

	// ===========================================================
	// Fields
	// ===========================================================


	// ===========================================================
	// Constructors
	// ===========================================================


	// ===========================================================
	// Getter or Setter
	// ===========================================================


	// ===========================================================
	// Methods for/from SuperClass/Interfaces
	// ===========================================================


	// ===========================================================
	// Methods
	// ===========================================================


	// ===========================================================
	// Inner and Anonymous Classes
	// ===========================================================

}
