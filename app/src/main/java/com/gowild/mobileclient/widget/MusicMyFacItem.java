/**
 * Copyright (C)  2016 深圳市狗尾草智能科技有限公司
 * GowildXB
 * MusicMyFacItem.java
 */
package com.gowild.mobileclient.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.gowild.mobileclient.R;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * @author temp
 * @since 2016/7/26 18:09
 * @version 1.0
 * <p><strong>Features draft description.主要功能介绍</strong></p>
 */
public class MusicMyFacItem extends RelativeLayout {

    private static final String TAG = MusicMyFacItem.class.getSimpleName();

    @Bind(R.id.tv_title)
    TextView mTvTitle;
    @Bind(R.id.tv_sub_title)
    TextView mTvSubTitle;

    private String mTitle;
    private String mSubTitle;

    public MusicMyFacItem(Context context) {
        this(context, null);
    }

    public MusicMyFacItem(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public MusicMyFacItem(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        TypedArray array = context.obtainStyledAttributes(attrs, R.styleable.MusicMyFacItem);
	    mTitle = array.getString(R.styleable.MusicMyFacItem_title_one);
	    mSubTitle = array.getString(R.styleable.MusicMyFacItem_subtitle_two);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        LayoutInflater.from(getContext()).inflate(R.layout.view_gowild_music_fac_item, this, true);
        ButterKnife.bind(this);
        setTitle(mTitle, mSubTitle);
    }

    public void setTitle(String title, String subTitle) {
        mTvTitle.setText(title);
        mTvSubTitle.setText(subTitle);
    }

    public void setTitle(int titleId, int subTitleId) {
        mTvTitle.setText(titleId);
        mTvSubTitle.setText(subTitleId);
    }
}
