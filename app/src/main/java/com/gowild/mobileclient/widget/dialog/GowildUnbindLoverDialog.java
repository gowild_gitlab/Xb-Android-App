package com.gowild.mobileclient.widget.dialog;

import android.app.DialogFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.gowild.mobileclient.R;
import com.gowild.mobileclient.callback.OnNormalDialogListener;


public class GowildUnbindLoverDialog extends DialogFragment {

    private Button mGowildCancelBtn ;
    private Button mGowildConfirmBtn ;

    /**
     * 点击监听
     */
    private OnNormalDialogListener mDialogListener = null;

    public OnNormalDialogListener getDialogListener() {
        return mDialogListener;
    }

    /**
     * 设置监听
     *
     * @param mDialogListener
     */
    public void setDialogListener(OnNormalDialogListener mDialogListener) {
        this.mDialogListener = mDialogListener;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NO_TITLE, R.style.gowildDialogStyle);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.gowild_dialog_loverunbind, container, false);
        initView(root);
        return root;
    }

    @Override
    public void onStart() {
        super.onStart();
    }


    /**
     * 初始化控件
     *
     * @param view
     */
    private void initView(View view) {
        mGowildCancelBtn = (Button) view.findViewById(R.id.btn_cancel);
        mGowildConfirmBtn = (Button) view.findViewById(R.id.btn_confirm);
        mGowildCancelBtn.setOnClickListener(mOnClickListener);
        mGowildConfirmBtn.setOnClickListener(mOnClickListener);
    }

    /**
     * 点击事件
     */
    private View.OnClickListener mOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.btn_cancel://取消
                    if (null != mDialogListener) {
                        mDialogListener.onCancelListener();
                    }
                    dismiss();
                    break;
                case R.id.btn_confirm://确定
                    if (null != mDialogListener) {
                        mDialogListener.onConfirmListener();
                    }
                    dismiss();
                    break;
            }
        }
    };

}
