/**
 * Copyright (C)  2016 深圳市狗尾草智能科技有限公司
 * MyTool
 * PhotoPreviewView.java
 */
package com.gowild.mobileclient.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;

/**
 * @author 王揆
 * @since 2016/7/21 10:32
 * @version 1.0
 * <p><strong>Features draft description.主要功能介绍</strong></p>
 */
public class PhotoPreviewView extends ImageView {

    // ===========================================================
    // HostConstants
    // ===========================================================

    private static final String TAG = PhotoPreviewView.class.getSimpleName();

    public PhotoPreviewView(Context context) {
        super(context);
    }

    public PhotoPreviewView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public PhotoPreviewView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }
}
