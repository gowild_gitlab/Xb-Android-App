/**
 * Copyright (C)  2016 深圳市狗尾草智能科技有限公司
 * MyTool
 * MyToolBar.java
 */
package com.gowild.mobileclient.widget;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.app.FragmentActivity;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.gowild.mobileclient.R;
import com.gowild.mobileclient.callback.OnToolBarListener;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * @author 王揆
 * @version 1.0
 *          <p><strong>Features draft description.主要功能介绍</strong></p>
 * @since 2016/7/23 14:13
 */
public class MyToolBar extends LinearLayout implements View.OnClickListener {

    @Bind(R.id.iv_back)
    protected View mVBack;
    @Bind(R.id.tv_toolbar_title)
    protected TextView mTvTitle;
    @Bind(R.id.tv_action)
    protected TextView mTvAction;
    private OnToolBarListener mListener;
    private static final String TAG = MyToolBar.class.getSimpleName();

    public MyToolBar(Context context) {
        super(context);
    }

    public MyToolBar(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public MyToolBar(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    /**
     * 设置右侧点击监听
     *
     * @param listener
     */
    public void setActionListener(OnToolBarListener listener) {
        this.mListener = listener;
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        ButterKnife.bind(this);
        mVBack.setOnClickListener(this);
        mTvAction.setOnClickListener(this);
    }

    public void setBackVisible(int visible) {
        mVBack.setVisibility(visible);
    }

    public void setActionVisible(int visible) {
        mTvAction.setVisibility(visible);
    }

    public void setTitle(String title) {
        mTvTitle.setText(title);
    }

    public void setTitle(int titleId) {
        mTvTitle.setText(titleId);
    }

    public void setActionStr(String str) {
        mTvAction.setText(str);
    }

    public void setActionStr(int strId) {
        mTvAction.setText(strId);
    }

    public void setActionDrawable(Drawable drawable) {
        mTvAction.setCompoundDrawablesWithIntrinsicBounds(drawable, null, null, null);
    }

    public void setActionDrawable(int drawableId) {
        mTvAction.setCompoundDrawablesWithIntrinsicBounds(0, 0, drawableId, 0);
    }

    /**
     * 设置back按钮的监听事件,back的id为R.id.iv_back
     *
     * @param listener
     */
    public void setBackListener(OnClickListener listener) {
        mVBack.setOnClickListener(listener);
    }

    /**
     * 设置右侧功能按钮的监听事件,back的id为R.id.tv_action
     *
     * @param listener
     */
    public void setActionListener(OnClickListener listener) {
        mTvAction.setOnClickListener(listener);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_back:
                Context context = v.getContext();
                if (context instanceof Activity) {
                    ((Activity) context).onBackPressed();
                    return;
                }
                if (context instanceof FragmentActivity) {
                    ((FragmentActivity) context).onBackPressed();
                    return;
                }
                break;
            case R.id.tv_action:
                if (null != mListener) {
                    mListener.onActionClickListener();
                }
                break;
        }
    }
}
