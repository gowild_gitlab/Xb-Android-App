/**
 * Copyright (C)  2016 深圳市狗尾草智能科技有限公司
 * GowildXB
 * PullableRecyclerView.java
 */
package com.gowild.mobileclient.widget;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

/**
 * @author temp
 * @since 2016/8/9 14:54
 * @version 1.0
 * <p><strong>Features draft description.主要功能介绍</strong></p>
 */
public class PullableRecyclerView extends RecyclerView implements Pullable {

    // ===========================================================
    // HostConstants
    // ===========================================================

    private static final String TAG = PullableRecyclerView.class.getSimpleName();

    public PullableRecyclerView(Context context) {
        super(context);
    }

    public PullableRecyclerView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public PullableRecyclerView(Context context, @Nullable AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    public boolean canPullDown() {
        LayoutManager manager = getLayoutManager();
        if (getAdapter() == null || manager == null) {
            return false;
        }
        if (getAdapter().getItemCount() == 0) {
            return true;
        }
        int firstPosition = 0;
        int lastVisiblePos;
        if (manager instanceof LinearLayoutManager) {
            if (firstPosition ==  ((LinearLayoutManager) manager).findFirstCompletelyVisibleItemPosition()) {
                return true;
            }
        } else if (manager instanceof GridLayoutManager) {
            if (firstPosition ==  ((GridLayoutManager) manager).findFirstCompletelyVisibleItemPosition()) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean canPullUp() {
        return isLastView();
    }

    public boolean isLastView() {
        LayoutManager manager = getLayoutManager();
        if (getAdapter() == null || manager == null) {
            return false;
        }
        int count = getAdapter().getItemCount();
        if (count == 0) {
            return true;
        }
        int lastPosition = count - 1;

        int lastVisiblePos;

        if (manager instanceof LinearLayoutManager) {
            lastVisiblePos = ((LinearLayoutManager) manager).findLastCompletelyVisibleItemPosition();
            if (lastPosition == lastVisiblePos) {
                return true;
            }
        } else if (manager instanceof GridLayoutManager) {
            lastVisiblePos = ((GridLayoutManager) manager).findLastCompletelyVisibleItemPosition();
            if (lastPosition == lastVisiblePos) {
                return true;
            }
        }
        return false;
    }

    @Override
    public void onScrollStateChanged(int state) {
        super.onScrollStateChanged(state);
    }
}
