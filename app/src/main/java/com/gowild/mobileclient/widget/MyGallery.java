/**
 * Copyright (C)  2016 深圳市狗尾草智能科技有限公司
 * MyTool
 * MyGallery.java
 */
package com.gowild.mobileclient.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.Gallery;

/**
 * @author 王揆
 * @since 2016/7/23 17:09
 * @version 1.0
 * <p><strong>Features draft description.主要功能介绍</strong></p>
 */
public class MyGallery extends Gallery {

    // ===========================================================
    // HostConstants
    // ===========================================================

    private static final String TAG = MyGallery.class.getSimpleName();

    private boolean bGalleryScrolling = false;

    public MyGallery(Context context) {
        super(context);
    }

    public MyGallery(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public MyGallery(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onScrollChanged(int l, int t, int oldl, int oldt) {
        super.onScrollChanged(l, t, oldl, oldt);
        if (oldl == l) {
            bGalleryScrolling = false;
        } else {
            bGalleryScrolling = true;
        }
    }

    public boolean isScrolling() {
        return bGalleryScrolling;
    }
}
