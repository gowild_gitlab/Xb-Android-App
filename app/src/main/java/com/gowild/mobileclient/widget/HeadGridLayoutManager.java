/**
 * Copyright (C)  2016 深圳市狗尾草智能科技有限公司
 * GowildXB
 * HeadGridLayoutManager.java
 */
package com.gowild.mobileclient.widget;

import android.content.Context;
import android.support.v7.widget.GridLayoutManager;
import android.util.AttributeSet;

/**
 * @author temp
 * @since 2016/8/10 10:46
 * @version 1.0
 * <p><strong>Features draft description.主要功能介绍</strong></p>
 */
public class HeadGridLayoutManager extends GridLayoutManager {

    // ===========================================================
    // HostConstants
    // ===========================================================

    private static final String TAG = HeadGridLayoutManager.class.getSimpleName();

    public HeadGridLayoutManager(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    public HeadGridLayoutManager(Context context, int spanCount) {
        super(context, spanCount);
    }

    public HeadGridLayoutManager(Context context, int spanCount, int orientation, boolean reverseLayout) {
        super(context, spanCount, orientation, reverseLayout);
    }

    @Override
    public void setSpanSizeLookup(SpanSizeLookup spanSizeLookup) {
        super.setSpanSizeLookup(spanSizeLookup);
    }
}
