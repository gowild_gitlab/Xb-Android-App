/**
 * Copyright (C)  2016 深圳市狗尾草智能科技有限公司
 * GowildXB
 * NetResourceLoverView.java
 */
package com.gowild.mobileclient.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;

import com.gowild.mobile.libcommon.utils.ToastUtils;
import com.gowild.mobileclient.R;
import com.gowild.mobileclient.base.baseactivity.GowildBaseActivity;
import com.gowild.mobileclient.config.IntContants;
import com.gowild.mobileclient.config.MediaConstants;
import com.gowild.mobileclient.event.RefreshEvent;
import com.gowild.mobileclient.manager.GowildAsyncHttpResponseHandler;
import com.gowild.mobileclient.manager.GowildHttpManager;
import com.gowild.mobileclient.model.BaseNetResourceInfo;

import org.greenrobot.eventbus.EventBus;

/**
 * @author temp
 * @since 2016/8/11 15:27
 * @version 1.0
 * <p><strong>Features draft description.主要功能介绍</strong></p>
 */
public class NetResourceCollectView extends ImageView {

    // ===========================================================
    // HostConstants
    // ===========================================================

    private GowildBaseActivity mActivity;

    private static final String TAG = NetResourceCollectView.class.getSimpleName();

    private BaseNetResourceInfo mInfo;

    public NetResourceCollectView(Context context) {
        super(context);
    }

    public NetResourceCollectView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public NetResourceCollectView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                changeLoveStatus();
            }
        });
        Context context = getContext();
        if (context instanceof GowildBaseActivity) {
            mActivity = (GowildBaseActivity) context;
        }
    }

    public void setInfo(BaseNetResourceInfo info) {
        if (info == null) {
            ToastUtils.showCustom(getContext(), "没有数据，无法收藏", false);
            return;
        }
        setSelected(info.collect);
        mInfo = null;
        mInfo = info;
    }

    public void changeLoveStatus() {
        if (mInfo == null) {
            return;
        }
        if (mActivity != null) {
            mActivity.showLoading(R.string.loading);
        }
        if (IntContants.isCurrentMusic()) {
            GowildHttpManager.changeMusicCollect(getContext(), mInfo.musicId, !mInfo.collect, mResponseHandler);
        } else {
            GowildHttpManager.changeStoryCollect(getContext(), mInfo.albumId, !mInfo.collect, mResponseHandler);
        }
    }

    public void changeCollectOk() {
        mInfo.collect = !mInfo.collect;
        MediaConstants.mMusicCollect = mInfo.collect ? 1 : 2;
        MediaConstants.mStoryCollect = mInfo.collect ? 1 : 2;
        setSelected(mInfo.collect);
    }

    public GowildAsyncHttpResponseHandler mResponseHandler = new GowildAsyncHttpResponseHandler(getContext()) {
        @Override
        public void onSuccess(int statusCode, Object responseBody) {
            changeCollectOk();
            if (mActivity != null) {
                mActivity.hideLoading();
                EventBus.getDefault().post(new RefreshEvent());
            }
        }

        @Override
        public void onFailure(int statusCode, String responseBody) {
            if (mActivity != null) {
                mActivity.hideLoading();
            }
        }
    };
}
