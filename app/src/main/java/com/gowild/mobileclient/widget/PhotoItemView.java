/**
 * Copyright (C)  2016 深圳市狗尾草智能科技有限公司
 * MyTool
 * PhotoItemView.java
 */
package com.gowild.mobileclient.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.RelativeLayout;

/**
 * @author 王揆
 * @since 2016/7/20 21:06
 * @version 1.0
 * <p><strong>Features draft description.主要功能介绍</strong></p>
 */
public class PhotoItemView extends RelativeLayout{

    private static final String TAG = PhotoItemView.class.getSimpleName();


    public PhotoItemView(Context context) {
        super(context);
    }

    public PhotoItemView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public PhotoItemView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }
}
