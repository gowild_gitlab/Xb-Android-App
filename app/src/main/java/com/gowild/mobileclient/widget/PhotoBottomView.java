/**
 * Copyright (C)  2016 深圳市狗尾草智能科技有限公司
 * MyTool
 * PhotoBottomView.java
 */
package com.gowild.mobileclient.widget;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.gowild.mobile.libcommon.utils.CheckNetworkUtil;
import com.gowild.mobile.libcommon.utils.ToastUtils;
import com.gowild.mobileclient.R;
import com.gowild.mobileclient.application.GowildApplication;
import com.gowild.mobileclient.base.baseactivity.GowildBaseActivity;
import com.gowild.mobileclient.callback.OnNormalDialogListener;
import com.gowild.mobileclient.db.DBUtil;
import com.gowild.mobileclient.manager.GowildAsyncHttpResponseHandler;
import com.gowild.mobileclient.manager.GowildHttpManager;
import com.gowild.mobileclient.model.PhotoVideoBaseContentInfo;
import com.gowild.mobileclient.utils.ImageUtils;
import com.gowild.mobileclient.utils.Utils;
import com.gowild.mobileclient.widget.dialog.GowildNormalDialog;
import com.gowild.socialshare.GowildShare;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.umeng.socialize.UMShareAPI;
import com.umeng.socialize.media.UMImage;
import com.umeng.socialize.utils.BitmapUtils;

import java.io.File;
import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * @author 王揆
 * @since 2016/7/23 16:24
 * @version 1.0
 * <p><strong>Features draft description.主要功能介绍</strong></p>
 */
public class PhotoBottomView extends LinearLayout implements View.OnClickListener {

    private static final String TAG = PhotoBottomView.class.getSimpleName();

    @Bind(R.id.iv_shared)
    protected ImageView mIvShare;
    @Bind(R.id.iv_download)
    protected ImageView mIvDownload;
    @Bind(R.id.iv_delete)
    protected ImageView mIvDelete;
    @Bind(R.id.left_space)
    protected View mLeftSpace;
    @Bind(R.id.right_space)
    protected View mRightSpace;
    private ArrayList<? extends PhotoVideoBaseContentInfo> mInfos;
    private ArrayList<Integer> mChooseItems = new ArrayList<>();
    private int mPos = -1;

    private GowildBaseActivity mActivity;

    private OnPhotoDeleteCallback mCallBack;
    public boolean bLocalRes;

    private Thread mThread;
    private boolean bThreadRun = false;
    private Handler mHandler = new Handler();
    //当前资源是否为照片
    private boolean bPhoto = true;

    public PhotoBottomView(Context context) {
        super(context);
    }

    public PhotoBottomView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public PhotoBottomView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onFinishInflate() {
        ButterKnife.bind(this);
        mIvShare.setOnClickListener(this);
        mIvDownload.setOnClickListener(this);
        mIvDelete.setOnClickListener(this);
        super.onFinishInflate();
        Context context = getContext();
        if (context instanceof GowildBaseActivity) {
            mActivity = (GowildBaseActivity) context;
        }
    }

    public void setIsVideo() {
        bPhoto = false;
    }

    public void hideShare() {
        mIvShare.setVisibility(GONE);
        mLeftSpace.setVisibility(GONE);
    }

    public void hideDownload() {
        mIvDownload.setVisibility(GONE);
        mRightSpace.setVisibility(GONE);
    }

    public void setInfo(ArrayList<? extends PhotoVideoBaseContentInfo> infos) {
        mInfos = infos;
    }

    public void setCurrentPos(int pos) {
        mPos = pos;
    }

    public void onClick(View view) {
        mChooseItems.clear();
        if (mPos != -1) {
            mChooseItems.add(mPos);
        } else {
            for (int i = 0; i< mInfos.size(); i++) {
                if (mInfos.get(i).bChecked) {
                    mChooseItems.add(i);
                }
            }
        }
        if (mChooseItems.isEmpty()) {
            ToastUtils.showCustom(getContext(),
                    bPhoto ? R.string.no_choose_photo : R.string.no_choose_Video, false);
            return;
        }
        bLocalRes = mInfos.get(0).isLocalRes();

        switch (view.getId()) {
            case R.id.iv_shared:
                if (CheckNetworkUtil.isNetworkAvailable(getContext())){
                    share();
                }else{
                    ToastUtils.showCustom(getContext(),"没有网络不能分享",false);
                }
                break;
            case R.id.iv_delete:
                if (mChooseItems.size() > 20) {
                    ToastUtils.showCustom(getContext(),
                            bPhoto ? R.string.delete_photo_more_than_20 : R.string.delete_video_more_than_20,
                            false);
                    return;
                }

                if (bLocalRes){
                    showMachineUpdateDialog();
                }else{
                    if (mActivity != null) {
                        if (CheckNetworkUtil.isNetworkAvailable(mActivity)){
                            showMachineUpdateDialog();
                        }else{
                            ToastUtils.showCustom(mActivity,"没有网络不能删除",false);

                        }
                    }
                }


                break;
            case R.id.iv_download:
                if (bLocalRes) {
                    return;
                }
                if (mChooseItems.size() > 20) {
                    ToastUtils.showCustom(getContext(), R.string.save_photo_more_than_20, false);
                    return;
                }
                if (mThread == null) {
                    if (mActivity != null) {
                        mActivity.showLoading(R.string.loading);
                    }
                    mThread = new Thread(new Runnable() {
                        @Override
                        public void run() {
                            download();
                        }
                    });
                    mThread.start();
                }
                //download();
                break;
            default:
                break;
        }
    }

    public void share() {
        int pos = mChooseItems.get(0);
        if (pos < 0) {
            pos = 0;
        }
        if (mInfos.size() <= pos) {
            return;
        }
        PhotoVideoBaseContentInfo info =  mInfos.get(pos);
        String uri = info.mFileOriginPath;
        UMImage image = new UMImage(getContext(),uri);
        if (info.isLocalRes()) {
            File file = new File(info.mFileOriginPath);
            if (file.exists()) {
                uri = "http://www.gowild.cn/";
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inTempStorage = new byte[5*1024];
                Bitmap bitmap = BitmapFactory.decodeFile(info.mFileOriginPath, options);
                if (bitmap.getByteCount() /1024 > 500){
                    options.inSampleSize=2;//图片高宽度都为原来的二分之一，即图片大小为原来的大小的四分之一
                }
                image = new UMImage(getContext(), BitmapFactory.decodeFile(info.mFileOriginPath, options));

                //image = new UMImage(getContext(), info.mFileOriginPath);
                GowildShare.setShareContent((Activity) getContext(), image, uri);
            }
        }else{
            final String finalUri = uri;
            ImageUtils.getInstance().loadNetImage(uri, new ImageLoadingListener() {

                @Override
                public void onLoadingStarted(String imageUri, View view) {
                }

                @Override
                public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                }

                @Override
                public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                    byte[]  bytes = BitmapUtils.bitmap2Bytes(loadedImage);
                    byte[]  bytes1 = BitmapUtils.compressBitmap(bytes, 512 * 1024);
                    Bitmap  bitmap = BitmapFactory.decodeByteArray(bytes1, 0, bytes1.length);
                    UMImage image = new UMImage(getContext(), bitmap);
                    GowildShare.setShareContent((Activity) getContext(), image, finalUri);
                }

                @Override
                public void onLoadingCancelled(String imageUri, View view) {
                }
            });

        }
//        } else {
//            if (!TextUtils.isEmpty(info.mLocalPath)) {
//                File file = new File(info.mLocalPath);
//                if (file.exists()) {
//                    //参数出错
//                    image = new UMImage(getContext(), BitmapFactory.decodeFile(info.mLocalPath));
//                    //image = new UMImage(getContext(), info.mLocalPath);
//                }
//            }
//        }

    }

    public void delete() {
        if (mActivity != null) {
            mActivity.showLoading(R.string.loading);
        }
        for (Integer pos : mChooseItems) {
            deleteRes(mInfos.get(pos));
        }

        if (!bLocalRes && !mChooseItems.isEmpty()) {
            GowildHttpManager.deletePhotos(getContext(), getString(mChooseItems), new GowildAsyncHttpResponseHandler(getContext()) {
                @Override
                public void onSuccess(int statusCode, Object responseBody) {
                    deleteDone();
                }

                @Override
                public void onFailure(int statusCode, String responseBody) {
                    if (mActivity != null) {
                        mActivity.hideLoading();
                    }
                    ToastUtils.showCustom(getContext(), R.string.gowild_photo_delete_failed, false);
                }
            });
        } else {
            deleteDone();
        }
    }

    public String getString(ArrayList<Integer> chooseItems) {
        StringBuilder buffer = new StringBuilder();
        for (int i = 0; i < chooseItems.size(); i++) {
            buffer.append(mInfos.get(chooseItems.get(i)).mId);
            if (i != chooseItems.size() - 1) {
                buffer.append(",");
            }
        }
        return buffer.toString();
    }

    public void deleteDone() {
        for (int i = mChooseItems.size() - 1; i >= 0; i--) {
            int pos = mChooseItems.get(i).intValue();
            PhotoVideoBaseContentInfo info = mInfos.get(pos);
            if (!info.isLocalRes()) {
                new File(info.mLocalPath).delete();
            }
            mInfos.remove(pos);
        }
        mChooseItems.clear();
        if (mCallBack != null) {
            mCallBack.onDelete();
        }
        if (mActivity != null) {
            mActivity.hideLoading();
        }
        showToast(bPhoto ? R.string.gowild_photo_delete__success : R.string.gowild_video_delete__success);
    }

    public boolean deleteRes(PhotoVideoBaseContentInfo info) {
        if (!bLocalRes){
           DBUtil.delete("id = ? ",new String[]{"info.mId"});
        }

        try {
            if (info.isLocalRes()) {
                new File(info.mFileOriginPath).delete();
                return true;
            } else {
                new File(info.mLocalPath).delete();
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;

    }

    private void download() {
        boolean isAllLocal = true;
        if (mPos != -1) {
            isAllLocal = downloadPhoto(mInfos.get(mPos));
        } else {
            for (Integer pos : mChooseItems) {
                PhotoVideoBaseContentInfo info = mInfos.get(pos);
                if (info.bChecked) {
                    //有至少一张图片需要下载
                    if (!downloadPhoto(info)) {
                        isAllLocal = false;
                    }
                }
            }
        }
        if (isAllLocal) {
            showToast(R.string.gowild_photo_unneed_save);
        }
        mThread = null;
        if (mActivity != null) {
            mActivity.hideLoading();
        }
    }

    private boolean downloadPhoto(final PhotoVideoBaseContentInfo info) {
        File file = new File(info.mLocalPath);
        if (file.exists()) {
            return true;
        }
        Bitmap bitmap = ImageLoader.getInstance().loadImageSync(info.mFileOriginPath);

        if (bitmap == null) {
            showToast(R.string.gowild_photo_save_failed);
//            bitmap = ImageLoader.getInstance().loadImageSync(info.);
            return false;
        }
        try {
            Utils.saveFile(bitmap, info.mLocalPath);
            Intent intent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
            Uri uri = Uri.fromFile(file);
            intent.setData(uri);

            GowildApplication.context.sendBroadcast(intent);
        } catch (Exception e) {
            e.printStackTrace();
            showToast(R.string.gowild_photo_save_failed);

            return false;
        }
        showToast(R.string.gowild_photo_save_success);
        return false;
//        ImageLoader.getInstance().loadImage(info.mFileOriginPath, new ImageLoadingListener() {
//
//            @Override
//            public void onLoadingStarted(String imageUri, View view) {
//
//            }
//
//            @Override
//            public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
//
//            }
//
//            @Override
//            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
//                try {
//                    Utils.saveFile(loadedImage, info.mLocalPath);
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//            }
//
//            @Override
//            public void onLoadingCancelled(String imageUri, View view) {
//
//            }
//        });
    }

    private void showToast(final int id) {
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                ToastUtils.showCustom(getContext(), id, false);
            }
        });
    }

    public void setOnPhotoDeleteCallback(OnPhotoDeleteCallback callBack) {
        mCallBack = callBack;
    }

    public interface OnPhotoDeleteCallback {
        void onDelete();
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        UMShareAPI.get(getContext()).onActivityResult( requestCode, resultCode, data);
    }

    /**
     *
     */
    private void showMachineUpdateDialog() {
        final GowildNormalDialog dialog = new GowildNormalDialog();
        dialog.setDialogConfirm("确定");
        dialog.setDialogCancel("取消");
        dialog.setDialogContent(bPhoto ? "您确定删除照片吗?" : "您确定删除视频吗?");
        dialog.setDialogListener(new OnNormalDialogListener() {
            @Override
            public void onCancelListener() {
                dialog.dismiss();
            }

            @Override
            public void onConfirmListener() {
                delete();
            }
        });
        if (mActivity != null) {
         dialog.show(mActivity.getFragmentManager(),TAG);
        }

    }
}
