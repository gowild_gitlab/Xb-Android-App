package com.gowild.mobileclient.widget.dialog;

import android.app.DialogFragment;
import android.content.Intent;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.gowild.mobileclient.R;

public class GowildRobotDataCleanDialog extends DialogFragment
        implements OnClickListener {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NO_TITLE, R.style.gowildDialogStyle);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.gowild_dialog_setting_robot_clear_data, container, false);
        Button btn = (Button) root.findViewById(R.id.btn_cancel);
        Button telBtn = (Button) root.findViewById(R.id.btn_tell);
        btn.setOnClickListener(this);
        telBtn.setOnClickListener(this);
        initContent(root);
        return root;
    }


    private void initContent(View root) {
        TextView content = (TextView) root.findViewById(R.id.text_setting_title);
        Resources res = getResources();
        SpannableStringBuilder spannable = new SpannableStringBuilder(res.getString(R.string.text_setting_robot_isDataClean_start));
        final SpannableString str = new SpannableString(res.getString(R.string.text_setting_robot_isDataClean_phone));
        str.setSpan(new ClickableSpan() {

            @Override
            public void onClick(View widget) {
                Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + str.toString()));
                startActivity(intent);
                dismiss();
            }
        }, 0, str.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        spannable.append(str);
        SpannableString end = new SpannableString(res.getString(R.string.text_setting_robot_isDataClean_end));
        spannable.append(end);
        content.setText(spannable);
        //增加该方法才可响应点击
        content.setMovementMethod(LinkMovementMethod.getInstance());
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_tell:
                Resources res = getResources();
                final SpannableString str = new SpannableString(res.getString(R.string.text_setting_robot_isDataClean_phone));
                Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + str.toString()));
                startActivity(intent);
                dismiss();
                break;
            case R.id.btn_cancel:
                dismiss();
                break;
        }
    }
}
