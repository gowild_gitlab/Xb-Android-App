/**
 * Copyright (C)  2016 深圳市狗尾草智能科技有限公司
 * MyTool
 * GowildTabview.java
 */
package com.gowild.mobileclient.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.gowild.mobileclient.R;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * @author 王揆
 * @since 2016/7/23 9:36
 * @version 1.0
 * <p><strong>Features draft description.主要功能介绍</strong></p>
 */
public class GowildTabview extends RelativeLayout implements View.OnClickListener {

    private static final String TAG = GowildTabview.class.getSimpleName();

    @Bind(R.id.v_devide_line)
    protected View mVdevideLine;
    @Bind(R.id.ly_tab_content)
    protected LinearLayout mLyTabContent;

    private OnTabChangeListener mTabChangeListener;

    public GowildTabview(Context context) {
        super(context);
    }

    public GowildTabview(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public GowildTabview(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        ButterKnife.bind(this);
    }

    public void setTabNum(int num) {
        if (num == 0) {
            throw new IllegalStateException("tab num can not be zero");
        }
        mLyTabContent.removeAllViews();
        for (int i = 0; i < num; i++) {
            mLyTabContent.addView(getTabView());
        }
        //改变执行时序，保证拿到正确的width
        postDelayed(new Runnable() {
            @Override
            public void run() {
                ViewGroup.LayoutParams params = mVdevideLine.getLayoutParams();
                params.width = mLyTabContent.getWidth() / mLyTabContent.getChildCount();
                mVdevideLine.setLayoutParams(params);
            }
        },0);

    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }

    private TextView getTabView() {
        TextView view = (TextView) LayoutInflater.from(getContext()).inflate(R.layout.view_gowild_tab_item, mLyTabContent, false);
        view.setOnClickListener(this);
        return view;
    }

    public void setTabStrs(ArrayList<String> strs) {
        if (strs == null || strs.size() < mLyTabContent.getChildCount()) {
            throw new IllegalStateException("must set string for every tab");
        }
        for (int i = 0; i < mLyTabContent.getChildCount(); i++) {
            TextView textView = (TextView) mLyTabContent.getChildAt(i);
            textView.setText(strs.get(i));
        }
    }

    public void setTabStr(String str, int index) {
        if (str == null || index >= mLyTabContent.getChildCount()) {
            throw new IllegalStateException("must set string for exist tab");
        }
        TextView textView = (TextView) mLyTabContent.getChildAt(index);
        textView.setText(str);
    }

    public void setTabChangeListener(OnTabChangeListener listener) {
        mTabChangeListener = listener;
    }

    public void setCurrentTab(int position) {
        if (position >= mLyTabContent.getChildCount()) {
            return;
        }
        onClick(mLyTabContent.getChildAt(position));
    }

    @Override
    public void onClick(View v) {
        if (mTabChangeListener == null) {
            return;
        }
        int index = mLyTabContent.indexOfChild(v);
        mTabChangeListener.onTabChanged(index);
        mVdevideLine.layout((int)v.getX(), mVdevideLine.getTop(), (int)v.getX() + v.getWidth(), mVdevideLine.getBottom());
    }

    public interface OnTabChangeListener {
        void onTabChanged(int tabIndex);
    }
}
