/**
 * Copyright (C)  2016 深圳市狗尾草智能科技有限公司
 * GowildXB
 * MusicItemView.java
 */
package com.gowild.mobileclient.widget;

import android.content.Context;
import android.content.Intent;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.gowild.mobile.libcommon.utils.Logger;
import com.gowild.mobile.libhttp.HttpUtil;
import com.gowild.mobileclient.R;
import com.gowild.mobileclient.activity.music.MusicMainActivity;
import com.gowild.mobileclient.activity.music.StorySearchResultActivity;
import com.gowild.mobileclient.config.Constants;
import com.gowild.mobileclient.config.IntContants;
import com.gowild.mobileclient.config.MediaConstants;
import com.gowild.mobileclient.manager.GowildAsyncHttpResponseHandler;
import com.gowild.mobileclient.manager.GowildHttpManager;
import com.gowild.mobileclient.model.BaseNetResourceInfo;
import com.loopj.android.http.RequestParams;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * @author temp
 * @version 1.0
 *          <p><strong>音乐和故事的问题/strong></p>
 * @since 2016/7/27 11:28
 */
public class NetResourceItemView extends RelativeLayout {

    // ===========================================================
    // HostConstants
    // ===========================================================

    private static final String TAG = NetResourceItemView.class.getSimpleName();
    private BaseNetResourceInfo mInfo;

    @Bind(R.id.tv_name)
    protected TextView mTvName;
    @Bind(R.id.tv_author)
    protected TextView mTvAuthor;
    @Bind(R.id.iv_love)
    protected NetResourceCollectView mIvLove;

    public NetResourceItemView(Context context) {
        super(context);
    }

    public NetResourceItemView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public NetResourceItemView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        ButterKnife.bind(this);
    }

    @OnClick({R.id.ly_music_item})
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ly_music_item:
                onItemClick(mInfo);
                break;
            default:
                break;
        }
    }

    public void setInfo(BaseNetResourceInfo info) {
        if (info == null) {
            return;
        }
        Logger.d(TAG, "setInfo info = " + info);

        mTvName.setText(IntContants.isCurrentMusic() ? info.title : info.albumName);
        mTvAuthor.setText(info.announcerName);
        mIvLove.setInfo(info);
        mInfo = info;
    }

    public void onItemClick(BaseNetResourceInfo info) {
        if (IntContants.isCurrentMusic()) {
            //音乐播放
            GowildHttpManager.changeMusic(getContext(), new RequestParams(Constants.MUSIC_ID, info.musicId, Constants.PLAY_STATUS, Constants.PLAY_STATUS_PLAY, Constants.PLAY_TYPE, (info.collect == true) ? MediaConstants.COLLECT : MediaConstants.COLLECT_NO), new GowildAsyncHttpResponseHandler(getContext()) {
                @Override
                public void onSuccess(int statusCode, Object responseBody) {
                    Intent intent = new Intent(getContext(), MusicMainActivity.class);
                    getContext().startActivity(intent);
                }

                @Override
                public void onFailure(int statusCode, String responseBody) {

                }
            });
        } else {
            Intent intent = new Intent(getContext(), StorySearchResultActivity.class);
            intent.putExtra("NAME",info.albumName);
            intent.putExtra(Constants.INFOS, info.albumId);
            getContext().startActivity(intent);
        }
    }
}
