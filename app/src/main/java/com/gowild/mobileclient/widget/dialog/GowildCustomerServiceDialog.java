package com.gowild.mobileclient.widget.dialog;

import android.app.DialogFragment;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import com.gowild.mobileclient.R;


/**
 * Created by 客服信息
 */
public class GowildCustomerServiceDialog extends DialogFragment {

    private Button mCancel = null;
    private LinearLayout mCall = null;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NO_TITLE, R.style.gowildDialogStyle);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.gowild_dialog_customer_service, container, false);
        initView(root);
        return root;
    }

    private void initView(View view) {
        mCancel = (Button) view.findViewById(R.id.cancel);
        mCancel.setOnClickListener(mOnClickListener);
        mCall = (LinearLayout) view.findViewById(R.id.call);
        mCall.setOnClickListener(mOnClickListener);
    }

    private View.OnClickListener mOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.cancel:
                    dismiss();
                    break;
                case R.id.call:
                    Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + "4008239139"));
                    startActivity(intent);
                    break;
            }
        }
    };

}
