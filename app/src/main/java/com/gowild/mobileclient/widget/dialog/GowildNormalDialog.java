package com.gowild.mobileclient.widget.dialog;

import android.app.DialogFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.gowild.mobileclient.R;
import com.gowild.mobileclient.callback.OnNormalDialogListener;


/**
 * @author Zhangct
 * @version 1.0
 *          <p> 通用弹出框 </p>
 * @since 2016/8/6 17:40
 */
public class GowildNormalDialog extends DialogFragment {

    private Button mGowildCancelBtn = null;
    private Button mGowildConfirmBtn = null;
    private TextView mGowildContentTv = null;
    private String mContentStr = null;
    private String mCancelStr = null;
    private String mConfirmStr = null;
    private TextView mGowildContentTitle;
    private String mTitle;
    private boolean visible;

    public GowildNormalDialog() {

    }

    /**
     * 点击监听
     */
    private OnNormalDialogListener mDialogListener = null;

    public OnNormalDialogListener getDialogListener() {
        return mDialogListener;
    }

    /**
     * 设置监听
     *
     * @param mDialogListener
     */
    public void setDialogListener(OnNormalDialogListener mDialogListener) {
        this.mDialogListener = mDialogListener;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NO_TITLE, R.style.gowildDialogStyle);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.gowild_dialog_normal, container, false);
        initView(root);
        return root;
    }

    @Override
    public void onStart() {
        super.onStart();
        updateUI();
    }

    private void updateUI() {
        mGowildContentTv.setText(mContentStr);
        mGowildCancelBtn.setText(mCancelStr);
        mGowildConfirmBtn.setText(mConfirmStr);
        mGowildContentTitle.setText(mTitle);
    }

    /**
     * 初始化控件
     *
     * @param view
     */
    private void initView(View view) {
        mGowildCancelBtn = (Button) view.findViewById(R.id.btn_cancel);
        mGowildConfirmBtn = (Button) view.findViewById(R.id.btn_confirm);
        mGowildContentTv = (TextView) view.findViewById(R.id.tv_dialog_centent);
        mGowildContentTitle = (TextView) view.findViewById(R.id.tv_dialog_title);
        mGowildContentTitle.setVisibility(visible ? View.VISIBLE:View.INVISIBLE);
        mGowildCancelBtn.setOnClickListener(mOnClickListener);
        mGowildConfirmBtn.setOnClickListener(mOnClickListener);
    }

    /**
     * 设置弹出框内容
     *
     * @param title 文本内容
     */
    public void setDialogTitle(String title) {
        this.mTitle = title;
    }


    /**
     * 设置弹出框内容
     *
     * @param contentStr 文本内容
     */
    public void setDialogContent(String contentStr) {
        this.mContentStr = contentStr;
    }

    /**
     * 设置取消按钮文本
     *
     * @param cancelStr
     */
    public void setDialogCancel(String cancelStr) {
        this.mCancelStr = cancelStr;
    }

    /**
     * 设置确定按钮文本
     *
     * @param confrimStr
     */
    public void setDialogConfirm(String confrimStr) {
        this.mConfirmStr = confrimStr;
    }


    /**
     * 点击事件
     */
    private View.OnClickListener mOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.btn_cancel://取消
                    if (null != mDialogListener) {
                        mDialogListener.onCancelListener();
                    }
                    dismiss();
                    break;
                case R.id.btn_confirm://确定
                    if (null != mDialogListener) {
                        mDialogListener.onConfirmListener();
                    }
                    dismiss();
                    break;
            }
        }
    };

    public void setDialogTitleVi(boolean visible) {
        this.visible= visible;
    }
}
