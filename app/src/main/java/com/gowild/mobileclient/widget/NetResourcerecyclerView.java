/**
 * Copyright (C)  2016 深圳市狗尾草智能科技有限公司
 * GowildXB
 * NetResourcerecyclerView.java
 */
package com.gowild.mobileclient.widget;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.View;

import com.gowild.mobileclient.R;
import com.gowild.mobileclient.adapter.NetResourceListAdapter;
import com.gowild.mobileclient.model.BaseNetResourceInfo;

import java.util.ArrayList;

/**
 * @author temp
 * @since 2016/7/29 15:43
 * @version 1.0
 * <p><strong>Features draft description.主要功能介绍</strong></p>
 */
public class NetResourcerecyclerView extends PullableRecyclerView {

    // ===========================================================
    // HostConstants
    // ===========================================================

    private static final String TAG = NetResourcerecyclerView.class.getSimpleName();
    private NetResourceListAdapter  mAdapter;

    public NetResourcerecyclerView(Context context) {
        super(context);
    }

    public NetResourcerecyclerView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public NetResourcerecyclerView(Context context, @Nullable AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        setLayoutManager(new LinearLayoutManager(getContext()));
        addItemDecoration(new DividerItemDecoration(getContext(), VERTICAL, R.drawable.devide_line_gray, true));
        mAdapter = new NetResourceListAdapter();
        setAdapter(mAdapter);
    }

    public void setInfos(ArrayList<BaseNetResourceInfo> infos) {
        mAdapter.setInfos(infos);
        mAdapter.notifyDataSetChanged();
    }
}
