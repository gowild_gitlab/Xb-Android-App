package com.gowild.mobileclient.widget;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.gowild.mobile.libimage.ImageUtil;
import com.gowild.mobileclient.R;
import com.gowild.mobileclient.fragment.SkillDetailDialog;
import com.gowild.mobileclient.config.Constants;
import com.gowild.mobileclient.model.SkillBack;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by 王揆 on 2016/7/20.
 */
public class SkillItemView extends RelativeLayout {

    @Bind(R.id.iv_thumb)
    protected ImageView mIvThumb;
    @Bind(R.id.tv_name)
    protected TextView mTvName;
    @Bind(R.id.tv_lock)
    protected TextView mTvLock;

    private SkillBack.SkillItem mInfo;

    public SkillItemView(Context context) {
        super(context);
    }

    public SkillItemView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public SkillItemView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        ButterKnife.bind(this);
        setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Context context = v.getContext();
                if (context instanceof FragmentActivity) {
                    FragmentManager manager = ((FragmentActivity)context).getSupportFragmentManager();
                    SkillDetailDialog fragment = new SkillDetailDialog();
                    Bundle bundle = new Bundle();
                    bundle.putParcelable(Constants.INFO, mInfo);
                    fragment.setArguments(bundle);
                    fragment.show(manager, SkillDetailDialog.class.getName());

                }
            }
        });
    }

    public void setInfo(SkillBack.SkillItem info) {
        if (info == null) {
            return;
        }
        mInfo = info;
        mTvLock.setVisibility(info.isLock() ? VISIBLE : GONE);
        mTvName.setText(info.skillName);
        ImageUtil.getInstance().displayImage(info.skillIcon, mIvThumb);
    }
}
