/**
 * Copyright (C)  2016 深圳市狗尾草智能科技有限公司
 * GowildXB
 * TestConstants.java
 */
package com.gowild.mobileclient.test;

/**
 * @author temp
 * @since 2016/8/23 11:40
 * @version 1.0
 * <p><strong>Features draft description.主要功能介绍</strong></p>
 */
public class TestConstants {

    // ===========================================================
    // HostConstants
    // ===========================================================

    private static final String TAG = TestConstants.class.getSimpleName();

    public static final String SP_TEST = "sp_test";

    public static final String SP_WEB_ENVIRONMENT = "sp_web_environment";
    public static final String SP_SAVE_LOG = "sp_save_log";
    public static final int WEB_ENVIRONMENT_TEST = 1;

    public static final String ACTION_CHANGE_WEB_ENVIRONMENT = "com.gowild.test.change_web_environment";
    public static final String ENVIRONMENTVALUE = "environment_value";
    public static final String ACTION_OPEN_SAVE_LOG = "com.gowild.test.save_log";
}
