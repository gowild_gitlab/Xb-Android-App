/**
 * Copyright (C)  2016 深圳市狗尾草智能科技有限公司
 * GowildXB
 * TestReceiver.java
 */
package com.gowild.mobileclient.test;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.gowild.mobile.libcommon.utils.Logger;

/**
 * @author temp
 * @since 2016/8/23 11:39
 * @version 1.0
 * <p><strong>Features draft description.主要功能介绍</strong></p>
 */
public class TestReceiver extends BroadcastReceiver {

    // ===========================================================
    // HostConstants
    // ===========================================================

    private static final String TAG = TestReceiver.class.getSimpleName();

    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        Log.e(TAG, "action = " + action);
        switch (action) {
            case TestConstants.ACTION_CHANGE_WEB_ENVIRONMENT:
                int value = intent.getIntExtra(TestConstants.ENVIRONMENTVALUE, 0);
                Log.e(TAG, "value = " + value);
                context.getSharedPreferences(TestConstants.SP_TEST, 0)
                        .edit()
                        .putInt(TestConstants.SP_WEB_ENVIRONMENT, value)
                        .commit();
                break;
            case TestConstants.ACTION_OPEN_SAVE_LOG:
                context.getSharedPreferences(TestConstants.SP_TEST, 0)
                        .edit()
                        .putBoolean(TestConstants.SP_SAVE_LOG, true)
                        .commit();
                Logger.setSaveLog(true);
            default:
                break;
        }
    }
}
