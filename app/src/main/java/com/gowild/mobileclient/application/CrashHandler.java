/**
 * 
 */
package com.gowild.mobileclient.application;

import android.util.Log;

import com.gowild.mobile.libcommon.utils.Logger;

import org.greenrobot.eventbus.EventBus;

import java.lang.Thread.UncaughtExceptionHandler;

/**
 * @author 风语
 *
 */
public class CrashHandler implements UncaughtExceptionHandler {
    private static final String TAG = "CrashHandler";

    private static CrashHandler crashHandler;
    
    private UncaughtExceptionHandler mOldUncaughtExceptionHandler;
    
    private CrashHandler() {
        
    }
    
    public void init() {
        mOldUncaughtExceptionHandler = Thread.getDefaultUncaughtExceptionHandler();
        Thread.setDefaultUncaughtExceptionHandler(crashHandler);
    }
    
    public static CrashHandler getInstance() {
        if (crashHandler == null) {
            crashHandler = new CrashHandler();
        }
        return crashHandler;
    }
    
    @Override
    public void uncaughtException(Thread thread, Throwable ex) {

        if (mOldUncaughtExceptionHandler != null) {
            Logger.e(TAG, Log.getStackTraceString(ex));
            EventBus.getDefault().post("finish");
            mOldUncaughtExceptionHandler.uncaughtException(thread, ex);
            //MobclickAgent.onKillProcess(GowildApplication.context);
        }
    }
}
