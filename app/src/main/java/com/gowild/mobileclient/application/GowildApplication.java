package com.gowild.mobileclient.application;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Handler;
import android.support.multidex.MultiDex;

import com.gowild.gwais.libstatstics.ReporterFacade;
import com.gowild.mobile.libcommon.utils.Logger;
import com.gowild.mobile.libcommon.utils.ScreenUtils;
import com.gowild.mobile.libimage.ImageUtil;
import com.gowild.mobile.libp2sp.core.listener.IDataListener;
import com.gowild.mobile.libp2sp.netty.NettyClientManager;
import com.gowild.mobile.libviewer.MyViewerHelper;
import com.gowild.mobileclient.config.Constants;
import com.gowild.mobileclient.handler.AlertRingHandler;
import com.gowild.mobileclient.handler.AlertSyncHandler;
import com.gowild.mobileclient.handler.ApplianceAddResultHandler;
import com.gowild.mobileclient.handler.ApplianceDelResultHandler;
import com.gowild.mobileclient.handler.ApplianceSyncResultHandler;
import com.gowild.mobileclient.handler.ClockRingHandler;
import com.gowild.mobileclient.handler.ClockSyncHandler;
import com.gowild.mobileclient.handler.ConnectSucuessHanlder;
import com.gowild.mobileclient.handler.GetAuthResultHandler;
import com.gowild.mobileclient.handler.LoginResultHandler;
import com.gowild.mobileclient.handler.LoginoutHandler;
import com.gowild.mobileclient.handler.MachineUpgradeDescHandler;
import com.gowild.mobileclient.handler.MusicChangeHandler;
import com.gowild.mobileclient.handler.MusicUnlikeHandler;
import com.gowild.mobileclient.handler.PhoneUpgradeInfoHandler;
import com.gowild.mobileclient.handler.PlayHandler;
import com.gowild.mobileclient.handler.QuitAVMonitorResultHandler;
import com.gowild.mobileclient.handler.RobotBindHanlder;
import com.gowild.mobileclient.handler.RobotInfoSyncHandler;
import com.gowild.mobileclient.handler.RobotReceviceHandler;
import com.gowild.mobileclient.handler.RobotResultHanlder;
import com.gowild.mobileclient.handler.SoundHandler;
import com.gowild.mobileclient.handler.StartAVMonitorResultHandler;
import com.gowild.mobileclient.handler.StartHreatBeatHanlder;
import com.gowild.mobileclient.handler.StoryChangeHandler;
import com.gowild.mobileclient.handler.StoryUnlikeHandler;
import com.gowild.mobileclient.manager.GowildHttpManager;
import com.gowild.mobileclient.manager.LoginMananger;
import com.gowild.mobileclient.misc.GowildResponseCode;
import com.gowild.mobileclient.test.TestConstants;
import com.gowild.mobileclient.utils.ImageUtils;
import com.gowild.socialshare.GowildShare;

import java.io.File;

/**
 * @author shuai. wang
 * @version 1.0
 * @since ${DATE} ${TIME}
 * <p><strong>Features draft description.主要功能介绍</strong></p>
 */

public class GowildApplication extends Application {

    // ===========================================================
    // Static Fields
    // ===========================================================

    public static Context context;
    //    public MusicChangeListener mMusicChangeListener = new MusicChangeListener();
//    public StoryChangeListener mStoryChangeListener = new StoryChangeListener();
    private QureyTCP mQureyTCP = new QureyTCP();
    private String mHostAddress;
    //	private RobotHeartListener mHeartListener = new RobotHeartListener();


    // ===========================================================
    // Methods for/from SuperClass/Interfaces
    // ===========================================================

    @Override
    public void onCreate() {
        MultiDex.install(this);
        super.onCreate();
        context = getApplicationContext();
        CrashHandler.getInstance().init();
        ReporterFacade.init(this);
        ScreenUtils.initScreenWidthAndHeight(this);
        mHandler = new Handler();
        initTest();
        initImageLoader();
        initTCPClient();
        initSetTCPListener();
        GowildShare.configPlatform();
        MyViewerHelper.initInstance(this);
    }

    private void initTest() {
        SharedPreferences preferences = getSharedPreferences(TestConstants.SP_TEST, 0);
        GowildHttpManager.sWebEnvirmonent = preferences.getInt(TestConstants.SP_WEB_ENVIRONMENT, 0);
        Logger.setSaveLog(false);
    }

    private void initTCPClient() {
        NettyClientManager manager = NettyClientManager.getInstance();
        manager.init("58.67.213.148", 6030);
        manager.putHandler(GowildResponseCode.LOGIN_RESULT, new LoginResultHandler());
        manager.putHandler(GowildResponseCode.ROBOT_BIND_RESULT, new RobotBindHanlder());
        manager.putHandler(GowildResponseCode.ROBOT_RESULT, new RobotResultHanlder());
        manager.putHandler(GowildResponseCode.START_AV_MONITOR_RESULT, new StartAVMonitorResultHandler());
        manager.putHandler(GowildResponseCode.QUIT_AV_MONITOR_RESULT, new QuitAVMonitorResultHandler());
        manager.putHandler(GowildResponseCode.MUSIC_CHANGE, new MusicChangeHandler());
        manager.putHandler(GowildResponseCode.STORY_CHANGE, new StoryChangeHandler());
        manager.putHandler(GowildResponseCode.GET_AUTH_RESULT, new GetAuthResultHandler());
        manager.putHandler(GowildResponseCode.ROBOT_INFO_SYNC, new RobotInfoSyncHandler());
        manager.putHandler(GowildResponseCode.LOGIN_OUT, new LoginoutHandler());
        manager.setHeartBeatHandler(new StartHreatBeatHanlder());
        manager.putHandler(GowildResponseCode.HRART_RECEVICE, new RobotReceviceHandler());//心跳
        manager.putHandler(GowildResponseCode.NETTY_CONNECT, new ConnectSucuessHanlder());//心跳
        manager.putHandler(GowildResponseCode.APPLIANCE_ADD_RESULT, new ApplianceAddResultHandler());//家电
        manager.putHandler(GowildResponseCode.APPLIANCE_SYN_RESULT, new ApplianceSyncResultHandler());//家电
        manager.putHandler(GowildResponseCode.APPLIANCE_CONTROL_RESULT, new ApplianceAddResultHandler());//家电
        manager.putHandler(GowildResponseCode.APPLIANCE_DEL_RESULT, new ApplianceDelResultHandler());//家电
        manager.putHandler(GowildResponseCode.ALERT_SYNC, new AlertSyncHandler());
        manager.putHandler(GowildResponseCode.ALERT_RING, new AlertRingHandler());
        manager.putHandler(GowildResponseCode.CLOCK_SYNC, new ClockSyncHandler());
        manager.putHandler(GowildResponseCode.CLOCK_RING, new ClockRingHandler());
        manager.putHandler(GowildResponseCode.PHONE_UPGRADEINFO_RESULT, new PhoneUpgradeInfoHandler());
        manager.putHandler(GowildResponseCode.MACHINE_UPGRADEINFO_RESULT, new MachineUpgradeDescHandler());
        manager.putHandler(GowildResponseCode.PLAY_CHANGE, new PlayHandler());
        manager.putHandler(GowildResponseCode.SOUND_CHANGE, new SoundHandler());
        manager.putHandler(GowildResponseCode.MUSIC_UNLIKE, new MusicUnlikeHandler());
        manager.putHandler(GowildResponseCode.STORY_UNLIKE, new StoryUnlikeHandler());
    }

    private void initSetTCPListener() {
        NettyClientManager.getInstance().addHandlerListener(GowildResponseCode.HRART_RECEVICE, mHeartListener);
    }


    /**
     * 初始化图片加载库
     */
    private void initImageLoader() {
        File cacheDir = new File(Constants.CACHE_IMAGE);
        if (!cacheDir.exists()) {
            cacheDir.mkdirs();
        }
        ImageUtil.initContext(this);
        ImageUtils.init(this);


    }


    boolean isHreat = true;

    public void sendHeartBeat() {
        if (isHreat) {
            time = 2;
            mHandler.postDelayed(mQureyTCP, 20000);
            isHreat = false;
        }
    }


    public void stopHeartBeat() {
        mHandler.removeCallbacks(mQureyTCP);
        isHreat = true;
    }

    public static Handler mHandler;


    private class QureyTCP implements Runnable {
        @Override
        public void run() {
            time++;
//			System.out.print("data执行开始");
            if (time > 4) {
                time = 2;
                LoginMananger.getInstence().TCPLoginRequest();
            }
            mHandler.postDelayed(this, 20000);
        }
    }


    private RobotHeartListener mHeartListener = new RobotHeartListener();

    long time = 2;

    private class RobotHeartListener implements IDataListener<String> {

        @Override
        public void onReceiveData(String data) {
//            106
            time = 2;
//            System.out.println("time" + time);
        }

        @Override
        public void onActiveChanged(boolean isActive) {

        }
    }
}
