/**
 * Copyright (C)  2016 深圳市狗尾草智能科技有限公司
 * GowildXB
 * ApplianceAddResultHandler.java
 */
package com.gowild.mobileclient.handler;

import com.google.protobuf.InvalidProtocolBufferException;
import com.gowild.mobile.libp2sp.core.AbsDataHandler;
import com.gowild.mobileclient.protocol.StringMsgPro;

/**
 * @author Zhangct
 * @version 1.0
 *          <p> 主要功能介绍 </p>
 * @since 2016/8/20 18:58
 */
public class ApplianceAddResultHandler extends AbsDataHandler<StringMsgPro.StringMsg> {

    private static final String TAG = ApplianceAddResultHandler.class.getSimpleName();

    @Override
    protected StringMsgPro.StringMsg parseData(byte[] bytes) {
        StringMsgPro.StringMsg msg = null;
        try {
            System.out.println("收到啦收到啦.........");
            msg = StringMsgPro.StringMsg.parseFrom(bytes);
        } catch (InvalidProtocolBufferException e) {
            e.printStackTrace();
        }
        return msg;
    }
}

