/**
 * Copyright (C)  2016 深圳市狗尾草智能科技有限公司
 * GowildXB
 * LoginResultHandler.java
 */
package com.gowild.mobileclient.handler;

import com.google.protobuf.InvalidProtocolBufferException;
import com.gowild.mobile.libcommon.utils.Logger;
import com.gowild.mobile.libp2sp.core.AbsDataHandler;
import com.gowild.mobileclient.protocol.LoginResultPro;

import java.util.Arrays;

/**
 * @author Ji.Li
 * @since 2016/8/11 16:37
 * @version 1.0
 * <p><strong>Features draft description.主要功能介绍</strong></p>
 */
public class LoginResultHandler extends AbsDataHandler<LoginResultPro.LoginForAppSMsg>{

    // ===========================================================
    // HostConstants
    // ===========================================================

    private static final String TAG = LoginResultHandler.class.getSimpleName();

    // ===========================================================
    // Static Fields
    // ===========================================================

    // ===========================================================
    // Fields
    // ===========================================================


    // ===========================================================
    // Constructors
    // ===========================================================


    // ===========================================================
    // Getter or Setter
    // ===========================================================


    // ===========================================================
    // Methods for/from SuperClass/Interfaces
    // ===========================================================

    @Override
    protected LoginResultPro.LoginForAppSMsg parseData(byte[] bytes) {
        try {
            LoginResultPro.LoginForAppSMsg msg = LoginResultPro.LoginForAppSMsg.parseFrom(bytes);
            return msg;
        } catch (InvalidProtocolBufferException e) {
            e.printStackTrace();
        }
        return null;
    }


    // ===========================================================
    // Methods
    // ===========================================================


    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================

}
