/**
 * Copyright (C)  2016 深圳市狗尾草智能科技有限公司
 * GowildXB
 * RobotInfoSyncHandler.java
 */
package com.gowild.mobileclient.handler;

import com.google.protobuf.InvalidProtocolBufferException;
import com.gowild.mobile.libp2sp.core.AbsDataHandler;
import com.gowild.mobileclient.protocol.MachineBothMsgProto;

/**
 * @author ljd
 * @version 1.0
 *          <p><strong>Features draft description.主要功能介绍</strong></p>
 * @since 2016/8/13 14:58
 */
public class RobotInfoSyncHandler extends AbsDataHandler<MachineBothMsgProto.MachineInfoMsg> {

    // ===========================================================
    // HostConstants
    // ===========================================================

    private static final String TAG = RobotInfoSyncHandler.class.getSimpleName();

    @Override
    protected MachineBothMsgProto.MachineInfoMsg parseData(byte[] bytes) {
        try {
            MachineBothMsgProto.MachineInfoMsg msg = MachineBothMsgProto.MachineInfoMsg.parseFrom(bytes);
            return msg;
        } catch (InvalidProtocolBufferException e) {
            e.printStackTrace();
        }
        return null;
    }

    // ===========================================================
    // Static Fields
    // ===========================================================

    // ===========================================================
    // Fields
    // ===========================================================


    // ===========================================================
    // Constructors
    // ===========================================================


    // ===========================================================
    // Getter or Setter
    // ===========================================================


    // ===========================================================
    // Methods for/from SuperClass/Interfaces
    // ===========================================================


    // ===========================================================
    // Methods
    // ===========================================================


    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================

}
