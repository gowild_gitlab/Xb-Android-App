/**
 * Copyright (C)  2016 深圳市狗尾草智能科技有限公司
 * GowildXB
 * ConnectSucuessHanlder.java
 */
package com.gowild.mobileclient.handler;

import com.gowild.mobile.libp2sp.core.AbsDataHandler;

/**
 * @author shuai. wang
 * @since 2016/8/18 11:43
 * @version 1.0
 * <p><strong>Features draft description.主要功能介绍</strong></p>
 */
public class ConnectSucuessHanlder extends AbsDataHandler<String> {

	// ===========================================================
	// HostConstants
	// ===========================================================

	private static final String TAG = ConnectSucuessHanlder.class.getSimpleName();

	@Override
	protected String parseData(byte[] bytes) {
		//判断是否连接上
		return null;
	}

	// ===========================================================
	// Static Fields
	// ===========================================================

	// ===========================================================
	// Fields
	// ===========================================================


	// ===========================================================
	// Constructors
	// ===========================================================


	// ===========================================================
	// Getter or Setter
	// ===========================================================


	// ===========================================================
	// Methods for/from SuperClass/Interfaces
	// ===========================================================


	// ===========================================================
	// Methods
	// ===========================================================


	// ===========================================================
	// Inner and Anonymous Classes
	// ===========================================================

}
