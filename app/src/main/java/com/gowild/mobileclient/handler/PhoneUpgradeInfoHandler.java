/**
 * Copyright (C)  2016 深圳市狗尾草智能科技有限公司
 * GowildXB
 * LoginoutHandler.java
 */
package com.gowild.mobileclient.handler;

import com.google.protobuf.InvalidProtocolBufferException;
import com.gowild.mobile.libp2sp.core.AbsDataHandler;
import com.gowild.mobileclient.protocol.PhoneUpgradeInfoPro;

/**
 * @author shuai. wang
 * @since 2016/8/17 20:35
 * @version 1.0
 * <p><strong>App升级信息</strong></p>
 */
public class PhoneUpgradeInfoHandler extends AbsDataHandler<PhoneUpgradeInfoPro.PhoneUpgradeInfoMsg> {

	// ===========================================================
	// HostConstants
	// ===========================================================

	private static final String TAG = PhoneUpgradeInfoHandler.class.getSimpleName();


	@Override
	protected PhoneUpgradeInfoPro.PhoneUpgradeInfoMsg parseData(byte[] bytes) {
		try {
			PhoneUpgradeInfoPro.PhoneUpgradeInfoMsg msg = PhoneUpgradeInfoPro.PhoneUpgradeInfoMsg.parseFrom(bytes);
			return msg;
		} catch (InvalidProtocolBufferException e) {
			e.printStackTrace();
		}
		return null;
	}

	// ===========================================================
	// Static Fields
	// ===========================================================

	// ===========================================================
	// Fields
	// ===========================================================


	// ===========================================================
	// Constructors
	// ===========================================================


	// ===========================================================
	// Getter or Setter
	// ===========================================================


	// ===========================================================
	// Methods for/from SuperClass/Interfaces
	// ===========================================================


	// ===========================================================
	// Methods
	// ===========================================================


	// ===========================================================
	// Inner and Anonymous Classes
	// ===========================================================

}
