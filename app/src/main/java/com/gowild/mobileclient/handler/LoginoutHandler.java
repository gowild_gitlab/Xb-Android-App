/**
 * Copyright (C)  2016 深圳市狗尾草智能科技有限公司
 * GowildXB
 * LoginoutHandler.java
 */
package com.gowild.mobileclient.handler;

import com.gowild.mobile.libcommon.utils.ToastUtils;
import com.gowild.mobile.libp2sp.core.AbsDataHandler;
import com.gowild.mobile.libp2sp.netty.NettyClientManager;
import com.gowild.mobileclient.R;
import com.gowild.mobileclient.application.GowildApplication;
import com.gowild.mobileclient.manager.LoginMananger;

/**
 * @author shuai. wang
 * @since 2016/8/17 20:35
 * @version 1.0
 * <p><strong>Features draft description.主要功能介绍</strong></p>
 */
public class LoginoutHandler extends AbsDataHandler<String> {

	// ===========================================================
	// HostConstants
	// ===========================================================

	private static final String TAG = LoginoutHandler.class.getSimpleName();


	@Override
	protected String parseData(byte[] bytes) {
		NettyClientManager.getInstance().stopHeartBeat();
		NettyClientManager.getInstance().close();
		GowildApplication.mHandler.post(new Runnable() {
            @Override
            public void run() {
                ToastUtils.showCustom(GowildApplication.context, R.string.loginout,false);
            }
        });
		LoginMananger.getInstence().startLunch();
		return "";
	}

	// ===========================================================
	// Static Fields
	// ===========================================================

	// ===========================================================
	// Fields
	// ===========================================================


	// ===========================================================
	// Constructors
	// ===========================================================


	// ===========================================================
	// Getter or Setter
	// ===========================================================


	// ===========================================================
	// Methods for/from SuperClass/Interfaces
	// ===========================================================


	// ===========================================================
	// Methods
	// ===========================================================


	// ===========================================================
	// Inner and Anonymous Classes
	// ===========================================================

}
