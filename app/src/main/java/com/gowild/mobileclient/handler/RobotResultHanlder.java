/**
 * Copyright (C)  2016 深圳市狗尾草智能科技有限公司
 * GowildXB
 * RobotResultHanlder.java
 */
package com.gowild.mobileclient.handler;

import com.google.protobuf.InvalidProtocolBufferException;
import com.gowild.mobile.libcommon.utils.Logger;
import com.gowild.mobile.libp2sp.core.AbsDataHandler;
import com.gowild.mobileclient.protocol.RobotResultPro;

import java.util.Arrays;

/**
 * @author shuai. wang
 * @version 1.0
 *          <p><strong>Features draft description.主要功能介绍</strong></p>
 * @since 2016/8/11 21:53
 */
public class RobotResultHanlder extends AbsDataHandler<RobotResultPro.BoolMsg> {

	// ===========================================================
	// HostConstants
	// ===========================================================

	private static final String TAG = RobotResultHanlder.class.getSimpleName();

	@Override
	protected RobotResultPro.BoolMsg parseData(byte[] bytes) {
		Logger.d(TAG, "parseData() bytes = " + Arrays.toString(bytes));
		try {
			RobotResultPro.BoolMsg msg = RobotResultPro.BoolMsg.parseFrom(bytes);
			return msg;
		} catch (InvalidProtocolBufferException e) {
			e.printStackTrace();
		}
		return null;
	}

	// ===========================================================
	// Static Fields
	// ===========================================================

	// ===========================================================
	// Fields
	// ===========================================================


	// ===========================================================
	// Constructors
	// ===========================================================


	// ===========================================================
	// Getter or Setter
	// ===========================================================


	// ===========================================================
	// Methods for/from SuperClass/Interfaces
	// ===========================================================


	// ===========================================================
	// Methods
	// ===========================================================


	// ===========================================================
	// Inner and Anonymous Classes
	// ===========================================================

}
