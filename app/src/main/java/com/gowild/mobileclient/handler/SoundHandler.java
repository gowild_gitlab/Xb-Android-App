/**
 * Copyright (C)  2016 深圳市狗尾草智能科技有限公司
 * GowildXB
 * AlertSyncHandler.java
 */
package com.gowild.mobileclient.handler;


import com.google.protobuf.InvalidProtocolBufferException;
import com.gowild.mobile.libp2sp.core.AbsDataHandler;
import com.gowild.mobileclient.protocol.PlayPro;
import com.gowild.mobileclient.protocol.SoundPro;

/**
 * @author ljd
 * @since 2016/8/29 18:31
 * @version 1.0
 * <p><strong>Features draft description.主要功能介绍</strong></p>
 */
public class SoundHandler extends AbsDataHandler<SoundPro.IntMsg> {

    // ===========================================================
    // HostConstants
    // ===========================================================

    private static final String TAG = SoundHandler.class.getSimpleName();

    @Override
    protected SoundPro.IntMsg parseData(byte[] bytes) {
        try {
            SoundPro.IntMsg intMsg = SoundPro.IntMsg.parseFrom(bytes);
            return intMsg;
        } catch (InvalidProtocolBufferException e) {
            e.printStackTrace();
        }
        return null;
    }


    // ===========================================================
    // Static Fields
    // ===========================================================

    // ===========================================================
    // Fields
    // ===========================================================


    // ===========================================================
    // Constructors
    // ===========================================================


    // ===========================================================
    // Getter or Setter
    // ===========================================================


    // ===========================================================
    // Methods for/from SuperClass/Interfaces
    // ===========================================================


    // ===========================================================
    // Methods
    // ===========================================================


    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================

}
