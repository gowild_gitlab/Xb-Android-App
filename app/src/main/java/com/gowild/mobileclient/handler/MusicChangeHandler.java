/**
 * Copyright (C)  2016 深圳市狗尾草智能科技有限公司
 * GowildXB
 * MusicChangeHandler.java
 */
package com.gowild.mobileclient.handler;

import com.google.protobuf.InvalidProtocolBufferException;
import com.gowild.mobile.libp2sp.core.AbsDataHandler;
import com.gowild.mobileclient.protocol.MusicBothMsgProto;
import com.gowild.mobileclient.protocol.MusicS2ACMsgProto;

/**
 * @author ljd
 * @since 2016/8/13 18:43
 * @version 1.0
 * <p><strong>Features draft description.主要功能介绍</strong></p>
 */
public class MusicChangeHandler extends AbsDataHandler<MusicS2ACMsgProto.MusicSyncForAppMsg> {

    // ===========================================================
    // HostConstants
    // ===========================================================

    private static final String TAG = MusicChangeHandler.class.getSimpleName();

    @Override
    protected MusicS2ACMsgProto.MusicSyncForAppMsg parseData(byte[] bytes) {
        try {
            MusicS2ACMsgProto.MusicSyncForAppMsg msg = MusicS2ACMsgProto.MusicSyncForAppMsg.parseFrom(bytes);
            return msg;
        } catch (InvalidProtocolBufferException e) {
            e.printStackTrace();
        }

        return null;
    }

    // ===========================================================
    // Static Fields
    // ===========================================================

    // ===========================================================
    // Fields
    // ===========================================================


    // ===========================================================
    // Constructors
    // ===========================================================


    // ===========================================================
    // Getter or Setter
    // ===========================================================


    // ===========================================================
    // Methods for/from SuperClass/Interfaces
    // ===========================================================


    // ===========================================================
    // Methods
    // ===========================================================


    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================

}
