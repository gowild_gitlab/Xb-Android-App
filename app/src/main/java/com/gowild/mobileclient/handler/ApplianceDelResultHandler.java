/**
 * Copyright (C)  2016 深圳市狗尾草智能科技有限公司
 * GowildXB
 * ApplianceDelResultHandler.java
 */
package com.gowild.mobileclient.handler;

import com.google.protobuf.InvalidProtocolBufferException;
import com.gowild.mobile.libp2sp.core.AbsDataHandler;
import com.gowild.mobileclient.protocol.IntMsgPro;

/**
 * @author Zhangct
 * @since 2016/8/20 18:59
 * @version 1.0
 * <p> 主要功能介绍 </p>
 */
public class ApplianceDelResultHandler extends AbsDataHandler<IntMsgPro.IntMsg> {

    @Override
    protected IntMsgPro.IntMsg parseData(byte[] bytes) {
        IntMsgPro.IntMsg msg = null;

        try {
            msg = IntMsgPro.IntMsg.parseFrom(bytes);
        } catch (InvalidProtocolBufferException e) {
            e.printStackTrace();
        }
        return msg;
    }
}
