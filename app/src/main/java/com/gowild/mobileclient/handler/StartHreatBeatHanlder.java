/**
 * Copyright (C)  2016 深圳市狗尾草智能科技有限公司
 * GowildXB
 * StartHreatBeatHanlder.java
 */
package com.gowild.mobileclient.handler;


import com.gowild.mobile.libp2sp.core.Message;
import com.gowild.mobile.libp2sp.core.exception.NotConnectedException;
import com.gowild.mobile.libp2sp.core.listener.IHeartBeatHandler;
import com.gowild.mobile.libp2sp.netty.NettyClientManager;

/**
 * @author shuai. wang
 * @since 2016/8/16 16:16
 * @version 1.0
 * <p><strong>Features draft description.主要功能介绍</strong></p>
 */
public class StartHreatBeatHanlder implements IHeartBeatHandler {

	// ===========================================================
	// HostConstants
	// ===========================================================

	private static final String TAG = StartHreatBeatHanlder.class.getSimpleName();

	@Override
	public void onSendHeartBeat(NettyClientManager client) {
		byte type = 2;
		short code = 103;
		Message message = Message.createMessage(type,code);
		try {
			NettyClientManager.getInstance().send(message);
		} catch (NotConnectedException e) {
			e.printStackTrace();
		}
	}

	// ===========================================================
	// Static Fields
	// ===========================================================

	// ===========================================================
	// Fields
	// ===========================================================


	// ===========================================================
	// Constructors
	// ===========================================================


	// ===========================================================
	// Getter or Setter
	// ===========================================================


	// ===========================================================
	// Methods for/from SuperClass/Interfaces
	// ===========================================================


	// ===========================================================
	// Methods
	// ===========================================================


	// ===========================================================
	// Inner and Anonymous Classes
	// ===========================================================

}
