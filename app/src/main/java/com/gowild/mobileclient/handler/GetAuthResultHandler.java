/**
 * Copyright (C)  2016 深圳市狗尾草智能科技有限公司
 * GowildXB
 * GetAuthResultHandler.java
 */
package com.gowild.mobileclient.handler;

import com.google.protobuf.InvalidProtocolBufferException;
import com.gowild.mobile.libp2sp.core.AbsDataHandler;
import com.gowild.mobileclient.protocol.MonitorBothMsgProto;

/**
 * @author Ji.Li
 * @since 2016/8/12 10:24
 * @version 1.0
 * <p><strong>Features draft description.主要功能介绍</strong></p>
 */
public class GetAuthResultHandler extends AbsDataHandler<MonitorBothMsgProto.MonitorPowerMsg>{

    // ===========================================================
    // HostConstants
    // ===========================================================

    private static final String TAG = GetAuthResultHandler.class.getSimpleName();

    // ===========================================================
    // Static Fields
    // ===========================================================

    // ===========================================================
    // Fields
    // ===========================================================


    // ===========================================================
    // Constructors
    // ===========================================================


    // ===========================================================
    // Getter or Setter
    // ===========================================================


    // ===========================================================
    // Methods for/from SuperClass/Interfaces
    // ===========================================================

    @Override
    protected MonitorBothMsgProto.MonitorPowerMsg parseData(byte[] bytes) {
        try {
            MonitorBothMsgProto.MonitorPowerMsg msg = MonitorBothMsgProto.MonitorPowerMsg.parseFrom(bytes);
            return msg;
        } catch (InvalidProtocolBufferException e) {
            e.printStackTrace();
        }
        return null;
    }


    // ===========================================================
    // Methods
    // ===========================================================


    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================

}
