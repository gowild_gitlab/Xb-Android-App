/**
 * Copyright (C)  2016 深圳市狗尾草智能科技有限公司
 * GowildXB
 * QuitAVMonitorResultHandler.java
 */
package com.gowild.mobileclient.handler;

import com.google.protobuf.InvalidProtocolBufferException;
import com.gowild.mobile.libp2sp.core.AbsDataHandler;
import com.gowild.mobileclient.protocol.BaseBothMsgProto;

/**
 * @author Ji.Li
 * @since 2016/8/12 10:22
 * @version 1.0
 * <p><strong>Features draft description.主要功能介绍</strong></p>
 */
public class QuitAVMonitorResultHandler extends AbsDataHandler<BaseBothMsgProto.IntMsg>{

    // ===========================================================
    // HostConstants
    // ===========================================================

    private static final String TAG = QuitAVMonitorResultHandler.class.getSimpleName();

    // ===========================================================
    // Static Fields
    // ===========================================================

    // ===========================================================
    // Fields
    // ===========================================================


    // ===========================================================
    // Constructors
    // ===========================================================


    // ===========================================================
    // Getter or Setter
    // ===========================================================


    // ===========================================================
    // Methods for/from SuperClass/Interfaces
    // ===========================================================

    @Override
    protected BaseBothMsgProto.IntMsg parseData(byte[] bytes) {
        try {
            BaseBothMsgProto.IntMsg msg = BaseBothMsgProto.IntMsg.parseFrom(bytes);
            return msg;
        } catch (InvalidProtocolBufferException e) {
            e.printStackTrace();
        }
        return null;
    }

    // ===========================================================
    // Methods
    // ===========================================================


    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================

}
