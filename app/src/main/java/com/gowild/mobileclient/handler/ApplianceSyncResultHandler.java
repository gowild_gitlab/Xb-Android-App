/**
 * Copyright (C)  2016 深圳市狗尾草智能科技有限公司
 * GowildXB
 * ApplianceAddResultHandler.java
 */
package com.gowild.mobileclient.handler;

import com.google.protobuf.InvalidProtocolBufferException;
import com.gowild.mobile.libp2sp.core.AbsDataHandler;
import com.gowild.mobileclient.protocol.ElectricInfoListPro;

/**
 * @author Zhangct
 * @version 1.0
 *          <p> 主要功能介绍 </p>
 * @since 2016/8/20 18:58
 */
public class ApplianceSyncResultHandler extends AbsDataHandler<ElectricInfoListPro.ElectricInfoListMsg> {

    private static final String TAG = ApplianceSyncResultHandler.class.getSimpleName();

    @Override
    protected ElectricInfoListPro.ElectricInfoListMsg parseData(byte[] bytes) {
        ElectricInfoListPro.ElectricInfoListMsg msg = null;
        try {
            if(null == bytes){
                msg = null;
            }else {
                msg = ElectricInfoListPro.ElectricInfoListMsg.parseFrom(bytes);
            }
            System.out.println("收到啦收到啦.........");
        } catch (InvalidProtocolBufferException e) {
            e.printStackTrace();
        }
        return msg;
    }
}

