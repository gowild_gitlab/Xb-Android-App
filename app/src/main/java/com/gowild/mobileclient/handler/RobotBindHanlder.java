/**
 * Copyright (C)  2016 深圳市狗尾草智能科技有限公司
 * GowildXB
 * RobotBindHanlder.java
 */
package com.gowild.mobileclient.handler;

import com.google.protobuf.InvalidProtocolBufferException;
import com.gowild.mobile.libp2sp.core.AbsDataHandler;
import com.gowild.mobile.libp2sp.core.listener.IDataHandler;
import com.gowild.mobileclient.protocol.LoginResultPro;
import com.gowild.mobileclient.protocol.RobotBindReslutIntMsgPro;

/**
 * @author ssywbj
 * @since 2016/8/23 21:11
 * @version 1.0
 * <p><strong>Features draft description.主要功能介绍</strong></p>
 */
public class RobotBindHanlder extends AbsDataHandler<RobotBindReslutIntMsgPro.IntMsg> {

    // ===========================================================
    // HostConstants
    // ===========================================================

    private static final String TAG = RobotBindHanlder.class.getSimpleName();

    @Override
    protected RobotBindReslutIntMsgPro.IntMsg parseData(byte[] bytes) {
        try {
            RobotBindReslutIntMsgPro.IntMsg msg= RobotBindReslutIntMsgPro.IntMsg.parseFrom(bytes);
            return msg;
        } catch (InvalidProtocolBufferException e) {
            e.printStackTrace();
        }
        return null;
    }

    // ===========================================================
    // Static Fields
    // ===========================================================

    // ===========================================================
    // Fields
    // ===========================================================


    // ===========================================================
    // Constructors
    // ===========================================================


    // ===========================================================
    // Getter or Setter
    // ===========================================================


    // ===========================================================
    // Methods for/from SuperClass/Interfaces
    // ===========================================================


    // ===========================================================
    // Methods
    // ===========================================================


    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================

}
