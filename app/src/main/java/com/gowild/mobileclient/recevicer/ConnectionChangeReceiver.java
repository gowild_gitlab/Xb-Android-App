package com.gowild.mobileclient.recevicer;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.gowild.mobileclient.manager.LoginMananger;


public class ConnectionChangeReceiver extends BroadcastReceiver {
    boolean isNetAvalib = true;

    @Override
    public void onReceive(Context context, Intent intent) {
        ConnectivityManager connectivityManager=(ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo mobNetInfo=connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        NetworkInfo wifiNetInfo=connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        if (!mobNetInfo.isConnected() && !wifiNetInfo.isConnected()) {
         //改变背景或者 处理网络的全局变量
         //网络不可用
            isNetAvalib = false;
        }else {
         //网络可用
         //改变背景或者 处理网络的全局变量
            if (!isNetAvalib){
                LoginMananger.getInstence().TCPLoginRequest();
                 isNetAvalib = true;
            }

        }

    }
}