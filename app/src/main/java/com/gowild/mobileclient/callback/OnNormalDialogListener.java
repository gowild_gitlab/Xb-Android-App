/**
 * Copyright (C)  2016 深圳市狗尾草智能科技有限公司
 * GowildXB
 * OnNormalDialogListener.java
 */
package com.gowild.mobileclient.callback;

/**
 * @author Zhangct
 * @since 2016/8/8 15:44
 * @version 1.0
 * <p> 主要功能介绍 </p>
 */
public interface OnNormalDialogListener {
    /**
     * 取消监听
     */
    void onCancelListener();

    /**
     * 确定监听
     */
    void onConfirmListener();
}
