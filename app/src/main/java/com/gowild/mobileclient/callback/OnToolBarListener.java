/**
 * Copyright (C)  2016 深圳市狗尾草智能科技有限公司
 * GowildXB
 * OnToolBarListener.java
 */
package com.gowild.mobileclient.callback;

/**
 * @author Administrator
 * @since 2016/8/3 9:20
 * @version 1.0
 * <p><strong>Features draft description.主要功能介绍</strong></p>
 */
public interface OnToolBarListener {

    /**
     * 当顶部菜单栏右边控件被点击时调用方法
     */
    public void onActionClickListener();
}
