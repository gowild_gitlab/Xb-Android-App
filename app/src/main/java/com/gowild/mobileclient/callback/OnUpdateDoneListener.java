/**
 * Copyright (C)  2016 深圳市狗尾草智能科技有限公司
 * GowildXB
 * OnUpdateDoneListener.java
 */
package com.gowild.mobileclient.callback;

import java.io.File;

/**
 * 实现这个监听，当升级检查完成时调用回调方法
 */
public abstract class OnUpdateDoneListener {
    public abstract void checkDone();

    public abstract void isNewVersion();

    public void downDone(){};

    public void exitApplication() {
    }
    public void cancel() {
    }

    public void install(String file){}
}