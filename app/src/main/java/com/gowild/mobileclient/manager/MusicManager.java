/**
 * Copyright (C)  2016 深圳市狗尾草智能科技有限公司
 * GowildXB
 * MusicManager.java
 */
package com.gowild.mobileclient.manager;

import android.graphics.Bitmap;
import android.view.View;

import com.gowild.mobile.libp2sp.core.listener.IDataListener;
import com.gowild.mobile.libp2sp.netty.NettyClientManager;
import com.gowild.mobileclient.config.MediaConstants;
import com.gowild.mobileclient.misc.GowildResponseCode;
import com.gowild.mobileclient.model.BaseNetResourceInfo;
import com.gowild.mobileclient.protocol.MusicS2ACMsgProto;
import com.gowild.mobileclient.protocol.PlayPro;
import com.gowild.mobileclient.protocol.StoryS2ACMsgProto;
import com.gowild.mobileclient.utils.ImageUtils;
import com.nostra13.universalimageloader.core.assist.FailReason;

/**
 * @author ssywbj
 * @since 2016/9/20 21:55
 * @version 1.0
 * <p><strong>Features draft description.主要功能介绍</strong></p>
 */
public class MusicManager {

    // ===========================================================
    // HostConstants
    // ===========================================================

    private static final String TAG = MusicManager.class.getSimpleName();

    private static MusicManager instence;

    public static MusicManager getInstence() {
        if (instence == null) {
            synchronized (LoginMananger.class) {
                if (instence == null) {
                    instence = new MusicManager();
                }
            }
        }
        return instence;
    }
    private StoryChangeListener mStoryChangeListener = new StoryChangeListener();
    private MusicChangeListener mMusicChangeListener = new MusicChangeListener();
    private PlayChangeListener mPlayChangeListener = new PlayChangeListener();
    private MusicUnlikeListener mMusicUnlikeListener = new MusicUnlikeListener();
    private MusicManager(){
        NettyClientManager manager = NettyClientManager.getInstance();
        manager.addHandlerListener(GowildResponseCode.MUSIC_CHANGE, mMusicChangeListener);
        manager.addHandlerListener(GowildResponseCode.STORY_CHANGE, new StoryChangeListener());
        manager.addHandlerListener(GowildResponseCode.PLAY_CHANGE, mPlayChangeListener);
        manager.addHandlerListener(GowildResponseCode.MUSIC_UNLIKE, mMusicUnlikeListener);
    }


    private class MusicChangeListener implements IDataListener<MusicS2ACMsgProto.MusicSyncForAppMsg> {

        @Override
        public void onReceiveData(final MusicS2ACMsgProto.MusicSyncForAppMsg data) {

        }

        @Override
        public void onActiveChanged(boolean isActive) {

        }
    }



    private class MusicUnlikeListener implements IDataListener<String> {

        @Override
        public void onReceiveData(String s) {

        }

        @Override
        public void onActiveChanged(boolean isActive) {

        }
    }


    private class PlayChangeListener implements IDataListener<PlayPro.IntMsg> {

        @Override
        public void onReceiveData(final PlayPro.IntMsg data) {
        }

        @Override
        public void onActiveChanged(boolean isActive) {

        }
    }


    private class StoryChangeListener implements IDataListener<StoryS2ACMsgProto.StorySyncForAppMsg> {

        @Override
        public void onReceiveData(final StoryS2ACMsgProto.StorySyncForAppMsg data) {

        }

        @Override
        public void onActiveChanged(boolean isActive) {

        }

    }

    private class CoverImageLoadingListener implements com.nostra13.universalimageloader.core.listener.ImageLoadingListener {

        @Override
        public void onLoadingStarted(String imageUri, View view) {

        }

        @Override
        public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
        }

        @Override
        public void onLoadingFailed(String imageUri, View view, FailReason failReason) {

        }

        @Override
        public void onLoadingCancelled(String imageUri, View view) {

        }

    }



    //


}
