/**
 * Copyright (C)  2016 深圳市狗尾草智能科技有限公司
 * GowildXB
 * GowildTCPManager.java
 */
package com.gowild.mobileclient.manager;

import com.gowild.mobile.libcommon.utils.Logger;
import com.gowild.mobile.libp2sp.core.Message;
import com.gowild.mobile.libp2sp.core.exception.NotConnectedException;
import com.gowild.mobile.libp2sp.netty.NettyClientManager;
import com.gowild.mobileclient.protocol.BaseBothMsgProto;
import com.gowild.mobileclient.protocol.ElectricInfoDeletePro;
import com.gowild.mobileclient.protocol.LoginPro;
import com.gowild.mobileclient.protocol.PhoneCheckVersionPro;
import com.gowild.mobileclient.protocol.StringMsgPro;
import com.gowild.mobileclient.protocol.WifiInfoPro;

/**
 * @author Ji.Li
 * @version 1.0
 *          <p><strong>Features draft description.主要功能介绍</strong></p>
 * @since 2016/8/11 16:28
 */
@SuppressWarnings("unused")
public final class GowildTCPManager {

	// ===========================================================
	// Constants
	// ===========================================================

	private static final String TAG = GowildTCPManager.class.getSimpleName();
	private static final byte TYPE = 2;

	private static final short REQUEST_PROTOCAL_REQUEST_LOGIN = 101;
	private static final short REQUEST_PROTOCAL_START_AV_MONITOR = 1101;
	private static final short REQUEST_PROTOCAL_QUIT_AV_MONITOR = 1107;
	private static final short REQUEST_PROTOCAL_GET_AUTH = 1103;
	private static final short REQUEST_PROTOCAL_ROTATE_HEAD = 1105;
	//添加家电
	private static final short REQUEST_PROTOCAL_REQUEST_APPLIANCE_ADD = 1401;
	//删除家电
	private static final short REQUEST_PROTOCAL_REQUEST_APPLIANCE_DEL = 1403;
	//家电同步
	private static final short REQUEST_PROTOCAL_REQUEST_APPLIANCE_SYN = 1407;
	//家电控制
	private static final short REQUEST_PROTOCAL_REQUEST_APPLIANCE_CONTROL = 1405;
	//媒体同步
	private static final short REQUEST_PROTOCAL_REQUEST_MEIDA_SYN = 201;
	//推送wifi
	private static final short REQUEST_PROTOCAL_REQUEST_CHANGE_WIFI = 105;

	private static final short REQUEST_PROTOCAL_REQUEST_CHECK_PHONE_VERSION = 1605;
	//获得机器升级信息
	private static final short REQUEST_PROTOCAL_REQUEST_MACHINE_UPGRADE_DESC = 1603;
	//机器升级
	private static final short REQUEST_PROTOCAL_REQUEST_MACHINE_UPDATE = 1601;


	//音乐升级
	private static final short REQUEST_PROTOCAL_REQUEST_MUSIC = 801;
	//故事升级
	private static final short REQUEST_PROTOCAL_REQUEST_STORY = 701;

	// ===========================================================
	// Static Fields
	// ===========================================================

	// ===========================================================
	// Fields
	// ===========================================================


	// ===========================================================
	// Constructors
	// ===========================================================

	private GowildTCPManager() {
	}

	// ===========================================================
	// Getter or Setter
	// ===========================================================


	// ===========================================================
	// Methods for/from SuperClass/Interfaces
	// ===========================================================


	// ===========================================================
	// Methods
	// ===========================================================

	public static final void requestLogin(final String token) throws NotConnectedException {
//		if (NettyClientManager.getInstance() != null && NettyClientManager.getInstance().mConnection != null && NettyClientManager.getInstance().mConnection.isConnected()) {
//			NettyClientManager.getInstance().close();
//		}
		LoginPro.LoginForAppCMsg.Builder builder = LoginPro.LoginForAppCMsg.newBuilder();
		builder.setToken(token);
		final Message msg = Message.createMessage(TYPE, REQUEST_PROTOCAL_REQUEST_LOGIN);
		msg.setBody(builder.build().toByteArray());
		NettyClientManager.getInstance().send(msg);
	}

	public static final void requestStartAVMonitor() throws NotConnectedException {
		Message msg = Message.createMessage(TYPE, REQUEST_PROTOCAL_START_AV_MONITOR);
		NettyClientManager.getInstance().send(msg);
	}

	public static final void requestQuitAVMonitor() throws NotConnectedException {
		Message msg = Message.createMessage(TYPE, REQUEST_PROTOCAL_QUIT_AV_MONITOR);
		NettyClientManager.getInstance().send(msg);
	}

	public static final void requestConnectionInfo() throws NotConnectedException {
		Message msg = Message.createMessage(TYPE, REQUEST_PROTOCAL_GET_AUTH);
		NettyClientManager.getInstance().send(msg);
	}

	public static final void requestRotateHead(int angle) throws NotConnectedException {
		BaseBothMsgProto.IntMsg.Builder builder = BaseBothMsgProto.IntMsg.newBuilder();
		builder.setValue(angle);
		Message msg = Message.createMessage(TYPE, REQUEST_PROTOCAL_ROTATE_HEAD);
		msg.setBody(builder.build().toByteArray());
		NettyClientManager.getInstance().send(msg);
	}

	/**
	 * 电器添加
	 *
	 * @param json
	 * @throws NotConnectedException
	 */
	public static final void requestApplianceAdd(String json) throws NotConnectedException {
		StringMsgPro.StringMsg.Builder builder = StringMsgPro.StringMsg.newBuilder();
		builder.setValue(json);
		Message msg = Message.createMessage(TYPE, REQUEST_PROTOCAL_REQUEST_APPLIANCE_ADD);
		msg.setBody(builder.build().toByteArray());
		NettyClientManager.getInstance().send(msg);
		Logger.d(TAG, "send()"+json);
	}

	/**
	 * 电器删除
	 *
	 * @param deviceType 设备类型
	 * @param sceneID 场景ID
	 * @throws NotConnectedException
	 */
	public static final void requestApplianceDel(int deviceType,int sceneID) throws NotConnectedException {

		ElectricInfoDeletePro.ElectricInfoDeleteMsg.Builder builder = ElectricInfoDeletePro.ElectricInfoDeleteMsg.newBuilder();
		builder.setDeviceType(deviceType);
		builder.setSceneID(sceneID);
		Message msg = Message.createMessage(TYPE, REQUEST_PROTOCAL_REQUEST_APPLIANCE_DEL);
		msg.setBody(builder.build().toByteArray());
		NettyClientManager.getInstance().send(msg);
	}

	/**
	 * 电器删除
	 *
	 * @param json
	 * @throws NotConnectedException
	 */
	public static final void requestApplianceDel(String json) throws NotConnectedException {
		StringMsgPro.StringMsg.Builder builder = StringMsgPro.StringMsg.newBuilder();
		builder.setValue(json);
		Message msg = Message.createMessage(TYPE, REQUEST_PROTOCAL_REQUEST_APPLIANCE_DEL);
		msg.setBody(builder.build().toByteArray());
		NettyClientManager.getInstance().send(msg);
	}

	/**
	 * 电器同步
	 *
	 * @throws NotConnectedException
	 */
	public static final void requestApplianceSyn() throws NotConnectedException {
		Message msg = Message.createMessage(TYPE, REQUEST_PROTOCAL_REQUEST_APPLIANCE_SYN);
		NettyClientManager.getInstance().send(msg);
	}

	/**
	 * 电器控制
	 *
	 * @param json
	 * @throws NotConnectedException
	 */
	public static final void requestApplianceControl(String json) throws NotConnectedException {
		StringMsgPro.StringMsg.Builder builder = StringMsgPro.StringMsg.newBuilder();
		builder.setValue(json);
		Message msg = Message.createMessage(TYPE, REQUEST_PROTOCAL_REQUEST_APPLIANCE_CONTROL);
		msg.setBody(builder.build().toByteArray());
		NettyClientManager.getInstance().send(msg);
	}

	/**
	 * 媒体同步
	 */
	public static final void requestRobotRequestSync() throws NotConnectedException {
		Message msg = Message.createMessage(TYPE, REQUEST_PROTOCAL_REQUEST_MEIDA_SYN);
		NettyClientManager.getInstance().send(msg);
	}

	/**
	 * 推送WIFI信息
	 * @param ssid
	 * @param pwd
	 * @throws NotConnectedException
     */
	public static final void requestChangeWifiConfig(String ssid,String pwd)  throws NotConnectedException {
		WifiInfoPro.MiscWifiInfoMsg.Builder builder = WifiInfoPro.MiscWifiInfoMsg.newBuilder();
		builder.setWifi(ssid);
		builder.setWifipass(pwd);
		Message msg = Message.createMessage(TYPE,REQUEST_PROTOCAL_REQUEST_CHANGE_WIFI);
		msg.setBody(builder.build().toByteArray());
		NettyClientManager.getInstance().send(msg);

	}

	/**
	 * 获得APP版本信息
	 * @param version
	 * @throws NotConnectedException
     */
	public static final void requestPhoneVersion(String version)  throws NotConnectedException {
		PhoneCheckVersionPro.PhoneCheckVersionMsg.Builder builder = PhoneCheckVersionPro.PhoneCheckVersionMsg.newBuilder();
		builder.setCurrentVersion(version);
		builder.setAppType(1);//Android
		Message msg = Message.createMessage(TYPE,REQUEST_PROTOCAL_REQUEST_CHECK_PHONE_VERSION);
		msg.setBody(builder.build().toByteArray());
		NettyClientManager.getInstance().send(msg);
	}

	/**
	 * 获得机器版本信息
	 * @throws NotConnectedException
	 */
	public static final void requestMachineUpgradeDesc()  throws NotConnectedException {
		Message msg = Message.createMessage(TYPE,REQUEST_PROTOCAL_REQUEST_MACHINE_UPGRADE_DESC);
		NettyClientManager.getInstance().send(msg);
	}

	public static final void requestMachineUpdate()  throws NotConnectedException {
		Message msg = Message.createMessage(TYPE,REQUEST_PROTOCAL_REQUEST_MACHINE_UPDATE);
		NettyClientManager.getInstance().send(msg);
	}

	public static void requestNewMusicSync() throws NotConnectedException {
		Message msg = Message.createMessage(TYPE, REQUEST_PROTOCAL_REQUEST_MUSIC);
		NettyClientManager.getInstance().send(msg);
	}

	public static void requestNewStorySync() throws NotConnectedException {
		Message msg = Message.createMessage(TYPE, REQUEST_PROTOCAL_REQUEST_STORY);
		NettyClientManager.getInstance().send(msg);
	}

	// ===========================================================
	// Inner and Anonymous Classes
	// ===========================================================

}
