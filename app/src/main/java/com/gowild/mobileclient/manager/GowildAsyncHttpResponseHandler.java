/**
 * Copyright (C)  2016 深圳市狗尾草智能科技有限公司
 * GowildXB
 * GowildAsyncHttpResponseHandler.java
 */
package com.gowild.mobileclient.manager;

import android.content.Context;
import android.util.Log;

import com.alibaba.fastjson.JSON;
import com.gowild.mobile.libcommon.utils.Logger;
import com.gowild.mobile.libcommon.utils.ToastUtils;
import com.gowild.mobileclient.R;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.apache.http.Header;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;


/**
 * @author 王揆
 * @version 1.0
 *          <p><strong>Features draft description.对json格式的数据返回做了一层过滤，减少自定义的数据类层级</strong></p>
 * @since 2016/7/27 16:54
 */
public abstract class GowildAsyncHttpResponseHandler<T> extends AsyncHttpResponseHandler {
    private static final String TAG = GowildAsyncHttpResponseHandler.class.getSimpleName();

    private static final int CODE_ERROR_TOKEN_OUTTIME = 401;
    private static final int CODE_ERROR_NEED_LUNCH = 400;
    // ===========================================================
    // HostConstants
    // ===========================================================
    private String mUrl = "";
    private Context mContext;
    private boolean bArray;
    public static final int CODE_ERROR_ENCODE = -1000;

    public static final int CODE_OK = 100;
    public static final int CODE_ERROR_LOGIN = 2000;

    private static final String CODE = "code";
    private static final String DESC = "desc";
    private static final String DATA = "data";

    /**
     * 构造方法
     *
     * @param context 用于处理授权过期消息，会被转成ApplicationContext
     */
    public GowildAsyncHttpResponseHandler(Context context) {
        this(context, false);
    }

    /**
     * 构造方法
     *
     * @param context 用于处理授权过期消息，会被转成ApplicationContext
     */
    public GowildAsyncHttpResponseHandler(Context context, boolean bArray) {
        mContext = context.getApplicationContext();
        this.bArray = bArray;
    }


    /**
     * @param url 请求地址 方便打印日志
     * @return
     */
    public GowildAsyncHttpResponseHandler setUrl(String url) {
        mUrl = url;
        return this;
    }


    @Override
    public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
        try {
            String tempStr = new String(responseBody,"UTF-8");
            String data = "";
            String desc = "";
            int code = 0;
            try {
                org.json.JSONObject json = new org.json.JSONObject(tempStr);
                code = json.getInt(CODE);
                if(json.has(DATA)) {
                    data = json.getString(DATA);
                }
                Logger.d(TAG, "mUrl = " + mUrl + "code = " + code + "data = " + data);
                desc = json.getString(DESC);
	            try {
	                data = json.getString(DATA);
	            } catch (Exception e) {
                    Log.e("Exception",e.toString());
	            }

            } catch (Exception e) {
                onFailure(CODE_ERROR_ENCODE, headers, responseBody, null);
                return;
            }
            switch (code) {
	            case CODE_OK:
//                    Log.d(TAG,  "parse time = " +  (System.currentTimeMillis() - time));
                    Type type = null;
		            try {
			            type = ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
		            } catch (Exception e) {

		            }
		            if (type != null) {
			            Class<T> clz = (Class<T>) (type);
			            if (bArray) {
				            onSuccess(code, (ArrayList<T>) JSON.parseArray(data, clz));
			            } else {
				            onSuccess(code, JSON.parseObject(data, clz));
			            }
		            } else {
			            onSuccess(code, (T) null);
		            }
                    break;
                case 2202:
                    //需要重新请求
	                LoginMananger.getInstence().startLunch();
                    //提示网络错误请稍后重试
                    ToastUtils.showCustom(mContext, desc, false);
                    onFailure(code, desc);
                    break;
	            case 2200:
	            case 2201: //系统内部错误
		            ToastUtils.showCustom(mContext, desc, false);
                    errorLogin(code, desc);
		            break;
	            case 2205: //用户名或者密码错误
		            ToastUtils.showCustom(mContext, desc, false);
		            errorLogin(code, desc);
		            break;
                case 2203:
                case 2204:
                case 2206:
                case 2207:
                case 2208:
                case 2209:
                case 2210:
                case 2211:
                case 2212:
                case 2213:
                case 2214:
                case 2215:
                case 2216:
                    //需要重新登录
                    ToastUtils.showCustom(mContext, desc, false);
	                LoginMananger.getInstence().startLunch();
					break;
	            case 2001:
		            ToastUtils.showCustom(mContext, desc, false);
		            errorLogin(code, desc);
		            break;
	            case 2007:
		            ToastUtils.showCustom(mContext, desc, false);
		            errorLogin(code, desc);
		            break;
	            case 2003:
		            ToastUtils.showCustom(mContext, desc, false);
		            errorLogin(code, desc);
		            break;
	            case 2002:
		            ToastUtils.showCustom(mContext, desc, false);
		            errorLogin(code, desc);
		            break;
	            case 201:
		            ToastUtils.showCustom(mContext, desc, false);
		            errorLogin(code, desc);
		            break;
	            case 2004:
		            ToastUtils.showCustom(mContext, desc, false);
		            errorLogin(code, desc);
		            break;
	            case 2005:
		            errorLogin(code, desc);
		            break;
	            case 2006:
		            ToastUtils.showCustom(mContext, desc, false);
		            errorLogin(code, desc);
		            break;
	            case 2008:
		            ToastUtils.showCustom(mContext,desc, false);
		            errorLogin(code, desc);
		            break;
	            case 2009:
		            ToastUtils.showCustom(mContext,desc, false);
		            errorLogin(code, desc);
		            break;
                case 200:
                    ToastUtils.showCustom(mContext, data, false);
                    errorLogin(code, data);
                break;
                case 2501:
                    ToastUtils.showCustom(mContext, desc, false);
                    errorLogin(code, desc);
                break;
                case 2506:
                case 2507:
                    ToastUtils.showCustom(mContext, desc, false);
                    errorLogin(code, desc);
                break;

                default:
                    onFailure(code, desc);
            }
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            onFailure(CODE_ERROR_ENCODE, headers, responseBody, null);
        }
    }



	@Override
    public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
        String message;
        if (responseBody == null) {
            message = "";
        } else {
            try {
                message = new String(responseBody, "UTF-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
                message = new String(responseBody);
            }
        }

        if (statusCode == CODE_ERROR_TOKEN_OUTTIME) {
            //需要刷新token
            ToastUtils.showCustom(mContext, message, false);
            LoginMananger.getInstence().startRefreshToken();
        } else if (statusCode == CODE_ERROR_NEED_LUNCH) {
            ToastUtils.showCustom(mContext,message,false);
            LoginMananger.getInstence().startLunch();
        }

        Logger.v(TAG, mUrl + "code = " + statusCode + " body = " + message);
        Logger.v(TAG, Log.getStackTraceString(error));
        ToastUtils.showCustom(mContext, mContext.getString(R.string.gowild_net_error), false);
//        ToastUtils.showCustom(mContext, "注意http 验证服务器异常" ,false);
        onFailure(statusCode, message);
    }

    public void errorLogin(int statusCode, String responseBody) {
        onFailure(statusCode, responseBody);
    }

    public abstract void onSuccess(int statusCode, T responseBody);

    public void onSuccess(int statusCode, ArrayList<T> responseBody) {
    }

    public abstract void onFailure(int statusCode, String responseBody);
}
