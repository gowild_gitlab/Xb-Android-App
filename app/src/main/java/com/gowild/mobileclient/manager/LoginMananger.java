/**
 * Copyright (C)  2016 深圳市狗尾草智能科技有限公司
 * GowildXB
 * LoginMananger.java
 */
package com.gowild.mobileclient.manager;

import android.content.Context;
import android.content.Intent;

import com.alibaba.fastjson.JSON;
import com.gowild.mobile.libcommon.utils.Logger;
import com.gowild.mobile.libcommon.utils.SpUtils;
import com.gowild.mobile.libcommon.utils.ToastUtils;
import com.gowild.mobile.libp2sp.core.exception.NotConnectedException;
import com.gowild.mobile.libp2sp.core.listener.IDataListener;
import com.gowild.mobile.libp2sp.netty.NettyClientManager;
import com.gowild.mobileclient.R;
import com.gowild.mobileclient.activity.login.GowildLunchActivity;
import com.gowild.mobileclient.application.GowildApplication;
import com.gowild.mobileclient.config.Constants;
import com.gowild.mobileclient.config.IntContants;
import com.gowild.mobileclient.event.BindResult;
import com.gowild.mobileclient.event.LoginResult;
import com.gowild.mobileclient.event.RobotOnline;
import com.gowild.mobileclient.misc.GowildResponseCode;
import com.gowild.mobileclient.protocol.LoginResultPro;
import com.gowild.mobileclient.protocol.RobotBindReslutIntMsgPro;
import com.gowild.mobileclient.protocol.RobotResultPro;
import com.gowild.mobileclient.vo.LunchHeader;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.apache.http.Header;
import org.greenrobot.eventbus.EventBus;

/**
 * @author shuai. wang
 * @version 1.0
 *          <p><strong>Features draft description.主要功能介绍</strong></p>
 * @since 2016/8/19 10:08
 */
public class LoginMananger {

    // ===========================================================
    // HostConstants
    // ===========================================================

    private static final String TAG = LoginMananger.class.getSimpleName();

    private static LoginMananger instence;


    private Context mContext = GowildApplication.context;
    private int type;
    // ===========================================================
    // Static Fields
    // ===========================================================

    // ===========================================================
    // Fields
    // ===========================================================

    // ===========================================================
    // Constructors
    // ===========================================================


    // ===========================================================
    // Getter or Setter
    // ===========================================================


    // ===========================================================
    // Methods for/from SuperClass/Interfaces
    // ===========================================================


    // ===========================================================
    // Methods
    // ===========================================================


    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================

    public void setType(int type) {
        this.type = type;
    }

    public int getType() {
        return type;
    }

    private LoginMananger() {
        addLinsenter();
    }

    private boolean isSettingOpen;

    public void setIsSettingOpen(boolean b) {
        this.isSettingOpen = b;
    }

    public boolean getIsSettingOpen() {
        return isSettingOpen;
    }

    public int getBindFrom() {
        return bindFrom;
    }

    /**
     * 设置是点击了设置哪个选项
     *
     * @param bindFrom
     */
    public void setBindFrom(int bindFrom) {
        this.bindFrom = bindFrom;
    }

    private             int bindFrom      = -1;
    /**
     * 机器设置
     */
    public final static int bindFromClear = -1;
    /**
     * 机器设置
     */
    public final static int bindFromRobot = 10;
    /**
     * 网络设置
     */
    public final static int bindFromNet   = 11;


    private void addLinsenter() {
        NettyClientManager.getInstance().addHandlerListener(GowildResponseCode.NETTY_CONNECT, mConnectListener);
        NettyClientManager.getInstance().addHandlerListener(GowildResponseCode.LOGIN_RESULT, mLoginResultListener);
        NettyClientManager.getInstance().addHandlerListener(GowildResponseCode.ROBOT_BIND_RESULT, mRobotResultListener);
        NettyClientManager.getInstance().addHandlerListener(GowildResponseCode.ROBOT_RESULT, mRobotOnlineListener);
    }

    private RobotResult mRobotOnlineListener = new RobotResult();

    public void startCountPage(Context context, String extra) {
        int count1 = 0;
        int count2 = 0;
        int count3 = 0;
        int count4 = 0;
        if (context != null && extra == null) {
            String name = context.getClass().getSimpleName();
            switch (name) {
                case "GowildSplashActivity": //开始
                    count1 ++;
                    //需要存储
                    SpUtils.putInt(Constants.STATSTICS, GowildApplication.context, "M_R", count1);
                    break;
                case "GowildCaptchaActivity"://获取验证码
                    count2 ++;
                    SpUtils.putInt(Constants.STATSTICS, GowildApplication.context, "M_FP", count2);
                    break;
                case "GowildLunchActivity"://登录
                    count3 ++;
                    SpUtils.putInt(Constants.STATSTICS, GowildApplication.context, "M_L", count3);
                    break;
                case "GowildSettingPwdActivity"://设置密码
                    count4 ++;
                    SpUtils.putInt(Constants.STATSTICS, GowildApplication.context, "M_SP", count4);//这个要在登录之后发送
                    break;
                case "GowildHomeActivity"://主页
                    GowildHttpManager.postStatutas("M_MP",1);
                    break;
                case "GowildSettingActivity"://设置
                    GowildHttpManager.postStatutas("S_P",1);
                    break;
                case "GowildMyInfoActivity"://我的
                    GowildHttpManager.postStatutas("S_I",1);
                    break;
                case "GowildRobotConfigActivity"://机器
                    GowildHttpManager.postStatutas("S_MS",1);
                    break;
                case "GowildChangeWifiActivity"://网络
                    GowildHttpManager.postStatutas("S_NS",1);
                    break;
                case "GowildAboutActivity"://关于
                    GowildHttpManager.postStatutas("S_A",1);
                    break;
                case "GowildRobotInfoActivity"://机器人介绍
                    GowildHttpManager.postStatutas("S_MI",1);
                    break;
                case "GowildFeedbackActivity"://建议
                    GowildHttpManager.postStatutas("S_AD",1);
                    break;
                case "SkillListActivity"://技能包
                    GowildHttpManager.postStatutas("MN_S",1);
                    break;
                case "MusicMainActivity"://音乐播放
                    if (IntContants.isCurrentMusic()) {
                        //音乐播放
                        GowildHttpManager.postStatutas("MC_P",1);
                    } else {
                        //故事播放
                        GowildHttpManager.postStatutas("SY_P",1);
                    }
                    break;
                case "MusicLoverActivity":
                case "MusicStoryNoLoverActivity"://收藏列表
                    if (IntContants.isCurrentMusic()) {
                        //音乐播放
                        GowildHttpManager.postStatutas("MC_C",1);
                    } else {
                        //故事播放
                        GowildHttpManager.postStatutas("SY_C",1);
                    }
                    break;
                case "MusicRecordActivity"://播放记录
                case "MusicStoryNoRecordActivity"://播放记录
                    if (IntContants.isCurrentMusic()) {
                        //音乐播放
                        GowildHttpManager.postStatutas("MC_PR",1);
                    } else {
                        //故事播放
                        GowildHttpManager.postStatutas("SY_PR",1);
                    }
                    break;
                case "MusicSearchActivity"://查找
                    if (IntContants.isCurrentMusic()) {
                        //音乐播放
                        GowildHttpManager.postStatutas("MC_FP",1);
                    } else {
                        //故事播放
                        GowildHttpManager.postStatutas("SY_FP",1);
                    }
                    break;
                case "MusicSingerListActivity"://设置
                    GowildHttpManager.postStatutas("SY_CF",1);
                    break;
                case "MusicSearchResultActivity"://设置
                    GowildHttpManager.postStatutas("MC_CF",1);
                    break;
                case "StoryAlbumActivity"://查找
                    GowildHttpManager.postStatutas("SY_CF",1);
                    break;
                case "StorySearchResultActivity"://设置
                    GowildHttpManager.postStatutas("SY_S",1);
                    break;
                case "GowildApplianceAir"://设置
                    GowildHttpManager.postStatutas("H_P",1);
                    break;
                case "GowildApplianceOptionActivity"://设置
                    GowildHttpManager.postStatutas("H_C",1);
                    break;
                case "GowildApplianceBrandActivity"://设置
                    GowildHttpManager.postStatutas("H_A",1);
                    break;
                case "GowildApplianceListActivity"://设置
                    GowildHttpManager.postStatutas("H_SB",1);
                    break;
                case "GowildApplianceSearchActivity"://设置
                    GowildHttpManager.postStatutas("H_M",1);
                    break;
                case "GowildAVMonitorActivity"://设置
                    GowildHttpManager.postStatutas("P_P",1);
                    break;
                case "PhotoDetailActivity"://照片功能
                    GowildHttpManager.postStatutas("P_PO",1);
//                    P_C
                    break;
                case "GowildAddOrUpdateAlertActivity"://设置
                    GowildHttpManager.postStatutas("A_A",1);
                    break;
                case "GowildAddOrUpdateClockActivity"://设置
                    GowildHttpManager.postStatutas("R_A",1);
                    break;
                case "GowildLoverActivity"://设置
                    GowildHttpManager.postStatutas("F_R",1);
                    break;
                case "GowildContantsActivity"://设置
                    GowildHttpManager.postStatutas("F_M",1);
                    break;
                case "GowildLoveTalkActivity"://设置
                    GowildHttpManager.postStatutas("F_S",1);
                    break;
                case "GowildLoverInfoActivity"://设置
                    GowildHttpManager.postStatutas("F_FI",1);
                    break;
            }
        }

        if (context == null && extra != null)
            switch (extra) {
                case "company": //公司介绍
                    GowildHttpManager.postStatutas("S_CI",1);
                    break;
                case "server": //服务
                    GowildHttpManager.postStatutas("S_SI",1);
                    break;
                case "growtask": //成长任务
                    GowildHttpManager.postStatutas("MN_SM",1);
                    break;
                case "dailytask": //日常任务
                    GowildHttpManager.postStatutas("MN_DM",1);
                    break;
                case "honorachievement": //成就勋章
                    GowildHttpManager.postStatutas("MN_A",1);
                    break;
                case "photolist": //财富排行
                    GowildHttpManager.postStatutas("P_PO",1);
                    break;
                case "videolist": //财富排行
                    GowildHttpManager.postStatutas("P_M",1);
                    break;
                case "alertlist": //
                    GowildHttpManager.postStatutas("A_P",1);
                    break;
                case "clocklist": //
                    GowildHttpManager.postStatutas("R_P",1);
                    break;
            }
    }


    /**
     * 机器人是否在线
     */
    private class RobotResult implements IDataListener<RobotResultPro.BoolMsg> {

        @Override
        public void onReceiveData(final RobotResultPro.BoolMsg data) {
            Logger.d(TAG, "onReceiveData() data = " + data.getValue());//消息可以收到,优先处理机器人在线
            //不知道机器怎么想的,没绑定还发208?
            if (type == 1 || type == 2) {
                setType(data.getValue() ? IntContants.TYPE_ROBOT_SUCESS : IntContants.TYPE_ROBOT_OUTLINE);
                EventBus.getDefault().post(new RobotOnline(data.getValue()));
            }
        }

        @Override
        public void onActiveChanged(boolean isActive) {

        }
    }


    private LoginResultListener mLoginResultListener = new LoginResultListener();

    /**
     * 登录结果
     */
    private class LoginResultListener implements IDataListener<LoginResultPro.LoginForAppSMsg> {

        @Override
        public void onReceiveData(final LoginResultPro.LoginForAppSMsg data) {
            Logger.d(TAG, "onReceiveData() data = " + data.getResult());
            type = data.getResult();
            EventBus.getDefault().post(new LoginResult(data.getResult()));
            ((GowildApplication) GowildApplication.context).sendHeartBeat();
            NettyClientManager.getInstance().startHeartBeat();
            //登录成功
            //发送统计数据
//            GowildHttpManager.postStatutas("M_R", SpUtils.getInt(HostConstants.STATSTICS, GowildApplication.context, "M_R"));
//            GowildHttpManager.postStatutas("M_FP", SpUtils.getInt(HostConstants.STATSTICS, GowildApplication.context, "M_FP"));
//            GowildHttpManager.postStatutas("M_L", SpUtils.getInt(HostConstants.STATSTICS, GowildApplication.context, "M_L"));
//            GowildHttpManager.postStatutas("M_SP", SpUtils.getInt(HostConstants.STATSTICS, GowildApplication.context, "M_SP"));
        }

        @Override
        public void onActiveChanged(boolean isActive) {
        }
    }


    private RobotResultListener mRobotResultListener = new RobotResultListener();

    /**
     * 绑定结果
     */
    private class RobotResultListener implements IDataListener<RobotBindReslutIntMsgPro.IntMsg> {
        @Override
        public void onReceiveData(RobotBindReslutIntMsgPro.IntMsg data) {
            int value = data.getValue();
            if (value == 1) {
                type = value;
                EventBus.getDefault().post(new BindResult(data.getValue()));
            }
            Logger.d(TAG, value + "");
        }

        @Override
        public void onActiveChanged(boolean isActive) {

        }
    }


    public static LoginMananger getInstence() {
        if (instence == null) {
            synchronized (LoginMananger.class) {
                if (instence == null) {
                    instence = new LoginMananger();
                }
            }
        }
        return instence;
    }

    /**
     * 登录TCP
     */
    public void TCPLoginRequest() {
        isFristIn = true;
        ((GowildApplication) GowildApplication.context).stopHeartBeat();
        if (!NettyClientManager.getInstance().isConnected()) {
            NettyClientManager.getInstance().connect();
        }
//        else {
//            NettyClientManager.getInstance().close();
//        }
    }


    /**
     * 停止TCP
     */
    public void stopTCP() {
        ((GowildApplication) GowildApplication.context).stopHeartBeat();
        NettyClientManager.getInstance().stopHeartBeat();
        NettyClientManager.getInstance().close();
    }


    private ConnectListener mConnectListener = new ConnectListener();

    boolean isFristIn = true;

    /**
     * 连接的回调
     */
    private class ConnectListener implements IDataListener<String> {
        @Override
        public void onReceiveData(String data) {

        }

        @Override
        public void onActiveChanged(boolean isActive) {
            if (isActive) {
                try {
                    if (isFristIn) {
                        GowildTCPManager.requestLogin(SpUtils.getString(GowildApplication.context, Constants.ACCESS_TOKEN_NOBEAR));
                        isFristIn = false;
                    }
                } catch (NotConnectedException e) {

                    e.printStackTrace();
                }
            } else {
                if (isFristIn) {
                    NettyClientManager.getInstance().connect();
                }
                if (!isFristIn) {
                    NettyClientManager.getInstance().stopHeartBeat();
                    NettyClientManager.getInstance().close();
                }
            }
        }

    }

    /**
     * 刷新token
     */
    public void startRefreshToken() {
        //
        Logger.v(TAG, "需要刷新token");
        AsyncHttpClient client = new AsyncHttpClient();
        client.addHeader(Constants.AUTHORIZATION, Constants.AUTHORIZATION_DESC);
        RequestParams params = new RequestParams();
        params.add(Constants.REFRESH_TOKEN, SpUtils.getString(mContext, Constants.REFRESH_TOKEN));
        params.add("grant_type", "refresh_token");
        client.post(mContext, GowildHttpManager.getTokenURL(GowildHttpManager.URL_LUNCH), params, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                String      bean   = new String(responseBody);
                LunchHeader header = JSON.parseObject(bean, LunchHeader.class);
                if (header.code == 100) {
                    //保存token
                    SpUtils.putString(mContext, Constants.ACCESS_TOKEN, "Bearer " + header.data.access_token);
                    //保存refresh_token
                    SpUtils.putString(mContext, Constants.REFRESH_TOKEN, header.data.refresh_token);
                    //跳
                    SpUtils.putString(mContext, Constants.ACCESS_TOKEN_NOBEAR, header.data.access_token);
                    //发送
                    LoginMananger.getInstence().TCPLoginRequest();
                } else {
                    ToastUtils.showCustom(GowildApplication.context, "连接服务器失败,请重新登录", false);
                    startLunch();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                ToastUtils.showCustom(mContext, mContext.getString(R.string.gowild_net_error), false);
            }
        });
    }

    /**
     * 重新登录
     */
    public void startLunch() {
        Logger.v(TAG, "需要重新登录");
        GowildApplication.mHandler.post(new Runnable() {
            @Override
            public void run() {
                ((GowildApplication) GowildApplication.context).stopHeartBeat();
                NettyClientManager.getInstance().stopHeartBeat();
                NettyClientManager.getInstance().close();
                Intent intent = new Intent(mContext, GowildLunchActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra(Constants.NEED_LUNCH, Constants.NEED_LUNCH);
                EventBus.getDefault().post("finish");
                mContext.startActivity(intent);
            }
        });
    }

}




