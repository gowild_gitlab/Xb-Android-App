package com.gowild.mobileclient.manager;

import android.content.Context;
import android.text.TextUtils;

import com.gowild.mobile.libcommon.utils.SpUtils;
import com.gowild.mobile.libhttp.HttpUtil;
import com.gowild.mobile.libhttp.ObjectAsynHttpResponseHandler;
import com.gowild.mobile.libp2sp.HostConstants;
import com.gowild.mobileclient.application.GowildApplication;
import com.gowild.mobileclient.config.Constants;
import com.gowild.mobileclient.test.TestConstants;
import com.gowild.mobileclient.vo.DaliyTask;
import com.gowild.mobileclient.vo.RequestAchievement;
import com.gowild.mobileclient.vo.RequestAlertList;
import com.gowild.mobileclient.vo.RequestClockList;
import com.gowild.mobileclient.vo.RequestCreateAlert;
import com.gowild.mobileclient.vo.RequestCreateClock;
import com.gowild.mobileclient.vo.RequestDeleteAlert;
import com.gowild.mobileclient.vo.RequestDeleteClock;
import com.gowild.mobileclient.vo.RequestFeedback;
import com.gowild.mobileclient.vo.RequestInfoGet;
import com.gowild.mobileclient.vo.RequestInfoUpdate;
import com.gowild.mobileclient.vo.RequestModifyPwd;
import com.gowild.mobileclient.vo.RequestRevokeToken;
import com.gowild.mobileclient.vo.RequestSinger;
import com.gowild.mobileclient.vo.RequestSingerSortList;
import com.gowild.mobileclient.vo.RequestStoryHistory;
import com.gowild.mobileclient.vo.RequestStorySort;
import com.gowild.mobileclient.vo.RequestUpdateAlert;
import com.gowild.mobileclient.vo.RequestUpdateClock;
import com.gowild.mobileclient.vo.RequestWealthMine;
import com.gowild.mobileclient.vo.RequestWealthRank;
import com.gowild.mobileclient.vo.WealthMine;
import com.gowild.mobileclient.vo.WealthRankPageInfo;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.apache.http.Header;
import org.apache.http.message.BasicHeader;


/**
 * Created by WangJunDong on 2016/6/1.
 */
@SuppressWarnings("unused")
public class GowildHttpManager {

    private static final String TAG = GowildHttpManager.class.getSimpleName();

    private static final String CONTENT_TYPE = "application/json;charset=utf-8";
    private static final String AUTHORIZATION = "Authorization";

    private static final String PAGENUM = "pageNum";
    private static final String PAGESIZE = "pageSize";
    public static final int DEFAULT_PAGE_NUM = 1;
    public static final int DEFAULT_PAGE_SIZE = 10;
    public static final int PHOTO_PAGE_SIZE = 30;
    private static final String BASE_URL_TCP = "xbweb.gowildweb.com";
    private static final String BASE_URL_TCP_TEST = "172.26.1.165";
    private static final int BASE_TCP_PORT_TEST = 6030;
    private static final int BASE_URL_PORT = 6030;
    //1：测试环境（内网）
    //默认正式环境
    public static int sWebEnvirmonent;
  /*  //正式环境
    //    private static final String BASE_URL_FORMAL = "http://xbweb.gowildweb.com:6130/web/rest";
    //    private static final String TOKEN_BASE_URL = "http://xbweb.gowildweb.com:6110/passport";

    //测试环境
    //        private static final String BASE_URL_FORMAL = "http://172.26.1.165:9001/web/rest";
    //        private static final String TOKEN_BASE_URL = "http://172.26.1.165:9000/passport";

    //预发布01 古老版本
    //        private static final String BASE_URL_FORMAL = "http://xb.gowildtech.com:6130/web/rest";
    //        private static final String TOKEN_BASE_URL = "http://xb.gowildtech.com:6110/passport";

    //    预发布
    private static final String BASE_URL_FORMAL = "http://uatxb.gowildweb.com:6130/web/rest";
    private static final String TOKEN_BASE_URL = "http://uatxb.gowildweb.com:6110/passport";*/

    private static final String BASE_URL_TEST = "http://172.26.1.165:9001/web/rest";
    private static final String BASE_URL_TOKEN_TEST = "http://172.26.1.165:9000/passport";
    public static final String URL_LUNCH = "/oauth/token";
    private static final String URL_ACHIEVEMENT = "/pet/getAccountPetAchieve";

    private static final String URL_WEALTH_MINE = "/pet/getPetAccountRank";

    private static final String URL_WEALTH_RANK = "/pet/getPetRank";

    private static final String PATH_REQUEST_SINGER_SORT = "/music/getMusicClassify";

    private static final String PATH_REQUEST_STORY_CATEGORY = "/story/getAllStoryCategory";

    private static final String PATH_REQUEST_STORY_SEARCH_RESULT = "/story/queryAlbum";

    private static final String PATH_REQUEST_SINGER_BY_PINYIN = "/music/getMusicClassifySinger";

    private static final String PATH_REQUEST_MUSIC_FROM_SINGERLIST_SEARCH_RESULT = "/music/queryMusicSinger";

    private static final String PATH_REQUEST_SEARCH_RESULT = "/music/queryMusic";

    private static final String PATH_REQUEST_MUSIC_RECORD = "/music/getMusicHistory";

    private static final String PATH_REQUEST_MUSIC_COLLECT = "/music/getMusicCollect";

    private static final String URL_FEEDBACK = "/suggest/saveSuggest";

    private static final String URL_UPDATE_USER = "/account/modifyUserInfo";

    //	http://172.19.1.166:9001/web/rest/relationship/request
    private static final String URL_CANPETINFO = "/pet/getPetHome";

    private static final String URL_GET_USER = "/account/getUserInfo";

    private static final String URL_MODIFY_PWD = "/account/updatePassword";

    private static final String URL_REVOKE_TOKEN = HostConstants.TOKEN_BASE_URL + "/oauth/revoketoken";

    //发送验证码
    public static final String URL_CAPTCHA = HostConstants.BASE_URL_FORMAL + "/account/getCaptcha";//

    //检校验证码
    public static final String URL_CHECK_CAPTCHA = "/account/validCaptcha";
    //情侣部分
    public static final String URL_LOVER_LIST = "/relationship/waitlist";
    //	http://172.19.1.166:9001/web/rest/relationship/request
    public static final String URL_REQUEST_LOVER = "/relationship/request";
    public static final String URL_REQUEST_BIND_LOVER = "/relationship/lover";
    public static final String URL_REQUEST_UNBIND_LOVER = "/relationship/bye";

    public static final String URL_SETTINGPWD = "/account/forgetPassword";
    //	/rest/account/forgetPassword

    private static final String PATH_REQUEST_ALERT_LIST = "/remind/query";

    private static final String PATH_CREATE_ALERT = "/remind/create";

    private static final String PATH_UPDATE_ALERT = "/remind/modify";

    private static final String PATH_DELETE_ALERT = "/remind/delete";

    private static final String PATH_REQUEST_CLOCK_LIST = "/alarm/query";

    private static final String PATH_CREATE_CLOCK = "/alarm/create";
    private static final String PATH_UPDATE_CLOCK = "/alarm/modify";
    private static final String URL_LOVER_RELATIONSHIP = "/relationship/lover";

    private static final String PATH_DELETE_CLOCK = "/alarm/delete";

    private static final String PATH_ADD_STORY_TO_COLLECT = "/story/addStoryAlbumToCollect";
    private static final String PATH_DEL_STORY_TO_COLLECT = "/story/deleteStoryAlbumToCollect";
    private static final String PATH_GET_STORY_HISTORY = "/story/getHistoryStoryAlbum";
    private static final String PATH_GET_STORY_COLLECT = "/story/getCollectStoryAlbum";
    private static final String PATH_GET_STORTALBUM_BY_CATEGORY_ID = "/story/getStoryAlbumByCategoryId";

    private static final String URL_LOVER_ACCEPT = "/relationship/accept";
    private static final String URL_LOVER_INFO = "/relationship/findOtherInfo";
    private static final String URL_QUREY_HOME = "/pet/getPetHome";
    private static final String URL_CANCLELOVER = "/relationship/lover";

    private static final String PATH_REQUEST_SKILL_LIST = "/skill/list";
    private static final String PATH_REQUEST_PHOTO_LIST = "/photo/list";
    private static final String PATH_DELETE_PHOTO = "/photo/delete";
    private static final String PATH_ADD_MUSIC_TO_COLLECT = "/music/addMusicToCollect";
    private static final String PATH_DEL_MUSIC_TO_COLLECT = "/music/delMusicToCollect";
    private static final String PATH_GET_MUSIC_HISTORY = "/music/getMusicHistory";
    private static final String PATH_GET_MUSIC_COLLECT = "/music/getMusicCollect";
    private static final String PATH_GET_BIND_INFO = "/robot/getBindInfo";

    private static final String PATH_UPDATE_TALK_SPEED_OR_VOLUME = "/robot/updateTalkSpeedOrVolume";

    private static final String PATH_UNBIND_ROBOT = "/robot/unbind";
    private static final String URL_GROW_TASK = "/pet/getAccountPetAchieve";


    public static final String URL_CONSTACTS_SENDMESSAGE = "/relationship/sendmsg";
    public static final String URL_TALKMSG = "/relationship/msglist";
    public static final String URL_TASKMISSION = "/pet/queryPetMission";
    public static final String PATH_REQUEST_STORY_CHAPTER_FROM_STORY_ALBUM_LIST = "/story/getStoryByAlbumId";
    public static final String PATH_REQUEST_STORY_ALBUM_FROM_STORY_KEYWORD = "/story/queryAlbum";
    public static final String PATH_CHANGE_MUSIC = "/music/changePlay";
    public static final String PATH_RESUME_MUSIC = "/music/resumeMusic";
    public static final String PATH_CHANGE_STORY = "/story/changePlay";

    public static final String URL_REGIEST_PWD = "/account/register";
    private static final String URL_REQUEST_DAILY_TASK = "/pet/getAccountPetMission";
    private static final String URL_REQUEST_MACHINE_TIME = "/robot/getRobotSettings";
    private static final String URL_REQUEST_UPDATE_TIME = "/robot/updateRobotSettings";
    private static final String URL_REQUEST_NET = "/robot/updateNetworkMode";


    private GowildHttpManager() {

    }


    /**
     * 请求聊天
     *
     * @param context
     * @param callback
     */
    public static final void getTalkMsg(Context context, RequestParams params, GowildAsyncHttpResponseHandler callback) {
        HttpUtil.getInstance().post(context, getUrl(URL_TALKMSG), getHeads(context), params, callback.setUrl(getUrl(URL_TALKMSG)));
    }


    /**
     * 请求配对申请
     *
     * @param context
     * @param callback
     */
    public static final void requestLoverRequest(Context context, ObjectAsynHttpResponseHandler callback) {
        HttpUtil.getInstance().post(context, getUrl(URL_LOVER_LIST), getHeads(context), null, callback);
    }

    /**
     * 获得情侣信息
     *
     * @param context
     * @param callback
     */
    public static final void requestLover(Context context, ObjectAsynHttpResponseHandler callback) {
        HttpUtil.getInstance().post(context, getUrl(URL_LOVER_LIST), getHeads(context), null, callback);
    }


    /**
     * 获得对方的消息
     *
     * @param context
     * @param callback
     */
    public static final void requestLoverInfo(Context context, RequestParams param, ObjectAsynHttpResponseHandler callback) {
        HttpUtil.getInstance().post(context, getUrl(URL_LOVER_INFO), getHeads(context), param, callback);
    }

    /**
     * 建立配对
     *
     * @param context
     * @param callback
     */
    public static final void requestAcceptLover(Context context, RequestParams param, GowildAsyncHttpResponseHandler callback) {

        HttpUtil.getInstance().post(context, getUrl(URL_LOVER_ACCEPT), getHeads(context), param, callback.setUrl(getUrl(URL_LOVER_ACCEPT)));
    }

    /**
     * 获得当前情侣信息
     *
     * @param context
     * @param callback
     */
    public static final void requestLover(Context context, GowildAsyncHttpResponseHandler callback) {
        HttpUtil.getInstance().post(context, getUrl(URL_LOVER_RELATIONSHIP), getHeads(context), null, callback.setUrl(getUrl(URL_LOVER_RELATIONSHIP)));
    }

    /**
     * 获得对方的消息
     *
     * @param context
     * @param callback
     */
    public static final void requestLoverInfo(Context context, GowildAsyncHttpResponseHandler callback) {
        HttpUtil.getInstance().post(context, getUrl(URL_LOVER_INFO), getHeads(context), null, callback.setUrl(getUrl(URL_LOVER_INFO)));
    }

    /**
     * 宠物首页
     *
     * @param context
     * @param callback
     */
    public static final void requestPetInfo(Context context, GowildAsyncHttpResponseHandler callback) {
        HttpUtil.getInstance().post(context, getUrl(URL_QUREY_HOME), getHeads(context), null, callback.setUrl(getUrl(URL_QUREY_HOME)));
    }

    /**
     * 解除配对
     *
     * @param context
     * @param callback
     */
    public static final void getInfo(Context context, GowildAsyncHttpResponseHandler callback) {
        HttpUtil.getInstance().post(context, getUrl(URL_CANCLELOVER), getHeads(context), null, callback.setUrl(getUrl(URL_CANCLELOVER)));
    }


    /**
     * 请求提醒列表
     *
     * @param context
     * @param req
     * @param callback
     */
    public static final void requestAlertList(Context context, RequestAlertList req, GowildAsyncHttpResponseHandler callback) {
        HttpUtil.getInstance().post(context, getUrl(PATH_REQUEST_ALERT_LIST), getHeads(context), req.getRequestParams(), callback.setUrl(getUrl(PATH_REQUEST_ALERT_LIST)));
    }

    /**
     * 请求闹钟列表
     *
     * @param context
     * @param req
     * @param callback
     */
    public static final void requestClockList(Context context, RequestClockList req, GowildAsyncHttpResponseHandler callback) {
        HttpUtil.getInstance().post(context, getUrl(PATH_REQUEST_CLOCK_LIST), getHeads(context), req.getRequestParams(), callback.setUrl(getUrl(PATH_REQUEST_CLOCK_LIST)));
    }

    /**
     * 请求创建提醒
     *
     * @param context
     * @param req
     * @param callback
     */
    public static final void requestCreateAlert(Context context, RequestCreateAlert req, GowildAsyncHttpResponseHandler callback) {
        HttpUtil.getInstance().post(context, getUrl(PATH_CREATE_ALERT), getHeads(context), req.getRequestParams(), callback.setUrl(getUrl(PATH_CREATE_ALERT)));
    }

    /**
     * 请求创建闹钟
     *
     * @param context
     * @param req
     * @param callback
     */
    public static final void requestCreateClock(Context context, RequestCreateClock req, GowildAsyncHttpResponseHandler callback) {
        HttpUtil.getInstance().post(context, getUrl(PATH_CREATE_CLOCK), getHeads(context), req.getRequestParams(), callback.setUrl(getUrl(PATH_CREATE_CLOCK)));
    }

    /**
     * 请求更新提醒
     *
     * @param context
     * @param req
     * @param callback
     */
    public static final void requestUpdateAlert(Context context, RequestUpdateAlert req, GowildAsyncHttpResponseHandler callback) {
        HttpUtil.getInstance().post(context, getUrl(PATH_UPDATE_ALERT), getHeads(context), req.getRequestParams(), callback.setUrl(getUrl(PATH_UPDATE_ALERT)));
    }

    /**
     * 请求更新闹钟
     *
     * @param context
     * @param req
     * @param callback
     */
    public static final void requestUpdateClock(Context context, RequestUpdateClock req, GowildAsyncHttpResponseHandler callback) {
        HttpUtil.getInstance().post(context, getUrl(PATH_UPDATE_CLOCK), getHeads(context), req.getRequestParams(), callback.setUrl(getUrl(PATH_UPDATE_CLOCK)));
    }

    /**
     * 请求删除提醒
     *
     * @param context
     * @param req
     * @param callback
     */
    public static final void requestDeleteAlert(Context context, RequestDeleteAlert req, GowildAsyncHttpResponseHandler callback) {
        HttpUtil.getInstance().post(context, getUrl(PATH_DELETE_ALERT), getHeads(context), req.getRequestParams(), callback.setUrl(getUrl(PATH_DELETE_ALERT)));
    }

    /**
     * 请求删除闹钟
     *
     * @param context
     * @param req
     * @param callback
     */
    public static final void requestDeleteClock(Context context, RequestDeleteClock req, GowildAsyncHttpResponseHandler callback) {
        HttpUtil.getInstance().post(context, getUrl(PATH_DELETE_CLOCK), getHeads(context), req.getRequestParams(), callback.setUrl(getUrl(PATH_DELETE_CLOCK)));
    }

    /**
     * 请求歌手类型
     *
     * @param context
     * @param req
     * @param callback
     */
    public static final void requestSingerSort(Context context, RequestSingerSortList req, GowildAsyncHttpResponseHandler callback) {
        HttpUtil.getInstance().post(context, getUrl(PATH_REQUEST_SINGER_SORT), getHeads(context), req.getRequestParams(), callback.setUrl(getUrl(PATH_REQUEST_SINGER_SORT)));
    }

    /**
     * 请求歌手列表
     *
     * @param context
     * @param req
     * @param callback
     */
    public static final void requestSingerListByPy(Context context, RequestSinger req, GowildAsyncHttpResponseHandler callback) {
        HttpUtil.getInstance().post(context, getUrl(PATH_REQUEST_SINGER_BY_PINYIN), getHeads(context), req.getRequestParams(), callback.setUrl(getUrl(PATH_REQUEST_SINGER_BY_PINYIN)));
    }


    /**
     * 成长任务
     */
    public static final void getGrowTask(Context context, RequestParams params, GowildAsyncHttpResponseHandler callback) {
        HttpUtil.getInstance().post(context, getUrl(URL_GROW_TASK), getHeads(context), params, callback.setUrl(getUrl(URL_GROW_TASK)));
    }

    /**
     * 请求音乐结果通过歌手列表
     *
     * @param context
     * @param callback
     */
    public static final void requestMusicFromSingerListSearchResult(Context context, String key, int pageNum, GowildAsyncHttpResponseHandler callback) {
        String url = getUrl(PATH_REQUEST_MUSIC_FROM_SINGERLIST_SEARCH_RESULT);
        RequestParams params = new RequestParams(Constants.SEARCH_KEY, key,
                PAGENUM, pageNum, PAGESIZE, DEFAULT_PAGE_SIZE);
        HttpUtil.getInstance().post(context, url, getHeads(context), params, callback.setUrl(url));
    }

    /**
     * 请求音乐结果通过关键字
     *
     * @param context
     * @param callback
     */
    public static final void requestMusicFromKeyWordSearchResult(Context context, String key, int pageNum, GowildAsyncHttpResponseHandler callback) {
        String url = getUrl(PATH_REQUEST_SEARCH_RESULT);
        RequestParams params = new RequestParams(Constants.SEARCH_KEY, key,
                PAGENUM, pageNum, PAGESIZE, DEFAULT_PAGE_SIZE);
        HttpUtil.getInstance().post(context, url, getHeads(context), params, callback.setUrl(url));
    }


    /**
     * 请求歌手类型
     *
     * @param context
     * @param req
     * @param callback
     */
    public static final void requestStoryCategorySort(Context context, RequestSingerSortList req, GowildAsyncHttpResponseHandler callback) {
        HttpUtil.getInstance().post(context, getUrl(PATH_REQUEST_SINGER_SORT), getHeads(context), req.getRequestParams(), callback);
    }

    /**
     * 请求每日任务
     *
     * @param context
     * @param callback
     */
    public static final void requestDailyTask(Context context, RequestParams pamra, GowildAsyncHttpResponseHandler<DaliyTask> callback) {
        HttpUtil.getInstance().post(context, getUrl(URL_TASKMISSION), getHeads(context), pamra, callback.setUrl(getUrl(URL_TASKMISSION)));
    }

    /**
     * 请求我的财富信息
     *
     * @param context
     * @param req
     * @param callback
     */
    public static final void requestWealthMine(Context context, RequestWealthMine req, GowildAsyncHttpResponseHandler<WealthMine> callback) {
        HttpUtil.getInstance().post(context, getUrl(URL_WEALTH_MINE), getHeads(context), req.getRequestParams(), callback.setUrl(getUrl(URL_WEALTH_MINE)));

    }

    /**
     * 获得财富排行信息
     *
     * @param context
     * @param req
     * @param callback
     */
    public static final void requestWealthRank(Context context, RequestWealthRank req, GowildAsyncHttpResponseHandler<WealthRankPageInfo> callback) {
        HttpUtil.getInstance().post(context, getUrl(URL_WEALTH_RANK), getHeads(context), req.getRequestParams(), callback.setUrl(getUrl(URL_WEALTH_RANK)));

    }

    /**
     * 意见反馈保存
     *
     * @param context
     * @param req
     * @param callback
     */
    public static final void requestSaveFeedback(Context context, RequestFeedback req, GowildAsyncHttpResponseHandler callback) {
        HttpUtil.getInstance().post(context, getUrl(URL_FEEDBACK), getHeads(context), req.getRequestParams(), callback.setUrl(getUrl(URL_FEEDBACK)));
    }

    /**
     * 获得个人信息
     *
     * @param context
     * @param req
     * @param callback
     */
    public static final void requestGetUserInfo(Context context, RequestInfoGet req, GowildAsyncHttpResponseHandler callback) {
        HttpUtil.getInstance().post(context, getUrl(URL_GET_USER), getHeads(context), req.getRequestParams(), callback.setUrl(getUrl(URL_GET_USER)));
    }


    /**
     * 请求验证码
     *
     * @param context
     * @param callback
     */
    public static final void requestCaptcha(Context context, RequestParams params, ObjectAsynHttpResponseHandler callback) {
        HttpUtil.getInstance().post(context, getUrl(URL_CAPTCHA), params, callback);
    }

    /**
     * 请求搜索结果
     *
     * @param context
     * @param callback
     */
    public static final void requestStoryAlbumSearchResult(Context context, String key, int pageNum, GowildAsyncHttpResponseHandler callback) {
        String url = getUrl(PATH_REQUEST_STORY_SEARCH_RESULT);
        RequestParams params = new RequestParams(Constants.SEARCH_KEY, key,
                PAGENUM, pageNum, PAGESIZE, DEFAULT_PAGE_SIZE);
        HttpUtil.getInstance().post(context, url, getHeads(context), params, callback.setUrl(url));
    }

    public static final void requestStoryAlbumByCategoryId(Context context, int categoryId, int pageNum, GowildAsyncHttpResponseHandler callback) {
        String url = getUrl(PATH_GET_STORTALBUM_BY_CATEGORY_ID);
        RequestParams params = new RequestParams(Constants.CATEGORY_ID, categoryId,
                PAGENUM, pageNum, PAGESIZE, DEFAULT_PAGE_SIZE);
        HttpUtil.getInstance().post(context, url, getHeads(context), params, callback.setUrl(url));
    }

    /**
     * 请求歌手类型
     *
     * @param context
     * @param req
     * @param callback
     */
    public static final void requestStoryCategorySort(Context context, RequestStorySort req, GowildAsyncHttpResponseHandler callback) {
        HttpUtil.getInstance().post(context, getUrl(PATH_REQUEST_STORY_CATEGORY), getHeads(context), req.getRequestParams(), callback.setUrl(getUrl(PATH_REQUEST_STORY_CATEGORY)));
    }

    public static final void requestStoryChapterFromStoryAlbumID(Context context, RequestParams req, GowildAsyncHttpResponseHandler callback) {
        HttpUtil.getInstance().post(context, getUrl(PATH_REQUEST_STORY_CHAPTER_FROM_STORY_ALBUM_LIST), getHeads(context), req, callback.setUrl(getUrl(PATH_REQUEST_STORY_CHAPTER_FROM_STORY_ALBUM_LIST)));
    }

    public static final void requestStoryAlbumFromKeyWord(Context context, RequestParams req, GowildAsyncHttpResponseHandler callback) {
        HttpUtil.getInstance().post(context, getUrl(PATH_REQUEST_STORY_ALBUM_FROM_STORY_KEYWORD), getHeads(context), req, callback.setUrl(getUrl(PATH_REQUEST_STORY_ALBUM_FROM_STORY_KEYWORD)));
    }

    /**
     * 请求成就数据
     *
     * @param context
     * @param req
     * @param callback
     */
    public static final void requestAchievement(Context context, RequestAchievement req, GowildAsyncHttpResponseHandler callback) {
        HttpUtil.getInstance().post(context, getUrl(URL_ACHIEVEMENT), getHeads(context), req.getRequestParams(), callback.setUrl(getUrl(URL_ACHIEVEMENT)));
    }

    /**
     * 修改密码
     *
     * @param context
     * @param req
     * @param callback
     */
    public static final void requestModifyPwd(Context context, RequestModifyPwd req, GowildAsyncHttpResponseHandler callback) {
        HttpUtil.getInstance().post(context, getUrl(URL_MODIFY_PWD), getHeads(context), req.getRequestParams(), callback.setUrl(getUrl(URL_MODIFY_PWD)));
    }

    /**
     * 清除服务端Token
     *
     * @param context
     * @param req
     * @param callback
     */
    public static final void requestRevokeToken(Context context, RequestRevokeToken req, GowildAsyncHttpResponseHandler callback) {
        HttpUtil.getInstance().post(context, URL_REVOKE_TOKEN, getHeads(context), req.getRequestParams(), callback.setUrl(URL_REVOKE_TOKEN));
    }


    /**
     * 更新个人信息
     *
     * @param context
     * @param req
     * @param callback
     */
    public static final void requestUpdateUserInfo(Context context, RequestInfoUpdate req, GowildAsyncHttpResponseHandler callback) {
        HttpUtil.getInstance().post(context, getUrl(URL_UPDATE_USER), getHeads(context), req.getRequestParams(), callback.setUrl(getUrl(URL_UPDATE_USER)));
    }


    public static final void requestSkills(Context context, GowildAsyncHttpResponseHandler handler) {
        String url = getUrl(PATH_REQUEST_SKILL_LIST);
        HttpUtil.getInstance().post(context, url, getHeads(context), null, handler.setUrl(url));
    }

    public static final void requestPhotos(Context context, int pageNum, GowildAsyncHttpResponseHandler handler) {
        String url = getUrl(PATH_REQUEST_PHOTO_LIST);
        RequestParams params = new RequestParams(PAGENUM, pageNum, PAGESIZE, PHOTO_PAGE_SIZE);
        HttpUtil.getInstance().post(context, url, getHeads(context), params, handler.setUrl(url));
    }

    public static final void deletePhotos(Context context, String ids, GowildAsyncHttpResponseHandler handler) {
        String url = getUrl(PATH_DELETE_PHOTO);
        RequestParams params = new RequestParams("ids", ids);
        HttpUtil.getInstance().post(context, url, getHeads(context), params, handler.setUrl(url));
    }

    public static final void changeMusicCollect(Context context, int id, boolean add, GowildAsyncHttpResponseHandler handler) {
        String url;
        if (add) {
            url = getUrl(PATH_ADD_MUSIC_TO_COLLECT);
        } else {
            url = getUrl(PATH_DEL_MUSIC_TO_COLLECT);
        }
        RequestParams params = new RequestParams("musicId", id);
        HttpUtil.getInstance().post(context, url, getHeads(context), params, handler.setUrl(url));
    }

    public static final void getMusicHistory(Context context, GowildAsyncHttpResponseHandler handler) {
        String url = getUrl(PATH_GET_MUSIC_HISTORY);
        HttpUtil.getInstance().post(context, url, getHeads(context), null, handler.setUrl(url));
    }

    public static final void getMusicCollect(Context context, int pageNum, GowildAsyncHttpResponseHandler handler) {
        String url = getUrl(PATH_GET_MUSIC_COLLECT);
        RequestParams params = new RequestParams(PAGENUM, pageNum, PAGESIZE, DEFAULT_PAGE_SIZE);
        HttpUtil.getInstance().post(context, url, getHeads(context), params, handler.setUrl(url));
    }

    public static final void changeStoryCollect(Context context, int id, boolean add, GowildAsyncHttpResponseHandler handler) {
        String url;
        if (add) {
            url = getUrl(PATH_ADD_STORY_TO_COLLECT);
        } else {
            url = getUrl(PATH_DEL_STORY_TO_COLLECT);
        }
        RequestParams params = new RequestParams("albumId", id);
        HttpUtil.getInstance().post(context, url, getHeads(context), params, handler.setUrl(url));
    }

    public static final void getStoryHistory(Context context, RequestStoryHistory params, GowildAsyncHttpResponseHandler callback) {
        HttpUtil.getInstance().post(context, getUrl(PATH_GET_STORY_HISTORY), getHeads(context), params.getRequestParams(), callback.setUrl(getUrl(PATH_GET_STORY_HISTORY)));
    }

    public static final void getStoryCollect(Context context, int pageNum, GowildAsyncHttpResponseHandler callback) {
        String url = getUrl(PATH_GET_STORY_COLLECT);
        RequestParams params = new RequestParams(PAGENUM, pageNum, PAGESIZE, DEFAULT_PAGE_SIZE);
        HttpUtil.getInstance().post(context, url, getHeads(context), params, callback.setUrl(url));
    }

    public static final void getRobotInfo(Context context, RequestParams params, GowildAsyncHttpResponseHandler callback) {
        HttpUtil.getInstance().post(context, getUrl(PATH_GET_BIND_INFO), getHeads(context), params, callback.setUrl(getUrl(PATH_GET_BIND_INFO)));
    }

    public static final void updateTalkSpeedOrVolume(Context context, RequestParams params, GowildAsyncHttpResponseHandler callback) {
        HttpUtil.getInstance().post(context, getUrl(PATH_UPDATE_TALK_SPEED_OR_VOLUME), getHeads(context), params, callback.setUrl(getUrl(PATH_UPDATE_TALK_SPEED_OR_VOLUME)));
    }

    public static final void unbindRobot(Context context, RequestParams params, GowildAsyncHttpResponseHandler callback) {
        HttpUtil.getInstance().post(context, getUrl(PATH_UNBIND_ROBOT), getHeads(context), params, callback.setUrl(getUrl(PATH_UNBIND_ROBOT)));
    }

    public static final void changeMusic(Context context, RequestParams params, GowildAsyncHttpResponseHandler callback) {
        HttpUtil.getInstance().post(context, getUrl(PATH_CHANGE_MUSIC), getHeads(context), params, callback.setUrl(getUrl(PATH_CHANGE_MUSIC)));
    }

    public static final void resumeMusic(Context context, RequestParams params, GowildAsyncHttpResponseHandler callback) {
        HttpUtil.getInstance().post(context, getUrl(PATH_RESUME_MUSIC), getHeads(context), params, callback.setUrl(getUrl(PATH_RESUME_MUSIC)));
    }

    public static final void changeStory(Context context, RequestParams params, GowildAsyncHttpResponseHandler callback) {
        HttpUtil.getInstance().post(context, getUrl(PATH_CHANGE_STORY), getHeads(context), params, callback.setUrl(getUrl(PATH_CHANGE_STORY)));
    }

    public static final void requestLogin(RequestParams params, GowildAsyncHttpResponseHandler callback) {
        HttpUtil.getInstance().post(GowildApplication.context, URL_LUNCH, Constants.AUTHORIZATION, Constants.AUTHORIZATION_DESC, params, callback.setUrl(URL_LUNCH));
    }

    public static void requestRegiest(Context context, RequestParams params, GowildAsyncHttpResponseHandler callback) {
        HttpUtil.getInstance().post(context, getUrl(URL_CHECK_CAPTCHA), params, callback.setUrl(getUrl(URL_CHECK_CAPTCHA)));
    }

    public static void requestForgotPwd(Context context, RequestParams params, GowildAsyncHttpResponseHandler callback) {
        HttpUtil.getInstance().post(context, getUrl(URL_CHECK_CAPTCHA), params, callback.setUrl(getUrl(URL_CHECK_CAPTCHA)));
    }

    public static void requestCaptcha(Context context, RequestParams params, GowildAsyncHttpResponseHandler callback) {
        HttpUtil.getInstance().post(context, URL_CAPTCHA, params, callback.setUrl(URL_CAPTCHA));
    }

    public static final void sendMessage(Context context, RequestParams params, GowildAsyncHttpResponseHandler callback) {
        HttpUtil.getInstance().post(context, getUrl(URL_CONSTACTS_SENDMESSAGE), getHeads(context), params, callback.setUrl(getUrl(URL_CONSTACTS_SENDMESSAGE)));
    }

    public static void joinLover(Context context, RequestParams params, AsyncHttpResponseHandler callback) {
        HttpUtil.getInstance().post(context, getUrl(URL_REQUEST_LOVER), getHeads(context), params, callback);
    }

    public static void requestUnbindLover(Context context, RequestParams params, AsyncHttpResponseHandler callback) {
        HttpUtil.getInstance().post(context, getUrl(URL_REQUEST_UNBIND_LOVER), getHeads(context), params, callback);
    }

    public static void dailyTask(Context context, AsyncHttpResponseHandler callback) {
        HttpUtil.getInstance().post(context, getUrl(URL_REQUEST_DAILY_TASK), getHeads(context), callback);
    }

    public static void requestMachineTime(Context context, AsyncHttpResponseHandler callback) {
        HttpUtil.getInstance().post(context, getUrl(URL_REQUEST_MACHINE_TIME), getHeads(context), null, callback);
    }

    public static void requestUpdateTime(Context context, RequestParams params, AsyncHttpResponseHandler callback) {
        HttpUtil.getInstance().post(context, getUrl(URL_REQUEST_UPDATE_TIME), getHeads(context), params, callback);
    }

    public static void requestChangeNet(Context context, RequestParams params, AsyncHttpResponseHandler callback) {
        HttpUtil.getInstance().post(context, getUrl(URL_REQUEST_NET), getHeads(context), params, callback);
    }


    /**
     *
     */
    public static void requestLogin(Context context, AsyncHttpResponseHandler callback) {
        //        HttpUtil.getInstance().post(context,URL_LUNCH, , callback);
    }


    /**
     * 转化拼接url
     *
     * @param url
     * @return 加上BASE_URL后的url
     */
    public static String getUrl(String url) {
        if (TextUtils.isEmpty(url)) {
            throw new IllegalStateException("url地址不能为空");
        }
        switch (sWebEnvirmonent) {
            case TestConstants.WEB_ENVIRONMENT_TEST:
                return BASE_URL_TEST + url;
            default:
                return HostConstants.BASE_URL_FORMAL + url;
        }
    }

    /**
     * 授权转换的url
     */
    public static String getTokenURL(String url) {
        if (TextUtils.isEmpty(url)) {
            throw new IllegalStateException("url地址不能为空");
        }
        switch (sWebEnvirmonent) {
            case TestConstants.WEB_ENVIRONMENT_TEST:
                return BASE_URL_TOKEN_TEST + url;
            default:
                return HostConstants.TOKEN_BASE_URL + url;
        }
    }

    /**
     * tcp转换
     */
    public static String getTCPAddress() {
        switch (sWebEnvirmonent) {
            case TestConstants.WEB_ENVIRONMENT_TEST:
                return BASE_URL_TCP_TEST;
            default:
                return BASE_URL_TCP;
        }
    }


    /**
     * tcp转换
     */
    public static int getTCPPort() {
        switch (sWebEnvirmonent) {
            case TestConstants.WEB_ENVIRONMENT_TEST:
                return BASE_TCP_PORT_TEST;
            default:
                return BASE_URL_PORT;
        }
    }


    public static Header[] getHeads(Context context) {
        Header[] headers = new Header[1];
        headers[0] = getAuthHead(context);
        return headers;
    }

    public static Header getAuthHead(Context context) {
        String token = SpUtils.getString(context, Constants.ACCESS_TOKEN);
        //Logger.d(TAG, "token = " + token);
        return new BasicHeader(AUTHORIZATION, token);
    }

    public static void postStatutas(String name, int value) {
        //        if (value == 0){
        //            return;
        //        }
        //        RequestParams params = new RequestParams(name,value);
        //        HttpUtil.getInstance().post(GowildApplication.context, getUrl(URL_REQUEST_NET), getHeads(GowildApplication.context), params, new AsyncHttpResponseHandler() {
        //            @Override
        //            public void onSuccess(int i, Header[] headers, byte[] bytes) {
        //
        //            }
        //
        //            @Override
        //            public void onFailure(int i, Header[] headers, byte[] bytes, Throwable throwable) {
        //
        //            }
        //        });
    }
}