/**
 * Copyright (C)  2016 深圳市狗尾草智能科技有限公司
 * GowildXB
 * ApplianceManager.java
 */
package com.gowild.mobileclient.manager;

import com.gowild.mobile.libp2sp.core.exception.NotConnectedException;

/**
 * @author Zhangct
 * @version 1.0
 *          <p> 电器TCP请求管理者 </p>
 * @since 2016/8/20 17:45
 */
public class ApplianceManager {

    private ApplianceManager() {
    }

    private static ApplianceManager instance = null;

    public static ApplianceManager getInstance() {
        if (null == instance) {
            synchronized (ApplianceManager.class) {
                if (null == instance) {
                    instance = new ApplianceManager();
                }
            }
        }
        return instance;
    }

    /**
     * 从服务端加载数据
     *
     */
    public void syncApplianceData() {
        //同步电器
        try {
            GowildTCPManager.requestApplianceSyn();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 删除电器
     * @param deviceType 设备类型
     * @param sceneID 场景ID
     */
    public void delApplianceData(int deviceType,int sceneID) {
        //删除电器
        try {
            GowildTCPManager.requestApplianceDel(deviceType,sceneID);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void addApplianceData(String json){
        try {
            GowildTCPManager.requestApplianceAdd(json);
        } catch (NotConnectedException e) {
            e.printStackTrace();
        }
    }

    public void controlAppliance(String json){
        try {
            GowildTCPManager.requestApplianceControl(json);
        } catch (NotConnectedException e) {
            e.printStackTrace();
        }
    }


}
