/**
 * Copyright (C)  2016 深圳市狗尾草智能科技有限公司
 * GowildXB
 * OrientationUtil.java
 */
package com.gowild.mobileclient.utils;

/**
 * @author Ji.Li
 * @since 2016/8/13 14:51
 * @version 1.0
 * <p><strong>Features draft description.主要功能介绍</strong></p>
 */
@SuppressWarnings("unused")
public final class OrientationUtil {

    // ===========================================================
    // HostConstants
    // ===========================================================

    private static final String TAG = OrientationUtil.class.getSimpleName();

    // ===========================================================
    // Static Fields
    // ===========================================================

    // ===========================================================
    // Fields
    // ===========================================================


    // ===========================================================
    // Constructors
    // ===========================================================

    private OrientationUtil() {}

    // ===========================================================
    // Getter or Setter
    // ===========================================================


    // ===========================================================
    // Methods for/from SuperClass/Interfaces
    // ===========================================================


    // ===========================================================
    // Methods
    // ===========================================================

    public static int calculateAngel(int index) {
        int angle = 0;
        switch (index) {
            case 0:
                angle = 271;
                break;
            case 1:
                angle = 316;
                break;
            case 2:
                angle = 0;
                break;
            case 3:
                angle = 44;
                break;
            case 4:
                angle = 89;
                break;
            default:
                angle = 0;
        }
        return angle;
    }

    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================

}
