package com.gowild.mobileclient.utils;

import android.content.Context;
import android.graphics.Bitmap.Config;
import android.net.Uri;
import android.widget.ImageView;

import com.gowild.mobileclient.R;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.DisplayImageOptions.Builder;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

public class ImageUtils {

    @SuppressWarnings("unused")
    private static final String TAG = ImageUtils.class.getSimpleName();
    private static final int DEFAULT_DRAWABLE_ID = R.color.transparent;
    private static final int DEFAULT_FADE_IN_TIME_MILLS = 500;

    private static volatile ImageUtils sInstance;
    private static Context sAppCtx;

    private DisplayImageOptions mDisplayOption = null;
    private ImageLoader mImageLoader = null;

    private ImageUtils() {
        this.mImageLoader = ImageLoader.getInstance();
        this.mImageLoader.init(ImageLoaderConfiguration.createDefault(sAppCtx));
        Builder builder = new Builder().showImageOnLoading(DEFAULT_DRAWABLE_ID)
                .showImageOnFail(DEFAULT_DRAWABLE_ID).cacheInMemory(true).cacheOnDisk(true)
                .displayer(new FadeInBitmapDisplayer(DEFAULT_FADE_IN_TIME_MILLS))
                .imageScaleType(ImageScaleType.IN_SAMPLE_POWER_OF_2).bitmapConfig(Config.RGB_565);
        this.mDisplayOption = builder.build();
    }

    public static ImageUtils getInstance() {
        if (sInstance == null) {
            synchronized (ImageUtils.class) {
                if (sInstance == null) {
                    sInstance = new ImageUtils();
                }
            }
        }
        return sInstance;
    }

    public static void init(Context ctx) {
        sAppCtx = ctx;
    }


    public void displayNetImage(ImageView imageView, String url) {
        Uri uri = Uri.parse(url);
        this.mImageLoader.displayImage(uri.toString(), imageView, this.mDisplayOption);
    }

    public void loadNetImage(String url, ImageLoadingListener listener) {
        this.mImageLoader.loadImage(url, listener);
    }

}
