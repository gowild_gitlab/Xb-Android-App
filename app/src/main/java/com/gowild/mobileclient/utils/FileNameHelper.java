/**
 * Copyright (C)  2016 深圳市狗尾草智能科技有限公司
 * GowildXB
 * FileNameHelper.java
 */
package com.gowild.mobileclient.utils;

/**
 * @author Ji.Li
 * @since 2016/8/16 17:37
 * @version 1.0
 * <p><strong>Features draft description.主要功能介绍</strong></p>
 */
@SuppressWarnings("unused")
public final class FileNameHelper {

    // ===========================================================
    // HostConstants
    // ===========================================================

    private static final String TAG = FileNameHelper.class.getSimpleName();

    // ===========================================================
    // Static Fields
    // ===========================================================

    // ===========================================================
    // Fields
    // ===========================================================


    // ===========================================================
    // Constructors
    // ===========================================================

    private FileNameHelper() {}

    // ===========================================================
    // Getter or Setter
    // ===========================================================


    // ===========================================================
    // Methods for/from SuperClass/Interfaces
    // ===========================================================


    // ===========================================================
    // Methods
    // ===========================================================

    /**
     * 根据当前时间戳和类型生成视频临时文件名
     * @return
     */
    public static String generateVideoFileName(){
        StringBuilder sb = new StringBuilder();
        long currentTimeMillions = System.currentTimeMillis();
        sb.append("video_")
                .append(currentTimeMillions)
                .append(".mp4");
        return sb.toString();
    }

    /**
     * 根据当前时间错和类型生成图片临时文件名
     * @return
     */
    public static String generateJPGPictureFileName(){
        StringBuilder sb = new StringBuilder();
        long currentTimeMillions = System.currentTimeMillis();
        sb.append("picture_")
                .append(currentTimeMillions)
                .append(".jpg");
        return sb.toString();
    }

    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================

}
