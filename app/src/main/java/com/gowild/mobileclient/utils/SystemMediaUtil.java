package com.gowild.mobileclient.utils;

import java.io.File;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.webkit.MimeTypeMap;

public class SystemMediaUtil {

	private SystemMediaUtil() {}

	public static void startVideoPlayerActivity(Context ctx, String filePath) {
		String extension = MimeTypeMap.getFileExtensionFromUrl(filePath);
		String mimeType = MimeTypeMap.getSingleton().getMimeTypeFromExtension(
				extension);
		Intent intent = new Intent();
		intent.setAction(Intent.ACTION_VIEW);
		//meizu crash
		intent.setDataAndType(Uri.fromFile(new File(filePath)), "video/*");
		ctx.startActivity(intent);
	}

	public static void startPictureBrowseActivity(Context ctx, String filePath) {
		String extension = MimeTypeMap.getFileExtensionFromUrl(filePath);
		String mimeType = MimeTypeMap.getSingleton().getMimeTypeFromExtension(
				extension);
		Intent intent = new Intent();
		intent.setAction(Intent.ACTION_VIEW);
		intent.setDataAndType(Uri.fromFile(new File(filePath)), "image/*");
		ctx.startActivity(intent);
	}
}
