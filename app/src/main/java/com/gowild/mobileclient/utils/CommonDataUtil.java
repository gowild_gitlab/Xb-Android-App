/**
 * Copyright (C)  2016 深圳市狗尾草智能科技有限公司
 * GowildXB
 * CommonDataUtil.java
 */
package com.gowild.mobileclient.utils;

import com.gowild.mobileclient.R;
import com.gowild.mobileclient.application.GowildApplication;

/**
 * @author temp
 * @since 2016/8/30 11:13
 * @version 1.0
 * <p><strong>Features draft description.主要功能介绍</strong></p>
 */
public class CommonDataUtil {

    // ===========================================================
    // HostConstants
    // ===========================================================

    private static boolean sBLitterWhite = true;
    private static final String TAG = CommonDataUtil.class.getSimpleName();

    public static String getCurrentRobotTitle() {
        int id = sBLitterWhite ? R.string.gowild_litter_white : R.string.gowild_prince;
       return GowildApplication.context.getResources().getString(id);
    }


    public static boolean isBLitterWhite() {
        return sBLitterWhite;
    }

    public static void setBLitterWhite(boolean sBLitterWhite) {
        CommonDataUtil.sBLitterWhite = sBLitterWhite;
    }
}
