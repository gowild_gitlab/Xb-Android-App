package com.gowild.mobileclient.utils;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import android.content.Context;
import android.graphics.Bitmap;
import android.media.MediaMetadataRetriever;
import android.media.ThumbnailUtils;
import android.provider.MediaStore.Video.Thumbnails;
import android.text.TextUtils;
import android.util.Log;

import com.gowild.mobile.libcommon.utils.Logger;
import com.gowild.mobile.libcommon.utils.PathUtil;
import com.gowild.mobileclient.model.PhotoVideoBaseContentInfo;

public class VideoInfoUtil {
	
	@SuppressWarnings("unused")
	private static final String TAG = VideoInfoUtil.class.getSimpleName();

	private static final String PHOTO = "photo/";
    private static final String END_MP4 = ".mp4";
	
	private static ItemComparator sComparator;
	
	private VideoInfoUtil() {}

	public static String getGowildSaveDir(Context context) {
		return PathUtil.getSdcardPath(context) + "/gowild/";
	}

	public static String getPhotoSavePath(Context context) {
		String path = getGowildSaveDir(context) + PHOTO;
		File file = new File(path);
		if (!file.exists() || !file.isDirectory()) {
            file.mkdirs();
		}
        return path;
	}
	
	/**
	 * 提取视频文件信息
	 * @param filePath
	 * @param out
	 * @return 是否提取成功
	 **/
	public static boolean extractVideoInfo(String filePath, VideoInfo out) {
		if (filePath == null || out == null) {
			return false;
		}
		MediaMetadataRetriever retriever = new MediaMetadataRetriever();
		retriever.setDataSource(filePath); 
		
		String tmp;
		// 获取拍摄时长
		tmp = retriever
				.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
		out.duration = tmp;
		// 获取拍摄标题
		tmp = retriever
				.extractMetadata(MediaMetadataRetriever.METADATA_KEY_TITLE);
		out.title = tmp;
		// 获取视频文件的大小
		File file = new File(filePath);
		long totalSize = file.length();
		out.size = totalSize;
		//获取视频的日期
		String fileName = file.getName();
		String[] arr = fileName.split("\\.");
		String fileNameNoExtension = arr[0];
		if (arr != null && fileNameNoExtension != null) {
			String[] names = fileNameNoExtension.split("_"); 
			if (names != null && names[1] != null){
				long millSeconds = Long.parseLong(names[1]);
				out.mTimeStamp = millSeconds;
				out.mDate = Utils.sFormatDate.format(new Date(millSeconds));
			}else{
				out.mDate = "";
				out.mTimeStamp = 0L;
			}
		} else {
			out.mDate = "";
			out.mTimeStamp = 0L;
		}
		
		// 获取视频文件的路径
		out.mFileOriginPath = filePath;
		return true;
	}
	
	public static boolean extractPictureInfo(String filePath, PictureInfo out) {
		if (filePath == null || out == null) {
			return false;
		}
		//获取图片文件的大小
		File file = new File(filePath);
		out.size = file.length();
        out.mFileOriginPath = filePath;
		String fileName = file.getName();
		String[] arr = fileName.split("\\.");
		if (arr != null && arr[0] != null){
			String[] content = arr[0].split("_");
			if (content != null && content[1] != null){
				long millSeconds = Long.parseLong(content[1]);
				out.mDate = Utils.sFormatDate.format(new Date(millSeconds));
				out.mTimeStamp = millSeconds;
			}else{
				out.mDate = "";
				out.mTimeStamp = 0L;
			}
		}else{
			out.mDate = "";
			out.mTimeStamp = 0L;
		}
		return true;
	}
	
	public static void scanDirectoryNoThrows(File file, List<VideoInfo> outList) {
		if (file == null || !file.isDirectory()) {
			return;
		}
		File[] list = file.listFiles();
		if (list != null && list.length > 0) {
			VideoInfo info;
			for (File f : list) {
				String path  = f.getAbsolutePath();
				Logger.d(TAG, "scanDirectoryNoThrows file path = " + path);
				if (!path.endsWith(END_MP4)) {
					file.delete();
					continue;
				}
				info = new VideoInfo();
				extractVideoInfo(path, info);
				outList.add(info);
			}
		}
		return;
	}
	
	public static void scanPictureDirectoryNoThrows(File file,
			List<PictureInfo> outList) {
		if (file == null || !file.isDirectory()) {
			return;
		}
		File[] list = file.listFiles();
		if (list != null && list.length > 0) {
			PictureInfo pictureInfo;
			for (File f : list) {
				pictureInfo = new PictureInfo();
				extractPictureInfo(f.getAbsolutePath(), pictureInfo);

                if (!TextUtils.isEmpty(pictureInfo.mDate)) {
					outList.add(pictureInfo);
				}
			}
		}
		return;
	}
	
	public static Bitmap getVideoThumbnails(String videoPath,int width,int height){
		Bitmap bmp = null;
		bmp = ThumbnailUtils.createVideoThumbnail(videoPath, Thumbnails.MICRO_KIND);
		bmp = ThumbnailUtils.extractThumbnail(bmp, width, height, ThumbnailUtils.OPTIONS_RECYCLE_INPUT);
		return bmp;
	}
	
	public static double convertBytesToMb(long bytes) {
		double d = bytes / 1024.0 / 1024.0;
		return d;
	}
	
	public static void sortList(List<? extends PhotoVideoBaseContentInfo> list) {
		if (sComparator == null) {
			sComparator = new ItemComparator();
		}
		Collections.sort(list, sComparator);
	}
	
	public static class VideoInfo extends PhotoVideoBaseContentInfo {
		public String title; // 视频的标题
		public String duration; // 视频的时长
		public long   size; // 视频文件的大小
	}
	
	public static class ItemComparator implements Comparator<PhotoVideoBaseContentInfo>{

		@Override
		public int compare(PhotoVideoBaseContentInfo lhs, PhotoVideoBaseContentInfo rhs) {
			long left = lhs.mTimeStamp;
			long right = rhs.mTimeStamp;
			if (left > right){
				return -1;
			}else if (left == right){
				return 0;
			}else{
				return 1;
			}
		}
	}

	public static class PictureInfo extends PhotoVideoBaseContentInfo {
		public long   size;
	}
}
