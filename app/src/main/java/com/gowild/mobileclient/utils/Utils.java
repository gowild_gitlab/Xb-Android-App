/**
 * Copyright (C)  2016 深圳市狗尾草智能科技有限公司
 * GowildXB
 * Utils.java
 */
package com.gowild.mobileclient.utils;

import android.graphics.Bitmap;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

/**
 * @author temp
 * @since 2016/8/4 15:08
 * @version 1.0
 * <p><strong>Features draft description.主要功能介绍</strong></p>
 */
public class Utils {

    // ===========================================================
    // HostConstants
    // ===========================================================

    private static final String TAG = Utils.class.getSimpleName();

    public static DateFormat sFormatTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    public static DateFormat sFormatDate = new SimpleDateFormat("yyyy-MM-dd");
    
    public static void saveFile(Bitmap bm, String fileName) throws IOException {
        File myCaptureFile = new File(fileName);
        BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(myCaptureFile));
        bm.compress(Bitmap.CompressFormat.JPEG, 80, bos);    
        bos.flush();
        bos.close();
    }
}
