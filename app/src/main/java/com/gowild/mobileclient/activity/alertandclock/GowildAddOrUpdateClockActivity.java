/**
 * @author JunDong Wang
 * @version 1.0
 * <p><strong>闹钟添加或者修改的界面</strong></p>
 * @since 2016/7/23 18:34
 */
package com.gowild.mobileclient.activity.alertandclock;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.gowild.gwais.libwheelview.adapter.ArrayWheelAdapter;
import com.gowild.gwais.libwheelview.widght.WheelView;
import com.gowild.mobileclient.R;
import com.gowild.mobileclient.base.baseactivity.GowildBaseActivity;
import com.gowild.mobileclient.config.Constants;
import com.gowild.mobileclient.manager.GowildAsyncHttpResponseHandler;
import com.gowild.mobileclient.manager.GowildHttpManager;
import com.gowild.mobileclient.vo.Clock;
import com.gowild.mobileclient.vo.RequestCreateClock;
import com.gowild.mobileclient.vo.RequestUpdateClock;
import com.gowild.mobileclient.vo.ResponseCreateClock;
import com.gowild.mobileclient.vo.ResponseUpdateClock;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

/**
 * @author JunDong Wang
 * @version 1.0
 *          <p><strong>待办添加或者闹钟的界面  修改</strong></p>
 * @since 2016/7/23 18:34
 */
public class GowildAddOrUpdateClockActivity extends GowildBaseActivity {

    // ===========================================================
    // HostConstants
    // ===========================================================

    private static final String TAG = GowildAddOrUpdateClockActivity.class.getSimpleName();


    // ===========================================================
    // Static Fields
    // ===========================================================

    // ===========================================================
    // Fields
    // ===========================================================
    TextView mTagContent, mRepeadContent;
    private int mCreateType;
    private View mBack;
    WheelView mMinteWheel;
    WheelView mHourWheel;
    private OnClickCallback mOnClickCallback = new OnClickCallback();
    private RequestCreateClockCallback mRequestCreateClockCallback;
    private RequestUpdateClockCallback mRequestUpdateClockCallback;
    ArrayList<String> mMinteList = new ArrayList<>();
    ArrayList<String> mHourList = new ArrayList<>();
    private int mCycleIndex = 0;
    private Clock mClock = new Clock();
    private TextView mTvTitle;
    private int type;

    // ===========================================================
    // Constructors
    // ===========================================================


    // ===========================================================
    // Getter or Setter
    // ===========================================================


    // ===========================================================
    // Methods for/from SuperClass/Interfaces
    // ===========================================================


    @Override
    protected int onSetTopbarView() {
        return R.layout.activity_gowild_add_update_clock_topbar;
    }

    @Override
    protected int onSetContentView() {
        return R.layout.activity_gowild_add_update_clock;
    }

    @Override
    protected int onSetTopBarHeight() {
        return getResources().getDimensionPixelSize(R.dimen.gowild_160px);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {
            mTagContent.setText(data.getStringExtra(Constants.CONTENT));
        } else if (requestCode == 2) {
            mCycleIndex = data.getIntExtra(Constants.INFO, 0);
            mRepeadContent.setText(checkRepeat(mCycleIndex));
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        type = getIntent().getIntExtra(Constants.TYPE,-1);
        mTvTitle = (TextView) findViewById(R.id.title);
        if (type == 1) {
            mTvTitle.setText("修改闹钟");
        } else {
            mTvTitle.setText("添加闹钟");
        }
        mTagContent = (TextView) findViewById(R.id.content);
        mRepeadContent = (TextView) findViewById(R.id.repead_content);
        mMinteWheel = (WheelView) findViewById(R.id.minte);
        mHourWheel = (WheelView) findViewById(R.id.hour);
        TextView commit = (TextView) findViewById(R.id.commit);
        mBack = findViewById(R.id.back);
        mBack.setOnClickListener(mOnClickCallback);
        commit.setOnClickListener(mOnClickCallback);
        RelativeLayout rltContent = (RelativeLayout) findViewById(R.id.tag);
        rltContent.setOnClickListener(mOnClickCallback);
        RelativeLayout rltRepead = (RelativeLayout) findViewById(R.id.repead);
        rltRepead.setOnClickListener(mOnClickCallback);
        mRequestCreateClockCallback = new RequestCreateClockCallback(this);
        mRequestUpdateClockCallback = new RequestUpdateClockCallback(this);
        mCreateType = getIntent().getIntExtra(Constants.TYPE, -1);
        mClock = new Clock();
        Log.d(TAG, "onResume");
        mClock.mAlarmId = getIntent().getIntExtra(Constants.ALARM_ID, -1);
        mClock.mMinute = getIntent().getIntExtra(Constants.MINUTE, 0);
        mClock.mHour = getIntent().getIntExtra(Constants.HOUR, 0);

        mClock.mAlarmFlag = getIntent().getStringExtra(Constants.ALARM_FLAG);
        mClock.mCycleIndex = getIntent().getIntExtra(Constants.CYCLE_INDEX, 0);
        if (!TextUtils.isEmpty(mClock.mAlarmFlag)) {
            mTagContent.setText(mClock.mAlarmFlag);
        }else{
            mTagContent.setText("起床");
        }
        initHour();
        initMinute();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mRepeadContent.getText().toString().equals("")) {
            mCycleIndex = mClock.mCycleIndex;
            mRepeadContent.setText(checkRepeat(mCycleIndex));
        }
    }


    // ===========================================================
    // Methods
    // ===========================================================
    public String checkRepeat(int cycleIndex) {
        String repeat = "";
        String[] weekArr = getResources().getStringArray(R.array.week_type);
        for (int i = 1; i <= 7; i++) {
            if ((cycleIndex & (1 << (i % 7))) != 0) {
                repeat = repeat + weekArr[i - 1] + " ";
            }
        }
        if (cycleIndex == 0) {
            return getResources().getString(R.string.gowild_alert_and_clock_never);
        } else {
            return repeat;
        }

    }

    private void initMinute() {
        mMinteList.clear();
        Calendar calendar = Calendar.getInstance();
        Date date = calendar.getTime();
        int minte = date.getMinutes();
        if (mCreateType == GowildAlertOrClockActivity.TYPE_ADD) {
            for (int i = 0; i < 60; i++) {
                mMinteList.add(Integer.toString(i));
            }
        } else if (mCreateType == GowildAlertOrClockActivity.TYPE_UPDATE) {
            for (int i = 0; i < 60; i++) {
                mMinteList.add(Integer.toString(i));
            }
        }
        String[] array = mMinteList.toArray(new String[]{});
        ArrayWheelAdapter<String> wheelAdapter = new ArrayWheelAdapter<>(this, array);
        wheelAdapter.setTextSize(18);
        wheelAdapter.setTextColor(getResources().getColor(R.color.gowild_alert_clock_title));
        mMinteWheel.setCyclic(true);
        mMinteWheel.setViewAdapter(wheelAdapter);
        if (type == 1) {
            mMinteWheel.setCurrentItem(mClock.mMinute);
        } else {
            mMinteWheel.setCurrentItem(minte);
        }
    }


    private void initHour() {
        mHourList.clear();
        Calendar calendar = Calendar.getInstance();
        Date date = calendar.getTime();
        int hour = date.getHours();
        if (mCreateType == GowildAlertOrClockActivity.TYPE_ADD) {
            for (int i = 0; i < 24; i++) {
                mHourList.add(Integer.toString(i));
            }
        } else if (mCreateType == GowildAlertOrClockActivity.TYPE_UPDATE) {
            for (int i = 0; i < 24; i++) {
                mHourList.add(Integer.toString(i));
            }
        }
        String[] array = mHourList.toArray(new String[]{});
        ArrayWheelAdapter<String> wheelAdapter = new ArrayWheelAdapter<>(this, array);
        wheelAdapter.setTextSize(18);
        mHourWheel.setCyclic(true);
        mHourWheel.setViewAdapter(wheelAdapter);
        wheelAdapter.setTextColor(getResources().getColor(R.color.gowild_alert_clock_title));
        if (type == 1) {
            mHourWheel.setCurrentItem(mClock.mHour);
        } else {
            mHourWheel.setCurrentItem(hour);
        }
    }

    private void commit() {
        Clock clock = new Clock();
        clock.mHour = Integer.valueOf(mHourList.get(mHourWheel.getCurrentItem()));
        clock.mMinute = Integer.valueOf(mMinteList.get(mMinteWheel.getCurrentItem()));
        clock.mAlarmFlag = mTagContent.getText().toString();
        clock.mCycleIndex = mCycleIndex;
        if (mCreateType == GowildAlertOrClockActivity.TYPE_ADD) {
            RequestCreateClock requestCreateClock = new RequestCreateClock();
            requestCreateClock.put(Constants.ALARM_FLAG, clock.mAlarmFlag);
            requestCreateClock.put(Constants.HOUR, Integer.toString(clock.mHour));
            requestCreateClock.put(Constants.MINUTE, Integer.toString(clock.mMinute));
            requestCreateClock.put(Constants.ENABLE, "true");
            requestCreateClock.put(Constants.CYCLE_INDEX, Integer.toString(clock.mCycleIndex));
            showLoading(R.string.loading);
            GowildHttpManager.requestCreateClock(this, requestCreateClock, mRequestCreateClockCallback);
        } else if (mCreateType == GowildAlertOrClockActivity.TYPE_UPDATE) {
            RequestUpdateClock requestUpdateClock = new RequestUpdateClock();
            requestUpdateClock.put(Constants.ALARM_ID, Integer.toString(mClock.mAlarmId));
            requestUpdateClock.put(Constants.ALARM_FLAG, clock.mAlarmFlag);
            requestUpdateClock.put(Constants.HOUR, Integer.toString(clock.mHour));
            requestUpdateClock.put(Constants.MINUTE, Integer.toString(clock.mMinute));
            requestUpdateClock.put(Constants.ENABLE, "true");
            requestUpdateClock.put(Constants.CYCLE_INDEX, Integer.toString(clock.mCycleIndex));
            showLoading(R.string.loading);
            GowildHttpManager.requestUpdateClock(this, requestUpdateClock, mRequestUpdateClockCallback);
        }
    }

    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================
    private class OnClickCallback implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            Intent intent;
            switch (v.getId()) {
                case R.id.repead:
                    intent = new Intent(GowildAddOrUpdateClockActivity.this, GowildClockRepeadTagActivit.class);
                    intent.putExtra(Constants.CYCLE_INDEX, mCycleIndex);
                    startActivityForResult(intent, 2);
                    break;
                case R.id.tag:
                    intent = new Intent(GowildAddOrUpdateClockActivity.this, GowildAlertOrClockTagsActivity.class);
                    intent.putExtra(Constants.CONTENT, mTagContent.getText().toString());
                    intent.putExtra(Constants.TYPE, "clock");
                    startActivityForResult(intent, 1);
                    break;
                case R.id.back:
                    onBackPressed();
                    break;
                case R.id.commit:
                    if (TextUtils.isEmpty(mTagContent.getText().toString().trim())) {
                        mTagContent.setText("起床");
                    }
                    commit();
                    break;
            }
        }
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    private class RequestCreateClockCallback extends GowildAsyncHttpResponseHandler<ResponseCreateClock> {


        /**
         * 构造方法
         *
         * @param context 用于处理授权过期消息，会被转成ApplicationContext
         */
        public RequestCreateClockCallback(Context context) {
            super(context);
        }

        @Override
        public void onSuccess(int statusCode, ResponseCreateClock responseBody) {
            hideLoading();
            if (statusCode == 100) {
                onBackPressed();
            }
        }

        @Override
        public void onFailure(int statusCode, String responseBody) {
            hideLoading();
        }
    }

    private class RequestUpdateClockCallback extends GowildAsyncHttpResponseHandler<ResponseUpdateClock> {

        /**
         * 构造方法
         *
         * @param context 用于处理授权过期消息，会被转成ApplicationContext
         */
        public RequestUpdateClockCallback(Context context) {
            super(context);
        }

        @Override
        public void onSuccess(int statusCode, ResponseUpdateClock responseBody) {
            hideLoading();
            if (statusCode == 100) {
                onBackPressed();
            }
        }

        @Override
        public void onFailure(int statusCode, String responseBody) {
            hideLoading();
        }
    }
}
