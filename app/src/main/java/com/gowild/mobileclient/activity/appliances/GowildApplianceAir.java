/**
 * Copyright (C)  2016 深圳市狗尾草智能科技有限公司
 * GowildXB
 * GowildApplianceAir.java
 */
package com.gowild.mobileclient.activity.appliances;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.SeekBar;
import android.widget.TextView;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.gowild.mobile.libcommon.utils.Logger;
import com.gowild.mobile.libp2sp.core.listener.IDataListener;
import com.gowild.mobile.libp2sp.netty.NettyClientManager;
import com.gowild.mobileclient.R;
import com.gowild.mobileclient.activity.base.GowildHasTitileActivity;
import com.gowild.mobileclient.config.ApplianceConstants;
import com.gowild.mobileclient.manager.ApplianceManager;
import com.gowild.mobileclient.misc.GowildResponseCode;
import com.gowild.mobileclient.protocol.StringMsgPro;

import butterknife.Bind;
import butterknife.OnClick;

/**
 * @author Administrator
 * @version 1.0
 *          <p><strong>空调控制</strong></p>
 * @since 2016/8/5 15:50
 */
public class GowildApplianceAir extends GowildHasTitileActivity {

    // ===========================================================
    // HostConstants
    // ===========================================================

    private static final String TAG = GowildApplianceAir.class.getSimpleName();
    //电器
    public static final String APPLIANCE_ID = "APPLIANCE_ID";
    public static final String APPLIANCE_NIKENAME = "APPLIANCE_NIKENAME";
    public static final String SCENE_TYPE_ID = "SCENE_TYPE_ID";
    //空调
    private final int DEVICE_TYPE_AIR = 0;

    // ===========================================================
    // Static Fields
    // ===========================================================

    // ===========================================================
    // Fields
    // ===========================================================

    private String mApplianceId = null;
    private String mApplianceNikeName = null;
    private int mSceneTypeId = -1;
    private int mCurrTemp = 16;
    //展示
    @Bind(R.id.text_show)
    TextView mShowTv;
    //模式
    @Bind(R.id.text_mode)
    TextView mModeTv;
    //风量
    @Bind(R.id.text_wind_count)
    TextView mWindCountTv;
    //风向
    @Bind(R.id.text_wind_dir)
    TextView mWindDirTv;
    //温度进度条
    @Bind(R.id.temp_seekBar)
    SeekBar mTempSb;
    //模式进度条
    @Bind(R.id.air_mode)
    ImageButton mAirModeIBtn;
    //风速
    @Bind(R.id.air_speed)
    ImageButton mAirSpeed;
    //风向
    @Bind(R.id.air_wind)
    ImageButton mAirWind;
    //重新匹配
    @Bind(R.id.b_rep)
    Button mRepBtn;
    //电源
    @Bind(R.id.air_power)
    Button mPowerBtn;

    private ApplianceSynnResultListener mApplianceSynnResultListener = new ApplianceSynnResultListener();

    private int Power, ModeIndex, Wind_count, Wind_dir, Tmp;
    private String[] mModeList = {ApplianceConstants.Air.COMMAND_AUTO,//自动
            ApplianceConstants.Air.COMMAND_REFRIGERATION,//制冷
            ApplianceConstants.Air.COMMAND_DRY,//抽湿
            ApplianceConstants.Air.COMMAND_BLAST,//送风
            ApplianceConstants.Air.COMMAND_HEATING//制热
    };

    String mWindCount = null;
    String mWindDir = null;
    String mModeName = null;
    private int mCurrentWindCount = 1;
    private int mCurrentWindDir = 1;
    private boolean isWindCountMax = false;

    private String mKey = null;
    private String mValue = null;
    private String mState = null;
    private String mAction = null;
    private String mMode = null;
    private String mPowerState = null;

    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            init();
            hideLoading();
        }
    };

    // ===========================================================
    // Constructors
    // ===========================================================


    // ===========================================================
    // Getter or Setter
    // ===========================================================


    // ===========================================================
    // Methods for/from SuperClass/Interfaces
    // ===========================================================
    @Override
    protected int onSetContentView() {
        return R.layout.activity_gowild_appliances_air;
    }

    @Override
    protected void initView() {
        super.initView();
        mTempSb.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onStopTrackingTouch(SeekBar seek) {
                mState = null;
                mAction = null;
                mKey = null;
                mValue = null;
                mMode = null;
                mCurrTemp = seek.getProgress() + 16;//最低16度
                mValue = String.valueOf(mCurrTemp);
                mKey = ApplianceConstants.Air.COMMAND_TEMP;
                mAction = ApplianceConstants.Air.COMMAND_TO;
                sendToRobot(ApplianceConstants.Air.MESSAGE_CONTROL);
            }

            @Override
            public void onStartTrackingTouch(SeekBar arg0) {

            }

            @Override
            public void onProgressChanged(SeekBar arg0, int progress, boolean arg2) {
                mShowTv.setText(progress + 16 + "℃");
            }
        });
    }

    @Override
    protected void initEvent() {
        Bundle bundle = getIntent().getExtras();
        if (null != bundle) {
            mApplianceId = bundle.getString(APPLIANCE_ID);
            mApplianceNikeName = bundle.getString(APPLIANCE_NIKENAME);
            mSceneTypeId = bundle.getInt(SCENE_TYPE_ID);
        }
        setTitle(mApplianceNikeName);
        sendToRobot(ApplianceConstants.Air.MESSAGE_SYN);
    }

    @Override
    protected void onResume() {
        super.onResume();
        NettyClientManager.getInstance().addHandlerListener(GowildResponseCode.APPLIANCE_CONTROL_RESULT, mApplianceSynnResultListener);
    }

    @Override
    protected void onPause() {
        super.onPause();
        NettyClientManager.getInstance().removeHandlerListener(GowildResponseCode.APPLIANCE_CONTROL_RESULT, mApplianceSynnResultListener);
    }

    /**
     * 点击事件
     */
    @OnClick({R.id.air_power, R.id.air_mode, R.id.air_speed, R.id.air_wind, R.id.b_rep})
    public void onclick(View view) {
        //每次点击都置空 ,每次按键不影响后续操作
        mState = null;
        mAction = null;
        mKey = null;
        mValue = null;
        mMode = null;
        switch (view.getId()) {
            case R.id.air_power://电源
                if (mPowerState == ApplianceConstants.Air.COMMAND_STAEON) {
                    mState = mPowerState = ApplianceConstants.Air.COMMAND_STAEOFF;
                } else if (mPowerState == ApplianceConstants.Air.COMMAND_STAEOFF) {
                    mState = mPowerState = ApplianceConstants.Air.COMMAND_STAEON;
                }
                break;
            // mode 固定的参数
            case R.id.air_mode://模式
                int modeCount = mModeList.length;
                Log.e("ModeIndex", "" + ModeIndex);
//                mModeName = getModeNameByIndex(ModeIndex);
//                mModeTv.setText(mModeName);
                if (ModeIndex >= modeCount ) {//如果已经到尾，从头开始
                    ModeIndex = 1;
                } else {
                    ModeIndex++;
                }
                mMode = mModeList[ModeIndex-1]; //1-5
                break;
            //
            case R.id.air_speed://风速

                if (mCurrentWindCount == 1) {
                    isWindCountMax = false;
                } else if (mCurrentWindCount == 4) {
                    isWindCountMax = true;
                }

                if (isWindCountMax) {
                    mAction = ApplianceConstants.Air.COMMAND_REDUCE;
                } else {
                    mAction = ApplianceConstants.Air.COMMAND_ADD;
                }

                mKey = ApplianceConstants.Air.COMMAND_SPEED;
                break;
            case R.id.air_wind://风向
                //如果从1到4，调用add
                //如果从4到1，调用reduce
//                switch (mCurrentWindDir) {
//                    case 0:
//                        mState = ApplianceConstants.Air.COMMAND_STAEON;
//                        break;
//                    case 1:
//                        mState = ApplianceConstants.Air.COMMAND_STAEOFF;
//                        break;
////                    case 4:
////                        mMode = ApplianceConstants.Air.COMMAND_SWING_UP_DOWN;
////                        break;
////                    case 3:
////                        mMode = ApplianceConstants.Air.COMMAND_SWING_LEFT_RIGHT;
////                        break;
//
//                }
//                mMode = ApplianceConstants.Air.COMMAND_SWING_UP_DOWN;
//                mState  = ApplianceConstants.Air.COMMAND_STAEON;

//                mWindDir = getWindDir(mCurrentWindDir);
//                mWindDirTv.setText(mWindDir);
                break;
            case R.id.b_rep:
                Bundle bundle = new Bundle();
                bundle.putInt(GowildApplianceSearchActivity.INTENT_EXTRA_SCENE, mSceneTypeId);
                bundle.putInt(GowildApplianceSearchActivity.INTENT_EXTRA_DEVICE_TYPE, DEVICE_TYPE_AIR);
                startActivity(GowildApplianceBrandActivity.class, bundle);
                break;
        }
        //发送指令到服务器
        sendToRobot(ApplianceConstants.Air.MESSAGE_CONTROL);
    }

    // ===========================================================
    // Methods
    // ===========================================================
    public void sendToRobot(int messageType) {
//        showLoading("正在发送...");
        try {
            JSONObject result = new JSONObject();
            result.put("head", ApplianceConstants.Air.APPLIANCES_AIR); //消息头300
            result.put("messageType", messageType);//消息类型
            switch (messageType) {
                case ApplianceConstants.Air.MESSAGE_SYN: //同步消息 ,刚进界面就显示
                    result.put("sceneTypeID", mSceneTypeId);
                    break;
                case ApplianceConstants.Air.MESSAGE_CONTROL: //控制设备（语义端支持复合式指令 为了和语义端保持一致所以这里使用json数组的形式）
                    JSONArray jsonArray = new JSONArray();
                    JSONObject json = new JSONObject();
                    json.put("na", "AIR"); //这个目前是定死的（目前只有空调）
                    json.put("st", mState);// 设备状态 根据上面定义参数填
                    json.put("cm", mAction); //设备动作 同上
                    json.put("at", mKey);//设备属性 同上
                    json.put("um", mValue);// 设备属性值  同上
                    json.put("mo", mMode);// 设备模式  同上
                    jsonArray.add(json);
                    result.put("data", jsonArray);
                    break;
                default:
                    return;
            }
            ApplianceManager.getInstance().controlAppliance(result.toJSONString());
        } catch (Exception e) {
        }
    }

    // 初始化
    public void init() {
        if (Power == 0) {
            // t_mode.setText(getString(R.string.air_mode_val));
            // t_wind_dir.setText(getString(R.string.air_wind_dir_val));
            // t_wind_count.setText(getString(R.string.air_wind_val));
            mModeTv.setText("");
            mWindCountTv.setText("");
            mWindDirTv.setText("");
            mShowTv.setText("");
            mPowerState = ApplianceConstants.Air.COMMAND_STAEOFF;
            mPowerBtn.setText("开机");
            return;
        } else {
            mPowerState = ApplianceConstants.Air.COMMAND_STAEON;
            mPowerBtn.setText("关机");
        }

        mModeName = getModeNameByIndex(ModeIndex);

        mWindCount = getWindCount(Wind_count);
        mCurrentWindCount = Wind_count;

        mWindDir = getWindDir(Wind_dir);
        mCurrentWindDir = Wind_dir;

        mTempSb.setProgress(Tmp - 16);
        mShowTv.setText(String.valueOf(Tmp) + getString(R.string.d));
        mModeTv.setText(mModeName);
        mWindDirTv.setText(mWindDir);
        mWindCountTv.setText(mWindCount);
    }

    /**
     * 根据索引获得风向
     *
     * @param index
     * @return
     */
    private String getWindDir(int index) {
        String tempWindDir = null;
        switch (index) {
            case 0:
                tempWindDir = getString(R.string.air_wind_dir_value_1);
                break;
            case 1:
                tempWindDir = getString(R.string.air_wind_dir_value_2);
                break;
        }
        return tempWindDir;
    }

    /**
     * 根据模式的下标获取模式名称
     *
     * @param index
     * @return
     */
    private String getModeNameByIndex(int index) {
        String TempName = null;
        switch (index) {
            case 1:
                TempName = getString(R.string.air_mode_value_1);
                break;
            case 2:
                TempName = getString(R.string.air_mode_value_2);
                break;
            case 3:
                TempName = getString(R.string.air_mode_value_3);
                break;
            case 4:
                TempName = getString(R.string.air_mode_value_4);
                break;
            case 5:
                TempName = getString(R.string.air_mode_value_5);
                break;
            default:
                TempName = getString(R.string.air_mode_val);
                break;
        }
        return TempName;
    }

    /**
     * 获得风速根据索引
     *
     * @param index
     * @return
     */
    private String getWindCount(int index) {
        String tempWindCount = null;
        switch (index) {
            case 1:
                tempWindCount = getString(R.string.air_wind_count_value_1);
                break;
            case 2:
                tempWindCount = getString(R.string.air_wind_count_value_2);
                break;
            case 3:
                tempWindCount = getString(R.string.air_wind_count_value_3);
                break;
            case 4:
                tempWindCount = getString(R.string.air_wind_count_value_4);
                break;
            default:
                tempWindCount = getString(R.string.air_wind_val);
                break;
        }
        return tempWindCount;
    }

    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================
    private class ApplianceSynnResultListener implements IDataListener<StringMsgPro.StringMsg> {

        @Override
        public void onReceiveData(StringMsgPro.StringMsg data) {
            Logger.e(TAG, "ApplianceSynnResultListener onReceiveData() data = " + data.getValue());

            //解析数据 包含 当前的空调状态
            String str = data.getValue();
            try {
                JSONObject json = JSON.parseObject(str);
                //电源
                Power = json.getInteger("power");
                //模式
                ModeIndex = json.getInteger("mode");
                //风速
                Wind_count = json.getInteger("windcount");
                //风向
                Wind_dir = json.getInteger("dir");
                //温度
                Tmp = json.getInteger("tmp");
            } catch (Exception e) {
                e.printStackTrace();
            }
            Message msg = mHandler.obtainMessage();
            msg.what = 1;
            mHandler.sendMessage(msg);


        }

        @Override
        public void onActiveChanged(boolean isActive) {
            Logger.e(TAG, "ApplianceSynnResultListener onActiveChanged() isActive = " + isActive);
        }
    }
}
