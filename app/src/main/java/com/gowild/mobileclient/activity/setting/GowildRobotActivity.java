/**
 * Copyright (C)  2016 深圳市狗尾草智能科技有限公司
 * GowildXB
 * GowildRobotActivity.java
 */
package com.gowild.mobileclient.activity.setting;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.gowild.mobileclient.R;
import com.gowild.mobileclient.activity.base.GowildHasTitileActivity;
import com.gowild.mobileclient.config.KeysContainer;

import butterknife.Bind;
import butterknife.OnClick;

/**
 * @author Zhangct
 * @version 1.0
 *          <p> 主要功能介绍 </p>
 * @since 2016/8/6 15:56
 */
public class GowildRobotActivity extends GowildHasTitileActivity {

    // ===========================================================
    // HostConstants
    // ===========================================================

    private static final String TAG = GowildRobotActivity.class.getSimpleName();

    // ===========================================================
    // Static Fields
    // ===========================================================

    // ===========================================================
    // Fields
    // ===========================================================

    @Bind(R.id.btn_goto_config)
    Button mGowildToConfigBtn;
    // ===========================================================
    // Constructors
    // ===========================================================


    // ===========================================================
    // Getter or Setter
    // ===========================================================


    // ===========================================================
    // Methods for/from SuperClass/Interfaces
    // ===========================================================


    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected int onSetContentView() {
        return R.layout.activity_gowild_setting_robot;
    }

    @Override
    protected void initEvent() {
        String title = getString(R.string.gowild_setting_robot_config);
        Bundle bundle = getIntent().getExtras();
        if (null != bundle) {
            title = bundle.getString(KeysContainer.KEY_ROBOT_BIND_TITLE);
        }
        setTitle(title);
    }

    @OnClick({R.id.btn_goto_config})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_goto_config:
                //去设置
                startActivity(GowildWifiConfigActivity.class, getIntent().getExtras());
                break;
            default:
                break;
        }
    }

    // ===========================================================
    // Methods
    // ===========================================================


    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================

}
