/**
 * Copyright (C)  2016 深圳市狗尾草智能科技有限公司
 * GowildXB
 * GowildAddOrUpdateAlertActivity.java
 */
package com.gowild.mobileclient.activity.alertandclock;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.gowild.gwais.libwheelview.adapter.ArrayWheelAdapter;
import com.gowild.gwais.libwheelview.widght.WheelView;
import com.gowild.mobile.libcommon.utils.TimeUtil;
import com.gowild.mobileclient.R;
import com.gowild.mobileclient.base.baseactivity.GowildBaseActivity;
import com.gowild.mobileclient.config.Constants;
import com.gowild.mobileclient.manager.GowildAsyncHttpResponseHandler;
import com.gowild.mobileclient.manager.GowildHttpManager;
import com.gowild.mobileclient.vo.Alert;
import com.gowild.mobileclient.vo.RequestCreateAlert;
import com.gowild.mobileclient.vo.RequestUpdateAlert;
import com.gowild.mobileclient.vo.ResponseCreateAlert;
import com.gowild.mobileclient.vo.ResponseUpdateAlert;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import static com.gowild.mobileclient.R.id.date;
import static com.gowild.mobileclient.R.id.hour;

/**
 * @author JunDong Wang
 * @version 1.0
 *          <p><strong>待办添加或者修改的界面</strong></p>
 * @since 2016/7/23 18:34
 */
public class GowildAddOrUpdateAlertActivity extends GowildBaseActivity {

    // ===========================================================
    // HostConstants
    // ===========================================================

    private static final String TAG = GowildAddOrUpdateAlertActivity.class.getSimpleName();
    TextView mContent;
    private View mBack;
    private int mCreateType;
    WheelView mDateWheel;
    WheelView mMinteWheel;
    WheelView mHourWheel;
    private OnClickCallback mOnClickCallback = new OnClickCallback();
    private RequestCreateAlertCallback mRequestCreateAlertCallback;
    private RequestUpdateAlertCallback mRequestUpdateAlertCallback;
    ArrayList<String> mMinteList = new ArrayList<>();
    ArrayList<String> mDateList = new ArrayList<>();
    ArrayList<String> mHourList = new ArrayList<>();
    private Alert mAlert = new Alert();
    private TextView mTvTitle;
    // ===========================================================
    // Static Fields
    // ===========================================================

    // ===========================================================
    // Fields
    // ===========================================================


    // ===========================================================
    // Constructors
    // ===========================================================


    // ===========================================================
    // Getter or Setter
    // ===========================================================


    // ===========================================================
    // Methods for/from SuperClass/Interfaces
    // ===========================================================
    @Override
    protected int onSetTopbarView() {
        return R.layout.activity_gowild_add_update_alert_topbar;
    }

    @Override
    protected int onSetContentView() {
        return R.layout.activity_gowild_add_update_alert;
    }

    @Override
    protected int onSetTopBarHeight() {
        return getResources().getDimensionPixelSize(R.dimen.gowild_160px);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBack = findViewById(R.id.back);
        mTvTitle = (TextView) findViewById(R.id.title);
        mBack.setOnClickListener(mOnClickCallback);
        mDateWheel = (WheelView) findViewById(date);
        mMinteWheel = (WheelView) findViewById(R.id.minte);
        mHourWheel = (WheelView) findViewById(hour);
        mContent = (TextView) findViewById(R.id.content);
        RelativeLayout rltTag = (RelativeLayout) findViewById(R.id.tag);
        rltTag.setOnClickListener(mOnClickCallback);
        RelativeLayout mBack = (RelativeLayout) findViewById(R.id.back);
        mBack.setOnClickListener(mOnClickCallback);
        TextView commit = (TextView) findViewById(R.id.commit);
        commit.setOnClickListener(mOnClickCallback);
        mCreateType = getIntent().getIntExtra(Constants.TYPE, -1);
        mRequestCreateAlertCallback = new RequestCreateAlertCallback(this);
        mRequestUpdateAlertCallback = new RequestUpdateAlertCallback(this);
        mAlert = new Alert();
        Log.d(TAG, "onResume");
        if (mCreateType == GowildAlertOrClockActivity.TYPE_UPDATE) {
            mTvTitle.setText("修改待办");
            mAlert.mRemindId = getIntent().getIntExtra(Constants.REMIND_ID, -1);
            mAlert.mRemindTime = getIntent().getStringExtra(Constants.REMIND_TIME);
            mAlert.mContent = getIntent().getStringExtra(Constants.CONTENT);
            if (!TextUtils.isEmpty(mAlert.mContent)) {
                mContent.setText(mAlert.mContent);
            } else {
                mContent.setText("起床");
            }
        } else {
            mTvTitle.setText("添加待办");

        }
        initDateAndWeek();
        initHour();
        initMinute();
    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    @Override
    public void onBackPressed() {
        finish();
    }

    // ===========================================================
    // Methods
    // ===========================================================


    private void initDateAndWeek() {
        mDateList.clear();
        int dateInterval = 0;
        Calendar calendar = Calendar.getInstance();
        //处理日期的问题
        if (mCreateType == GowildAlertOrClockActivity.TYPE_ADD) {
            for (int i = 0; i < getDay(calendar.getTime().getYear()); i++) {
                mDateList.add(TimeUtil.getTimeFromDateAndTimeType(calendar.getTime(), "M.dd    E"));
                calendar.add(Calendar.DAY_OF_YEAR, 1);
            }
        } else if (mCreateType == GowildAlertOrClockActivity.TYPE_UPDATE) {
            for (int i = 0; i < getDay(calendar.getTime().getYear()); i++) {
                mDateList.add(TimeUtil.getTimeFromDateAndTimeType(calendar.getTime(), "M.dd    E"));
                calendar.add(Calendar.DAY_OF_YEAR, 1);
            }
            dateInterval = getDateInterval(mAlert.mRemindTime);
        }
        String[] array = mDateList.toArray(new String[]{});
        ArrayWheelAdapter<String> wheelAdapter = new ArrayWheelAdapter<>(this, array);
        wheelAdapter.setTextSize(18);
        wheelAdapter.setTextColor(getResources().getColor(R.color.gowild_alert_clock_title));
        mDateWheel.setCyclic(true);
        mDateWheel.setViewAdapter(wheelAdapter);
        mDateWheel.setCurrentItem(dateInterval);
    }

    private int getDay(int year) {
        if ((year + 1900) % 4 == 0) {
            return 366;
        } else {
            return 365;
        }
    }

    private void initHour() {
        mHourList.clear();
        Calendar calendar = Calendar.getInstance();
        Date date = calendar.getTime();
        int hour = date.getHours();
        if (mCreateType == GowildAlertOrClockActivity.TYPE_ADD) {
            for (int i = 0; i < 24; i++) {
                mHourList.add(Integer.toString(i));
            }
        } else if (mCreateType == GowildAlertOrClockActivity.TYPE_UPDATE) {
            for (int i = 0; i < 24; i++) {
                mHourList.add(Integer.toString(i));
            }
            hour = getHour(mAlert.mRemindTime);
        }
        String[] array = mHourList.toArray(new String[]{});
        ArrayWheelAdapter<String> wheelAdapter = new ArrayWheelAdapter<>(this, array);
        wheelAdapter.setTextSize(18);
        mHourWheel.setCyclic(true);
        mHourWheel.setViewAdapter(wheelAdapter);
        wheelAdapter.setTextColor(getResources().getColor(R.color.gowild_alert_clock_title));
        mHourWheel.setCurrentItem(hour);
    }

    private int getHour(String date) {
        String time = date.split(" ")[1];
        int hour = Integer.parseInt(time.split(":")[0]);
        return hour;
    }

    private int getMinute(String date) {
        String time = date.split(" ")[1];
        int minute = Integer.parseInt(time.split(":")[1]);
        return minute;
    }

    private int getDateInterval(String datetime) {
        int interval = TimeUtil.dateCompare(new Date(),TimeUtil.parseDate(datetime));
        return interval;
    }

    private void initMinute() {
        mMinteList.clear();
        Calendar calendar = Calendar.getInstance();
        Date date = calendar.getTime();
        int minte = date.getMinutes();
        if (mCreateType == GowildAlertOrClockActivity.TYPE_ADD) {
            for (int i = 0; i < 60; i++) {
                mMinteList.add(Integer.toString(i));
            }
        } else if (mCreateType == GowildAlertOrClockActivity.TYPE_UPDATE) {
            for (int i = 0; i < 60; i++) {
                mMinteList.add(Integer.toString(i));
            }
            minte = getMinute(mAlert.mRemindTime);
        }
        String[] array = mMinteList.toArray(new String[]{});
        ArrayWheelAdapter<String> wheelAdapter = new ArrayWheelAdapter<>(this, array);
        wheelAdapter.setTextSize(18);
        wheelAdapter.setTextColor(getResources().getColor(R.color.gowild_alert_clock_title));
        mMinteWheel.setCyclic(true);
        mMinteWheel.setViewAdapter(wheelAdapter);
        mMinteWheel.setCurrentItem(minte);
    }


    private void commit() {
        Calendar calendar = Calendar.getInstance();
        Date nowDate = calendar.getTime();
        Alert alert = new Alert();
        alert.mContent = mContent.getText().toString();
        Date time = TimeUtil.parseDateAndTimeFromType(mDateList.get(mDateWheel.getCurrentItem()), "M.dd    E");
        time.setYear(nowDate.getYear());
        time.setHours(Integer.valueOf(mHourList.get(mHourWheel.getCurrentItem())));
        time.setMinutes(Integer.valueOf(mMinteList.get(mMinteWheel.getCurrentItem())));
        alert.mRemindTime = TimeUtil.getTimeFromDateAndTimeType(time, "yyyy-MM-dd HH:mm");
        if (mCreateType == GowildAlertOrClockActivity.TYPE_ADD) {
            RequestCreateAlert requestCreateAlert = new RequestCreateAlert();
            requestCreateAlert.put(Constants.REMIND_TIME, alert.mRemindTime);
            requestCreateAlert.put(Constants.CONTENT, alert.mContent);
            showLoading(R.string.loading);
            GowildHttpManager.requestCreateAlert(this, requestCreateAlert, mRequestCreateAlertCallback);
        } else if (mCreateType == GowildAlertOrClockActivity.TYPE_UPDATE) {
            mAlert.mRemindTime = TimeUtil.getTimeFromDateAndTimeType(time, "yyyy-MM-dd HH:mm");
            mAlert.mContent = mContent.getText().toString();
            RequestUpdateAlert requestUpdateAlert = new RequestUpdateAlert();
            requestUpdateAlert.put(Constants.REMIND_TIME, mAlert.mRemindTime);
            requestUpdateAlert.put(Constants.CONTENT, mAlert.mContent);
            requestUpdateAlert.put(Constants.REMIND_ID, Integer.toString(mAlert.mRemindId));
            showLoading(R.string.loading);
            GowildHttpManager.requestUpdateAlert(this, requestUpdateAlert, mRequestUpdateAlertCallback);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        mContent.setText(data.getStringExtra(Constants.CONTENT));
    }

    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================
    private class OnClickCallback implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.tag:
                    Intent intent = new Intent(GowildAddOrUpdateAlertActivity.this, GowildAlertOrClockTagsActivity.class);
                    intent.putExtra(Constants.CONTENT, mContent.getText().toString());
                    intent.putExtra(Constants.TYPE, "alert");
                    startActivityForResult(intent, 1);
                    break;
                case R.id.back:
                    onBackPressed();
                    break;
                case R.id.commit:
                    if (TextUtils.isEmpty(mContent.getText().toString().trim())) {
                        mContent.setText("开会");
                    }
                    commit();
                    break;
            }
        }
    }

    private class RequestCreateAlertCallback extends GowildAsyncHttpResponseHandler<ResponseCreateAlert> {

        /**
         * 构造方法
         *
         * @param context 用于处理授权过期消息，会被转成ApplicationContext
         */
        public RequestCreateAlertCallback(Context context) {
            super(context);
        }

        @Override
        public void onSuccess(int statusCode, ResponseCreateAlert responseBody) {
            hideLoading();
            if (statusCode == 100) {
                onBackPressed();
            }
        }

        @Override
        public void onFailure(int statusCode, String responseBody) {
            hideLoading();
        }
    }

    private class RequestUpdateAlertCallback extends GowildAsyncHttpResponseHandler<ResponseUpdateAlert> {

        /**
         * 构造方法
         *
         * @param context 用于处理授权过期消息，会被转成ApplicationContext
         */
        public RequestUpdateAlertCallback(Context context) {
            super(context);
        }

        @Override
        public void onSuccess(int statusCode, ResponseUpdateAlert responseBody) {
            hideLoading();
            if (statusCode == 100) {
                onBackPressed();
            }
        }

        @Override
        public void onFailure(int statusCode, String responseBody) {
            hideLoading();
        }
    }
}
