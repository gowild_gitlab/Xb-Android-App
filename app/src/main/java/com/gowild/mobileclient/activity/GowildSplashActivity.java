package com.gowild.mobileclient.activity;

import android.content.Intent;
import android.view.View;

import com.gowild.mobileclient.R;
import com.gowild.mobileclient.activity.base.GowildNoTitileActivity;
import com.gowild.mobileclient.activity.login.GowildCaptchaActivity;
import com.gowild.mobileclient.activity.login.GowildLunchActivity;
import com.gowild.mobileclient.config.Constants;

import butterknife.OnClick;

/**
 * @author shuai. wang
 * @version 1.0
 * @since ${DATE} ${TIME}
 * <p><strong>Features draft description.初始化界面</strong></p>
 */
public class GowildSplashActivity extends GowildNoTitileActivity {
    // ===========================================================
    // Fields
    // ===========================================================

    // ===========================================================
    // Methods for/from SuperClass/Interfaces
    // ===========================================================
    private static final String TAG = GowildSplashActivity.class.getSimpleName();


    @Override
    public int getLayout() {
        return R.layout.activity_gowild_splash;
    }

    @Override
    protected void initView() {
    }

    @Override
    protected void initData() {
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void initEvent() {
    }

    // ===========================================================
    // Methods
    // ===========================================================
    @OnClick({R.id.gowild_splash_login, R.id.gowild_splash_start})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.gowild_splash_start:
                //点击开始
                nextActivtiy(GowildCaptchaActivity.class);
                break;
            case R.id.gowild_splash_login:
                nextActivtiy(GowildLunchActivity.class);
                break;
            default:
                break;
        }
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    /**
     * 开启下一个activity,下一步
     */
    @Override
    public void nextActivtiy(Class clazz) {
        Intent intent = new Intent(GowildSplashActivity.this, clazz);
        intent.putExtra(Constants.CLASS, TAG);
        startActivity(intent);
    }
}
