/**
 * Copyright (C)  2016 深圳市狗尾草智能科技有限公司
 * GowildXB
 * GowildLoveTalkActivity.java
 */
package com.gowild.mobileclient.activity.love;

import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.alibaba.fastjson.JSON;
import com.gowild.mobile.libcommon.utils.SpUtils;
import com.gowild.mobile.libcommon.utils.ToastUtils;
import com.gowild.mobileclient.R;
import com.gowild.mobileclient.activity.base.GowildLoginAndRegisterActivity;
import com.gowild.mobileclient.config.Constants;
import com.gowild.mobileclient.manager.GowildAsyncHttpResponseHandler;
import com.gowild.mobileclient.manager.GowildHttpManager;
import com.gowild.mobileclient.vo.TalkMessage;
import com.gowild.mobileclient.vo.TalkMessageEntry;
import com.gowild.mobileclient.widget.MyToolBar;
import com.gowild.mobileclient.widget.PullToRefreshLayout;
import com.gowild.mobileclient.widget.PullableListView;
import com.loopj.android.http.RequestParams;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;



/**
 * @author shuai. wang
 * @version 1.0
 *          <p><strong>Features draft description.主要功能介绍</strong></p>
 * @since 2016/7/26 20:40
 */
public class GowildLoveTalkActivity extends GowildLoginAndRegisterActivity {

    // ===========================================================
    // HostConstants
    // ===========================================================

    private static final String TAG = GowildLoveTalkActivity.class.getSimpleName();
    private static final int TYPE_MINE = 0;
    private static final int TYPE_OTHER = 1;
    private static final int TYPE_COUNT = 4;
    private static final int TYPE_FRIST = 3;
    @Bind(R.id.gowild_lover_lv)
    PullableListView mGowildLoverLv;
    @Bind(R.id.refresh_view)
    PullToRefreshLayout mPrltRefresh;
    private List<TalkMessageEntry> mList = new ArrayList<>();
    private MyAdapter mAdapter;

    public boolean hasNextPage = true;


    @Override
    public int getLayout() {
        return R.layout.activity_gowild_love_talk;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    protected void initView() {
        MyToolBar toolbar = (MyToolBar) findViewById(R.id.view_toolbar);
        toolbar.setTitle(R.string.gowild_lovertalk_title);
    }

    @Override
    protected void initData() {
        //加载数据
        loadDataFormNet(1);

    }

    int index = 2;
    int type = 1;

    private void loadDataFormNet(int fresh) {
        showLoading(R.string.loading);
        RequestParams params = new RequestParams(Constants.MAGICCODE, SpUtils.getLong(this, Constants.MAGICCODE));
//			pageNum
//			Integer	第几页	默认1
        params.put("pageSize",20);
        if (!hasNextPage && fresh == 0) {
            hideLoading();
            ToastUtils.showCustom(GowildLoveTalkActivity.this, "没有更多数据", false);
            mPrltRefresh.refreshFinish(PullToRefreshLayout.SUCCEED);
            return;
        }
        if (fresh == 1) {
            //刷新数据
            mList.clear();
            if (mAdapter != null) {
                mAdapter.notifyDataSetChanged();
            }
            params.add("pageNum", 1 + "");
        } else {
            //加载更多
            params.add("pageNum", index + "");
        }
        index++;
        GowildHttpManager.getTalkMsg(this, params, new GowildAsyncHttpResponseHandler<TalkMessage>(GowildLoveTalkActivity.this) {

            @Override
            public void onSuccess(int statusCode, TalkMessage responseBody) {
                hideLoading();
                hasNextPage = responseBody.hasNextPage;//是否有下一页
                for (Object obj : responseBody.list) {
                    TalkMessageEntry talkMessageEntry = JSON.parseObject(obj.toString(), TalkMessageEntry.class);
                    mList.add(talkMessageEntry);
                }
                mAdapter.notifyDataSetChanged();
                if (type == 1) {
                    mPrltRefresh.refreshFinish(PullToRefreshLayout.SUCCEED);
                }

                if (type == 0) {
                    mPrltRefresh.loadmoreFinish(PullToRefreshLayout.SUCCEED);
                }
            }

            @Override
            public void onFailure(int statusCode, String responseBody) {
                hideLoading();
//                if (TextUtils.isEmpty(responseBody)){
//                    return;
//                }

                if (type == 1) {
                    mPrltRefresh.refreshFinish(PullToRefreshLayout.FAIL);
                }
                if (type == 0) {
                    mPrltRefresh.loadmoreFinish(PullToRefreshLayout.FAIL);
                }
            }
        });
    }

    @Override
    protected void initEvent() {
        if (mAdapter == null) {
            mAdapter = new MyAdapter();
        }
        mGowildLoverLv.setAdapter(mAdapter);
        mAdapter.notifyDataSetChanged();
        mPrltRefresh.setOnRefreshListener(new PullToRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh(PullToRefreshLayout pullToRefreshLayout) {
                type = 1;
                loadDataFormNet(1);
            }

            @Override
            public void onLoadMore(PullToRefreshLayout pullToRefreshLayout) {
                //加载更多
                type = 0;
                loadDataFormNet(0);
            }
        });
    }


    private class MyAdapter extends BaseAdapter {


        @Override
        public int getCount() {
            return mList.size();
        }

        public class MineHolder {
            public TextView mTvTime;
            public TextView mTvName;
            public TextView mTvDes;

            public MineHolder(View view) {
                mTvTime = (TextView) view.findViewById(R.id.gowild_lover_tv_talk_time);
                mTvName = (TextView) view.findViewById(R.id.gowild_lover_tv_talk_name);
                mTvDes = (TextView) view.findViewById(R.id.gowild_lover_tv_talk_des);
            }
        }

        public class OtherHolder {
            public TextView mTvTime;
            public TextView mTvName;
            public TextView mTvDes;

            public OtherHolder(View view) {
                mTvTime = (TextView) view.findViewById(R.id.gowild_lover_tv_talk_time);
                mTvName = (TextView) view.findViewById(R.id.gowild_lover_tv_talk_name);
                mTvDes = (TextView) view.findViewById(R.id.gowild_lover_tv_talk_des);
            }
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            TalkMessageEntry bean = mList.get(position);
            int currentType = getItemViewType(position);
            if (currentType == TYPE_FRIST) {
                convertView = View.inflate(GowildLoveTalkActivity.this, R.layout.view_gowild_lover_frist, null);
                TextView tvTime = (TextView) convertView.findViewById(R.id.gowild_lover_tv_talk_time);
                TextView tvDes = (TextView) convertView.findViewById(R.id.gowild_lover_tv_talk_des);
                tvTime.setText(bean.createTime);
                tvDes.setText(bean.content);
            } else if (currentType == TYPE_MINE) {
                MineHolder holder = null;
                if (convertView == null) {
                    convertView = View.inflate(GowildLoveTalkActivity.this, R.layout.view_gowild_lover_talk, null);
                    holder = new MineHolder(convertView);
                    convertView.setTag(holder);
                } else {
                    holder = (MineHolder) convertView.getTag();
                }
                holder.mTvTime.setText(bean.createTime);
                holder.mTvDes.setText(bean.content);
                holder.mTvName.setText(bean.sendNickName);
            } else {
                OtherHolder otherholder = null;
                if (convertView == null) {
                    convertView = View.inflate(GowildLoveTalkActivity.this, R.layout.view_gowild_lover_talk_right, null);
                    otherholder = new OtherHolder(convertView);
                    convertView.setTag(otherholder);
                } else {
                    otherholder = (OtherHolder) convertView.getTag();
                }
                otherholder.mTvTime.setText(bean.createTime);
                otherholder.mTvDes.setText(bean.content);
                otherholder.mTvName.setText(bean.sendNickName);
            }
            return convertView;
        }

        @Override
        public Object getItem(int position) {
            return mList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }


        @Override
        public int getItemViewType(int position) {
            if (mList.get(position).content.contains("建立了配对关系")) {
                return TYPE_FRIST;
            } else {
                if (SpUtils.getString(GowildLoveTalkActivity.this, Constants.USERNAME).equals(mList.get(position).sendId)) {
                    return TYPE_MINE;// 自己发送的
                } else {
                    return TYPE_OTHER;
                }
            }

        }

        @Override
        public int getViewTypeCount() {
            return TYPE_COUNT;
        }

    }


    // ===========================================================
    // Static Fields
    // ===========================================================

    // ===========================================================
    // Fields
    // ===========================================================


    // ===========================================================
    // Constructors
    // ===========================================================


    // ===========================================================
    // Getter or Setter
    // ===========================================================


    // ===========================================================
    // Methods for/from SuperClass/Interfaces
    // ===========================================================


    // ===========================================================
    // Methods
    // ===========================================================


    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================

}
