/**
 * Copyright (C)  2016 深圳市狗尾草智能科技有限公司
 * GowildXB
 * GowildUserProActivity.java
 */
package com.gowild.mobileclient.activity;

import android.os.Bundle;
import android.widget.TextView;

import com.gowild.mobileclient.R;
import com.gowild.mobileclient.activity.base.GowildLoginAndRegisterActivity;
import com.gowild.mobileclient.widget.MyToolBar;

import java.io.IOException;
import java.io.InputStream;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * @author shuai. wang
 * @version 1.0
 *          <p><strong>Features draft description.主要功能介绍</strong></p>
 * @since 2016/8/16 14:37
 */
public class GowildUserProActivity extends GowildLoginAndRegisterActivity {

    // ===========================================================
    // HostConstants
    // ===========================================================

    private static final String TAG = GowildUserProActivity.class.getSimpleName();
    @Bind(R.id.userpro_tv)
    TextView userproTv;

    @Override
    public int getLayout() {
        return R.layout.activity_gowild_userpro;
    }

    @Override
    protected void initData() {

        MyToolBar toolbar = (MyToolBar) findViewById(R.id.view_toolbar);
        toolbar.setTitle(getString(R.string.gowild_user_title));
        InputStream is = null;
        try {
            is = getAssets().open("robot.txt");
            int size = is.available();

            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();

            String text = new String(buffer, "UTF-8");

            userproTv.setText(text);
        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    @Override
    protected void initEvent() {
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
    }

    // ===========================================================
    // Static Fields
    // ===========================================================

    // ===========================================================
    // Fields
    // ===========================================================


    // ===========================================================
    // Constructors
    // ===========================================================


    // ===========================================================
    // Getter or Setter
    // ===========================================================


    // ===========================================================
    // Methods for/from SuperClass/Interfaces
    // ===========================================================


    // ===========================================================
    // Methods
    // ===========================================================


    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================

}
