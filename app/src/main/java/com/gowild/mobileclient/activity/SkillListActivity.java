package com.gowild.mobileclient.activity;

import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.gowild.mobileclient.R;
import com.gowild.mobileclient.adapter.SkillRecyclerAdapter;
import com.gowild.mobileclient.base.baseactivity.GowildBaseActivity;
import com.gowild.mobileclient.manager.GowildAsyncHttpResponseHandler;
import com.gowild.mobileclient.manager.GowildHttpManager;
import com.gowild.mobileclient.model.SkillBack;
import com.gowild.mobileclient.model.SkillTitleInfo;
import com.gowild.mobileclient.utils.CommonDataUtil;
import com.gowild.mobileclient.widget.MyToolBar;

import java.util.ArrayList;

/**
 * Created by 王揆 on 2016/7/19.
 */
public class SkillListActivity extends GowildBaseActivity {
	private ArrayList<SkillTitleInfo> mTitleInfos;
	private ArrayList<ArrayList<SkillBack.SkillItem>> mContentInfos;
	private RecyclerView mRecyclerView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
//		mExpandableListView = (ExpandableListView) findViewById(R.id.expandList);
        mRecyclerView = (RecyclerView) findViewById(R.id.recycler);
		MyToolBar toolbar = (MyToolBar) findViewById(R.id.view_toolbar);
		toolbar.setTitle(CommonDataUtil.getCurrentRobotTitle() + getString(R.string.gowild_who_skill));
		//initData();
		initInfos();
	}


	@Override
	protected int onSetTopbarView() {
		return R.layout.view_gowild_toolbar;
	}

	@Override
	protected int onSetContentView() {
		return R.layout.view_gowild_recycleview;
	}

	@Override
	protected int onSetTopBarHeight() {
		return getResources().getDimensionPixelSize(R.dimen.gowild_toolbar_height);
	}

	private void initRecycleData(SkillBack skillBack) {
		SkillRecyclerAdapter adapter = new SkillRecyclerAdapter(this);
		GridLayoutManager manager = new GridLayoutManager(this, SkillRecyclerAdapter.GRID_ROW);
		manager.setSpanSizeLookup(adapter.lookup);
		mRecyclerView.setLayoutManager(manager);
        int padding = getResources().getDimensionPixelSize(R.dimen.gowild_padding_edge);
        mRecyclerView.setPadding(padding, 0, padding, 0);

        mTitleInfos = new ArrayList<>();
		mContentInfos= new ArrayList<>();

		SkillTitleInfo info = new SkillTitleInfo(getString(R.string.gowild_title_new_skill), true);
		mTitleInfos.add(info);
		mContentInfos.add(skillBack.newSkillList);
        info.setCount(skillBack.newSkillList.size());

        info = new SkillTitleInfo(getString(R.string.gowild_title_lock_skill));
		mTitleInfos.add(info);
		mContentInfos.add(skillBack.lockedSkillList);
        info.setCount(skillBack.lockedSkillList.size());

		info = new SkillTitleInfo(getString(R.string.gowild_title_unlock_skill));
		mTitleInfos.add(info);
		mContentInfos.add(skillBack.unLockedSkillList);
        info.setCount(skillBack.unLockedSkillList.size());

        adapter.setInfos(mTitleInfos, mContentInfos);
		mRecyclerView.setAdapter(adapter);

	}

	private void initInfos() {
		showLoading(R.string.loading);
		GowildHttpManager.requestSkills(this, new GowildAsyncHttpResponseHandler<SkillBack>(this) {
			@Override
			public void onSuccess(int statusCode, SkillBack responseBody) {
                if (isActivityFinished()) {
                    return;
                }
				initRecycleData(responseBody);
				hideLoading();
			}

			@Override
			public void onFailure(int statusCode, String responseBody) {
                if (isActivityFinished()) {
                    return;
                }
				hideLoading();
			}
		});

	}
}
