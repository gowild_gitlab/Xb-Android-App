/**
 * Copyright (C)  2016 深圳市狗尾草智能科技有限公司
 * GowildXB
 * GowildWifiConfigActivity.java
 */
package com.gowild.mobileclient.activity.setting;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.gowild.mobile.libp2sp.core.exception.NotConnectedException;
import com.gowild.mobile.libp2sp.core.listener.IDataListener;
import com.gowild.mobile.libp2sp.netty.NettyClientManager;
import com.gowild.mobileclient.R;
import com.gowild.mobileclient.activity.base.GowildHasTitileActivity;
import com.gowild.mobileclient.config.Constants;
import com.gowild.mobileclient.config.KeysContainer;
import com.gowild.mobileclient.event.RobotOnline;
import com.gowild.mobileclient.manager.GowildAsyncHttpResponseHandler;
import com.gowild.mobileclient.manager.GowildHttpManager;
import com.gowild.mobileclient.manager.GowildTCPManager;
import com.gowild.mobileclient.misc.GowildResponseCode;
import com.gowild.mobileclient.protocol.MachineBothMsgProto;
import com.gowild.mobileclient.utils.CommonDataUtil;
import com.loopj.android.http.RequestParams;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * @author Zhangct
 * @version 1.0
 *          <p> 更改wifi配置</p>
 * @since 2016/8/6 16:18
 */
public class GowildChangeWifiActivity extends GowildHasTitileActivity {

    // ===========================================================
    // HostConstants
    // ===========================================================

    private static final String TAG = GowildChangeWifiActivity.class.getSimpleName();


    // ===========================================================
    // Static Fields
    // ===========================================================

    // ===========================================================
    // Fields
    // ===========================================================
    @Bind(R.id.tv_robot_wifi_name)
    TextView     mGowildWifiNameTv;
    @Bind(R.id.tv_robot_wifi_state)
    TextView     mGowildWifiStateTv;
    @Bind(R.id.btn_change_wifi)
    Button       mGowildWifiChangeBtn;
    @Bind(R.id.net_setting_ll)
    LinearLayout ll;
    @Bind(R.id.net_setting_fourg)
    LinearLayout fourg;
    @Bind(R.id.change)
    TextView     change;
    @Bind(R.id.flow)
    TextView     mFlow;
    @Bind(R.id.wifi_name)
    TextView     mWifiName;
    @Bind(R.id.sim_num)
    TextView     mSimNum;
    @Bind(R.id.net_status)
    TextView     netStatus;
    @Bind(R.id.iv_robot_icon)
    ImageView mIvRobotIcon;

    private MachineInfoListener mMachineInfoListener = new MachineInfoListener();

    // ===========================================================
    // Constructors
    // ===========================================================


    // ===========================================================
    // Getter or Setter
    // ===========================================================


    // ===========================================================
    // Methods for/from SuperClass/Interfaces
    // ===========================================================


    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            MachineBothMsgProto.MachineInfoMsg data     = (MachineBothMsgProto.MachineInfoMsg) msg.obj;
            String                             wifiName = null;
            if (!TextUtils.isEmpty(data.getWifi())) {
                wifiName = data.getWifi();
            } else {
                wifiName = getIntent().getExtras().getString(KeysContainer.KEY_WIFI_NAME);
            }
            has4Gcard(data.getHas4GCard());
            mIs4G = data.getNetMode() == 2;
            //设置开关名字
            change.setText(data.getNetMode() == 1 ? "切换到4G" : "切换到WIFI");
            mGowildWifiNameTv.setText(wifiName);
            mWifiName.setText(wifiName);
            netStatus.setText(data.getNetMode() == 1 ? "已连接WIFI" : "已连接4G");
            mSimNum.setText(data.getCard4GId());
            String robotState = getStringById(R.string.gowild_robot_state) + (data.getState() == Constants.ROBOT_CONNECTION ? getStringById(R.string.gowild_setting_robot_connected) : getStringById(R.string.gowild_setting_robot_disconnected));
            mGowildWifiStateTv.setText(robotState);
        }
    };
    private boolean mIs4G;

    @Override
    protected int onSetContentView() {
        return R.layout.activity_gowild_setting_net_reconfig;
    }

    @Override
    protected void initEvent() {
        setTitle(R.string.gowild_robot_net_config);
        mIvRobotIcon.setBackgroundResource(CommonDataUtil.getCurrentRobotTitle().equals("小白") ? R.drawable.gowild_xb_defoult : R.drawable.gowild_xb_hui);
        mIs4G = getIntent().getExtras().getBoolean(KeysContainer.IS4G);
        has4Gcard(mIs4G);
    }

    private void has4Gcard(boolean is4G) {
        if (is4G) {
            ll.setVisibility(View.GONE);
            fourg.setVisibility(View.VISIBLE);
        } else {
            //            ll.setVisibility(View.GONE);
            //            fourg.setVisibility(View.VISIBLE);
            ll.setVisibility(View.VISIBLE);
            fourg.setVisibility(View.GONE);
        }
    }

    @Override
    protected void loadData() {
        super.loadData();
        try {
            GowildTCPManager.requestRobotRequestSync();
        } catch (NotConnectedException e) {
            e.printStackTrace();
        }
    }


    @OnClick({R.id.btn_change_wifi, R.id.change, R.id.btn_change_wifi_g})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_change_wifi_g:
            case R.id.btn_change_wifi:
                Bundle bundle = new Bundle();
                bundle.putInt(GowildWifiConfigActivity.INTENT_EXTRA_FROM, GowildWifiConfigActivity.FROM_CHANGE_WIFI);
                startActivity(GowildWifiConfigActivity.class, bundle);
                finish();
                break;
            case R.id.change:
                RequestParams params = new RequestParams();
                if (mIs4G) {
                    params.put("netMode", 1);
                    mIs4G = false;
                } else {
                    mIs4G = true;
                    params.put("netMode", 2);
                }
                GowildHttpManager.requestChangeNet(this, params, new GowildAsyncHttpResponseHandler<String>(this) {

                    @Override
                    public void onSuccess(int statusCode, String responseBody) {
                        change.setText("切换中");
                        change.setEnabled(false);
                    }

                    @Override
                    public void onFailure(int statusCode, String responseBody) {

                    }
                });
                break;
            default:
                break;
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void subRobotOnline(RobotOnline online) {
        change.setEnabled(true);
        try {
            GowildTCPManager.requestRobotRequestSync();
        } catch (NotConnectedException e) {
            e.printStackTrace();
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
        NettyClientManager.getInstance().addHandlerListener(GowildResponseCode.ROBOT_INFO_SYNC, mMachineInfoListener);
    }

    @Override
    protected void onPause() {
        super.onPause();
        NettyClientManager.getInstance().removeHandlerListener(GowildResponseCode.ROBOT_INFO_SYNC, mMachineInfoListener);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
    }

    // ===========================================================
    // Methods
    // ===========================================================


    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================

    private class MachineInfoListener implements IDataListener<MachineBothMsgProto.MachineInfoMsg> {


        @Override
        public void onReceiveData(MachineBothMsgProto.MachineInfoMsg data) {
            Message msg = mHandler.obtainMessage();
            msg.obj = data;
            mHandler.sendMessage(msg);
        }

        @Override
        public void onActiveChanged(boolean isActive) {

        }
    }
}
