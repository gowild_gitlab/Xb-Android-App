/**
 * Copyright (C)  2016 深圳市狗尾草智能科技有限公司
 * GowildXB
 * MusicLoverActivity.java
 */
package com.gowild.mobileclient.activity.music;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.gowild.mobileclient.R;
import com.gowild.mobileclient.config.Constants;
import com.gowild.mobileclient.config.IntContants;
import com.gowild.mobileclient.config.MediaConstants;
import com.gowild.mobileclient.event.RefreshEvent;
import com.gowild.mobileclient.manager.GowildAsyncHttpResponseHandler;
import com.gowild.mobileclient.manager.GowildHttpManager;
import com.gowild.mobileclient.model.MusicItemInfoBack;
import com.gowild.mobileclient.model.StoryAlbumItemInfoBack;
import com.gowild.mobileclient.utils.CommonDataUtil;
import com.loopj.android.http.RequestParams;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import butterknife.Bind;

/**
 * @author temp
 * @version 1.0
 *          <p><strong>Features draft description.主要功能介绍</strong></p>
 * @since 2016/8/2 21:51
 */
public class MusicLoverActivity extends NetResourceListBaseActivity {

    private static final String TAG = MusicLoverActivity.class.getSimpleName();
    @Bind(R.id.pull_icon)        ImageView      mPullIcon;
    @Bind(R.id.refreshing_icon)  ImageView      mRefreshingIcon;
    @Bind(R.id.state_tv)         TextView       mStateTv;
    @Bind(R.id.state_iv)         ImageView      mStateIv;
    @Bind(R.id.head_view)        RelativeLayout mHeadView;
    @Bind(R.id.pullup_icon)      ImageView      mPullupIcon;
    @Bind(R.id.loading_icon)     ImageView      mLoadingIcon;
    @Bind(R.id.loadstate_tv)     TextView       mLoadstateTv;
    @Bind(R.id.loadstate_iv)     ImageView      mLoadstateIv;
    @Bind(R.id.loadmore_view)    RelativeLayout mLoadmoreView;
    @Bind(R.id.music_tips_two)   TextView       mMusicTipsTwo;
    @Bind(R.id.music_tips_one)   TextView       mMusicTipsOne;
    @Bind(R.id.music_tips)       TextView       mMusicTips;
    @Bind(R.id.btn_add_music)    Button         mBtnAddMusic;
    @Bind(R.id.music_container)  LinearLayout   mMusicContainer;
    @Bind(R.id.gowild_stroy_two) TextView       mGowildStroyTwo;
    @Bind(R.id.gowild_stroy_one) TextView       mGowildStroyOne;
    @Bind(R.id.story_tips)       TextView       mStoryTips;
    @Bind(R.id.btn_add_story)    Button         mBtnAddStory;
    @Bind(R.id.story_container)  LinearLayout   mStoryContainer;
    private                      TextView       mView;
    private                      TextView       mView1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mView = (TextView) findViewById(R.id.add_more);
        Button mMusic = (Button) findViewById(R.id.btn_add_music);
        mMusic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MusicLoverActivity.this, MusicSearchActivity.class));
            }
        });
        Button mStory = (Button) findViewById(R.id.btn_add_story);
        mStory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MusicLoverActivity.this, MusicSearchActivity.class));
            }
        });
        if (IntContants.isCurrentMusic()) {
            mView.setVisibility(View.VISIBLE);
            mView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(new Intent(MusicLoverActivity.this, MusicSearchActivity.class));
                }
            });
        } else {
            //            TextView mView = (TextView) findViewById(R.id.add_more);
            mView.setText("+添加喜欢的故事");
            mView.setVisibility(View.VISIBLE);
            mView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(new Intent(MusicLoverActivity.this, MusicSearchActivity.class));
                }
            });
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public String getToobarTitle() {
        if (IntContants.isCurrentMusic()) {
            return getResources().getString(R.string.gowild_music_lover_title);
        } else {
            return getResources().getString(R.string.gowild_story_lover_title);
        }
    }

    @Override
    public View getTipView() {
        mView1 = (TextView) LayoutInflater.from(this).
                inflate(R.layout.view_gowild_net_resource_top_hint, getRootView(), false);
        mView1.setText(IntContants.isCurrentMusic() ?
                       R.string.gowild_music_play_lover : R.string.gowild_story_play_lover);
        mView1.setCompoundDrawablesWithIntrinsicBounds(R.drawable.gowild_music_play_hint, 0, 0, 0);
        mView1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (IntContants.isCurrentMusic()) {
                    GowildHttpManager.changeMusic(getBaseContext(), new RequestParams(Constants.MUSIC_ID, -1, Constants.PLAY_STATUS, Constants.PLAY_STATUS_PLAY, Constants.PLAY_TYPE, MediaConstants.COLLECT), new GowildAsyncHttpResponseHandler(getBaseContext()) {
                        @Override
                        public void onSuccess(int statusCode, Object responseBody) {
                            Intent intent = new Intent(MusicLoverActivity.this, MusicMainActivity.class);
                            startActivity(intent);
                        }

                        @Override
                        public void onFailure(int statusCode, String responseBody) {

                        }
                    });
                } else {
                    GowildHttpManager.changeStory(getBaseContext(), new RequestParams(Constants.STORY_ID, -1, Constants.PLAY_STATUS, Constants.PLAY_STATUS_PLAY, Constants.PLAY_TYPE, MediaConstants.COLLECT), new GowildAsyncHttpResponseHandler(getBaseContext()) {
                        @Override
                        public void onSuccess(int statusCode, Object responseBody) {

                        }

                        @Override
                        public void onFailure(int statusCode, String responseBody) {

                        }
                    });
                }
            }
        });
        return mView1;
    }

    @Override
    protected int onSetContentView() {
        return R.layout.activity_music_lover;
    }

    public void loadInfo(int pageNum) {
        final int num = pageNum;
        if (IntContants.isCurrentMusic()) {
            showLoading(R.string.loading);
            GowildHttpManager.getMusicCollect(this, pageNum, new GowildAsyncHttpResponseHandler<MusicItemInfoBack>(this) {
                @Override
                public void onSuccess(int statusCode, MusicItemInfoBack responseBody) {
                    hideLoading();
                    if (responseBody == null) {
                        onLoadSuccess(null);
                    }
                    onLoadSuccess(responseBody.musics);
                    if (num == 1) {
                        if (responseBody.musics.size() == 0) {
                            updateView(false);
                        } else {
                            updateView(true);
                        }
                    }
                }

                @Override
                public void onFailure(int statusCode, String responseBody) {
                    hideLoading();
                    onLoadFailure();
                }
            });
        } else {
            showLoading(R.string.loading);
            GowildHttpManager.getStoryCollect(this, pageNum, new GowildAsyncHttpResponseHandler<StoryAlbumItemInfoBack>(this) {
                @Override
                public void onSuccess(int statusCode, StoryAlbumItemInfoBack responseBody) {
                    hideLoading();
                    if (responseBody == null) {
                        onLoadSuccess(null);
                    }
                    onLoadSuccess(responseBody.storyAlbums);
                    if (num == 1) {

                    if (responseBody.storyAlbums.size() == 0) {
                        updateView(false);
                    } else {
                        updateView(true);
                    }

                    }
                }

                @Override
                public void onFailure(int statusCode, String responseBody) {
                    hideLoading();
                    onLoadFailure();
                }
            });
        }
    }

    private void updateView(boolean isShowMainView) {
        if (isShowMainView) {
            mPullLayout.setVisibility(View.VISIBLE);
            mView1.setVisibility(View.VISIBLE);
            mView.setVisibility(View.VISIBLE);
            updateDate(isShowMainView);
        } else {
            mView1.setVisibility(View.GONE);
            mView.setVisibility(View.GONE);
            mPullLayout.setVisibility(View.GONE);
            updateDate(isShowMainView);
        }
    }

    private void updateDate(boolean show) {
        if (IntContants.isCurrentMusic()) {
            mStoryContainer.setVisibility(View.GONE);
            if (show) {
                mMusicContainer.setVisibility(View.GONE);
            } else {
                mMusicContainer.setVisibility(View.VISIBLE);
            }
            mMusicTipsOne.setText(CommonDataUtil.getCurrentRobotTitle() + "也会喜欢上这首歌，并记录在" + CommonDataUtil.getCurrentRobotTitle() + "的脑壳里面及在此登记在册。");
            mMusicTipsTwo.setText("在我" + CommonDataUtil.getCurrentRobotTitle() + "正在开心愉快唱歌的时候，你只要对" + CommonDataUtil.getCurrentRobotTitle() + "说，“" + CommonDataUtil.getCurrentRobotTitle() + "，我喜欢这首歌”之类的话语。");
            SpannableStringBuilder builder = new SpannableStringBuilder("当然，只要你喜欢，你肯定也是可以点击下面的【+添加喜欢的歌】来把它刻印在" + CommonDataUtil.getCurrentRobotTitle() + "的脑壳里面并在此登记成册。");
            builder.setSpan(new ForegroundColorSpan(Color.RED), 21, 30, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            mMusicTips.setText(builder);
        } else {
            mMusicContainer.setVisibility(View.GONE);
            if (show) {
                mStoryContainer.setVisibility(View.GONE);
            } else {
                mStoryContainer.setVisibility(View.VISIBLE);

            }
            mGowildStroyOne.setText(CommonDataUtil.getCurrentRobotTitle() + "也会喜欢上这个故事，并记录在" + CommonDataUtil.getCurrentRobotTitle() + "的脑壳里面及在此登记在册。");
            mGowildStroyTwo.setText("在我" + CommonDataUtil.getCurrentRobotTitle() + "正在开心愉快讲故事的时候，你只要对" + CommonDataUtil.getCurrentRobotTitle() + "说，“" + CommonDataUtil.getCurrentRobotTitle() + "，我喜欢这个故事”之类的话语。");
            SpannableStringBuilder builder = new SpannableStringBuilder("当然，只要你喜欢，你肯定也是可以点击下面的【+添加喜欢的故事】来把它刻印在" + CommonDataUtil.getCurrentRobotTitle() + "的脑壳里面并在此登记成册。");
            builder.setSpan(new ForegroundColorSpan(Color.RED), 21, 31, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            mStoryTips.setText(builder);
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void subReFresh(RefreshEvent event) {
        //刷新列表
        loadInfo(GowildHttpManager.DEFAULT_PAGE_NUM);
    }

}
