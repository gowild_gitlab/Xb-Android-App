package com.gowild.mobileclient.activity.base;


import com.gowild.mobileclient.R;

/**
 * @author shuai. wang
 * @version 1.0
 * @since ${DATE} ${TIME}
 * <p><strong>Features draft description.主要功能介绍: 登录和注册的基类</strong></p>
 */
public abstract class GowildLoginAndRegisterActivity extends GowildNoTitileActivity {
	// ===========================================================
	// Methods for/from SuperClass/Interfaces
	// ===========================================================

	/**
	 * 设置内容
	 */
	@Override
	protected void initView() {
	}


	/**
	 * 子类重写该方法,返回顶部的效果
	 *
	 * @return
	 */
	@Override
	protected int onSetTopBarHeight() {
		return (int) getResources().getDimension(R.dimen.gowild_110px);
	}
}
