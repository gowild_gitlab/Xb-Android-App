/**
 * Copyright (C)  2016 深圳市狗尾草智能科技有限公司
 * GowildXB
 * GowildLoverInfoActivity.java
 */
package com.gowild.mobileclient.activity.love;

import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.gowild.mobile.libcommon.utils.SpUtils;
import com.gowild.mobileclient.R;
import com.gowild.mobileclient.activity.base.GowildLoginAndRegisterActivity;
import com.gowild.mobileclient.callback.OnNormalDialogListener;
import com.gowild.mobileclient.config.Constants;
import com.gowild.mobileclient.event.UnBindLover;
import com.gowild.mobileclient.manager.GowildAsyncHttpResponseHandler;
import com.gowild.mobileclient.manager.GowildHttpManager;
import com.gowild.mobileclient.vo.OtherLoverInfo;
import com.gowild.mobileclient.widget.MyToolBar;
import com.gowild.mobileclient.widget.dialog.GowildUnbindLoverDialog;
import com.loopj.android.http.RequestParams;

import org.greenrobot.eventbus.EventBus;

import butterknife.Bind;
import butterknife.OnClick;

/**
 * @author shuai. wang
 * @version 1.0
 *          <p><strong>Features draft description.主要功能介绍</strong></p>
 * @since 2016/8/3 16:37
 */
public class GowildLoverInfoActivity extends GowildLoginAndRegisterActivity {

    // ===========================================================
    // HostConstants
    // ===========================================================
    private static final String TAG = GowildLoverInfoActivity.class.getSimpleName();
    @Bind(R.id.gowild_lover_info_unbind)
    Button mGowildLoverInfoUnbind;
    @Bind(R.id.gowild_love_tv_account)
    TextView mGowildLoveTvAccount;
    @Bind(R.id.gowild_love_tv_nickname)
    TextView mGowildLoveTvNickname;
    @Bind(R.id.gowild_love_tv_mail)
    TextView mGowildLoveTvMail;
    @Bind(R.id.gowild_love_tv_sex)
    TextView mGowildLoveTvSex;
    @Bind(R.id.gowild_love_tv_brith)
    TextView mGowildLoveTvBrith;
    @Bind(R.id.gowild_love_tv_address)
    TextView mGowildLoveTvAddress;
    @Bind(R.id.gowild_love_tv_love)
    TextView mGowildLoveTvLove;
    private MyToolBar mToolbar;

    @Override
    public int getLayout() {
        return R.layout.gowild_activity_loverinfo;
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    protected void initView() {
        mToolbar = (MyToolBar) findViewById(R.id.view_toolbar);
    }

    @Override
    protected void initData() {
        //获得对方的消息
        showLoading(R.string.loading);
        GowildHttpManager.requestLoverInfo(this, new GowildAsyncHttpResponseHandler<OtherLoverInfo>(this) {

            @Override
            public void onSuccess(int statusCode, OtherLoverInfo responseBody) {
                hideLoading();
                if (responseBody == null){
                    return;
                }else{
                    setInfo(responseBody);
                }
            }

            @Override
            public void onFailure(int statusCode, String responseBody) {
                hideLoading();
            }
        });
    }

    private void setInfo(OtherLoverInfo info) {
        mToolbar.setTitle(info.nickname);
        mGowildLoveTvMail.setText(info.email);
        mGowildLoveTvAccount.setText(info.phone);
        mGowildLoveTvNickname.setText(info.nickname);
        mGowildLoveTvSex.setText(1 == info.sex ? "男" : "女");
        mGowildLoveTvBrith.setText(info.birthday);
        mGowildLoveTvAddress.setText(info.address);
        mGowildLoveTvLove.setText(info.hobby);
    }

    @Override
    protected void initEvent() {
    }

    @OnClick(R.id.gowild_lover_info_unbind)
    public void onClick(View view) {
        //解除绑定
        showDialog();
    }

    private void showDialog() {
        final GowildUnbindLoverDialog dialog = new GowildUnbindLoverDialog();
        dialog.setDialogListener(new OnNormalDialogListener() {
            @Override
            public void onCancelListener() {
                dialog.dismiss();
            }

            @Override
            public void onConfirmListener() {
                unbindLover();
                dialog.dismiss();
            }
        });
        dialog.show(getFragmentManager(), "dialog");
    }

    private void unbindLover() {
        showLoading(R.string.loading);
        RequestParams params = new RequestParams("magicCode", SpUtils.getLong(this, Constants.MAGICCODE));
        GowildHttpManager.requestUnbindLover(this, params, new GowildAsyncHttpResponseHandler<String>(this) {

            @Override
            public void onSuccess(int statusCode, String responseBody) {
                hideLoading();
                nextActivtiy(GowildLoverActivity.class);
                EventBus.getDefault().post(new UnBindLover(true));
                finish();
            }

            @Override
            public void onFailure(int statusCode, String responseBody) {
                hideLoading();
            }
        });
    }

    // ===========================================================
    // Static Fields
    // ===========================================================

    // ===========================================================
    // Fields
    // ===========================================================


    // ===========================================================
    // Constructors
    // ===========================================================


    // ===========================================================
    // Getter or Setter
    // ===========================================================


    // ===========================================================
    // Methods for/from SuperClass/Interfaces
    // ===========================================================


    // ===========================================================
    // Methods
    // ===========================================================


    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================

}
