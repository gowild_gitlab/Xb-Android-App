/**
 * Copyright (C)  2016 深圳市狗尾草智能科技有限公司
 * GowildXB
 * MusicSearchResultActivity.java
 */
package com.gowild.mobileclient.activity.music;


import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.gowild.mobile.libcommon.utils.ToastUtils;
import com.gowild.mobileclient.R;
import com.gowild.mobileclient.config.Constants;
import com.gowild.mobileclient.manager.GowildAsyncHttpResponseHandler;
import com.gowild.mobileclient.manager.GowildHttpManager;
import com.gowild.mobileclient.model.BaseNetResourceInfo;
import com.gowild.mobileclient.vo.Music;
import com.gowild.mobileclient.vo.RequestSinger;
import com.gowild.mobileclient.vo.ResponseMusic;

import java.util.ArrayList;

/**
 * @author ljd
 * @version 1.0
 *          <p><strong>Features draft description.主要功能介绍</strong></p>
 * @since 2016/8/5 10:16
 */
public class MusicSearchResultActivity extends NetResourceListBaseActivity {

    private static final String TAG = MusicRecordActivity.class.getSimpleName();
    private OnRequestMusicSearchResultCallback mOnRequestMusicSearchResultCallback;
    public static final int FROM_SINGERLIST = 1;
    public static final int FROM_SINGER_SEARCH_BAR = 2;
    private String key = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void initInfoBeforeLoad() {
        mOnRequestMusicSearchResultCallback = new OnRequestMusicSearchResultCallback(this);
        key = getIntent().getStringExtra(Constants.INFOS);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public String getToobarTitle() {
        if (getIntent().getIntExtra(Constants.TYPE, FROM_SINGERLIST) == FROM_SINGERLIST) {
            return getIntent().getStringExtra(Constants.INFOS);
        } else {
            return getIntent().getStringExtra(Constants.INFOS);
        }

    }

    @Override
    public View getTipView() {
        TextView view = (TextView) LayoutInflater.from(this).
                inflate(R.layout.view_gowild_net_resource_top_hint, getRootView(), false);
        view.setText(R.string.gowild_search_result);
        return view;
    }

    @Override
    public void loadInfo(int pageNum) {
        if (getIntent().getIntExtra(Constants.TYPE, FROM_SINGERLIST) == FROM_SINGERLIST) {
            showLoading(R.string.loading);
            GowildHttpManager.requestMusicFromSingerListSearchResult(this, key, pageNum, mOnRequestMusicSearchResultCallback);
        } else {
            showLoading(R.string.loading);
            GowildHttpManager.requestMusicFromKeyWordSearchResult(this, key, pageNum, mOnRequestMusicSearchResultCallback);
        }
    }

    private class OnRequestMusicSearchResultCallback extends GowildAsyncHttpResponseHandler<ResponseMusic> {

        public OnRequestMusicSearchResultCallback(Context context) {
            super(context);
        }

        @Override
        public void onSuccess(int statusCode, ResponseMusic responseBody) {
            hideLoading();
            if (responseBody.musics.size() != 0) {
                onLoadSuccess(responseBody.musics);
            } else {
                onLoadFailure();
                ToastUtils.showCustom(getBaseContext(), R.string.gowild_no_more, false);
            }
        }

        @Override
        public void onFailure(int statusCode, String responseBody) {
            hideLoading();
            onLoadFailure();
        }
    }


}
