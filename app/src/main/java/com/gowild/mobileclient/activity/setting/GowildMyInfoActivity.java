/**
 * Copyright (C)  2016 深圳市狗尾草智能科技有限公司
 * GowildXB
 * GowildMyInfoActivity.java
 */
package com.gowild.mobileclient.activity.setting;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.gowild.mobile.libcommon.utils.SpUtils;
import com.gowild.mobile.libcommon.utils.ToastUtils;
import com.gowild.mobileclient.R;
import com.gowild.mobileclient.activity.base.GowildHasTitileActivity;
import com.gowild.mobileclient.config.Constants;
import com.gowild.mobileclient.config.KeysContainer;
import com.gowild.mobileclient.manager.GowildAsyncHttpResponseHandler;
import com.gowild.mobileclient.manager.GowildHttpManager;
import com.gowild.mobileclient.model.AccountModel;
import com.gowild.mobileclient.vo.RequestInfoGet;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.Bind;
import butterknife.OnClick;

/**
 * @author Zhangct
 * @version 1.0
 *          <p> 我的个人信息 </p>
 * @since 2016/8/6 9:29
 */
public class GowildMyInfoActivity extends GowildHasTitileActivity {

    // ===========================================================
    // HostConstants
    // ===========================================================

    private static final String TAG = GowildMyInfoActivity.class.getSimpleName();

    // ===========================================================
    // Static Fields
    // ===========================================================

    // ===========================================================
    // Fields
    // ===========================================================
    //昵称
    @Bind(R.id.rl_onwer_nickname)
    RelativeLayout mGowildOwnerNicknameRl;
    //邮箱
    @Bind(R.id.rl_onwer_email)
    RelativeLayout mGowildOwnerEmailRl;
    //性别
    @Bind(R.id.rl_onwer_sex)
    RelativeLayout mGowildOwnerSexRl;
    //生日
    @Bind(R.id.rl_onwer_birth)
    RelativeLayout mGowildOwnerBirthRl;
    //地址
    @Bind(R.id.rl_onwer_address)
    RelativeLayout mGowildOwnerAddressRl;
    //爱好
    @Bind(R.id.rl_onwer_hobby)
    RelativeLayout mGowildOwnerHobbyRl;

    //账户
    @Bind(R.id.tv_onwer_account)
    TextView mGowildOwnerAccountTv;
    //昵称
    @Bind(R.id.tv_onwer_nickname)
    TextView mGowildOwnerNicknameTv;
    //邮箱
    @Bind(R.id.tv_onwer_email)
    TextView mGowildOwnerEmailTv;
    //性别
    @Bind(R.id.tv_onwer_sex)
    TextView mGowildOwnerSexTv;
    //生日
    @Bind(R.id.tv_onwer_birth)
    TextView mGowildOwnerBirthTv;
    //地址
    @Bind(R.id.tv_onwer_address)
    TextView mGowildOwnerAddressTv;
    //爱好
    @Bind(R.id.tv_onwer_hobby)
    TextView mGowildOwnerHobbyTv;

    private RequesGetUserCallback mGetUserCallback = null;

    private AccountModel mAccount = null;

    private Handler mHandler = new Handler() {

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case MSG_CODE_SUCCESS:
                    mAccount = (AccountModel) msg.obj;
                    initUserInfo();
                    hideLoading();
                    break;
                case MSG_CODE_FAIL:
                    hideLoading();
                    break;
            }
        }
    };

    // ===========================================================
    // Constructors
    // ===========================================================


    // ===========================================================
    // Getter or Setter
    // ===========================================================


    // ===========================================================
    // Methods for/from SuperClass/Interfaces
    // ===========================================================
    @Override
    protected int onSetContentView() {
        return R.layout.activity_gowild_setting_onwer;
    }

    @Override
    protected void initEvent() {
        setTitle(R.string.gowild_setting_owner);
    }

    @Override
    protected void initView() {
        super.initView();
        showLoading(R.string.loading);
        mGetUserCallback = new RequesGetUserCallback(GowildMyInfoActivity.this);
        mGowildOwnerAccountTv.setText(SpUtils.getString(this, Constants.USERNAME));
        requestUserInfo();
    }

    @OnClick({R.id.rl_onwer_nickname, R.id.rl_onwer_email, R.id.rl_onwer_sex, R.id.rl_onwer_birth,
            R.id.rl_onwer_address, R.id.rl_onwer_hobby})
    public void onclick(View view) {
        if (mAccount != null) {
            Bundle bundle = new Bundle();
            bundle.putString(KeysContainer.KEY_PRE_NAME_ACCOUNT, mAccount.toJSON().toString());
            switch (view.getId()) {
                case R.id.rl_onwer_nickname:
                    bundle.putString(Constants.KEY_STYLE, Constants.KEY_NICKNAME);
                    break;
                case R.id.rl_onwer_email:
                    bundle.putString(Constants.KEY_STYLE, Constants.KEY_EMAIL);
                    break;
                case R.id.rl_onwer_sex:
                    bundle.putString(Constants.KEY_STYLE, Constants.KEY_SEX);
                    break;
                case R.id.rl_onwer_birth:
                    bundle.putString(Constants.KEY_STYLE, Constants.KEY_BIRTHDAY);
                    break;
                case R.id.rl_onwer_address:
                    bundle.putString(Constants.KEY_STYLE, Constants.KEY_ADDRESS);
                    break;
                case R.id.rl_onwer_hobby:
                    bundle.putString(Constants.KEY_STYLE, Constants.KEY_HOBBY);
                    break;
                default:
                    break;
            }
            startActivityForResult(GowildUpdateInfoActivity.class, KeysContainer.KEY_REQUEST_USERINFO, bundle);
        } else {
//            ToastUtils.showCustom(getBaseContext(), R.string.gowild_network_error, false);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case KeysContainer.KEY_REQUEST_USERINFO:
                    String tempStr = data.getStringExtra(KeysContainer.KEY_PRE_NAME_ACCOUNT);
                    try {
                        mAccount = new AccountModel(new JSONObject(tempStr));
                        initUserInfo();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;
            }
        }
    }

    // ===========================================================
    // Methods
    // ===========================================================

    /**
     * 请求个人信息
     */
    private void requestUserInfo() {
        RequestInfoGet requestInfoGet = new RequestInfoGet();
        showLoading(R.string.loading);
        GowildHttpManager.requestGetUserInfo(this, requestInfoGet, mGetUserCallback);
    }

    /**
     * 初始化个人信息
     */
    private void initUserInfo() {
        if (null != mAccount) {
            mGowildOwnerNicknameTv.setText(mAccount.getNickName());
            mGowildOwnerEmailTv.setText(mAccount.getEmail());
            if (mAccount.getSex() == 1) {
                mGowildOwnerSexTv.setText(R.string.gowild_onwer_updateinfo_man);
            } else {
                mGowildOwnerSexTv.setText(R.string.gowild_onwer_updateinfo_woman);
            }
            mGowildOwnerBirthTv.setText(mAccount.getBirthday());
//        mPhoneNum.setText(mAccount.getPhone());
            mGowildOwnerAddressTv.setText(mAccount.getAddress());
            mGowildOwnerHobbyTv.setText(mAccount.getHobby());
        }
    }

    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================
    private class RequesGetUserCallback extends GowildAsyncHttpResponseHandler<AccountModel> {

        /**
         * 构造方法
         *
         * @param context 用于处理授权过期消息，会被转成ApplicationContext
         */
        public RequesGetUserCallback(Context context) {
            super(context);
        }

        @Override
        public void onSuccess(int statusCode, AccountModel responseBody) {
            Message msg = mHandler.obtainMessage();
            msg.what = MSG_CODE_SUCCESS;
            msg.obj = responseBody;
            mHandler.sendMessage(msg);
        }

        @Override
        public void onFailure(int statusCode, String responseBody) {
            Message msg = mHandler.obtainMessage();
            msg.what = MSG_CODE_FAIL;
            mHandler.sendMessage(msg);
        }
    }
}
