/**
 * Copyright (C)  2016 深圳市狗尾草智能科技有限公司
 * GowildXB
 * GowildWifiConfigActivity.java
 */
package com.gowild.mobileclient.activity.setting;

import android.content.Context;
import android.content.Intent;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.gowild.mobile.libcommon.utils.SpUtils;
import com.gowild.mobile.libcommon.utils.ToastUtils;
import com.gowild.mobile.libp2sp.core.exception.NotConnectedException;
import com.gowild.mobileclient.R;
import com.gowild.mobileclient.activity.base.GowildHasTitileActivity;
import com.gowild.mobileclient.adapter.WifiConfigAdapter;
import com.gowild.mobileclient.config.Constants;
import com.gowild.mobileclient.config.KeysContainer;
import com.gowild.mobileclient.event.BindResult;
import com.gowild.mobileclient.event.RobotOnline;
import com.gowild.mobileclient.manager.GowildTCPManager;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import butterknife.Bind;
import butterknife.OnClick;

/**
 * @author Zhangct
 * @version 1.0
 *          <p> Wifi 配置 </p>
 * @since 2016/8/6 16:18
 */
public class GowildWifiConfigActivity extends GowildHasTitileActivity {

    // ===========================================================
    // HostConstants
    // ===========================================================

    private static final String TAG = GowildWifiConfigActivity.class.getSimpleName();

    public static final String INTENT_EXTRA_FROM = "INTENT_EXTRA_FROM";

    public static final int FROM_CHANGE_WIFI = 1;

    private boolean isChangeWifiConfig = false;

    // ===========================================================
    // Static Fields
    // ===========================================================

    // ===========================================================
    // Fields
    // ===========================================================
    @Bind(R.id.gowild_wifi_title)
    TextView mGowildWifiTitle;
    @Bind(R.id.gowild_wifi_next)
    Button mGowildWifiNext;
    @Bind(R.id.gowild_wifi_lv)
    ListView mGowildWifiLv;
    @Bind(R.id.gowild_wifi_et)
    EditText mGowildWifiPwdEt;
    private List<ScanResult> mScan;
    private String mSsid;
    private WifiConfigAdapter mAdapter;

    private String mTitle = null;

    // ===========================================================
    // Constructors
    // ===========================================================


    // ===========================================================
    // Getter or Setter
    // ===========================================================


    // ===========================================================
    // Methods for/from SuperClass/Interfaces
    // ===========================================================
    @Override
    protected int onSetContentView() {
        return R.layout.activity_gowild_wifi_config;
    }


    @Override
    protected void initEvent() {
        mTitle = getString(R.string.gowild_setting_robot_config_to_bind_robot);
        Bundle bundle = getIntent().getExtras();
        if (null != bundle) {
//            mTitle = bundle.getString(KeysContainer.KEY_ROBOT_BIND_TITLE);
            int from = bundle.getInt(GowildWifiConfigActivity.INTENT_EXTRA_FROM);
            if (from == GowildWifiConfigActivity.FROM_CHANGE_WIFI) {
                mTitle = getString(R.string.gowild_robot_net_config);
            }
        }
        setTitle(mTitle);
        initWifiInfo();
        mGowildWifiTitle.setText(mSsid);
        setPwdNumber();
        mAdapter = new WifiConfigAdapter(GowildWifiConfigActivity.this, mScan);
        mGowildWifiLv.setAdapter(mAdapter);
//        mAdapter.notifyDataSetChanged();
        mGowildWifiLv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                mGowildWifiTitle.setText(mScan.get(position).SSID);
                setPwdNumber();
                mGowildWifiPwdEt.requestFocus();
            }
        });
        //下一步发送验证码
    }

    @Override
    protected void initView() {
        super.initView();
    }

    private void setPwdNumber() {
        String pwd = SpUtils.getString(getApplication(), mGowildWifiTitle.getText().toString().trim());
        if (!TextUtils.isEmpty(pwd)) {
            mGowildWifiPwdEt.setText(pwd);
        } else {
            mGowildWifiPwdEt.setText("");
        }
    }

    @OnClick(R.id.gowild_wifi_next)
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.gowild_wifi_next:
                toPage();
                break;
            default:
                break;
        }
    }

    // ===========================================================
    // Methods
    // ===========================================================

    /**
     * 判断是跳转到哪个界面
     */
    private void toPage() {
        toCreateQR(GowildQRActivity.class);
    }

    /**
     * wifi推送
     */
    private void toChangeWifiConfig() {
        String pwd = mGowildWifiPwdEt.getText().toString();
        String ssid = mGowildWifiTitle.getText().toString();
        if (TextUtils.isEmpty(ssid)) {
            ToastUtils.showCustom(this, getString(R.string.gowild_wifi_ssid_isnull), false);
            return;
        }
        if (TextUtils.isEmpty(pwd)) {
            ToastUtils.showCustom(this, getString(R.string.gowild_wifi_pwd), false);
            return;
        }
        try {
            GowildTCPManager.requestChangeWifiConfig(ssid, pwd);
            ToastUtils.showCustom(this, getString(R.string.gowild_wifi_push_success), false);
            finish();
        } catch (NotConnectedException e) {
            ToastUtils.showCustom(this, getString(R.string.gowild_wifi_push_fail), false);
            e.printStackTrace();
        }
    }

    /**
     * 根据wifi信息产生二维码
     *
     * @param clazz
     */
    public void toCreateQR(Class clazz) {
        String pwd = mGowildWifiPwdEt.getText().toString();
        String ssid = mGowildWifiTitle.getText().toString();
        if (TextUtils.isEmpty(ssid)) {
            ToastUtils.showCustom(this, getString(R.string.gowild_wifi_ssid_isnull), false);
            return;
        }
        Intent intent = new Intent(GowildWifiConfigActivity.this, clazz);
        LinkedHashMap<String, String> map = new LinkedHashMap<>();
        map.put("account", SpUtils.getString(this, Constants.USERNAME));
        map.put("sex","男");
        map.put("name","狗尾草");
        map.put("pwd", pwd);
        map.put("ssid", ssid);

        SpUtils.putString(this, mGowildWifiTitle.getText().toString(), pwd);
        JSONObject jsonObject = new JSONObject(map);
        intent.putExtra(Constants.SSID, jsonObject.toString());
        intent.putExtra(KeysContainer.KEY_ROBOT_BIND_TITLE, mTitle);
        startActivity(intent);

    }

    /**
     * 初始化wifi信息
     */
    private void initWifiInfo() {
        WifiManager wifiManager = (WifiManager) getSystemService(Context.WIFI_SERVICE);
        List<ScanResult> results = wifiManager.getScanResults();
        WifiInfo info = wifiManager.getConnectionInfo();


        mScan = new ArrayList<>();
        for (ScanResult result : results) {
            if (result.SSID == null || result.SSID.length() == 0 || result.capabilities.contains("[IBSS]")) {
                continue;
            }
            boolean found = false;
            for (ScanResult item : mScan) {
                if (item.SSID.equals(result.SSID) && item.capabilities.equals(result.capabilities)) {
                    found = true;
                    break;
                }
            }
            if (!found) {
                mScan.add(result);
            }
        }

        //获得当前wifi的名字
        if (mScan.size() == 0) {
            mSsid = "";
            return;
        }
        mSsid = info.getSSID();
        if (mSsid.startsWith("\"")) {
            mSsid = mSsid.substring(1, mSsid.length() - 1);
        }

    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void subRobotOnline(RobotOnline online) {
        if (online.result) {
            finish();
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void subBindResult(BindResult result) {
        finish();
    }


    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================
}
