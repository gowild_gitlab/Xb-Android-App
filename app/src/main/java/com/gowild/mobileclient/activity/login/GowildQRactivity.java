/**
 * Copyright (C)  2016 深圳市狗尾草智能科技有限公司
 * GowildXB
 * GowildQRactivity.java
 */
package com.gowild.mobileclient.activity.login;

import android.content.Intent;
import android.graphics.Bitmap;
import android.view.View;
import android.widget.ImageView;

import com.gowild.mobile.libcommon.utils.EncodingUtils;
import com.gowild.mobileclient.R;
import com.gowild.mobileclient.activity.base.GowildNoTitileActivity;
import com.gowild.mobileclient.config.Constants;
import com.gowild.mobileclient.event.BindResult;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import butterknife.Bind;
import butterknife.OnClick;

/**
 * @author shuai. wang
 * @version 1.0
 *          <p><strong>Features draft description.主要功能介绍</strong></p>
 * @since 2016/8/1 9:43
 */
public class GowildQRactivity extends GowildNoTitileActivity {
    // ===========================================================
    // HostConstants
    // ===========================================================
    public static final String INTENT_EXTRA_FROM = "INTENT_EXTRA_FROM";

    public static final int FROM_CHANGE_WIFI = 1;
    private static final String TAG = GowildQRactivity.class.getSimpleName();
    @Bind(R.id.gowild_qr_tv)
    ImageView mGowildQrTv;
    private String mWifiInfo;
    private String mSsid;

    // ===========================================================
    // Static Fields
    // ===========================================================

    // ===========================================================
    // Fields
    // ===========================================================


    // ===========================================================
    // Constructors
    // ===========================================================


    // ===========================================================
    // Getter or Setter
    // ===========================================================


    // ===========================================================
    // Methods for/from SuperClass/Interfaces
    // ===========================================================

    @Override
    public int getLayout() {
        return R.layout.activity_gowild_qr;
    }

    @Override
    protected void initView() {

    }

    @Override
    protected void initData() {
    }


    @Override
    protected void initEvent() {
        Intent intent = getIntent();
        mWifiInfo = intent.getStringExtra(Constants.SSID);
        Bitmap bitmap = EncodingUtils.createQRCode(mWifiInfo, 500, 500, null);
        mGowildQrTv.setImageBitmap(bitmap);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void subBindResult(BindResult result) {
        nextActivtiy(GowildBindSuccessActivity.class);
    }

    @OnClick({R.id.gowild_qr_tv_setting, R.id.gowild_qr_tv_back})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.gowild_qr_tv_setting:
                onBackPressed();
            case R.id.gowild_qr_tv_back:
                onBackPressed();
            default:
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    // ===========================================================
    // Methods
    // ===========================================================


    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================

}
