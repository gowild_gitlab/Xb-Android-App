package com.gowild.mobileclient.activity;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;

import com.gowild.mobileclient.R;
import com.gowild.mobileclient.base.baseactivity.GowildBaseActivity;
import com.gowild.mobileclient.config.Constants;
import com.gowild.mobileclient.fragment.PhotoAndVideoBaseFrament;
import com.gowild.mobileclient.fragment.PhotoListFragment;
import com.gowild.mobileclient.fragment.VideoListFragment;
import com.gowild.mobileclient.utils.CommonDataUtil;
import com.gowild.mobileclient.widget.MyToolBar;
import com.gowild.mobileclient.widget.PagerSlidingTabStrip;
import com.gowild.mobileclient.widget.dialog.GowildNormalDialog;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by 王揆 on 2016/7/19.
 */
public class PhotoAndMediaListActivity extends GowildBaseActivity {

    public static final int INDEX_PHOTO = 0;
    public static final int INDEX_MEDIA = 1;

//    @Bind(R.id.gowild_tab_view)
//    protected GowildTabview        mTabView;
    @Bind(R.id.gowild_tab)
    protected PagerSlidingTabStrip mTabStrip;
    @Bind(R.id.vp_photo_media)
    protected ViewPager            mViewPager;
    @Bind(R.id.view_toolbar)
    protected MyToolBar            mToolBar;

    private boolean bPhoto;

    private boolean bSelectMode;

    private ArrayList<PhotoAndVideoBaseFrament> mFragments = new ArrayList<>();
    private boolean mOnlyPhoto;
    private GowildNormalDialog       mDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        mOnlyPhoto = getIntent().getBooleanExtra(Constants.BPHOTO, false);
        if (mOnlyPhoto) {
            mTabStrip.setVisibility(View.GONE);
            mFragments.add(new PhotoListFragment());
            mViewPager.setAdapter(mAdapter);
            mViewPager.setCurrentItem(INDEX_PHOTO);
        } else {
            initPhotoAndMedia();
        }
        initToolBar();
    }

    @Override
    protected int onSetTopbarView() {
        return R.layout.view_gowild_toolbar;
    }

    @Override
    protected int onSetContentView() {
        return R.layout.activity_gowild_photo_media;
    }

    @Override
    protected int onSetTopBarHeight() {
        return getResources().getDimensionPixelSize(R.dimen.gowild_toolbar_height);
    }

    private void initToolBar() {
        if (mOnlyPhoto) {
            mToolBar.setTitle(CommonDataUtil.getCurrentRobotTitle() + getString(R.string.gowild_who_photo));
        } else {
            mToolBar.setTitle(getString(R.string.gowild_av_monitor_photo));

        }
        bSelectMode = false;
        mToolBar.setActionStr(bSelectMode ? R.string.gowild_cancel : R.string.gowild_edit);
        mToolBar.setActionListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bSelectMode = !bSelectMode;
                for (int i = 0 ; i < mFragments.size(); i++) {
                    mFragments.get(i).setSelectMode(bSelectMode);
                }
                mToolBar.setActionStr(bSelectMode ? R.string.gowild_cancel : R.string.gowild_edit);
            }
        });
    }

    private void initPhotoAndMedia() {
        mFragments = new ArrayList<>();
        PhotoAndVideoBaseFrament fragment = new PhotoListFragment();
        Bundle bundle = new Bundle();
        bundle.putBoolean(PhotoListFragment.ISLOCAL, true);
        fragment.setArguments(bundle);
        mFragments.add(fragment);
        mFragments.add(new VideoListFragment());
        //mTabView.setTabNum(mFragments.size());
        mViewPager.setAdapter(mAdapter);

        mTabStrip.setViewPager(mViewPager);
        mViewPager.setCurrentItem(INDEX_PHOTO);
        setPhotoTabTitle(0, true);
        setMediaTabTitle(0);

//        mTabView.setTabChangeListener(new GowildTabview.OnTabChangeListener() {
//            @Override
//            public void onTabChanged(int tabIndex) {
//                mViewPager.setCurrentItem(tabIndex);
//            }
//        });
        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                mToolBar.setActionVisible(mFragments.get(position).getInfoSize() == 0 ? View.INVISIBLE : View.VISIBLE);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    public void setPhotoTabTitle(int num, boolean islocal) {
        if (islocal) {
            mTabStrip.setTabStr(String.format(getString(R.string.gowild_title_photo), num), 0);
        }
        if (mViewPager.getCurrentItem() == INDEX_PHOTO) {
            mToolBar.setActionVisible(num == 0 ? View.INVISIBLE : View.VISIBLE);
        }
    }

    public void setMediaTabTitle(int num) {
        mTabStrip.setTabStr(String.format(getString(R.string.gowild_title_media), num), 1);
        if (mViewPager.getCurrentItem() == INDEX_MEDIA) {
            mToolBar.setActionVisible(num == 0 ? View.INVISIBLE : View.VISIBLE);
        }
    }

    public void changeActionVisible(int visible) {
        mToolBar.setActionVisible(visible);
    }

    private PagerAdapter mAdapter = new FragmentPagerAdapter(getSupportFragmentManager()) {
        @Override
        public Fragment getItem(int position) {
            return mFragments.get(position);
        }

        @Override
        public int getCount() {
            return mFragments.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return getItem(position).toString();
        }
    };

}
