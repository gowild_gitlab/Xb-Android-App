/**
 * Copyright (C)  2016 深圳市狗尾草智能科技有限公司
 * GowildXB
 * GowildSettingPwdActivity.java
 */
package com.gowild.mobileclient.activity;

import android.text.TextUtils;
import android.text.method.DigitsKeyListener;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.gowild.mobile.libcommon.utils.Logger;
import com.gowild.mobile.libcommon.utils.SpUtils;
import com.gowild.mobile.libcommon.utils.ToastUtils;
import com.gowild.mobileclient.R;
import com.gowild.mobileclient.activity.base.GowildLoginAndRegisterActivity;
import com.gowild.mobileclient.activity.login.GowildLunchActivity;
import com.gowild.mobileclient.activity.login.GowildWifiConfigActivity;
import com.gowild.mobileclient.config.Constants;
import com.gowild.mobileclient.config.IntContants;
import com.gowild.mobileclient.event.LoginResult;
import com.gowild.mobileclient.manager.GowildAsyncHttpResponseHandler;
import com.gowild.mobileclient.manager.GowildHttpManager;
import com.gowild.mobileclient.manager.LoginMananger;
import com.gowild.mobileclient.model.Lunch;
import com.gowild.mobileclient.model.PasswordBackBean;
import com.gowild.mobileclient.vo.LunchHeader;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.RequestParams;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import butterknife.Bind;
import butterknife.OnClick;

/**
 * @author shuai. wang
 * @version 1.0
 *          <p><strong>Features draft description.主要功能介绍</strong></p>
 * @since 2016/8/1 14:40
 */
public class GowildSettingPwdActivity extends GowildLoginAndRegisterActivity {
	// ===========================================================
	// Static Fields
	// ===========================================================
	private static final String PASSWORD = "password";
	// ===========================================================
	// HostConstants
	// ===========================================================

	private static final String TAG = GowildSettingPwdActivity.class.getSimpleName();
	@Bind(R.id.gowild_settingpwd_pwd)
	EditText mGowildSettingpwdPwd;
	@Bind(R.id.gowild_settingpwd_tv_pwd)
	EditText mGowildSettingpwdTvPwd;
	@Bind(R.id.gowild_settingpwd_tv_lunch)
	TextView mGowildSettingpwdTvLunch;
	private String mClazz;

	// ===========================================================
	// Methods for/from SuperClass/Interfaces
	// ===========================================================
	@Override
	public void onBackPressed() {
		super.onBackPressed();
		finish();
	}

	@Override
	public int getLayout() {
		return R.layout.activity_gowild_settingpwd;
	}

	@Override
	protected void initData() {

//		[A-Z0-9a-z!@#$%^&*.~///{//}|()'/"?><,.`//+-=_//[//]:;]+
		String digists = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ!@#$%^&*.~///{//}|()'/\"?><,.`//+-=_//[//]:;]+";
		mGowildSettingpwdPwd.setKeyListener(DigitsKeyListener.getInstance(digists));
		mGowildSettingpwdTvPwd.setKeyListener(DigitsKeyListener.getInstance(digists));
		mClazz = getIntent().getStringExtra(Constants.CLASS);
	}

	@Override
	protected void onPause() {
		super.onPause();
	}

	@Override
	protected void initEvent() {

	}

	// ===========================================================
	// Fields
	// ===========================================================

	// ===========================================================
	// Methods
	// ===========================================================
	@OnClick(R.id.gowild_settingpwd_tv_lunch)
	public void onClick(View view) {
		//设置帐号
		String pwd1 = mGowildSettingpwdPwd.getText().toString().trim();
		String pwd2 = mGowildSettingpwdTvPwd.getText().toString().trim();

		if (pwd1.length() < 6){
			ToastUtils.showCustom(this, "密码至少要6位", false);
			return;
		}

		if (TextUtils.isEmpty(pwd1) || TextUtils.isEmpty(pwd2)) {
			ToastUtils.showCustom(this, getString(R.string.gowild_pwd_not_exit), false);
			return;
		}

		if (!pwd1.equals(pwd2)) {
			ToastUtils.showCustom(this, getString(R.string.gowild_pwd_not_equals), false);
			mGowildSettingpwdPwd.setText("");
			mGowildSettingpwdTvPwd.setText("");
			return;
		}

		//发起网络请求
		showLoading(R.string.loading);
		RequestParams params = new RequestParams();
		params.put(Constants.USERNAME, SpUtils.getString(this, Constants.USERNAME));
		params.put(PASSWORD, mGowildSettingpwdTvPwd.getText().toString().trim());
		params.put(Constants.TOKEN, SpUtils.getString(this, Constants.ACCESS_TOKEN_NOBEAR));
		AsyncHttpClient client = new AsyncHttpClient();
		String url = null;
		if (mClazz.equals(GowildSplashActivity.class.getSimpleName())) {
			//注册
			url = GowildHttpManager.getUrl(GowildHttpManager.URL_REGIEST_PWD);
			client.post(GowildSettingPwdActivity.this, url, params, new GowildAsyncHttpResponseHandler<PasswordBackBean>(GowildSettingPwdActivity.this) {
				@Override
				public void onSuccess(int statusCode, PasswordBackBean responseBody) {
					if (responseBody.access_token != null) {
						SpUtils.putBoolean(GowildSettingPwdActivity.this, Constants.REGISTER, true);
						SpUtils.putString(GowildSettingPwdActivity.this, Constants.ACCESS_TOKEN, "Bearer " + responseBody.access_token);
						SpUtils.putString(GowildSettingPwdActivity.this, Constants.ACCESS_TOKEN_NOBEAR, responseBody.access_token);
						SpUtils.putInt(GowildSettingPwdActivity.this, Constants.EXPIRES_IN, responseBody.expires_in);
						SpUtils.putString(GowildSettingPwdActivity.this, Constants.REFRESH_TOKEN, responseBody.refresh_token);
						LoginMananger.getInstence().TCPLoginRequest();
					} else {
						RequestParams params = new RequestParams();
						params.put(Constants.USERNAME, SpUtils.getString(GowildSettingPwdActivity.this, Constants.USERNAME));
						params.put("password", mGowildSettingpwdTvPwd.getText().toString().trim());
						params.put("grant_type", "password");

						AsyncHttpClient client = new AsyncHttpClient();
						client.addHeader(Constants.AUTHORIZATION, Constants.AUTHORIZATION_DESC);
						client.post(GowildSettingPwdActivity.this,GowildHttpManager.getTokenURL(GowildHttpManager.URL_LUNCH), params, new GowildAsyncHttpResponseHandler<LunchHeader.DataEntity>(GowildSettingPwdActivity.this) {
							@Override
							public void onSuccess(int statusCode, LunchHeader.DataEntity responseBody) {
								//保存token
								SpUtils.putBoolean(GowildSettingPwdActivity.this, Constants.REGISTER, true);
								SpUtils.putString(GowildSettingPwdActivity.this, Constants.ACCESS_TOKEN, "Bearer " + responseBody.access_token);
								//保存refresh_token
								SpUtils.putString(GowildSettingPwdActivity.this, Constants.REFRESH_TOKEN, responseBody.refresh_token);
								//跳
								SpUtils.putString(GowildSettingPwdActivity.this, Constants.ACCESS_TOKEN_NOBEAR, responseBody.access_token);
								LoginMananger.getInstence().TCPLoginRequest();
							}

							@Override
							public void onFailure(int statusCode, String responseBody) {
								hideLoading();
							}

						});
					}
				}

				@Override
				public void onFailure(int statusCode, String responseBody) {
					hideLoading();
				}
			});
		} else {
			//忘记密码
			url = GowildHttpManager.getUrl(GowildHttpManager.URL_SETTINGPWD);
			client.post(GowildSettingPwdActivity.this, url, params, new GowildAsyncHttpResponseHandler<Lunch>(GowildSettingPwdActivity.this) {
				@Override
				public void onSuccess(int statusCode, Lunch responseBody) {
					RequestParams params = new RequestParams();
					params.put(Constants.USERNAME, SpUtils.getString(GowildSettingPwdActivity.this, Constants.USERNAME));
					params.put("password", mGowildSettingpwdTvPwd.getText().toString().trim());
					params.put("grant_type", "password");
					AsyncHttpClient client = new AsyncHttpClient();
					client.addHeader(Constants.AUTHORIZATION, Constants.AUTHORIZATION_DESC);
					client.post(GowildSettingPwdActivity.this, GowildHttpManager.getTokenURL(GowildHttpManager.URL_LUNCH), params, new GowildAsyncHttpResponseHandler<LunchHeader.DataEntity>(GowildSettingPwdActivity.this) {
						@Override
						public void onSuccess(int statusCode, LunchHeader.DataEntity responseBody) {
							hideLoading();
							SpUtils.putBoolean(GowildSettingPwdActivity.this, Constants.REGISTER, true);
							//保存token
							SpUtils.putString(GowildSettingPwdActivity.this, Constants.ACCESS_TOKEN, "Bearer " + responseBody.access_token);
							//保存refresh_token
							SpUtils.putString(GowildSettingPwdActivity.this, Constants.REFRESH_TOKEN, responseBody.refresh_token);
							//跳
							SpUtils.putString(GowildSettingPwdActivity.this, Constants.ACCESS_TOKEN_NOBEAR, responseBody.access_token);
							nextActivtiy(GowildLunchActivity.class);
							finish();
						}

						@Override
						public void onFailure(int statusCode, String responseBody) {
							hideLoading();
						}

					});
				}

				@Override
				public void onFailure(int statusCode, String responseBody) {
					hideLoading();
				}
			});
		}
	}


	@Subscribe(threadMode = ThreadMode.MAIN)
	public void subLoginResult(LoginResult result){
		hideLoading();
		switch (result.type) {
			case IntContants.TYPE_ROBOT_SUCESS:
			case IntContants.TYPE_ROBOT_OUTLINE:
			case IntContants.TYPE_ROBOT_NO_ROBOT:
				if (mClazz.equals(GowildSplashActivity.class.getSimpleName())) {
					nextActivtiy(GowildWifiConfigActivity.class);
//					finish();
				} else {
					EventBus.getDefault().post("finish");
					nextActivtiy(GowildLunchActivity.class);
				}
				break;
			case -2:
				//token过期
				ToastUtils.showCustom(GowildSettingPwdActivity.this, "登录过期,请重新登录", false);
				break;
			default:
				break;
		}
	}
}

