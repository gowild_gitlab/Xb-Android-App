/**
 * Copyright (C)  2016 深圳市狗尾草智能科技有限公司
 * GowildXB
 * GowildApplianceBrandActivity.java
 */
package com.gowild.mobileclient.activity.appliances;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.gowild.mobileclient.R;
import com.gowild.mobileclient.activity.base.GowildHasTitileActivity;
import com.gowild.mobileclient.adapter.SortAdapter;
import com.gowild.mobileclient.model.SortModel;
import com.gowild.mobileclient.utils.CharacterParser;
import com.gowild.mobileclient.utils.PinyinComparator;
import com.gowild.mobileclient.widget.ClearEditText;
import com.gowild.mobileclient.widget.SideBar;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * @author zhangct
 * @version 1.0
 *          <p><strong>设备品牌</strong></p>
 * @since 2016/8/4 9:24
 */
public class GowildApplianceBrandActivity extends GowildHasTitileActivity {

    // ===========================================================
    // HostConstants
    // ===========================================================

    private static final String TAG = GowildApplianceBrandActivity.class.getSimpleName();

    // ===========================================================
    // Static Fields
    // ===========================================================

    // ===========================================================
    // Fields
    // ===========================================================
    @Bind(R.id.country_lvcountry)
    ListView sortListView;
    @Bind(R.id.sidrbar)
    SideBar sideBar;
    @Bind(R.id.dialog)
    TextView dialog;
    @Bind(R.id.filter_edit)
    ClearEditText mClearEditText;
    @Bind(R.id.tv_search)
    TextView mTvSearch;

    private SortAdapter adapter;
    private Boolean isFirst = false;
    private int mScene = -1;
    private int mDeviceType = -1;

    /**
     * 汉字转换成拼音的类
     */
    private CharacterParser characterParser;
    private List<SortModel> SourceDateList;

    /**
     * 根据拼音来排列ListView里面的数据类
     */
    private PinyinComparator pinyinComparator;

    // ===========================================================
    // Constructors
    // ===========================================================


    // ===========================================================
    // Getter or Setter
    // ===========================================================


    // ===========================================================
    // Methods for/from SuperClass/Interfaces
    // ===========================================================
    @Override
    protected int onSetContentView() {
        return R.layout.activity_gowild_appliance_brand;
    }

    @Override
    protected void initEvent() {
        setTitle("空调品牌");
    }

    @Override
    protected void initView() {
        super.initView();
        Bundle bundle = getIntent().getExtras();
        if (null != bundle) {
            mScene = bundle.getInt(GowildApplianceSearchActivity.INTENT_EXTRA_SCENE);
            mDeviceType = bundle.getInt(GowildApplianceSearchActivity.INTENT_EXTRA_DEVICE_TYPE);
        }
        initViews();
    }

    // ===========================================================
    // Methods
    // ===========================================================
    private void initViews() {
        //实例化汉字转拼音类
        characterParser = CharacterParser.getInstance();
        pinyinComparator = new PinyinComparator();
        sideBar.setTextView(dialog);
        //设置右侧触摸监听
        sideBar.setOnTouchingLetterChangedListener(new SideBar.OnTouchingLetterChangedListener() {

            @Override
            public void onTouchingLetterChanged(String s) {
                //该字母首次出现的位置
                int position = adapter.getPositionForSection(s.charAt(0));
                if (position != -1) {
                    sortListView.setSelection(position);
                }

            }
        });

        sortListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                String brandName = null;
                //这里要利用adapter.getItem(position)来获取当前position所对应的对象
                Log.d(TAG, "position = " + position);
                brandName = ((SortModel) adapter.getItem(position)).getName();
                Bundle bundle = new Bundle();
                bundle.putInt(GowildApplianceSearchActivity.INTENT_EXTRA_POSITION, position);
                bundle.putString(GowildApplianceSearchActivity.INTENT_EXTRA_BRAND_NAME, brandName);
                bundle.putInt(GowildApplianceSearchActivity.INTENT_EXTRA_SCENE, mScene);
                bundle.putInt(GowildApplianceSearchActivity.INTENT_EXTRA_DEVICE_TYPE, mDeviceType);
                startActivity(GowildApplianceSearchActivity.class, bundle);
            }
        });

        SourceDateList = filledData(getResources().getStringArray(getArray()));

        // 根据a-z进行排序源数据
        Collections.sort(SourceDateList, pinyinComparator);
        adapter = new SortAdapter(GowildApplianceBrandActivity.this, SourceDateList);
        sortListView.setAdapter(adapter);


        //根据输入框输入值的改变来过滤搜索
        mClearEditText.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                //当输入框里面的值为空，更新为原来的列表，否则为过滤数据列表
                filterData(s.toString());
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {

            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
    }

    /**
     * 获得品牌数组
     *
     * @return
     */
    public int getArray() {
        int device_array = 0;
        int m_type = 0; // 代表是家电设备类型index
        //isFirst = this.getIntent().getBooleanExtra("FirstOwner", false);
        switch (m_type) {
            case 0:
                device_array = R.array.strs_air_brand;
                break;
//            case 1:
//                device_array = R.array.tv_array;
//                break;
//            case 2:
//                device_array = R.array.stb_array;
//                break;
//            case 3:
//                device_array = R.array.pjt_array;
//                break;
//            case 4:
//                device_array = R.array.fans_array;
//                break;
//            case 5:
//                device_array = R.array.dvd_array;
//                break;
        }

        return device_array;
    }


    /**
     * 为ListView填充数据
     *
     * @param date
     * @return
     */
    private List<SortModel> filledData(String[] date) {
        List<SortModel> mSortList = new ArrayList<SortModel>();

        for (int i = 0; i < date.length; i++) {
            SortModel sortModel = new SortModel();
            sortModel.setName(date[i]);
            //汉字转换成拼音
            String pinyin = characterParser.getSelling(date[i]);
            String sortString = pinyin.substring(0, 1).toUpperCase();

            // 正则表达式，判断首字母是否是英文字母
            if (sortString.matches("[A-Z]")) {
                sortModel.setSortLetters(sortString.toUpperCase());
            } else {
                sortModel.setSortLetters("#");
            }
            mSortList.add(sortModel);
        }
        return mSortList;

    }

    /**
     * 根据输入框中的值来过滤数据并更新ListView
     *
     * @param filterStr
     */
    private void filterData(String filterStr) {
        List<SortModel> filterDateList = new ArrayList<SortModel>();

        if (TextUtils.isEmpty(filterStr)) {
            filterDateList = SourceDateList;
        } else {
            filterDateList.clear();
            for (SortModel sortModel : SourceDateList) {
                String name = sortModel.getName();
                if (name.indexOf(filterStr.toString()) != -1 || characterParser.getSelling(name).startsWith(filterStr.toString())) {
                    filterDateList.add(sortModel);
                }
            }
        }
        // 根据a-z进行排序
        if (filterDateList.size() == 0) {
            mTvSearch.setVisibility(View.VISIBLE);
        } else {
            mTvSearch.setVisibility(View.GONE);
            Collections.sort(filterDateList, pinyinComparator);
            adapter.updateListView(filterDateList);
        }
    }


    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================

}
