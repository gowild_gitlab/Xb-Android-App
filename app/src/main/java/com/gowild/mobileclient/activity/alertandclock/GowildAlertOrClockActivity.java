/**
 * Copyright (C)  2016 深圳市狗尾草智能科技有限公司
 * GowildXB
 * GowildAlertOrClockActivity.java
 */
package com.gowild.mobileclient.activity.alertandclock;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.gowild.mobile.libcommon.utils.CheckNetworkUtil;
import com.gowild.mobile.libp2sp.core.listener.IDataListener;
import com.gowild.mobile.libp2sp.netty.NettyClientManager;
import com.gowild.mobileclient.R;
import com.gowild.mobileclient.base.baseactivity.GowildBaseActivity;
import com.gowild.mobileclient.config.Constants;
import com.gowild.mobileclient.fragment.GowildAlertListFragment;
import com.gowild.mobileclient.fragment.GowildClockListFragment;
import com.gowild.mobileclient.manager.GowildAsyncHttpResponseHandler;
import com.gowild.mobileclient.manager.GowildHttpManager;
import com.gowild.mobileclient.misc.GowildResponseCode;
import com.gowild.mobileclient.vo.Alert;
import com.gowild.mobileclient.vo.Clock;
import com.gowild.mobileclient.vo.RequestAlertList;
import com.gowild.mobileclient.vo.RequestClockList;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * @author JunDong Wang
 * @version 1.0
 *          <p><strong>进入闹钟提醒显示界面  修改标题</strong></p>
 * @since 2016/7/21 11:01
 */
public class GowildAlertOrClockActivity extends GowildBaseActivity {

    // ===========================================================
    // HostConstants
    // ===========================================================
    private static final String TAG = GowildAlertOrClockActivity.class.getSimpleName();
    public static final int TYPE_ADD = 0;
    public static final int TYPE_UPDATE = 1;
    private static final int TYPE_CLOCK = 0;
    private static final int TYPE_ALERT = 1;
    // ===========================================================
    // Static Fields
    // ===========================================================

    // ===========================================================
    // Fields
    // ===========================================================
    GowildAlertListFragment alertsFragment = new GowildAlertListFragment();
    ArrayList<Alert> alerts = new ArrayList<>();
    GowildClockListFragment clocksFragment = new GowildClockListFragment();
    ArrayList<Clock> clocks = new ArrayList<>();
    private RequestAlertsCallback mRequestAlertCallback;
    private RequestClocksCallback mRequestClockCallback;
    private AlertSyncListener mAlertSyncListener = new AlertSyncListener();
    private AlertRingListener mAlertRingListener = new AlertRingListener();
    private ClockSyncListener mClockSyncListener = new ClockSyncListener();
    private ClockRingListener mClockRingListener = new ClockRingListener();
    private int mPageType = TYPE_CLOCK;
    @Bind(R.id.add)
    TextView mTvAdd;
    @Bind(R.id.main_container)
    RelativeLayout mRltMainContainer;
    @Bind(R.id.clock_count)
    TextView mTvClockCount;
    @Bind(R.id.clock_line)
    View mClockDeepLine;
    @Bind(R.id.alert_count)
    TextView mTvAlertCount;
    @Bind(R.id.alert_line)
    View mAlertDeepLine;
    @Bind(R.id.viewpager)
    ViewPager mViewPager;
    @Bind(R.id.nowifi_tips)
    LinearLayout mLLtNoWifiTips;
    private FragmentsAdapter mAdapter;
    private PageChange mPageChange = new PageChange();
    private TextView mTvTitle;
    // ===========================================================
    // Constructors
    // ===========================================================


    // ===========================================================
    // Getter or Setter
    // ===========================================================


    // ===========================================================
    // Methods for/from SuperClass/Interfaces
    // ===========================================================
    @Override
    protected int onSetTopbarView() {
        return R.layout.activity_gowild_alert_clock_main_topbar;
    }

    @Override
    protected int onSetContentView() {
        return R.layout.activity_gowild_alert_clock_main;
    }

    @Override
    protected int onSetTopBarHeight() {
        return getResources().getDimensionPixelSize(R.dimen.gowild_160px);
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        mTvTitle = (TextView) findViewById(R.id.title);
        mAlertSyncListener = new AlertSyncListener();
        mAlertRingListener = new AlertRingListener();
        mClockSyncListener = new ClockSyncListener();
        mClockRingListener = new ClockRingListener();
        NettyClientManager manager = NettyClientManager.getInstance();
        manager.addHandlerListener(GowildResponseCode.ALERT_RING, mAlertRingListener);
        manager.addHandlerListener(GowildResponseCode.ALERT_SYNC, mAlertSyncListener);
        manager.addHandlerListener(GowildResponseCode.CLOCK_RING, mClockRingListener);
        manager.addHandlerListener(GowildResponseCode.CLOCK_SYNC, mClockSyncListener);
    }

    @Override
    protected void onResume() {
        super.onResume();
        isShowDialog = true;
        mRequestAlertCallback = new RequestAlertsCallback(this);
        mRequestClockCallback = new RequestClocksCallback(this);
        initAllView();
        List<Fragment> fragments = new ArrayList<>();
        fragments.add(clocksFragment);
        fragments.add(alertsFragment);
        mAdapter = new FragmentsAdapter(getSupportFragmentManager(), fragments);
        mViewPager.setAdapter(mAdapter);
        mViewPager.setCurrentItem(TYPE_CLOCK);
        mViewPager.addOnPageChangeListener(mPageChange);
        if (CheckNetworkUtil.isNetworkAvailable(this)) {
            initShowViewPage();
            requestClocks();
            requestAlerts();
        } else {
            initShowNoWifiTips();
        }
    }

    boolean isShowDialog;

    @Override
    protected void onPause() {
        super.onPause();
        isShowDialog = false;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        NettyClientManager manager = NettyClientManager.getInstance();
        manager.removeHandlerListener(GowildResponseCode.ALERT_RING, mAlertRingListener);
        manager.removeHandlerListener(GowildResponseCode.ALERT_SYNC, mAlertSyncListener);
        manager.removeHandlerListener(GowildResponseCode.CLOCK_RING, mClockRingListener);
        manager.removeHandlerListener(GowildResponseCode.CLOCK_SYNC, mClockSyncListener);
    }

    // ===========================================================
    // Methods
    // ===========================================================

    private void initAllView() {
        mTvAdd.setVisibility(View.GONE);
        mRltMainContainer.setVisibility(View.GONE);
        mLLtNoWifiTips.setVisibility(View.GONE);
    }

    private void initShowViewPage() {
        mTvAdd.setVisibility(View.VISIBLE);
        mRltMainContainer.setVisibility(View.VISIBLE);
        mViewPager.setCurrentItem(mPageType);
    }

    private void initShowNoWifiTips() {
        mTvAdd.setVisibility(View.GONE);
        mRltMainContainer.setVisibility(View.GONE);
        mLLtNoWifiTips.setVisibility(View.VISIBLE);
    }

    public void requestAlerts() {
        if (isShowDialog) {
            showLoading(R.string.loading);
        }
        GowildHttpManager.requestAlertList(this, new RequestAlertList(), mRequestAlertCallback);
    }

    public void requestClocks() {
        if (isShowDialog) {
            showLoading(R.string.loading);
        }
        GowildHttpManager.requestClockList(this, new RequestClockList(), mRequestClockCallback);
    }

    @OnClick({R.id.back, R.id.add, R.id.clock, R.id.alert, R.id.config_network})
    public void onclick(View view) {
        Intent intent;
        switch (view.getId()) {
            case R.id.back:
                onBackPressed();
                break;
            case R.id.add:
                if (mPageType == TYPE_CLOCK) {
                    intent = new Intent(getBaseContext(), GowildAddOrUpdateClockActivity.class);
                    intent.putExtra(Constants.TYPE, TYPE_ADD);
                    startActivity(intent);
                } else {
                    intent = new Intent(getBaseContext(), GowildAddOrUpdateAlertActivity.class);
                    intent.putExtra(Constants.TYPE, TYPE_ADD);
                    startActivity(intent);
                }
                break;
            case R.id.clock:
                if (mPageType != TYPE_CLOCK) {
                    mPageType = TYPE_CLOCK;
                    mAlertDeepLine.setVisibility(View.GONE);
                    mClockDeepLine.setVisibility(View.VISIBLE);
                    initShowViewPage();
                }
                break;
            case R.id.alert:
                if (mPageType != TYPE_ALERT) {
                    mPageType = TYPE_ALERT;
                    mClockDeepLine.setVisibility(View.GONE);
                    mAlertDeepLine.setVisibility(View.VISIBLE);
                    initShowViewPage();
                }
                break;
            case R.id.config_network:
                intent = new Intent(Settings.ACTION_SETTINGS);
                startActivity(intent);
                break;
        }
    }

    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================
    private class FragmentsAdapter extends FragmentPagerAdapter {
        private List<Fragment> mFragments = new ArrayList<>();


        public FragmentsAdapter(FragmentManager fm, List<Fragment> fragments) {
            super(fm);
            mFragments.clear();
            mFragments.addAll(fragments);
            notifyDataSetChanged();
        }

        @Override
        public Fragment getItem(int position) {
            return mFragments.get(position);
        }

        @Override
        public int getCount() {
            return mFragments.size();
        }
    }

    private class PageChange implements ViewPager.OnPageChangeListener {

        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

        }

        @Override
        public void onPageSelected(int position) {
            if (mPageType != position) {
                if (position == TYPE_CLOCK) {
                    mPageType = TYPE_CLOCK;
                    mClockDeepLine.setVisibility(View.VISIBLE);
                    mAlertDeepLine.setVisibility(View.GONE);
                    if (clocks.size() == 0) {
                        mTvAdd.setVisibility(View.GONE);
                    } else {
                        mTvAdd.setVisibility(View.VISIBLE);
                    }
                } else {
                    mPageType = TYPE_ALERT;
                    mClockDeepLine.setVisibility(View.GONE);
                    mAlertDeepLine.setVisibility(View.VISIBLE);
                    if (alerts.size() == 0) {
                        mTvAdd.setVisibility(View.GONE);
                    } else {
                        mTvAdd.setVisibility(View.VISIBLE);
                    }
                }
            }
        }

        @Override
        public void onPageScrollStateChanged(int state) {

        }
    }

    private class RequestAlertsCallback extends GowildAsyncHttpResponseHandler<Alert> {

        /**
         * 构造方法
         *
         * @param context 用于处理授权过期消息，会被转成ApplicationContext
         */
        public RequestAlertsCallback(Context context) {
            super(context, true);
        }

        @Override
        public void onSuccess(int statusCode, Alert responseBody) {

        }


        @Override
        public void onSuccess(int statusCode, ArrayList<Alert> responseBody) {
            alerts.clear();
            hideLoading();
            if (responseBody != null) {
                alerts.addAll(responseBody);
                alertsFragment.setAlertList(alerts);
                String str = getResources().getString(R.string.gowild_alert_and_clock_alertcount);
                str = str.replace("%", Integer.toString(alerts.size()));
                mTvAlertCount.setText(str);
            }
        }

        @Override
        public void onFailure(int statusCode, String responseBody) {
            hideLoading();
        }
    }

    private class RequestClocksCallback extends GowildAsyncHttpResponseHandler<Clock> {

        /**
         * 构造方法
         *
         * @param context 用于处理授权过期消息，会被转成ApplicationContext
         */
        public RequestClocksCallback(Context context) {
            super(context, true);
        }

        @Override
        public void onSuccess(int statusCode, Clock responseBody) {

        }


        @Override
        public void onSuccess(int statusCode, ArrayList<Clock> responseBody) {
            clocks.clear();
            hideLoading();
            if (responseBody != null) {
                clocks.addAll(responseBody);
                clocksFragment.setClockList(clocks);
                String str = getResources().getString(R.string.gowild_alert_and_clock_clockcount);
                str = str.replace("%", Integer.toString(clocks.size()));
                mTvClockCount.setText(str);
            }
        }

        @Override
        public void onFailure(int statusCode, String responseBody) {
            hideLoading();
//            ToastUtils.showCustom(getBaseContext(), getString(R.string.gowild_network_error), false);
        }
    }

    private class AlertSyncListener implements IDataListener {

        @Override
        public void onReceiveData(Object data) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    requestAlerts();
                }
            });

        }

        @Override
        public void onActiveChanged(boolean isActive) {

        }
    }

    private class AlertRingListener implements IDataListener {

        @Override
        public void onReceiveData(Object data) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    requestAlerts();
                }
            });
        }

        @Override
        public void onActiveChanged(boolean isActive) {

        }
    }

    private class ClockSyncListener implements IDataListener {

        @Override
        public void onReceiveData(Object data) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    requestClocks();
                }
            });
        }

        @Override
        public void onActiveChanged(boolean isActive) {

        }
    }

    private class ClockRingListener implements IDataListener {

        @Override
        public void onReceiveData(Object data) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    requestClocks();

                }
            });
        }

        @Override
        public void onActiveChanged(boolean isActive) {

        }
    }

}
