/**
 * Copyright (C)  2016 深圳市狗尾草智能科技有限公司
 * GowildXB
 * GowildTaskActivity.java
 */
package com.gowild.mobileclient.activity;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;

import com.gowild.mobileclient.R;
import com.gowild.mobileclient.activity.base.GowildLoginAndRegisterActivity;
import com.gowild.mobileclient.config.Constants;
import com.gowild.mobileclient.event.PetTaskAndGorwTask;
import com.gowild.mobileclient.fragment.GowildeDailyTaskFragment;
import com.gowild.mobileclient.fragment.GowildeGrowTaskFragment;
import com.gowild.mobileclient.utils.CommonDataUtil;
import com.gowild.mobileclient.widget.MyToolBar;
import com.gowild.mobileclient.widget.PagerSlidingTabStrip;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import butterknife.Bind;

/**
 * @author shuai. wang
 * @version 1.0
 *          <p><strong>Features draft description.主要功能介绍</strong></p>
 * @since 2016/7/23 17:08ho
 */
public class GowildTaskActivity extends GowildLoginAndRegisterActivity {

    // ===========================================================
    // HostConstants
    // ===========================================================

    private static final String TAG = GowildTaskActivity.class.getSimpleName();
    @Bind(R.id.gowild_task_pt)
    PagerSlidingTabStrip mGowildTaskPt;
    @Bind(R.id.gowild_task_vp)
    ViewPager mGowildTaskVp;

    // ===========================================================
    // Static Fields
    // ===========================================================
    public static final int DAILY_TASK = 0;
    public static final int GROW_TASK = 1;
    // ===========================================================
    // Fields
    // ===========================================================
    private String[] title = {"每日任务", "成长任务"};
    private String task;
    private String honor;


    // ===========================================================
    // Constructors
    // ===========================================================


    // ===========================================================
    // Getter or Setter
    // ===========================================================


    // ===========================================================
    // Methods for/from SuperClass/Interfaces
    // ===========================================================
    @Override
    public int getLayout() {
        return R.layout.activity_gowild_task;
    }

    @Override
    protected void initView() {
        MyToolBar toolbar = (MyToolBar) findViewById(R.id.view_toolbar);
        String title = CommonDataUtil.getCurrentRobotTitle() + getStringById(R.string.gowild_task_title);
        toolbar.setTitle(title);
    }

    @Override
    protected void initData() {
        task = getIntent().getStringExtra(Constants.TASK);
        honor = getIntent().getStringExtra(Constants.HONOR);
        if (!TextUtils.isEmpty(task)) {
            title[0] = "每日任务" + task;
        }
        if (!TextUtils.isEmpty(honor)) {
            title[1] = "成长任务" + honor;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    protected void initEvent() {
        mGowildTaskVp.setAdapter(mTaskadpter);
        mGowildTaskPt.setViewPager(mGowildTaskVp);
    }

    private FragmentPagerAdapter mTaskadpter = new FragmentPagerAdapter(getSupportFragmentManager()) {

        private GowildeGrowTaskFragment gFragment;
        private GowildeDailyTaskFragment dfragment;

        @Override
        public Fragment getItem(int position) {
            if (position == DAILY_TASK) {
                if (dfragment == null) {
                    dfragment = new GowildeDailyTaskFragment();
                    dfragment.setOnFreshLinsenter(new GowildeDailyTaskFragment.OnFreshLinsenter() {
                        @Override
                        public void start() {
                            showLoading(getString(R.string.loading));
                        }

                        @Override
                        public void finish() {
                            hideLoading();
                        }
                    });
                    dfragment.refreshData();
                }
                return dfragment;

            } else {
                if (gFragment == null) {
                    gFragment = new GowildeGrowTaskFragment();
                    gFragment.setOnFreshLinsenter(new GowildeGrowTaskFragment.OnFreshLinsenter() {
                        @Override
                        public void start() {
                            showLoading(getString(R.string.loading));
                        }

                        @Override
                        public void finish() {
                            hideLoading();
                        }
                    });

                }
                return gFragment;
            }
        }

        @Override
        public int getCount() {
            return title.length;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return title[position];
        }
    };


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void subUpdata(PetTaskAndGorwTask pet) {
        title[0] = "每日任务" + pet.task;
        title[1] = "成长任务" + pet.grow;
        mTaskadpter.notifyDataSetChanged();
    }


    // ===========================================================
    // Methods
    // ===========================================================


    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================

}
