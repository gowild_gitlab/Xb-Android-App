package com.gowild.mobileclient.activity.setting.update;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.net.Uri;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.view.ContextThemeWrapper;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.gowild.mobile.libcommon.utils.CheckNetworkUtil;
import com.gowild.mobile.libcommon.utils.ToastUtils;
import com.gowild.mobile.libp2sp.core.exception.NotConnectedException;
import com.gowild.mobile.libp2sp.core.listener.IDataListener;
import com.gowild.mobile.libp2sp.netty.NettyClientManager;
import com.gowild.mobileclient.R;
import com.gowild.mobileclient.callback.OnUpdateDoneListener;
import com.gowild.mobileclient.manager.GowildTCPManager;
import com.gowild.mobileclient.misc.GowildResponseCode;
import com.gowild.mobileclient.protocol.PhoneUpgradeInfoPro;
import com.gowild.mobileclient.widget.NumberProgressBar;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * 更新管理者
 */
public class UpdateManager {

    private Context mContext;
    // 返回的安装包url

    private AlertDialog noticeDialog;
    private AlertDialog downloadDialog = null;

    /* 下载包安装路径 */
    private static String savePath = "";

    private static String saveFileName = "";

    private static final int DOWN_UPDATE = 1;

    private static final int DOWN_OVER = 2;


    public UpdateManager(Context context) {
        this.mContext = context;
    }

    /**
     * 需要更新
     */
    private static final int NEEDUP = 3;

    /**
     * 下载失败
     */
    private static final int DOWN_FAIL = 4;

    /**
     * 不需要更新
     */
    private static final int NOTNEEDUP = 5;

    /**
     * 检查完成
     */
    private static final int CHECK_DONE = 6;

    private static final int UPDATE_MAX = 7;

    /**
     * 检查APP版本
     */
    private static final int MESSAGE_CHECK_VERSION = 8;

    private int progress;
    private int max;
    private VersionInfo mAppinfo = null;
    private Thread downLoadThread;

    private NumberProgressBar mGowildNumberProgressBar        = null;
    private TextView          mGowildCurProgress              = null;
    private TextView          mGowildMax                      = null;
    private Button            mGowildUpdateInBackgroundBtn    = null;
    private Button            mGowildUpdateCancelBtn          = null;
    private Button            mGowildUpdateRequireCancelBtn   = null;
    private Button            mGowildUpdateRequireDownloadBtn = null;
    private TextView          mGowildRequireTitleTv           = null;
    private TextView          mGowildRequireContentTv         = null;
    public OnUpdateDoneListener mUpdateListener;
    public final int ENFORCE = 1;//强制升级
    public boolean isForceUpdate;

    private PhoneUpgradeInfoResultListener mPhoneUpgradeInfoListener = new PhoneUpgradeInfoResultListener();

    private       boolean interceptFlag = false;
    //获取版本信息地址
    private final String  ClientURL     = "http://www.gowild.top:1234/gowild-manage/getUpgradeInfo";
    //包名
    private final String  apkName       = "com.gowild.mobileclient";

    /**
     * 当前软件已经是最新版本
     */
    private final int PHONE_VERSION_IS_NEW = 0;

    private Handler mHandler = new Handler() {
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case DOWN_UPDATE:
                    mGowildCurProgress.setText(String.valueOf(convertByteToMb(progress)));
                    mGowildNumberProgressBar.setProgress(convertByteToMb(progress));
                    break;
                case DOWN_OVER:

                    if (downloadDialog != null) {
                        downloadDialog.dismiss();
                    }
                    if (mUpdateListener != null) {
                        mUpdateListener.downDone();
                    }

                    if (mUpdateListener != null) {
                        mUpdateListener.install(saveFileName);
                    }
                    break;
                case NEEDUP:
                    interceptFlag = false;
                    showNoticeDialog();
                    break;
                case NOTNEEDUP:// 不需要升级时执行回调方法
                    interceptFlag = true;
                    if (mUpdateListener != null) {
                        mUpdateListener.isNewVersion();
                    }
                    break;
                case CHECK_DONE:// 检查完成
                    if (mUpdateListener != null) {
                        mUpdateListener.checkDone();
                    }
                    break;
                case DOWN_FAIL:
                    if (!interceptFlag) {
                        Toast.makeText(mContext, "下载失败，请稍后重试", Toast.LENGTH_LONG).show();
                    }
                    mHandler.sendEmptyMessage(NOTNEEDUP);
                    break;
                case UPDATE_MAX:
                    mGowildNumberProgressBar.setMax(convertByteToMb(max));
                    mGowildMax.setText(String.valueOf(convertByteToMb(max)));
                    break;
                case MESSAGE_CHECK_VERSION:
                    //注销监听
                    NettyClientManager.getInstance().removeHandlerListener(GowildResponseCode.PHONE_UPGRADEINFO_RESULT, mPhoneUpgradeInfoListener);
                    PhoneUpgradeInfoPro.PhoneUpgradeInfoMsg data = (PhoneUpgradeInfoPro.PhoneUpgradeInfoMsg) msg.obj;
                    if (null != data) {
                        mAppinfo = new VersionInfo();
                        if (data.getType() == PHONE_VERSION_IS_NEW) {
                            mHandler.sendEmptyMessage(NOTNEEDUP);
                            return;
                        }
                        mAppinfo.setType(data.getType());
                        mAppinfo.setEnforce(data.getConstraintUp() == ENFORCE);
                        isForceUpdate = data.getConstraintUp() == ENFORCE;
                        //                        isForceUpdate = true;se
                        mAppinfo.setSize(data.getSize());
                        mAppinfo.setVersion(data.getVersion());
                        mAppinfo.setDownloadURL(data.getDownloadUrl());
                        mAppinfo.setDisplayMessage(data.getDisplayMessage());
                        mAppinfo.setVersionCode(Integer.parseInt(data.getVersion()));
                        compareVersion();
                    }
                    break;
                default:
                    break;
            }
        }
    };

    public void onResume() {
        //注册监听
        NettyClientManager.getInstance().addHandlerListener(GowildResponseCode.PHONE_UPGRADEINFO_RESULT, mPhoneUpgradeInfoListener);
    }

    public void onPause() {
        //注销监听
        NettyClientManager.getInstance().removeHandlerListener(GowildResponseCode.PHONE_UPGRADEINFO_RESULT, mPhoneUpgradeInfoListener);
    }


    // 外部接口让主Activity调用

    /**
     * 版本比较
     */
    private void compareVersion() {
        new Thread() {
            @Override
            public void run() {
                try {
                    if (checkUpdate()) {
                        if (!Environment.getExternalStorageState()
                                        .equals(Environment.MEDIA_MOUNTED)) {
                            // 如果没有SD卡
                            savePath = "/storage/sdcard1/";
                            saveFileName = "/storage/sdcard1/" + mAppinfo.getApkName();
                        } else {
                            savePath = Environment.getExternalStorageDirectory().getAbsolutePath() + "/updateApkFile/";
                            saveFileName = Environment.getExternalStorageDirectory().getAbsolutePath() + "/updateApkFile/"
                                           + mAppinfo.getApkName();
                        }
                        mHandler.sendEmptyMessage(NEEDUP);
                    } else {
                        mHandler.sendEmptyMessage(NOTNEEDUP);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        }.start();
    }

    /**
     * 检查更新信息
     */
    public void checkUpdateInfo() {

        //判断网络
        if (!CheckNetworkUtil.isNetworkAvailable(mContext)) {
            ToastUtils.showCustom(mContext, R.string.gowild_net_error, false);
            mHandler.sendEmptyMessage(CHECK_DONE);
            return;
        }

        //注册监听
        NettyClientManager.getInstance().addHandlerListener(GowildResponseCode.PHONE_UPGRADEINFO_RESULT, mPhoneUpgradeInfoListener);
        try {
            //获得当前安装版本
            PackageInfo pi = mContext.getPackageManager().getPackageInfo(mContext.getPackageName(),
                                                                         PackageManager.GET_CONFIGURATIONS);
            int versionCode = pi.versionCode;
            GowildTCPManager.requestPhoneVersion(String.valueOf(versionCode));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /**
     * 检查是否可以更新
     *
     * @return
     */
    private boolean checkUpdate() {
        try {
            //获得当前安装版本
            PackageInfo pi = mContext.getPackageManager().getPackageInfo(mContext.getPackageName(),
                                                                         PackageManager.GET_CONFIGURATIONS);
            int versionCode = pi.versionCode;
            //从服务器获得版本信息
            if (mAppinfo != null) {
                mHandler.sendEmptyMessage(CHECK_DONE);

                //当前安装版本是否小于服务器版本
                if (versionCode < mAppinfo.getVersionCode()) {
                    return true;

                }
            }
        } catch (NameNotFoundException e) {
            e.printStackTrace();
        }
        return false;
    }

    /**
     * 从服务端获取版本信息
     *
     * @return
     */
    private VersionInfo getVersionInfoFromServer(String version) {
        VersionInfo info = null;
        URL         url  = null;
        try {
            url = new URL(ClientURL);
            if (url != null) {
                HttpURLConnection urlConn = (HttpURLConnection) url.openConnection();
                urlConn.setConnectTimeout(4000);
                urlConn.connect();
                if (urlConn.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    //勿随便更改
                    BufferedReader reader = new BufferedReader(new InputStreamReader(urlConn.getInputStream(), "utf-8"));
                    String         result = reader.readLine();
                    if (result != null && result.contains("errCode")) {
                        JSONObject object = new JSONObject(result);
                        if (object.getInt("errCode") == 100) {
                            JSONArray jsonarr = object.getJSONArray("datas");
                            for (int i = 0; i < jsonarr.length(); i++) {
                                //查找当前应用需要的版本信息
                                if (jsonarr.getJSONObject(i).getString("apkName").equals(apkName)) {
                                    info = new VersionInfo(jsonarr.getJSONObject(i));
                                    break;
                                }
                            }
                        }
                    }
                }

                if (urlConn != null) {
                    urlConn.disconnect();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            GowildTCPManager.requestPhoneVersion(version);
        } catch (NotConnectedException e) {
            e.printStackTrace();
        }


        return info;
    }

    /**
     * 弹出服务器最新版本的更新信息
     */
    private void showNoticeDialog() {
        StringBuffer title = new StringBuffer("版本更新");
        if (!CheckNetworkUtil.isWifiConnected(mContext)) {
            title.append(" 当前非WIFI网络，请注意！");
        }
        View proView = View.inflate(mContext, R.layout.gowild_update_require, null);
        mGowildRequireTitleTv = (TextView) proView.findViewById(R.id.tv_update_require_title);
        mGowildRequireContentTv = (TextView) proView.findViewById(R.id.tv_update_require_content);
        mGowildUpdateRequireCancelBtn = (Button) proView.findViewById(R.id.btn_update_require_cancel);
        mGowildUpdateRequireDownloadBtn = (Button) proView.findViewById(R.id.btn_update_require_download);
        mGowildRequireTitleTv.setText(title);
        mGowildRequireContentTv.setText(mAppinfo.getDisplayMessage());
        noticeDialog = new AlertDialog.Builder(new ContextThemeWrapper(mContext, R.style.Translucent_NoTitle)).create();
        final boolean enForce = mAppinfo.getEnforce();
        if (enForce) {
            mGowildUpdateRequireCancelBtn.setText(mContext.getResources().getString(R.string.update_require_exit));
        } else {
            mGowildUpdateRequireCancelBtn.setText(mContext.getResources().getString(R.string.update_require_cancel));
        }
        //取消或者退出应用
        mGowildUpdateRequireCancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (enForce) {
                    if (null != mUpdateListener) {
                        mUpdateListener.exitApplication();
                    }
                    noticeDialog.dismiss();
                    return;
                } else {
                    if (null != mUpdateListener) {
                        mUpdateListener.cancel();
                    }
                    noticeDialog.dismiss();
                    return;
                }
            }
        });
        //下载更新
        mGowildUpdateRequireDownloadBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                noticeDialog.dismiss();
                if (apkExit()) {
                    installApk();
                } else {
                    showDownloadDialog();
                }
            }
        });
        noticeDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        noticeDialog.setView(proView, 0, 0, 0, 0);
        noticeDialog.show();
        noticeDialog.setCanceledOnTouchOutside(false);
        noticeDialog.setCancelable(false);
        noticeDialog.show();

    }

    /**
     * 弹出下载框
     */
    private void showDownloadDialog() {
        View proView = View.inflate(mContext, R.layout.gowild_update_progess, null);
        mGowildNumberProgressBar = (NumberProgressBar) proView.findViewById(R.id.numberbar1);
        mGowildCurProgress = (TextView) proView.findViewById(R.id.cur_progress);
        mGowildMax = (TextView) proView.findViewById(R.id.update_total);
        mGowildUpdateInBackgroundBtn = (Button) proView.findViewById(R.id.btn_update_background);
        mGowildUpdateCancelBtn = (Button) proView.findViewById(R.id.btn_update_cancel);
        mGowildNumberProgressBar.setProgress(0);
        if (mAppinfo.getEnforce()) {
            mGowildUpdateCancelBtn.setVisibility(View.GONE);
            mGowildNumberProgressBar.setVisibility(View.GONE);
        } else {
            mGowildUpdateCancelBtn.setVisibility(View.VISIBLE);
            mGowildNumberProgressBar.setVisibility(View.VISIBLE);
        }
        downloadDialog = new AlertDialog.Builder(new ContextThemeWrapper(mContext, R.style.Translucent_NoTitle)).create();
        //后台升级
        mGowildUpdateInBackgroundBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                downloadDialog.dismiss();
                interceptFlag = false;
                ToastUtils.showCustom(mContext, "已经切换至后台", false);
                if (null != mUpdateListener) {
                    mUpdateListener.cancel();
                }
            }
        });
        //取消升级
        mGowildUpdateCancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                downloadDialog.dismiss();
                mHandler.sendEmptyMessage(NOTNEEDUP);
            }
        });
        downloadDialog.setView(proView, 0, 0, 0, 0);
        downloadDialog.show();
        downloadDialog.setCanceledOnTouchOutside(false);
        downloadDialog.setCancelable(false);
        downloadDialog.show();

        downloadApk();
    }


    /**
     * 是否下载完成
     */
    private boolean apkExit() {
        //        //开始下载安装包
        File file = new File(saveFileName);

        if (file.exists() && (mAppinfo.getSize() == file.length())) {
            return true;
        } else {
            //需要下载
            return false;
        }
    }


    /**
     * 下载安装包线程
     */
    private Runnable mdownApkRunnable = new Runnable() {
        public void run() {
            //此处应该检查是否下载完成

            int msg = DOWN_FAIL;
            try {
                URL               url  = new URL(mAppinfo.getDownloadURL());
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.connect();
                int         length = conn.getContentLength();
                InputStream is     = conn.getInputStream();
                File        file   = new File(savePath);
                if (!file.exists()) {
                    file.mkdir();
                }
                String           apkFile = saveFileName;
                File             ApkFile = new File(apkFile);
                FileOutputStream fos     = new FileOutputStream(ApkFile);

                int  count = 0;
                byte buf[] = new byte[1024 * 10];
                max = length;
                mHandler.sendEmptyMessage(UPDATE_MAX);
                do {
                    int numread = is.read(buf);
                    count += numread;
                    progress = count;
                    // 更新进度
                    mHandler.sendEmptyMessage(DOWN_UPDATE);
                    if (numread <= 0) {
                        // 下载完成通知安装
                        msg = DOWN_OVER;
                        break;
                    }
                    fos.write(buf, 0, numread);
                } while (!interceptFlag);// 点击取消就停止下载.
                if (interceptFlag) {
                    msg = NOTNEEDUP;
                }
                fos.close();
                is.close();
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            mHandler.sendEmptyMessage(msg);
        }
    };

    /**
     * 下载apk
     */
    private void downloadApk() {

        downLoadThread = new Thread(mdownApkRunnable);
        downLoadThread.start();
    }


    /**
     * 安装apk
     */
    private void installApk() {
        File apkfile = new File(saveFileName);
        if (!apkfile.exists()) {
            return;
        }
        EventBus.getDefault().post("finish");
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setDataAndType(Uri.parse("file://" + apkfile.toString()), "application/vnd.android.package-archive");
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        mContext.getApplicationContext().startActivity(i);
    }


    public int convertByteToMb(int b) {
        int mbValue = b / 1024;
        return mbValue;
    }

    public void setOnUpdateDoneListener(OnUpdateDoneListener doneListener) {
        this.mUpdateListener = doneListener;
    }

    /**
     * APP升级信息反馈
     */
    private class PhoneUpgradeInfoResultListener implements IDataListener<PhoneUpgradeInfoPro.PhoneUpgradeInfoMsg> {

        @Override
        public void onReceiveData(PhoneUpgradeInfoPro.PhoneUpgradeInfoMsg data) {
            Message msg = mHandler.obtainMessage();
            msg.what = MESSAGE_CHECK_VERSION;
            msg.obj = data;
            mHandler.sendMessage(msg);
        }

        @Override
        public void onActiveChanged(boolean isActive) {
        }

    }


}
