/**
 * Copyright (C)  2016 深圳市狗尾草智能科技有限公司
 * GowildXB
 * GowildSettingActivity.java
 */
package com.gowild.mobileclient.activity.setting;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;

import com.gowild.mobile.libcommon.utils.SpUtils;
import com.gowild.mobile.libcommon.utils.ToastUtils;
import com.gowild.mobile.libp2sp.core.exception.NotConnectedException;
import com.gowild.mobile.libp2sp.core.listener.IDataListener;
import com.gowild.mobile.libp2sp.netty.NettyClientManager;
import com.gowild.mobileclient.R;
import com.gowild.mobileclient.activity.login.GowildLunchActivity;
import com.gowild.mobileclient.activity.base.GowildHasTitileActivity;
import com.gowild.mobileclient.activity.setting.update.UpdateManager;
import com.gowild.mobileclient.callback.OnNormalDialogListener;
import com.gowild.mobileclient.callback.OnUpdateDoneListener;
import com.gowild.mobileclient.config.Constants;
import com.gowild.mobileclient.config.KeysContainer;
import com.gowild.mobileclient.manager.GowildAsyncHttpResponseHandler;
import com.gowild.mobileclient.manager.GowildHttpManager;
import com.gowild.mobileclient.manager.GowildTCPManager;
import com.gowild.mobileclient.manager.LoginMananger;
import com.gowild.mobileclient.misc.GowildResponseCode;
import com.gowild.mobileclient.protocol.MachineBothMsgProto;
import com.gowild.mobileclient.vo.RequestRevokeToken;
import com.gowild.mobileclient.vo.RobotInfo;
import com.gowild.mobileclient.widget.dialog.GowildNormalDialog;
import com.gowild.mobileclient.widget.dialog.GowildRobotDataCleanDialog;
import com.loopj.android.http.RequestParams;

import org.greenrobot.eventbus.EventBus;

import java.io.File;

import butterknife.Bind;
import butterknife.OnClick;

/**
 * @author Zhangct
 * @version 1.0
 *          <p> 设置 </p>
 * @since 2016/8/6 9:21
 */
public class GowildSettingActivity extends GowildHasTitileActivity {

    // ===========================================================
    // HostConstants
    // ===========================================================

    private static final String TAG = GowildSettingActivity.class.getSimpleName();

    // ===========================================================
    // Static Fields
    // ===========================================================

    // ===========================================================
    // Fields
    // ===========================================================

    //个人信息
    @Bind(R.id.rl_owner_setting)
    RelativeLayout mGowildSettingOwner;
    //修改密码
    @Bind(R.id.rl_modify_pwd)
    RelativeLayout mGowildSettingModifyPwd;
    //机器人管理
    @Bind(R.id.rl_robot_manager)
    RelativeLayout mGowildSettingRobotManager;
    //网络设置
    @Bind(R.id.rl_network_setting)
    RelativeLayout mGowildSettingNetwork;
    //关于
    @Bind(R.id.rl_about)
    RelativeLayout mGowildSettingAbout;
    //清除数据
    @Bind(R.id.rl_clear_data)
    RelativeLayout mGowildSettingClearData;
    //检查更新
    @Bind(R.id.rl_check_update)
    RelativeLayout mGowildSettingCheckUpdate;
    //退出登录
    @Bind(R.id.btn_logout)
    Button mGowildSettingLogout;

    private RequesRevokeTokenCallback mRevokeTokenCallback = null;

    private GowildRobotDataCleanDialog mCleanDataDialog = null;
    private GowildNormalDialog mLogoutDialog = null;

    private UpdateManager mUpdateManager = null;
    private String sn;

    private Handler mHandler = new Handler() {

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case MSG_CODE_SUCCESS:
                    hideLoading();
                    //请重新登录
                    SpUtils.putString(GowildSettingActivity.this, Constants.ACCESS_TOKEN, null);
                    SpUtils.putString(GowildSettingActivity.this, Constants.REFRESH_TOKEN, null);
                    SpUtils.putString(GowildSettingActivity.this, Constants.ACCESS_TOKEN_NOBEAR, null);
                    Intent intent = new Intent(GowildSettingActivity.this,GowildLunchActivity.class);
                    intent.putExtra(Constants.BACK,Constants.BACK);
                    EventBus.getDefault().post("finish");
                    LoginMananger.getInstence().stopTCP();
                    startActivity(intent);
                    break;
                case MSG_CODE_FAIL:
                    hideLoading();
                    break;
            }
        }
    };

    // ===========================================================
    // Constructors
    // ===========================================================


    // ===========================================================
    // Getter or Setter
    // ===========================================================


    // ===========================================================
    // Methods for/from SuperClass/Interfaces
    // ===========================================================
    @Override
    protected int onSetContentView() {
        return R.layout.activity_gowild_setting;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        LoginMananger.getInstence().setIsSettingOpen(false);
    }

    @Override
    protected void initEvent() {
        LoginMananger.getInstence().setIsSettingOpen(true);
        setTitle(R.string.gowild_setting);
        mRevokeTokenCallback = new RequesRevokeTokenCallback(this);
        mUpdateManager = new UpdateManager(this);
        mUpdateManager.setOnUpdateDoneListener(new OnUpdateDoneListener() {

            @Override
            public void downDone() {
                hideLoading();
            }

            @Override
            public void exitApplication() {
                android.os.Process.killProcess(android.os.Process.myPid());
                System.exit(0);
            }

            @Override
            public void checkDone() {
                hideLoading();
            }

            @Override
            public void isNewVersion() {
                //已经是最新版本
                ToastUtils.showCustom(GowildSettingActivity.this, "您当前已经是最新版本了", false);
                hideLoading();
            }
            @Override
            public void install(String file) {
                // 安装apk
                Intent intent = new Intent("android.intent.action.VIEW");
                intent.addCategory("android.intent.category.DEFAULT");
                intent.setDataAndType(Uri.fromFile(new File(file)),
                                      "application/vnd.android.package-archive");
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivityForResult(intent, 0);
            }
        });
        try {
            GowildTCPManager.requestRobotRequestSync();
        } catch (NotConnectedException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 0) {
            if (resultCode == RESULT_CANCELED) {
                // 进入主界面
                if (mUpdateManager.isForceUpdate) {
                   EventBus.getDefault().post("finish");
                }
            }
        }
    }



    @OnClick({R.id.rl_owner_setting, R.id.rl_modify_pwd, R.id.rl_robot_manager, R.id.rl_network_setting,
              R.id.rl_about, R.id.rl_clear_data, R.id.rl_check_update, R.id.btn_logout})
    public void onclick(View view) {
        switch (view.getId()) {
            case R.id.rl_owner_setting:
                startActivity(GowildMyInfoActivity.class);
                break;
            case R.id.rl_modify_pwd:
                startActivity(GowildModifyPwdActivity.class);
                break;
            case R.id.rl_robot_manager:
                //判断当前是否已经绑定机器人，没有的话跳转到网络设置界面
                robotConfig();
                break;
            case R.id.rl_network_setting:
                netConfig();
                break;
            case R.id.rl_about:
                startActivity(GowildAboutActivity.class);
                break;
            case R.id.rl_clear_data:
                //检查是否已经绑定了账号
                if (null == mCleanDataDialog) {
                    mCleanDataDialog = new GowildRobotDataCleanDialog();
                }
                mCleanDataDialog.show(getFragmentManager(), "GowildRobotDataCleanDialog");
                break;
            case R.id.rl_check_update:
                showLoading(R.string.gowild_setting_check_update);
                mUpdateManager.checkUpdateInfo();
                break;
            case R.id.btn_logout:
                logout();
                break;
            default:
                break;
        }
    }


    // ===========================================================
    // Methods
    // ===========================================================
    private void logout() {
        if (null == mLogoutDialog) {
            mLogoutDialog = new GowildNormalDialog();
            mLogoutDialog.setDialogContent(getStringById(R.string.gowild_setting_logout_confirm));
        }
        mLogoutDialog.setDialogCancel(getString(R.string.gowild_cancel));
        mLogoutDialog.setDialogConfirm(getString(R.string.gowild_ok));
        mLogoutDialog.setDialogListener(new OnNormalDialogListener() {
            @Override
            public void onCancelListener() {
                //                Toast.makeText(getApplicationContext(), "取消", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onConfirmListener() {
                //退出账号,清除服务器token
                requestRevokeToken();
            }
        });
        mLogoutDialog.show(getFragmentManager(), "mLogoutDialog");
    }

    /**
     * 清除服务器登录状态
     */
    private void requestRevokeToken() {
        RequestRevokeToken requestRevokeToken = new RequestRevokeToken();
        requestRevokeToken.put(Constants.TOKEN, SpUtils.getString(this, Constants.ACCESS_TOKEN));
        requestRevokeToken.put(Constants.USER, SpUtils.getString(this, Constants.USERNAME));
        showLoading(R.string.loading);
        GowildHttpManager.requestRevokeToken(this, requestRevokeToken, mRevokeTokenCallback);
    }

    /**
     * 机器人设置
     */
    private void robotConfig() {
        showLoading(R.string.loading);
        LoginMananger.getInstence().setBindFrom(LoginMananger.bindFromRobot);
        GowildHttpManager.getRobotInfo(getBaseContext(), new RequestParams(), new GowildAsyncHttpResponseHandler<RobotInfo>(this) {

            @Override
            public void onSuccess(int statusCode, RobotInfo responseBody) {
                hideLoading();
                if (responseBody != null) {
                    if (responseBody.bindStatus == 0) {
                        goRobotBind(R.string.gowild_setting_robot_config);
                    } else {
                        Bundle bundle = new Bundle();
                        bundle.putString(KeysContainer.KEY_MAC, responseBody.mac);
                        bundle.putString(KeysContainer.KEY_ROBOT_VERSION, responseBody.robotVersion);
                        bundle.putString(KeysContainer.SN, responseBody.serialNo);
                        startActivity(GowildRobotConfigActivity.class, bundle);
                    }
                } else {
                    goRobotBind(R.string.gowild_setting_robot_config);
                }
            }

            @Override
            public void onFailure(int statusCode, String responseBody) {
                hideLoading();
            }
        });
    }

    private void goRobotBind(int titleId){
        Bundle bundle = new Bundle();
        bundle.putString(KeysContainer.KEY_ROBOT_BIND_TITLE, getString(titleId));
        startActivity(GowildRobotActivity.class, bundle);
    }


    /**
     * 网络配置
     */
    private void netConfig(){
        showLoading(R.string.loading);
        LoginMananger.getInstence().setBindFrom(LoginMananger.bindFromNet);
        GowildHttpManager.getRobotInfo(getBaseContext(), new RequestParams(), new GowildAsyncHttpResponseHandler<RobotInfo>(this) {

            @Override
            public void onSuccess(int statusCode, RobotInfo responseBody) {
                hideLoading();
                if (responseBody != null) {
                    if (responseBody.bindStatus == 0) {
                        goRobotBind(R.string.gowild_robot_net_config);
                    } else {
                        wifiName = responseBody.wifiName;
                        sn= responseBody.serialNo;
                        Bundle bundle = new Bundle();
                        bundle.putBoolean(KeysContainer.IS4G,is4G);
                        bundle.putString(KeysContainer.KEY_WIFI_NAME, wifiName);
                        startActivity(GowildChangeWifiActivity.class,bundle);
                    }
                } else {
                    goRobotBind(R.string.gowild_robot_net_config);
                }
            }

            @Override
            public void onFailure(int statusCode, String responseBody) {
                hideLoading();
                //                        ToastUtils.showCustom(getBaseContext(), R.string.gowild_network_error, false);
            }
        });
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        switch (LoginMananger.getInstence().getBindFrom()){
            case LoginMananger.bindFromRobot:
                robotConfig();
                break;
            case LoginMananger.bindFromNet:
                netConfig();
                break;
        }
        LoginMananger.getInstence().setBindFrom(LoginMananger.bindFromClear);
    }

    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================
    private class RequesRevokeTokenCallback extends GowildAsyncHttpResponseHandler<Object> {

        /**
         * 构造方法
         *
         * @param context 用于处理授权过期消息，会被转成ApplicationContext
         */
        public RequesRevokeTokenCallback(Context context) {
            super(context);
        }

        @Override
        public void onSuccess(int statusCode, Object responseBody) {
            SpUtils.putBoolean(GowildSettingActivity.this, Constants.REGISTER, false);
            Message msg = mHandler.obtainMessage();
            msg.what = MSG_CODE_SUCCESS;
            msg.obj = responseBody;
            mHandler.sendMessage(msg);
        }

        @Override
        public void onFailure(int statusCode, String responseBody) {
            Message msg = mHandler.obtainMessage();
            msg.what = MSG_CODE_FAIL;
            msg.obj = responseBody;
            mHandler.sendMessage(msg);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        NettyClientManager.getInstance().addHandlerListener(GowildResponseCode.ROBOT_INFO_SYNC, mMachineInfoListener);
    }

    @Override
    protected void onPause() {
        super.onPause();
        NettyClientManager.getInstance().removeHandlerListener(GowildResponseCode.ROBOT_INFO_SYNC, mMachineInfoListener);
    }

    private MachineInfoListener  mMachineInfoListener = new MachineInfoListener();

    private String wifiName;
    boolean  is4G ;
    private class MachineInfoListener implements IDataListener<MachineBothMsgProto.MachineInfoMsg> {

        @Override
        public void onReceiveData(final MachineBothMsgProto.MachineInfoMsg data) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    //处理是否是4G的机器
                    if (data.getHas4GCard()){
                        //开启4g模块
                        is4G = true;
                    }
                }
            });

        }

        @Override
        public void onActiveChanged(boolean isActive) {

        }
    }
}

