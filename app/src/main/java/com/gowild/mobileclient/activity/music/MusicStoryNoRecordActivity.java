/**
 * Copyright (C)  2016 深圳市狗尾草智能科技有限公司
 * GowildXB
 * MusicStoryNoRecordActivity.java
 */
package com.gowild.mobileclient.activity.music;

import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;

import com.gowild.mobileclient.R;
import com.gowild.mobileclient.base.baseactivity.GowildBaseActivity;
import com.gowild.mobileclient.config.IntContants;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * @author ljd
 * @version 1.0
 *          <p><strong>Features draft description.主要功能介绍</strong></p>
 * @since 2016/8/26 14:34
 */
public class MusicStoryNoRecordActivity extends GowildBaseActivity {

    // ===========================================================
    // HostConstants
    // ===========================================================

    private static final String TAG = MusicStoryNoRecordActivity.class.getSimpleName();
    @Bind(R.id.music_container)
    LinearLayout musicContainer;
    @Bind(R.id.story_container)
    LinearLayout storyContainer;


    @Override
    protected int onSetTopbarView() {
        return R.layout.activity_gowild_music_story_no_record_topbar;
    }

    @Override
    protected int onSetContentView() {
        return R.layout.activity_gowild_music_story_no_record;
    }

    @Override
    protected int onSetTopBarHeight() {
        return getResources().getDimensionPixelOffset(R.dimen.gowild_160px);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.back)
    public void onClick() {
        onBackPressed();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (IntContants.isCurrentMusic()) {
            storyContainer.setVisibility(View.GONE);
            musicContainer.setVisibility(View.VISIBLE);
        } else {
            musicContainer.setVisibility(View.GONE);
            storyContainer.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onBackPressed() {
        finish();
    }
    // ===========================================================
    // Static Fields
    // ===========================================================

    // ===========================================================
    // Fields
    // ===========================================================


    // ===========================================================
    // Constructors
    // ===========================================================


    // ===========================================================
    // Getter or Setter
    // ===========================================================


    // ===========================================================
    // Methods for/from SuperClass/Interfaces
    // ===========================================================


    // ===========================================================
    // Methods
    // ===========================================================


    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================

}
