package com.gowild.mobileclient.activity;

import android.os.Handler;
import android.os.Message;
import android.view.KeyEvent;
import android.webkit.JavascriptInterface;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.gowild.mobile.libp2sp.HostConstants;
import com.gowild.mobileclient.R;
import com.gowild.mobileclient.activity.base.GowildHasTitileActivity;

import butterknife.Bind;


/**
 * @author zhangct
 * @version 1.0
 * @since 2016/7/22 18:29
 * <p><strong> declare </strong></p>
 */
public class GowildWebActivity extends GowildHasTitileActivity {

    String mUrl = HostConstants.HOST_HTML;

    String mSn = "";
    String mPhone = "";
    String mToken = "";
    public static final String WEB_URL = "WEB_URL";
    public static final String WEB_SN = "WEB_SN";
    public static final String WEB_PHONE = "WEB_PHONE";
    public static final String WEB_TOKEN = "WEB_TOKEN";

    @Bind(R.id.content_wv)
    WebView mGowildContentWeb;


    @Override
    protected int onSetTopBarHeight() {
        return getResources().getDimensionPixelSize(R.dimen.gowild_1px);
    }

    @Override
    protected int onSetContentView() {
        return R.layout.activity_gowild_web;
    }

    @Override
    protected void initEvent() {
        initExtra();
        showLoading(R.string.loading);
        WebSettings settings = mGowildContentWeb.getSettings();
        settings.setJavaScriptEnabled(true);
        settings.setCacheMode(WebSettings.LOAD_NO_CACHE);
        mGowildContentWeb.setWebChromeClient(new WebChromeClient());
        mGowildContentWeb.addJavascriptInterface(new JsInteration(), "xbNative");
        mGowildContentWeb.loadUrl(mUrl);
        mGowildContentWeb.setWebViewClient(new WebViewClient() {

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                //隐藏加载框
                super.onPageFinished(view, url);
                hideLoading();
            }
        });
    }

    public void initExtra() {
        mUrl = getIntent().getStringExtra(WEB_URL);
        mSn = getIntent().getStringExtra(WEB_SN);
        mPhone = getIntent().getStringExtra(WEB_PHONE);
        mToken = getIntent().getStringExtra(WEB_TOKEN);
    }

    public class JsInteration {

        @JavascriptInterface
        public void goBack() {
            mHandler.sendEmptyMessage(0);
        }

        @JavascriptInterface
        public String getSn() {
            return mSn;
        }

        @JavascriptInterface
        public String getPhone() {
            return mPhone;
        }

        @JavascriptInterface
        public String getAccessToken() {
            return mToken.toLowerCase();
        }
    }

    private Handler mHandler = new Handler() {
        public void handleMessage(Message msg) {
            back();
        }
    };

    private void back() {
            finish();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK
                && event.getRepeatCount() == 0) {
            mGowildContentWeb.loadUrl("javascript:onBackPress()");

            if (mGowildContentWeb.canGoBack()) {
//                mGowildContentWeb.goBack();
            } else {
                finish();
            }
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }
}
