/**
 * Copyright (C)  2016 深圳市狗尾草智能科技有限公司
 * GowildXB
 * MusicSearchActivity.java
 */
package com.gowild.mobileclient.activity.music;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.gowild.mobile.libcommon.utils.ToastUtils;
import com.gowild.mobileclient.R;
import com.gowild.mobileclient.base.baseactivity.GowildBaseActivity;
import com.gowild.mobileclient.config.Constants;
import com.gowild.mobileclient.config.MediaConstants;
import com.gowild.mobileclient.manager.GowildAsyncHttpResponseHandler;
import com.gowild.mobileclient.manager.GowildHttpManager;
import com.gowild.mobileclient.vo.ResponseStoryChapter;
import com.gowild.mobileclient.vo.StoryChapter;
import com.gowild.mobileclient.widget.PullToRefreshLayout;
import com.loopj.android.http.RequestParams;

import java.util.ArrayList;


/**
 * @author JunDongWang
 * @version 1.0
 *          <p><strong>搜索故事章节类别</strong></p>
 * @since 2016/7/27 11:24
 */
public class StorySearchResultActivity extends GowildBaseActivity {

    // ===========================================================
    // HostConstants
    // ===========================================================

    private static final String TAG = StorySearchResultActivity.class.getSimpleName();
    private OnRefreshCallback mOnRefreshCallback = new OnRefreshCallback();
    private PullToRefreshLayout mPrltRefresh;
    private ListView mListView;
    private TextView mTvTitle;
    private int mPageNum = 1;
    private int mPageSize = 10;
    private View mBack;
    private boolean hasNext = true;
    private boolean hasPrevious = true;
    private int key;
    private StoryChapterAdapter mStoryChapterAdapter = new StoryChapterAdapter();
    private OnItemClickCallback mOnItemClickCallback = new OnItemClickCallback();
    private OnRequestStorySearchResultCallback mOnRequestStorySearchResultCallback;
    private OnClickCallBack mOnClickCallBack = new OnClickCallBack();
    private ArrayList<StoryChapter> mStoryChapterList = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initView();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mListView.setAdapter(mStoryChapterAdapter);
        mListView.setOnItemClickListener(mOnItemClickCallback);
        mOnRequestStorySearchResultCallback = new OnRequestStorySearchResultCallback(this);
        mPrltRefresh.setOnRefreshListener(mOnRefreshCallback);
        if (getIntent().getStringExtra("NAME") == null) {
            mTvTitle.setText("查找故事");
        } else {
            mTvTitle.setText(getIntent().getStringExtra("NAME"));
        }
        key = getIntent().getIntExtra(Constants.INFOS, 0);
        RequestParams requestParams = new RequestParams(Constants.ALBUM_ID, key, Constants.PAGE_NUM, mPageNum, Constants.PAGESIZE, mPageSize);
        showLoading(R.string.loading);
        GowildHttpManager.requestStoryChapterFromStoryAlbumID(this, requestParams, mOnRequestStorySearchResultCallback);
    }

    @Override
    protected int onSetTopbarView() {
        return R.layout.activity_gowild_find_music_by_sort_topbar;
    }

    @Override
    protected int onSetContentView() {
        return R.layout.activity_gowild_story_chapter;
    }

    @Override
    protected int onSetTopBarHeight() {
        return getResources().getDimensionPixelOffset(R.dimen.gowild_160px);
    }

    private void initView() {
        mPrltRefresh = (PullToRefreshLayout) findViewById(R.id.refresh_view);
        mTvTitle = (TextView) findViewById(R.id.title);
        mListView = (ListView) findViewById(R.id.list);
        mBack = findViewById(R.id.back);
        mBack.setOnClickListener(mOnClickCallBack);
    }


    private class StoryChapterAdapter extends BaseAdapter {
        private ArrayList<StoryChapter> mStoryChapter = new ArrayList<>();


        public void updateStoryChapterList(ArrayList<StoryChapter> storySorts) {
            mStoryChapter.clear();
            mStoryChapter.addAll(storySorts);
            notifyDataSetChanged();
        }

        @Override
        public int getCount() {
            return mStoryChapter.size();
        }

        @Override
        public StoryChapter getItem(int position) {
            return mStoryChapter.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = LayoutInflater.from(getBaseContext()).inflate(R.layout.item_gowild_singer_sort_list, parent, false);
            }
            StoryChapter storyChapter = getItem(position);
            TextView sort = (TextView) convertView.findViewById(R.id.sort);
            sort.setText(storyChapter.title);
            return convertView;
        }
    }

    private class OnItemClickCallback implements AdapterView.OnItemClickListener {

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

            StoryChapter chapter = mStoryChapterAdapter.getItem(position);
            showLoading(R.string.loading);
            GowildHttpManager.changeStory(getBaseContext(), new RequestParams(Constants.STORY_ID, chapter.id, Constants.PLAY_STATUS, Constants.PLAY_STATUS_PLAY, Constants.PLAY_TYPE, MediaConstants.COLLECT_NO), new GowildAsyncHttpResponseHandler(getBaseContext()) {
                @Override
                public void onSuccess(int statusCode, Object responseBody) {
                    hideLoading();
                    Intent intent = new Intent(getBaseContext(), MusicMainActivity.class);
                    startActivity(intent);
                }

                @Override
                public void onFailure(int statusCode, String responseBody) {
                    hideLoading();
//                    ToastUtils.showCustom(getBaseContext(), R.string.gowild_network_error, false);
                }
            });
        }
    }


    private class OnRequestStorySearchResultCallback extends GowildAsyncHttpResponseHandler<ResponseStoryChapter> {

        public OnRequestStorySearchResultCallback(Context context) {
            super(context);
        }

        @Override
        public void onSuccess(int statusCode, ResponseStoryChapter responseBody) {
            hideLoading();
            mPrltRefresh.loadmoreFinish(PullToRefreshLayout.SUCCEED);
            mPageNum = responseBody.pageNum;
            hasNext = responseBody.hasNextPage;
            hasPrevious = responseBody.hasPreviousPage;
            if (mPageNum == 1) {
                mStoryChapterList.clear();
                mStoryChapterList.addAll(responseBody.storyChapters);
            } else {
                mStoryChapterList.addAll(responseBody.storyChapters);
            }
            mStoryChapterAdapter.updateStoryChapterList(mStoryChapterList);
            if (mStoryChapterList.size() == 0) {
                ToastUtils.showCustom(getBaseContext(), R.string.gowild_no_more, false);
            }

        }

        @Override
        public void onFailure(int statusCode, String responseBody) {
            mPrltRefresh.loadmoreFinish(PullToRefreshLayout.SUCCEED);
            hideLoading();
//            ToastUtils.showCustom(getBaseContext(), R.string.gowild_network_error, false);
        }
    }

    private class OnClickCallBack implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.back:
                    finish();
                    break;
            }
        }
    }

    private class OnRefreshCallback implements PullToRefreshLayout.OnRefreshListener {

        @Override
        public void onRefresh(PullToRefreshLayout pullToRefreshLayout) {
            mPrltRefresh.loadmoreFinish(PullToRefreshLayout.SUCCEED);
        }

        @Override
        public void onLoadMore(PullToRefreshLayout pullToRefreshLayout) {
            if (hasNext) {
                mPageNum = mPageNum + 1;
                RequestParams requestParams = new RequestParams(Constants.ALBUM_ID, key, Constants.PAGE_NUM, mPageNum, Constants.PAGESIZE, mPageSize);
                showLoading(R.string.loading);
                GowildHttpManager.requestStoryChapterFromStoryAlbumID(getBaseContext(), requestParams, mOnRequestStorySearchResultCallback);
            } else {
                mPrltRefresh.loadmoreFinish(PullToRefreshLayout.FAIL);
                ToastUtils.showCustom(getBaseContext(), R.string.gowild_no_more, false);
            }
        }
    }
}
