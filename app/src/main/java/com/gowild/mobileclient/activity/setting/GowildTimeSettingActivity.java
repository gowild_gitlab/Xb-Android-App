package com.gowild.mobileclient.activity.setting;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.ViewGroup;

import com.gowild.mobileclient.R;
import com.gowild.mobileclient.activity.base.GowildLoginAndRegisterActivity;
import com.gowild.mobileclient.config.Constants;
import com.gowild.mobileclient.event.RefreshTimeEvent;
import com.gowild.mobileclient.event.TimeEvent;
import com.gowild.mobileclient.fragment.TimeFragment;
import com.gowild.mobileclient.manager.GowildAsyncHttpResponseHandler;
import com.gowild.mobileclient.manager.GowildHttpManager;
import com.gowild.mobileclient.widget.MyToolBar;
import com.gowild.mobileclient.widget.PagerSlidingTabStrip;
import com.loopj.android.http.RequestParams;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import butterknife.Bind;

public class GowildTimeSettingActivity extends GowildLoginAndRegisterActivity {
    @Bind(R.id.gowild_time_pt)
    public PagerSlidingTabStrip trip;

    @Bind(R.id.gowild_time_vp)
    ViewPager mPager;


    String[] titles = new String[]{"", ""};

    @Override
    public int getLayout() {
        return R.layout.activity_net_setting;
    }

    @Override
    protected void initView() {
        MyToolBar toolbar = (MyToolBar) findViewById(R.id.view_toolbar);
        toolbar.setTitle("设置时间");
        toolbar.setActionStr("保存");
        toolbar.setActionListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String start=  trip.getTilte(0);
                String end =  trip.getTilte(1);
                //// TODO: 2016/10/13  网络请求
                showLoading("加载中");
                RequestParams params = new RequestParams();
                params.put("isSilence",1);
                params.put("silenceStart",start.replace("从","").replace("至",""));
                params.put("silenceEnd",end.replace("至","").replace("从",""));
                //                isSilence
                //                int	默认 1 （0关闭免打扰 1开启免打扰）	0/1
                //                silenceStart
                //                String	免打扰开始时间，默认23:00（格式 HH:mm）	格式 HH:mm
                //                        silenceEnd
                //                String	免打扰开始时间，默认08:00（格式 HH:mm）	格式 HH:mm

                GowildHttpManager.requestUpdateTime(GowildTimeSettingActivity.this, params, new GowildAsyncHttpResponseHandler<String>(GowildTimeSettingActivity.this) {

                    @Override
                    public void onSuccess(int statusCode, String responseBody) {
                        finish();
                        hideLoading();
                        EventBus.getDefault().post(new RefreshTimeEvent());
                    }

                    @Override
                    public void onFailure(int statusCode, String responseBody) {
                        hideLoading();
                    }
                });
            }
        });
        titles[0] = "从" + getIntent().getStringExtra(Constants.STARTTIME);
        titles[1] = "至" + getIntent().getStringExtra(Constants.ENDTIME);
    }

    @Override
    protected void initData() {
    }

    @Override
    protected void initEvent() {
        mPager.setAdapter(new TimeAdatpter(getSupportFragmentManager()));
        trip.setViewPager(mPager);
    }

    private class TimeAdatpter extends FragmentPagerAdapter {

        public TimeAdatpter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View) object);
        }

        @Override
        public Fragment getItem(int position) {
            TimeFragment fragment = new TimeFragment();
            if (position == 0) {
                fragment.setTime(titles[0], 0);
            } else {
                fragment.setTime(titles[1], 1);
            }
            return fragment;
        }

        @Override
        public int getCount() {
            return titles.length;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return titles[position];
        }
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void suTimeEvent(TimeEvent event) {
        if (event.index == 0) {
            trip.setTabStr("从" + event.hour + ":" + event.minutes, event.index);
        } else {
            trip.setTabStr("至" + event.hour + ":" + event.minutes, event.index);
        }
    }

}
