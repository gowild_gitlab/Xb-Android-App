/**
 * Copyright (C)  2016 深圳市狗尾草智能科技有限公司
 * GowildXB
 * NetResourceListBaseActivity.java
 */
package com.gowild.mobileclient.activity.music;

import android.os.Bundle;
import android.view.View;

import com.gowild.mobileclient.R;
import com.gowild.mobileclient.base.baseactivity.GowildBaseActivity;
import com.gowild.mobileclient.manager.GowildHttpManager;
import com.gowild.mobileclient.model.BaseNetResourceInfo;
import com.gowild.mobileclient.widget.MyToolBar;
import com.gowild.mobileclient.widget.NetResourcerecyclerView;
import com.gowild.mobileclient.widget.PullToRefreshLayout;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * @author temp
 * @version 1.0
 *          <p><strong>Features draft description.主要功能介绍</strong></p>
 * @since 2016/8/2 21:37
 */
public abstract class NetResourceListBaseActivity extends GowildBaseActivity implements PullToRefreshLayout.OnRefreshListener {

    // ===========================================================
    // HostConstants
    // ===========================================================

    private static final String TAG = NetResourceListBaseActivity.class.getSimpleName();

    @Bind(R.id.recycler)
    public NetResourcerecyclerView mRecyclerView;
    @Bind(R.id.refresh_view)
    public PullToRefreshLayout mPullLayout;
    private int mPageNum = GowildHttpManager.DEFAULT_PAGE_NUM;
    public ArrayList<BaseNetResourceInfo> mInfos = new ArrayList<>();
    private boolean bLoadMore = false;

    /**
     * 之类网络请求相关数据初始化应该在调用该onCreate之前
     *
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        initToolbarTitle();
        addTipView(getTipView());
        mPullLayout.setOnRefreshListener(this);
        initInfoBeforeLoad();
        showLoading(R.string.loading);
        loadInfo(GowildHttpManager.DEFAULT_PAGE_NUM);
    }

    public void initInfoBeforeLoad() {

    }

    public void initToolbarTitle() {
        MyToolBar toolBar = (MyToolBar) findViewById(R.id.view_toolbar);
        toolBar.setTitle(getToobarTitle());
    }

    public String getToobarTitle() {
        return "";
    }

    public void setInfos(ArrayList<BaseNetResourceInfo> infos) {
        mRecyclerView.setInfos(infos);
    }

    public void addTipView(View view) {
        if (view != null) {
            getRootView().addView(view, 1);
        }
    }

    public View getTipView() {
        return null;
    }

    @Override
    protected int onSetTopbarView() {
        return R.layout.view_gowild_toolbar;
    }

    @Override
    protected int onSetContentView() {
        return R.layout.layout_gowild_net_resource_pull_recyclerview;
    }

    @Override
    protected int onSetTopBarHeight() {
        return getResources().getDimensionPixelSize(R.dimen.gowild_toolbar_height);
    }

    @Override
    public void onRefresh(PullToRefreshLayout pullToRefreshLayout) {
        bLoadMore = false;
        loadInfo(GowildHttpManager.DEFAULT_PAGE_NUM);
    }

    @Override
    public void onLoadMore(PullToRefreshLayout pullToRefreshLayout) {
        bLoadMore = true;
        loadInfo(mPageNum);
    }

    /**
     * 请注意，请求页数为1时，需要清除数据，初始化pageNum;
     *
     * @param pageNum 当前分页的页数
     */
    public abstract void loadInfo(int pageNum);

    public void onLoadSuccess(ArrayList<? extends BaseNetResourceInfo> infos) {
        if (isActivityFinished()) {
            return;
        }
        if (!bLoadMore) {
            mPageNum = 1;
            mInfos.clear();
        }
        if (infos != null && infos.size() > 0) {
            mPageNum++;
        }
        mInfos.addAll(infos);
        setInfos(mInfos);
        loadDone(PullToRefreshLayout.SUCCEED);
    }

    public void onLoadFailure() {
        if (isActivityFinished()) {
            return;
        }
        loadDone(PullToRefreshLayout.FAIL);
    }

    private void loadDone(int status) {
        hideLoading();
        if (bLoadMore) {
            mPullLayout.loadmoreFinish(status);
        } else {
            mPullLayout.refreshFinish(status);
        }
    }
}
