/**
 * Copyright (C)  2016 深圳市狗尾草智能科技有限公司
 * GowildXB
 * GowildApplianceOptionActivity.java
 */
package com.gowild.mobileclient.activity.appliances;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.gowild.mobile.libcommon.utils.DimenUtil;
import com.gowild.mobile.libcommon.utils.ToastUtils;
import com.gowild.mobileclient.R;
import com.gowild.mobileclient.activity.base.GowildHasTitileActivity;
import com.gowild.mobileclient.adapter.DeviceAdapter;
import com.gowild.mobileclient.model.ApplianceItem;
import com.gowild.mobileclient.model.ApplianceModel;
import com.gowild.mobileclient.widget.LineGridView;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;

/**
 * @author Administrator
 * @version 1.0
 *          <p><strong>选择设备</strong></p>
 * @since 2016/8/3 11:00
 */
public class GowildApplianceOptionActivity extends GowildHasTitileActivity {

    // ===========================================================
    // HostConstants
    // ===========================================================

    private static final String TAG = GowildApplianceOptionActivity.class.getSimpleName();

    @Bind(R.id.gowild_appliance_gv)
    LineGridView mGowildApplianceGv;

    private DeviceAdapter mAdapter;
    private Dialog mDialog = null;
    private List<ApplianceItem> mApplianceList = new ArrayList<ApplianceItem>();
    private List<ApplianceModel> m_current = new ArrayList<ApplianceModel>();
    public static final String INTENT_EXTRA_ADDED = "INTENT_EXTRA_ADDED";

    private int img[] = {R.drawable.appliances_air, R.drawable.appliances_tv,
            R.drawable.appliances_stb, R.drawable.appliances_pjt,
            R.drawable.appliances_fans, R.drawable.appliances_dvd};


    private AdapterView.OnItemClickListener mItemClickListener = new AdapterView.OnItemClickListener() {

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            switch (position) {
                case 0:
                    DEVICE_TYPE_CURRENT = DEVICE_TYPE_AIR;
                    SceneDialog();
                    break;
                case 1:
                case 2:
                case 3:
                case 4:
                case 5:
                    ToastUtils.showCustom(GowildApplianceOptionActivity.this, "暂时还不支持此设备。", false);
                    break;
                default:

                    break;
            }

        }
    };

    private View.OnClickListener mSceneClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.tv_scene_living://客厅
                    SCENE_CURRENT = SCENE_PARLOUR;
                    break;
                case R.id.tv_scene_main://主卧
                    SCENE_CURRENT = SCENE_MASTER_BEDROOM;
                    break;
                case R.id.tv_scene_guest://客卧
                    SCENE_CURRENT = SCENE_GUEST_BEDROOM;
                    break;
                case R.id.tv_scene_book://书房
                    SCENE_CURRENT = SCENE_BOOK_ROOM;
                    break;
                case R.id.tv_scene_other://其他
                    SCENE_CURRENT = SCENE_OTHER;
                    break;
                default:
                    SCENE_CURRENT = -1;
                    break;
            }

            if (SCENE_CURRENT != -1) {
                mDialog.dismiss();
                Bundle bundle = new Bundle();
                bundle.putInt(GowildApplianceSearchActivity.INTENT_EXTRA_SCENE, SCENE_CURRENT);
                bundle.putInt(GowildApplianceSearchActivity.INTENT_EXTRA_DEVICE_TYPE, DEVICE_TYPE_CURRENT);
                startActivity(GowildApplianceBrandActivity.class, bundle);
            }
        }
    };


    // ===========================================================
    // Static Fields
    // ===========================================================
    //当前场景
    private int SCENE_CURRENT = -1;

    private static final int SCENE_PARLOUR = 0; //客厅
    private static final int SCENE_MASTER_BEDROOM = 1; //主卧
    private static final int SCENE_GUEST_BEDROOM = 2; //客卧
    private static final int SCENE_BOOK_ROOM= 3;  //书房
    private static final int SCENE_OTHER= 4;  //其他
    

    //当前电器
    private int DEVICE_TYPE_CURRENT = -1;
    //空调
    private final int DEVICE_TYPE_AIR = 0;

    // ===========================================================
    // Fields
    // ===========================================================


    // ===========================================================
    // Constructors
    // ===========================================================


    // ===========================================================
    // Getter or Setter
    // ===========================================================


    // ===========================================================
    // Methods for/from SuperClass/Interfaces
    // ===========================================================

    @Override
    protected int onSetContentView() {
        return R.layout.activity_gowild_appliance_option;
    }

    @Override
    protected void initEvent() {
        setTitle("选择设备");
        mAdapter = new DeviceAdapter(GowildApplianceOptionActivity.this, mApplianceList);
        mGowildApplianceGv.setAdapter(mAdapter);
        mGowildApplianceGv.setOnItemClickListener(mItemClickListener);
    }

    @Override
    protected void loadData() {
        super.loadData();
        m_current = (List<ApplianceModel>) getIntent().getSerializableExtra( INTENT_EXTRA_ADDED);
        mApplianceList.add(new ApplianceItem(1, img[0]));
        mApplianceList.add(new ApplianceItem(2, img[1]));
        mApplianceList.add(new ApplianceItem(3, img[2]));
        mApplianceList.add(new ApplianceItem(4, img[3]));
        mApplianceList.add(new ApplianceItem(5, img[4]));
        mApplianceList.add(new ApplianceItem(6, img[5]));

        mAdapter.notifyDataSetChanged();
    }

    // ===========================================================
    // Methods
    // ===========================================================

    /**
     * 显示设备场景对话框
     */
    private void SceneDialog() {
        mDialog = new Dialog(this, R.style.dialog);
        LayoutInflater mInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = mInflater.inflate(R.layout.view_gowild_appliances_dialog, null);
        RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT,
                RelativeLayout.LayoutParams.WRAP_CONTENT);
        lp.width = DimenUtil.getWindowWidth(getApplicationContext());
        //置灰
        for (int i = 0; i < m_current.size(); i++) {
            mlist(view, m_current.get(i).getSceneID());
        }
        // for (int i = 0; i < 5; i++) {
        // ((Button) view.findViewById(button_id[i])).setOnClickListener(this);
        // }
        ViewGroup vv = (ViewGroup) view.findViewById(R.id.d_scene);
        startAnimation(vv);
        mDialog.getWindow().setGravity(Gravity.BOTTOM);
        mDialog.setContentView(view, lp);
        mDialog.setCancelable(true);
        mDialog.show();
    }

    // 遍历显示家电场景 已添加按钮无效 置灰
    public void mlist(View view, int item) {
        TextView but = null;
        switch (item) {
            case 0:
                but = (TextView) view.findViewById(R.id.tv_scene_living);
                break;
            case 1:
                but = (TextView) view.findViewById(R.id.tv_scene_main);
                break;
            case 2:
                but = (TextView) view.findViewById(R.id.tv_scene_guest);
                break;
            case 3:
                but = (TextView) view.findViewById(R.id.tv_scene_book);
                break;
            case 4:
                but = (TextView) view.findViewById(R.id.tv_scene_other);
                break;
            default:
                return;
        }
        but.setEnabled(false);
        // but.setTextSize(11);
        but.setText("已添加");
    }

    public void startAnimation(ViewGroup sceneView) {
        int entY = 600;
        int duration = 1600;
        for (int i = 0; i < sceneView.getChildCount(); i++) {
            entY = entY - 100;
            duration = duration - 100;
            // ObjectAnimator obj = ObjectAnimator.ofFloat(vv.getChildAt(i),
            // "translationX",0,0);
            ObjectAnimator obj = ObjectAnimator.ofFloat(
                    sceneView.getChildAt(i), "translationY", 1280, 0);
            obj.setDuration(duration);
            // obj.start();
            ObjectAnimator obj1 = ObjectAnimator.ofFloat(
                    sceneView.getChildAt(i), "alpha", 0.0f, 1.0f);
            obj1.setDuration(duration + 200);
            // obj1.start();
            //
            AnimatorSet anim = new AnimatorSet();
            anim.playTogether(obj, obj1);
            // anim.setDuration(duration);
            //      anim.start();
            if (sceneView.getChildAt(i) instanceof TextView) {
                sceneView.getChildAt(i).setOnClickListener(mSceneClickListener);
//                if(sceneView.getChildAt(i).getId() == R.id.tv_scene_title) {
//                    sceneView.getChildAt(i).setEnabled(false);
//                }
            }
        }
    }


    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================

}
