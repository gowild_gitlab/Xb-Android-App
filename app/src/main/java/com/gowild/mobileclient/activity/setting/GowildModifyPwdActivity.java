/**
 * Copyright (C)  2016 深圳市狗尾草智能科技有限公司
 * GowildXB
 * GowildModifyPwdActivity.java
 */
package com.gowild.mobileclient.activity.setting;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.method.DigitsKeyListener;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.gowild.mobile.libcommon.utils.SpUtils;
import com.gowild.mobile.libcommon.utils.ToastUtils;
import com.gowild.mobileclient.R;
import com.gowild.mobileclient.activity.login.GowildLunchActivity;
import com.gowild.mobileclient.activity.base.GowildHasTitileActivity;
import com.gowild.mobileclient.config.Constants;
import com.gowild.mobileclient.manager.GowildAsyncHttpResponseHandler;
import com.gowild.mobileclient.manager.GowildHttpManager;
import com.gowild.mobileclient.manager.LoginMananger;
import com.gowild.mobileclient.vo.ModifyPwd;
import com.gowild.mobileclient.vo.RequestModifyPwd;

import org.greenrobot.eventbus.EventBus;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * @author Zhangct
 * @version 1.0
 *          <p> 主要功能介绍 </p>
 * @since 2016/8/6 15:18
 */
public class GowildModifyPwdActivity extends GowildHasTitileActivity {

    // ===========================================================
    // HostConstants
    // ===========================================================

    private static final String TAG = GowildModifyPwdActivity.class.getSimpleName();

    // ===========================================================
    // Static Fields
    // ===========================================================

    // ===========================================================
    // Fields
    // ===========================================================
    //原始密码
    @Bind(R.id.edt_modify_pwd_old)
    EditText mGowildModifyPwdOldEdt;
    //新密码
    @Bind(R.id.edt_modify_pwd_new)
    EditText mGowildModifyPwdNewEdt;
    //确认新密码
    @Bind(R.id.edt_modify_pwd_new_confirm)
    EditText mGowildModifyPwdConfirmEdt;
    //原始密码
    @Bind(R.id.btn_modify_pwd)
    Button mGowildModifyPwdCommitBtn;
    @Bind(R.id.gowild_lunch_vi)
    ImageView mGowildOldPwdIv;
    @Bind(R.id.gowild_lunch_vi_tw)
    ImageView mGowildConfirmPwdIv;
    @Bind(R.id.gowild_pwd_old_iv)
    ImageView mGowildNewPwdIv;

    private RequesModifyPwdCallback mModifyPwdCallback = null;

    private Handler mHandler = new Handler() {

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case MSG_CODE_SUCCESS:
                    hideLoading();
                    //请重新登录
                    SpUtils.putString(GowildModifyPwdActivity.this, Constants.ACCESS_TOKEN, null);
                    SpUtils.putString(GowildModifyPwdActivity.this, Constants.REFRESH_TOKEN, null);
                    SpUtils.putString(GowildModifyPwdActivity.this, Constants.ACCESS_TOKEN_NOBEAR, null);
                    ToastUtils.showCustom(GowildModifyPwdActivity.this,R.string.gowild_setting_pwd_modify_success , false);
                    Intent intent = new Intent(GowildModifyPwdActivity.this,GowildLunchActivity.class);
                    intent.putExtra(Constants.BACK,Constants.BACK);
                    EventBus.getDefault().post("finish");
                    LoginMananger.getInstence().stopTCP();
                    startActivity(intent);
                    break;
                case MSG_CODE_FAIL:
//                    String errMsg = (String) msg.obj;
//                    ToastUtils.showCustom(GowildModifyPwdActivity.this, errMsg, false);
                    mGowildModifyPwdNewEdt.setText("");
                    mGowildModifyPwdOldEdt.setText("");
                    mGowildModifyPwdConfirmEdt.setText("");
                    mGowildModifyPwdOldEdt.requestFocus();

                    hideLoading();
                    break;
            }
        }
    };
    private boolean visible = false;
    private boolean visibleTw = false;
    private boolean visibleOldPwd = true;
    // ===========================================================
    // Constructors
    // ===========================================================


    // ===========================================================
    // Getter or Setter
    // ===========================================================


    // ===========================================================
    // Methods for/from SuperClass/Interfaces
    // ===========================================================
    @Override
    protected int onSetContentView() {
        return R.layout.activity_gowild_setting_modify_pwd;
    }

    @Override
    protected void initEvent() {
        String digists = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ!@#$%^&*.~///{//}|()'/\"?><,.`//+-=_//[//]:;]+";
        mGowildModifyPwdNewEdt.setKeyListener(DigitsKeyListener.getInstance(digists));
        mGowildModifyPwdConfirmEdt.setKeyListener(DigitsKeyListener.getInstance(digists));
        mGowildModifyPwdOldEdt.setKeyListener(DigitsKeyListener.getInstance(digists));

        mGowildConfirmPwdIv.setVisibility(View.GONE);
        mGowildOldPwdIv.setVisibility(View.GONE);
        mGowildNewPwdIv.setVisibility(View.GONE);
        setTitle(R.string.gowild_setting_modify_pwd);
        mModifyPwdCallback = new RequesModifyPwdCallback(GowildModifyPwdActivity.this);


        mGowildModifyPwdOldEdt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() == 0) {
                    mGowildNewPwdIv.setVisibility(View.GONE);
                } else {
                    mGowildNewPwdIv.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


        mGowildModifyPwdNewEdt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() == 0) {
                    mGowildOldPwdIv.setVisibility(View.GONE);
                } else {
                    mGowildOldPwdIv.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


        mGowildModifyPwdConfirmEdt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() == 0) {
                    mGowildConfirmPwdIv.setVisibility(View.GONE);
                } else {
                    mGowildConfirmPwdIv.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

    }

    @OnClick({R.id.btn_modify_pwd,R.id.gowild_lunch_vi,R.id.gowild_lunch_vi_tw,R.id.gowild_pwd_old_iv})
    public void onclick(View view) {
        switch (view.getId()) {
            case R.id.btn_modify_pwd:
                requestModify();
                break;
            case R.id.gowild_lunch_vi:
                if (visible) {
                    mGowildModifyPwdNewEdt.setTransformationMethod(PasswordTransformationMethod.getInstance());
                } else {
                    mGowildModifyPwdNewEdt.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                }
                mGowildModifyPwdNewEdt.setSelection(mGowildModifyPwdNewEdt.getText().length());
                visible = !visible;
                break;
            case R.id.gowild_lunch_vi_tw:
                if (visibleTw) {
                    mGowildModifyPwdConfirmEdt.setTransformationMethod(PasswordTransformationMethod.getInstance());
                } else {
                    mGowildModifyPwdConfirmEdt.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                }
                mGowildModifyPwdConfirmEdt.setSelection(mGowildModifyPwdConfirmEdt.getText().length());
                visibleTw = !visibleTw;
                break;
            case R.id.gowild_pwd_old_iv:
                if (visibleOldPwd) {
                    mGowildModifyPwdOldEdt.setTransformationMethod(PasswordTransformationMethod.getInstance());
                } else {
                    mGowildModifyPwdOldEdt.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                }
                mGowildModifyPwdOldEdt.setSelection(mGowildModifyPwdOldEdt.getText().length());
                visibleOldPwd = !visibleOldPwd;
                break;
            default:
                break;
        }
    }

    // ===========================================================
    // Methods
    // ===========================================================

    /**
     * 确认修改密码
     */
    private void requestModify() {
        String oldPwdStr = mGowildModifyPwdOldEdt.getText().toString().trim();
        String newPwdStr = mGowildModifyPwdNewEdt.getText().toString().trim();
        String confirmPwdStr = mGowildModifyPwdConfirmEdt.getText().toString().trim();
        if (TextUtils.isEmpty(oldPwdStr) || TextUtils.isEmpty(newPwdStr) || TextUtils.isEmpty(confirmPwdStr)) {
            ToastUtils.showCustom(this, R.string.gowild_setting_pwd_null, false);
            return;
        }
        //新旧密码不能一样
        if (oldPwdStr.equals(newPwdStr)) {
            ToastUtils.showCustom(this, R.string.gowild_setting_pwd_same, false);
            mGowildModifyPwdConfirmEdt.setText("");
            mGowildModifyPwdNewEdt.setText("");
            mGowildModifyPwdNewEdt.requestFocus();
            return;
        }
        //确认密码和密码不一样
        if (!newPwdStr.equals(confirmPwdStr)) {
            ToastUtils.showCustom(this, R.string.gowild_setting_pwd_different, false);
            mGowildModifyPwdConfirmEdt.setText("");
            mGowildModifyPwdNewEdt.setText("");
            mGowildModifyPwdNewEdt.requestFocus();
            return;
        }
        String username = SpUtils.getString(GowildModifyPwdActivity.this, Constants.USERNAME);
        RequestModifyPwd requestModifyPwd = new RequestModifyPwd();
        requestModifyPwd.put(Constants.KEY_USERNAME, username);
        requestModifyPwd.put(Constants.KEY_OLDPASSWORD, oldPwdStr);
        requestModifyPwd.put(Constants.KEY_PASSWORD, confirmPwdStr);
        showLoading(R.string.loading);
        GowildHttpManager.requestModifyPwd(this, requestModifyPwd, mModifyPwdCallback);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
    }

    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================
    private class RequesModifyPwdCallback extends GowildAsyncHttpResponseHandler<ModifyPwd> {

        /**
         * 构造方法
         *
         * @param context 用于处理授权过期消息，会被转成ApplicationContext
         */
        public RequesModifyPwdCallback(Context context) {
            super(context);
        }

        @Override
        public void onSuccess(int statusCode, ModifyPwd responseBody) {
            Message msg = mHandler.obtainMessage();
            msg.what = MSG_CODE_SUCCESS;
            msg.obj = responseBody;
            mHandler.sendMessage(msg);
        }

        @Override
        public void onFailure(int statusCode, String responseBody) {
            Message msg = mHandler.obtainMessage();
            msg.what = MSG_CODE_FAIL;
            msg.obj = responseBody;
            mHandler.sendMessage(msg);
        }
    }
}
