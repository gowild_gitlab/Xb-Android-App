/**
 * Copyright (C)  2016 深圳市狗尾草智能科技有限公司
 * GowildXB
 * GowildRobotInfoActivity.java
 */
package com.gowild.mobileclient.activity.setting;

import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.gowild.mobileclient.R;
import com.gowild.mobileclient.activity.base.GowildHasTitileActivity;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * @author Zhangct
 * @version 1.0
 *          <p> 主要功能介绍 </p>
 * @since 2016/8/8 14:54
 */
public class GowildRobotInfoActivity extends GowildHasTitileActivity {

    // ===========================================================
    // HostConstants
    // ===========================================================

    private static final String TAG = GowildRobotInfoActivity.class.getSimpleName();
    @Bind(R.id.vp_about)
    ViewPager mVpAbout;

    // ===========================================================
    // Static Fields
    // ===========================================================

    // ===========================================================
    // Fields
    // ===========================================================

    // ===========================================================
    // Constructors
    // ===========================================================


    // ===========================================================
    // Getter or Setter
    // ===========================================================


    // ===========================================================
    // Methods for/from SuperClass/Interfaces
    // ===========================================================
    @Override
    protected int onSetContentView() {
        return R.layout.activity_robot_info;
    }

    @Override
    protected void initEvent() {
        setTitle(R.string.gowild_setting_about_robot);
    }

    @Override
    protected void initView() {
        super.initView();
        initData();
    }

    // ===========================================================
    // Methods
    // ===========================================================
    private void initData() {
        final int[] ids = {R.drawable.ic_setting_guide_page1,R.drawable.ic_setting_guide_page2,R.drawable.ic_setting_guide_page3,R.drawable.ic_setting_guide_page4,R.drawable.ic_setting_guide_page5};

        mVpAbout.setAdapter(new PagerAdapter() {
            @Override
            public int getCount() {
                return ids.length;
            }

            @Override
            public boolean isViewFromObject(View view, Object object) {
                return view ==  object;
            }

            @Override
            public void destroyItem(ViewGroup container, int position, Object object) {
               container.removeView((View) object);
            }

            @Override
            public Object instantiateItem(ViewGroup container, int position) {
                ImageView imageView = new ImageView(GowildRobotInfoActivity.this);
                imageView.setImageResource(ids[position]);
                imageView.setScaleType(ImageView.ScaleType.CENTER);
                container.addView(imageView);
                return imageView;
            }
        });
    }


}
