package com.gowild.mobileclient.activity.setting.update;

import org.json.JSONObject;


/**
 * 软件版本信息对象
 */
public class VersionInfo {

    // 版本描述字符串
    private String version = "";
    // 版本更新时间
    private String updateTime = "";
    // 新版本更新下载地址
    private String downloadURL = "";
    // 更新描述信息
    private String displayMessage = "";
    // 版本号
    private int versionCode = 0;

    private int size ;

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    // apk名称
    private String apkName = "com.gowild.mobileclient.apk";

    private int type;


    private boolean enforce = false;//是否强制升级


    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }

    public String getDownloadURL() {
        return downloadURL;
    }

    public void setDownloadURL(String downloadURL) {
        this.downloadURL = downloadURL;
    }

    public String getDisplayMessage() {
        return displayMessage;
    }

    public void setDisplayMessage(String displayMessage) {
        this.displayMessage = displayMessage;
    }

    public int getVersionCode() {
        return versionCode;
    }

    public void setVersionCode(int versionCode) {
        this.versionCode = versionCode;
    }

    public boolean getEnforce() {
        return enforce;
    }

    public void setEnforce(boolean enforce) {
        this.enforce = enforce;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }


    public String getApkName() {
        return apkName;
    }

    public void setApkName(String apkName) {
        this.apkName = apkName;
    }

    public VersionInfo(JSONObject json) {
        try {
            if (json.has("apkName")) {
                apkName = json.getString("apkName");
            }
            if (json.has("version")) {
                version = json.getString("version");
            }
            if (json.has("versionCode")) {
                versionCode = json.getInt("versionCode");
            }
            if (json.has("downloadURL")) {
                downloadURL = json.getString("downloadURL");
            }
            if (json.has("displayMessage")) {
                displayMessage = json.getString("displayMessage");
            }
            if (json.has("updateTime")) {
                updateTime = json.getString("updateTime");
            }


        } catch (Exception e) {
            // TODO: handle exception
        }
    }

    public VersionInfo() {

    }
}
