package com.gowild.mobileclient.activity.base;

import android.content.Intent;
import android.os.Bundle;

import com.gowild.mobileclient.R;
import com.gowild.mobileclient.base.baseactivity.GowildBaseActivity;

import butterknife.ButterKnife;
/**
 * @author shuai. wang
 * @version 1.0
 * @since ${DATE} ${TIME}
 * <p><strong>Features draft description.主要功能介绍</strong></p>
 */
public abstract class GowildNoTitileActivity extends GowildBaseActivity {

	// ===========================================================
	// Methods for/from SuperClass/Interfaces
	// ===========================================================

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		ButterKnife.bind(this);  //butterKnife的绑定
		initView();
		initData();
		initEvent();
	}


	@Override
	protected int onSetContentView() {
		return getLayout();
	}

	@Override
	protected int onSetTopbarView() {
		return R.layout.view_gowild_toolbar;
	}
	/**
	 * 子类重写该方法,返回顶部的效果
	 * @return
	 */
	@Override
	protected int onSetTopBarHeight() {
		return 0;
	}
	// ===========================================================
	// Methods
	// ===========================================================

	/**
	 * 子类重写该方法
	 * @return 布局id
	 */
	public abstract int getLayout();

	/**
	 * 子类重写该方法,初始化视图
	 */
	protected abstract void initView();

	/**
	 * 子类重写该方法,初始化数据
	 */
	protected abstract void initData();

	/**
	 * 子类重写该方法,初始化事件
	 */
	protected abstract void initEvent();

	/**
	 * 上一个actvivity,返回
	 *
	 * @param clazz
	 */
	public void preActivtiy(Class clazz) {
		Intent intent = new Intent(GowildNoTitileActivity.this, clazz);
		startActivity(intent);
		finish();
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
	}

	/**
	 * 开启下一个activity,下一步
	 *
	 * @param clazz
	 */
	public void nextActivtiy(Class clazz) {
		Intent intent = new Intent(GowildNoTitileActivity.this, clazz);
		startActivity(intent);
	}
}
