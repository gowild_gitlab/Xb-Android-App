/**
 * Copyright (C)  2016 深圳市狗尾草智能科技有限公司
 * GowildXB
 * MusicSearchActivity.java
 */
package com.gowild.mobileclient.activity.music;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.gowild.mobileclient.R;
import com.gowild.mobileclient.base.baseactivity.GowildBaseActivity;
import com.gowild.mobileclient.config.Constants;
import com.gowild.mobileclient.config.IntContants;
import com.gowild.mobileclient.manager.GowildAsyncHttpResponseHandler;
import com.gowild.mobileclient.manager.GowildHttpManager;
import com.gowild.mobileclient.vo.RequestSingerSortList;
import com.gowild.mobileclient.vo.RequestStorySort;
import com.gowild.mobileclient.vo.SingerSort;
import com.gowild.mobileclient.vo.StorySort;

import java.util.ArrayList;


/**
 * @author JunDongWang
 * @version 1.0
 *          <p><strong>搜索歌手类别</strong></p>
 * @since 2016/7/27 11:24
 */
public class MusicSearchActivity extends GowildBaseActivity {

    // ===========================================================
    // HostConstants
    // ===========================================================

    private static final String TAG = MusicSearchActivity.class.getSimpleName();

    private OnEditorActionCallback mOnEditorActionCallback = new OnEditorActionCallback();
    private EditText edit;
    private ListView mListView;
    private TextView mTvTitle;
    private SingerTypeAdapter mSingerTypeAdapter = new SingerTypeAdapter();
    private OnItemClickCallback mOnItemClickCallback = new OnItemClickCallback();
    private ResponseSingerSortCallback mResponseSingerSortCallback;
    private ResponseStorySortCallback mResponseStorySortCallback;
    private OnClickCallback mOnClickCallback = new OnClickCallback();
    private View mBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initView();
    }

    @Override
    protected void onResume() {
        super.onResume();
        isClick = false;
        mListView.setAdapter(mSingerTypeAdapter);
        mListView.setOnItemClickListener(mOnItemClickCallback);
        mResponseSingerSortCallback = new ResponseSingerSortCallback(this);
        mResponseStorySortCallback = new ResponseStorySortCallback(this);
        if (IntContants.isCurrentMusic()) {
            edit.setHint(R.string.gowild_search_music_singer);
            mTvTitle.setText(R.string.gowild_title_music_search);
            showLoading(R.string.loading);
            GowildHttpManager.requestSingerSort(this, new RequestSingerSortList(), mResponseSingerSortCallback);
        } else {
            edit.setHint(R.string.gowild_search_story);
            mTvTitle.setText(R.string.gowild_title_story_search);
            showLoading(R.string.loading);
            GowildHttpManager.requestStoryCategorySort(this, new RequestStorySort(), mResponseStorySortCallback);
        }
    }

    @Override
    protected int onSetTopbarView() {
        return R.layout.activity_gowild_find_music_by_sort_topbar;
    }

    @Override
    protected int onSetContentView() {
        return R.layout.activity_gowild_find_music_by_sort;
    }

    @Override
    protected int onSetTopBarHeight() {
        return getResources().getDimensionPixelOffset(R.dimen.gowild_toolbar_height);
    }

    private void initView() {
        edit = (EditText) findViewById(R.id.search_bar);
        edit.setOnEditorActionListener(mOnEditorActionCallback);
        mTvTitle = (TextView) findViewById(R.id.title);
        mListView = (ListView) findViewById(R.id.list);
        mBack = findViewById(R.id.back);
        mBack.setOnClickListener(mOnClickCallback);
    }

    boolean isClick = false;

    private class OnEditorActionCallback implements TextView.OnEditorActionListener {

        @Override
        public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
            if (isClick){
                return false;
            }
            if (actionId == EditorInfo.IME_ACTION_SEARCH || (event != null && event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) {
                isClick = true;
                if (IntContants.isCurrentMusic()) {
                    Intent intent = new Intent(getBaseContext(), MusicSearchResultActivity.class);
                    intent.putExtra(Constants.TYPE, MusicSearchResultActivity.FROM_SINGER_SEARCH_BAR);
                    intent.putExtra(Constants.INFOS, edit.getText().toString());
                    startActivity(intent);
                } else {
                    Intent intent = new Intent(getBaseContext(), StoryAlbumActivity.class);
                    intent.putExtra(Constants.TYPE, StoryAlbumActivity.FROM_KEY_WORD);
                    intent.putExtra(Constants.INFOS, edit.getText().toString());
                    startActivity(intent);
                }
                return false;
            }
            return false;
        }
    }

    private class SingerTypeAdapter extends BaseAdapter {
        private ArrayList<SingerSort> mSingerSort = new ArrayList<>();
        private ArrayList<StorySort> mStorySort = new ArrayList<>();

        public void updateSingerSortList(ArrayList<SingerSort> singerSorts) {
            mSingerSort.clear();
            mSingerSort.addAll(singerSorts);
            notifyDataSetChanged();
        }

        public void updateStorySortList(ArrayList<StorySort> storySorts) {
            mStorySort.clear();
            mStorySort.addAll(storySorts);
            notifyDataSetChanged();
        }

        @Override
        public int getCount() {
            if (IntContants.isCurrentMusic()) {
                return mSingerSort.size();
            } else {
                return mStorySort.size();
            }
        }

        @Override
        public Object getItem(int position) {
            if (IntContants.isCurrentMusic()) {
                return mSingerSort.get(position);
            } else {
                return mStorySort.get(position);
            }
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = LayoutInflater.from(getBaseContext()).inflate(R.layout.item_gowild_singer_sort_list, parent, false);
            }
            if (IntContants.isCurrentMusic()) {
                SingerSort singerSort = (SingerSort) getItem(position);
                TextView sort = (TextView) convertView.findViewById(R.id.sort);
                sort.setText(singerSort.musicType);
            } else {
                StorySort storySort = (StorySort) getItem(position);
                TextView sort = (TextView) convertView.findViewById(R.id.sort);
                sort.setText(storySort.category);
            }
            return convertView;
        }
    }

    private class OnItemClickCallback implements AdapterView.OnItemClickListener {

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            if (IntContants.isCurrentMusic()) {
                SingerSort singerSort = (SingerSort) mSingerTypeAdapter.getItem(position);
                Intent intent = new Intent(getBaseContext(), MusicSingerListActivity.class);
                intent.putExtra(Constants.ID, singerSort.id);
                intent.putExtra(Constants.SORT, singerSort.musicType);
                startActivity(intent);
            } else {
                StorySort storySort = (StorySort) mSingerTypeAdapter.getItem(position);
                Intent intent = new Intent(getBaseContext(), StoryAlbumActivity.class);
                intent.putExtra(Constants.TYPE, StoryAlbumActivity.FROM_SORT_LIST);
                intent.putExtra(Constants.INFOS, storySort.category);
                intent.putExtra(Constants.CATEGORY_ID, storySort.id);
                startActivity(intent);
            }
        }
    }

    private class ResponseStorySortCallback extends GowildAsyncHttpResponseHandler<StorySort> {
        public ResponseStorySortCallback(Context context) {
            super(context, true);
        }

        @Override
        public void onSuccess(int statusCode, StorySort responseBody) {

        }

        @Override
        public void onSuccess(int statusCode, ArrayList<StorySort> response) {
            hideLoading();
            mSingerTypeAdapter.updateStorySortList(response);
        }

        @Override
        public void onFailure(int statusCode, String responseBody) {
            hideLoading();
//            ToastUtils.showCustom(getBaseContext(),R.string.gowild_network_error, false);
        }
    }

    private class ResponseSingerSortCallback extends GowildAsyncHttpResponseHandler<SingerSort> {

        public ResponseSingerSortCallback(Context context) {
            super(context, true);
        }

        @Override
        public void onSuccess(int statusCode, ArrayList<SingerSort> response) {
            hideLoading();
            mSingerTypeAdapter.updateSingerSortList(response);
        }

        @Override
        public void onSuccess(int statusCode, SingerSort responseBody) {

        }

        @Override
        public void onFailure(int statusCode, String responseBody) {
            hideLoading();
//            ToastUtils.showCustom(getBaseContext(),R.string.gowild_network_error, false);
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    private class OnClickCallback implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.back:
                    finish();
                    break;
            }
        }
    }

}
