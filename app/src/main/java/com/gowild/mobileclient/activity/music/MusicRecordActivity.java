/**
 * Copyright (C)  2016 深圳市狗尾草智能科技有限公司
 * GowildXB
 * MusicLoverActivity.java
 */
package com.gowild.mobileclient.activity.music;

import android.content.Intent;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.gowild.mobileclient.R;
import com.gowild.mobileclient.manager.GowildHttpManager;
import com.gowild.mobileclient.model.BaseNetResourceInfo;
import com.gowild.mobileclient.config.IntContants;
import com.gowild.mobileclient.manager.GowildAsyncHttpResponseHandler;
import com.gowild.mobileclient.model.MusicItemInfoBack;
import com.gowild.mobileclient.model.StoryAlbumItemInfoBack;
import com.gowild.mobileclient.vo.RequestStoryHistory;


import java.util.ArrayList;


/**
 * @author temp
 * @version 1.0
 *          <p><strong>Features draft description.主要功能介绍</strong></p>
 * @since 2016/8/2 21:51
 */
public class MusicRecordActivity extends NetResourceListBaseActivity {

    private static final String TAG = MusicRecordActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //不需要上拉下拉
        mPullLayout.setPullDownToRefreshEnable(false);
        mPullLayout.setPullUpToLoadEnable(false);
    }


    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public String getToobarTitle() {
        return getResources().getString(R.string.gowild_music_play_record_title);
    }

    @Override
    public View getTipView() {
        TextView view = (TextView) LayoutInflater.from(this).
                inflate(R.layout.view_gowild_net_resource_top_hint, getRootView(), false);
        view.setText(R.string.gowild_all_record);
        return view;
    }

    public void loadInfo(int pageNum) {
        if (IntContants.isCurrentMusic()) {
            showLoading(R.string.loading);
            GowildHttpManager.getMusicHistory(this, new GowildAsyncHttpResponseHandler<MusicItemInfoBack>(this, true) {
                @Override
                public void onSuccess(int statusCode, MusicItemInfoBack responseBody) {

                }

                @Override
                public void onSuccess(int statusCode, ArrayList<MusicItemInfoBack> responseBody) {
                    super.onSuccess(statusCode, responseBody);
                    hideLoading();
                    onLoadSuccess(responseBody);
                }

                @Override
                public void onFailure(int statusCode, String responseBody) {
                    hideLoading();
                    onLoadFailure();
                }
            });
        } else {
            showLoading(R.string.loading);
            GowildHttpManager.getStoryHistory(this, new RequestStoryHistory(), new GowildAsyncHttpResponseHandler<StoryAlbumItemInfoBack>(this, true) {
                @Override
                public void onSuccess(int statusCode, StoryAlbumItemInfoBack responseBody) {

                }

                @Override
                public void onSuccess(int statusCode, ArrayList<StoryAlbumItemInfoBack> responseBody) {
                    super.onSuccess(statusCode, responseBody);
                    hideLoading();
                    onLoadSuccess(responseBody);
                }

                @Override
                public void onFailure(int statusCode, String responseBody) {
                    hideLoading();
                    onLoadFailure();
                }
            });
        }

    }


}
