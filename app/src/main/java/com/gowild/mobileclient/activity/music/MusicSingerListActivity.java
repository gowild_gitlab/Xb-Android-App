package com.gowild.mobileclient.activity.music;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.gowild.gwais.libwheelview.adapter.ArrayWheelAdapter;
import com.gowild.gwais.libwheelview.widght.WheelView;
import com.gowild.mobile.libcommon.utils.ToastUtils;
import com.gowild.mobileclient.R;
import com.gowild.mobileclient.base.baseactivity.GowildBaseActivity;
import com.gowild.mobileclient.config.Constants;
import com.gowild.mobileclient.manager.GowildAsyncHttpResponseHandler;
import com.gowild.mobileclient.manager.GowildHttpManager;
import com.gowild.mobileclient.vo.RequestSinger;
import com.gowild.mobileclient.vo.ResponseSinger;
import com.gowild.mobileclient.vo.Singer;
import com.gowild.mobileclient.widget.PullToRefreshLayout;

import java.util.ArrayList;

/**
 * Created by ljd on 2016/8/4.
 */
public class MusicSingerListActivity extends GowildBaseActivity {

    private EditText edit;
    private SingerAdapter mAdapter;
    private ListView mList;
    private OnRefreshCallback mOnRefreshCallback = new OnRefreshCallback();
    private PullToRefreshLayout mPrltRefresh;
    private OnClickCallback mOnClickCallback = new OnClickCallback();
    private int mSingerType = 1;
    private String mTypeName = "";
    private TextView mTvSearchChar;
    private ArrayList<String> A_Zarr = new ArrayList<>();
    private View mBack;
    private WheelView wheel;
    private RelativeLayout mWheelContainer;
    private OnRequestSingerByPinYinCallback mOnRequestSingerByPinYinCallback;
    private OnItemClickCallback mOnItemClickCallback = new OnItemClickCallback();
    private OnEditorActionCallback mOnEditorActionCallback = new OnEditorActionCallback();
    private ArrayList<Singer> mSingerList = new ArrayList<>();
    private int mPageNum = 1;
    private int mPageSize = 10;
    private boolean hasNext = true;
    private boolean hasPrevious = true;

    @Override
    protected int onSetTopbarView() {
        return 0;
    }

    @Override
    protected int onSetContentView() {
        return R.layout.activity_gowild_singer_list;
    }

    @Override
    protected int onSetTopBarHeight() {
        return 0;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        TextView cancel = (TextView) findViewById(R.id.cancel);
        cancel.setOnClickListener(mOnClickCallback);
        TextView complete = (TextView) findViewById(R.id.complete);
        complete.setOnClickListener(mOnClickCallback);
        mWheelContainer = (RelativeLayout) findViewById(R.id.wheel_container);
        wheel = (WheelView) findViewById(R.id.wheel);
        edit = (EditText) findViewById(R.id.search_bar);
        mList = (ListView) findViewById(R.id.list);
        mPrltRefresh = (PullToRefreshLayout) findViewById(R.id.refresh_view);
        mTvSearchChar = (TextView) findViewById(R.id.search_char);
        mTvSearchChar.setOnClickListener(mOnClickCallback);
        EditText edit = (EditText) findViewById(R.id.search_bar);
        edit.setOnEditorActionListener(mOnEditorActionCallback);
        mBack = findViewById(R.id.back);
        mBack.setOnClickListener(mOnClickCallback);
    }

    @Override
    protected void onResume() {
        super.onResume();
        isClick = false;
        mOnRequestSingerByPinYinCallback = new OnRequestSingerByPinYinCallback(this);
        initWheel();
        mWheelContainer.setVisibility(View.GONE);
        mSingerType = getIntent().getIntExtra(Constants.ID, 1);
        mTypeName = getIntent().getStringExtra(Constants.SORT);
        mPrltRefresh.setOnRefreshListener(mOnRefreshCallback);
        mPrltRefresh.setPullDownToRefreshEnable(false);
        mAdapter = new SingerAdapter();
        mList.setAdapter(mAdapter);
        mList.setOnItemClickListener(mOnItemClickCallback);
        requestSinger();
    }

    private class OnRefreshCallback implements PullToRefreshLayout.OnRefreshListener {

        @Override
        public void onRefresh(PullToRefreshLayout pullToRefreshLayout) {
            mPrltRefresh.loadmoreFinish(PullToRefreshLayout.SUCCEED);
        }

        @Override
        public void onLoadMore(PullToRefreshLayout pullToRefreshLayout) {
            if (hasNext) {
                mPageNum = mPageNum + 1;
                requestSinger();
            } else {
                mPrltRefresh.loadmoreFinish(PullToRefreshLayout.FAIL);
                ToastUtils.showCustom(getBaseContext(), R.string.gowild_no_more, false);
            }
        }
    }

    private void initWheel() {
        for (int i = (int) 'A'; i <= (int) 'Z'; ++i) {
            A_Zarr.add(String.valueOf((char) i));
        }
        String[] array = A_Zarr.toArray(new String[]{});
        ArrayWheelAdapter<String> wheelAdapter = new ArrayWheelAdapter<>(getBaseContext(), array);
        wheelAdapter.setTextSize(18);
        wheelAdapter.setTextColor(getResources().getColor(R.color.gowild_alert_clock_title));
        wheel.setCyclic(false);
        wheel.setViewAdapter(wheelAdapter);
        wheel.setCurrentItem(0);
    }


    private void requestSinger() {
        RequestSinger requestSinger = new RequestSinger();
        if (!mTvSearchChar.getText().toString().equals(getStringById(R.string.gowild_sort))) {
            requestSinger.put(Constants.SEARCH_KEY, mTvSearchChar.getText().toString());
        }
        requestSinger.put(Constants.CLASSIFY_ID, Integer.toString(mSingerType));
        requestSinger.put(Constants.CLASSIFY, mTypeName);
        requestSinger.put(Constants.PAGE_NUM, Integer.toString(mPageNum));
        requestSinger.put(Constants.PAGE_SIZE, Integer.toString(mPageSize));
        showLoading(R.string.loading);
        GowildHttpManager.requestSingerListByPy(getBaseContext(), requestSinger, mOnRequestSingerByPinYinCallback);
    }

    private class SingerAdapter extends BaseAdapter {

        private ArrayList<Singer> mSingerList = new ArrayList<>();

        public void update(ArrayList<Singer> singerList, boolean clean) {
            if (clean) {
                mSingerList.clear();
            }
            mSingerList.addAll(singerList);
            notifyDataSetChanged();
        }

        @Override
        public int getCount() {
            return mSingerList.size();
        }

        @Override
        public Singer getItem(int position) {
            return mSingerList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = LayoutInflater.from(getBaseContext()).inflate(R.layout.item_gowild_singer_list, parent, false);
            }
            Singer singer = mSingerList.get(position);
            TextView mTvsinger = (TextView) convertView.findViewById(R.id.singer);
            mTvsinger.setText(singer.singerName);
            return convertView;
        }
    }

    private class OnClickCallback implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.cancel:
                    mWheelContainer.setVisibility(View.GONE);
                    break;
                case R.id.complete:
                    mTvSearchChar.setText(A_Zarr.get(wheel.getCurrentItem()));
                    mWheelContainer.setVisibility(View.GONE);
                    mPageNum = 1;
                    requestSinger();
                    break;
                case R.id.search_char:
                    mWheelContainer.setVisibility(View.VISIBLE);
                    break;
                case R.id.back:
                    finish();
                    break;
            }
        }
    }

    private class OnRequestSingerByPinYinCallback extends GowildAsyncHttpResponseHandler<ResponseSinger> {
        public OnRequestSingerByPinYinCallback(Context context) {
            super(context);
        }

        @Override
        public void onSuccess(int statusCode, ResponseSinger response) {
            hideLoading();
            mPageNum = response.pageNum;
            hasNext = response.hasNextPage;
            hasPrevious = response.hasPreviousPage;
            if (mPageNum == 1) {
                mSingerList.clear();
                mSingerList.addAll(response.singers);
                mAdapter.update(mSingerList, true);
            } else {
                mSingerList.addAll(response.singers);
                mAdapter.update(mSingerList, true);
            }
            mPrltRefresh.loadmoreFinish(PullToRefreshLayout.SUCCEED);
        }

        @Override
        public void onFailure(int statusCode, String response) {
            hideLoading();
            mPrltRefresh.loadmoreFinish(PullToRefreshLayout.FAIL);
        }


    }

    private class OnItemClickCallback implements AdapterView.OnItemClickListener {

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            Singer singer = mAdapter.getItem(position);
            Intent intent = new Intent(getBaseContext(), MusicSearchResultActivity.class);
            intent.putExtra(Constants.TYPE, MusicSearchResultActivity.FROM_SINGERLIST);
            intent.putExtra(Constants.INFOS, singer.singerName);
            startActivity(intent);
        }
    }
    boolean isClick = false;

    //开始搜索列表
    private class OnEditorActionCallback implements TextView.OnEditorActionListener {

        @Override
        public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
            if (isClick){
                return false;
            }
            if (actionId == EditorInfo.IME_ACTION_SEARCH || (event != null && event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) {
                Intent intent = new Intent(getBaseContext(), MusicSearchResultActivity.class);
                intent.putExtra(Constants.TYPE, MusicSearchResultActivity.FROM_SINGER_SEARCH_BAR);
                intent.putExtra(Constants.INFOS, edit.getText().toString());
                startActivity(intent);
                return true;
            }
            return false;
        }
    }
}