/**
 * Copyright (C)  2016 深圳市狗尾草智能科技有限公司
 *
 * PhotoDetailActivity.java
 */
package com.gowild.mobileclient.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.gowild.mobile.libimage.ImageUtil;
import com.gowild.mobileclient.R;
import com.gowild.mobileclient.base.baseactivity.GowildBaseActivity;
import com.gowild.mobileclient.config.Constants;
import com.gowild.mobileclient.fragment.PhotoListFragment;
import com.gowild.mobileclient.model.PhotoContentInfo;
import com.gowild.mobileclient.widget.MyGallery;
import com.gowild.mobileclient.widget.MyToolBar;
import com.gowild.mobileclient.widget.PhotoBottomView;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * @author 王揆
 * @since 2016/7/20 21:47
 * @version 1.0
 * <p><strong>Features draft description.主要功能介绍</strong></p>
 */
public class PhotoDetailActivity extends GowildBaseActivity implements PhotoBottomView.OnPhotoDeleteCallback {

    private static final String         TAG    = PhotoDetailActivity.class.getSimpleName();
    private ArrayList<PhotoContentInfo> mInfos = new ArrayList<>();
    private int mPos;

    @Bind(R.id.vp_photo_preview)
    protected ViewPager       mVpPreview;
    @Bind(R.id.gallery)
    protected MyGallery       mGallery;
    @Bind(R.id.ly_photo_list_bottom)
    protected PhotoBottomView mPhotoBottomView;

    private   LayoutInflater  mInflater;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        mInflater = LayoutInflater.from(this);
        MyToolBar toolBar = (MyToolBar) findViewById(R.id.view_toolbar);
        toolBar.setTitle("");
        mInfos = getIntent().getParcelableArrayListExtra(Constants.INFOS);
        mPos = getIntent().getIntExtra(Constants.POS, 0);
        if (mInfos == null || mInfos.isEmpty()) {
            finish();
            return;
        }
        if (mInfos.get(0).isLocalRes()) {
            mPhotoBottomView.hideDownload();
        }
        initViewPager();
        initThumbGl();
        mPhotoBottomView.setInfo(mInfos);
        mPhotoBottomView.setCurrentPos(mPos);
        mPhotoBottomView.setOnPhotoDeleteCallback(this);
        mPhotoBottomView.setVisibility(View.VISIBLE);
    }

    @Override
    protected int onSetTopbarView() {
        return R.layout.view_gowild_toolbar;
    }

    @Override
    protected int onSetContentView() {
        return R.layout.activity_gowild_photo_detail;
    }

    @Override
    protected int onSetTopBarHeight() {
        return getResources().getDimensionPixelSize(R.dimen.gowild_toolbar_height);
    }

    private void initViewPager() {
        mVpPreview.setAdapter(mPreviewAdapter);
        mVpPreview.setCurrentItem(mPos);
        mVpPreview.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if (!mGallery.isScrolling()) {
                    mGallery.setSelection(position);
                }
                mPhotoBottomView.setCurrentPos(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    private void initThumbGl() {
        mGallery.setAdapter(mAdapter);
        mGallery.setSelection(mPos);
        mGallery.setSpacing(getResources().getDimensionPixelSize(R.dimen.gowild_photo_detail_gallery_space));
        mGallery.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                mPos = position;
                mAdapter.notifyDataSetChanged();
                mVpPreview.setCurrentItem(mPos);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private PagerAdapter mPreviewAdapter = new PagerAdapter() {
        public int mChildCount;

        @Override
        public int getCount() {
            return mInfos.size();
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

        @Override public void notifyDataSetChanged() {
            mChildCount = getCount();
            super.notifyDataSetChanged();
        }

        /**
         * 默认返回POSITION_UNCHANGED，在调用notifyDataSetChanged返回POSITION_NONE
         * @param object
         * @return
         */
        @Override
        public int getItemPosition(Object object) {
            if (mChildCount > 0) {
                mChildCount--;
                return POSITION_NONE;
            }
            return super.getItemPosition(object);
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            ImageView view = (ImageView) mInflater.inflate(R.layout.view_gowild_photopreview, container, false);
            ImageUtil.getInstance().displayImage(mInfos.get(position).mPreviewName, view);
            container.addView(view);
            return view;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View)object);
        }
    };

    private BaseAdapter mAdapter = new BaseAdapter() {

        @Override
        public int getCount() {
            return mInfos.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = mInflater.inflate(R.layout.view_gowild_photothumb, parent, false);
            }
            if (mPos == position) {
                ViewGroup.LayoutParams params = convertView.getLayoutParams();
                params.width *= 1.5;
                params.height *= 1.5;
                convertView.setLayoutParams(params);
            }
            ImageUtil.getInstance().displayImage(mInfos.get(position).mPreviewName, (ImageView) convertView);
            return convertView;
        }
    };

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void finish() {
        setResult(PhotoListFragment.CODE_RESULT_OK,
                new Intent().putParcelableArrayListExtra(Constants.INFOS, mInfos));
        super.finish();
    }

    @Override
    public void onDelete() {
        if(mInfos.isEmpty()) {
            finish();
        }
        mAdapter.notifyDataSetChanged();
        mPreviewAdapter.notifyDataSetChanged();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        mPhotoBottomView.onActivityResult(requestCode, resultCode, data);
    }
}
