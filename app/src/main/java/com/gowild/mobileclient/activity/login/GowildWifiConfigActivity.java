package com.gowild.mobileclient.activity.login;

import android.content.Context;
import android.content.Intent;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.gowild.mobile.libcommon.utils.SpUtils;
import com.gowild.mobile.libcommon.utils.ToastUtils;
import com.gowild.mobileclient.R;
import com.gowild.mobileclient.activity.GowildHomeActivity;
import com.gowild.mobileclient.activity.base.GowildLoginAndRegisterActivity;
import com.gowild.mobileclient.base.baseadapter.HomeBaseAdapter;
import com.gowild.mobileclient.base.baseadapter.ViewHolder;
import com.gowild.mobileclient.config.Constants;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import butterknife.Bind;
import butterknife.OnClick;

/**
 * @author shuai. wang
 * @version 1.0
 * @since ${DATE} ${TIME}
 * <p><strong>Features draft description.主要功能介绍</strong></p>
 */
public class GowildWifiConfigActivity extends GowildLoginAndRegisterActivity {
	@Bind(R.id.gowild_wifi_title)
	EditText mGowildWifiTitle;
	@Bind(R.id.gowild_wifi_next)
	TextView mGowildWifiNext;
	@Bind(R.id.gowild_wifi_lv)
	ListView mGowildWifiLv;
	@Bind(R.id.gowild_wifi_et)
	EditText mGowildWifiEt;
	private List<ScanResult> mScanResults;
	private String mSsid;
	private String mClazz;

	// ===========================================================
	// Methods
	// ===========================================================

	// ===========================================================
	// Methods for/from SuperClass/Interfaces
	// ===========================================================
	@Override
	public int getLayout() {
		return R.layout.activity_gowild_wificonfig;
	}


	@Override
	protected void initData() {
		WifiManager wifiManager = (WifiManager) getSystemService(Context.WIFI_SERVICE);
		List<ScanResult> results = wifiManager.getScanResults();

		mScanResults = new ArrayList<>();
		for (ScanResult result : results) {
			if (result.SSID == null || result.SSID.length() == 0 || result.capabilities.contains("[IBSS]")) {
				continue;
			}
			boolean found = false;
			for (ScanResult item : mScanResults) {
				if (item.SSID.equals(result.SSID) && item.capabilities.equals(result.capabilities)) {
					found = true;
					break;
				}
			}
			if (!found) {
				mScanResults.add(result);
			}
		}


		WifiInfo info = wifiManager.getConnectionInfo();
		//获得当前wifi的名字
		mSsid = info.getSSID();
		if (mSsid.startsWith("\"")) {
			mSsid = mSsid.substring(1, mSsid.length() - 1);
		}
		mClazz = getIntent().getStringExtra(Constants.CLASS);
	}


	@Override
	protected void initView() {
	}

	@Override
	protected void onPause() {
		super.onPause();
	}

	@Override
	protected void onResume() {
		super.onResume();
	}

	@Override
	protected void initEvent() {
		mGowildWifiTitle.setText(mSsid);
		setPwdNumber();
		mGowildWifiLv.setAdapter(new HomeBaseAdapter<ScanResult>(mGowildWifiLv, mScanResults) {


			@Override
			protected ViewHolder getViewHolder() {
				return new MyHolder();
			}

			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				mGowildWifiTitle.setText(mScanResults.get(position).SSID);
				setPwdNumber();
			}
		});
	}

	private void setPwdNumber() {
		String pwd = SpUtils.getString(getApplication(), mGowildWifiTitle.getText().toString().trim());
		if (!TextUtils.isEmpty(pwd)) {
			mGowildWifiEt.setText(pwd);
		}
	}


	@Override
	public void onBackPressed() {
		super.onBackPressed();
		EventBus.getDefault().post("finish");
		nextActivtiy(GowildHomeActivity.class);
	}

	private class MyHolder extends ViewHolder<ScanResult> {

		private TextView mView;
		private String ssid;

		@Override
		public View initHolderView() {
			mView = new TextView(GowildWifiConfigActivity.this);
			mView.setGravity(Gravity.CENTER);
			mView.setTextColor(getResources().getColor(R.color.default_blue));
			mView.setTextSize(getResources().getDimension(R.dimen.gowild_15px));
			return mView;
		}

		@Override
		public void refreshHolderView(ScanResult data) {
			ssid = data.SSID;
			mView.setText(ssid);
		}
	}


	@OnClick(R.id.gowild_wifi_next)
	public void onClick() {
		//如果配置好调到下一步
		nextActivtiy(GowildQRactivity.class);
	}


	@Override
	public void nextActivtiy(Class clazz) {
		String pwd = mGowildWifiEt.getText().toString().trim();
		String title = mGowildWifiTitle.getText().toString().trim();
		if (TextUtils.isEmpty(title)) {
			ToastUtils.showCustom(this, getString(R.string.gowild_wifi_ssid_isnull), false);
			return;
		}
		Intent intent = new Intent(GowildWifiConfigActivity.this, clazz);
		LinkedHashMap<String, String> map = new LinkedHashMap<>();
		map.put("account", SpUtils.getString(this, Constants.USERNAME));
		map.put("pwd", pwd);
		map.put("sex","男");
		map.put("name","狗尾草");
		map.put("ssid", mGowildWifiTitle.getText().toString());
		JSONObject jsonObject = new JSONObject(map);
		intent.putExtra(Constants.SSID, jsonObject.toString());
		startActivity(intent);
	}

}
