/**
 * Copyright (C)  2016 深圳市狗尾草智能科技有限公司
 * GowildXB
 * GowildFeedbackActivity.java
 */
package com.gowild.mobileclient.activity.setting;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.text.Editable;
import android.text.Html;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.gowild.mobile.libcommon.utils.SpUtils;
import com.gowild.mobile.libcommon.utils.ToastUtils;
import com.gowild.mobileclient.R;
import com.gowild.mobileclient.activity.base.GowildHasTitileActivity;
import com.gowild.mobileclient.config.Constants;
import com.gowild.mobileclient.manager.GowildAsyncHttpResponseHandler;
import com.gowild.mobileclient.manager.GowildHttpManager;
import com.gowild.mobileclient.vo.RequestFeedback;

import butterknife.Bind;
import butterknife.OnClick;

/**
 * @author Zhangct
 * @version 1.0
 *          <p> 主要功能介绍 </p>
 * @since 2016/8/6 18:52
 */
public class GowildFeedbackActivity extends GowildHasTitileActivity {

    // ===========================================================
    // HostConstants
    // ===========================================================

    private static final String TAG = GowildFeedbackActivity.class.getSimpleName();

    // ===========================================================
    // Static Fields
    // ===========================================================

    // ===========================================================
    // Fields
    // ===========================================================

    private int mContentCharTotal = 0;
    private int mContentCharMax = 80;
    //请求成功
    private final int MSG_CODE_SUCCESS = 0x001;
    //请求失败
    private final int MSG_CODE_FAIL = 0x002;

    //意见建议文本框
    @Bind(R.id.edt_suggest_content)
    EditText mGowildSuggestEdt;
    //手机号码文本框
    @Bind(R.id.edt_phonenum)
    EditText mGowildPhoneNumEdt;
    //提交按钮
    @Bind(R.id.btn_commit)
    Button mGowildCommitBtn;
    //剩余字数
    @Bind(R.id.tv_char)
    TextView mGowildCharTv;

    private RequesFeedbackCallback mFeedbackCallback = null;

    private Handler mHandler = new Handler() {

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case MSG_CODE_SUCCESS:
                    hideLoading();
                    finish();
                    break;
                case MSG_CODE_FAIL:
//                    ToastUtils.showCustom(GowildFeedbackActivity.this, R.string.gowild_network_error, false);
                    hideLoading();
                    break;
            }
        }
    };


    // ===========================================================
    // Constructors
    // ===========================================================


    // ===========================================================
    // Getter or Setter
    // ===========================================================


    // ===========================================================
    // Methods for/from SuperClass/Interfaces
    // ===========================================================
    @Override
    protected int onSetContentView() {
        return R.layout.activity_gowild_feedback;
    }

    @Override
    protected void initEvent() {
        setTitle(R.string.gowild_setting_about_advise);
        mFeedbackCallback = new RequesFeedbackCallback(this);
        mGowildSuggestEdt.addTextChangedListener(new EditChangedListener());
    }

    @Override
    protected void loadData() {
        super.loadData();
        mGowildPhoneNumEdt.setText(SpUtils.getString(getApplicationContext(),Constants.USERNAME));
    }

    @OnClick({R.id.btn_commit})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_commit:
                commitSuggest();
            default:
                break;
        }
    }

    /**
     * 保存意见反馈
     */
    private void commitSuggest() {
        String content = mGowildSuggestEdt.getText().toString().trim();
        String phoneNum = mGowildPhoneNumEdt.getText().toString().trim();
        //判断内容的字符数是否正确
        if (TextUtils.isEmpty(content)) {
            ToastUtils.showCustom(this, "反馈内容不能为空", false);
            return;
        }
        if (content.length() < 2 || content.length() > 80) {
            ToastUtils.showCustom(this, "反馈内容字符个数为2到80", false);
            return;
        }
        showLoading(R.string.gowild_committing);
        //提交数据
        RequestFeedback requestFeedback = new RequestFeedback();
        requestFeedback.put(Constants.KEY_FEEDBACK_PHONE, phoneNum);
        requestFeedback.put(Constants.KEY_FEEDBACK_CONTENT, content);
        requestFeedback.put("source","Android");
        showLoading(R.string.loading);
        GowildHttpManager.requestSaveFeedback(this, requestFeedback, mFeedbackCallback);

    }

    // ===========================================================
    // Methods
    // ===========================================================


    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================

    class EditChangedListener implements TextWatcher {
        private CharSequence temp;// 监听前的文本
        private int editStart;// 光标开始位置
        private int editEnd;// 光标结束位置

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count,
                                      int after) {
            Log.e(this.toString(), "输入文本之前的状态");
            temp = s;
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before,
                                  int count) {
            Log.e(this.toString(), "s.length() = " + s.length());
            mGowildCharTv.setText(mContentCharMax - s.length() + "");
            if (s.length() > 0 && s.length() <= mContentCharMax) {
                mGowildCommitBtn.setEnabled(true);
            } else if (s.length() > mContentCharMax) {
                mGowildCommitBtn.setEnabled(false);
                mGowildCommitBtn.setText(Html.fromHtml("<font color='#ff3030'>"
                        + (mContentCharMax - s.length()) + "</font>"));
            } else {
                mGowildCommitBtn.setEnabled(false);
            }

        }

        @Override
        public void afterTextChanged(Editable s) {
            Log.e(this.toString(), "输入文字后的状态");

            /** 得到光标开始和结束位置 ,超过最大数后记录刚超出的数字索引进行控制 */
            /*
             * editStart = mContentEditText.getSelectionStart(); editEnd =
			 * mContentEditText.getSelectionEnd(); if (temp.length() >
			 * charMaxNum) { //输入的字数已经超过了限制 s.delete(editStart - 1, editEnd);
			 * int tempSelection = editStart; mContentEditText.setText(s);
			 * mContentEditText.setSelection(tempSelection); }
			 */
        }
    }


    private class RequesFeedbackCallback extends GowildAsyncHttpResponseHandler<Object> {

        /**
         * 构造方法
         *
         * @param context 用于处理授权过期消息，会被转成ApplicationContext
         */
        public RequesFeedbackCallback(Context context) {
            super(context);
        }

        @Override
        public void onSuccess(int statusCode, Object responseBody) {
            Message msg = mHandler.obtainMessage();
            msg.what = MSG_CODE_SUCCESS;
            mHandler.sendMessage(msg);
        }

        @Override
        public void onFailure(int statusCode, String responseBody) {
            Message msg = mHandler.obtainMessage();
            msg.what = MSG_CODE_FAIL;
            mHandler.sendMessage(msg);
        }
    }

}
