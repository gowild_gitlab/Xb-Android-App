/**
 * Copyright (C)  2016 深圳市狗尾草智能科技有限公司
 * GowildXB
 * GowildApplianceSearchActivity.java
 */
package com.gowild.mobileclient.activity.appliances;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.alibaba.fastjson.JSONObject;
import com.gowild.mobile.libcommon.utils.Logger;
import com.gowild.mobile.libcommon.utils.ToastUtils;
import com.gowild.mobile.libp2sp.core.listener.IDataListener;
import com.gowild.mobile.libp2sp.netty.NettyClientManager;
import com.gowild.mobileclient.R;
import com.gowild.mobileclient.activity.base.GowildHasTitileActivity;
import com.gowild.mobileclient.config.ApplianceConstants;
import com.gowild.mobileclient.manager.ApplianceManager;
import com.gowild.mobileclient.misc.GowildResponseCode;
import com.gowild.mobileclient.protocol.StringMsgPro;

/**
 * @author Administrator
 * @version 1.0
 *          <p><strong>配对设备</strong></p>
 * @since 2016/8/5 9:13
 */
public class GowildApplianceSearchActivity extends GowildHasTitileActivity {

    // ===========================================================
    // HostConstants
    // ===========================================================

    private static final String TAG = GowildApplianceSearchActivity.class.getSimpleName();

    public static final String INTENT_EXTRA_POSITION = "INTENT_EXTRA_POSITION";

    public static final String INTENT_EXTRA_BRAND_NAME = "INTENT_EXTRA_BRAND_NAME";

    public static final String INTENT_EXTRA_SCENE = "INTENT_EXTRA_SCENE";

    public static final String INTENT_EXTRA_DEVICE_TYPE = "INTENT_EXTRA_DEVICE_TYPE";

    private int mTotalCount = 0;

    private ApplianceControlnResultListener mApplianceControlnResultListener = new ApplianceControlnResultListener();

    private View.OnClickListener mOnclickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.b_auto:
                    if (m_tag == 0) {// 开始
                        m_key = ApplianceConstants.Air.KEY_START;
                        m_auto.setText("设备有反应点我喔~~");
                        m_tag = 1;
                    } else if (m_tag == 1) {// 停止
                        m_key = ApplianceConstants.Air.KEY_STOP;
                        m_auto.setVisibility(View.GONE);
                        m_repeat.setVisibility(View.VISIBLE);
                        m_save.setVisibility(View.VISIBLE);
                        mUpBtn.setEnabled(true);
                        mDownBtn.setEnabled(true);
                        m_tag = 0;
                    } else {
                        break;
                    }
                    break;
                case R.id.bb_repeat:// 重新匹配
                    m_key = ApplianceConstants.Air.KEY_START;
                    // showHind();
                    m_repeat.setVisibility(View.GONE);
                    m_save.setVisibility(View.GONE);
                    mUpBtn.setEnabled(false);
                    mDownBtn.setEnabled(false);
                    m_auto.setVisibility(View.VISIBLE);
                    m_auto.setText("设备有反应点我喔~~");
                    m_tag = 1;
                    break;
                case R.id.b_save:// 保存匹配
                    m_key = ApplianceConstants.Air.KEY_SAVE;
                    sendToRobot(ApplianceConstants.Air.MESSAGE_COMMAND, true);
                    ToastUtils.showCustom(GowildApplianceSearchActivity.this, "保存成功", false);
                    startActivity(GowildApplianceListActivity.class);
                    break;
                case R.id.str_down:// 下一个
                    Log.d(TAG, "下一个");
                    m_key = ApplianceConstants.Air.KEY_NEXT;
                    break;
                case R.id.str_up:// 上一个
                    Log.d(TAG, "上一个");
                    m_key = ApplianceConstants.Air.KEY_PRE;
                    break;
                default:
                    return;
            }
            if (m_key != 3 && m_key != 0) {
                sendToRobot(ApplianceConstants.Air.MESSAGE_COMMAND, false);//发送指令
            }
        }
    };


    // ===========================================================
    // Static Fields
    // ===========================================================

    // ===========================================================
    // Fields
    // ===========================================================

    private int mPosition = -1;
    private int mScene = -1;
    private int mDeviceType = -1;
    private String mBrandName = null;

    private int m_key = 0;
    private TextView mDeviceCount = null;
    private TextView mRemainCount = null;
    private int m_tag = 0;
    private Button m_auto, m_repeat, m_save;
    private Button mUpBtn;
    private Button mDownBtn;
    private int mCurrentCount = 0;// 当前匹配项
    private int mCount = 0;

    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            String str = (String) msg.obj;
            try {
                JSONObject json = JSONObject.parseObject(str);
                //初始化的时候不返回
                if (json.containsKey("count")) {
                    mCurrentCount = json.getInteger("count");// 当前个数
                    mRemainCount.setText(String.valueOf(mCurrentCount));
                    if (mCurrentCount == mTotalCount) {
                        overdispose(mCurrentCount);
                    }
                }
                //开始匹配的时候不返回
                if (json.containsKey("mcount")) {
                    mTotalCount = json.getInteger("mcount");// 总个数
                    mDeviceCount.setText("此品牌共有" + "(" + String.valueOf(mTotalCount) + ")" + "种型号");
                }
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    };

    // ===========================================================
    // Constructors
    // ===========================================================


    // ===========================================================
    // Getter or Setter
    // ===========================================================


    // ===========================================================
    // Methods for/from SuperClass/Interfaces
    // ===========================================================

    @Override
    protected int onSetContentView() {
        return R.layout.activity_gowild_appliance_search;
    }

    @Override
    protected void initEvent() {
        setTitle("配对设备");
        mDeviceCount = (TextView) findViewById(R.id.t_all);
        mRemainCount = (TextView) findViewById(R.id.t_current);
        m_auto = (Button) findViewById(R.id.b_auto);
        m_repeat = (Button) findViewById(R.id.bb_repeat);
        m_save = (Button) findViewById(R.id.b_save);
        mUpBtn = (Button) findViewById(R.id.str_up);
        mDownBtn = (Button) findViewById(R.id.str_down);

        m_repeat.setOnClickListener(mOnclickListener);
        m_save.setOnClickListener(mOnclickListener);
        m_auto.setOnClickListener(mOnclickListener);
        m_auto.setOnClickListener(mOnclickListener);
        mUpBtn.setOnClickListener(mOnclickListener);
        mDownBtn.setOnClickListener(mOnclickListener);
    }

    @Override
    protected void initView() {
        super.initView();
        Bundle bundle = getIntent().getExtras();
        if (null != bundle) {
            mPosition = bundle.getInt(INTENT_EXTRA_POSITION);
            mBrandName = bundle.getString(INTENT_EXTRA_BRAND_NAME);
            mScene = bundle.getInt(INTENT_EXTRA_SCENE);
            mDeviceType = bundle.getInt(INTENT_EXTRA_DEVICE_TYPE);
        }
        sendToRobot(ApplianceConstants.Air.MESSAGE_INIT, false);
    }

    @Override
    protected void onPause() {
        super.onPause();
        NettyClientManager.getInstance().removeHandlerListener(GowildResponseCode.APPLIANCE_CONTROL_RESULT, mApplianceControlnResultListener);
        sendToRobot(ApplianceConstants.Air.MESSAGE_RETURN, false);
    }

    @Override
    public void onResume() {
        super.onResume();
        NettyClientManager.getInstance().addHandlerListener(GowildResponseCode.APPLIANCE_CONTROL_RESULT, mApplianceControlnResultListener);
        if (mCurrentCount != 0) {
            sendToRobot(ApplianceConstants.Air.MESSAGE_CONTINUE_MATCHING, false);
        }
    }

    // ===========================================================
    // Methods
    // ===========================================================

    /**
     * 发送tcp消息
     */
    public void sendToRobot(int messageType, boolean isCommit) {
        try {
            JSONObject result = new JSONObject();
            result.put("head", ApplianceConstants.Air.APPLIANCES_OPTION);//消息头200
            result.put("messageType", messageType);
            switch (messageType) {
                case ApplianceConstants.Air.MESSAGE_INIT:
                    result.put("stype", mDeviceType);// 代表是家电设备类型index
                    result.put("deviceName", mBrandName);// 选择的家电品牌名字
                    result.put("mitem", mScene);// 家电场景index
                    result.put("brandIndex", mPosition);// 选择的家电品牌index
                    ApplianceManager.getInstance().controlAppliance(result.toJSONString());
                    break;
                case ApplianceConstants.Air.MESSAGE_COMMAND:
                    result.put("key", m_key); // 手机端按键处理（开始匹配、停止匹配、上一步、下一步、保存）
                    if (isCommit) {
                        ApplianceManager.getInstance().controlAppliance(result.toJSONString());
                    } else {
                        ApplianceManager.getInstance().controlAppliance(result.toJSONString());
                    }
                    break;
                case ApplianceConstants.Air.MESSAGE_RETURN:
                    ApplianceManager.getInstance().addApplianceData(result.toJSONString());
                    break;
                case ApplianceConstants.Air.MESSAGE_CONTINUE_MATCHING:
                    result.put("mCurrValue", mCurrentCount); // 从第mCurrentCount个开始匹配（手机端要记录推出页面是mCurrentCount值）
                    ApplianceManager.getInstance().addApplianceData(result.toJSONString());
                    break;
                case ApplianceConstants.Air.MESSAGE_FINISH:
                    ApplianceManager.getInstance().addApplianceData(result.toJSONString());
                    break;
                default:
                    return;
            }
        } catch (Exception e) {
        }
    }


    public void showHind() {
        Log.d(TAG, "showhind");

        new AlertDialog.Builder(this)
                // .setTitle("Warning")
                .setMessage("是否继续匹配其它家电设备?").setNegativeButton("是", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                // Intent intent = new Intent();
                // intent.putExtra("FirstOwner", isFirst);
                // intent.setClass(OptionActivity.this,
                // DrvicesActivity.class);
                // startActivity(intent);
            }
        }).setPositiveButton("否", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Toast.makeText(GowildApplianceSearchActivity.this, "首次开机流程结束", Toast.LENGTH_SHORT).show();
                // sendInform();

                sendToRobot(ApplianceConstants.Air.MESSAGE_FINISH, false);
                // Intent intent = new Intent();
                // intent.setClass(OptionActivity.this,
                // LauncherActivity.class);
                // startActivity(intent);
                finish();
            }
        }).show();
    }

    // 处理所有型号匹配完成 但是没有匹配成功
    private void overdispose(int count) {
        if (count >= mCount) {
            m_auto.setVisibility(View.GONE);
            m_repeat.setVisibility(View.VISIBLE);
            m_save.setVisibility(View.VISIBLE);
            mUpBtn.setEnabled(true);
            mDownBtn.setEnabled(true);
            m_tag = 0;
            mCurrentCount = 0;
        }
    }

    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================

    /**
     * 添加电器   设备的类型
     */
    private class ApplianceControlnResultListener implements IDataListener<StringMsgPro.StringMsg> {

        @Override
        public void onReceiveData(StringMsgPro.StringMsg data) {
            Logger.e(TAG, "ApplianceControlnResultListener onReceiveData() data = " + data.getValue());
            //解析该品牌电器数量等信息
            String str = data.getValue();
            Message msg = mHandler.obtainMessage();
            msg.obj = str;
            mHandler.sendMessage(msg);
        }

        @Override
        public void onActiveChanged(boolean isActive) {
            Logger.e(TAG, "ApplianceControlnResultListener onActiveChanged() isActive = " + isActive);
        }
    }
}
