/**
 * Copyright (C)  2016 深圳市狗尾草智能科技有限公司
 * GowildXB
 * GowildApplianceListActivity.java
 */
package com.gowild.mobileclient.activity.appliances;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;

import com.gowild.mobile.libcommon.utils.Logger;
import com.gowild.mobile.libcommon.utils.ToastUtils;
import com.gowild.mobile.libp2sp.core.listener.IDataListener;
import com.gowild.mobile.libp2sp.netty.NettyClientManager;
import com.gowild.mobileclient.R;
import com.gowild.mobileclient.activity.base.GowildHasTitileActivity;
import com.gowild.mobileclient.adapter.ApplianceListAdapter;
import com.gowild.mobileclient.callback.OnNormalDialogListener;
import com.gowild.mobileclient.callback.OnToolBarListener;
import com.gowild.mobileclient.manager.ApplianceManager;
import com.gowild.mobileclient.misc.GowildResponseCode;
import com.gowild.mobileclient.model.ApplianceModel;
import com.gowild.mobileclient.protocol.ElectricInfoListPro;
import com.gowild.mobileclient.protocol.ElectricInfoPro;
import com.gowild.mobileclient.protocol.IntMsgPro;
import com.gowild.mobileclient.widget.dialog.GowildNormalDialog;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;

/**
 * @author Zhangct
 * @version 1.0
 *          <p>已经添加设备 家电首页</p>
 * @since 2016/8/3 17:57
 */
public class GowildApplianceListActivity extends GowildHasTitileActivity {

    // ===========================================================
    // HostConstants
    // ===========================================================

    private static final String TAG = GowildApplianceListActivity.class.getSimpleName();

    // ===========================================================
    // Static Fields
    // ===========================================================

    // ===========================================================
    // Fields
    // ===========================================================
    //设备数组
    public int devices_str[] = {R.string.app_text_air, R.string.app_text_tv, R.string.app_text_stb,
            R.string.app_text_pjt, R.string.app_text_fans, R.string.app_text_dvd};

    //场景数组
    public int scene_str[] = {R.string.app_text_parlor, R.string.app_text_abovestairs, R.string.app_text_low_bedroom,
            R.string.app_text_guest_bedroom, R.string.app_text_study};

    /**
     * 请求已经添加电器数据回调
     */
    private ApplianceSynnResultListener mApplianceSynnResultListener = new ApplianceSynnResultListener();
    /**
     * 删除已经添加电器回调
     */
    private ApplianceDelnResultListener mApplianceDelResultListener = new ApplianceDelnResultListener();

    @Bind(R.id.gowild_appliance_list_gv)
    GridView mGowildApplianceListGv;

    @Bind(R.id.appliance_tip)
    TextView mGowildApplianceTipTv;
    private int mDelPosition;

    /**
     * 请求回来的设备列表
     */
    private List<ApplianceModel> mList = new ArrayList<ApplianceModel>();

    /**
     * 已经添加设备列表适配器
     */
    private ApplianceListAdapter mAdapter = null;
    private GowildNormalDialog mDelDialog = null;

    /**
     * 添加新设备
     */
    private OnToolBarListener mListener = new OnToolBarListener() {
        @Override
        public void onActionClickListener() {
            Intent intent = new Intent(GowildApplianceListActivity.this, GowildApplianceOptionActivity.class);
            intent.putExtra(GowildApplianceOptionActivity.INTENT_EXTRA_ADDED, (Serializable) mList);
            startActivity(intent);
        }
    };

    /**
     * 进入设备控制界面
     */
    private AdapterView.OnItemClickListener mOnItemClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            ApplianceModel data = mList.get(position);
            Bundle bundle = new Bundle();
            bundle.putString(GowildApplianceAir.APPLIANCE_ID, data.getModelID() + "");
            bundle.putString(GowildApplianceAir.APPLIANCE_NIKENAME, data.getDisplayName());
            bundle.putInt(GowildApplianceAir.SCENE_TYPE_ID, data.getSceneID());
            startActivity(GowildApplianceAir.class, bundle);
        }
    };

    /**
     * 删除设备
     */
    private AdapterView.OnItemLongClickListener mItemLongClickListener = new AdapterView.OnItemLongClickListener() {

        @Override
        public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
            delAppliance(position);
            return true;
        }
    };

    private final int MESSAGE_INIT = 1;
    private final int MESSAGE_DEL = 2;
    private final int RESULT_SUCCESS = 1;

    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case MESSAGE_INIT:
                    ElectricInfoListPro.ElectricInfoListMsg data = (ElectricInfoListPro.ElectricInfoListMsg) msg.obj;
                    if(null == data){
                        mGowildApplianceTipTv.setVisibility(View.VISIBLE);
                        mGowildApplianceListGv.setVisibility(View.GONE);
                        mAdapter.notifyDataSetChanged();
                    }else {
                        mGowildApplianceTipTv.setVisibility(View.GONE);
                        mGowildApplianceListGv.setVisibility(View.VISIBLE);
                        List<ElectricInfoPro.ElectricInfoMsg> list = data.getValueList();
                        mList.clear();
                        dispost(list);
                        mAdapter.notifyDataSetChanged();
                    }
                    break;
                case MESSAGE_DEL:
                    //1成功；-1失败
                    int result = (int) msg.obj;
                    if (result == RESULT_SUCCESS) {
                        mList.remove(mDelPosition);
                        mAdapter.notifyDataSetChanged();
                        ToastUtils.showCustom(GowildApplianceListActivity.this, "删除成功", false);
                        if(mAdapter.getCount() == 0){
                            mGowildApplianceTipTv.setVisibility(View.VISIBLE);
                            mGowildApplianceListGv.setVisibility(View.GONE);
                        }
                        //删除成功
                    } else {
                        //删除失败
                        ToastUtils.showCustom(GowildApplianceListActivity.this, "删除失败", false);
                    }
//                    mAdapter.notifyDataSetChanged();
                    break;
            }
        }
    };

    // ===========================================================
    // Constructors
    // ===========================================================


    // ===========================================================
    // Getter or Setter
    // ===========================================================


    // ===========================================================
    // Methods for/from SuperClass/Interfaces
    // ===========================================================


    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        loadDataFormServe();
    }

    @Override
    protected void onResume() {
        super.onResume();
        //注册监听
//        NettyClientManager.getInstance().addHandlerListener(GowildResponseCode.APPLIANCE_SYN_RESULT, mApplianceSynnResultListener);
        NettyClientManager.getInstance().addHandlerListener(GowildResponseCode.APPLIANCE_DEL_RESULT, mApplianceDelResultListener);
        NettyClientManager.getInstance().addHandlerListener(GowildResponseCode.APPLIANCE_SYN_RESULT, mApplianceSynnResultListener);
//        NettyClientManager.getInstance().addHandlerListener(GowildResponseCode.APPLIANCE_CONTROL_RESULT, mApplianceDelResultListener);
    }

    @Override
    protected void onPause() {
        super.onPause();
        //注销监听
//        NettyClientManager.getInstance().removeHandlerListener(GowildResponseCode.APPLIANCE_SYN_RESULT, mApplianceSynnResultListener);
        NettyClientManager.getInstance().removeHandlerListener(GowildResponseCode.APPLIANCE_DEL_RESULT, mApplianceDelResultListener);
        NettyClientManager.getInstance().removeHandlerListener(GowildResponseCode.APPLIANCE_SYN_RESULT, mApplianceSynnResultListener);
//        NettyClientManager.getInstance().removeHandlerListener(GowildResponseCode.APPLIANCE_CONTROL_RESULT, mApplianceDelResultListener);
    }


    @Override
    protected int onSetContentView() {
        return R.layout.activity_gowild_appliance_list;
    }

    @Override
    protected void initEvent() {
        setActionListener(mListener);
        setTitle("家电");
        setActionDrawable(R.drawable.gowild_appliance_add_btn_selector);
    }

    @Override
    protected void initView() {
        super.initView();
        mAdapter = new ApplianceListAdapter(GowildApplianceListActivity.this, mList);
        mGowildApplianceListGv.setAdapter(mAdapter);
        mAdapter.notifyDataSetChanged();
        mGowildApplianceListGv.setOnItemClickListener(mOnItemClickListener);
        mGowildApplianceListGv.setOnItemLongClickListener(mItemLongClickListener);
    }

    @Override
    protected void loadData() {
        super.loadData();
        //从服务器请求电器数据
        loadDataFormServe();
    }

    // ===========================================================
    // Methods
    // ===========================================================

    /**
     * 从网络加载数据
     */
    private void loadDataFormServe() {
        //从服务端加载数据
//        ApplianceManager.getInstance().syncApplianceData();
        ApplianceManager.getInstance().syncApplianceData();

    }

    /**
     * 删除设备
     */
    private void delAppliance(final int position) {
        if (null == mDelDialog) {
            mDelDialog = new GowildNormalDialog();
            mDelDialog.setDialogContent(getStringById(R.string.gowild_setting_logout_confirm));
        }
        mDelDialog.setDialogContent("你确定要删除当前设备？");
        mDelDialog.setDialogCancel(getString(R.string.gowild_cancel));
        mDelDialog.setDialogConfirm(getString(R.string.gowild_ok));
        mDelDialog.setDialogListener(new OnNormalDialogListener() {
            @Override
            public void onCancelListener() {
                Toast.makeText(getApplicationContext(), "取消", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onConfirmListener() {
                mDelPosition = position;
                ApplianceModel data = mList.get(position);
                //删除服务器数据
                ApplianceManager.getInstance().delApplianceData(data.getDeviceType(), data.getSceneID());
            }
        });
        mDelDialog.show(getFragmentManager(), "mDelDialog");
    }

    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================

    /**
     * 同步设备
     */
    private class ApplianceSynnResultListener implements IDataListener<ElectricInfoListPro.ElectricInfoListMsg> {

        @Override
        public void onReceiveData(ElectricInfoListPro.ElectricInfoListMsg data) {
            Message msg = mHandler.obtainMessage();
            msg.what = MESSAGE_INIT;
            msg.obj = data;
            mHandler.sendMessage(msg);
        }

        @Override
        public void onActiveChanged(boolean isActive) {
            Logger.e(TAG, "ApplianceSynnResultListener onActiveChanged() isActive = " + isActive);
        }

    }

    /**
     * 删除设备回调
     */
    private class ApplianceDelnResultListener implements IDataListener<IntMsgPro.IntMsg> {

        @Override
        public void onReceiveData(IntMsgPro.IntMsg data) {
            int result = data.getValue();
            Message msg = mHandler.obtainMessage();
            msg.what = MESSAGE_DEL;
            msg.obj = result;
            mHandler.sendMessage(msg);
        }

        @Override
        public void onActiveChanged(boolean isActive) {

        }
    }




    /**
     * 初始化遥控器数据
     *
     * @param
     */
    public void dispost(List<ElectricInfoPro.ElectricInfoMsg> list) {
        for (int i = 0; i < list.size(); i++) {
            ApplianceModel item = new ApplianceModel();
            ElectricInfoPro.ElectricInfoMsg data = list.get(i);
            item.setDeviceType(data.getDeviceType());
            item.setBrandName(data.getBrandName());
            item.setModelID(data.getModelID());
            item.setSceneID(data.getSceneID());
            item.setDisplayName(getString(scene_str[data.getSceneID()]) + getString(devices_str[data.getDeviceType()]));
            mList.add(item);
        }
    }

}
