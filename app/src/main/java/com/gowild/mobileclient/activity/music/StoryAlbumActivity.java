/**
 * Copyright (C)  2016 深圳市狗尾草智能科技有限公司
 * GowildXB
 * StoryAlbumActivity.java
 */
package com.gowild.mobileclient.activity.music;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.gowild.mobile.libcommon.utils.ToastUtils;
import com.gowild.mobileclient.R;
import com.gowild.mobileclient.config.Constants;
import com.gowild.mobileclient.manager.GowildAsyncHttpResponseHandler;
import com.gowild.mobileclient.manager.GowildHttpManager;
import com.gowild.mobileclient.vo.ResponseStoryAlbum;
import com.loopj.android.http.RequestParams;


/**
 * @author ljd
 * @version 1.0
 *          <p><strong>Features draft description.主要功能介绍</strong></p>
 * @since 2016/8/10 17:09
 */
public class StoryAlbumActivity extends NetResourceListBaseActivity {

    // ===========================================================
    // HostConstants
    // ===========================================================

    private static final String TAG = StoryAlbumActivity.class.getSimpleName();

    // ===========================================================
    // Static Fields
    // ===========================================================

    // ===========================================================
    // Fields
    // ===========================================================
    private RequestStoryAlbumCallback mRequestStoryAlbumCallback;
    private boolean hasNext = true;
    private boolean hasPrevious = true;
    private int mCateGoryId;
    private String mKeyWord;
    private int mPageSize = 10;
    public static final int FROM_KEY_WORD = 2;
    public static final int FROM_SORT_LIST = 1;
    // ===========================================================
    // Constructors
    // ===========================================================


    // ===========================================================
    // Getter or Setter
    // ===========================================================


    // ===========================================================
    // Methods for/from SuperClass/Interfaces
    // ===========================================================


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    @Override
    public String getToobarTitle() {
        if (getIntent().getIntExtra(Constants.TYPE, 1) == 1) {
            String cateGory = getIntent().getStringExtra(Constants.INFOS);
            return cateGory;
        } else {
            String keyWord = getIntent().getStringExtra(Constants.INFOS);
            return keyWord;
        }

    }

    @Override
    public View getTipView() {
        TextView view = (TextView) LayoutInflater.from(this).
                inflate(R.layout.view_gowild_net_resource_top_hint, getRootView(), false);
        view.setText(R.string.gowild_search_result);
        return view;
    }

    @Override
    public void loadInfo(int pageNum) {
        mRequestStoryAlbumCallback = new RequestStoryAlbumCallback(this);
        if (getIntent().getIntExtra(Constants.TYPE, 1) == 1) {
            mCateGoryId = getIntent().getIntExtra(Constants.CATEGORY_ID, 0);
            showLoading(R.string.loading);
            GowildHttpManager.requestStoryAlbumByCategoryId(this, mCateGoryId, pageNum, mRequestStoryAlbumCallback);
        } else {
            showLoading(R.string.loading);
            mKeyWord = getIntent().getStringExtra(Constants.INFOS);
            GowildHttpManager.requestStoryAlbumFromKeyWord(this, new RequestParams(Constants.SEARCH_KEY, mKeyWord, Constants.PAGE_NUM, pageNum, Constants.PAGE_SIZE, mPageSize), mRequestStoryAlbumCallback);
        }


    }

    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================
    private class RequestStoryAlbumCallback extends GowildAsyncHttpResponseHandler<ResponseStoryAlbum> {
        public RequestStoryAlbumCallback(Context context) {
            super(context);
        }

        @Override
        public void onSuccess(int statusCode, ResponseStoryAlbum responseBody) {
            hideLoading();
            if (responseBody != null) {
                if (responseBody.storyAlbums.size() != 0) {
                    onLoadSuccess(responseBody.storyAlbums);
                }else{
                    ToastUtils.showCustom(getBaseContext(),R.string.gowild_no_more,false);
                    onLoadFailure();
                }
            } else {
                onLoadFailure();
            }
        }


        @Override
        public void onFailure(int statusCode, String responseBody) {
            hideLoading();
            onLoadFailure();
        }
    }
}
