/**
 * Copyright (C)  2016 深圳市狗尾草智能科技有限公司
 * GowildXB
 * GowildNeedBindRobotActivity.java
 */
package com.gowild.mobileclient.activity.login;

import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.gowild.mobile.libcommon.utils.CheckNetworkUtil;
import com.gowild.mobileclient.R;
import com.gowild.mobileclient.activity.PhotoAndMediaListActivity;
import com.gowild.mobileclient.base.baseactivity.GowildBaseActivity;
import com.gowild.mobileclient.config.Constants;
import com.gowild.mobileclient.manager.LoginMananger;
import com.gowild.mobileclient.utils.CommonDataUtil;
import com.gowild.mobileclient.widget.MyToolBar;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * @author ljd
 * @version 1.0
 *          <p><strong>Features draft description.主要功能介绍</strong></p>
 * @since 2016/8/17 11:11
 */
public class GowildNeedBindRobotActivity extends GowildBaseActivity {

    // ===========================================================
    // HostConstants
    // ===========================================================

    private static final String TAG = GowildNeedBindRobotActivity.class.getSimpleName();
    @Bind(R.id.tips)
    TextView tips;
    @Bind(R.id.gowild_tv_title)
    TextView mGowildTvTitle;
    @Bind(R.id.bind_robot)
    Button mBindRobot;
    @Bind(R.id.gowild_title_content)
    TextView mGowildTitleContent;
//    @Bind(R.id.title)
//    TextView title;
//    @Bind(R.id.enter)
//    TextView enter;


    // ===========================================================
    // Static Fields
    // ===========================================================

    // ===========================================================
    // Fields
    // ===========================================================
    private String[] mTitleArr;
    private String[] mTipsArr;
    private int mKey;

    // ===========================================================
    // Constructors
    // ===========================================================


    // ===========================================================
    // Getter or Setter
    // ===========================================================


    // ===========================================================
    // Methods for/from SuperClass/Interfaces
    // ===========================================================


    @Override
    protected void onResume() {
        super.onResume();
        mTitleArr = getResources().getStringArray(R.array.module_title);
        mTipsArr = getResources().getStringArray(R.array.module_tips);
        initTipsAndTitle();
    }

    @Override
    protected int onSetTopbarView() {
        return R.layout.view_gowild_toolbar;
    }

    @Override
    protected int onSetContentView() {
        return R.layout.activity_gowild_need_bindrobot;
    }

    @Override
    protected int onSetTopBarHeight() {
        return getResources().getDimensionPixelSize(R.dimen.gowild_toolbar_height);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
    }

    // ===========================================================
    // Methods
    // ===========================================================
    private void initTipsAndTitle() {
        MyToolBar myToolBar = (MyToolBar) findViewById(R.id.view_toolbar);
        mKey = getIntent().getIntExtra(Constants.TYPE, 0);
        if (mKey == Constants.ALARM_OUTLINE) {
            myToolBar.setTitle(mTitleArr[1]);
            tips.setText(mTipsArr[1]);
            if (CheckNetworkUtil.isNetworkAvailable(this)) {
                mGowildTvTitle.setText(String.format(getString(R.string.gowild_xb_netoutline), CommonDataUtil.getCurrentRobotTitle()));
                mGowildTitleContent.setText(String.format(getString(R.string.gowild_xb_retry), CommonDataUtil.getCurrentRobotTitle()));
                mBindRobot.setText("现在去设置！");
            } else {
                mGowildTvTitle.setText("检测到手机的网络异常");
                mGowildTitleContent.setText("请检查手机的网络后重试");
                mBindRobot.setText("现在去设置！");
            }
        } else {
            myToolBar.setTitle(mTitleArr[mKey]);
            if (CheckNetworkUtil.isNetworkAvailable(this)) {
                mGowildTvTitle.setText("没有检测到与账户绑定的" + CommonDataUtil.getCurrentRobotTitle() + "，");
                mGowildTitleContent.setText("请绑定" + CommonDataUtil.getCurrentRobotTitle() + "后重试");
                mBindRobot.setText("现在去绑定！");
            } else {
                mBindRobot.setText("现在去设置！");
                mGowildTvTitle.setText("检测到手机的网络异常");
                mGowildTitleContent.setText("请检查手机的网络后重试");
            }
            tips.setText(mTipsArr[mKey]);
        }
    }


    @OnClick({R.id.bind_robot})
    public void onClick(View view) {
        Intent intent;
        switch (view.getId()) {
            case R.id.back:
                finish();
                break;
            case R.id.enter:
                intent = new Intent(getBaseContext(), PhotoAndMediaListActivity.class);
                startActivity(intent);
                break;
            case R.id.bind_robot:
                if (CheckNetworkUtil.isNetworkAvailable(this)) {
                    LoginMananger.getInstence().setBindFrom(1);
                } else {
                    intent = new Intent();
                    intent.setAction(Settings.ACTION_WIFI_SETTINGS);
                    startActivity(intent);
                    return;
                }
                intent = new Intent(getBaseContext(), com.gowild.mobileclient.activity.setting.GowildWifiConfigActivity.class);
                startActivity(intent);

                break;
        }
    }

// ===========================================================
// Inner and Anonymous Classes
// ===========================================================

}
