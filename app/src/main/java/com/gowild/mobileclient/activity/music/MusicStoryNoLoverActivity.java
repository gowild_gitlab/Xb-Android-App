/**
 * Copyright (C)  2016 深圳市狗尾草智能科技有限公司
 * GowildXB
 * MusicStoryNoLoverActivity.java
 */
package com.gowild.mobileclient.activity.music;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.gowild.mobileclient.R;
import com.gowild.mobileclient.base.baseactivity.GowildBaseActivity;
import com.gowild.mobileclient.config.IntContants;
import com.gowild.mobileclient.utils.CommonDataUtil;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * @author ljd
 * @version 1.0
 *          <p><strong>Features draft description.主要功能介绍</strong></p>
 * @since 2016/8/26 10:15
 */
public class MusicStoryNoLoverActivity extends GowildBaseActivity {

    // ===========================================================
    // HostConstants
    // ===========================================================

    private static final String TAG = MusicStoryNoLoverActivity.class.getSimpleName();
    @Bind(R.id.title)
    TextView title;
    @Bind(R.id.music_container)
    LinearLayout musicContainer;
    @Bind(R.id.story_container)
    LinearLayout storyContainer;
    @Bind(R.id.music_tips)
    TextView musicTips;
    @Bind(R.id.story_tips)
    TextView storyTips;
    @Bind(R.id.music_tips_two)
    TextView mMusicTipsTwo;
    @Bind(R.id.music_tips_one)
    TextView mMusicTipsOne;
    @Bind(R.id.btn_add_music)
    Button mBtnAddMusic;
    @Bind(R.id.gowild_stroy_two)
    TextView mGowildStroyTwo;
    @Bind(R.id.gowild_stroy_one)
    TextView mGowildStroyOne;
    @Bind(R.id.btn_add_story)
    Button mBtnAddStory;


    // ===========================================================
    // Static Fields
    // ===========================================================

    // ===========================================================
    // Fields
    // ===========================================================


    // ===========================================================
    // Constructors
    // ===========================================================


    // ===========================================================
    // Getter or Setter
    // ===========================================================


    // ===========================================================
    // Methods for/from SuperClass/Interfaces
    // ===========================================================


    // ===========================================================
    // Methods
    // ===========================================================
    @Override
    protected int onSetTopbarView() {
        return R.layout.activity_gowild_music_story_no_lover_topbar;
    }

    @Override
    protected int onSetContentView() {
        return R.layout.activity_gowild_music_story_no_lover;
    }

    @Override
    protected int onSetTopBarHeight() {
        return getResources().getDimensionPixelOffset(R.dimen.gowild_160px);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (IntContants.isCurrentMusic()) {
            title.setText(R.string.gowild_music_lover_title);
            storyContainer.setVisibility(View.GONE);
            musicContainer.setVisibility(View.VISIBLE);
            mMusicTipsOne.setText(CommonDataUtil.getCurrentRobotTitle() + "也会喜欢上这首歌，并记录在"+CommonDataUtil.getCurrentRobotTitle()+"的脑壳里面及在此登记在册。");
            mMusicTipsTwo.setText("在我"+CommonDataUtil.getCurrentRobotTitle()+"正在开心愉快唱歌的时候，你只要对"+CommonDataUtil.getCurrentRobotTitle()+"说，“"+CommonDataUtil.getCurrentRobotTitle()+"，我喜欢这首歌”之类的话语。");
            SpannableStringBuilder builder = new SpannableStringBuilder("当然，只要你喜欢，你肯定也是可以点击下面的【+添加喜欢的歌】来把它刻印在"+CommonDataUtil.getCurrentRobotTitle()+"的脑壳里面并在此登记成册。");
            builder.setSpan(new ForegroundColorSpan(Color.RED), 21, 30, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            musicTips.setText(builder);
        } else {
            title.setText(R.string.gowild_story_lover_title);
            musicContainer.setVisibility(View.GONE);
            storyContainer.setVisibility(View.VISIBLE);
            mMusicTipsOne.setText(CommonDataUtil.getCurrentRobotTitle() + "也会喜欢上这个故事，并记录在"+CommonDataUtil.getCurrentRobotTitle()+"的脑壳里面及在此登记在册。");
            mMusicTipsTwo.setText("在我"+CommonDataUtil.getCurrentRobotTitle()+"正在开心愉快讲故事的时候，你只要对"+CommonDataUtil.getCurrentRobotTitle()+"说，“"+CommonDataUtil.getCurrentRobotTitle()+"，我喜欢这个故事”之类的话语。");
            SpannableStringBuilder builder = new SpannableStringBuilder("当然，只要你喜欢，你肯定也是可以点击下面的【+添加喜欢的故事】来把它刻印在"+CommonDataUtil.getCurrentRobotTitle()+"的脑壳里面并在此登记成册。");
            builder.setSpan(new ForegroundColorSpan(Color.RED), 21, 31, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            storyTips.setText(builder);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
    }

    @OnClick({R.id.btn_add_music, R.id.btn_add_story, R.id.back})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.back:
                finish();
                break;
            case R.id.btn_add_music:
            case R.id.btn_add_story:
                Intent intent = new Intent(getBaseContext(), MusicSearchActivity.class);
                startActivity(intent);
                finish();
                break;
        }
    }

    @Override
    public void onBackPressed() {
        finish();
    }
    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================

}
