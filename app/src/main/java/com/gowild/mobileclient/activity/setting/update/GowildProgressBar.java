package com.gowild.mobileclient.activity.setting.update;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.view.View;

import java.math.BigDecimal;

public class GowildProgressBar extends View {
	android.widget.TextView t;
	private int max = 0;
	private Paint mPaint = null;
	private int maxHeight = 20;
	private float right = 0;

	public GowildProgressBar(Context context) {
		super(context);
		mPaint = new Paint();
		mPaint.setAntiAlias(true);
		mPaint.setStyle(Style.FILL);
		mPaint.setStrokeWidth(maxHeight);
	}

	@Override
	public void onDraw(Canvas canvas) {
		mPaint.setColor(Color.GRAY);
		canvas.drawRect(0, 0, getWidth(), maxHeight, mPaint);
		mPaint.setColor(Color.BLUE);
		canvas.drawRect(0, 0, right, maxHeight, mPaint);
	}

	public void setMax(int max) {
		this.max = max;
	}

	public void setProgress(int progress) {
		if (max > 0) {
			BigDecimal max = new BigDecimal(Float.toString(this.max));
			BigDecimal pros = new BigDecimal(Float.toString(progress));
			BigDecimal wid = new BigDecimal(Float.toString(getWidth()));
			right = pros.multiply(wid).divide(max, BigDecimal.ROUND_CEILING)
					.floatValue();
		}
		invalidate();
	}
}
