/**
 * Copyright (C)  2016 深圳市狗尾草智能科技有限公司
 * GowildXB
 * GowildStartActivity.java
 */
package com.gowild.mobileclient.activity.login;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.text.TextUtils;

import com.gowild.mobile.libcommon.utils.CheckNetworkUtil;
import com.gowild.mobile.libcommon.utils.SpUtils;
import com.gowild.mobile.libcommon.utils.ToastUtils;
import com.gowild.mobileclient.R;
import com.gowild.mobileclient.activity.GowildHomeActivity;
import com.gowild.mobileclient.activity.GowildSplashActivity;
import com.gowild.mobileclient.activity.setting.update.UpdateManager;
import com.gowild.mobileclient.base.baseactivity.GowildBaseActivity;
import com.gowild.mobileclient.base.widget.GowildLoadingDialog;
import com.gowild.mobileclient.callback.OnUpdateDoneListener;
import com.gowild.mobileclient.config.Constants;
import com.gowild.mobileclient.config.IntContants;
import com.gowild.mobileclient.event.LoginResult;
import com.gowild.mobileclient.manager.LoginMananger;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.File;

/**
 * @author shuai. wang
 * @version 1.0
 *          <p><strong>Features draft description.主要功能介绍</strong></p>
 * @since 2016/8/16 15:02
 */
public class GowildStartActivity extends GowildBaseActivity {

    // ===========================================================
    // HostConstants
    // ===========================================================

    private static final String TAG = GowildStartActivity.class.getSimpleName();

    private Runnable mRunnable;
    //先检查系统版本是否需要升级
    private UpdateManager mUpdateManager = null;
    private Runnable mR;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initView();
        initEvent();
    }

    private void initEvent() {
        mR = new Runnable() {
            @Override
            public void run() {
                mLoadingDialog = null;
                nextActivtiy(GowildHomeActivity.class, false);
            }
        };
        mHandler.postDelayed(mR, 3000);
    }

    protected void initView() {
        mLoadingDialog = new GowildLoadingDialog(this);
        mLoadingDialog.setOnBackPressedListener(new GowildLoadingDialog.OnBackPressedListener() {
                                                    @Override
                                                    public void onDialogBackPressed() {
                                                        onBackPressed();
                                                    }
                                                }
        );
        mUpdateManager = new UpdateManager(this);
        mUpdateManager.setOnUpdateDoneListener(new OnUpdateDoneListener() {

            @Override
            public void exitApplication() {
                finish();
            }

            @Override
            public void checkDone() {
                mHandler.removeCallbacks(mR);
            }

            @Override
            public void cancel() {
                nextActivtiy(GowildHomeActivity.class, true);
            }

            @Override
            public void isNewVersion() {
                //已经是最新版本
                mHandler.removeCallbacks(mR);
                nextActivtiy(GowildHomeActivity.class, true);

            }

            @Override
            public void install(String file) {
                // 安装apk
                mHandler.removeCallbacks(mR);
                Intent intent = new Intent("android.intent.action.VIEW");
                intent.addCategory("android.intent.category.DEFAULT");
                intent.setDataAndType(Uri.fromFile(new File(file)),
                        "application/vnd.android.package-archive");
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                EventBus.getDefault().post("finish");
                startActivityForResult(intent, 0);
            }
        });

        tcpLogin();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 0) {
            if (resultCode == RESULT_CANCELED) {
                // 进入主界面 安装被终止
                if (mUpdateManager.isForceUpdate) {
                    //如果是强制安装,就要关闭
                    finish();
                } else {
                    //如果不是强制安装,打开首页
                    //6.0以下啥事也不干,才对,4.0,5.0版本不受影响
                    nextActivtiy(GowildHomeActivity.class, true);
                }
            }
        }
    }


    private void tcpLogin() {
        String token = SpUtils.getString(GowildStartActivity.this, Constants.ACCESS_TOKEN_NOBEAR);
        if (!TextUtils.isEmpty(token) && SpUtils.getBoolean(this, Constants.REGISTER)) {
            if (CheckNetworkUtil.isNetworkAvailable(this)) {
                LoginMananger.getInstence().TCPLoginRequest();
            } else {
                ToastUtils.showCustom(this, R.string.gowild_net_error, false); //没有网络连接
                nextActivtiy(GowildHomeActivity.class, false);
            }
        } else {
            nextActivtiy(GowildSplashActivity.class, true);
            SpUtils.putBoolean(this, Constants.FRIST_LUNCH, true);
        }
    }


    @Override
    protected int onSetTopbarView() {
        return 0;
    }

    @Override
    protected int onSetContentView() {
        return R.layout.activity_gowild_start;
    }

    @Override
    protected int onSetTopBarHeight() {
        return 0;
    }


    private Handler mHandler = new Handler();


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void subLoginResult(LoginResult result) {
        mHandler.removeCallbacks(mR);
        switch (result.type) {
            case IntContants.TYPE_ROBOT_SUCESS:
            case IntContants.TYPE_ROBOT_OUTLINE:
            case IntContants.TYPE_ROBOT_NO_ROBOT:
                mUpdateManager.checkUpdateInfo();
                break;
            case -2:
                //token过期
                LoginMananger.getInstence().startRefreshToken();
                break;
            default:
                break;
        }
    }


    /**
     * 开启下一个activity,下一步
     */
    public void nextActivtiy(final Class clazz, boolean isNetError) {
        final Intent intent = new Intent(GowildStartActivity.this, clazz);
        intent.putExtra(Constants.CLASS, TAG);
        if (!isNetError) {
            intent.putExtra(Constants.NET_ERROR, Constants.NET_ERROR);
        }
        mRunnable = new Runnable() {
            @Override
            public void run() {
                startActivity(intent);
                finish();
            }
        };
        mHandler.postDelayed(mRunnable, 1000);
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        mUpdateManager= null;
        mHandler.removeCallbacks(mRunnable);
        mHandler.removeCallbacks(mR);
    }

}
