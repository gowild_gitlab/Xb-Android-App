/**
 * Copyright (C)  2016 深圳市狗尾草智能科技有限公司
 * GowildXB
 * GowildHomeActivity.java
 */
package com.gowild.mobileclient.activity;

import android.animation.ValueAnimator;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.RotateAnimation;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.Gallery;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.gowild.mobile.libcommon.utils.CheckNetworkUtil;
import com.gowild.mobile.libcommon.utils.SpUtils;
import com.gowild.mobile.libcommon.utils.ToastUtils;
import com.gowild.mobile.libp2sp.HostConstants;
import com.gowild.mobileclient.R;
import com.gowild.mobileclient.activity.alertandclock.GowildAlertOrClockActivity;
import com.gowild.mobileclient.activity.appliances.GowildApplianceListActivity;
import com.gowild.mobileclient.activity.base.GowildNoTitileActivity;
import com.gowild.mobileclient.activity.login.GowildNeedBindRobotActivity;
import com.gowild.mobileclient.activity.love.GowildContantsActivity;
import com.gowild.mobileclient.activity.love.GowildLoverActivity;
import com.gowild.mobileclient.activity.music.MusicMainActivity;
import com.gowild.mobileclient.activity.setting.GowildSettingActivity;
import com.gowild.mobileclient.activity.video.GowildAVMonitorActivity;
import com.gowild.mobileclient.config.Constants;
import com.gowild.mobileclient.config.IntContants;
import com.gowild.mobileclient.event.BindResult;
import com.gowild.mobileclient.event.LoginResult;
import com.gowild.mobileclient.event.PetTaskAndGorwTask;
import com.gowild.mobileclient.event.RobotOnline;
import com.gowild.mobileclient.event.UnbindEvent;
import com.gowild.mobileclient.event.Update;
import com.gowild.mobileclient.manager.GowildAsyncHttpResponseHandler;
import com.gowild.mobileclient.manager.GowildHttpManager;
import com.gowild.mobileclient.manager.LoginMananger;
import com.gowild.mobileclient.utils.CommonDataUtil;
import com.gowild.mobileclient.vo.BindLoverInfo;
import com.gowild.mobileclient.vo.PetInfo;
import com.gowild.mobileclient.vo.RobotInfo;
import com.gowild.mobileclient.widget.CircleProgressBar;
import com.loopj.android.http.RequestParams;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import butterknife.Bind;
import butterknife.OnClick;


/**
 * @author shuai. wang
 * @version 1.0
 *          <p><strong>Features draft description.主页</strong></p>
 * @since 2016/7/19 9:31
 */
public class GowildHomeActivity extends GowildNoTitileActivity {

    private static final int REQUEST_CODE_SETTING = 1;
    // ===========================================================
    // HostConstants
    // ===========================================================
    @Bind(R.id.gowild_home_lyt_bottom)
    LinearLayout mGowildHomeLytBottom;
    @Bind(R.id.gowild_home_setting_btn)
    Button mGowildHomeSettingBtn;
    @Bind(R.id.gowild_home_title)
    TextView mGowildHomeTitle;
    @Bind(R.id.gowild_home_net_error)
    TextView mGowildHomeNetError;
    @Bind(R.id.gowild_home_rank)
    TextView mGowildHomeRank;
    @Bind(R.id.gowild_home_tv_exp)
    TextView mGowildHomeTvExp;
    @Bind(R.id.gowild_home_fly_top)
    RelativeLayout mGowildHomeFlyTop;
    @Bind(R.id.gowild_home_tv_level)
    TextView mGowildHomeTvLevel;
    @Bind(R.id.iv_home_setting)
    ImageView mIvHomeSetting;
    @Bind(R.id.gowild_home_iv_like)
    ImageView mivhomelike;
    @Bind(R.id.gowild_home_cc)
    CircleProgressBar mGowildHomeCc;
    @Bind(R.id.gowild_home_gl_iv)
    Gallery mGowildHomeGlIv;//技能
    @Bind(R.id.gowild_home_iv_bot)
    ImageView mGowildHomeIvBot;

    @Bind(R.id.gowild_home_rl)
    RelativeLayout mGowildHomerl;
    @Bind(R.id.gowild_home_top_ll)
    LinearLayout mGowildHomeTopll;
    @Bind(R.id.gowild_av_monitor_btn)
    ImageView mAVMonitorBtn;
    @Bind(R.id.iv_robot_icon)
    ImageView robotIcon;

    private boolean mIsTaskShow = false;
    public static int sex;
    // ===========================================================
    // Fields
    // ===========================================================
    private int[] galleypics = {R.drawable.gowild_home_medal_big, R.drawable.gowild_home_task, R.drawable.gowild_home_cup_big};
    private String[] galleytitles;
    private String[] mAlltaskInfo = {"0/0", "0/0", "0/0"};

    // ===========================================================
    // Static Fieldskg
    // ===========================================================
    private static final String TAG = GowildHomeActivity.class.getSimpleName();
    private static final int HOME_MEDAL = 0;
    private static final int HOME_TASK = 1;
    private static final int HOME_SKILL = 2;
    private boolean mIsXBBind;
    private int mType;
    private String misson;
    private String honor;
    private boolean showRedTaskPoint = true;


    //底部箭头
    private Animation mAnimationRotateDown = null;
    private Animation mAnimationRotateUp = null;
    private Animation mAnimationOut = null;
    private Animation mAnimationIn = null;
    private Animation animation = null;
    private View invisiblView;
    private View visibleView;
    private String mSkillAll;
//    private ConnectionChangeReceiver myReceiver; //服务器的设定导致不能监听实时变化

    // ===========================================================
    // Methods for/from SuperClass/Interfaces
    // ===========================================================
    @Override
    public int getLayout() {
        return R.layout.activity_gowild_home;
    }


    @Override
    protected void initView() {
        initTalentArrowAnimation();
        String net = getIntent().getStringExtra(Constants.NET_ERROR);
        if (net != null) {
            initViewNetAvaliable();
            return;
        }
        mType = LoginMananger.getInstence().getType();
        switch (mType) {
            case IntContants.TYPE_ROBOT_SUCESS:
                loadDataFromNet();
                initViewNetAvaliable();
                break;
            case IntContants.TYPE_ROBOT_OUTLINE:
                initViewNetInvaliable();
                loadDataFromNet();
                break;
            case IntContants.TYPE_ROBOT_NO_ROBOT:
                initXBNoBind();
                break;
            default:
                break;
        }
    }

    @Override
    protected void initData() {
        galleytitles = new String[]{getResources().getString(R.string.gowild_home_medal), getResources().getString(R.string.gowild_home_task), getResources().getString(R.string.gowild_home_cup)};
    }

    @Override
    protected void initEvent() {

        mGowildHomeGlIv.setCameraDistance(90);
        mGowildHomeGlIv.setSpacing(50);
        mGowildHomeGlIv.setClipChildren(true);
        mGowildHomeGlIv.setUnselectedAlpha(0.5f);
        mGowildHomeGlIv.setSpacing(getResources().getDimensionPixelOffset(R.dimen.gowild_100px));
        mGowildHomeGlIv.setOnItemSelectedListener(mHomeOnItemSelectedListener);//条目选中的回调,他将调动下面的
        mGowildHomeGlIv.setAdapter(mHomeAdapter);
        mGowildHomeGlIv.setSelection(HOME_TASK);//注意此时触发事件
        mGowildHomeGlIv.setOnItemClickListener(mGalleyOnItemClick);
//        //开启网络监听的广播
//        IntentFilter filter =new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
//        myReceiver=new ConnectionChangeReceiver();
//        this.registerReceiver(myReceiver, filter);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        switch (LoginMananger.getInstence().getBindFrom()) {
            case Constants.ALARM:
                nextActivtiy(GowildAlertOrClockActivity.class, Constants.ALARM);
                break;
            case Constants.HOME:
                nextActivtiy(GowildApplianceListActivity.class, Constants.HOME);
                break;
            case Constants.MUSIC:
                IntContants.sCurrentResourceType = IntContants.TYPE_MUSIC;
                nextActivtiy(MusicMainActivity.class, Constants.MUSIC);
                break;
            case Constants.STORY:
                IntContants.sCurrentResourceType = IntContants.TYPE_STORY;
                nextActivtiy(MusicMainActivity.class, Constants.STORY);
                break;
            case Constants.AV:
                goMonitor();
                break;
        }
        LoginMananger.getInstence().setBindFrom(LoginMananger.bindFromClear);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mGowildHomeGlIv.setSelection(1);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
//        unregisterReceiver(myReceiver);
    }

    // ===========================================================
    // Methods
    // ===========================================================

    /**
     * 网络不可用
     */
    @SuppressWarnings("deprecation")
    private void initViewNetInvaliable() {
        mIsXBBind = false;
        mGowildHomeGlIv.setVisibility(View.GONE);
        mGowildHomeIvBot.setClickable(true);
        mivhomelike.setClickable(true);
        mGowildHomerl.setVisibility(View.VISIBLE);
        mGowildHomeTvLevel.setVisibility(View.VISIBLE);
        mGowildHomeTvExp.setVisibility(View.VISIBLE);
        mGowildHomeTitle.setVisibility(View.VISIBLE);
        mGowildHomeTopll.setVisibility(View.GONE);
        mGowildHomeLytBottom.setVisibility(View.INVISIBLE);
        mGowildHomeTitle.setText(getResources().getString(R.string.gowild_home_title_grey));//title
        mGowildHomeIvBot.setImageResource(R.drawable.gowild_home_bot_grey);
        mGowildHomeTitle.setTextColor(getResources().getColor(R.color.home_title_grey));
        mGowildHomeTvLevel.setTextColor(getResources().getColor(R.color.home_title_grey));
        mIvHomeSetting.setBackgroundResource(R.drawable.gowild_home_setting_grey);
        mivhomelike.setImageResource(R.drawable.gowild_home_like_click);
        mGowildHomeRank.setTextColor(getResources().getColor(R.color.home_title_grey));
        mGowildHomeTvExp.setTextColor(getResources().getColor(R.color.home_title_grey));
        //开始设置圆环
        mGowildHomeCc.setProgressStartColor(getResources().getColor(R.color.circle_end_color_grey));
        mGowildHomeCc.setProgressEndColor(getResources().getColor(R.color.circle_start_color_grey));
        mGowildHomeCc.setProgressBackgroundColor(getResources().getColor(R.color.circle_start_color_grey));
        //结束
        mGowildHomeFlyTop.setVisibility(View.VISIBLE);
        mGowildHomeNetError.setText(String.format("网络差的话%s会变笨，请重新设置网络，帮%s联网吧。", CommonDataUtil.getCurrentRobotTitle(), CommonDataUtil.getCurrentRobotTitle()));
    }

    /**
     * 网络可用
     */
    @SuppressWarnings("deprecation")
    private void initViewNetAvaliable() {
        mIsXBBind = true;
        mGowildHomerl.setVisibility(View.VISIBLE);
        mGowildHomeTvLevel.setVisibility(View.VISIBLE);
        mGowildHomeTvExp.setVisibility(View.VISIBLE);
        mGowildHomeTitle.setVisibility(View.VISIBLE);
        mGowildHomeTopll.setVisibility(View.GONE);
        mGowildHomeRank.setVisibility(View.VISIBLE);
//        mGowildHomeLytBottom.setVisibility(View.GONE);
//        mGowildHomeGlIv.setVisibility(View.VISIBLE);
        mGowildHomeTitle.setTextColor(getResources().getColor(R.color.default_blue));
        mIvHomeSetting.setBackgroundResource(R.drawable.gowild_home_setting);
        mivhomelike.setImageResource(R.drawable.gowild_home_like);
        mGowildHomeRank.setTextColor(getResources().getColor(R.color.default_blue));
        mGowildHomeTvExp.setTextColor(getResources().getColor(R.color.default_blue));
        mGowildHomeTvLevel.setTextColor(getResources().getColor(R.color.default_blue));
        mGowildHomeIvBot.setVisibility(View.VISIBLE);
        mGowildHomeIvBot.setImageResource(R.drawable.gowild_home_funtion);
        //开始设置圆环
        mGowildHomeCc.setProgressStartColor(getResources().getColor(R.color.circle_end_color));
        mGowildHomeCc.setProgressEndColor(getResources().getColor(R.color.circle_start_color));
        mGowildHomeCc.setProgressBackgroundColor(getResources().getColor(R.color.circle_back_color));
        //结束
        mGowildHomeFlyTop.setVisibility(View.GONE);
//        startAnimotion();
    }

    /**
     * 开始动画
     * 调用此方法显示动画
     *
     * @param percent 百分比
     */
    private void simulateProgress(float percent) {

        ValueAnimator animator = ValueAnimator.ofInt(0, (int) (percent + 0.5));//进度控制
        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                Object value = animation.getAnimatedValue();
                int progress = Integer.parseInt(value.toString());
                mGowildHomeCc.setProgress(progress);
            }
        });
        animator.setRepeatCount(0);
        animator.setDuration(1000);
        animator.start();
    }


    private void loadDataFromNet() {
        if (!mLoadingDialog.isShowing()) {
            showLoading("加载中");
        }
        GowildHttpManager.requestPetInfo(this, new GowildAsyncHttpResponseHandler<PetInfo>(this) {

            @Override
            public void onSuccess(int statusCode, PetInfo responseBody) {
                hideLoading();
                updataView(responseBody);
            }

            @Override
            public void onFailure(int statusCode, String responseBody) {
                hideLoading();
            }
        });
    }

    public boolean showRedSkillPoint;


    private long exitTime = 0;

    @Override
    public void onBackPressed() {
        if ((System.currentTimeMillis() - exitTime) > 5000) {
            ToastUtils.showCustom(this, getString(R.string.gowild_exit_app), false);
            exitTime = System.currentTimeMillis();
        } else {
            LoginMananger.getInstence().stopTCP();
            finish();
            android.os.Process.killProcess(android.os.Process.myPid());
            System.exit(0);
        }
    }

    /**
     * 点击事件
     */
    @OnClick({R.id.iv_home_setting, R.id.gowild_home_iv_like, R.id.gowild_home_iv_bot, R.id.gowild_home_setting_btn,
            R.id.gowild_home_bind, R.id.gowild_home_appliance_iv, R.id.iv_music, R.id.iv_photo, R.id.iv_story, R.id.iv_robot_icon, R.id.iv_alert, R.id.gowild_av_monitor_btn})
    public void onclick(View view) {
        switch (view.getId()) {
            case R.id.gowild_home_iv_bot:
                //执行动画
                startAnimotion();//执行按钮
                //执行view的滚动动画
                break;
            case R.id.gowild_home_setting_btn:
                nextActivtiy(com.gowild.mobileclient.activity.setting.GowildWifiConfigActivity.class);
                break;
            case R.id.gowild_home_iv_like:
                isBindlover();
                break;
            case R.id.gowild_home_appliance_iv:
                nextActivtiy(GowildApplianceListActivity.class, Constants.HOME);
                break;
            case R.id.iv_music:
                IntContants.sCurrentResourceType = IntContants.TYPE_MUSIC;
                nextActivtiy(MusicMainActivity.class, Constants.MUSIC);
                break;
            case R.id.iv_story:
                IntContants.sCurrentResourceType = IntContants.TYPE_STORY;
                nextActivtiy(MusicMainActivity.class, Constants.STORY);
                break;
            case R.id.iv_photo:
                startActivity(new Intent(this, PhotoAndMediaListActivity.class)
                        .putExtra(Constants.BPHOTO, true));
                break;
            case R.id.iv_alert:
                if (CheckNetworkUtil.isNetworkAvailable(this)){
                    if (LoginMananger.getInstence().getType() == 2) {
                        Intent intent = new Intent(GowildHomeActivity.this, GowildNeedBindRobotActivity.class);
                        intent.putExtra(Constants.TYPE, Constants.ALARM_OUTLINE);
                        startActivity(intent);
                    } else {
                        nextActivtiy(GowildAlertOrClockActivity.class, Constants.ALARM);
                    }
                }else{
                    Intent intent = new Intent();
                    intent.setClass(this, GowildNeedBindRobotActivity.class);
                    LoginMananger.getInstence().setBindFrom(Constants.ALARM_OUTLINE);
                    intent.putExtra(Constants.TYPE, Constants.ALARM_OUTLINE);
                    startActivity(intent);
                }

                break;
            case R.id.iv_robot_icon:
                requestRobotInfoAndStartWeb();
                break;
            case R.id.gowild_av_monitor_btn:
                goMonitor();
                break;
            case R.id.iv_home_setting:
                Intent settingIntent = new Intent(this, GowildSettingActivity.class);
                startActivityForResult(settingIntent, REQUEST_CODE_SETTING);
                break;
            case R.id.gowild_home_bind:
                nextActivtiy(com.gowild.mobileclient.activity.setting.GowildWifiConfigActivity.class);
                break;
        }
    }

    private void requestRobotInfoAndStartWeb() {
        GowildHttpManager.getRobotInfo(getBaseContext(), new RequestParams(), new GowildAsyncHttpResponseHandler<RobotInfo>(this) {
            @Override
            public void onSuccess(int statusCode, RobotInfo responseBody) {
                if (responseBody != null) {
                    if (responseBody.bindStatus == 0) {
                        Intent intent = new Intent(getBaseContext(), GowildNeedBindRobotActivity.class);
                        intent.putExtra(Constants.TYPE, Constants.WEB);
                        startActivity(intent);
                    } else {
                        Intent intentUrl = new Intent(getBaseContext(), GowildWebActivity.class);
                        String token = SpUtils.getString(getBaseContext(), Constants.ACCESS_TOKEN);
                        intentUrl.putExtra(GowildWebActivity.WEB_URL, HostConstants.HOST_HTML);
                        intentUrl.putExtra(GowildWebActivity.WEB_SN, responseBody.mac);
                        intentUrl.putExtra(GowildWebActivity.WEB_PHONE, SpUtils.getString(getBaseContext(), Constants.USERNAME));
                        intentUrl.putExtra(GowildWebActivity.WEB_TOKEN, token);
                        startActivity(intentUrl);
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, String responseBody) {
            }
        });
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void updateDate(Update update) {
        loadDataFromNet();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void subUnbind(UnbindEvent event) {
        initXBNoBind();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void subBindEvent(BindResult result){
        initViewNetAvaliable();
        startAnimotion();
        loadDataFromNet();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void subRobotLogin(LoginResult result){
        if (result.type == 1){
            mIsXBBind = true;
            mIsTaskShow = true;
            initViewNetAvaliable();
            loadDataFromNet();
        }else if(result.type == 2){
            mIsTaskShow = false;
            initViewNetInvaliable();
        }else if (result.type == 3){
            initXBNoBind();
        }
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void subOnline(RobotOnline online) {
        if (online.result) {//成功
            mIsXBBind = true;
            mIsTaskShow = true;
            startAnimotion();
            initViewNetAvaliable();
            loadDataFromNet();
        } else {
            mIsTaskShow = false;
//            startAnimotion();
            initViewNetInvaliable();
        }
    }


    private void updataView(PetInfo pet) {
        //存储东西
        SpUtils.putString(this, Constants.PETID, pet.id);
        SpUtils.putString(this, Constants.LEVEL, pet.level);
        if (pet.accountMission.equals(pet.missionAll)) {
            //显示小红点的步骤
            showRedTaskPoint = false;
        } else {
            showRedTaskPoint = true;
        }

        mSkillAll = pet.skillAll;
        String skillNum = SpUtils.getString(this, Constants.SKILL_NUM);
        if (!pet.accountSkill.equals(pet.skillAll)&& !mSkillAll.equals(skillNum)) {
            showRedSkillPoint = true;
        } else {
            showRedSkillPoint = false;
        }
        mAlltaskInfo = new String[]{pet.honorAccount + "/" + pet.honorAll, pet.accountMission + "/" + pet.missionAll, pet.accountSkill + "/" + pet.skillAll};
        //设置排名
        mGowildHomeRank.setText("排名: " +pet.petAccontRank);
        //设置等级
        mGowildHomeTvLevel.setText(String.format("等级:%s", pet.level));
        //设置标题
        sex = pet.sex;
        CommonDataUtil.setBLitterWhite(pet.sex == 0);
        robotIcon.setImageResource(pet.sex == 1 ? R.drawable.gowild_xb_hui :R.drawable.gowild_xb_defoult );
        mGowildHomeTitle.setText(pet.sex == 1 ? "<我的公子>" : "<我的小白>");

        //设置经验
        mGowildHomeTvExp.setText(String.format("%s/%s", pet.experience, pet.experienceAll));
        //设置任务
        mHomeAdapter.notifyDataSetChanged();

        misson = "(" + pet.accountMission + "/" + pet.missionAll + ")";
        honor = "(" + pet.honorAccount + "/" + pet.honorAll + ")";

        //百分比
        float percent = Float.parseFloat(pet.experience) / Float.parseFloat(pet.experienceAll) * 100;
        //动画
        simulateProgress(percent);
        //发布事件
        EventBus.getDefault().post(new PetTaskAndGorwTask(misson, honor));

    }


    /**
     * galley 的点击事件
     */
    private AdapterView.OnItemClickListener mGalleyOnItemClick = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            if (index != position) {
                return;
            }
            Intent intent;
            switch (position) {
                case HOME_MEDAL:
                    intent = new Intent(GowildHomeActivity.this, GowildHonorActivity.class);
                    intent.putExtra(Constants.HONOR, honor);
                    startActivity(intent);
                    break;
                case HOME_TASK:
                    intent = new Intent(GowildHomeActivity.this, GowildTaskActivity.class);
                    intent.putExtra(Constants.TASK, misson);
                    intent.putExtra(Constants.HONOR, honor);
                    startActivity(intent);
                    break;
                case HOME_SKILL:
                    SpUtils.putString(GowildHomeActivity.this,Constants.SKILL_NUM,mSkillAll);
                    showRedSkillPoint = false;
                    mHomeAdapter.notifyDataSetChanged();
                    startActivity(new Intent(GowildHomeActivity.this, SkillListActivity.class));
                    break;
                default:
                    break;
            }
        }
    };


    /**
     * 小白没有绑定
     */
    private void initXBNoBind() {
        mIsXBBind = false;
        mGowildHomeIvBot.setImageResource(R.drawable.gowild_home_funtion);
        mGowildHomeFlyTop.setVisibility(View.GONE);
        mGowildHomeLytBottom.setVisibility(View.INVISIBLE);
        mGowildHomeGlIv.setVisibility(View.GONE);
        mGowildHomeIvBot.setClickable(true);
        mGowildHomerl.setVisibility(View.INVISIBLE);
        mGowildHomeTvLevel.setVisibility(View.INVISIBLE);
        mGowildHomeTvExp.setVisibility(View.INVISIBLE);
        mGowildHomeTitle.setVisibility(View.INVISIBLE);
        mGowildHomeTopll.setVisibility(View.VISIBLE);
        mGowildHomeRank.setVisibility(View.INVISIBLE);
        mGowildHomeGlIv.setVisibility(View.GONE);
    }

    /**
     * galley的适配器
     */
    private BaseAdapter mHomeAdapter = new BaseAdapter() {


        @Override
        public int getCount() {
            return galleypics.length;
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        //就三个条目
        @SuppressLint("ViewHolder")
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            convertView = View.inflate(getApplicationContext(), R.layout.activity_gowild_home_galley_item, null);
            ImageView iv = (ImageView) convertView.findViewById(R.id.gowild_home_galley_item_iv);
            ImageView mRedShape = (ImageView) convertView.findViewById(R.id.gowild_home_galley_item_red);
            TextView tv = (TextView) convertView.findViewById(R.id.gowild_home_galley_item_tv);
            TextView tvTask = (TextView) convertView.findViewById(R.id.gowild_home_galley_item_tv_task);
            iv.setImageResource(galleypics[position]);
            tv.setText(galleytitles[position]);
            if (mAlltaskInfo != null) {
                tvTask.setText(mAlltaskInfo[position]);
            }

            if (position == 1) {
                if (showRedTaskPoint) {

                    mRedShape.setVisibility(View.VISIBLE);
                } else {

                    mRedShape.setVisibility(View.GONE);
                }
                return convertView;
            }

            //记住总技能数,比较总技能有没有变
            //如果没有,并且已经点击,就无需显示
            if (position == 2) {
                if (showRedSkillPoint) {
                    mRedShape.setVisibility(View.VISIBLE);
                } else {
                    mRedShape.setVisibility(View.GONE);
                }
                return convertView;
            }
            return convertView;
        }
    };

    int index;

    /**
     * galley的选中效果
     */
    private AdapterView.OnItemSelectedListener mHomeOnItemSelectedListener = new AdapterView.OnItemSelectedListener() {
        //条目选中的回调.这将决定它下面的文字
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            index = position;
            if (view == null) {
                return;
            }
            ImageView bg = (ImageView) view.findViewById(R.id.gowild_home_galley_item_iv);
            TextView tv = (TextView) view.findViewById(R.id.gowild_home_galley_item_tv);
            tv.setTextSize(19);
            int w = bg.getLayoutParams().width;
            int h = bg.getLayoutParams().height;
            RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(w + 6, h + 6);
            params.addRule(RelativeLayout.CENTER_VERTICAL);
            bg.setLayoutParams(params);
            params = new RelativeLayout.LayoutParams(w, h);
            params.addRule(RelativeLayout.CENTER_VERTICAL);
            for (int i = 0; i < parent.getChildCount(); i++) {
                View v = parent.getChildAt(i);
                if (v != view) {
                    ImageView bg2 = (ImageView) v.findViewById(R.id.gowild_home_galley_item_iv);
                    TextView tv2 = (TextView) view.findViewById(R.id.gowild_home_galley_item_tv);
                    tv2.setTextSize(17);
                    bg2.setLayoutParams(params);
                }
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    };

    /**
     * 这个动画问题在与按钮无法被禁用
     */
    private void startAnimotion() {
        if (mIsTaskShow) {
            //1.0 当前状态，箭头向下
            //1.1 需要加载箭头向上的动画
            animation = mAnimationRotateUp;
            //1.2 弹出 成就、技能等选项
            invisiblView = mGowildHomeLytBottom;
            visibleView = mGowildHomeGlIv;
            mGowildHomeLytBottom.setVisibility(View.GONE);
            if (mIsXBBind) {
                //2.3 绑定小白，显示技能
                mGowildHomeGlIv.setVisibility(View.VISIBLE);
            } else {
                mGowildHomeGlIv.setVisibility(View.GONE);
            }
        } else {
            //2.0 用户点击，如果当前状态是展示箭头向上
            //2.1 需要加载箭头向下的动画
            animation = mAnimationRotateDown;
            //2.2 弹出 家电、故事等选项
            visibleView = mGowildHomeLytBottom;
            invisiblView = mGowildHomeGlIv;
            mGowildHomeLytBottom.setVisibility(View.VISIBLE);
            mGowildHomeGlIv.setVisibility(View.GONE);
        }
        mGowildHomeIvBot.startAnimation(animation);
        visibleView.startAnimation(mAnimationIn);
        mIsTaskShow = !mIsTaskShow;
    }

    /**
     * 初始化动画。
     */
    private void initTalentArrowAnimation() {
        mAnimationRotateUp = new RotateAnimation(180f, 360f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        mAnimationRotateUp.setFillAfter(true);
        mAnimationRotateUp.setDuration(200);

        mAnimationRotateDown = new RotateAnimation(0, 180f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        mAnimationRotateDown.setFillAfter(true);
        mAnimationRotateDown.setDuration(200);

        mAnimationOut = AnimationUtils.loadAnimation(this, R.anim.gowild_home_bot_anim_out);
        mAnimationIn = AnimationUtils.loadAnimation(this, R.anim.gowild_home_bot_anim_in);

    }

    /**
     * 是否绑定情侣
     */
    public void isBindlover() {
        showLoading(R.string.loading);
        GowildHttpManager.requestLover(this, new GowildAsyncHttpResponseHandler<BindLoverInfo>(this) {
            @Override
            public void onSuccess(int statusCode, BindLoverInfo responseBody) {
                hideLoading();
                //成功
                if (responseBody == null) {
                    nextActivtiy(GowildLoverActivity.class);
                } else {
                    nextActivtiy(GowildContantsActivity.class);
                    SpUtils.putLong(GowildHomeActivity.this, Constants.MAGICCODE, responseBody.magicCode);
                }
            }

            @Override
            public void onFailure(int statusCode, String responseBody) {
                hideLoading();
            }
        });
    }

    private void goMonitor() {
        LoginMananger.getInstence().setBindFrom(Constants.AV);
        Intent intent = new Intent();
        intent.setClass(this, GowildAVMonitorActivity.class);
        startActivity(intent);
    }


    public void nextActivtiy(Class clazz, int type) {
        Intent intent = new Intent();
        if (LoginMananger.getInstence().getType() == IntContants.TYPE_ROBOT_SUCESS
                || LoginMananger.getInstence().getType() == IntContants.TYPE_ROBOT_OUTLINE) {
            intent.setClass(this, clazz);
            startActivity(intent);
        } else {
            intent.setClass(this, GowildNeedBindRobotActivity.class);
            LoginMananger.getInstence().setBindFrom(type);
            intent.putExtra(Constants.TYPE, type);
            startActivity(intent);
        }
    }

}

