package com.gowild.mobileclient.activity;

import android.os.Bundle;
import android.support.v4.view.ViewPager;

import com.gowild.mobileclient.R;
import com.gowild.mobileclient.adapter.HonorFragmentPageAdapter;
import com.gowild.mobileclient.base.baseactivity.GowildBaseActivity;
import com.gowild.mobileclient.config.Constants;
import com.gowild.mobileclient.event.PetTaskAndGorwTask;
import com.gowild.mobileclient.fragment.HonorAchievementFragment;
import com.gowild.mobileclient.fragment.HonorWealthFragment;
import com.gowild.mobileclient.model.PagerItem;
import com.gowild.mobileclient.utils.CommonDataUtil;
import com.gowild.mobileclient.widget.MyToolBar;
import com.gowild.mobileclient.widget.PagerSlidingTabStrip;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * @author zhangct
 * @version 1.0
 * @since 2016/7/23 09:18
 * <p><strong> 荣誉界面 </strong></p>
 */
/**
 * @author zhangct
 * @version 1.0
 * @since 2016/7/23 09:18
 * <p><strong> 荣誉界面 </strong></p>
 */
public class GowildHonorActivity extends GowildBaseActivity {

    // ===========================================================
    // HostConstants
    // ===========================================================

    private static final String TAG = GowildHonorActivity.class.getSimpleName();
    @Bind(R.id.gowild_honor_pt)
    PagerSlidingTabStrip mGowildHonorPt;
    @Bind(R.id.gowild_honor_vp)
    ViewPager mGowildHonorVp;
    @Bind(R.id.view_toolbar)
    MyToolBar mToolbar;

    // Static Fields
    // ===========================================================

    // ===========================================================
    // Fields
    // ===========================================================
    List<PagerItem> mPagerItems = new ArrayList<PagerItem>();


    // ===========================================================
    // Constructors
    // ===========================================================


    // ===========================================================
    // Getter or Setter
    // ===========================================================


    // ===========================================================
    // Methods for/from SuperClass/Interfaces
    // ===========================================================


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        mToolbar.setTitle(CommonDataUtil.getCurrentRobotTitle() + getString(R.string.gowild_honor_title));
        initPageDatas();
        mGowildHonorVp.setAdapter(new HonorFragmentPageAdapter(getSupportFragmentManager(),mPagerItems));
        mGowildHonorPt.setViewPager(mGowildHonorVp);
    }


    @Override
    protected int onSetTopbarView() {
        return R.layout.view_gowild_toolbar;
    }

    @Override
    protected int onSetContentView() {
        return R.layout.activity_gowild_honor;
    }

    @Override
    protected int onSetTopBarHeight() {
        return getResources().getDimensionPixelSize(R.dimen.gowild_toolbar_height);
    }


    // ===========================================================
    // Methods
    // ===========================================================

    /**
     * 初始化选项卡
     */
    private void initPageDatas() {
        String honor = getIntent().getStringExtra(Constants.HONOR);
        mPagerItems.clear();
        mPagerItems.add(new PagerItem("勋章成就" + honor, new HonorAchievementFragment()));
        mPagerItems.add(new PagerItem("财富排行", new HonorWealthFragment()));
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void subSetTitle(PetTaskAndGorwTask honor){
        mGowildHonorPt.setTabStr("勋章成就" +honor.grow ,0);
    }

    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================
}
