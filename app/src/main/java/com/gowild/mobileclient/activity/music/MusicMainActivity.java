/**
 * Copyright (C)  2016 深圳市狗尾草智能科技有限公司
 * GowildXB
 * MusicMainActivity.java
 */
package com.gowild.mobileclient.activity.music;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.AudioManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;
import android.widget.ImageView;
import android.widget.TextView;

import com.gowild.mobile.libcommon.utils.ToastUtils;
import com.gowild.mobile.libp2sp.core.exception.NotConnectedException;
import com.gowild.mobile.libp2sp.core.listener.IDataListener;
import com.gowild.mobile.libp2sp.netty.NettyClientManager;
import com.gowild.mobileclient.R;
import com.gowild.mobileclient.base.baseactivity.GowildBaseActivity;
import com.gowild.mobileclient.config.Constants;
import com.gowild.mobileclient.config.IntContants;
import com.gowild.mobileclient.config.KeysContainer;
import com.gowild.mobileclient.config.MediaConstants;
import com.gowild.mobileclient.manager.GowildAsyncHttpResponseHandler;
import com.gowild.mobileclient.manager.GowildHttpManager;
import com.gowild.mobileclient.manager.GowildTCPManager;
import com.gowild.mobileclient.misc.GowildResponseCode;
import com.gowild.mobileclient.model.BaseNetResourceInfo;
import com.gowild.mobileclient.model.MusicItemInfoBack;
import com.gowild.mobileclient.model.StoryAlbumItemInfoBack;
import com.gowild.mobileclient.protocol.MachineBothMsgProto;
import com.gowild.mobileclient.protocol.MusicBothMsgProto;
import com.gowild.mobileclient.protocol.MusicS2ACMsgProto;
import com.gowild.mobileclient.protocol.PlayerStatusMsgProto;
import com.gowild.mobileclient.protocol.StoryBothMsgProto;
import com.gowild.mobileclient.protocol.StoryS2ACMsgProto;
import com.gowild.mobileclient.utils.ImageUtils;
import com.gowild.mobileclient.vo.RequestStoryHistory;
import com.gowild.mobileclient.widget.CircleProgressBar;
import com.gowild.mobileclient.widget.MusicMyFacItem;
import com.gowild.mobileclient.widget.MyToolBar;
import com.gowild.mobileclient.widget.NetResourceCollectView;
import com.loopj.android.http.RequestParams;
import com.nostra13.universalimageloader.core.assist.FailReason;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * @author temp
 * @version 1.0
 *          <p><strong>Features draft description.主要功能介绍</strong></p>
 * @since 2016/7/26 20:24
 */
public class MusicMainActivity extends GowildBaseActivity {

    // ===========================================================
    // HostConstants
    // ===========================================================

    private static final String TAG           = MusicMainActivity.class.getSimpleName();
    private static final int    CHANGE_MEIDA  = 0;
    private static final int    IMAGE_LOADING = 1;
    private static final int    INIT_STATE    = 2;
    @Bind(R.id.cpb_music_play)
    CircleProgressBar mCPBMusicPlay;
    @Bind(R.id.tv_name)
    TextView          tvTitle;
    @Bind(R.id.tv_author)
    TextView          tvAuthor;
    @Bind(R.id.iv_music_photo)
    protected ImageView mMusicPhoto;
    public    int       type;
    @Bind(R.id.btn_play)
    ImageView              btnPlay;
    @Bind(R.id.btn_stop)
    ImageView              btnStop;
    @Bind(R.id.collect)
    NetResourceCollectView collect;
    @Bind(R.id.item_lover)
    MusicMyFacItem         loverMyFacItem;
    @Bind(R.id.item_record)
    MusicMyFacItem         recordMyFacItem;
    private Bitmap                    mCoverImage             = null;
    //    private MachineInfoListener mMachineInfoListener = new MachineInfoListener();
    private StoryChangeListener       mStoryChangeListener    = new StoryChangeListener();
    private MusicChangeListener       mMusicChangeListener    = new MusicChangeListener();
    private CoverImageLoadingListener mCoverIvLoadingListener = new CoverImageLoadingListener();
    private MachineInfoListener       mMachineInfoListener    = new MachineInfoListener();
    private PlayChangeListener        mPlayChangeListener     = new PlayChangeListener();
    private Animation    mAnimation;
    private int          mVolume;
    private AudioManager mAm;
    private int          mMax;
    private int          mCurrent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        initToolBar();
        NettyClientManager manager = NettyClientManager.getInstance();
        manager.addHandlerListener(GowildResponseCode.MUSIC_CHANGE, mMusicChangeListener);
        manager.addHandlerListener(GowildResponseCode.STORY_CHANGE, mStoryChangeListener);
        manager.addHandlerListener(GowildResponseCode.PLAY_CHANGE, mPlayChangeListener);
        manager.addHandlerListener(GowildResponseCode.MUSIC_UNLIKE, mMusicUnlikeListener);
        manager.addHandlerListener(GowildResponseCode.STORY_UNLIKE, mStoryUnlikeListener);
        manager.addHandlerListener(GowildResponseCode.ROBOT_INFO_SYNC, mMachineInfoListener);
        mAm = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        mMax = mAm.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
        mCurrent = mAm.getStreamVolume(AudioManager.STREAM_MUSIC);
        sendSyncRobot();
    }

    private void sendSyncRobot() {
        try {
            GowildTCPManager.requestRobotRequestSync();
        } catch (NotConnectedException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected int onSetTopbarView() {
        return R.layout.view_gowild_toolbar;
    }

    @Override
    protected int onSetContentView() {
        return R.layout.activity_gowild_music_main;
    }

    @Override
    protected int onSetTopBarHeight() {
        return getResources().getDimensionPixelSize(R.dimen.gowild_toolbar_height);
    }

    @Override
    protected void onResume() {
        super.onResume();
        btnPlay.setVisibility(View.VISIBLE);
        btnStop.setVisibility(View.GONE);
        tvTitle.setText("");
        tvAuthor.setText("");
        if (IntContants.isCurrentMusic()) {
            //信息不为空
            if (MediaConstants.mMusicMsg != null && MediaConstants.mMusicMsg.getMusicAlbumInfoMsg().hasId()) {
                if (!TextUtils.isEmpty(MediaConstants.mMusicMsg.getMusicAlbumInfoMsg().getCoverUrl())) {
                    ImageUtils.getInstance().loadNetImage(MediaConstants.mMusicMsg.getMusicAlbumInfoMsg().getCoverUrl(), mCoverIvLoadingListener);
                } else {
                    mMusicPhoto.setImageBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.gowild_splash_logo));
                }
                tvTitle.setText(MediaConstants.mMusicMsg.getMusicInfoMsg().getTitle());
                tvAuthor.setText(MediaConstants.mMusicMsg.getMusicInfoMsg().getSinger());
                if (MediaConstants.mPlayState == MediaConstants.PLAY_STATE_PLAY) {
                    btnPlay.setVisibility(View.GONE);
                    btnStop.setVisibility(View.VISIBLE);
                } else {
                    btnPlay.setVisibility(View.VISIBLE);
                    btnStop.setVisibility(View.GONE);
                }
                //信息为空
            } else {
                btnPlay.setVisibility(View.VISIBLE);
                btnStop.setVisibility(View.GONE);
                tvTitle.setText("");
                tvAuthor.setText("");
                mMusicPhoto.setImageBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.gowild_splash_logo));
            }
            sendMusic();
        } else {
            if (MediaConstants.mStoryMsg != null && MediaConstants.mStoryMsg.getStoryAlbumInfoMsg().hasId()) {
                ImageUtils.getInstance().loadNetImage(MediaConstants.mStoryMsg.getStoryAlbumInfoMsg().getCoverUrl(), mCoverIvLoadingListener);
                tvTitle.setText(MediaConstants.mStoryMsg.getStoryInfoMsg().getTitle());
                tvAuthor.setText(MediaConstants.mStoryMsg.getStoryInfoMsg().getAuthor());
                if (MediaConstants.mPlayState == MediaConstants.PLAY_STATE_PLAY) {
                    btnPlay.setVisibility(View.GONE);
                    btnStop.setVisibility(View.VISIBLE);
                } else {
                    btnPlay.setVisibility(View.VISIBLE);
                    btnStop.setVisibility(View.GONE);
                }
            } else {
                btnPlay.setVisibility(View.VISIBLE);
                btnStop.setVisibility(View.GONE);
                tvTitle.setText("");
                tvAuthor.setText("");
                mMusicPhoto.setImageBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.gowild_splash_logo));
            }
            sendStory();
        }

    }

    private void sendStory() {
        //发送故事同步
        try {
            GowildTCPManager.requestNewStorySync();
        } catch (NotConnectedException e) {
            e.printStackTrace();
        }
    }

    private void sendMusic() {
        //发送音乐同步
        try {
            GowildTCPManager.requestNewMusicSync();
        } catch (NotConnectedException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        NettyClientManager manager = NettyClientManager.getInstance();
        //        manager.removeHandlerListener(GowildResponseCode.ROBOT_INFO_SYNC, mMachineInfoListener);
        manager.removeHandlerListener(GowildResponseCode.MUSIC_CHANGE, mMusicChangeListener);
        manager.removeHandlerListener(GowildResponseCode.STORY_CHANGE, mStoryChangeListener);
        manager.removeHandlerListener(GowildResponseCode.PLAY_CHANGE, mPlayChangeListener);
        manager.removeHandlerListener(GowildResponseCode.MUSIC_UNLIKE, mMusicUnlikeListener);
        manager.removeHandlerListener(GowildResponseCode.STORY_UNLIKE, mStoryUnlikeListener);
        manager.removeHandlerListener(GowildResponseCode.ROBOT_INFO_SYNC, mMachineInfoListener);
    }

    @Override
    protected void onPause() {
        super.onPause();

    }

    private void initToolBar() {
        MyToolBar toolBar = (MyToolBar) findViewById(R.id.view_toolbar);
        if (IntContants.isCurrentMusic()) {
            toolBar.setTitle(R.string.gowild_title_music);
            loverMyFacItem.setTitle(R.string.gowild_music_lover_title, R.string.gowild_music_lover_sub_title);
            recordMyFacItem.setTitle(R.string.gowild_music_play_record_title, R.string.gowild_music_play_record_sub_title);
        } else {
            toolBar.setTitle(R.string.gowild_title_story);
            loverMyFacItem.setTitle(R.string.gowild_story_lover_title, R.string.gowild_story_lover_sub_title);
            recordMyFacItem.setTitle(R.string.gowild_music_play_record_title, R.string.gowild_story_play_record_sub_title);
        }
        toolBar.setActionDrawable(R.drawable.gowild_search);
        toolBar.setActionListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MusicMainActivity.this, MusicSearchActivity.class));
            }
        });
        mCPBMusicPlay.setProgress(0);
        mCPBMusicPlay.setProgressTextSize(0);
        mAnimation = AnimationUtils.loadAnimation(this, R.anim.rotating);
        mAnimation.setInterpolator(new LinearInterpolator());
    }


    @OnClick({R.id.item_lover, R.id.item_record, R.id.btn_play, R.id.btn_stop, R.id.btn_next})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.item_lover:
                changeLikeStatus();
                break;
            case R.id.item_record:
                if (IntContants.isCurrentMusic()) {
                    showLoading(R.string.loading);
                    GowildHttpManager.getMusicHistory(this, new GowildAsyncHttpResponseHandler<MusicItemInfoBack>(this, true) {
                        @Override
                        public void onSuccess(int statusCode, MusicItemInfoBack responseBody) {

                        }

                        @Override
                        public void onSuccess(int statusCode, ArrayList<MusicItemInfoBack> responseBody) {
                            super.onSuccess(statusCode, responseBody);
                            hideLoading();
                            if (responseBody.size() == 0) {
                                Intent intent = new Intent(getBaseContext(), MusicStoryNoRecordActivity.class);
                                startActivity(intent);
                            } else {
                                startActivity(new Intent(getBaseContext(), MusicRecordActivity.class));
                            }
                        }

                        @Override
                        public void onFailure(int statusCode, String responseBody) {
                            hideLoading();
                        }
                    });
                } else {
                    showLoading(R.string.loading);
                    GowildHttpManager.getStoryHistory(this, new RequestStoryHistory(), new GowildAsyncHttpResponseHandler<StoryAlbumItemInfoBack>(this, true) {
                        @Override
                        public void onSuccess(int statusCode, StoryAlbumItemInfoBack responseBody) {

                        }

                        @Override
                        public void onSuccess(int statusCode, ArrayList<StoryAlbumItemInfoBack> responseBody) {
                            super.onSuccess(statusCode, responseBody);
                            hideLoading();
                            if (responseBody.size() == 0) {
                                Intent intent = new Intent(getBaseContext(), MusicStoryNoRecordActivity.class);
                                startActivity(intent);
                            } else {
                                startActivity(new Intent(getBaseContext(), MusicRecordActivity.class));
                            }
                        }

                        @Override
                        public void onFailure(int statusCode, String responseBody) {
                            hideLoading();
                        }
                    });
                }
                break;

            case R.id.btn_next:
                if (IntContants.isCurrentMusic()) {
                    btnPlay.setEnabled(false);
                    showLoading(R.string.loading);
                    MusicBothMsgProto.MusicAndAlbumInfoMsg musicMsg = MediaConstants.mMusicMsg;
                    int                                    musicId  = -1;
                    int                                    collect  = MediaConstants.COLLECT_NO;
                    if (musicMsg != null && musicMsg.getMusicAlbumInfoMsg().hasId()) {
                        collect = MediaConstants.mMusicCollect == 1 ? 1 : 0;
                    }
                    GowildHttpManager.changeMusic(getBaseContext(), new RequestParams(Constants.MUSIC_ID, musicId, Constants.PLAY_STATUS, Constants.PLAY_STATUS_NEXT, Constants.PLAY_TYPE, collect), new GowildAsyncHttpResponseHandler(getBaseContext()) {
                        @Override
                        public void onSuccess(int statusCode, Object responseBody) {
                            hideLoading();
                            btnPlay.setEnabled(true);
                            btnPlay.setVisibility(View.GONE);
                            btnStop.setVisibility(View.VISIBLE);
                        }

                        @Override
                        public void onFailure(int statusCode, String responseBody) {
                            hideLoading();
                            //                            ToastUtils.showCustom(getBaseContext(), R.string.gowild_network_error, false);
                            btnPlay.setEnabled(true);
                        }
                    });
                } else {
                    btnPlay.setEnabled(false);
                    showLoading(R.string.loading);
                    StoryBothMsgProto.StoryAndAlbumInfoMsg storyMsg = MediaConstants.mStoryMsg;
                    int                                    storyid  = -1;
                    int                                    collect  = MediaConstants.COLLECT_NO;
                    if (storyMsg != null && storyMsg.getStoryAlbumInfoMsg().hasId()) {
                        collect = MediaConstants.mStoryCollect == 1 ? 1 : 0;
                    }
                    GowildHttpManager.changeStory(getBaseContext(), new RequestParams(Constants.STORY_ID, storyid, Constants.PLAY_STATUS, Constants.PLAY_STATUS_NEXT, Constants.PLAY_TYPE, collect), new GowildAsyncHttpResponseHandler(getBaseContext()) {
                        @Override
                        public void onSuccess(int statusCode, Object responseBody) {
                            hideLoading();
                            btnPlay.setEnabled(true);
                            btnPlay.setVisibility(View.GONE);
                            btnStop.setVisibility(View.VISIBLE);
                        }

                        @Override
                        public void onFailure(int statusCode, String responseBody) {
                            hideLoading();
                            btnPlay.setEnabled(true);

                        }
                    });
                }
                break;
            case R.id.btn_play:
                if (IntContants.isCurrentMusic()) {
                    MusicBothMsgProto.MusicAndAlbumInfoMsg musicMsg = MediaConstants.mMusicMsg;
                    int                                    musicId  = -1;
                    int                                    collect  = MediaConstants.COLLECT_NO;
                    int                                    status   = Constants.PLAY_STATUS_NEXT;
                    if (musicMsg != null && musicMsg.getMusicAlbumInfoMsg().hasId()) {
                        musicId = musicMsg.getMusicInfoMsg().getId();
                        collect = MediaConstants.mMusicCollect == 1 ? 1 : 0;
                        status = Constants.PLAY_STATUS_RESUME;
                    }
                    btnPlay.setEnabled(false);
                    showLoading(R.string.loading);
                    GowildHttpManager.changeMusic(getBaseContext(), new RequestParams(Constants.MUSIC_ID, musicId, Constants.PLAY_STATUS, status, Constants.PLAY_TYPE, collect), new GowildAsyncHttpResponseHandler(getBaseContext()) {
                        @Override
                        public void onSuccess(int statusCode, Object responseBody) {
                            hideLoading();
                            btnPlay.setEnabled(true);
                            btnPlay.setVisibility(View.GONE);
                            btnStop.setVisibility(View.VISIBLE);
                        }

                        @Override
                        public void onFailure(int statusCode, String responseBody) {
                            hideLoading();
                            btnPlay.setEnabled(true);
                        }
                    });
                } else {
                    StoryBothMsgProto.StoryAndAlbumInfoMsg storyMsg = MediaConstants.mStoryMsg;
                    btnPlay.setEnabled(false);
                    showLoading(R.string.loading);
                    int storyid = -1;
                    int collect = MediaConstants.COLLECT_NO;
                    int status  = Constants.PLAY_STATUS_NEXT;
                    if (storyMsg != null && storyMsg.getStoryAlbumInfoMsg().hasId()) {
                        storyid = storyMsg.getStoryInfoMsg().getId();
                        collect = MediaConstants.mStoryCollect == 1 ? 1 : 0;
                        status = Constants.PLAY_STATUS_RESUME;
                    }
                    GowildHttpManager.changeStory(getBaseContext(), new RequestParams(Constants.STORY_ID, storyid, Constants.PLAY_STATUS, status, Constants.PLAY_TYPE, collect), new GowildAsyncHttpResponseHandler(getBaseContext()) {
                        @Override
                        public void onSuccess(int statusCode, Object responseBody) {
                            hideLoading();
                            btnPlay.setEnabled(true);
                            btnPlay.setVisibility(View.GONE);
                            btnStop.setVisibility(View.VISIBLE);
                        }

                        @Override
                        public void onFailure(int statusCode, String responseBody) {
                            hideLoading();
                            btnPlay.setEnabled(true);
                        }
                    });
                }
                break;
            case R.id.btn_stop:
                if (IntContants.isCurrentMusic()) {
                    int collect = MediaConstants.COLLECT_NO;
                    if (MediaConstants.mMusicMsg != null && MediaConstants.mMusicMsg.getMusicAlbumInfoMsg().hasId()) {
                        MusicBothMsgProto.MusicAndAlbumInfoMsg musicMsg  = MediaConstants.mMusicMsg;
                        MusicBothMsgProto.MusicInfoMsg         musicInfo = musicMsg.getMusicInfoMsg();
                        btnStop.setEnabled(false);
                        showLoading(R.string.loading);
                        collect = MediaConstants.mMusicCollect == 1 ? 1 : 0;
                        GowildHttpManager.changeMusic(getBaseContext(), new RequestParams(Constants.MUSIC_ID, musicInfo.getId(), Constants.PLAY_STATUS, Constants.PLAY_STATUS_PAUSE, Constants.PLAY_TYPE, collect), new GowildAsyncHttpResponseHandler(getBaseContext()) {
                            @Override
                            public void onSuccess(int statusCode, Object responseBody) {
                                hideLoading();
                                btnStop.setEnabled(true);
                                btnStop.setVisibility(View.GONE);
                                btnPlay.setVisibility(View.VISIBLE);
                            }

                            @Override
                            public void onFailure(int statusCode, String responseBody) {
                                hideLoading();
                                btnStop.setEnabled(true);
                            }
                        });
                    } else {
                        ToastUtils.showCustom(getBaseContext(), "没有音乐数据哦，无法暂停", false);
                    }
                } else {
                    int collect = MediaConstants.COLLECT_NO;
                    if (MediaConstants.mStoryMsg != null && MediaConstants.mStoryMsg.getStoryAlbumInfoMsg().hasId()) {
                        StoryBothMsgProto.StoryAndAlbumInfoMsg storyMsg  = MediaConstants.mStoryMsg;
                        StoryBothMsgProto.StoryInfoMsg         storyInfo = storyMsg.getStoryInfoMsg();
                        showLoading(R.string.loading);
                        collect = MediaConstants.mStoryCollect == 1 ? 1 : 0;
                        GowildHttpManager.changeStory(getBaseContext(), new RequestParams(Constants.STORY_ID, storyInfo.getId(), Constants.PLAY_STATUS, Constants.PLAY_STATUS_PAUSE, Constants.PLAY_TYPE, collect), new GowildAsyncHttpResponseHandler(getBaseContext()) {
                            @Override
                            public void onSuccess(int statusCode, Object responseBody) {
                                hideLoading();
                                btnStop.setEnabled(true);
                                btnStop.setVisibility(View.GONE);
                                btnPlay.setVisibility(View.VISIBLE);
                            }

                            @Override
                            public void onFailure(int statusCode, String responseBody) {
                                hideLoading();
                                btnStop.setEnabled(true);
                            }
                        });
                    } else {
                        ToastUtils.showCustom(getBaseContext(), "没有故事数据哦，无法暂停", false);
                    }
                }
                break;
        }
    }

    private void changeLikeStatus() {
        if (IntContants.isCurrentMusic()) {
            showLoading(R.string.loading);
            GowildHttpManager.getMusicCollect(this, 1, new GowildAsyncHttpResponseHandler<MusicItemInfoBack>(this) {
                @Override
                public void onSuccess(int statusCode, MusicItemInfoBack responseBody) {
                    hideLoading();
                    if (responseBody == null) {
                        Intent intent = new Intent(getBaseContext(), MusicStoryNoLoverActivity.class);
                        startActivity(intent);
                    } else {
                        if (responseBody.musics.size() == 0) {
                            Intent intent = new Intent(getBaseContext(), MusicStoryNoLoverActivity.class);
                            startActivity(intent);
                        } else {
                            startActivity(new Intent(getBaseContext(), MusicLoverActivity.class));
                        }
                    }

                }

                @Override
                public void onFailure(int statusCode, String responseBody) {
                    hideLoading();
                }
            });
        } else {
            showLoading(R.string.loading);
            GowildHttpManager.getStoryCollect(this, 1, new GowildAsyncHttpResponseHandler<StoryAlbumItemInfoBack>(this) {
                @Override
                public void onSuccess(int statusCode, StoryAlbumItemInfoBack responseBody) {
                    hideLoading();
                    if (responseBody == null) {
                        Intent intent = new Intent(getBaseContext(), MusicStoryNoLoverActivity.class);
                        startActivity(intent);
                    } else {
                        if (responseBody.storyAlbums.size() == 0) {
                            Intent intent = new Intent(getBaseContext(), MusicStoryNoLoverActivity.class);
                            startActivity(intent);
                        } else {
                            startActivity(new Intent(getBaseContext(), MusicLoverActivity.class));
                        }
                    }
                }


                @Override
                public void onFailure(int statusCode, String responseBody) {
                    hideLoading();
                }
            });
        }
    }

    private class MusicChangeListener implements IDataListener<MusicS2ACMsgProto.MusicSyncForAppMsg> {

        @Override
        public void onReceiveData(final MusicS2ACMsgProto.MusicSyncForAppMsg data) {
            runOnUiThread(new Runnable() {

                @Override
                public void run() {
                    if (IntContants.isCurrentMusic()) {
                        int status = data.getStatus();
                        changePlaystatus(status);
                        int collection = data.getCollection();
                        collect.setSelected(collection == 1);
                        MediaConstants.mMusicCollect = data.getCollection();//1.收藏 ,2 未收藏
                        MediaConstants.mMusicMsg = data.getMusicAndAlbumInfoMsg();
                        ImageUtils.getInstance().loadNetImage(data.getMusicAndAlbumInfoMsg().getMusicAlbumInfoMsg().getCoverUrl(), mCoverIvLoadingListener);
                        tvTitle.setText(MediaConstants.mMusicMsg.getMusicInfoMsg().getTitle());
                        tvAuthor.setText(MediaConstants.mMusicMsg.getMusicInfoMsg().getSinger());
                        BaseNetResourceInfo info = new BaseNetResourceInfo();
                        info.collect = collection == 1;
                        info.musicId = data.getMusicAndAlbumInfoMsg().getMusicInfoMsg().getId();
                        collect.setInfo(info);
                    }
                }
            });

        }

        @Override
        public void onActiveChanged(boolean isActive) {

        }
    }

    private MusicUnlikeListener mMusicUnlikeListener = new MusicUnlikeListener();

    private class MusicUnlikeListener implements IDataListener<String> {

        @Override
        public void onReceiveData(String s) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    sendStory();
                }
            });

        }

        @Override
        public void onActiveChanged(boolean isActive) {

        }
    }

    private StoryUnlikeListener mStoryUnlikeListener = new StoryUnlikeListener();

    private class StoryUnlikeListener implements IDataListener<String> {

        @Override
        public void onReceiveData(String s) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    sendMusic();
                }
            });

        }

        @Override
        public void onActiveChanged(boolean isActive) {

        }
    }


    private class PlayChangeListener implements IDataListener<PlayerStatusMsgProto.PlayerStatusMsg> {

        @Override
        public void onReceiveData(final PlayerStatusMsgProto.PlayerStatusMsg data) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (IntContants.isCurrentMusic()) {
                        changePlaystatus(data.getMusicPlayer());
                    } else {
                        changePlaystatus(data.getStoryPlayer());
                    }
                }
            });
        }

        @Override
        public void onActiveChanged(boolean isActive) {

        }
    }


    private void changePlaystatus(int data) {
        if (data == 1 || data == 4) {
            btnPlay.setEnabled(true);
            btnPlay.setVisibility(View.GONE);
            btnStop.setVisibility(View.VISIBLE);
            mMusicPhoto.startAnimation(mAnimation);
        } else {
            btnStop.setEnabled(true);
            btnStop.setVisibility(View.GONE);
            btnPlay.setVisibility(View.VISIBLE);
            mMusicPhoto.clearAnimation();
        }
    }


    private class StoryChangeListener implements IDataListener<StoryS2ACMsgProto.StorySyncForAppMsg> {

        @Override
        public void onReceiveData(final StoryS2ACMsgProto.StorySyncForAppMsg data) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (!IntContants.isCurrentMusic()) {
                        int status = data.getStatus();
                        changePlaystatus(status);
                        int collection = data.getCollection();
                        collect.setSelected(collection == 1);
                        MediaConstants.mStoryCollect = data.getCollection();
                        MediaConstants.mStoryMsg = data.getStoryAndAlbumInfoMsg();
                        ImageUtils.getInstance().loadNetImage(data.getStoryAndAlbumInfoMsg().getStoryAlbumInfoMsg().getCoverUrl(), mCoverIvLoadingListener);
                        tvTitle.setText(MediaConstants.mStoryMsg.getStoryInfoMsg().getTitle());
                        tvAuthor.setText(MediaConstants.mStoryMsg.getStoryInfoMsg().getAuthor());
                        BaseNetResourceInfo info = new BaseNetResourceInfo();
                        info.collect = collection == 1;
                        info.albumId = data.getStoryAndAlbumInfoMsg().getStoryAlbumInfoMsg().getId();
                        collect.setInfo(info);
                    }
                }
            });
        }

        @Override
        public void onActiveChanged(boolean isActive) {

        }

    }

    private class CoverImageLoadingListener implements com.nostra13.universalimageloader.core.listener.ImageLoadingListener {

        @Override
        public void onLoadingStarted(String imageUri, View view) {

        }

        @Override
        public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
            mCoverImage = loadedImage;
            mHandler.sendEmptyMessage(IMAGE_LOADING);
        }

        @Override
        public void onLoadingFailed(String imageUri, View view, FailReason failReason) {

        }

        @Override
        public void onLoadingCancelled(String imageUri, View view) {

        }

    }

    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case CHANGE_MEIDA:
                    Log.d(TAG, "MachineInfoListener state:" + MediaConstants.mPlayState);
                    if (!IntContants.isCurrentMusic()) {
                        //                        tvTitle.setText(MediaConstants.mStoryMsg.getStoryInfoMsg().getTitle());
                        //                        tvAuthor.setText(MediaConstants.mStoryMsg.getStoryInfoMsg().getAuthor());
                        //                        if (MediaConstants.mStoryMsg != null && MediaConstants.mStoryMsg.getStoryAlbumInfoMsg().hasId() &&
                        //                                MediaConstants.mPlayState == MediaConstants.PLAY_STATE_PLAY) {
                        //                            btnPlay.setEnabled(true);
                        //                            btnStop.setEnabled(true);
                        //                            btnPlay.setVisibility(View.GONE);
                        //                            btnStop.setVisibility(View.VISIBLE);
                        //
                        //                        } else {
                        //                            btnPlay.setEnabled(true);
                        //                            btnStop.setEnabled(true);
                        //                            btnPlay.setVisibility(View.VISIBLE);
                        //                            btnStop.setVisibility(View.GONE);
                        //                        }
                    } else {
                        //                        tvTitle.setText(MediaConstants.mMusicMsg.getMusicInfoMsg().getTitle());
                        //                        tvAuthor.setText(MediaConstants.mMusicMsg.getMusicInfoMsg().getSinger());
                        //                        if (MediaConstants.mMusicMsg != null && MediaConstants.mMusicMsg.getMusicAlbumInfoMsg().hasId() &&
                        //                                MediaConstants.mPlayState == MediaConstants.PLAY_STATE_PLAY) {
                        //                            btnPlay.setEnabled(true);
                        //                            btnStop.setEnabled(true);
                        //                            btnPlay.setVisibility(View.GONE);
                        //                            btnStop.setVisibility(View.VISIBLE);
                        //                        } else {
                        //                            btnPlay.setEnabled(true);
                        //                            btnStop.setEnabled(true);
                        //                            btnPlay.setVisibility(View.VISIBLE);
                        //                            btnStop.setVisibility(View.GONE);
                        //                        }
                    }
                    //                    setCollectInfo();

                    break;
                case IMAGE_LOADING:
                    mMusicPhoto.setImageBitmap(mCoverImage);
                    //                    Animation animation = AnimationUtils.loadAnimation(getBaseContext(), R.anim.rotating);
                    //                    animation.setInterpolator(new LinearInterpolator());
                    //                    mMusicPhoto.setAnimation(animation);
                    break;
                case INIT_STATE:
                    //                    btnPlay.setVisibility(View.VISIBLE);
                    //                    btnStop.setVisibility(View.GONE);
                    //                    mMusicPhoto.startAnimation(mAnimation);
                    break;
            }
        }
    };


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        // 获取手机当前音量值
        switch (keyCode) {
            // 音量增大
            case KeyEvent.KEYCODE_VOLUME_UP:
                mAm.adjustStreamVolume(
                        AudioManager.STREAM_MUSIC,
                        AudioManager.ADJUST_RAISE,
                        AudioManager.FLAG_PLAY_SOUND | AudioManager.FLAG_SHOW_UI);
                mCurrent = mAm.getStreamVolume(AudioManager.STREAM_MUSIC);
                float cut = mCurrent;
                float max = mMax;
                float percent = cut / max;
                if (percent == 1) {
                    updataVolume(20);
                } else {
                    mVolume = (int) (percent * 14 + 0.5);
                    updataVolume(mVolume);
                }
                return true;
            // 音量减小
            case KeyEvent.KEYCODE_VOLUME_DOWN:
                mAm.adjustStreamVolume(
                        AudioManager.STREAM_MUSIC,
                        AudioManager.ADJUST_LOWER,
                        AudioManager.FLAG_PLAY_SOUND | AudioManager.FLAG_SHOW_UI);
                mCurrent = mAm.getStreamVolume(AudioManager.STREAM_MUSIC);
                float cut1 = mCurrent;
                float max1 = mMax;
                float per = cut1 / max1;
                    if (per == 0) {
                        updataVolume(-10);
                } else {
                    mVolume = (int) (per * 14 + 0.5);
                    updataVolume(mVolume);
                }
                return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    private class MachineInfoListener implements IDataListener<MachineBothMsgProto.MachineInfoMsg> {

        @Override
        public void onReceiveData(final MachineBothMsgProto.MachineInfoMsg data) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    //处理是否是4G的机器
                    mVolume = data.getVolume();
                }
            });
        }

        @Override
        public void onActiveChanged(boolean isActive) {

        }
    }

    private void updataVolume(int volume) {
        if (volume > 15) {
            volume = 15;
        } else if (volume < 0) {
            volume = 0;
        }
        RequestParams params = new RequestParams(KeysContainer.KEY_VOLUME, volume);
        GowildHttpManager.updateTalkSpeedOrVolume(getBaseContext(), params, new GowildAsyncHttpResponseHandler(getBaseContext()) {
            @Override
            public void onSuccess(int statusCode, Object responseBody) {
            }

            @Override
            public void onFailure(int statusCode, String responseBody) {

            }
        });
    }

}
