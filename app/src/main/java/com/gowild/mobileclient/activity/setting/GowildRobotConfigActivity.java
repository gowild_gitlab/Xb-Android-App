/**
 * Copyright (C)  2016 深圳市狗尾草智能科技有限公司
 * GowildXB
 * GowildRobotConfigActivity.java
 */
package com.gowild.mobileclient.activity.setting;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.ContextThemeWrapper;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.gowild.mobile.libcommon.utils.ToastUtils;
import com.gowild.mobile.libp2sp.core.exception.NotConnectedException;
import com.gowild.mobile.libp2sp.core.listener.IDataListener;
import com.gowild.mobile.libp2sp.netty.NettyClientManager;
import com.gowild.mobileclient.R;
import com.gowild.mobileclient.activity.GowildHomeActivity;
import com.gowild.mobileclient.activity.base.GowildHasTitileActivity;
import com.gowild.mobileclient.callback.OnNormalDialogListener;
import com.gowild.mobileclient.config.Constants;
import com.gowild.mobileclient.config.IntContants;
import com.gowild.mobileclient.config.KeysContainer;
import com.gowild.mobileclient.event.RefreshTimeEvent;
import com.gowild.mobileclient.manager.GowildAsyncHttpResponseHandler;
import com.gowild.mobileclient.manager.GowildHttpManager;
import com.gowild.mobileclient.manager.GowildTCPManager;
import com.gowild.mobileclient.manager.LoginMananger;
import com.gowild.mobileclient.misc.GowildResponseCode;
import com.gowild.mobileclient.protocol.MachineBothMsgProto;
import com.gowild.mobileclient.protocol.MachineUpgradeDescPro;
import com.gowild.mobileclient.protocol.SoundPro;
import com.gowild.mobileclient.vo.MachineTime;
import com.gowild.mobileclient.widget.dialog.GowildNormalDialog;
import com.gowild.mobileclient.widget.togglebutton.facebook.rebound.ToggleButton;
import com.loopj.android.http.RequestParams;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import butterknife.Bind;
import butterknife.OnClick;


/**
 * @author Zhangct
 * @version 1.0
 *          <p> 机器人设置界面 </p>
 * @since 2016/8/6 17:40
 */
public class GowildRobotConfigActivity extends GowildHasTitileActivity {

    // ===========================================================
    // HostConstants
    // ===========================================================

    private static final String TAG = GowildRobotConfigActivity.class.getSimpleName();

    // ===========================================================
    // Static Fields
    // ===========================================================

    // ===========================================================
    // Fields
    // ===========================================================
    private UnbindRobotCallback mUnbindRobotCallback;
    //机器人图标
    //    @Bind(R.id.iv_robot_icon)
    //    ImageView mGowildRobotIconIv;
    //机器人系统版本
    @Bind(R.id.tv_robot_version)
    TextView mGowildRobotVersionTv;
    //机器人ID
    @Bind(R.id.tv_robot_id)
    TextView mGowildIdTv;
    //机器人状态
    @Bind(R.id.tv_robot_state)
    TextView mGowildRobotStateTv;
    //立即更新
    @Bind(R.id.btn_update_now)
    TextView mGowildUpdateNowBtn;
    //解绑
    @Bind(R.id.btn_unbind)
    TextView mGowildUnbindBtn;
    //声音调节
    @Bind(R.id.sb_robot_sound)
    SeekBar  mGowildRobotSoundSb;
    //语速调节
    @Bind(R.id.sb_robot_speed)
    SeekBar  mGowildRobotSpeedSb;

    @Bind(R.id.setting_toggle)
    ToggleButton mToggleButton;

    @Bind(R.id.setting_start)
    TextView       startTime;
    @Bind(R.id.setting_end)
    TextView       endTime;
    @Bind(R.id.rl_start)
    RelativeLayout Rlstart;
    @Bind(R.id.rl_end)
    RelativeLayout Rlend;
    @Bind(R.id.setting_next)
    ImageView      ivNext;

    @Bind(R.id.iv_robot_sn)
    TextView sn;


    String mac = "";

    private MachineInfoListener mMachineInfoListener = new MachineInfoListener();

    private MachineUpgradeListener mMachineUpgradeListener         = new MachineUpgradeListener();
    private SoundChangeListener    mSoundChangeListener            = new SoundChangeListener();
    //
    //    private Button mGowildUpdateInBackgroundBtn = null;
    //    private Button mGowildUpdateCancelBtn = null;
    private Button                 mGowildUpdateRequireCancelBtn   = null;
    private Button                 mGowildUpdateRequireDownloadBtn = null;
    private TextView               mGowildRequireTitleTv           = null;
    private TextView               mGowildRequireContentTv         = null;
    private GowildNormalDialog mDialog;
    private AlertDialog        noticeDialog;
    private final int MESSAGE_MACHINEUPGRADE = 1;

    private final int MESSAGE_MACHINEINFO = 2;

    /**
     * 当前机器已经是最新版本
     */
    private final int MACHINE_IS_NEW = 0;

    // ===========================================================
    // Constructors
    // ===========================================================


    // ===========================================================
    // Getter or Setter
    // ===========================================================


    // ===========================================================
    // Methods for/from SuperClass/Interfaces
    // ===========================================================

    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case MESSAGE_MACHINEUPGRADE:
                    MachineUpgradeDescPro.MachineUpgradeDescMsg upgradeInfo = (MachineUpgradeDescPro.MachineUpgradeDescMsg) msg.obj;
                    if (upgradeInfo.getType() == MACHINE_IS_NEW) {
                        ToastUtils.showCustom(GowildRobotConfigActivity.this, "当前机器已经是最新版本，不能再升级了哦", false);
                    } else {
                        //询问用户是否升级
                        showMachineUpdateDialog(upgradeInfo);
                    }
                    break;
                case MESSAGE_MACHINEINFO:
                    MachineBothMsgProto.MachineInfoMsg data = (MachineBothMsgProto.MachineInfoMsg) msg.obj;
                    mGowildRobotSpeedSb.setProgress(data.getTalkSpeed());
                    mGowildRobotSoundSb.setProgress(data.getVolume());
                    String version = getStringById(R.string.gowild_setting_robot_version) + data.getVersion();
                    mGowildRobotVersionTv.setText(version);
                    String robotState = data.getState() == Constants.ROBOT_CONNECTION ? "已绑定" : "未绑定";
                    mGowildRobotStateTv.setText(robotState);
                    break;
            }
        }
    };


    @Override
    protected void onResume() {
        super.onResume();
        mUnbindRobotCallback = new UnbindRobotCallback(this);
        NettyClientManager.getInstance().addHandlerListener(GowildResponseCode.ROBOT_INFO_SYNC, mMachineInfoListener);
        NettyClientManager.getInstance().addHandlerListener(GowildResponseCode.MACHINE_UPGRADEINFO_RESULT, mMachineUpgradeListener);
        NettyClientManager.getInstance().addHandlerListener(GowildResponseCode.SOUND_CHANGE, mSoundChangeListener);
        initData();
    }

    @Override
    protected void onPause() {
        super.onPause();
        NettyClientManager.getInstance().removeHandlerListener(GowildResponseCode.ROBOT_INFO_SYNC, mMachineInfoListener);
        NettyClientManager.getInstance().removeHandlerListener(GowildResponseCode.MACHINE_UPGRADEINFO_RESULT, mMachineUpgradeListener);
        NettyClientManager.getInstance().removeHandlerListener(GowildResponseCode.SOUND_CHANGE, mSoundChangeListener);
    }

    @Override
    protected int onSetContentView() {
        return R.layout.activity_gowild_setting_robot_config;
    }

    @Override
    protected void initEvent() {
        showLoading("加载中");
        updateMachineInfo();
        setTitle(R.string.gowild_setting_robot_config);
    }

    @OnClick({R.id.btn_update_now, R.id.btn_unbind, R.id.setting_next})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_unbind:
                unbindRobot();
                break;
            case R.id.setting_next:
                Intent intent = new Intent(this, GowildTimeSettingActivity.class);
                intent.putExtra(Constants.STARTTIME, startTime.getText().toString().trim());
                intent.putExtra(Constants.ENDTIME, endTime.getText().toString().trim());
                startActivity(intent);
                break;
            case R.id.btn_update_now:
                //检查机器版本
                try {
                    GowildTCPManager.requestMachineUpgradeDesc();
                } catch (NotConnectedException e) {
                    e.printStackTrace();
                }
            default:
                break;
        }
    }

    @Override
    protected void initView() {
        super.initView();
        mGowildRobotSpeedSb.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                RequestParams params = new RequestParams(KeysContainer.KEY_TALK_SPEED, seekBar.getProgress());
                GowildHttpManager.updateTalkSpeedOrVolume(getBaseContext(), params, new GowildAsyncHttpResponseHandler(getBaseContext()) {
                    @Override
                    public void onSuccess(int statusCode, Object responseBody) {

                    }

                    @Override
                    public void onFailure(int statusCode, String responseBody) {

                    }
                });
            }
        });
        mGowildRobotSoundSb.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                RequestParams params = new RequestParams(KeysContainer.KEY_VOLUME, seekBar.getProgress());
                GowildHttpManager.updateTalkSpeedOrVolume(getBaseContext(), params, new GowildAsyncHttpResponseHandler(getBaseContext()) {
                    @Override
                    public void onSuccess(int statusCode, Object responseBody) {

                    }

                    @Override
                    public void onFailure(int statusCode, String responseBody) {

                    }
                });
            }
        });
        mToggleButton.setOnToggleChanged(new ToggleButton.OnToggleChanged() {
            @Override
            public void onToggle(boolean on) {
                //打开要做的事
                if (on) {
                    requestSlience(false);
                } else {
                    //关闭要做的事
                    requestSlience(true);
                }
            }
        });
    }

    private void requestSlience(boolean isSilence) {
        RequestParams params = new RequestParams();
        params.put("isSilence", (isSilence ? 0 : 1));
        params.put("silenceStart", startTime.getText().toString().trim());
        params.put("silenceEnd", endTime.getText().toString().trim());
        GowildHttpManager.requestUpdateTime(this, params, new GowildAsyncHttpResponseHandler<String>(this) {
            @Override
            public void onSuccess(int statusCode, String responseBody) {
                updateMachineInfo();
            }

            @Override
            public void onFailure(int statusCode, String responseBody) {

            }
        });
    }

    @Override
    protected void loadData() {
        super.loadData();

    }

    private void updateMachineInfo() {
        GowildHttpManager.requestMachineTime(this, new GowildAsyncHttpResponseHandler<MachineTime>(this) {

            @Override
            public void onSuccess(int statusCode, MachineTime responseBody) {
                hideLoading();
                if (responseBody.isSilence == 1) {
                    mToggleButton.setToggleOn();

                    showTime(true);
                } else {
                    mToggleButton.setToggleOff();
                    showTime(false);
                }
                startTime.setText(responseBody.silenceStart);
                endTime.setText(responseBody.silenceEnd);
            }

            @Override
            public void onFailure(int statusCode, String responseBody) {
                hideLoading();
            }
        });
    }

    private void showTime(boolean show) {
        if (show) {
            Rlstart.setVisibility(View.VISIBLE);
            Rlend.setVisibility(View.VISIBLE);
            ivNext.setVisibility(View.VISIBLE);
        } else {
            Rlstart.setVisibility(View.GONE);
            Rlend.setVisibility(View.GONE);
            ivNext.setVisibility(View.GONE);
        }
    }

    // ===========================================================
    // Methods
    // ===========================================================
    private void initData() {
        Bundle bundle = getIntent().getExtras();
        mac = bundle.getString(KeysContainer.KEY_MAC);
        mGowildIdTv.setText(mac);
        sn.setText(bundle.getString(KeysContainer.SN));
        try {
            GowildTCPManager.requestRobotRequestSync();
        } catch (NotConnectedException e) {
            e.printStackTrace();
        }
    }

    /**
     * 弹出机器更新信息
     */
    private void showMachineUpdateDialog(final MachineUpgradeDescPro.MachineUpgradeDescMsg upgradeInfo) {
        View proView = View.inflate(this, R.layout.gowild_update_require, null);
        mGowildRequireTitleTv = (TextView) proView.findViewById(R.id.tv_update_require_title);
        mGowildRequireContentTv = (TextView) proView.findViewById(R.id.tv_update_require_content);
        mGowildUpdateRequireCancelBtn = (Button) proView.findViewById(R.id.btn_update_require_cancel);
        mGowildUpdateRequireDownloadBtn = (Button) proView.findViewById(R.id.btn_update_require_download);
        mGowildUpdateRequireDownloadBtn.setText("立即升级");
        mGowildRequireTitleTv.setText("机器升级");
        mGowildRequireContentTv.setText("当前机器有最新版本,是否升级？");
        if (noticeDialog == null) {
            noticeDialog = new AlertDialog.Builder(new ContextThemeWrapper(this, R.style.Translucent_NoTitle)).create();
            mGowildUpdateRequireCancelBtn.setText(getResources().getString(R.string.update_require_cancel));
            //取消或者退出应用
            mGowildUpdateRequireCancelBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    noticeDialog.dismiss();
                }
            });
            //下载更新
            mGowildUpdateRequireDownloadBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    noticeDialog.dismiss();
                    updateMachine();
                }
            });
            noticeDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            noticeDialog.setView(proView, 0, 0, 0, 0);
            noticeDialog.show();
            noticeDialog.setCanceledOnTouchOutside(false);
            noticeDialog.setCancelable(false);
        } else {
            noticeDialog.show();
        }

    }

    /**
     * 升级机器
     */
    private void updateMachine() {
        try {
            GowildTCPManager.requestMachineUpdate();
        } catch (NotConnectedException e) {
            e.printStackTrace();
        }
    }

    /**
     * 解绑机器人
     */
    private void unbindRobot() {

        if (mDialog == null) {
            mDialog = new GowildNormalDialog();
            mDialog.setCancelable(false);
            mDialog.setDialogListener(new OnNormalDialogListener() {
                @Override
                public void onCancelListener() {

                }

                @Override
                public void onConfirmListener() {
                    RequestParams params = new RequestParams(KeysContainer.KEY_MAC, mac);
                    showLoading(R.string.loading);
                    GowildHttpManager.unbindRobot(getBaseContext(), params, mUnbindRobotCallback);
                }
            });
            mDialog.setDialogTitleVi(false);
            mDialog.setDialogCancel(getString(R.string.gowild_cancel));
            mDialog.setDialogConfirm(getString(R.string.gowild_ok));
            mDialog.setDialogTitle(getString(R.string.gowild_prompt));
        }
        mDialog.setDialogContent(getString(R.string.gowild_setting_robot_unbind_tip));
        mDialog.show(getFragmentManager(), GowildNormalDialog.class.getSimpleName());

    }

    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================

    private class UnbindRobotCallback extends GowildAsyncHttpResponseHandler<String> {
        public UnbindRobotCallback(Context context) {
            super(context);
        }

        @Override
        public void onSuccess(int statusCode, String responseBody) {
            hideLoading();
            ToastUtils.showCustom(getBaseContext(), R.string.gowild_setting_robot_unbind_robot_success, false);
            LoginMananger.getInstence().setType(IntContants.TYPE_ROBOT_NO_ROBOT);
            EventBus.getDefault().post("finish");
            startActivity(new Intent(GowildRobotConfigActivity.this, GowildHomeActivity.class));
            finish();
        }

        @Override
        public void onFailure(int statusCode, String responseBody) {
            ToastUtils.showCustom(getBaseContext(), responseBody, false);
            hideLoading();
        }
    }

    private class MachineInfoListener implements IDataListener<MachineBothMsgProto.MachineInfoMsg> {

        @Override
        public void onReceiveData(MachineBothMsgProto.MachineInfoMsg data) {
            Message msg = mHandler.obtainMessage();
            msg.obj = data;
            msg.what = MESSAGE_MACHINEINFO;
            mHandler.sendMessage(msg);
        }

        @Override
        public void onActiveChanged(boolean isActive) {

        }
    }

    private class SoundChangeListener implements IDataListener<SoundPro.IntMsg> {

        @Override
        public void onReceiveData(final SoundPro.IntMsg data) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mGowildRobotSoundSb.setProgress(data.getValue());
                }
            });
        }

        @Override
        public void onActiveChanged(boolean isActive) {

        }
    }

    private class MachineUpgradeListener implements IDataListener<MachineUpgradeDescPro.MachineUpgradeDescMsg> {

        @Override
        public void onReceiveData(MachineUpgradeDescPro.MachineUpgradeDescMsg data) {
            Message msg = mHandler.obtainMessage();
            msg.obj = data;
            msg.what = MESSAGE_MACHINEUPGRADE;
            mHandler.sendMessage(msg);
        }

        @Override
        public void onActiveChanged(boolean isActive) {

        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void subTimeEvent(RefreshTimeEvent event) {
        updateMachineInfo();
    }
}
