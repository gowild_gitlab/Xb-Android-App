/**
 * Copyright (C)  2016 深圳市狗尾草智能科技有限公司
 * GowildXB
 * GowildBindSuccessActivity.java
 */
package com.gowild.mobileclient.activity.login;

import android.os.CountDownTimer;
import android.view.View;
import android.widget.TextView;

import com.gowild.mobileclient.R;
import com.gowild.mobileclient.activity.GowildHomeActivity;
import com.gowild.mobileclient.activity.base.GowildLoginAndRegisterActivity;

import org.greenrobot.eventbus.EventBus;

import butterknife.Bind;
import butterknife.OnClick;

/**
 * @author shuai. wang
 * @version 1.0
 *          <p><strong>Features draft description.主要功能介绍</strong></p>
 * @since 2016/7/29 20:53
 */
public class GowildBindSuccessActivity extends GowildLoginAndRegisterActivity {

	// ===========================================================
	// HostConstants
	// ===========================================================

	private static final String TAG = GowildBindSuccessActivity.class.getSimpleName();
	@Bind(R.id.gowild_home_setting_tv)
	TextView mGowildHomeSettingTv;
	@Bind(R.id.gowild_bind_start)
	TextView mGowildBindStart;
	private CountDownTimer mCountDownTimer;


	// ===========================================================
	// Static Fields
	// ===========================================================

	// ===========================================================
	// Fields
	// ===========================================================


	// ===========================================================
	// Constructors
	// ===========================================================


	// ===========================================================
 	// ===========================================================


	// ===========================================================
	// Methods for/from SuperClass/Interfaces
	// ===========================================================
	@Override
	public int getLayout() {
		return R.layout.activity_gowild_bind;
	}

	@Override
	protected void initData() {
	}

	@Override
	protected void initEvent() {
		//发送验证码的计时器
		mCountDownTimer = new CountDownTimer(5000, 1000) {
			@Override
			public void onTick(long millisUntilFinished) {
				mGowildHomeSettingTv.setText(String.format(getString(R.string.gowild_bind_sucess), Long.toString(millisUntilFinished / 1000)));
			}

			@Override
			public void onFinish() {
				EventBus.getDefault().post("finish");
				nextActivtiy(GowildHomeActivity.class);
			}
		}.start();

	}


	// ===========================================================
	// Methods
	// ===========================================================
	@OnClick({R.id.gowild_bind_start, R.id.gowild_home_setting_tv})
	public void onClick(View view) {
		switch (view.getId()) {
			case R.id.gowild_bind_start:
				mCountDownTimer.cancel();
				EventBus.getDefault().post("finish");
				nextActivtiy(GowildHomeActivity.class);
				break;
			case R.id.gowild_home_setting_tv:
				break;

			default:
				break;
		}
	}

	// ===========================================================
	// Inner and Anonymous Classes
	// ===========================================================

}
