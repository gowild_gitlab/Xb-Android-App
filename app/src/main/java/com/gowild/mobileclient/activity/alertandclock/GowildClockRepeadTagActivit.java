/**
 * Copyright (C)  2016 深圳市狗尾草智能科技有限公司
 * GowildXB
 * GowildClockRepeadTagActivit.java
 */
package com.gowild.mobileclient.activity.alertandclock;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckedTextView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.gowild.mobileclient.R;
import com.gowild.mobileclient.base.baseactivity.GowildBaseActivity;
import com.gowild.mobileclient.config.Constants;

/**
 * @author Administrator
 * @version 1.0
 *          <p><strong>Features draft description.主要功能介绍</strong></p>
 * @since 2016/8/3 9:26
 */
public class GowildClockRepeadTagActivit extends GowildBaseActivity {

    // ===========================================================
    // HostConstants
    // ===========================================================

    private static final String TAG = GowildClockRepeadTagActivit.class.getSimpleName();


    // ===========================================================
    // Static Fields
    // ===========================================================

    // ===========================================================
    // Fields
    // ===========================================================
    private CheckedTextView mTvMonday, mTvTuesday, mTvWednesday, mTvThuesday, mTvFirday, mSaturday, mSunday, mNever;
    private OnClickCallback mOnClickCallback = new OnClickCallback();
    private int mCycleIndex = 0;
    // ===========================================================
    // Constructors
    // ===========================================================


    // ===========================================================
    // Getter or Setter
    // ===========================================================


    // ===========================================================
    // Methods for/from SuperClass/Interfaces
    // ===========================================================
    @Override
    protected int onSetTopbarView() {
        return R.layout.activity_gowild_clock_repead_tags_topbar;
    }

    @Override
    protected int onSetContentView() {
        return R.layout.activity_gowild_clock_repead_tags;
    }

    @Override
    protected int onSetTopBarHeight() {
        return getResources().getDimensionPixelSize(R.dimen.gowild_160px);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mTvMonday = (CheckedTextView) findViewById(R.id.ctvmonday);
        mTvMonday.setOnClickListener(mOnClickCallback);
        mTvTuesday = (CheckedTextView) findViewById(R.id.ctvtuesday);
        mTvTuesday.setOnClickListener(mOnClickCallback);
        mTvWednesday = (CheckedTextView) findViewById(R.id.ctvwednesday);
        mTvWednesday.setOnClickListener(mOnClickCallback);
        mTvThuesday = (CheckedTextView) findViewById(R.id.ctvthuesday);
        mTvThuesday.setOnClickListener(mOnClickCallback);
        mTvFirday = (CheckedTextView) findViewById(R.id.ctvfriday);
        mTvFirday.setOnClickListener(mOnClickCallback);
        mSaturday = (CheckedTextView) findViewById(R.id.ctvsaturday);
        mSaturday.setOnClickListener(mOnClickCallback);
        mSunday = (CheckedTextView) findViewById(R.id.ctvsunday);
        mSunday.setOnClickListener(mOnClickCallback);
        mNever = (CheckedTextView) findViewById(R.id.ctvnever);
        mNever.setOnClickListener(mOnClickCallback);
        RelativeLayout back = (RelativeLayout) findViewById(R.id.back);
        back.setOnClickListener(mOnClickCallback);
        TextView commit = (TextView) findViewById(R.id.commit);
        commit.setOnClickListener(mOnClickCallback);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mCycleIndex = getIntent().getIntExtra(Constants.CYCLE_INDEX, 0);
        initViews(mCycleIndex);

    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent();
        intent.putExtra(Constants.INFO, mCycleIndex);
        setResult(1, intent);
        finish();
    }

    // ===========================================================
    // Methods
    // ===========================================================
    public void setRepeat(int day, boolean repeat) {
        if (repeat) {
            mCycleIndex |= (1 << (day % 7));
        } else {
            mCycleIndex &= ~(1 << (day % 7));
        }
    }

    private void initViews(int cycleindex) {
        CheckedTextView[] arr = {mTvMonday, mTvTuesday, mTvWednesday, mTvThuesday, mTvFirday, mSaturday, mSunday};
        if (cycleindex == 0) {
            mTvMonday.setChecked(false);
            mTvTuesday.setChecked(false);
            mTvWednesday.setChecked(false);
            mTvThuesday.setChecked(false);
            mTvFirday.setChecked(false);
            mSaturday.setChecked(false);
            mSunday.setChecked(false);
            mNever.setChecked(true);
        } else {
            mNever.setChecked(false);
            for (int i = 1; i <= 7; i++) {
                if ((cycleindex & (1 << (i % 7))) != 0) {
                    arr[i - 1].setChecked(true);
                } else {
                    arr[i - 1].setChecked(false);
                }
            }
        }
    }

    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================
    private class OnClickCallback implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.back:
                    onBackPressed();
                    break;
                case R.id.commit:
                    onBackPressed();
                    break;
                case R.id.ctvmonday:
                    if (mTvMonday.isChecked()) {
                        setRepeat(1, false);
                    } else {
                        setRepeat(1, true);
                    }
                    initViews(mCycleIndex);
                    break;
                case R.id.ctvtuesday:
                    if (mTvTuesday.isChecked()) {
                        setRepeat(2, false);
                    } else {
                        setRepeat(2, true);
                    }
                    initViews(mCycleIndex);
                    break;
                case R.id.ctvwednesday:
                    if (mTvWednesday.isChecked()) {
                        setRepeat(3, false);
                    } else {
                        setRepeat(3, true);
                    }
                    initViews(mCycleIndex);
                    break;
                case R.id.ctvthuesday:
                    if (mTvThuesday.isChecked()) {
                        setRepeat(4, false);
                    } else {
                        setRepeat(4, true);
                    }
                    initViews(mCycleIndex);
                    break;
                case R.id.ctvfriday:
                    if (mTvFirday.isChecked()) {
                        setRepeat(5, false);
                    } else {
                        setRepeat(5, true);
                    }
                    initViews(mCycleIndex);
                    break;
                case R.id.ctvsaturday:
                    if (mSaturday.isChecked()) {
                        setRepeat(6, false);
                    } else {
                        setRepeat(6, true);
                    }
                    initViews(mCycleIndex);
                    break;
                case R.id.ctvsunday:
                    if (mSunday.isChecked()) {
                        setRepeat(7, false);
                    } else {
                        setRepeat(7, true);
                    }
                    initViews(mCycleIndex);
                    break;
                case R.id.ctvnever:
                    mCycleIndex = 0;
                    initViews(mCycleIndex);
                    break;
            }
        }
    }
}
