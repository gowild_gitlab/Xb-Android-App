/**
 * Copyright (C)  2016 深圳市狗尾草智能科技有限公司
 * GowildXB
 * GowildContantsActivity.java
 */
package com.gowild.mobileclient.activity.love;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.gowild.mobileclient.R;
import com.gowild.mobileclient.activity.base.GowildLoginAndRegisterActivity;
import com.gowild.mobileclient.event.UnBindLover;
import com.gowild.mobileclient.manager.GowildAsyncHttpResponseHandler;
import com.gowild.mobileclient.manager.GowildHttpManager;
import com.gowild.mobileclient.model.LunchBack;
import com.gowild.mobileclient.widget.MyToolBar;
import com.loopj.android.http.RequestParams;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * @author shuai. wang
 * @version 1.0
 *          <p><strong>Features draft description.主要功能介绍</strong></p>
 * @since 2016/8/3 19:15
 */
public class GowildContantsActivity extends GowildLoginAndRegisterActivity {

    // ===========================================================
    // HostConstants
    // ===========================================================

    private static final String TAG = GowildContantsActivity.class.getSimpleName();
    @Bind(R.id.gowild_contacts_tv_one)
    TextView mGowildContactsTvOne;
    @Bind(R.id.gowild_contacts_tv_two)
    TextView mGowildContactsTvTwo;
    @Bind(R.id.gowild_contacts_tv_three)
    TextView mGowildContactsTvThree;
    @Bind(R.id.gowild_contacts_tv_four)
    TextView mGowildContactsTvFour;
    @Bind(R.id.gowild_contacts_bt)
    Button   mGowildContactsBt;
    @Bind(R.id.robot)
    ImageView mRobot;


    @Override
    public int getLayout() {
        return R.layout.activity_gowild_contants;
    }


    @Override
    protected void initData() {
//        mRobot.setImageResource( CommonDataUtil.getCurrentRobotTitle().equals("小白") ? R.drawable.gowild_xb_defoult : R.drawable.gowild_xb_hui);
    }

    @Override
    protected void initEvent() {
        MyToolBar toolbar = (MyToolBar) findViewById(R.id.view_toolbar);
        toolbar.setTitle(R.string.gowild_lover_dui);
        toolbar.setActionStr(R.string.gowild_lover_ta);
        toolbar.setActionListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                nextActivtiy(GowildLoverInfoActivity.class);
            }
        });
    }

    @OnClick({R.id.gowild_contacts_tv_one, R.id.gowild_contacts_tv_two,
              R.id.gowild_contacts_tv_three, R.id.gowild_contacts_tv_four, R.id.gowild_contacts_bt})
    public void onClick(View view) {
        String action = null;
        switch (view.getId()) {
            case R.id.gowild_contacts_tv_one:
                action = "singleTapTop";
                sendMessageTo(action);
                break;
            case R.id.gowild_contacts_tv_two:
                action = "swipeGesture";
                sendMessageTo(action);
                break;
            case R.id.gowild_contacts_tv_three:
                action = "pinch";
                sendMessageTo(action);
                break;
            case R.id.gowild_contacts_tv_four:
                action = "singleTapBottom";
                sendMessageTo(action);
                break;
            case R.id.gowild_contacts_bt:
                nextActivtiy(GowildLoveTalkActivity.class);
                break;
        }
    }


    private void sendMessageTo(String action) {
        showLoading("加载中");
        RequestParams params = new RequestParams("content", action);
        GowildHttpManager.sendMessage(this, params, new GowildAsyncHttpResponseHandler<LunchBack>(GowildContantsActivity.this) {
            @Override
            public void onSuccess(int statusCode, LunchBack responseBody) {
                hideLoading();
            }

            @Override
            public void onFailure(int statusCode, String responseBody) {
                hideLoading();
            }
        });
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void subUnbindLover(UnBindLover unBindLover) {
        finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
    }
}
