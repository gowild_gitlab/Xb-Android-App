/**
 * Copyright (C)  2016 深圳市狗尾草智能科技有限公司
 * GowildXB
 * GowildQRActivity.java
 */
package com.gowild.mobileclient.activity.setting;

import android.graphics.Bitmap;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.gowild.mobile.libcommon.utils.EncodingUtils;
import com.gowild.mobileclient.R;
import com.gowild.mobileclient.activity.GowildHomeActivity;
import com.gowild.mobileclient.activity.base.GowildHasTitileActivity;
import com.gowild.mobileclient.config.Constants;
import com.gowild.mobileclient.config.KeysContainer;
import com.gowild.mobileclient.event.BindResult;
import com.gowild.mobileclient.event.RobotOnline;
import com.gowild.mobileclient.manager.LoginMananger;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import butterknife.Bind;
import butterknife.OnClick;

/**
 * @author Zhangct
 * @version 1.0
 *          <p> 主要功能介绍 </p>
 * @since 2016/8/6 16:28
 */
public class GowildQRActivity extends GowildHasTitileActivity {

    // ===========================================================
    // HostConstants
    // ===========================================================

    private static final String TAG = GowildQRActivity.class.getSimpleName();

    // ===========================================================
    // Static Fields
    // ===========================================================

    // ===========================================================
    // Fields
    // ===========================================================
    @Bind(R.id.gowild_qr_tv)
    ImageView mGowildQrTv;
    //重新设置网络
    @Bind(R.id.btn_qr_setting)
    Button mGowildQrResetBtn;
    private String mPwd;
    private String mSsid;
    private String mJson;
    private String mTitle = null;


    // ===========================================================
    // Constructors
    // ===========================================================


    // ===========================================================
    // Getter or Setter
    // ===========================================================


    // ===========================================================
    // Methods for/from SuperClass/Interfaces
    // ===========================================================


    @Override
    protected int onSetContentView() {
        return R.layout.activity_gowild_wifi_qr_code;
    }

    @Override
    protected void initEvent() {
        mJson = getIntent().getStringExtra(Constants.SSID);
        mTitle = getIntent().getStringExtra(KeysContainer.KEY_ROBOT_BIND_TITLE);
        if (TextUtils.isEmpty(mTitle)) {
            mTitle = getString(R.string.gowild_setting_robot_config_to_bind_net);
        }
        setTitle(mTitle);

        Bitmap bitmap = EncodingUtils.createQRCode(mJson, 500, 500, null);
        mGowildQrTv.setImageBitmap(bitmap);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void subBindResult(BindResult result) {
        //订阅绑定的事件,如果是上线重新设置密码就不会出现这种问题
        if (LoginMananger.getInstence().getIsSettingOpen()) {
            startActivity(GowildSettingActivity.class);
        }else{
            startActivity(GowildHomeActivity.class);
        }
        finish();

    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void subRobotOnline(RobotOnline online) {
        if (online.result) {
            //已经绑定,更换wifi成功
            if (LoginMananger.getInstence().getIsSettingOpen()) {
                startActivity(GowildSettingActivity.class);
            } else {
                startActivity(GowildHomeActivity.class);
            }
        }
    }


    @Override
    protected void initView() {
    }


    @OnClick({R.id.btn_qr_setting})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_qr_setting:
//                preActivtiy(GowildWifiConfigActivity.class);
                finish();
            default:
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    // ===========================================================
    // Methods
    // ===========================================================


    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================

}
