/**
 * Copyright (C)  2016 深圳市狗尾草智能科技有限公司
 * GowildXB
 * GowildLoverActivity.java
 */
package com.gowild.mobileclient.activity.love;

import android.content.Intent;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.gowild.mobile.libcommon.utils.SpUtils;
import com.gowild.mobile.libcommon.utils.ToastUtils;
import com.gowild.mobile.libhttp.ObjectAsynHttpResponseHandler;
import com.gowild.mobileclient.R;
import com.gowild.mobileclient.activity.base.GowildLoginAndRegisterActivity;
import com.gowild.mobileclient.base.baseadapter.HomeBaseAdapter;
import com.gowild.mobileclient.base.baseadapter.ViewHolder;
import com.gowild.mobileclient.config.Constants;
import com.gowild.mobileclient.manager.GowildAsyncHttpResponseHandler;
import com.gowild.mobileclient.manager.GowildHttpManager;
import com.gowild.mobileclient.vo.LoverList;
import com.gowild.mobileclient.widget.MyToolBar;
import com.gowild.mobileclient.widget.PullToRefreshLayout;
import com.gowild.mobileclient.widget.PullableListView;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.apache.http.Header;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;

/**
 * @author shuai. wang
 * @version 1.0
 *          <p><strong>Features draft description.主要功能介绍</strong></p>
 * @since 2016/7/26 19:55
 */
public class GowildLoverActivity extends GowildLoginAndRegisterActivity {

    // ===========================================================
    // HostConstants
    // ===========================================================

    private static final String TAG = GowildLoverActivity.class.getSimpleName();

    @Bind(R.id.gowild_lover_lv)
    PullableListView mGowildLoverLv;
    @Bind(R.id.refresh_view)
    PullToRefreshLayout mRefreshView;
    private List<LoverList.Entity> mList = new ArrayList<>();
    private MyAdapter mAdapter;
    private EditText mGowildLoverPhone;
    private ImageView mDelete;
    private TextView mGowildLoverReq;

    @Override
    public int getLayout() {
        return R.layout.activity_gowild_loverlist;
    }

    @Override
    protected void initView() {
        MyToolBar toolbar = (MyToolBar) findViewById(R.id.view_toolbar);
        toolbar.setTitle(getResources().getString(R.string.lover));
    }

    @Override
    protected void initData() {
        //获得配对情侣的消息
        getLoverInfo();
    }

    @Override
    protected void initEvent() {
        View view = View.inflate(this,R.layout.activity_gowild_lover,null);
        mGowildLoverPhone = (EditText) view.findViewById(R.id.gowild_lover_phone);
        mDelete = (ImageView) view.findViewById(R.id.delete);
        mGowildLoverReq = (TextView) view.findViewById(R.id.gowild_lover_req);

        view.findViewById(R.id.gowild_lover_tv_start).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            //发起网络请求
            startJionLover();

            }
        });
        mGowildLoverLv.addHeaderView(view);
        if (mAdapter == null)
            mAdapter = new MyAdapter(mGowildLoverLv, mList);
        mGowildLoverLv.setAdapter(mAdapter);
        mRefreshView.setPullUpToLoadEnable(false);
        mRefreshView.setOnRefreshListener(new PullToRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh(PullToRefreshLayout pullToRefreshLayout) {
                //刷新数据
                getLoverInfo();
            }

            @Override
            public void onLoadMore(PullToRefreshLayout pullToRefreshLayout) {
                //加载更多
            }
        });

        mDelete.setVisibility(View.INVISIBLE);
        mGowildLoverPhone.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() == 0) {
                    mDelete.setVisibility(View.GONE);
                } else {
                    mDelete.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        mDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mGowildLoverPhone.setText("");
            }
        });

    }

    /**
     * 请求配对消息
     */
    private void getLoverInfo() {
        GowildHttpManager.requestLoverRequest(this, new ObjectAsynHttpResponseHandler<LoverList>() {
            @Override
            public void onResponseSuccess(int statusCode, Header[] headers, LoverList response) {
                mRefreshView.refreshFinish(PullToRefreshLayout.SUCCEED);
                if (response.code.equals(Constants.SUCCESS)) {
                    if (response.data != null) {
                        mList = response.data;
                        if (mList.size() == 0) {
                        } else {
                            mGowildLoverReq.setVisibility(View.VISIBLE);
                        }
                        mAdapter.setDate(mList);
                    } else {
                        mGowildLoverReq.setVisibility(View.GONE);
                    }
                }
            }

            @Override
            public void onResponseFailure(int statusCode, Header[] headers, LoverList response, Throwable error) {
                //获得失败
                mRefreshView.refreshFinish(PullToRefreshLayout.FAIL);
            }
        });
    }

    private class MyAdapter extends HomeBaseAdapter<LoverList.Entity> {


        public MyAdapter(AbsListView absListView, List datas) {
            super(absListView, datas);
        }

        @Override
        protected ViewHolder getViewHolder() {
            MyHolder mHolder = new MyHolder();
            return mHolder;
        }
    }


    private class MyHolder extends ViewHolder<LoverList.Entity> {

        private TextView mTextView;
        private Button mButton;

        @Override
        public View initHolderView() {
            View view = View.inflate(GowildLoverActivity.this, R.layout.view_gowild_lover_item, null);
            mButton = (Button) view.findViewById(R.id.gowild_lover_lv_item_bt);
            mTextView = (TextView) view.findViewById(R.id.gowild_lover_lv_item_tv);
            return view;
        }

        @Override
        public void refreshHolderView(final LoverList.Entity data) {
            mButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    acceptLover(data.magicCode);
                }

            });
            mTextView.setText(String.format(getString(R.string.gowild_lover_request), data.nickname));
        }

    }

    private void acceptLover(long code) {
        //存储对方的数据,发起网络连接
        showLoading(R.string.loading);
        SpUtils.putLong(this, Constants.MAGICCODE, code);
        RequestParams params = new RequestParams(Constants.MAGICCODE, code);
        GowildHttpManager.requestAcceptLover(this, params, new GowildAsyncHttpResponseHandler<String>(this) {
            @Override
            public void onSuccess(int statusCode, String responseBody) {
                //接收成功
                hideLoading();
                nextActivtiy(GowildContantsActivity.class);
                finish();
            }

            @Override
            public void onFailure(int statusCode, String responseBody) {
                //接收失败
                hideLoading();
            }
        });
    }

    @Override
    public void nextActivtiy(Class clazz) {
        Intent intent = new Intent(GowildLoverActivity.this, clazz);
        startActivity(intent);
        // 关闭当前的activit
        finish();
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }



    private void startJionLover() {
        String phone = mGowildLoverPhone.getText().toString().trim();
        if (TextUtils.isEmpty(phone)) {
            ToastUtils.showCustom(GowildLoverActivity.this, getString(R.string.gowild_lover_phone_error), false);
            return;
        }
        RequestParams params = new RequestParams();
        params.add("relateAccountName", phone);
        GowildHttpManager.joinLover(this, params, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                try {
                    JSONObject json = new JSONObject(new String(responseBody));
                    if (json.optString("code").equals(Constants.SUCCESS)) {
                        //请求发出
                        ToastUtils.showCustom(GowildLoverActivity.this, getString(R.string.gowild_lover_send), false);
                    } else {
                        ToastUtils.showCustom(GowildLoverActivity.this, json.optString("desc"), false);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                ToastUtils.showCustom(GowildLoverActivity.this, getString(R.string.gowild_net_error), false);
            }
        });
    }

    // ===========================================================
    // Static Fields
    // ===========================================================

    // ===========================================================
    // Fields
    // ===========================================================


    // ===========================================================
    // Constructors
    // ===========================================================


    // ===========================================================
    // Getter or Setter
    // ===========================================================


    // ===========================================================
    // Methods for/from SuperClass/Interfaces
    // ===========================================================


    // ===========================================================
    // Methods
    // ===========================================================


    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================

}


