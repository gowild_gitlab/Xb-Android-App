/**
 * Copyright (C)  2016 深圳市狗尾草智能科技有限公司
 * GowildXB
 * GowildAboutActivity.java
 */
package com.gowild.mobileclient.activity.setting;

import android.content.Intent;
import android.net.Uri;
import android.view.View;
import android.widget.RelativeLayout;

import com.gowild.mobileclient.R;
import com.gowild.mobileclient.activity.base.GowildHasTitileActivity;
import com.gowild.mobileclient.widget.dialog.GowildCustomerServiceDialog;

import butterknife.Bind;
import butterknife.OnClick;

/**
 * @author Zhangct
 * @version 1.0
 *          <p> 主要功能介绍 </p>
 * @since 2016/8/6 18:42
 */
public class GowildAboutActivity extends GowildHasTitileActivity {

    // ===========================================================
    // HostConstants
    // ===========================================================

    private static final String TAG = GowildAboutActivity.class.getSimpleName();

    @Bind(R.id.rl_about_robot)
    RelativeLayout mGowildAboutRobotRl;
    @Bind(R.id.rl_about_company)
    RelativeLayout mGowildAboutCompanyRl;
    @Bind(R.id.rl_about_advise)
    RelativeLayout mGowildAboutAdviceRl;
    @Bind(R.id.rl_about_server)
    RelativeLayout mGowildAboutServerRl;

    //客服弹窗
    private GowildCustomerServiceDialog dialog = null;

    // ===========================================================
    // Static Fields
    // ===========================================================

    // ===========================================================
    // Fields
    // ===========================================================


    // ===========================================================
    // Constructors
    // ===========================================================


    // ===========================================================
    // Getter or Setter
    // ===========================================================


    // ===========================================================
    // Methods for/from SuperClass/Interfaces
    // ===========================================================
    @Override
    protected int onSetContentView() {
        return R.layout.activity_gowild_about;
    }

    @Override
    protected void initEvent() {
        setTitle(R.string.gowild_setting_about);
    }

    @OnClick({R.id.rl_about_robot, R.id.rl_about_company, R.id.rl_about_advise, R.id.rl_about_server})
    public void onclick(View view) {
        Intent intent = null;
        switch (view.getId()) {
            case R.id.rl_about_robot:
                startActivity(GowildRobotInfoActivity.class);
                break;
            case R.id.rl_about_company:
                String url = "http://www.gowild.cn/"; // web address
                intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse(url));
                startActivity(intent);
//                LoginMananger.getInstence().startCountPage(null,"company");
                break;
            case R.id.rl_about_advise:
                startActivity(GowildFeedbackActivity.class);
                break;
            case R.id.rl_about_server:
//                LoginMananger.getInstence().startCountPage(null,"server");
                if (null == dialog) {
                    dialog = new GowildCustomerServiceDialog();
                }
                dialog.show(getFragmentManager(), "service_dialog");
                break;
            default:
                break;
        }
    }

    // ===========================================================
    // Methods
    // ===========================================================


    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================

}
