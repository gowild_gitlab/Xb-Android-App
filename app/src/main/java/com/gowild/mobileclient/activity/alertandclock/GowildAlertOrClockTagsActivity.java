/**
 * Copyright (C)  2016 深圳市狗尾草智能科技有限公司
 * GowildXB
 * GowildAlertOrClockTagsActivity.java
 */
package com.gowild.mobileclient.activity.alertandclock;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.gowild.mobile.libcommon.utils.CheckContentUtil;
import com.gowild.mobile.libcommon.utils.ToastUtils;
import com.gowild.mobileclient.R;
import com.gowild.mobileclient.base.baseactivity.GowildBaseActivity;
import com.gowild.mobileclient.config.Constants;

/**
 * @author JunDong Wang
 * @version 1.0
 *          <p><strong>设置闹钟待办的TAG值 要设置标题</strong></p>
 * @since 2016/7/25 18:03
 */
public class GowildAlertOrClockTagsActivity extends GowildBaseActivity {

    // ===========================================================
    // HostConstants
    // ===========================================================

    private static final String TAG = GowildAlertOrClockTagsActivity.class.getSimpleName();


    // ===========================================================
    // Static Fields
    // ===========================================================

    // ===========================================================
    // Fields
    // ===========================================================
    private TextView mTvTitle;
    private OnClickCallback mOnClickCallback = new OnClickCallback();
    private EditText mEditText;
    private EditChangedListener mEditChangedListener = new EditChangedListener();
    private TextView mTvLimit;
    private boolean resetText;
    private TextView mCommit;
    private boolean mAlert;
    // ===========================================================
    // Constructors
    // ===========================================================


    // ===========================================================
    // Getter or Setter
    // ===========================================================


    // ===========================================================
    // Methods for/from SuperClass/Interfaces
    // ===========================================================

    @Override
    protected int onSetTopbarView() {
        return R.layout.activity_gowild_alert_clock_tags_topbar;
    }

    @Override
    protected int onSetContentView() {
        return R.layout.activity_gowild_alert_clock_tags;
    }

    @Override
    protected int onSetTopBarHeight() {
        return getResources().getDimensionPixelSize(R.dimen.gowild_160px);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mAlert = getIntent().getStringExtra(Constants.TYPE).equals("alert");
        if (mAlert) {
            mTvTitle.setText(R.string.gowild_alert_and_clock_alert_tag);
        } else {
            mTvTitle.setText(R.string.gowild_alert_and_clock_clock_tag);
        }
        mEditText.setText(getIntent().getStringExtra(Constants.CONTENT));
        String str = getResources().getString(R.string.gowild_alert_and_clock_count_of_word);
        str = str.replace("%", Integer.toString(mEditText.getText().toString().length()));
        mTvLimit.setText(str);
        mEditText.addTextChangedListener(mEditChangedListener);
    }


    @Override
    public void onBackPressed() {
        Intent intent = new Intent();
        if (TextUtils.isEmpty(mEditText.getText().toString().trim())) {
            if(mAlert){
                mEditText.setText("开会");
            }else{
                mEditText.setText("起床");
            }
        }
        if (CheckContentUtil.containsEmoji(mEditText.getText().toString().trim())){
            if(mAlert){
                mEditText.setText("开会");
            }else{
                mEditText.setText("起床");
            }
        }

        if (CheckContentUtil.containersSpecialChar(mEditText.getText().toString().trim())){
            if(mAlert){
                mEditText.setText("开会");
            }else{
                mEditText.setText("起床");
            }
        }

        if (CheckContentUtil.isContainChar(mEditText.getText().toString().trim())){
            if(mAlert){
                mEditText.setText("开会");
            }else{
                mEditText.setText("起床");
            }
        }
        intent.putExtra(Constants.CONTENT, mEditText.getText().toString());
        setResult(1, intent);
        finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mTvTitle = (TextView) findViewById(R.id.title);
        RelativeLayout back = (RelativeLayout) findViewById(R.id.back);
        back.setOnClickListener(mOnClickCallback);
        mCommit = (TextView) findViewById(R.id.commit);
        mCommit.setOnClickListener(mOnClickCallback);
        mEditText = (EditText) findViewById(R.id.tags);
        mTvLimit = (TextView) findViewById(R.id.limit);
    }
    // ===========================================================
    // Methods
    // ===========================================================


    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================
    private class OnClickCallback implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.back:
                    onBackPressed();
                    break;
                case R.id.commit:

                    if (TextUtils.isEmpty(mEditText.getText().toString().trim())) {
                        ToastUtils.showCustom(getApplicationContext(),"请输入正确的标签",false);
                        return;
                    }
                    if (CheckContentUtil.containsEmoji(mEditText.getText().toString().trim())){
                        ToastUtils.showCustom(getApplicationContext(),"不能有表情",false);
                        return;
                    }

                    if (CheckContentUtil.containersSpecialChar(mEditText.getText().toString().trim())){
                        ToastUtils.showCustom(getApplicationContext(),"不能含有特殊字符",false);
                        return;
                    }

                    if (CheckContentUtil.isContainChar(mEditText.getText().toString().trim())){
                        ToastUtils.showCustom(getApplicationContext(),"不能含有符号",false);
                        return;
                    }
                    onBackPressed();
                    break;
            }
        }
    }

    private class EditChangedListener implements TextWatcher {
        private CharSequence temp;//监听前的文本
        private int editStart;//光标开始位置
        private int editEnd;//光标结束位置
        private final int charMaxNum = 12;
        private Toast mToast;

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            temp = s;
            if (!resetText) {
                // 这里用s.toString()而不直接用s是因为如果用s，
                // 那么，inputAfterText和s在内存中指向的是同一个地址，s改变了，
                // inputAfterText也就改变了，那么表情过滤就失败了
            }

        }



        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
//            String str = getResources().getString(R.string.gowild_alert_and_clock_count_of_word);
//            str = str.replace("%", Integer.toString(s.length()));
//            mTvLimit.setText(str);

//            if (!resetText) {
//                if (count >= 2) {//表情符号的字符长度最小为2
//                    //如果是字母
//                    CharSequence input = s.subSequence(cursorPos, cursorPos + count);
//                    if (NumberUtils.containsEmoji(input.toString())) {
//                        resetText = true;
////                      Toast.makeText(getApplicationContext(), "不支持输入Emoji表情符号", Toast.LENGTH_SHORT).show();
//                        //是表情符号就将文本还原为输入表情符号之前的内容
//                        mEditText.setText(inputAfterText);
//                        CharSequence text = mEditText.getText();
//                        if (text instanceof Spannable) {
//                            Spannable spanText = (Spannable) text;
//                            Selection.setSelection(spanText, text.length());
//                        }
//                    }
//                } else if (count == 1) {
//                    CharSequence input = s.subSequence(cursorPos, cursorPos + count);
//                    if (NumberUtils.containsUnChar(input.toString())) {
//                        resetText = true;
////                        Toast.makeText(getApplicationContext(), "不支持输入特殊符号", Toast.LENGTH_SHORT).show();
//                        //是表情符号就将文本还原为输入表情符号之前的内容
//                        mEditText.setText(inputAfterText);
//                        CharSequence text = mEditText.getText();
//                        if (text instanceof Spannable) {
//                            Spannable spanText = (Spannable) text;
//                            Selection.setSelection(spanText, text.length());
//                        }
//                    }
//                }
//            } else {
//                resetText = false;
//            }
        }

        @Override
        public void afterTextChanged(Editable s) {
            editStart = mEditText.getSelectionStart();
            editEnd = mEditText.getSelectionEnd();
            if ((temp.toString().length()-1>=0)&&CheckContentUtil.containersSpecialChar(temp.toString().substring(temp.toString().length() - 1))&&CheckContentUtil.containsEmoji(temp.toString().substring(temp.toString().length() - 1))) {
                s.delete(editStart - 1, editEnd);
                int tempSelection = editStart;
                mEditText.setText(s);
                mEditText.setSelection(tempSelection);
                if (mToast == null) {
                    mToast = Toast.makeText(getApplicationContext(), "不能含有特殊字符！", Toast.LENGTH_LONG);
                }
                mToast.show();
            }



            if (temp.length() > charMaxNum) {
                if (mToast == null) {
                    mToast = Toast.makeText(getApplicationContext(), "您输入的字数已经超过了限制！", Toast.LENGTH_LONG);
                }
                mToast.show();
                s.delete(editStart - 1, editEnd);
            }else{
                String str = getResources().getString(R.string.gowild_alert_and_clock_count_of_word);
                str = str.replace("%", Integer.toString(temp.length()));
                mTvLimit.setText(str);
            }
        }
    }
}
