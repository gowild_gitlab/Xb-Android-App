/**
 * Copyright (C)  2016 深圳市狗尾草智能科技有限公司
 * GowildXB
 * GowildUpdateInfoActivity.java
 */
package com.gowild.mobileclient.activity.setting;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Editable;
import android.text.InputType;
import android.text.Selection;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.gowild.mobile.libcommon.utils.CheckContentUtil;
import com.gowild.mobile.libcommon.utils.CheckNetworkUtil;
import com.gowild.mobile.libcommon.utils.TimeUtil;
import com.gowild.mobile.libcommon.utils.ToastUtils;
import com.gowild.mobileclient.R;
import com.gowild.mobileclient.activity.base.GowildHasTitileActivity;
import com.gowild.mobileclient.callback.OnToolBarListener;
import com.gowild.mobileclient.config.Constants;
import com.gowild.mobileclient.config.KeysContainer;
import com.gowild.mobileclient.manager.GowildAsyncHttpResponseHandler;
import com.gowild.mobileclient.manager.GowildHttpManager;
import com.gowild.mobileclient.model.AccountModel;
import com.gowild.mobileclient.utils.CharacterParser;
import com.gowild.mobileclient.vo.RequestInfoUpdate;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;
import java.util.Date;

import butterknife.Bind;
import butterknife.OnClick;

/**
 * @author Zhangct
 * @version 1.0
 *          <p> 更新个人信息 </p>
 * @since 2016/8/6 12:43
 */
public class GowildUpdateInfoActivity extends GowildHasTitileActivity {

    // ===========================================================
    // HostConstants
    // ===========================================================

    private static final String TAG = GowildUpdateInfoActivity.class.getSimpleName();

    // ===========================================================
    // Static Fields
    // ===========================================================

    // ===========================================================
    // Fields
    // ===========================================================
    private String mStyle = "";
    private AccountModel mAccount = null;
    @Bind(R.id.default_content_container)
    RelativeLayout mContentContainer;
    @Bind(R.id.sex_container)
    LinearLayout mSexContainer;
    @Bind(R.id.man)
    RelativeLayout mManContainer;
    @Bind(R.id.woman)
    RelativeLayout mWomanContainer;
    @Bind(R.id.content)
    EditText mContent;
    @Bind(R.id.clean)
    Button mClean;
    @Bind(R.id.selectman)
    ImageView mSelectMan;
    @Bind(R.id.selectwoman)
    ImageView mSelectWoman;

    //请求成功
    private final int MSG_CODE_SUCCESS = 0x001;
    //请求失败
    private final int MSG_CODE_FAIL = 0x002;

    private int mMaxCount = 0;

    private RequesInfoUpdateCallback mInfoUpdateCallback = null;

    private OnToolBarListener mToolBarListener = new OnToolBarListener() {

        @Override
        public void onActionClickListener() {
            //保存
            if (CheckNetworkUtil.isNetworkAvailable(getApplicationContext())) {
                commitData();
            } else {
                ToastUtils.showCustom(getBaseContext(), R.string.not_exist_net, false);
            }
        }
    };

    private Handler mHandler = new Handler() {

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case MSG_CODE_SUCCESS:
                    ToastUtils.showCustom(GowildUpdateInfoActivity.this, "保存成功", false);
                    hideLoading();
                    Intent intent = new Intent();
                    intent.putExtra(KeysContainer.KEY_PRE_NAME_ACCOUNT, mAccount.toJSON().toString());
                    setResult(RESULT_OK, intent);
                    finish();
                    break;
                case MSG_CODE_FAIL:
                    hideLoading();
                    break;
            }
        }
    };

    private TextWatcher mContenttextWatcher = new TextWatcher() {
        private CharSequence temp;
        private int editStart ;
        private int editEnd ;
        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            temp = s;
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count,
                                      int after) {
        }

        @Override
        public void afterTextChanged(Editable s) {
            editStart = mContent.getSelectionStart();
            editEnd = mContent.getSelectionEnd();
            if(editStart != 0 && editEnd != 0){
                if (CharacterParser.getStringCharCount(temp.toString()) > mMaxCount) {
                    s.delete(editStart-1, editEnd);
                    int tempSelection = editStart;
                    mContent.setText(s);
                    mContent.setSelection(tempSelection);
                }
            }
        }
    };



    // ===========================================================
    // Constructors
    // ===========================================================


    // ===========================================================
    // Getter or Setter
    // ===========================================================


    // ===========================================================
    // Methods for/from SuperClass/Interfaces
    // ===========================================================
    @Override
    protected int onSetContentView() {
        return R.layout.activity_gowild_setting_onwer_upateinfo;
    }

    @Override
    protected void initEvent() {
        setTitle(R.string.gowild_setting_owner);
        setActionStr(R.string.gowild_setting_owner_save);
        setActionListener(mToolBarListener);
        mInfoUpdateCallback = new RequesInfoUpdateCallback(this);
    }

    @Override
    protected void loadData() {
        super.loadData();
        initData();
        initTitle();
        initView();
    }

    @OnClick({R.id.clean, R.id.man, R.id.woman})
    public void onclick(View view) {
        switch (view.getId()) {
            case R.id.clean:
                mContent.setText("");
                break;

            case R.id.man:
                if (mStyle.equals(Constants.KEY_SEX)) {
                    mSelectMan.setVisibility(View.VISIBLE);
                    mSelectWoman.setVisibility(View.GONE);
                    mAccount.setSex(1);
                }
                break;
            case R.id.woman:
                if (mStyle.equals(Constants.KEY_SEX)) {
                    mSelectMan.setVisibility(View.GONE);
                    mSelectWoman.setVisibility(View.VISIBLE);
                    mAccount.setSex(0);
                }
                break;
            default:
                break;
        }
    }

    // ===========================================================
    // Methods
    // ===========================================================
    private void initData() {
        Bundle bundle = getIntent().getExtras();
        if (null != bundle) {
            mStyle = bundle.getString(Constants.KEY_STYLE);
            String tempAccountStr = bundle.getString(KeysContainer.KEY_PRE_NAME_ACCOUNT);
            try {
                mAccount = new AccountModel(new JSONObject(tempAccountStr));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 初始化标题
     */
    private void initTitle() {
        if (mAccount == null) {
            return;
        }
        if (mStyle.equals(Constants.KEY_NICKNAME)) {
            setTitle(R.string.gowild_setting_owner_nickname);
            mContentContainer.setVisibility(View.VISIBLE);
            mSexContainer.setVisibility(View.GONE);
            mContent.setText(mAccount.getNickName());
            mMaxCount = 10;
        } else if (mStyle.equals(Constants.KEY_EMAIL)) {
            setTitle(R.string.gowild_setting_owner_email);
            mContentContainer.setVisibility(View.VISIBLE);
            mSexContainer.setVisibility(View.GONE);
            mContent.setText(mAccount.getEmail());
            mMaxCount = 20;
        } else if (mStyle.equals(Constants.KEY_SEX)) {
            setTitle(R.string.gowild_setting_owner_sex);
            mContentContainer.setVisibility(View.GONE);
            mSexContainer.setVisibility(View.VISIBLE);
            if (mAccount.getSex() == 1) {
                mSelectMan.setVisibility(View.VISIBLE);
                mSelectWoman.setVisibility(View.GONE);
            } else {
                mSelectMan.setVisibility(View.GONE);
                mSelectWoman.setVisibility(View.VISIBLE);
            }
        } else if (mStyle.equals(Constants.KEY_ADDRESS)) {
            setTitle(R.string.gowild_setting_owner_address);
            mContentContainer.setVisibility(View.VISIBLE);
            mSexContainer.setVisibility(View.GONE);
            mContent.setText(mAccount.getAddress());
            mMaxCount = 20;
        } else if (mStyle.equals(Constants.KEY_HOBBY)) {
            setTitle(R.string.gowild_setting_owner_hobby);
            mContentContainer.setVisibility(View.VISIBLE);
            mSexContainer.setVisibility(View.GONE);
            mContent.setText(mAccount.getHobby());
            mMaxCount = 20;
        } else if (mStyle.equals(Constants.KEY_BIRTHDAY)) {
            setTitle(R.string.gowild_setting_owner_birthday);
            mContent.setInputType(InputType.TYPE_CLASS_NUMBER);
            mContent.setHint(R.string.birthday_edit_style);
            mContentContainer.setVisibility(View.VISIBLE);
            mSexContainer.setVisibility(View.GONE);
            mMaxCount = 8;
            if (!TextUtils.isEmpty(mAccount.getBirthday())) {
                mContent.setText(mAccount.getBirthday().replaceAll("-", ""));
            }
        }
        mContent.addTextChangedListener(mContenttextWatcher);
        //将光标移至最后
        Editable etext = mContent.getText();
        Selection.setSelection(etext, etext.length());
    }

    /**
     * 提交数据
     */
    private void commitData() {
        String content = mContent.getText().toString().trim();
        if (mStyle.equals(Constants.KEY_SEX)) {
            saveAccountModule(mAccount);
        } else {
            if (mStyle.equals(Constants.KEY_NICKNAME)) {
                if (!CheckContentUtil.containsEmoji(mContent.getText().toString().trim())) {
                    if (checkNickNameLength(content)) {
                        mAccount.setNickName(content);
                        saveAccountModule(mAccount);
                    }
                } else {
                    ToastUtils.showCustom(this, R.string.no_memo, false);
                    mContent.setText("");
                }

            } else if (mStyle.equals(Constants.KEY_EMAIL)) {
                if (!mContent.getText().toString().trim().isEmpty()) {
                    if (!CheckContentUtil.containsEmoji(mContent.getText().toString().trim())) {
                        if (CheckContentUtil.containsEmail(mContent.getText().toString().trim())) {
                            if (checkEmailLength(mContent.getText().toString().trim())) {
                                mAccount.setEmail(mContent.getText().toString().trim());
                                saveAccountModule(mAccount);
                            }
                        } else {
                            mContent.setText("");
                            ToastUtils.showCustom(this, R.string.errno_bad_email, false);
                        }
                    } else {
                        ToastUtils.showCustom(this, R.string.no_memo, false);
                        mContent.setText("");
                    }
                } else {
                    mAccount.setEmail(mContent.getText().toString().trim());
                    saveAccountModule(mAccount);
                }
            } else if (mStyle.equals(Constants.KEY_PHONE)) {
                if (checkIsPhone(content)) {
//                    mAccount.setPhone(content);
                    saveAccountModule(mAccount);
                } else {
                    ToastUtils.showCustom(this, R.string.errno_bad_phonenum, false);
                }
            } else if (mStyle.equals(Constants.KEY_ADDRESS)) {
                if (!CheckContentUtil.containsEmoji(content)) {
                    if (checkAddressLength(content)) {
                        mAccount.setAddress(content);
                        saveAccountModule(mAccount);
                    }
                } else {
                    ToastUtils.showCustom(this, R.string.no_memo, false);
                    mContent.setText("");
                }
            } else if (mStyle.equals(Constants.KEY_HOBBY)) {
                if (!CheckContentUtil.containsEmoji(content)) {
                    if (checkHobbyLength(content)) {
                        mAccount.setHobby(content);
                        saveAccountModule(mAccount);
                    }
                } else {
                    ToastUtils.showCustom(this, R.string.no_memo, false);
                    mContent.setText("");
                }
            } else if (mStyle.equals(Constants.KEY_BIRTHDAY)) {
                checkBirthday();
            }
        }
    }

    /**
     * 保存账户信息
     *
     * @param account
     */
    public void saveAccountModule(AccountModel account) {
        showLoading(R.string.gowild_committing);
        //提交数据
        RequestInfoUpdate requestInfoUpdate = new RequestInfoUpdate();
        requestInfoUpdate.put(Constants.KEY_NICKNAME, account.getNickName());
        requestInfoUpdate.put(Constants.KEY_EMAIL, account.getEmail());
        requestInfoUpdate.put(Constants.KEY_SEX, String.valueOf(account.getSex()));
        requestInfoUpdate.put(Constants.KEY_BIRTHDAY, account.getBirthday());
        requestInfoUpdate.put(Constants.KEY_ADDRESS, account.getAddress());
        requestInfoUpdate.put(Constants.KEY_HOBBY, account.getHobby());
        showLoading(R.string.loading);
        GowildHttpManager.requestUpdateUserInfo(this, requestInfoUpdate, mInfoUpdateCallback);
    }

    /**
     * 检查生日
     */
    private void checkBirthday() {
        String time = mContent.getText().toString().trim();
        Date timeDate = TimeUtil.parseDate2(time);
        if (time.length() != 8) {
            ToastUtils.showCustom(this, R.string.errno_edit_style_time, false);
        } else {
            if (checkBirtoday(time)) {
                Calendar calendar = Calendar.getInstance();
                Date nowDate = calendar.getTime();
                int nowYear = calendar.get(Calendar.YEAR);
                int oldYear = nowYear - 70;
                calendar.set(Calendar.YEAR, oldYear);
                Date oldDate = calendar.getTime();
                if (timeDate.compareTo(nowDate) <= 0) {
                    if (timeDate.compareTo(oldDate) >= 0) {
                        time = TimeUtil.getDateFormat(timeDate);
                        mAccount.setBirthday(time);
                        saveAccountModule(mAccount);
                    } else {
                        ToastUtils.showCustom(this, R.string.errno_edit_time_too_old, false);
                    }
                } else {
                    ToastUtils.showCustom(this, R.string.errno_edit_time_too_new, false);
                }
            } else {
                ToastUtils.showCustom(this, R.string.errno_edit_style_time, false);
            }
        }
    }

    /**
     * 检查生日是否正确
     *
     * @param time
     * @return
     */
    private boolean checkBirtoday(String time) {
        Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);

        if (Integer.valueOf(time.substring(0, 4)) > year) {
            return false;
        } else {
            if (Integer.valueOf(time.substring(4, 6)) > 12) {
                return false;
            } else {
                if (Integer.valueOf(time.substring(6, 8)) > 31) {
                    return false;
                } else {
                    return true;
                }
            }
        }
    }

    /**
     * 检查昵称是否正确
     *
     * @param nickName
     * @return
     */
    private boolean checkNickNameLength(String nickName) {
        boolean check = true;
        if (nickName.length() == 0) {
            check = true;
        } else if (CharacterParser.getStringCharCount(nickName) > 10) {
            ToastUtils.showCustom(this, R.string.nickname_too_longer, false);
            check = false;
        } else {
            check = true;
        }
        return check;
    }

    /**
     * 检查地址长度是否正确
     *
     * @param address
     * @return
     */
    private boolean checkAddressLength(String address) {
        boolean check = true;
        if (address.length() == 0) {
            check = true;
        } else if (CharacterParser.getStringCharCount(address) > 20) {
            ToastUtils.showCustom(this, R.string.address_too_longer, false);
            check = false;
        } else {
            check = true;
        }
        return check;
    }

    /**
     * 检查爱好长度是否正确
     *
     * @param hobby
     * @return
     */
    private boolean checkHobbyLength(String hobby) {
        boolean check = true;
        if (hobby.length() == 0) {
            check = true;
        } else if (CharacterParser.getStringCharCount(hobby) > 20) {
            ToastUtils.showCustom(this, R.string.hobby_too_longer, false);
            check = false;
        } else {
            check = true;
        }
        return check;
    }

    /**
     * 检查电子邮件长度是否正确
     *
     * @param email
     * @return
     */
    private boolean checkEmailLength(String email) {
        boolean check = true;
        if (email.length() == 0) {
            check = true;
        } else if (CharacterParser.getStringCharCount(email) > 20) {
            ToastUtils.showCustom(this, R.string.email_too_longer, false);
            check = false;
        } else {
            check = true;
        }
        return check;
    }

    /**
     * 检查是否正确的手机号码
     *
     * @param phoneNum
     * @return
     */
    private boolean checkIsPhone(String phoneNum) {
        boolean isPhone = true;
        if (phoneNum.length() != 11) {
            isPhone = false;
        } else {
            if (phoneNum.substring(0, 1).equals("1")) {
                isPhone = true;
            } else {
                isPhone = false;
            }
        }
        return isPhone;
    }

// ===========================================================
// Inner and Anonymous Classes
// ===========================================================
private class RequesInfoUpdateCallback extends GowildAsyncHttpResponseHandler<Object> {

    /**
     * 构造方法
     *
     * @param context 用于处理授权过期消息，会被转成ApplicationContext
     */
    public RequesInfoUpdateCallback(Context context) {
        super(context);
    }

    @Override
    public void onSuccess(int statusCode, Object responseBody) {
        Message msg = mHandler.obtainMessage();
        msg.what = MSG_CODE_SUCCESS;
        mHandler.sendMessage(msg);
    }

    @Override
    public void onFailure(int statusCode, String responseBody) {
        Message msg = mHandler.obtainMessage();
        msg.what = MSG_CODE_FAIL;
        mHandler.sendMessage(msg);
    }
}
}
