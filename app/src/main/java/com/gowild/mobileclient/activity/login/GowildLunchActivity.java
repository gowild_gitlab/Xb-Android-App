package com.gowild.mobileclient.activity.login;


import android.content.Intent;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.method.DigitsKeyListener;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.gowild.mobile.libcommon.utils.CheckContentUtil;
import com.gowild.mobile.libcommon.utils.CheckNetworkUtil;
import com.gowild.mobile.libcommon.utils.Logger;
import com.gowild.mobile.libcommon.utils.SpUtils;
import com.gowild.mobile.libcommon.utils.ToastUtils;
import com.gowild.mobileclient.R;
import com.gowild.mobileclient.activity.GowildHomeActivity;
import com.gowild.mobileclient.activity.GowildSplashActivity;
import com.gowild.mobileclient.activity.base.GowildLoginAndRegisterActivity;
import com.gowild.mobileclient.activity.setting.update.UpdateManager;
import com.gowild.mobileclient.callback.OnUpdateDoneListener;
import com.gowild.mobileclient.config.Constants;
import com.gowild.mobileclient.config.IntContants;
import com.gowild.mobileclient.event.LoginResult;
import com.gowild.mobileclient.manager.GowildAsyncHttpResponseHandler;
import com.gowild.mobileclient.manager.GowildHttpManager;
import com.gowild.mobileclient.manager.LoginMananger;
import com.gowild.mobileclient.vo.LunchHeader;
import com.gowild.mobileclient.widget.MyToolBar;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.RequestParams;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import butterknife.Bind;
import butterknife.OnClick;

/**
 * @author shuai. wang
 * @version 1.0
 * @since ${DATE} ${TIME}
 * <p><strong>Features draft description.登录的界面,第一分支通过,没有问题
 * 下面开始测试第二分支,忘记密码部分
 * </strong></p>
 */
public class GowildLunchActivity extends GowildLoginAndRegisterActivity {
    @Bind(R.id.gowild_lunch_et_phone)
    EditText mGowildLunchEtPhone;
    @Bind(R.id.gowild_lunch_et_pwd)
    EditText mGowildLunchEtPwd;
    @Bind(R.id.gowild_lunch_tv_back_pwd)
    TextView mGowildLunchTvBackPwd;
    @Bind(R.id.gowild_lunch_bt_lunch)
    Button mGowildLunchBtLunch;
    @Bind(R.id.delete)
    ImageView mDelete;
    @Bind(R.id.gowild_lunch_vi)
    ImageView vi;

    private final String TAG = GowildLunchActivity.class.getSimpleName();
    private String mBack;
    private boolean visible = false;
    private String needLunch;

    //先检查系统版本是否需要升级
    private UpdateManager mUpdateManager = null;


    // ===========================================================
    // Methods for/from SuperClass/Interfaces
    // ===========================================================
    @Override
    public int getLayout() {
        return R.layout.activity_gowild_lunch;
    }

    @Override
    protected void initView() {
        MyToolBar toolbar = (MyToolBar) findViewById(R.id.view_toolbar);
        toolbar.setTitle(getResources().getString(R.string.gowild_chooselunch_lunch_title));
        mGowildLunchEtPhone.setText(SpUtils.getString(this, Constants.USERNAME));
        mDelete.setVisibility(View.GONE);
        vi.setVisibility(View.GONE);
        String digists = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ!@#$%^&*.~///{//}|()'/\"?><,.`//+-=_//[//]:;]+";
        mGowildLunchEtPwd.setKeyListener(DigitsKeyListener.getInstance(digists));
    }

    @Override
    protected void initData() {
        mBack = getIntent().getStringExtra(Constants.BACK);
        needLunch = getIntent().getStringExtra(Constants.NEED_LUNCH);
    }

    @Override
    protected void initEvent() {

        mDelete.setVisibility(TextUtils.isEmpty(mGowildLunchEtPhone.getText().toString()) ? View.GONE : View.VISIBLE);

        mGowildLunchEtPhone.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() == 0) {
                    mDelete.setVisibility(View.GONE);
                } else {
                    mDelete.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        mGowildLunchEtPwd.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() == 0) {
                    vi.setVisibility(View.GONE);
                } else {
                    vi.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        mUpdateManager = new UpdateManager(this);
        mUpdateManager.setOnUpdateDoneListener(new OnUpdateDoneListener() {

            @Override
            public void exitApplication() {
                finish();
            }

            @Override
            public void checkDone() {
            }

            @Override
            public void cancel() {
                EventBus.getDefault().post("finish");
                nextActivtiy(GowildHomeActivity.class);
            }

            @Override
            public void isNewVersion() {
                //已经是最新版本
                EventBus.getDefault().post("finish");
                nextActivtiy(GowildHomeActivity.class);

            }
        });

    }

    // ===========================================================
    // Methods
    // ===========================================================
    @OnClick({R.id.gowild_lunch_bt_lunch, R.id.gowild_lunch_tv_back_pwd, R.id.gowild_lunch_vi, R.id.delete})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.gowild_lunch_bt_lunch:
                //此处处理登录注册的逻辑问题
//				mGowildLunchBtLunch.setEnabled(false);

                startlogin();
                break;
            case R.id.gowild_lunch_tv_back_pwd:
                //忘记密码
                nextActivtiy(GowildCaptchaActivity.class, 0);
                break;
            case R.id.gowild_lunch_vi:
                //变化可见
                if (visible) {
                    mGowildLunchEtPwd.setTransformationMethod(PasswordTransformationMethod.getInstance());
                } else {
                    mGowildLunchEtPwd.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                }
                mGowildLunchEtPwd.setSelection(mGowildLunchEtPwd.getText().length());
                visible = !visible;
                break;
            case R.id.delete:
                mGowildLunchEtPhone.setText("");
                break;
            default:
                break;
        }
    }

    private void startlogin() {
        final String phoneNumber = mGowildLunchEtPhone.getText().toString().trim();
        String password = mGowildLunchEtPwd.getText().toString().trim();

        if (!CheckContentUtil.isCorrectPhone(phoneNumber)) {
            ToastUtils.showCustom(this, getString(R.string.gowid_lunch_phone), false);
            return;
        }
        if (TextUtils.isEmpty(password)) {
            ToastUtils.showCustom(this, getString(R.string.gowid_lunch_pwd), false);
            return;
        }

        if (!CheckNetworkUtil.isNetworkAvailable(this)) {
            ToastUtils.showCustom(this, getString(R.string.gowild_lunch_error), false);
            return;
        }
        showLoading("加载中");
        RequestParams params = new RequestParams();
        params.put(Constants.USERNAME, phoneNumber);
        params.put("password", password);
        params.put("grant_type", "password");
        System.out.println(System.currentTimeMillis() + "start------------------------");
        AsyncHttpClient client = new AsyncHttpClient();
        client.setConnectTimeout(30000);
        client.addHeader(Constants.AUTHORIZATION, Constants.AUTHORIZATION_DESC);
        String tokenURL = GowildHttpManager.getTokenURL(GowildHttpManager.URL_LUNCH);
        System.out.print(tokenURL);
        client.post(GowildLunchActivity.this, GowildHttpManager.getTokenURL(GowildHttpManager.URL_LUNCH), params, new GowildAsyncHttpResponseHandler<LunchHeader.DataEntity>(this) {
            @Override
            public void onSuccess(int statusCode, LunchHeader.DataEntity responseBody) {
                System.out.println(System.currentTimeMillis() + "http------------------------");
                //保存token
                SpUtils.putBoolean(GowildLunchActivity.this, Constants.REGISTER, true);
                SpUtils.putString(GowildLunchActivity.this, Constants.ACCESS_TOKEN, "Bearer " + responseBody.access_token);
                //保存手机号
                SpUtils.putString(GowildLunchActivity.this, Constants.USERNAME, phoneNumber);
                //保存refresh_token
                SpUtils.putString(GowildLunchActivity.this, Constants.REFRESH_TOKEN, responseBody.refresh_token);
                //跳
                SpUtils.putString(GowildLunchActivity.this, Constants.ACCESS_TOKEN_NOBEAR, responseBody.access_token);
                System.out.println(System.currentTimeMillis() + "tcp start------------------------");
                LoginMananger.getInstence().TCPLoginRequest();
            }

            @Override
            public void onFailure(int statusCode, String responseBody) {
                Logger.d(TAG,"onFailure"+statusCode+"-"+responseBody);
                hideLoading();
            }

        });
    }


    public void nextActivtiy(Class clazz, int result) {
        Intent intent = new Intent(GowildLunchActivity.this, clazz);
        intent.putExtra(Constants.CLASS, TAG);
        intent.putExtra(Constants.FORGOT_PASSWORD, result);
        startActivity(intent);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void subLoginResult(LoginResult result) {
        hideLoading();
        switch (result.type) {
            case IntContants.TYPE_ROBOT_SUCESS:
            case IntContants.TYPE_ROBOT_OUTLINE:
            case IntContants.TYPE_ROBOT_NO_ROBOT:
                System.out.println(System.currentTimeMillis() + "tcp end------------------------");
                EventBus.getDefault().post("finish");
                nextActivtiy(GowildHomeActivity.class);
                break;
            case -2:
                //token过期
                ToastUtils.showCustom(GowildLunchActivity.this, "登录过期,请重新登录", false);
                break;
            default:
                break;
        }
    }

    /**
     * 上一个actvivity,返回
     *
     * @param clazz
     */
    public void preActivtiy(Class clazz) {
        Intent intent = new Intent(GowildLunchActivity.this, clazz);
        intent.putExtra(Constants.CLASS, TAG);
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (!TextUtils.isEmpty(needLunch)) {
            preActivtiy(GowildSplashActivity.class);
            finish();
        }
        if (!TextUtils.isEmpty(getIntent().getStringExtra(Constants.BACK))) {
            preActivtiy(GowildSplashActivity.class);
            finish();
        }

    }
}
