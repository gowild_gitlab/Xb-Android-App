/**
 * Copyright (C)  2016 深圳市狗尾草智能科技有限公司
 * GowildXB
 * GowildCaptchaActivity.java
 */
package com.gowild.mobileclient.activity.login;

import android.content.Intent;
import android.graphics.Paint;
import android.os.CountDownTimer;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.gowild.mobile.libcommon.utils.CheckContentUtil;
import com.gowild.mobile.libcommon.utils.SpUtils;
import com.gowild.mobile.libcommon.utils.ToastUtils;
import com.gowild.mobileclient.R;
import com.gowild.mobileclient.activity.GowildSettingPwdActivity;
import com.gowild.mobileclient.activity.GowildSplashActivity;
import com.gowild.mobileclient.activity.GowildUserProActivity;
import com.gowild.mobileclient.activity.base.GowildLoginAndRegisterActivity;
import com.gowild.mobileclient.config.Constants;
import com.gowild.mobileclient.fragment.LunchDialog;
import com.gowild.mobileclient.manager.GowildAsyncHttpResponseHandler;
import com.gowild.mobileclient.manager.GowildHttpManager;
import com.gowild.mobileclient.model.LunchBack;
import com.gowild.mobileclient.vo.Captcha;
import com.loopj.android.http.RequestParams;

import butterknife.Bind;
import butterknife.OnClick;

/**
 * @author shuai. wang
 * @version 1.0
 *          <p><strong>Features draft description.主要功能介绍</strong></p>
 * @since 2016/7/18 16:54
 */
public class GowildCaptchaActivity extends GowildLoginAndRegisterActivity {
    private static final String USERNAME = "username";
    private static final String TYPE = "type";
    private static final String CAPTCHA = "captcha";
    @Bind(R.id.gowild_captcha_et_phone)
    EditText mEtCaptchaPhone;
    @Bind(R.id.gowild_captcha_et_cap)
    EditText mGowildCaptchaEtCap;
    @Bind(R.id.gowild_captcha_et_send_cap)
    TextView mGowildCaptchaEtSendCap;
    @Bind(R.id.gowild_captcha_tv)
    TextView mGowildCaptchaTv;
    @Bind(R.id.gowild_captcha_cb)
    CheckBox mGowildCaptchaCb;
    @Bind(R.id.gowild_captcha_tv_arg)
    TextView mGowildCaptchaTvArg;
    @Bind(R.id.gowild_capcha_delete)
    Button mDelete;
    @Bind(R.id.ll)
    LinearLayout mll;

    private CountDownTimer countDownTimer;
    private String mClazz;

    // ===========================================================
    // Methods for/from SuperClass/Interfaces
    // ===========================================================
    @Override
    public int getLayout() {
        return R.layout.activity_gowild_captcha;
    }


    @Override
    protected void initView() {
        //加上下画线
        mGowildCaptchaEtSendCap.getPaint().setFlags(Paint.UNDERLINE_TEXT_FLAG);
        mGowildCaptchaEtSendCap.getPaint().setAntiAlias(true);//抗锯齿
        mGowildCaptchaTvArg.getPaint().setFlags(Paint.UNDERLINE_TEXT_FLAG);
        mGowildCaptchaTvArg.getPaint().setAntiAlias(true);//抗锯齿
    }

    @Override
    protected void initData() {
        mClazz = getIntent().getStringExtra(Constants.CLASS);
    }

    @Override
    protected void initEvent() {
        if (GowildSplashActivity.class.getSimpleName().equals(mClazz)) {
            //忘记密码
        } else if (GowildLunchActivity.class.getSimpleName().equals(mClazz)) {
            //登录页面过来的,需要隐藏下面
            mll.setVisibility(View.GONE);
        }

        //发送验证码的计时器
        countDownTimer = new CountDownTimer(60000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                mGowildCaptchaEtSendCap.setClickable(false);
                mGowildCaptchaEtSendCap.setText(String.format("%s%s" , Long.toString(millisUntilFinished / 1000), getString(R.string.gowild_captcha_sendtext)));
            }

            @Override
            public void onFinish() {
                mGowildCaptchaEtSendCap.setClickable(true);
                mGowildCaptchaEtSendCap.setText(R.string.gowild_captcha_captcha);
            }
        };

        mEtCaptchaPhone.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() == 0) {
                    mDelete.setVisibility(View.GONE);
                } else {
                    mDelete.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

    }

    // ===========================================================
    // Methods
    // ===========================================================
    @OnClick({R.id.gowild_captcha_et_send_cap, R.id.gowild_captcha_tv, R.id.gowild_capcha_delete, R.id.gowild_captcha_tv_arg})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.gowild_captcha_et_send_cap:
                //检查手机号是否正确，正确就发送验证码
                if (checkPhoneNumber()) {
                    //发送验证码的逻辑开始
                    requestCapcha();
                }
                break;
            case R.id.gowild_capcha_delete:
                mEtCaptchaPhone.setText("");
                break;
            case R.id.gowild_captcha_tv:
//				检查用户协议是否勾选,检查验证码是否正确,根据返回结果跳转到相应的activity
//				检查条件是否符合
//				showLoading("加载中");
                chackNextState();
                break;
            case R.id.gowild_captcha_tv_arg:
                nextActivtiy(GowildUserProActivity.class);
                break;

            default:
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    /**
     * 检查条件是否符合开启下一步
     */
    private void chackNextState() {
        if (!mGowildCaptchaCb.isChecked()) {
            ToastUtils.showCustom(this, R.string.gowild_captcha_costomargs, false);
            return;
        }

        //检查手机号是否合理
        if (!checkPhoneNumber()) {
            return;
        }

        String captcha = mGowildCaptchaEtCap.getText().toString().trim();
        if (TextUtils.isEmpty(captcha)) {
            ToastUtils.showCustom(this, R.string.gowild_captcha_captcha_empty, false);
            return;
        }
        //发起网络检校,成功跳转设置账号密码,失败提示验证码错误
        checkCaptcha(captcha);
    }

    /**
     * 检查验证码是否正确
     *
     * @param captcha
     */
    private void checkCaptcha(final String captcha) {
        showLoading("加载中");
        RequestParams params = new RequestParams();
        params.put(USERNAME, mEtCaptchaPhone.getText().toString().trim());
        //FORGOT_PASSWORD
        if (GowildSplashActivity.class.getSimpleName().equals(mClazz)) {
            params.put(TYPE, Constants.KEY_REGISTER);
            params.put(CAPTCHA, captcha);
            GowildHttpManager.requestRegiest(GowildCaptchaActivity.this,params,new GowildAsyncHttpResponseHandler<Captcha>(this) {
                @Override
                public void onSuccess(int statusCode, Captcha responseBody) {
                    hideLoading();
                    //验证成功 存储这个token用于忘记密码用
                    SpUtils.putString(GowildCaptchaActivity.this, Constants.ACCESS_TOKEN_NOBEAR, responseBody.token);
                    SpUtils.putString(GowildCaptchaActivity.this, Constants.ACCESS_TOKEN, "Bearer " + responseBody.token);
                    SpUtils.putString(GowildCaptchaActivity.this, Constants.USERNAME, mEtCaptchaPhone.getText().toString().trim());
                    //跳转设置帐号密码
                    nextActivtiy(GowildSettingPwdActivity.class);
                }

                @Override
                public void onFailure(int statusCode, String responseBody) {
                    hideLoading();
                }
            });
        } else if (GowildLunchActivity.class.getSimpleName().equals(mClazz)) {
            params.put(TYPE, Constants.FORGOT_PASSWORD);
            params.put(CAPTCHA, captcha);
            GowildHttpManager.requestForgotPwd(GowildCaptchaActivity.this, params, new GowildAsyncHttpResponseHandler<LunchBack>(this) {
                @Override
                public void onSuccess(int statusCode, LunchBack responseBody) {
                    hideLoading();
                    //验证成功 存储这个token用于忘记密码用
                    SpUtils.putString(GowildCaptchaActivity.this, Constants.ACCESS_TOKEN_NOBEAR, responseBody.token);
                    SpUtils.putString(GowildCaptchaActivity.this, Constants.ACCESS_TOKEN, "Bearer " + responseBody.token);
                    SpUtils.putString(GowildCaptchaActivity.this, Constants.USERNAME, mEtCaptchaPhone.getText().toString().trim());
                    //跳转设置帐号密码
                    nextActivtiy(GowildSettingPwdActivity.class);
                }

                @Override
                public void onFailure(int statusCode, String responseBody) {
                    hideLoading();
                }
            });
        }
    }

    @Override
    public void nextActivtiy(Class clazz) {
        Intent intent = new Intent(this, clazz);
        intent.putExtra(Constants.CLASS, mClazz);
        startActivity(intent);
    }

    /**
     * 验证码请求开始
     */
    private void requestCapcha() {
        showLoading("加载中");
        RequestParams params = new RequestParams();
        final String phone = mEtCaptchaPhone.getText().toString().trim();
        params.put(USERNAME, phone);
        if (GowildSplashActivity.class.getSimpleName().equals(mClazz)) {
            params.put(TYPE, Constants.KEY_REGISTER);
        } else {
            params.put(TYPE, Constants.FORGOT_PASSWORD);
        }
        //就当数据请求回来 了,若手机号码已被注册，跳转到提示直接登录页面,否则发送验证码
        GowildHttpManager.requestCaptcha(GowildCaptchaActivity.this, params, new GowildAsyncHttpResponseHandler<String>(GowildCaptchaActivity.this) {

            @Override
            public void onSuccess(int statusCode, String responseBody) {
                ToastUtils.showCustom(GowildCaptchaActivity.this, "验证码发送成功" , false);
                countDownTimer.start();
                hideLoading();
            }

            @Override
            public void onFailure(int statusCode, String responseBody) {
                hideLoading();
                if (statusCode == 2005) {
                    final LunchDialog dialog = new LunchDialog();
                    dialog.setOnClickChangeLinsenter(new LunchDialog.OnClickChangeLinsenter() {
                        @Override
                        public void startLogin() {
                            dialog.dismiss();
                            nextActivtiy(GowildLunchActivity.class);
                        }

                        @Override
                        public void changeAccount() {
                            dialog.dismiss();
                        }
                    });
                    dialog.show(getSupportFragmentManager(), LunchDialog.class.getName());

                }
            }
        });
    }

    /**
     * 检查手机号是否正确,
     */
    private boolean checkPhoneNumber() {
        String phone = mEtCaptchaPhone.getText().toString().trim();
        //为空
        if (TextUtils.isEmpty(phone)) {
            ToastUtils.showCustom(this, R.string.gowild_captcha_phoneempty, false);
            return false;
        }
        //号码不正确
        if (!CheckContentUtil.isCorrectPhone(phone)) {
            ToastUtils.showCustom(this, R.string.gowild_captcha_phoneerror, false);
            return false;
        }
        return true;
    }
}
