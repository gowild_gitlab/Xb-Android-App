/**
 * Copyright (C)  2016 深圳市狗尾草智能科技有限公司
 * GowildXB
 * GowildHasTitileActivity.java
 */
package com.gowild.mobileclient.activity.base;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;

import com.gowild.mobileclient.R;
import com.gowild.mobileclient.base.baseactivity.GowildBaseActivity;
import com.gowild.mobileclient.callback.OnToolBarListener;
import com.gowild.mobileclient.widget.MyToolBar;

import butterknife.ButterKnife;

/**
 * @author Zhangct
 * @version 1.0
 *          <p><strong>有顶部标题栏的Activity基类</strong></p>
 * @since 2016/8/3 14:37
 */
public abstract class GowildHasTitileActivity extends GowildBaseActivity {


    private static final String TAG = GowildHasTitileActivity.class.getSimpleName();

    //请求成功
    protected final int MSG_CODE_SUCCESS = 0x001;
    //请求失败
    protected final int MSG_CODE_FAIL = 0x002;

    MyToolBar mToolbar;

    @Override
    protected int onSetTopbarView() {
        return R.layout.view_gowild_toolbar;
    }

    @Override
    protected int onSetTopBarHeight() {
        return getResources().getDimensionPixelSize(R.dimen.gowild_110px);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mToolbar = (MyToolBar) findViewById(R.id.view_toolbar);
        ButterKnife.bind(this);
        initEvent();
        initView();
        loadData();
    }

    /**
     * 设置标题
     *
     * @param title
     */
    public void setTitle(String title) {
        if (null != mToolbar) {
            mToolbar.setTitle(title);
        }
    }

    /**
     * 设置标题
     *
     * @param titleId
     */
    public void setTitle(int titleId) {
        if (null != mToolbar) {
            mToolbar.setTitle(getResources().getString(titleId));
        }
    }

    /**
     * 设置右侧图片资源
     *
     * @param drawable
     */
    public void setActionDrawable(Drawable drawable) {
        if (null != mToolbar) {
            mToolbar.setActionDrawable(drawable);
        }
    }

    /**
     * 设置右侧图片资源
     *
     * @param drawableId
     */
    public void setActionDrawable(int drawableId) {
        if (null != mToolbar) {
            mToolbar.setActionDrawable(drawableId);
        }
    }

    /**
     * 设置右侧文字
     *
     * @param str
     */
    public void setActionStr(String str) {
        if (null != mToolbar) {
            mToolbar.setActionStr(str);
        }
    }

    /**
     * 设置右侧文字
     *
     * @param strId
     */
    public void setActionStr(int strId) {
        if (null != mToolbar) {
            mToolbar.setActionStr(strId);
        }
    }

    /**
     * 设置右侧监听
     *
     * @param listener
     */
    public void setActionListener(OnToolBarListener listener) {
        if (null != mToolbar) {
            mToolbar.setActionListener(listener);
        }
    }

    protected void startActivity(Class target) {
        Intent intent = new Intent(GowildHasTitileActivity.this, target);
        startActivity(intent);
    }

    protected void startActivity(Class target, Bundle bundle) {
        Intent intent = new Intent(GowildHasTitileActivity.this, target);
        intent.putExtras(bundle);
        startActivity(intent);
    }

    protected void startActivityForResult(Class target, int requestCode, Bundle bundle) {
        Intent intent = new Intent(GowildHasTitileActivity.this, target);
        intent.putExtras(bundle);
        startActivityForResult(intent, requestCode);
    }

    protected abstract int onSetContentView();

    /**
     * 初始化事件
     */
    protected abstract void initEvent();

    /**
     * 加载数据
     */
    protected void loadData() {
    }

    /**
     * 加载试图
     */
    protected void initView() {
    }
}
