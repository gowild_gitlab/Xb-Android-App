/**
 * Copyright (C)  2016 深圳市狗尾草智能科技有限公司
 * GowildXBTest
 * GowildAVMonitorActivity.java
 */
package com.gowild.mobileclient.activity.video;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.PowerManager;
import android.provider.Settings;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.gowild.mobile.libcommon.utils.CheckNetworkUtil;
import com.gowild.mobile.libcommon.utils.FileUtil;
import com.gowild.mobile.libcommon.utils.Logger;
import com.gowild.mobile.libcommon.utils.PathUtil;
import com.gowild.mobile.libcommon.utils.ToastUtils;
import com.gowild.mobile.libp2sp.core.exception.NotConnectedException;
import com.gowild.mobile.libp2sp.core.listener.IDataListener;
import com.gowild.mobile.libp2sp.netty.NettyClientManager;
import com.gowild.mobile.libviewer.MyViewerHelper;
import com.gowild.mobileclient.R;
import com.gowild.mobileclient.activity.PhotoAndMediaListActivity;
import com.gowild.mobileclient.base.baseactivity.GowildBaseActivity;
import com.gowild.mobileclient.callback.OnNormalDialogListener;
import com.gowild.mobileclient.config.Constants;
import com.gowild.mobileclient.config.IntContants;
import com.gowild.mobileclient.manager.GowildTCPManager;
import com.gowild.mobileclient.manager.LoginMananger;
import com.gowild.mobileclient.misc.GowildResponseCode;
import com.gowild.mobileclient.protocol.BaseBothMsgProto;
import com.gowild.mobileclient.protocol.MonitorBothMsgProto;
import com.gowild.mobileclient.utils.CommonDataUtil;
import com.gowild.mobileclient.utils.FileNameHelper;
import com.gowild.mobileclient.utils.OrientationUtil;
import com.gowild.mobileclient.widget.dialog.GowildNormalDialog;
import com.ichano.rvs.viewer.Viewer;
import com.ichano.rvs.viewer.ui.AbstractMediaView;
import com.ichano.rvs.viewer.ui.GLMediaView;
import com.umeng.socialize.utils.Log;

import java.io.File;
import java.io.IOException;
import java.util.Locale;

import butterknife.Bind;

/**
 * @author zhangct
 * @since 2017/5/16 16:43
 * @version 1.0
 * <p><strong>Features draft description.主要功能介绍</strong></p>
 */
public class GowildAVMonitorActivity extends GowildBaseActivity {

    private static final String TAG = GowildAVMonitorActivity.class.getSimpleName();

    private static final int[] INDICATOR_VIEWS  = new int[]{R.id.gowild_av_monitor_indicator0_view, R.id.gowild_av_monitor_indicator1_view, R.id.gowild_av_monitor_indicator2_view,
            R.id.gowild_av_monitor_indicator3_view, R.id.gowild_av_monitor_indicator4_view};
    private static final int DEFAULT_CAMERA_INDEX  = 0;
    private static final long MIN_RECORD_TIME_MILLS = 5 * 1000;
    private static final String TAG_GLVIEW  = "GLView";

    private static final long TIME_TICK_INTERVAL = 1000;
    private static final int CENTER_INDICATOR_IDX = 2;
    private static final int MIN_INDICATOR_IDX  = 0;
    private static final int MAX_INDICATOR_IDX  = 4;
    private static final int MSG_TIMER_TICK  = 0x101;
    private static final int MSG_HIDE_FULLSCREEN_TOOLBAR = 0x102;
    private static final int MSG_SHOW_FULLSCREEN_TOOLBAR = 0x103;
    private static final int MSG_CHECK_LINK_SUCCESS = 0x104;

    PowerManager powerManager = null;
    PowerManager.WakeLock wakeLock = null;
    private OnFucBtnTouchListener mOnFucTouchListener    = new OnFucBtnTouchListener();
    private OnFucBtnClickListener mOnFucBtnClickListener = new OnFucBtnClickListener();
    private UIHandler mHandler = new UIHandler();
    private int                   mCurrentIndex          = CENTER_INDICATOR_IDX;
    private boolean               mIsRecording           = false;
    private TextView mTimerView             = null;
    private TextView              mFullscreenTimerView   = null;
    private long                  mStartRecordMills      = 0L;
    private boolean               mIsPortrait            = true;

    private int mSurfaceViewWidth;
    private int mSurfaceViewHeight;
    private int mVideoWidth  = 800;
    private int mVideoHeight = 480;
    private ViewGroup mGLViewLayout;

    private boolean   mIsShowFullscreenToolBar;
    private View mFullscreenTitleView;
    private View      mFullscreenToolbarView;
    private ViewGroup mLandscapeLayout;
    private ViewGroup mPortraitLayout;

    private String mCaptureImgPath;
    private String mRecordVideoPath;

    private boolean mIsGetAuthSuccess = false;
    private boolean mIsLinkSuccess    = false;
    private View    mNotBindTv        = null;
    private View    mGoBindBtn        = null;
    private TextView mIsRecordingTv;
    private boolean                  mIsInitGLMediaView      = false;
    private boolean                  mIsBindedRobot          = false;
    private ViewGroup.LayoutParams mPortraitLayoutParams;
    private ViewGroup.LayoutParams mLandscapeLayoutParams;
    private boolean mIsEnterPhotoList = false;
    private boolean mMachineOnLine;

    private GowildNormalDialog mDialog;
    private OnNormalDialogListener mDialogListener = new DialogListener();
    private boolean                mIsTicking      = false;
    private String mCurrentSaveVideoPath;
    private View   mLandscapeUnbindedLayout;
    private boolean mScheduleCheckLinkSuccess = false;
    private boolean mIsNetworkAvailable       = false;
    private TextView    mPBindTv;
    private TextView    mLBindTv;
    private Button mLandscapeBindBtn;
    private Button      mBindBtn;
    private FrameLayout mNoBindLayout;
    private TextView    mShowTalk;
    private boolean isShowDialog = true;
    private boolean needSendQuit;
    private boolean isShow = false;

    private long mCID = 0;
    private String mUserName = null;
    private String mPwd = null;
    @Bind(R.id.gowild_av_monitor_glview)
    GLMediaView mGLMediaView;

    //接口请求回调
    private OnStartAVMonitorListener mStartAVMonitorListener = new OnStartAVMonitorListener();
    private OnQuitAVMonitorListener  mQuitAVMonitorListener  = new OnQuitAVMonitorListener();
    private OnGetAuthListener mGetAuthListener  = new OnGetAuthListener();


    private OnLinkCameraStatusListener mLinkListener = new OnLinkCameraStatusListener();
    //监控状态回调
    private MyViewerHelper.ViewerListener mViewerListener = new MyViewerHelper.ViewerListener(){
        //底层返回sdk登录状态，如果登录成功跳转到设备列表页面
        @Override
        public void onLoginResult(boolean success) {
            if(success){
                //登录成功，发起开启监控请求
                startAVMonitor();
            }
        }

        @Override
        public void onRemoteStreamState(long streamerCid, boolean session) {
            Log.e(TAG,"onRemoteStreamState  streamerCid = "+streamerCid+",session = "+session);
            if(session){
                mGLMediaView.bindCid(Long.valueOf(streamerCid), DEFAULT_CAMERA_INDEX);
                mGLMediaView.soundOff();
                mGLMediaView.setOnLinkCameraStatusListener(mLinkListener);
                mGLMediaView.openLiveVideo();
                mIsInitGLMediaView = true;
            }
        }
    };

    @Override
    protected int onSetTopbarView() {
        return R.layout.layout_gowild_av_monitor_topbar;
    }

    @Override
    protected int onSetContentView() {
        return R.layout.activity_gowild_av_monitor_content;
    }

    @Override
    protected int onSetTopBarHeight() {
        return getResources().getDimensionPixelSize(R.dimen.gowild_110px);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //1.0 旋转机器头部到中间
        rotateHeadToCenter(mCurrentIndex);
        //2.0 设置监控状态监听
        MyViewerHelper.getInstance().setViewerListener(mViewerListener);
        //2.1 尝试登录监控服务器
        MyViewerHelper.getInstance().login();
        //3.0 初始化界面相关信息
        initViews();
        //4.0
        initUI();
        onRequestPortraitScreen();
    }


    /**
     * 初始化界面相关信息
     */
    private void initViews() {
        mMachineOnLine = true;
        //1.0 获取机器当前账号是否已经绑定机器
        int type = LoginMananger.getInstence().getType();
        if (type == IntContants.TYPE_ROBOT_SUCESS || type == IntContants.TYPE_ROBOT_OUTLINE) {
            mIsBindedRobot = true;
        } else {
            mIsBindedRobot = false;
        }

        // 2.0 计算出监控浏览界面宽高
        DisplayMetrics metrics = getResources().getDisplayMetrics();
        this.mSurfaceViewWidth = metrics.widthPixels;
        this.mSurfaceViewHeight = (int) Math.ceil(mSurfaceViewWidth * mVideoHeight / mVideoWidth);

        //3.0 设置相关目录保存路径
        String path = PathUtil.getSdcardPath(getApplicationContext()) + File.separator + "gowild";
        this.mCaptureImgPath = path + File.separator + Constants.AV_MONITOR_CAPTURE_IMAGE_FILE_NAME;
        this.mRecordVideoPath = path + File.separator + Constants.AV_MONITOR_CAPTURE_VIDEO_FILE_NAME;
        try {
            FileUtil.createFile(mCaptureImgPath, true, true);
            FileUtil.createFile(mRecordVideoPath, true, true);
        } catch (IOException e) {
            e.printStackTrace();
        }

        //4.0 设置屏幕常亮
        powerManager = (PowerManager) this.getSystemService(this.POWER_SERVICE);
        wakeLock = this.powerManager.newWakeLock(PowerManager.FULL_WAKE_LOCK, "My Lock");
    }

    private void initUI() {
        View view = findViewById(R.id.gowild_av_monitor_portrait_back_btn);
        view.setOnClickListener(mOnFucBtnClickListener);
        view = findViewById(R.id.gowild_av_monitor_photo_list_btn);
        view.setOnClickListener(mOnFucBtnClickListener);
        view = findViewById(R.id.gowild_av_monitor_portrait_fullscreen_btn);
        view.setOnClickListener(mOnFucBtnClickListener);
        view = findViewById(R.id.gowild_av_monitor_portrait_left_btn);
        view.setOnClickListener(mOnFucBtnClickListener);
        view = findViewById(R.id.gowild_av_monitor_portrait_right_btn);
        view.setOnClickListener(mOnFucBtnClickListener);
        view = findViewById(R.id.gowild_av_monitor_portrait_record_btn);
        view.setOnTouchListener(mOnFucTouchListener);
        view = findViewById(R.id.gowild_av_monitor_portrait_send_audio_btn);
        view.setOnTouchListener(mOnFucTouchListener);
        view = findViewById(R.id.gowild_av_monitor_portrait_capture_btn);
        view.setOnClickListener(mOnFucBtnClickListener);

        view = findViewById(R.id.gowild_av_monitor_landscape_left_btn);
        view.setOnClickListener(mOnFucBtnClickListener);
        view = findViewById(R.id.gowild_av_monitor_landscape_right_btn);
        view.setOnClickListener(mOnFucBtnClickListener);
        view = findViewById(R.id.gowild_av_monitor_landscape_capture_btn);
        view.setOnClickListener(mOnFucBtnClickListener);
        view = findViewById(R.id.gowild_av_monitor_landscape_send_audio_btn);
        view.setOnTouchListener(mOnFucTouchListener);
        view = findViewById(R.id.gowild_av_monitor_landscape_go_bind_btn);
        view.setOnClickListener(mOnFucBtnClickListener);
        view = findViewById(R.id.gowild_av_monitor_landscape_portrait_btn);
        view.setOnClickListener(mOnFucBtnClickListener);
        view = findViewById(R.id.gowild_av_monitor_landscape_record_btn);
        view.setOnTouchListener(mOnFucTouchListener);

        findViewById(INDICATOR_VIEWS[CENTER_INDICATOR_IDX]).setSelected(true);

        this.mTimerView = (TextView) findViewById(R.id.gowild_av_monitor_portrait_timer_tv);
        this.mGLViewLayout = (ViewGroup) findViewById(R.id.gowild_av_monitor_glview_layout);
        this.mGLMediaView = (GLMediaView) findViewById(R.id.gowild_av_monitor_glview);
        //// TODO: 2016/10/24
        mGLMediaView.setOnLinkCameraStatusListener(new OnLinkCameraStatusListener());
        this.mGLMediaView.setTag(TAG_GLVIEW);
        this.mGLMediaView.autoOpenLiveVideo(false);
        this.mGLViewLayout.setOnClickListener(mOnFucBtnClickListener);
        this.mFullscreenTimerView = (TextView) findViewById(R.id.gowild_av_monitor_landscape_timer_tv);

        this.mLandscapeLayout = (ViewGroup) findViewById(R.id.gowild_av_monitor_landscape_layout);
        this.mPortraitLayout = (ViewGroup) findViewById(R.id.gowild_av_monitor_portrait_layout);
        this.mFullscreenToolbarView = findViewById(R.id.gowild_av_monitor_landscape_tool_bar);
        this.mNotBindTv = findViewById(R.id.gowild_av_monitor_portrait_no_bind_tips_tv);
        this.mGoBindBtn = findViewById(R.id.gowild_av_monitor_go_bind_btn);
        this.mGoBindBtn.setOnClickListener(mOnFucBtnClickListener);
        this.mIsRecordingTv = (TextView) findViewById(R.id.gowild_av_monitor_portrait_recording_tv);

        this.mPortraitLayoutParams = this.mGLViewLayout.getLayoutParams();
        this.mLandscapeLayoutParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);

        this.mIsRecordingTv.setVisibility(View.INVISIBLE);
        this.mTimerView.setVisibility(View.INVISIBLE);
        this.mFullscreenTimerView.setVisibility(View.INVISIBLE);
        this.mLandscapeUnbindedLayout = findViewById(R.id.gowild_av_monitor_landscape_no_bind_layout);

        mPBindTv = (TextView) findViewById(R.id.gowild_av_monitor_portrait_no_bind_tips_tv);
        mLBindTv = (TextView) findViewById(R.id.gowild_av_monitor_landscape_no_bind_tips_tv);
        mLandscapeBindBtn = (Button) findViewById(R.id.gowild_av_monitor_landscape_go_bind_btn);
        mBindBtn = (Button) findViewById(R.id.gowild_av_monitor_go_bind_btn);
        mNoBindLayout = (FrameLayout) findViewById(R.id.gowild_av_monitor_portrait_no_bind_layout);
        mShowTalk = (TextView) findViewById(R.id.gowild_av_monitor_showtalk);
        mDialog = new GowildNormalDialog();
    }

    @Override
    protected void onStart() {
        super.onStart();
        isShowDialog = true;
        Logger.d(TAG, "onStart()");
        if (!mIsEnterPhotoList) {
            updateUI();
            if (mIsLinkSuccess) {
                Logger.d(TAG, "onStart() openVideo");
                mGLMediaView.openLiveVideo();
                mGLMediaView.setOnLinkCameraStatusListener(new OnLinkCameraStatusListener());
            }
        }
        mIsEnterPhotoList = false;
    }

    @Override
    protected void onStop() {
        super.onStop();
        isShowDialog = false;
        Logger.d(TAG, "onStop()");
        if (!mIsEnterPhotoList) {
            if (mIsLinkSuccess) {
                quitAVMonitor();
            }else{
                if (needSendQuit){
                    quitAVMonitor();
                }
            }

            //            mGLMediaView.setOnLinkCameraStatusListener(null);
            if (mGLMediaView != null) {
                mGLMediaView.closeLiveVideo();
                onStopSendAudio();
                onStopRecord();
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        Logger.d(TAG, "onResume()");
        //播放开始 保持屏幕常亮
        if (!mIsPortrait) {
            onRequestPortraitScreen();
        }

        wakeLock.acquire();
        NettyClientManager manager = NettyClientManager.getInstance();
        manager.addHandlerListener(GowildResponseCode.START_AV_MONITOR_RESULT, mStartAVMonitorListener);
        manager.addHandlerListener(GowildResponseCode.QUIT_AV_MONITOR_RESULT, mQuitAVMonitorListener);
        manager.addHandlerListener(GowildResponseCode.GET_AUTH_RESULT, mGetAuthListener);

    }

    @Override
    protected void onPause() {

        wakeLock.release();
        //看护相册进入保存
        if (mIsEnterPhotoList) {
            View view = findViewById(R.id.gowild_av_monitor_portrait_record_btn);
            view.setSelected(false);
        }
        if (mGLMediaView != null) {
            //不需要走此方法 退出监听收到后会走
            onStopRecord();
            Logger.d(TAG, "onPause onStopRecord ");
        }
        NettyClientManager manager = NettyClientManager.getInstance();
        manager.removeHandlerListener(GowildResponseCode.START_AV_MONITOR_RESULT, mStartAVMonitorListener);
        manager.removeHandlerListener(GowildResponseCode.QUIT_AV_MONITOR_RESULT, mQuitAVMonitorListener);
        manager.removeHandlerListener(GowildResponseCode.GET_AUTH_RESULT, mGetAuthListener);
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        MyViewerHelper.getInstance().logout();
        if (mDialog != null &&mDialog.isAdded()) {
            isShow = false;
            mDialog.dismiss();
        }
    }

    /**
     * 1.0 进入此界面之后，先将机器头部旋转到中间
     * @param index
     */
    private void rotateHeadToCenter(int index) {
        int angle = OrientationUtil.calculateAngel(index);
        try {
            GowildTCPManager.requestRotateHead(angle);
            Logger.d(TAG, "onRotateHead() angle = " + angle);
        } catch (NotConnectedException e) {
            e.printStackTrace();
        }
    }

    /**
     * 2.0 之后向机器发起开启实时看护请求，机器尝试与监控服务器建立连接
     */
    private void startAVMonitor() {
        try {
            GowildTCPManager.requestStartAVMonitor();
            Logger.d(TAG, "startAVMonitor()");
        } catch (NotConnectedException e) {
            e.printStackTrace();
        }
    }

    /**
     * 3.0 机器返回与监控服务器建立连接确认，观看端再次发送请求，获取授权账号与密码
     */
    private void requestConnectionInfo() {
        try {
            GowildTCPManager.requestConnectionInfo();
            Logger.d(TAG, "onStartAVMonitorSuccess()");
        } catch (NotConnectedException e) {
            e.printStackTrace();
        }
    }

    /**
     * 4.0 通知机器退出监控状态
     */
    private void quitAVMonitor() {
        try {
            GowildTCPManager.requestQuitAVMonitor();
            Logger.d(TAG, "startQuitAVMonitor()");
        } catch (NotConnectedException e) {
            e.printStackTrace();
        }
    }

    /**
     * 机器与监控服务器建联成功确认
     */
    private class OnStartAVMonitorListener implements IDataListener<BaseBothMsgProto.IntMsg> {

        @Override
        public void onReceiveData(final BaseBothMsgProto.IntMsg data) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    int result = data.getValue();
                    Logger.d(TAG, "OnStartAVMonitorListener result = " + result);
                    if (result == GowildResponseCode.CODE_START_AV_MONITOR_SUCCESS) {
                        //向机器请求授权信息
                        requestConnectionInfo();
                    }else if (result == GowildResponseCode.CODE_START_AV_MONITOR_NOT_ONLINE) {
                        onStartAVMonitorRobotNotOnline(); //机器人不在线
                    } else if (result == GowildResponseCode.CODE_START_AV_MONITOR_BUSY) {
                        onStartAVMonitorRobotBusy();//机器人繁忙
                    }
                }
            });
        }

        @Override
        public void onActiveChanged(boolean isActive) {
            Logger.d(TAG, "onActiveChanged() isActive = " + isActive);
        }
    }


    /**
     * 得到授权信息，观看端开始尝试与监控服务器建立连接
     */
    private class OnGetAuthListener implements IDataListener<MonitorBothMsgProto.MonitorPowerMsg> {

        @Override
        public void onReceiveData(final MonitorBothMsgProto.MonitorPowerMsg data) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    int code = data.getCode();
                    if (code == GowildResponseCode.CODE_GET_AUTH_SUCCESS) {//授权成功
                        mCID = Long.parseLong(data.getCID());
                        mUserName = data.getUsername();
                        mPwd = data.getPassword();
                        Viewer.getViewer().connectStreamer(Long.valueOf(mCID), mUserName, mPwd);
                        Log.e(TAG,"OnGetAuthListener  mCID = "+mCID+",mUserName = "+mUserName+",mPwd = "+mPwd);
                        //打开视频流
                        mGLMediaView.bindCid(mCID, DEFAULT_CAMERA_INDEX);
                        mGLMediaView.openLiveVideo();
                    }else if (code == GowildResponseCode.CODE_GET_AUTH_FAIL) { //机器人不在线
                        mCID = 0L;
                        mUserName = null;
                        mPwd = null;
                        onStartAVMonitorRobotNotOnline();
                    } else if (code == -2) { //繁忙
                        onGetAuthorizationFail(); //授权失败
                    }
                }
            });
        }

        @Override
        public void onActiveChanged(boolean isActive) {
            Logger.d(TAG, "onActiveChanged() isActive = " + isActive);
        }
    }

    /**
     * 机器通知观看端退出监控状态
     */
    private class OnQuitAVMonitorListener implements IDataListener<BaseBothMsgProto.IntMsg> {

        @Override
        public void onReceiveData(BaseBothMsgProto.IntMsg data) {
            int result = data.getValue();
            Logger.d(TAG, "OnQuitAVMonitorListener result = " + result);
            if (result == GowildResponseCode.CODE_QUIT_AV_MONITOR_BY_ROBOT) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        showRobotBusyDialog(true);
                        onStopRecord();
                    }
                });
            }
        }

        @Override
        public void onActiveChanged(boolean isActive) {
            Logger.d(TAG, "onActiveChanged() isActive = " + isActive);
        }
    }

    //==============================================================================================================================

    private class OnFucBtnClickListener implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            int viewID = v.getId();
            Logger.d(TAG, "onClick() viewID = " + viewID);
            switch (viewID) {
                case R.id.gowild_av_monitor_portrait_back_btn:
                case R.id.gowild_av_monitor_landscape_back_btn:
                    onBackBtnClick();
                    break;
                case R.id.gowild_av_monitor_photo_list_btn:
                case R.id.gowild_av_monitor_landscape_photo_list_btn:
                    onPhotoListBtnClick();
                    break;
                case R.id.gowild_av_monitor_portrait_left_btn:
                case R.id.gowild_av_monitor_landscape_left_btn:
                    if (mIsLinkSuccess) {
                        onGoLeftBtnClick();
                    }
                    break;
                case R.id.gowild_av_monitor_portrait_right_btn:
                case R.id.gowild_av_monitor_landscape_right_btn:
                    if (mIsLinkSuccess) {
                        onGoRightBtnClick();
                    }
                    break;
                case R.id.gowild_av_monitor_portrait_fullscreen_btn:
                case R.id.gowild_av_monitor_landscape_portrait_btn:
                    onSwitchScreenBtnClick();
                    break;
                case R.id.gowild_av_monitor_portrait_capture_btn:
                case R.id.gowild_av_monitor_landscape_capture_btn:
                    if (mIsLinkSuccess) {
                        onCaptureBtnClick();
                    }
                    break;
                case R.id.gowild_av_monitor_glview_layout:
                    Logger.d(TAG, "gowild_av_monitor_glview_layout onClick()");
                    break;
                case R.id.gowild_av_monitor_go_bind_btn:
                case R.id.gowild_av_monitor_landscape_go_bind_btn:
                    Intent intent = new Intent();
                    if (mIsNetworkAvailable) {
                        intent.setClass(GowildAVMonitorActivity.this, com.gowild.mobileclient.activity.setting.GowildWifiConfigActivity.class);
                    } else {
                        intent.setAction(Settings.ACTION_WIFI_SETTINGS);
                    }
                    startActivity(intent);
                    break;
                default:
            }
        }
    }

    private class OnFucBtnTouchListener implements View.OnTouchListener {

        @Override
        public boolean onTouch(View v, MotionEvent event) {
            int id     = v.getId();
            int action = event.getAction();
            if (!mIsLinkSuccess) {
                return false;
            }
            switch (id) {
                case R.id.gowild_av_monitor_portrait_send_audio_btn:
                case R.id.gowild_av_monitor_landscape_send_audio_btn:
                    if (action == MotionEvent.ACTION_DOWN) {
                        mShowTalk.setVisibility(View.VISIBLE);
                        v.setSelected(true);
                        onStartSendAudio();
                    } else if (action == MotionEvent.ACTION_UP) {
                        mShowTalk.setVisibility(View.INVISIBLE);
                        v.setSelected(false);
                        onStopSendAudio();
                    }
                    return false;
            }

            if (MotionEvent.ACTION_UP != event.getAction()) {
                return false;
            }
            switch (id) {
                case R.id.gowild_av_monitor_portrait_record_btn:
                case R.id.gowild_av_monitor_landscape_record_btn:
                    if (mIsRecording) {
                        v.setSelected(false);
                        onStopRecord();
                    } else {
                        v.setSelected(true);
                        onStartRecord();
                    }
                    break;
            }
            return false;
        }
    }

    private class UIHandler extends Handler {

        @Override
        public void handleMessage(Message msg) {
            int what = msg.what;
            if (what == MSG_TIMER_TICK) {
                onTick();
            } else if (what == MSG_HIDE_FULLSCREEN_TOOLBAR) {
                hideFullscreenToolbar();
            } else if (what == MSG_SHOW_FULLSCREEN_TOOLBAR) {
                showFullscreenBar();
            } else if (what == MSG_CHECK_LINK_SUCCESS) {
                Logger.d(TAG, "MSG_CHECK_LINK_SUCCESS");
                if (!mIsLinkSuccess) {
                    Logger.d(TAG, "mIsLinkSuccess" + mIsLinkSuccess);
                    hideLoading();
                    Logger.d(TAG, "showEnterErrorDialog");
                    showEnterErrorDialog();
                } else {
                    quitAVMonitor();
                }
            }
        }
    }

    @Override
    public void onBackPressed() {
        if (mIsPortrait) {
            super.onBackPressed();
            hideLoading();
            if (mDialog != null&& mDialog.isAdded()) {
                isShow = false;
                mDialog.dismiss();
            }
            quitAVMonitor();
            finish();
        } else {
            onRequestPortraitScreen();
        }
    }

    private void onBackBtnClick() {
        if (mIsPortrait) {
            onStopRecord();
            quitAVMonitor();
            finish();
        } else {
            onRequestPortraitScreen();
        }
    }

    private void onPhotoListBtnClick() {
        Intent intent = new Intent();
        intent.setClass(this, PhotoAndMediaListActivity.class);
        startActivity(intent);
        mIsEnterPhotoList = true;
        //如果开始录制需要保存视频
        onStopRecord();
    }

    //变更屏幕
    private void onSwitchScreenBtnClick() {
        Logger.d(TAG, "onSwitchScreenBtnClick() mIsPortrait = " + mIsPortrait);
        if (mIsPortrait) {
            onRequestLandscapeScreen();
        } else {
            onRequestPortraitScreen();
        }
    }

    private void onGoLeftBtnClick() {
        int idx = mCurrentIndex - 1;
        if (idx >= MIN_INDICATOR_IDX) {
            findViewById(INDICATOR_VIEWS[idx]).setSelected(true);
            findViewById(INDICATOR_VIEWS[mCurrentIndex]).setSelected(false);
            mCurrentIndex = idx;
            rotateHeadToCenter(idx);
        } else {
            ToastUtils.showCustom(this, R.string.gowild_go_left_end, false);
        }
    }

    private void onGoRightBtnClick() {
        int idx = mCurrentIndex + 1;
        if (idx <= MAX_INDICATOR_IDX) {
            findViewById(INDICATOR_VIEWS[idx]).setSelected(true);
            findViewById(INDICATOR_VIEWS[mCurrentIndex]).setSelected(false);
            mCurrentIndex = idx;
            rotateHeadToCenter(idx);
        } else {
            ToastUtils.showCustom(this, R.string.gowild_go_right_end, false);
        }
    }

    private void onCaptureBtnClick() {
        if (mIsLinkSuccess) {
            String path = mCaptureImgPath + File.separator + FileNameHelper.generateJPGPictureFileName();

            if (mGLMediaView.takeCapture(path)) {
                ToastUtils.showCustom(this, getString(R.string.gowild_save_pic_success), false);
            } else {
                ToastUtils.showCustom(this, R.string.gowild_save_pic_fail, false);
            }
        }
    }

    private void onStartSendAudio() {
        if (mIsLinkSuccess) {
            if (!mGLMediaView.isSendRevAudio()) {
                mIsRecordingTv.setVisibility(View.VISIBLE);
                mGLMediaView.startSendRevAudio();
            }
        }
    }

    private void onStopSendAudio() {
        if (mIsLinkSuccess) {
            if (mGLMediaView.isSendRevAudio()) {
                mIsRecordingTv.setVisibility(View.INVISIBLE);
                mGLMediaView.stopSendRevAudio();
            }
        }
    }

    private void onStartRecord() {
        Logger.d(TAG, "onStartRecord()");
        startTick();
        mIsRecording = true;
        if (mIsLinkSuccess) {
            if (mIsPortrait) {
                mTimerView.setVisibility(View.VISIBLE);
                mFullscreenTimerView.setVisibility(View.INVISIBLE);
            } else {
                mTimerView.setVisibility(View.INVISIBLE);
                mFullscreenTimerView.setVisibility(View.VISIBLE);
            }
            String path = mRecordVideoPath + File.separator + FileNameHelper.generateVideoFileName();
            mCurrentSaveVideoPath = path;
            boolean success = mGLMediaView.startRecordVideo(path);
            Logger.d(TAG, "onStartRecord() path = " + path);
            Logger.d(TAG, "onStartRecord() success = " + success);
        }
    }

    private void onStopRecord() {
        Logger.d(TAG, "onStopRecord()");
        stopTick();
        mIsRecording = false;
        findViewById(R.id.gowild_av_monitor_portrait_record_btn).setSelected(false);
        findViewById(R.id.gowild_av_monitor_landscape_record_btn).setSelected(false);
        if (mIsLinkSuccess) {
            mTimerView.setVisibility(View.INVISIBLE);
            mFullscreenTimerView.setVisibility(View.INVISIBLE);
            boolean success = mGLMediaView.stopRecordVideo();
            if (System.currentTimeMillis() - mStartRecordMills < MIN_RECORD_TIME_MILLS) {
                mStartRecordMills = 0L;
                ToastUtils.showCustom(this, R.string.gowild_av_record_to_short, false);
                FileUtil.deleteFile(mCurrentSaveVideoPath);
            } else {
                if (success) {
                    mStartRecordMills = 0L;
                    ToastUtils.showCustom(this, getString(R.string.gowild_av_record_save), false);
                }
            }
        }
    }

    private void onTick() {
        long   time     = (System.currentTimeMillis() - mStartRecordMills) / 1000;
        long   hour     = time / 3600;
        long   minute   = time / 60;
        long   seconds  = time % 60;
        String duration = String.format(Locale.getDefault(), "%02d:%02d:%02d", hour, minute, seconds);
        mTimerView.setText(duration);
        mFullscreenTimerView.setText(duration);
        mHandler.sendEmptyMessageDelayed(MSG_TIMER_TICK, TIME_TICK_INTERVAL);
    }

    private void startTick() {
        mIsTicking = true;
        mTimerView.setVisibility(View.VISIBLE);
        mStartRecordMills = System.currentTimeMillis();
        mHandler.sendEmptyMessage(MSG_TIMER_TICK);
    }

    private void stopTick() {
        mIsTicking = false;
        mTimerView.setVisibility(View.INVISIBLE);
        //        mStartRecordMills = 0L;
        mHandler.removeMessages(MSG_TIMER_TICK);
    }

    private void setPortraitGLView() {
        Logger.d(TAG, "setPortraitGLView()");
        mGLViewLayout.setLayoutParams(mPortraitLayoutParams);
    }

    private void setLandscapeGLView() {
        Logger.d(TAG, "setLandscapeGLView()");
        mGLViewLayout.setLayoutParams(mLandscapeLayoutParams);
    }

    private void showFullscreenBar() {
        mFullscreenToolbarView.setVisibility(View.VISIBLE);
        mFullscreenTitleView.setVisibility(View.VISIBLE);
        mIsShowFullscreenToolBar = true;
    }

    private void hideFullscreenToolbar() {
        mFullscreenTitleView.setVisibility(View.INVISIBLE);
        mFullscreenToolbarView.setVisibility(View.INVISIBLE);
        mIsShowFullscreenToolBar = false;
    }

    private void showNetworkBadDialog() {
        showDialogFragment(getString(R.string.gowild_av_network_bad));
    }

    private void showEnterErrorDialog() {
        showDialogFragment(getString(R.string.gowild_av_enter_error));
    }

    private void showRobotNotOnlineDialog() {
        showDialogFragment(getString(R.string.gowild_av_robot_not_online));
    }

    private void showDialogFragment(String content) {
        hideLoading();
        isShow = true;
        if (!isShowDialog) {
            return;
        }
        if (isFinishing()) {
            return;
        }
        if (mDialog != null) {
            mDialog.setDialogContent(content);
            mDialog.setDialogTitle(getString(R.string.gowild_prompt));
            mDialog.setCancelable(false);
            mDialog.setDialogListener(mDialogListener);
            mDialog.setDialogTitleVi(false);
            mDialog.setDialogCancel(getString(R.string.gowild_cancel));
            mDialog.setDialogConfirm(getString(R.string.gowild_ok));
            mDialog.setDialogContent(content);
            mDialog.setDialogTitle(getString(R.string.gowild_prompt));
            if (!mDialog.isAdded()) {
                mDialog.show(getFragmentManager(), GowildNormalDialog.class.getSimpleName());
            }
        }
    }

    private void showRobotBusyDialog(boolean isInterrupt) {
        mGLMediaView.closeLiveVideo();
        String content;
        if (isInterrupt) {
            content = getString(R.string.gowild_av_robot_interrupt);
        } else {
            content = getString(R.string.gowild_av_robot_busy);
        }
        showDialogFragment(content);
    }

    private class DialogListener implements OnNormalDialogListener {

        @Override
        public void onCancelListener() {
            Logger.d(TAG, "onCancelListener()");
        }

        @Override
        public void onConfirmListener() {
            Logger.d(TAG, "onConfirmListener()");
            hideLoading();
            if (mDialog != null &&mDialog.isAdded()) {
                isShow = false;
                mDialog.dismiss();
            }
            mHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    onBackPressed();
                }
            },500);
        }
    }

    private void onRequestPortraitScreen() {
        Logger.d(TAG, "onRequestPortraitScreen()");
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        this.mLandscapeLayout.setVisibility(View.GONE);
        this.mPortraitLayout.setVisibility(View.VISIBLE);
        if (mIsBindedRobot && mIsNetworkAvailable) {
            this.mGLViewLayout.setVisibility(View.VISIBLE);
        } else {
            this.mGLViewLayout.setVisibility(View.INVISIBLE);
        }
        showTopbarView();
        setPortraitGLView();
        mIsPortrait = true;
        updateRecordView();
    }

    private void updateRecordView() {
        if (mIsPortrait) {
            if (mIsTicking) {
                mTimerView.setVisibility(View.VISIBLE);
                mFullscreenTimerView.setVisibility(View.INVISIBLE);
            }
            findViewById(R.id.gowild_av_monitor_portrait_record_btn).setSelected(mIsRecording);
        } else {
            if (mIsTicking) {
                mTimerView.setVisibility(View.INVISIBLE);
                mFullscreenTimerView.setVisibility(View.VISIBLE);
            }
            findViewById(R.id.gowild_av_monitor_landscape_record_btn).setSelected(mIsRecording);
        }
    }

    private void onRequestLandscapeScreen() {
        Logger.d(TAG, "onRequestLandscape()");
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        initFullscreenToolbarView();
        this.mLandscapeLayout.setVisibility(View.VISIBLE);
        this.mPortraitLayout.setVisibility(View.INVISIBLE);
        if (mIsBindedRobot && mIsNetworkAvailable) {
            this.mGLViewLayout.setVisibility(View.VISIBLE);
        } else {
            this.mGLViewLayout.setVisibility(View.INVISIBLE);
        }
        goneTopbarView();
        showFullscreenBar();
        setLandscapeGLView();
        mIsPortrait = false;
        updateRecordView();
    }

    private void initFullscreenToolbarView() {
        if (mFullscreenTitleView == null) {
            LayoutInflater inflater = getLayoutInflater();
            View                        view     = inflater.inflate(R.layout.layout_gowild_av_monitor_fullscreen_topbar, mLandscapeLayout, false);
            RelativeLayout.LayoutParams params   = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, getFullTopbarHeight());
            params.addRule(RelativeLayout.ALIGN_PARENT_TOP);
            params.addRule(RelativeLayout.CENTER_HORIZONTAL);
            int left   = view.getPaddingLeft();
            int right  = view.getPaddingRight();
            int top    = view.getPaddingTop() + getTopbarPaddingTop();
            int bottom = view.getPaddingBottom();
            view.setPadding(left, top, right, bottom);
            View fullscreenView = view.findViewById(R.id.gowild_av_monitor_landscape_back_btn);
            fullscreenView.setOnClickListener(mOnFucBtnClickListener);
            fullscreenView = view.findViewById(R.id.gowild_av_monitor_landscape_photo_list_btn);
            fullscreenView.setOnClickListener(mOnFucBtnClickListener);
            this.mLandscapeLayout.addView(view, params);
            this.mFullscreenTitleView = view;
        }
    }

    private class OnLinkCameraStatusListener implements GLMediaView.LinkCameraStatusListener {

        @Override
        public void startToLink() {
            Log.d(TAG, "startToLink()");
            showLoading(R.string.gowild_av_loading);
            mIsLinkSuccess = false;
        }

        @Override
        public void linkSucces() {
            Log.d(TAG, "linkSuccess()");
            hideLoading();
            mIsLinkSuccess = true;
            mGLMediaView.soundOn();
        }

        @Override
        public void linkFailed(AbstractMediaView.LinkCameraError errorDesc) {
            Log.d(TAG, "linkFailed() s = " + errorDesc.toString());
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
//                    quitAVMonitor();
                    showNetworkBadDialog();
                    mIsLinkSuccess = false;
                    hideLoading();
//                    if (!mScheduleCheckLinkSuccess) {
//                        showNetworkBadDialog();
//                    } else {
//                        quitAVMonitor();
//                        showEnterErrorDialog();
//                        mIsLinkSuccess = false;
//                        hideLoading();
//                    }
                }
            });
        }
    }

    private void updateUI() {
        showLoading(R.string.gowild_av_loading);
        //逻辑太复杂重写
        mIsNetworkAvailable = CheckNetworkUtil.isNetworkAvailable(this);
        // 首先判断是否异常
        if (!mIsNetworkAvailable || !mIsBindedRobot || !mMachineOnLine) {
            mNoBindLayout.setVisibility(View.VISIBLE);
            mGLViewLayout.setVisibility(View.INVISIBLE);
            mLandscapeUnbindedLayout.setVisibility(View.VISIBLE);
        }

        // 然后三种异常情况逐一判断
        if (!mIsNetworkAvailable) {
            Logger.d(TAG, "网络不可用");
            mPBindTv.setText(R.string.gowild_av_network_unavailable);
            mLBindTv.setText(R.string.gowild_av_network_unavailable);
            mBindBtn.setText(R.string.gowild_av_set_network_now);
            mLandscapeBindBtn.setText(R.string.gowild_av_set_network_now);
            hideLoading();
            return;
        }

        if (!mIsBindedRobot) {
            Logger.d(TAG, "未绑定");

            mPBindTv.setText("没有检测到与账号绑定的" + CommonDataUtil.getCurrentRobotTitle() + "，\n暂时无法进行实时看护，\n请检测 " + CommonDataUtil.getCurrentRobotTitle() + "的网络设置后重试！");
            mLBindTv.setText("没有检测到与账号绑定的" + CommonDataUtil.getCurrentRobotTitle() + "，\n暂时无法进行实时看护，\n请检测 " + CommonDataUtil.getCurrentRobotTitle() + "的网络设置后重试！");
            mBindBtn.setText(R.string.gowild_av_monitor_go_bind);
            mLandscapeBindBtn.setText(R.string.gowild_av_monitor_go_bind);
            hideLoading();
            return;
        }

        //最后做连接判断
        //        if (!mIsLinkSuccess) {
        Logger.d(TAG, "视频连接中");
        mNoBindLayout.setVisibility(View.INVISIBLE);
        this.mLandscapeUnbindedLayout.setVisibility(View.INVISIBLE);
        mGLViewLayout.setVisibility(View.VISIBLE);
//        if (mViewerManager.hasLogin()) {
//            startAVMonitor();
//        }
        if (mDialog != null &&mDialog.isAdded()) {
            isShow = false;
            mDialog.dismiss();
        }
    }

    private void onStartAVMonitorRobotNotOnline() {
        mMachineOnLine = false;
        //        updateUI();
        showRobotNotOnlineDialog();
    }

    private void onStartAVMonitorRobotBusy() {
        showRobotBusyDialog(false);
    }

    private void onGetAuthorizationFail() {
        Logger.d(TAG, "onGetAuthorizationFail()");
        showDialogFragment("授权失败");
    }
}
