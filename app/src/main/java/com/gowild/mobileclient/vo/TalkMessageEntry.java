/**
 * Copyright (C)  2016 深圳市狗尾草智能科技有限公司
 * GowildXB
 * TalkMessageEntry.java
 */
package com.gowild.mobileclient.vo;

/**
 * @author shuai. wang
 * @version 1.0
 *          <p><strong>Features draft description.主要功能介绍</strong></p>
 * @since 2016/8/5 21:19
 */
public class TalkMessageEntry {
	// ===========================================================
	// HostConstants
	// ===========================================================

	private static final String TAG = TalkMessageEntry.class.getSimpleName();
	/**
	 * sendId : 13800138002
	 * id : 80
	 * msgType : 0
	 * msgSource : 1
	 * createTime : 2016-08-05 18:14:05
	 * msgStatus : 0
	 * recvId : 13800138001
	 * content : 小胖子一直在牵小飞飞的手，我可不可以和TA去约会呀
	 * sendNickName : 小胖子
	 * recvIdNickName : 小飞飞
	 * magicCode : 761126927955132400
	 */

	public String sendId;
	public String id;
	public String msgType;
	public int msgSource;
	public String createTime;
	public String msgStatus;
	public String recvId;
	public String content;
	public String sendNickName;
	public String recvIdNickName;
	public long magicCode;


	// ===========================================================
	// Static Fields
	// ===========================================================

	// ===========================================================
	// Fields
	// ===========================================================


	// ===========================================================
	// Constructors
	// ===========================================================


	// ===========================================================
	// Getter or Setter
	// ===========================================================


	// ===========================================================
	// Methods for/from SuperClass/Interfaces
	// ===========================================================


	// ===========================================================
	// Methods
	// ===========================================================


	// ===========================================================
	// Inner and Anonymous Classes
	// ===========================================================

}
