/**
 * Copyright (C)  2016 深圳市狗尾草智能科技有限公司
 * GowildXB
 * PetInfo.java
 */
package com.gowild.mobileclient.vo;

/**
 * @author shuai. wang
 * @since 2016/8/4 21:11
 * @version 1.0
 * <p><strong>Features draft description.主要功能介绍</strong></p>
 */
public class PetInfo {

	// ===========================================================
	// HostConstants
	// ===========================================================

	private static final String TAG = PetInfo.class.getSimpleName();
	/**
	 * id : 1
	 * accountId : 2
	 * coinCnt : 15
	 * experience : 15
	 * experienceAll : 29
	 * petAccontRank : 125
	 * petRankAll : 11
	 * honorName : wode
	 * level : 1
	 * honorAll : 48
	 * honorAccount : 0
	 * missionAll : 48
	 * accountMission : 0
	 * skillAll : 15
	 * accountSkill : 15
	 */

	public String id;
	public String accountId;
	public String coinCnt;
	public String experience;
	public String experienceAll;
	public String petAccontRank;
	public String petRankAll;
	public String honorName;
	public String level;
	public String honorAll;
	public String honorAccount;
	public String missionAll;
	public String accountMission;
	public String skillAll;
	public String accountSkill;
	public int sex;

	// ===========================================================
	// Static Fields
	// ===========================================================

	// ===========================================================
	// Fields
	// ===========================================================


	// ===========================================================
	// Constructors
	// ===========================================================


	// ===========================================================
	// Getter or Setter
	// ===========================================================


	// ===========================================================
	// Methods for/from SuperClass/Interfaces
	// ===========================================================


	// ===========================================================
	// Methods
	// ===========================================================


	// ===========================================================
	// Inner and Anonymous Classes
	// ===========================================================

}
