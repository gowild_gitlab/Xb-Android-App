/**
 * Copyright (C)  2016 深圳市狗尾草智能科技有限公司
 * GowildXB
 * LoverList.java
 */
package com.gowild.mobileclient.vo;

import java.util.List;

/**
 * @author shuai. wang
 * @since 2016/8/4 15:50
 * @version 1.0
 * <p><strong>Features draft description.主要功能介绍</strong></p>
 */
public class LoverList {
	/**
	 * code : 100
	 * desc : 请求成功
	 * data : [{"nickname":"女月金","id":"1","userId":"5","relateId":"6","relateStatus":"11","relateType":"1","magicCode":"757775277056065536","createTime":"2016-06-03 16:21:22"}]
	 */

	public String code;
	public String desc;
	/**
	 * nickname : 女月金
	 * id : 1
	 * userId : 5
	 * relateId : 6
	 * relateStatus : 11
	 * relateType : 1
	 * magicCode : 757775277056065536
	 * createTime : 2016-06-03 16:21:22
	 */

	public List<Entity> data;

	public static class Entity {
		public String nickname;
		public String id;
		public String userId;
		public String relateId;
		public String relateStatus;
		public String relateType;
		public long magicCode;
		public String createTime;

		@Override
		public String toString() {
			return "Entity{" +
					"nickname='" + nickname + '\'' +
					", id='" + id + '\'' +
					", userId='" + userId + '\'' +
					", relateId='" + relateId + '\'' +
					", relateStatus='" + relateStatus + '\'' +
					", relateType='" + relateType + '\'' +
					", magicCode=" + magicCode +
					", createTime='" + createTime + '\'' +
					'}';
		}
	}

//	public static class GrowTaskEntry {
//		public String nickname;
//		public String id;
//		public String userId;
//		public String relateId;
//		public String relateStatus;
//		public String relateType;
//		public String magicCode;
//		public String createTime;
//	}

	@Override
	public String toString() {
		return "LoverList{" +
				"code='" + code + '\'' +
				", desc='" + desc + '\'' +
				", data=" + data +
				'}';
	}


	// ===========================================================
	// Static Fields
	// ===========================================================

	// ===========================================================
	// Fields
	// ===========================================================


	// ===========================================================
	// Constructors
	// ===========================================================


	// ===========================================================
	// Getter or Setter
	// ===========================================================


	// ===========================================================
	// Methods for/from SuperClass/Interfaces
	// ===========================================================


	// ===========================================================
	// Methods
	// ===========================================================


	// ===========================================================
	// Inner and Anonymous Classes
	// ===========================================================

}
