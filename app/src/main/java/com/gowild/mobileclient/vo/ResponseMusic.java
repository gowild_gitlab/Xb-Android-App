/**
 * Copyright (C)  2016 深圳市狗尾草智能科技有限公司
 * GowildXB
 * ResponseMusic.java
 */
package com.gowild.mobileclient.vo;

import com.alibaba.fastjson.annotation.JSONField;

import java.util.ArrayList;

/**
 * @author ljd
 * @since 2016/8/5 22:15
 * @version 1.0
 * <p><strong>Features draft description.主要功能介绍</strong></p>
 */
public class ResponseMusic {

    // ===========================================================
    // HostConstants
    // ===========================================================

    private static final String TAG = ResponseMusic.class.getSimpleName();

    // ===========================================================
    // Static Fields
    // ===========================================================

    // ===========================================================
    // Fields
    // ===========================================================
//    @JSONField(name = "data")
//    public Data data;
    // ===========================================================
    // Constructors
    // ===========================================================


    // ===========================================================
    // Getter or Setter
    // ===========================================================


    // ===========================================================
    // Methods for/from SuperClass/Interfaces
    // ===========================================================


    // ===========================================================
    // Methods
    // ===========================================================


    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================
//    public class Data {
        @JSONField(name = "pageNum")
        public int pageNum;
        @JSONField(name = "hasPreviousPage")
        public boolean hasPreviousPage;
        @JSONField(name = "hasNextPage")
        public boolean hasNextPage;
        @JSONField(name = "prePage")
        public boolean prePage;
        @JSONField(name = "nextPage")
        public boolean nextPage;
        @JSONField(name = "list")
        public ArrayList<Music> musics;
//    }

}
