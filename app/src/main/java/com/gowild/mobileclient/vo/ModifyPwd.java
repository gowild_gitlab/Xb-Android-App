/**
 * Copyright (C)  2016 深圳市狗尾草智能科技有限公司
 * GowildXB
 * Achievement.java
 */
package com.gowild.mobileclient.vo;

import com.alibaba.fastjson.annotation.JSONField;

/**
 * @author Administrator
 * @version 1.0
 *          <p><strong>修改密码</strong></p>
 * @since 2016/8/4 14:41
 */
public class ModifyPwd {

    @JSONField(name = "id")
    public String mId;

}
