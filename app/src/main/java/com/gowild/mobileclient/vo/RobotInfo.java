/**
 * Copyright (C)  2016 深圳市狗尾草智能科技有限公司
 * GowildXB
 * RobotInfo.java
 */
package com.gowild.mobileclient.vo;

import com.alibaba.fastjson.annotation.JSONField;

/**
 * @author ljd
 * @version 1.0
 *          <p><strong>Features draft description.主要功能介绍</strong></p>
 * @since 2016/8/12 10:48
 */
public class RobotInfo extends Response{

    // ===========================================================
    // HostConstants
    // ===========================================================

    private static final String TAG = RobotInfo.class.getSimpleName();

    // ===========================================================
    // Static Fields
    // ===========================================================

    // ===========================================================
    // Fields
    // ===========================================================
    @JSONField(name = "bindStatus")
    public int bindStatus;
    @JSONField(name = "robotVersion")
    public String robotVersion;
    @JSONField(name = "robotVolume")
    public int robotVolume;
    @JSONField(name = "talkSpeed")
    public int talkSpeed;
    @JSONField(name = "mac")
    public String mac;
    @JSONField(name = "wifiName")
    public String wifiName;
    @JSONField(name = "wifiPass")
    public String wifiPass;
    @JSONField(name = "serialNo")
    public String serialNo;
    // ===========================================================
    // Constructors
    // ===========================================================


    // ===========================================================
    // Getter or Setter
    // ===========================================================


    // ===========================================================
    // Methods for/from SuperClass/Interfaces
    // ===========================================================


    // ===========================================================
    // Methods
    // ===========================================================


    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================

}
