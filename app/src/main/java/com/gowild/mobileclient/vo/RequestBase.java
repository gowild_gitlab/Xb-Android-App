/**
 * Copyright (C)  2016 深圳市狗尾草智能科技有限公司
 * GowildXB
 * RequestBase.java
 */
package com.gowild.mobileclient.vo;

import com.loopj.android.http.RequestParams;

import java.util.HashMap;

/**
 * @author Administrator
 * @since 2016/7/28 14:06
 * @version 1.0
 * <p><strong>Features draft description.主要功能介绍</strong></p>
 */
public abstract class RequestBase {

    // ===========================================================
    // HostConstants
    // ===========================================================

    private static final String TAG = RequestBase.class.getSimpleName();

    // ===========================================================
    // Static Fields
    // ===========================================================

    // ===========================================================
    // Fields
    // ===========================================================
    protected HashMap<String, String> mMap = new HashMap<>();

    // ===========================================================
    // Constructors
    // ===========================================================


    // ===========================================================
    // Getter or Setter
    // ===========================================================


    // ===========================================================
    // Methods for/from SuperClass/Interfaces
    // ===========================================================


    // ===========================================================
    // Methods
    // ===========================================================
    public void put(String key, String value) {
        mMap.put(key, value);
    }

    public RequestParams getRequestParams() {
        return new RequestParams(mMap);
    }

    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================

}
