/**
 * Copyright (C)  2016 深圳市狗尾草智能科技有限公司
 * GowildXB
 * LunchHeader.java
 */
package com.gowild.mobileclient.vo;

/**
 * @author shuai. wang
 * @since 2016/7/29 16:37
 * @version 1.0
 * <p><strong>Features draft description.主要功能介绍</strong></p>
 */
public class LunchHeader {

	// ===========================================================
	// HostConstants
	// ===========================================================

	private static final String TAG = LunchHeader.class.getSimpleName();
	/**
	 * code : 100
	 * desc : 请求成功
	 * data : {"access_token":"7c999618-d5e4-4d53-bb5c-8b2d912fa163","token_type":"bearer","refresh_token":"9738f926-9f15-4688-91cb-197dbd27a230","expires_in":43099,"scope":"read write trust"}
	 * success : true
	 */

	public int code;
	public String desc;
	/**
	 * access_token : 7c999618-d5e4-4d53-bb5c-8b2d912fa163
	 * token_type : bearer
	 * refresh_token : 9738f926-9f15-4688-91cb-197dbd27a230
	 * expires_in : 43099
	 * scope : read write trust
	 */

	public DataEntity data;
	public boolean success;

	public static class DataEntity {
		public String access_token;
		public String token_type;
		public String refresh_token;
		public int expires_in;
		public String scope;
	}

//	/**
//	 * access_token : ac0749fd-f0f3-4be2-8f2c-47d58db38f63
//	 * token_type : bearer
//	 * refresh_token : bb0d6a36-034f-47c7-8111-8c680f07f1b0
//	 * expires_in : 43199
//	 * scope : read write trust
//	 */
//
//	public String access_token;
//	public String token_type;
//	public String refresh_token;
//	public int expires_in;
//	public String scope;




}
