/**
 * Copyright (C)  2016 深圳市狗尾草智能科技有限公司
 * GowildXB
 * TalkMessage.java
 */
package com.gowild.mobileclient.vo;

import java.util.List;

/**
 * @author shuai. wang
 * @since 2016/8/5 20:31
 * @version 1.0
 * <p><strong>Features draft description.主要功能介绍</strong></p>
 */
public class TalkMessage {

	// ===========================================================
	// HostConstants
	// ===========================================================

	private static final String TAG = TalkMessage.class.getSimpleName();

	public int pageNum;
	public int pageSize;
	public int size;
	public String orderBy;
	public int startRow;
	public int endRow;
	public int total;
	public int pages;
	public int firstPage;
	public int prePage;
	public int nextPage;
	public int lastPage;
	public boolean isFirstPage;
	public boolean isLastPage;
	public boolean hasPreviousPage;
	public boolean hasNextPage;
	public int navigatePages;
	/**
	 * id : 1
	 * sendId : 5
	 * recvId : 6
	 * content : 李大大与女月金建立了配对关系
	 * duration : null
	 * msgStatus : 1
	 * msgType : 0
	 * msgSource : 1
	 * magicCode : 757775277056065500
	 * createTime : 2016-06-0316: 21: 22
	 * sendNickName : 李大大
	 * recvIdNickName : 女月金
	 */

	public List list;
	public List<Integer> navigatepageNums;

	// ===========================================================
	// Static Fields
	// ===========================================================

	// ===========================================================
	// Fields
	// ===========================================================


	// ===========================================================
	// Constructors
	// ===========================================================


	// ===========================================================
	// Getter or Setter
	// ===========================================================


	// ===========================================================
	// Methods for/from SuperClass/Interfaces
	// ===========================================================


	// ===========================================================
	// Methods
	// ===========================================================


	// ===========================================================
	// Inner and Anonymous Classes
	// ===========================================================

}
