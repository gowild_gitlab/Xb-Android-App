/**
 * Copyright (C)  2016 深圳市狗尾草智能科技有限公司
 * GowildXB
 * ResponseCreateClock.java
 */
package com.gowild.mobileclient.vo;

import com.alibaba.fastjson.annotation.JSONField;

/**
 * @author Administrator
 * @since 2016/7/28 10:32
 * @version 1.0
 * <p><strong>Features draft description.主要功能介绍</strong></p>
 */
public class ResponseAchievement extends Response {

    // ===========================================================
    // HostConstants
    // ===========================================================


    private static final String TAG = ResponseAchievement.class.getSimpleName();

    // ===========================================================
    // Static Fields
    // ===========================================================

    // ===========================================================
    // Fields
    // ===========================================================
    @JSONField(name = "data")
    public AchievementPageInfo mAchievemen;

    public AchievementPageInfo getAchievemen() {
        return mAchievemen;
    }

    public void setAchievemen(AchievementPageInfo achievemen) {
        this.mAchievemen = achievemen;
    }

    // ===========================================================
    // Constructors
    // ===========================================================


    // ===========================================================
    // Getter or Setter
    // ===========================================================


    // ===========================================================
    // Methods for/from SuperClass/Interfaces
    // ===========================================================


    // ===========================================================
    // Methods
    // ===========================================================


    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================

}
