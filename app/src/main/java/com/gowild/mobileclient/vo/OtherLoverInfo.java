/**
 * Copyright (C)  2016 深圳市狗尾草智能科技有限公司
 * GowildXB
 * OtherLoverInfo.java
 */
package com.gowild.mobileclient.vo;

/**
 * @author shuai. wang
 * @since 2016/8/5 19:53
 * @version 1.0
 * <p><strong>Features draft description.主要功能介绍</strong></p>
 */
public class OtherLoverInfo {

	// ===========================================================
	// HostConstants
	// ===========================================================


	private static final String TAG = OtherLoverInfo.class.getSimpleName();
	/**
	 * nickname : 女月金
	 * email : null
	 * sex : 1
	 * birthday : null
	 * phone : 13923492068
	 * address : null
	 * hobby : null
	 */

	public String nickname;
	public String email;
	public int sex;
	public String birthday;
	public String phone;
	public String address;
	public String hobby;

	// ===========================================================
	// Static Fields
	// ===========================================================

	// ===========================================================
	// Fields
	// ===========================================================


	// ===========================================================
	// Constructors
	// ===========================================================


	// ===========================================================
	// Getter or Setter
	// ===========================================================


	// ===========================================================
	// Methods for/from SuperClass/Interfaces
	// ===========================================================


	// ===========================================================
	// Methods
	// ===========================================================


	// ===========================================================
	// Inner and Anonymous Classes
	// ===========================================================

}
