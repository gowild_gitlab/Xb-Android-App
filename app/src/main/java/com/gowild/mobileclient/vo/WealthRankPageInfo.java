/**
 * Copyright (C)  2016 深圳市狗尾草智能科技有限公司
 * GowildXB
 * AchievementPageInfo.java
 */
package com.gowild.mobileclient.vo;

import com.alibaba.fastjson.annotation.JSONField;

import java.util.List;

/**
 * @author Administrator
 * @version 1.0
 *          <p><strong>财富排行分页</strong></p>
 * @since 2016/8/4 14:32
 */
public class WealthRankPageInfo {

    @JSONField(name = "pageNum")
    public int mPageNum;

    @JSONField(name = "pageSize")
    public int mPageSize;

    @JSONField(name = "size")
    public int mSize;


    @JSONField(name = "startRow")
    public int mStartRow;

    @JSONField(name = "endRow")
    public int mEndRow;

    @JSONField(name = "total")
    public int mTotal;

    @JSONField(name = "pages")
    public int mPages;

    @JSONField(name = "firstPage")
    public int mFirstPage;

    @JSONField(name = "prePage")
    public int mPrePage;

    @JSONField(name = "nextPage")
    public int mNextPage;

    @JSONField(name = "lastPage")
    public int mLastPage;

    @JSONField(name = "isFirstPage")
    public boolean mIsFirstPage;

    @JSONField(name = "isLastPage")
    public boolean mIsLastPage;

    @JSONField(name = "hasPreviousPage")
    public boolean mHasPreviousPage;

    @JSONField(name = "hasNextPage")
    public boolean mHasNextPage;

    @JSONField(name = "navigatePages")
    public int mNavigatePages;


//    @JSONField(name = "navigatepageNums")
//    public List<Object> mNavigatepageNums;


    @JSONField(name = "list")
    public List<WealthRank> mWealthRanktList;

    public int getPageNum() {
        return mPageNum;
    }

    public int getPageSize() {
        return mPageSize;
    }

    public int getSize() {
        return mSize;
    }

    public int getStartRow() {
        return mStartRow;
    }

    public int getEndRow() {
        return mEndRow;
    }

    public int getTotal() {
        return mTotal;
    }

    public int getPages() {
        return mPages;
    }

    public int getFirstPage() {
        return mFirstPage;
    }

    public int getPrePage() {
        return mPrePage;
    }

    public int getNextPage() {
        return mNextPage;
    }

    public int getLastPage() {
        return mLastPage;
    }

    public boolean isIsFirstPage() {
        return mIsFirstPage;
    }

    public boolean isIsLastPage() {
        return mIsLastPage;
    }

    public boolean isHasPreviousPage() {
        return mHasPreviousPage;
    }

    public boolean isHasNextPage() {
        return mHasNextPage;
    }

    public int getNavigatePages() {
        return mNavigatePages;
    }

    public List<WealthRank> getWealthRankList() {
        return mWealthRanktList;
    }
}
