/**
 * Copyright (C)  2016 深圳市狗尾草智能科技有限公司
 * GowildXB
 * TalkMsg.java
 */
package com.gowild.mobileclient.vo;

import java.util.List;

/**
 * @author shuai. wang
 * @version 1.0
 *          <p><strong>Features draft description.主要功能介绍</strong></p>
 * @since 2016/8/3 21:08
 */
public class TalkMsg {

	// ===========================================================
	// HostConstants
	// ===========================================================

	private static final String TAG = TalkMsg.class.getSimpleName();
	/**
	 * pageNum : 1
	 * pageSize : 10
	 * size : 10
	 * orderBy : CREATE_TIME desc
	 * startRow : 1
	 * endRow : 10
	 * total : 18
	 * pages : 2
	 * list : [{"photoUrl":"http://1469522795246","createTime":"2016-07-26 16:46:35.0"},{"photoUrl":"http://1469522490542","createTime":"2016-07-26 16:41:31.0"},{"photoUrl":"http://1469522086021","createTime":"2016-07-26 16:34:46.0"},{"photoUrl":"http://1469521950072","createTime":"2016-07-26 16:32:30.0"},{"photoUrl":"http://1469521875529","createTime":"2016-07-26 16:31:16.0"},{"photoUrl":"http://1469521831899","createTime":"2016-07-26 16:30:32.0"},{"photoUrl":"http://resource.gowild.cn/2016053118482755800790.jpg","createTime":"2016-07-26 14:02:59.0"},{"photoUrl":"http://resource.gowild.cn/2016053118482755800790.jpg","createTime":"2016-07-26 13:56:44.0"},{"photoUrl":"http://resource.gowild.cn/2016053118482755800790.jpg","createTime":"2016-07-26 13:55:15.0"},{"photoUrl":"http://resource.gowild.cn/2016053118482755800790.jpg","createTime":"2016-07-26 13:54:16.0"}]
	 * firstPage : 1
	 * prePage : 0
	 * nextPage : 2
	 * lastPage : 2
	 * isFirstPage : true
	 * isLastPage : false
	 * hasPreviousPage : false
	 * hasNextPage : true
	 * navigatePages : 8
	 * navigatepageNums : [1,2]
	 */

	public int pageNum;
	public int pageSize;
	public int size;
	public String orderBy;
	public int startRow;
	public int endRow;
	public int total;
	public int pages;
	public String firstPage;
	public String prePage;
	public String nextPage;
	public String lastPage;
	public boolean isFirstPage;
	public boolean isLastPage;
	public boolean hasPreviousPage;
	public boolean hasNextPage;
	public int navigatePages;
	/**
	 * photoUrl : http://1469522795246
	 * createTime : 2016-07-26 16:46:35.0
	 */

	public List<Entity> list;
	public List<Integer> navigatepageNums;

	public static class Entity {

		/**
		 * id : 1
		 * sendId : 5
		 * recvId : 6
		 * content : 李大大与女月金建立了配对关系
		 * duration : null
		 * msgStatus : 1
		 * msgType : 0
		 * msgSource : 1
		 * magicCode : 757775277056065536
		 * createTime : 2016-06-03 16:21:22
		 * sendNickName : 李大大
		 * recvIdNickName : 女月金
		 */

		public String id;
		public int sendId;
		public int recvId;
		public String content;
		public Object duration;
		public String msgStatus;
		public String msgType;
		public String msgSource;
		public String magicCode;
		public String createTime;
		public String sendNickName;
		public String recvIdNickName;
	}


	// ===========================================================
	// Static Fields
	// ===========================================================

	// ===========================================================
	// Fields
	// ===========================================================


	// ===========================================================
	// Constructors
	// ===========================================================


	// ===========================================================
	// Getter or Setter
	// ===========================================================


	// ===========================================================
	// Methods for/from SuperClass/Interfaces
	// ===========================================================


	// ===========================================================
	// Methods
	// ===========================================================


	// ===========================================================
	// Inner and Anonymous Classes
	// ===========================================================

}
