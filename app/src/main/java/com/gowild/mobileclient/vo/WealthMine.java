/**
 * Copyright (C)  2016 深圳市狗尾草智能科技有限公司
 * GowildXB
 * WealthMine.java
 */
package com.gowild.mobileclient.vo;

import com.alibaba.fastjson.annotation.JSONField;

/**
 * @author Administrator
 * @since 2016/8/4 19:00
 * @version 1.0
 * <p><strong>我的财富信息</strong></p>
 */
public class WealthMine {

    @JSONField(name = "allCount")
    public String mAllCount;

    @JSONField(name = "petRank")
    public String mPetRank;

    @JSONField(name = "coinCnt")
    public String mCoinCnt;

    public String getAllCount() {
        return mAllCount;
    }

    public String getCoinCnt() {
        return mCoinCnt;
    }

    public String getPetRank() {
        return mPetRank;
    }
}
