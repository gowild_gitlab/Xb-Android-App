/**
 * Copyright (C)  2016 深圳市狗尾草智能科技有限公司
 * GowildXB
 * Achievement.java
 */
package com.gowild.mobileclient.vo;

import com.alibaba.fastjson.annotation.JSONField;

/**
 * @author Administrator
 * @version 1.0
 *          <p><strong>勋章成就信息</strong></p>
 * @since 2016/8/4 14:41
 */
public class Achievement {

    @JSONField(name = "id")
    public String mId;

    @JSONField(name = "achieveContent")
    public String mAchieveContent;

    @JSONField(name = "experience")
    public int mExperience;

    @JSONField(name = "coinCnt")
    public int mCoinCnt;

    @JSONField(name = "achieveMedal")
    public String mAchieveMedal;

    @JSONField(name = "type")
    public String mType;

    @JSONField(name = "typeName")
    public String mTypeName;

    @JSONField(name = "surplus")
    public int mSurplus;

    @JSONField(name = "countAll")
    public int mCountAll;

    @JSONField(name = "number")
    public float mNumber;

    @JSONField(name = "accountNumber")
    public float mAccountNumbe;

    @JSONField(name = "pictureUrl")
    public String mPictureUrl;

    public String getId() {
        return mId;
    }

    public String getAchieveContent() {
        return mAchieveContent;
    }

    public int getExperience() {
        return mExperience;
    }

    public int getCoinCnt() {
        return mCoinCnt;
    }

    public String getAchieveMedal() {
        return mAchieveMedal;
    }

    public String getType() {
        return mType;
    }

    public int getSurplus() {
        return mSurplus;
    }

    public int getCountAll() {
        return mCountAll;
    }

    public float getNumber() {
        return mNumber;
    }

    public float getAccountNumbe() {
        return mAccountNumbe;
    }

    public String getPictureUrl() {
        return mPictureUrl;
    }

    public String getTypeName() {
        return mTypeName;
    }
}
