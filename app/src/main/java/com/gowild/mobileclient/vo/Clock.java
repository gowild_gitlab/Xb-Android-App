/**
 * Copyright (C)  2016 深圳市狗尾草智能科技有限公司
 * GowildXB
 * Clock.java
 */
package com.gowild.mobileclient.vo;

import com.alibaba.fastjson.annotation.JSONField;

/**
 * @author Administrator
 * @version 1.0
 *          <p><strong>Features draft description.主要功能介绍</strong></p>
 * @since 2016/7/28 18:11
 */
public class Clock {

    // ===========================================================
    // HostConstants
    // ===========================================================

    private static final String TAG = Clock.class.getSimpleName();

    // ===========================================================
    // Static Fields
    // ===========================================================

    // ===========================================================
    // Fields
    // ===========================================================
    @JSONField(name = "alarmId")
    public int mAlarmId;

    @JSONField(name = "alarmFlag")
    public String mAlarmFlag;

    @JSONField(name = "hour")
    public int mHour;

    @JSONField(name = "minute")
    public int mMinute;

    @JSONField(name = "enabled")
    public int mEnabled;

    @JSONField(name = "cycleIndex")
    public int mCycleIndex;
    // ===========================================================
    // Constructors
    // ===========================================================


    // ===========================================================
    // Getter or Setter
    // ===========================================================


    // ===========================================================
    // Methods for/from SuperClass/Interfaces
    // ===========================================================


    // ===========================================================
    // Methods
    // ===========================================================


    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================

}
