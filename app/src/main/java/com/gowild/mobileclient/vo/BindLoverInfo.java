/**
 * Copyright (C)  2016 深圳市狗尾草智能科技有限公司
 * GowildXB
 * BindLoverInfo.java
 */
package com.gowild.mobileclient.vo;

/**
 * @author shuai. wang
 * @version 1.0
 *          <p><strong>Features draft description.主要功能介绍</strong></p>
 * @since 2016/8/3 16:42
 */
public class BindLoverInfo {
	public String nickname;
	public String id;
	public String userId;
	public String relateId;
	public String relateStatus;
	public String relateType;
	public long magicCode;
	public String createTime;
}


// ===========================================================
// Static Fields
// ===========================================================

// ===========================================================
// Fields
// ===========================================================


// ===========================================================
// Constructors
// ===========================================================


// ===========================================================
// Getter or Setter
// ===========================================================


// ===========================================================
// Methods for/from SuperClass/Interfaces
// ===========================================================


// ===========================================================
// Methods
// ===========================================================


// ===========================================================
// Inner and Anonymous Classes
// ===========================================================

