/**
 * Copyright (C)  2016 深圳市狗尾草智能科技有限公司
 * GowildXB
 * StoryAlbum.java
 */
package com.gowild.mobileclient.vo;

import com.alibaba.fastjson.annotation.JSONField;

/**
 * @author ljd
 * @version 1.0
 *          <p><strong>Features draft description.主要功能介绍</strong></p>
 * @since 2016/8/9 18:02
 */
public class StoryChapter {

    // ===========================================================
    // HostConstants
    // ===========================================================

    private static final String TAG = StoryChapter.class.getSimpleName();

    // ===========================================================
    // Static Fields
    // ===========================================================

    // ===========================================================
    // Fields
    // ===========================================================
    @JSONField(name = "id")
    public int id;
    @JSONField(name = "announcerName")
    public String announcerName;
    @JSONField(name = "duration")
    public int duration;
    @JSONField(name = "enabled")
    public int enabled;
    @JSONField(name = "filePath")
    public String filePath;
    @JSONField(name = "fileSize")
    public int fileSize;
    @JSONField(name = "modifyTime")
    public int modifyTime;
    @JSONField(name = "orderNum")
    public int orderNum;
    @JSONField(name = "playCount")
    public int playCount;
    @JSONField(name = "source")
    public int source;
    @JSONField(name = "storyAlbumId")
    public int storyAlbumId;
    @JSONField(name = "storyAlbumName")
    public String storyAlbumName;
    @JSONField(name = "title")
    public String title;
    @JSONField(name = "status")
    public String status;
    @JSONField(name = "coverUrl")
    public String coverUrl;
    // ===========================================================
    // Constructors
    // ===========================================================


    // ===========================================================
    // Getter or Setter
    // ===========================================================


    // ===========================================================
    // Methods for/from SuperClass/Interfaces
    // ===========================================================


    // ===========================================================
    // Methods
    // ===========================================================


    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================

}
