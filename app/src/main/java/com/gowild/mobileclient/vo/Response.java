package com.gowild.mobileclient.vo;

import com.alibaba.fastjson.annotation.JSONField;

/**
 * Created by Ji.Li on 2016/6/9.
 */
public class Response {

    @JSONField(name = "code")
    public int code;

    @JSONField(name = "desc")
    public String desc;

}
