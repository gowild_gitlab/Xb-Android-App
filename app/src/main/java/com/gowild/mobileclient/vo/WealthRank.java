/**
 * Copyright (C)  2016 深圳市狗尾草智能科技有限公司
 * GowildXB
 * WealthRank.java
 */
package com.gowild.mobileclient.vo;

import com.alibaba.fastjson.annotation.JSONField;

/**
 * @author Administrator
 * @since 2016/8/4 19:58
 * @version 1.0
 * <p><strong>财富排行/strong></p>
 */
public class WealthRank {

    @JSONField(name = "nickname")
    public String mNickname;

    @JSONField(name = "coinCnt")
    public String mCoinCnt;

    @JSONField(name = "id")
    public String mId;

    public String getId() {
        return mId;
    }

    public String getCoinCnt() {
        return mCoinCnt;
    }

    public String getNickname() {
        return mNickname;
    }
}
