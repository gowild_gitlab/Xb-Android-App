/**
 * Copyright (C)  2016 深圳市狗尾草智能科技有限公司
 * GowildXB
 * ResponseCreateClock.java
 */
package com.gowild.mobileclient.vo;

import com.alibaba.fastjson.annotation.JSONField;

/**
 * @author Administrator
 * @version 1.0
 *          <p><strong>我的财富排行</strong></p>
 * @since 2016/7/28 10:32
 */
public class ResponseWealthRank extends Response {

    @JSONField(name = "data")
    public WealthRankPageInfo mWealthRank;

    @JSONField(name = "success")
    public boolean mSuccess;

    public WealthRankPageInfo getWealthRank() {
        return mWealthRank;
    }

    public boolean isSuccess() {
        return mSuccess;
    }
}
