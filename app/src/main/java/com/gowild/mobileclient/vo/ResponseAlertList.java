/**
 * Copyright (C)  2016 深圳市狗尾草智能科技有限公司
 * GowildXB
 * ResponseAlertList.java
 */
package com.gowild.mobileclient.vo;

import com.alibaba.fastjson.annotation.JSONField;

import java.util.ArrayList;

/**
 * @author JunDongWang
 * @version 1.0
 *          <p><strong>解析提醒列表</strong></p>
 * @since 2016/7/28 10:22
 */
public class ResponseAlertList extends Response {

    // ===========================================================
    // HostConstants
    // ===========================================================

    private static final String TAG = ResponseAlertList.class.getSimpleName();

    // ===========================================================
    // Static Fields
    // ===========================================================

    // ===========================================================
    // Fields
    // ===========================================================
    @JSONField(name = "data")
    public ArrayList<Alert> mAlertList;

    // ===========================================================
    // Constructors
    // ===========================================================


    // ===========================================================
    // Getter or Setter
    // ===========================================================


    // ===========================================================
    // Methods for/from SuperClass/Interfaces
    // ===========================================================


    // ===========================================================
    // Methods
    // ===========================================================


    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================

}
