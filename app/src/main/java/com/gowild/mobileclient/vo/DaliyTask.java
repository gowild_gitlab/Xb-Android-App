/**
 * Copyright (C)  2016 深圳市狗尾草智能科技有限公司
 * GowildXB
 * DaliyTask.java
 */
package com.gowild.mobileclient.vo;

/**
 * @author shuai. wang
 * @since 2016/8/5 23:22
 * @version 1.0
 * <p><strong>Features draft description.主要功能介绍</strong></p>
 */
public class DaliyTask {

	// ===========================================================
	// HostConstants
	// ===========================================================

	private static final String TAG = DaliyTask.class.getSimpleName();
	/**
	 * id : 1
	 * coinCnt : 50
	 * count : 1
	 * experience : 10
	 * name : 完成1个闹钟进程
	 * type : clock
	 */

	public String id;
	public String coinCnt;
	public String count;
	public String experience;
	public String name;
	public String type;
	public String accountCount;


	// ===========================================================
	// Static Fields
	// ===========================================================

	// ===========================================================
	// Fields
	// ===========================================================


	// ===========================================================
	// Constructors
	// ===========================================================


	// ===========================================================
	// Getter or Setter
	// ===========================================================


	// ===========================================================
	// Methods for/from SuperClass/Interfaces
	// ===========================================================


	// ===========================================================
	// Methods
	// ===========================================================


	// ===========================================================
	// Inner and Anonymous Classes
	// ===========================================================

}
