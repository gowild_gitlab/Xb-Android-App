// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: Account.proto

package com.gowild.mobileclient.protocol;

public final class RobotResultPro {
  private RobotResultPro() {}
  public static void registerAllExtensions(
      com.google.protobuf.ExtensionRegistry registry) {
  }
  public interface BoolMsgOrBuilder extends
      // @@protoc_insertion_point(interface_extends:com.gowild.mobileclient.BoolMsg)
      com.google.protobuf.MessageOrBuilder {

    /**
     * <code>required bool value = 1;</code>
     *
     * <pre>
     * true:连接成功，false:未连接
     * </pre>
     */
    boolean hasValue();
    /**
     * <code>required bool value = 1;</code>
     *
     * <pre>
     * true:连接成功，false:未连接
     * </pre>
     */
    boolean getValue();
  }
  /**
   * Protobuf type {@code com.gowild.mobileclient.BoolMsg}
   */
  public static final class BoolMsg extends
      com.google.protobuf.GeneratedMessage implements
      // @@protoc_insertion_point(message_implements:com.gowild.mobileclient.BoolMsg)
      BoolMsgOrBuilder {
    // Use BoolMsg.newBuilder() to construct.
    private BoolMsg(com.google.protobuf.GeneratedMessage.Builder<?> builder) {
      super(builder);
      this.unknownFields = builder.getUnknownFields();
    }
    private BoolMsg(boolean noInit) { this.unknownFields = com.google.protobuf.UnknownFieldSet.getDefaultInstance(); }

    private static final BoolMsg defaultInstance;
    public static BoolMsg getDefaultInstance() {
      return defaultInstance;
    }

    public BoolMsg getDefaultInstanceForType() {
      return defaultInstance;
    }

    private final com.google.protobuf.UnknownFieldSet unknownFields;
    @Override
    public final com.google.protobuf.UnknownFieldSet
        getUnknownFields() {
      return this.unknownFields;
    }
    private BoolMsg(
        com.google.protobuf.CodedInputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws com.google.protobuf.InvalidProtocolBufferException {
      initFields();
      int mutable_bitField0_ = 0;
      com.google.protobuf.UnknownFieldSet.Builder unknownFields =
          com.google.protobuf.UnknownFieldSet.newBuilder();
      try {
        boolean done = false;
        while (!done) {
          int tag = input.readTag();
          switch (tag) {
            case 0:
              done = true;
              break;
            default: {
              if (!parseUnknownField(input, unknownFields,
                                     extensionRegistry, tag)) {
                done = true;
              }
              break;
            }
            case 8: {
              bitField0_ |= 0x00000001;
              value_ = input.readBool();
              break;
            }
          }
        }
      } catch (com.google.protobuf.InvalidProtocolBufferException e) {
        throw e.setUnfinishedMessage(this);
      } catch (java.io.IOException e) {
        throw new com.google.protobuf.InvalidProtocolBufferException(
            e.getMessage()).setUnfinishedMessage(this);
      } finally {
        this.unknownFields = unknownFields.build();
        makeExtensionsImmutable();
      }
    }
    public static final com.google.protobuf.Descriptors.Descriptor
        getDescriptor() {
      return RobotResultPro.internal_static_com_gowild_mobileclient_BoolMsg_descriptor;
    }

    protected FieldAccessorTable
        internalGetFieldAccessorTable() {
      return RobotResultPro.internal_static_com_gowild_mobileclient_BoolMsg_fieldAccessorTable
          .ensureFieldAccessorsInitialized(
              BoolMsg.class, Builder.class);
    }

    public static com.google.protobuf.Parser<BoolMsg> PARSER =
        new com.google.protobuf.AbstractParser<BoolMsg>() {
      public BoolMsg parsePartialFrom(
          com.google.protobuf.CodedInputStream input,
          com.google.protobuf.ExtensionRegistryLite extensionRegistry)
          throws com.google.protobuf.InvalidProtocolBufferException {
        return new BoolMsg(input, extensionRegistry);
      }
    };

    @Override
    public com.google.protobuf.Parser<BoolMsg> getParserForType() {
      return PARSER;
    }

    private int bitField0_;
    public static final int VALUE_FIELD_NUMBER = 1;
    private boolean value_;
    /**
     * <code>required bool value = 1;</code>
     *
     * <pre>
     * true:连接成功，false:未连接
     * </pre>
     */
    public boolean hasValue() {
      return ((bitField0_ & 0x00000001) == 0x00000001);
    }
    /**
     * <code>required bool value = 1;</code>
     *
     * <pre>
     * true:连接成功，false:未连接
     * </pre>
     */
    public boolean getValue() {
      return value_;
    }

    private void initFields() {
      value_ = false;
    }
    private byte memoizedIsInitialized = -1;
    public final boolean isInitialized() {
      byte isInitialized = memoizedIsInitialized;
      if (isInitialized == 1) return true;
      if (isInitialized == 0) return false;

      if (!hasValue()) {
        memoizedIsInitialized = 0;
        return false;
      }
      memoizedIsInitialized = 1;
      return true;
    }

    public void writeTo(com.google.protobuf.CodedOutputStream output)
                        throws java.io.IOException {
      getSerializedSize();
      if (((bitField0_ & 0x00000001) == 0x00000001)) {
        output.writeBool(1, value_);
      }
      getUnknownFields().writeTo(output);
    }

    private int memoizedSerializedSize = -1;
    public int getSerializedSize() {
      int size = memoizedSerializedSize;
      if (size != -1) return size;

      size = 0;
      if (((bitField0_ & 0x00000001) == 0x00000001)) {
        size += com.google.protobuf.CodedOutputStream
          .computeBoolSize(1, value_);
      }
      size += getUnknownFields().getSerializedSize();
      memoizedSerializedSize = size;
      return size;
    }

    private static final long serialVersionUID = 0L;
    @Override
    protected Object writeReplace()
        throws java.io.ObjectStreamException {
      return super.writeReplace();
    }

    public static BoolMsg parseFrom(
        com.google.protobuf.ByteString data)
        throws com.google.protobuf.InvalidProtocolBufferException {
      return PARSER.parseFrom(data);
    }
    public static BoolMsg parseFrom(
        com.google.protobuf.ByteString data,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws com.google.protobuf.InvalidProtocolBufferException {
      return PARSER.parseFrom(data, extensionRegistry);
    }
    public static BoolMsg parseFrom(byte[] data)
        throws com.google.protobuf.InvalidProtocolBufferException {
      return PARSER.parseFrom(data);
    }
    public static BoolMsg parseFrom(
        byte[] data,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws com.google.protobuf.InvalidProtocolBufferException {
      return PARSER.parseFrom(data, extensionRegistry);
    }
    public static BoolMsg parseFrom(java.io.InputStream input)
        throws java.io.IOException {
      return PARSER.parseFrom(input);
    }
    public static BoolMsg parseFrom(
        java.io.InputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws java.io.IOException {
      return PARSER.parseFrom(input, extensionRegistry);
    }
    public static BoolMsg parseDelimitedFrom(java.io.InputStream input)
        throws java.io.IOException {
      return PARSER.parseDelimitedFrom(input);
    }
    public static BoolMsg parseDelimitedFrom(
        java.io.InputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws java.io.IOException {
      return PARSER.parseDelimitedFrom(input, extensionRegistry);
    }
    public static BoolMsg parseFrom(
        com.google.protobuf.CodedInputStream input)
        throws java.io.IOException {
      return PARSER.parseFrom(input);
    }
    public static BoolMsg parseFrom(
        com.google.protobuf.CodedInputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws java.io.IOException {
      return PARSER.parseFrom(input, extensionRegistry);
    }

    public static Builder newBuilder() { return Builder.create(); }
    public Builder newBuilderForType() { return newBuilder(); }
    public static Builder newBuilder(BoolMsg prototype) {
      return newBuilder().mergeFrom(prototype);
    }
    public Builder toBuilder() { return newBuilder(this); }

    @Override
    protected Builder newBuilderForType(
        BuilderParent parent) {
      Builder builder = new Builder(parent);
      return builder;
    }
    /**
     * Protobuf type {@code com.gowild.mobileclient.BoolMsg}
     */
    public static final class Builder extends
        com.google.protobuf.GeneratedMessage.Builder<Builder> implements
        // @@protoc_insertion_point(builder_implements:com.gowild.mobileclient.BoolMsg)
        BoolMsgOrBuilder {
      public static final com.google.protobuf.Descriptors.Descriptor
          getDescriptor() {
        return RobotResultPro.internal_static_com_gowild_mobileclient_BoolMsg_descriptor;
      }

      protected FieldAccessorTable
          internalGetFieldAccessorTable() {
        return RobotResultPro.internal_static_com_gowild_mobileclient_BoolMsg_fieldAccessorTable
            .ensureFieldAccessorsInitialized(
                BoolMsg.class, Builder.class);
      }

      // Construct using com.gowild.mobileclient.protocol.RobotResultPro.BoolMsg.newBuilder()
      private Builder() {
        maybeForceBuilderInitialization();
      }

      private Builder(
          BuilderParent parent) {
        super(parent);
        maybeForceBuilderInitialization();
      }
      private void maybeForceBuilderInitialization() {
        if (com.google.protobuf.GeneratedMessage.alwaysUseFieldBuilders) {
        }
      }
      private static Builder create() {
        return new Builder();
      }

      public Builder clear() {
        super.clear();
        value_ = false;
        bitField0_ = (bitField0_ & ~0x00000001);
        return this;
      }

      public Builder clone() {
        return create().mergeFrom(buildPartial());
      }

      public com.google.protobuf.Descriptors.Descriptor
          getDescriptorForType() {
        return RobotResultPro.internal_static_com_gowild_mobileclient_BoolMsg_descriptor;
      }

      public BoolMsg getDefaultInstanceForType() {
        return BoolMsg.getDefaultInstance();
      }

      public BoolMsg build() {
        BoolMsg result = buildPartial();
        if (!result.isInitialized()) {
          throw newUninitializedMessageException(result);
        }
        return result;
      }

      public BoolMsg buildPartial() {
        BoolMsg result = new BoolMsg(this);
        int from_bitField0_ = bitField0_;
        int to_bitField0_ = 0;
        if (((from_bitField0_ & 0x00000001) == 0x00000001)) {
          to_bitField0_ |= 0x00000001;
        }
        result.value_ = value_;
        result.bitField0_ = to_bitField0_;
        onBuilt();
        return result;
      }

      public Builder mergeFrom(com.google.protobuf.Message other) {
        if (other instanceof BoolMsg) {
          return mergeFrom((BoolMsg)other);
        } else {
          super.mergeFrom(other);
          return this;
        }
      }

      public Builder mergeFrom(BoolMsg other) {
        if (other == BoolMsg.getDefaultInstance()) return this;
        if (other.hasValue()) {
          setValue(other.getValue());
        }
        this.mergeUnknownFields(other.getUnknownFields());
        return this;
      }

      public final boolean isInitialized() {
        if (!hasValue()) {
          
          return false;
        }
        return true;
      }

      public Builder mergeFrom(
          com.google.protobuf.CodedInputStream input,
          com.google.protobuf.ExtensionRegistryLite extensionRegistry)
          throws java.io.IOException {
        BoolMsg parsedMessage = null;
        try {
          parsedMessage = PARSER.parsePartialFrom(input, extensionRegistry);
        } catch (com.google.protobuf.InvalidProtocolBufferException e) {
          parsedMessage = (BoolMsg) e.getUnfinishedMessage();
          throw e;
        } finally {
          if (parsedMessage != null) {
            mergeFrom(parsedMessage);
          }
        }
        return this;
      }
      private int bitField0_;

      private boolean value_ ;
      /**
       * <code>required bool value = 1;</code>
       *
       * <pre>
       * true:连接成功，false:未连接
       * </pre>
       */
      public boolean hasValue() {
        return ((bitField0_ & 0x00000001) == 0x00000001);
      }
      /**
       * <code>required bool value = 1;</code>
       *
       * <pre>
       * true:连接成功，false:未连接
       * </pre>
       */
      public boolean getValue() {
        return value_;
      }
      /**
       * <code>required bool value = 1;</code>
       *
       * <pre>
       * true:连接成功，false:未连接
       * </pre>
       */
      public Builder setValue(boolean value) {
        bitField0_ |= 0x00000001;
        value_ = value;
        onChanged();
        return this;
      }
      /**
       * <code>required bool value = 1;</code>
       *
       * <pre>
       * true:连接成功，false:未连接
       * </pre>
       */
      public Builder clearValue() {
        bitField0_ = (bitField0_ & ~0x00000001);
        value_ = false;
        onChanged();
        return this;
      }

      // @@protoc_insertion_point(builder_scope:com.gowild.mobileclient.BoolMsg)
    }

    static {
      defaultInstance = new BoolMsg(true);
      defaultInstance.initFields();
    }

    // @@protoc_insertion_point(class_scope:com.gowild.mobileclient.BoolMsg)
  }

  private static final com.google.protobuf.Descriptors.Descriptor
    internal_static_com_gowild_mobileclient_BoolMsg_descriptor;
  private static
    com.google.protobuf.GeneratedMessage.FieldAccessorTable
      internal_static_com_gowild_mobileclient_BoolMsg_fieldAccessorTable;

  public static com.google.protobuf.Descriptors.FileDescriptor
      getDescriptor() {
    return descriptor;
  }
  private static com.google.protobuf.Descriptors.FileDescriptor
      descriptor;
  static {
    String[] descriptorData = {
      "\n\rAccount.proto\022\027com.gowild.mobileclient" +
      "\"\030\n\007BoolMsg\022\r\n\005value\030\001 \002(\010B2\n com.gowild" +
      ".mobileclient.protocolB\016RobotResultPro"
    };
    com.google.protobuf.Descriptors.FileDescriptor.InternalDescriptorAssigner assigner =
        new com.google.protobuf.Descriptors.FileDescriptor.    InternalDescriptorAssigner() {
          public com.google.protobuf.ExtensionRegistry assignDescriptors(
              com.google.protobuf.Descriptors.FileDescriptor root) {
            descriptor = root;
            return null;
          }
        };
    com.google.protobuf.Descriptors.FileDescriptor
      .internalBuildGeneratedFileFrom(descriptorData,
        new com.google.protobuf.Descriptors.FileDescriptor[] {
        }, assigner);
    internal_static_com_gowild_mobileclient_BoolMsg_descriptor =
      getDescriptor().getMessageTypes().get(0);
    internal_static_com_gowild_mobileclient_BoolMsg_fieldAccessorTable = new
      com.google.protobuf.GeneratedMessage.FieldAccessorTable(
        internal_static_com_gowild_mobileclient_BoolMsg_descriptor,
        new String[] { "Value", });
  }

  // @@protoc_insertion_point(outer_class_scope)
}
