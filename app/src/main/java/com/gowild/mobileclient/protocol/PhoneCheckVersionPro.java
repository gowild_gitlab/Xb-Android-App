// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: PhoneCheckVersion.proto

package com.gowild.mobileclient.protocol;

public final class PhoneCheckVersionPro {
  private PhoneCheckVersionPro() {}
  public static void registerAllExtensions(
      com.google.protobuf.ExtensionRegistry registry) {
  }
  public interface PhoneCheckVersionMsgOrBuilder extends
      // @@protoc_insertion_point(interface_extends:com.gowild.mobileclient.PhoneCheckVersionMsg)
      com.google.protobuf.MessageOrBuilder {

    /**
     * <code>optional int32 appType = 1;</code>
     *
     * <pre>
     * app类型(1:android, 2:ios)
     * </pre>
     */
    boolean hasAppType();
    /**
     * <code>optional int32 appType = 1;</code>
     *
     * <pre>
     * app类型(1:android, 2:ios)
     * </pre>
     */
    int getAppType();

    /**
     * <code>optional string currentVersion = 2;</code>
     *
     * <pre>
     * 用户当前版本
     * </pre>
     */
    boolean hasCurrentVersion();
    /**
     * <code>optional string currentVersion = 2;</code>
     *
     * <pre>
     * 用户当前版本
     * </pre>
     */
    String getCurrentVersion();
    /**
     * <code>optional string currentVersion = 2;</code>
     *
     * <pre>
     * 用户当前版本
     * </pre>
     */
    com.google.protobuf.ByteString
        getCurrentVersionBytes();
  }
  /**
   * Protobuf type {@code com.gowild.mobileclient.PhoneCheckVersionMsg}
   *
   * <pre>
   * 检查版本
   * </pre>
   */
  public static final class PhoneCheckVersionMsg extends
      com.google.protobuf.GeneratedMessage implements
      // @@protoc_insertion_point(message_implements:com.gowild.mobileclient.PhoneCheckVersionMsg)
      PhoneCheckVersionMsgOrBuilder {
    // Use PhoneCheckVersionMsg.newBuilder() to construct.
    private PhoneCheckVersionMsg(com.google.protobuf.GeneratedMessage.Builder<?> builder) {
      super(builder);
      this.unknownFields = builder.getUnknownFields();
    }
    private PhoneCheckVersionMsg(boolean noInit) { this.unknownFields = com.google.protobuf.UnknownFieldSet.getDefaultInstance(); }

    private static final PhoneCheckVersionMsg defaultInstance;
    public static PhoneCheckVersionMsg getDefaultInstance() {
      return defaultInstance;
    }

    public PhoneCheckVersionMsg getDefaultInstanceForType() {
      return defaultInstance;
    }

    private final com.google.protobuf.UnknownFieldSet unknownFields;
    @Override
    public final com.google.protobuf.UnknownFieldSet
        getUnknownFields() {
      return this.unknownFields;
    }
    private PhoneCheckVersionMsg(
        com.google.protobuf.CodedInputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws com.google.protobuf.InvalidProtocolBufferException {
      initFields();
      int mutable_bitField0_ = 0;
      com.google.protobuf.UnknownFieldSet.Builder unknownFields =
          com.google.protobuf.UnknownFieldSet.newBuilder();
      try {
        boolean done = false;
        while (!done) {
          int tag = input.readTag();
          switch (tag) {
            case 0:
              done = true;
              break;
            default: {
              if (!parseUnknownField(input, unknownFields,
                                     extensionRegistry, tag)) {
                done = true;
              }
              break;
            }
            case 8: {
              bitField0_ |= 0x00000001;
              appType_ = input.readInt32();
              break;
            }
            case 18: {
              com.google.protobuf.ByteString bs = input.readBytes();
              bitField0_ |= 0x00000002;
              currentVersion_ = bs;
              break;
            }
          }
        }
      } catch (com.google.protobuf.InvalidProtocolBufferException e) {
        throw e.setUnfinishedMessage(this);
      } catch (java.io.IOException e) {
        throw new com.google.protobuf.InvalidProtocolBufferException(
            e.getMessage()).setUnfinishedMessage(this);
      } finally {
        this.unknownFields = unknownFields.build();
        makeExtensionsImmutable();
      }
    }
    public static final com.google.protobuf.Descriptors.Descriptor
        getDescriptor() {
      return PhoneCheckVersionPro.internal_static_com_gowild_mobileclient_PhoneCheckVersionMsg_descriptor;
    }

    protected FieldAccessorTable
        internalGetFieldAccessorTable() {
      return PhoneCheckVersionPro.internal_static_com_gowild_mobileclient_PhoneCheckVersionMsg_fieldAccessorTable
          .ensureFieldAccessorsInitialized(
              PhoneCheckVersionMsg.class, Builder.class);
    }

    public static com.google.protobuf.Parser<PhoneCheckVersionMsg> PARSER =
        new com.google.protobuf.AbstractParser<PhoneCheckVersionMsg>() {
      public PhoneCheckVersionMsg parsePartialFrom(
          com.google.protobuf.CodedInputStream input,
          com.google.protobuf.ExtensionRegistryLite extensionRegistry)
          throws com.google.protobuf.InvalidProtocolBufferException {
        return new PhoneCheckVersionMsg(input, extensionRegistry);
      }
    };

    @Override
    public com.google.protobuf.Parser<PhoneCheckVersionMsg> getParserForType() {
      return PARSER;
    }

    private int bitField0_;
    public static final int APPTYPE_FIELD_NUMBER = 1;
    private int appType_;
    /**
     * <code>optional int32 appType = 1;</code>
     *
     * <pre>
     * app类型(1:android, 2:ios)
     * </pre>
     */
    public boolean hasAppType() {
      return ((bitField0_ & 0x00000001) == 0x00000001);
    }
    /**
     * <code>optional int32 appType = 1;</code>
     *
     * <pre>
     * app类型(1:android, 2:ios)
     * </pre>
     */
    public int getAppType() {
      return appType_;
    }

    public static final int CURRENTVERSION_FIELD_NUMBER = 2;
    private Object currentVersion_;
    /**
     * <code>optional string currentVersion = 2;</code>
     *
     * <pre>
     * 用户当前版本
     * </pre>
     */
    public boolean hasCurrentVersion() {
      return ((bitField0_ & 0x00000002) == 0x00000002);
    }
    /**
     * <code>optional string currentVersion = 2;</code>
     *
     * <pre>
     * 用户当前版本
     * </pre>
     */
    public String getCurrentVersion() {
      Object ref = currentVersion_;
      if (ref instanceof String) {
        return (String) ref;
      } else {
        com.google.protobuf.ByteString bs = 
            (com.google.protobuf.ByteString) ref;
        String s = bs.toStringUtf8();
        if (bs.isValidUtf8()) {
          currentVersion_ = s;
        }
        return s;
      }
    }
    /**
     * <code>optional string currentVersion = 2;</code>
     *
     * <pre>
     * 用户当前版本
     * </pre>
     */
    public com.google.protobuf.ByteString
        getCurrentVersionBytes() {
      Object ref = currentVersion_;
      if (ref instanceof String) {
        com.google.protobuf.ByteString b = 
            com.google.protobuf.ByteString.copyFromUtf8(
                (String) ref);
        currentVersion_ = b;
        return b;
      } else {
        return (com.google.protobuf.ByteString) ref;
      }
    }

    private void initFields() {
      appType_ = 0;
      currentVersion_ = "";
    }
    private byte memoizedIsInitialized = -1;
    public final boolean isInitialized() {
      byte isInitialized = memoizedIsInitialized;
      if (isInitialized == 1) return true;
      if (isInitialized == 0) return false;

      memoizedIsInitialized = 1;
      return true;
    }

    public void writeTo(com.google.protobuf.CodedOutputStream output)
                        throws java.io.IOException {
      getSerializedSize();
      if (((bitField0_ & 0x00000001) == 0x00000001)) {
        output.writeInt32(1, appType_);
      }
      if (((bitField0_ & 0x00000002) == 0x00000002)) {
        output.writeBytes(2, getCurrentVersionBytes());
      }
      getUnknownFields().writeTo(output);
    }

    private int memoizedSerializedSize = -1;
    public int getSerializedSize() {
      int size = memoizedSerializedSize;
      if (size != -1) return size;

      size = 0;
      if (((bitField0_ & 0x00000001) == 0x00000001)) {
        size += com.google.protobuf.CodedOutputStream
          .computeInt32Size(1, appType_);
      }
      if (((bitField0_ & 0x00000002) == 0x00000002)) {
        size += com.google.protobuf.CodedOutputStream
          .computeBytesSize(2, getCurrentVersionBytes());
      }
      size += getUnknownFields().getSerializedSize();
      memoizedSerializedSize = size;
      return size;
    }

    private static final long serialVersionUID = 0L;
    @Override
    protected Object writeReplace()
        throws java.io.ObjectStreamException {
      return super.writeReplace();
    }

    public static PhoneCheckVersionMsg parseFrom(
        com.google.protobuf.ByteString data)
        throws com.google.protobuf.InvalidProtocolBufferException {
      return PARSER.parseFrom(data);
    }
    public static PhoneCheckVersionMsg parseFrom(
        com.google.protobuf.ByteString data,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws com.google.protobuf.InvalidProtocolBufferException {
      return PARSER.parseFrom(data, extensionRegistry);
    }
    public static PhoneCheckVersionMsg parseFrom(byte[] data)
        throws com.google.protobuf.InvalidProtocolBufferException {
      return PARSER.parseFrom(data);
    }
    public static PhoneCheckVersionMsg parseFrom(
        byte[] data,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws com.google.protobuf.InvalidProtocolBufferException {
      return PARSER.parseFrom(data, extensionRegistry);
    }
    public static PhoneCheckVersionMsg parseFrom(java.io.InputStream input)
        throws java.io.IOException {
      return PARSER.parseFrom(input);
    }
    public static PhoneCheckVersionMsg parseFrom(
        java.io.InputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws java.io.IOException {
      return PARSER.parseFrom(input, extensionRegistry);
    }
    public static PhoneCheckVersionMsg parseDelimitedFrom(java.io.InputStream input)
        throws java.io.IOException {
      return PARSER.parseDelimitedFrom(input);
    }
    public static PhoneCheckVersionMsg parseDelimitedFrom(
        java.io.InputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws java.io.IOException {
      return PARSER.parseDelimitedFrom(input, extensionRegistry);
    }
    public static PhoneCheckVersionMsg parseFrom(
        com.google.protobuf.CodedInputStream input)
        throws java.io.IOException {
      return PARSER.parseFrom(input);
    }
    public static PhoneCheckVersionMsg parseFrom(
        com.google.protobuf.CodedInputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws java.io.IOException {
      return PARSER.parseFrom(input, extensionRegistry);
    }

    public static Builder newBuilder() { return Builder.create(); }
    public Builder newBuilderForType() { return newBuilder(); }
    public static Builder newBuilder(PhoneCheckVersionMsg prototype) {
      return newBuilder().mergeFrom(prototype);
    }
    public Builder toBuilder() { return newBuilder(this); }

    @Override
    protected Builder newBuilderForType(
        BuilderParent parent) {
      Builder builder = new Builder(parent);
      return builder;
    }
    /**
     * Protobuf type {@code com.gowild.mobileclient.PhoneCheckVersionMsg}
     *
     * <pre>
     * 检查版本
     * </pre>
     */
    public static final class Builder extends
        com.google.protobuf.GeneratedMessage.Builder<Builder> implements
        // @@protoc_insertion_point(builder_implements:com.gowild.mobileclient.PhoneCheckVersionMsg)
        PhoneCheckVersionMsgOrBuilder {
      public static final com.google.protobuf.Descriptors.Descriptor
          getDescriptor() {
        return PhoneCheckVersionPro.internal_static_com_gowild_mobileclient_PhoneCheckVersionMsg_descriptor;
      }

      protected FieldAccessorTable
          internalGetFieldAccessorTable() {
        return PhoneCheckVersionPro.internal_static_com_gowild_mobileclient_PhoneCheckVersionMsg_fieldAccessorTable
            .ensureFieldAccessorsInitialized(
                PhoneCheckVersionMsg.class, Builder.class);
      }

      // Construct using com.gowild.mobileclient.protocol.PhoneCheckVersionPro.PhoneCheckVersionMsg.newBuilder()
      private Builder() {
        maybeForceBuilderInitialization();
      }

      private Builder(
          BuilderParent parent) {
        super(parent);
        maybeForceBuilderInitialization();
      }
      private void maybeForceBuilderInitialization() {
        if (com.google.protobuf.GeneratedMessage.alwaysUseFieldBuilders) {
        }
      }
      private static Builder create() {
        return new Builder();
      }

      public Builder clear() {
        super.clear();
        appType_ = 0;
        bitField0_ = (bitField0_ & ~0x00000001);
        currentVersion_ = "";
        bitField0_ = (bitField0_ & ~0x00000002);
        return this;
      }

      public Builder clone() {
        return create().mergeFrom(buildPartial());
      }

      public com.google.protobuf.Descriptors.Descriptor
          getDescriptorForType() {
        return PhoneCheckVersionPro.internal_static_com_gowild_mobileclient_PhoneCheckVersionMsg_descriptor;
      }

      public PhoneCheckVersionMsg getDefaultInstanceForType() {
        return PhoneCheckVersionMsg.getDefaultInstance();
      }

      public PhoneCheckVersionMsg build() {
        PhoneCheckVersionMsg result = buildPartial();
        if (!result.isInitialized()) {
          throw newUninitializedMessageException(result);
        }
        return result;
      }

      public PhoneCheckVersionMsg buildPartial() {
        PhoneCheckVersionMsg result = new PhoneCheckVersionMsg(this);
        int from_bitField0_ = bitField0_;
        int to_bitField0_ = 0;
        if (((from_bitField0_ & 0x00000001) == 0x00000001)) {
          to_bitField0_ |= 0x00000001;
        }
        result.appType_ = appType_;
        if (((from_bitField0_ & 0x00000002) == 0x00000002)) {
          to_bitField0_ |= 0x00000002;
        }
        result.currentVersion_ = currentVersion_;
        result.bitField0_ = to_bitField0_;
        onBuilt();
        return result;
      }

      public Builder mergeFrom(com.google.protobuf.Message other) {
        if (other instanceof PhoneCheckVersionMsg) {
          return mergeFrom((PhoneCheckVersionMsg)other);
        } else {
          super.mergeFrom(other);
          return this;
        }
      }

      public Builder mergeFrom(PhoneCheckVersionMsg other) {
        if (other == PhoneCheckVersionMsg.getDefaultInstance()) return this;
        if (other.hasAppType()) {
          setAppType(other.getAppType());
        }
        if (other.hasCurrentVersion()) {
          bitField0_ |= 0x00000002;
          currentVersion_ = other.currentVersion_;
          onChanged();
        }
        this.mergeUnknownFields(other.getUnknownFields());
        return this;
      }

      public final boolean isInitialized() {
        return true;
      }

      public Builder mergeFrom(
          com.google.protobuf.CodedInputStream input,
          com.google.protobuf.ExtensionRegistryLite extensionRegistry)
          throws java.io.IOException {
        PhoneCheckVersionMsg parsedMessage = null;
        try {
          parsedMessage = PARSER.parsePartialFrom(input, extensionRegistry);
        } catch (com.google.protobuf.InvalidProtocolBufferException e) {
          parsedMessage = (PhoneCheckVersionMsg) e.getUnfinishedMessage();
          throw e;
        } finally {
          if (parsedMessage != null) {
            mergeFrom(parsedMessage);
          }
        }
        return this;
      }
      private int bitField0_;

      private int appType_ ;
      /**
       * <code>optional int32 appType = 1;</code>
       *
       * <pre>
       * app类型(1:android, 2:ios)
       * </pre>
       */
      public boolean hasAppType() {
        return ((bitField0_ & 0x00000001) == 0x00000001);
      }
      /**
       * <code>optional int32 appType = 1;</code>
       *
       * <pre>
       * app类型(1:android, 2:ios)
       * </pre>
       */
      public int getAppType() {
        return appType_;
      }
      /**
       * <code>optional int32 appType = 1;</code>
       *
       * <pre>
       * app类型(1:android, 2:ios)
       * </pre>
       */
      public Builder setAppType(int value) {
        bitField0_ |= 0x00000001;
        appType_ = value;
        onChanged();
        return this;
      }
      /**
       * <code>optional int32 appType = 1;</code>
       *
       * <pre>
       * app类型(1:android, 2:ios)
       * </pre>
       */
      public Builder clearAppType() {
        bitField0_ = (bitField0_ & ~0x00000001);
        appType_ = 0;
        onChanged();
        return this;
      }

      private Object currentVersion_ = "";
      /**
       * <code>optional string currentVersion = 2;</code>
       *
       * <pre>
       * 用户当前版本
       * </pre>
       */
      public boolean hasCurrentVersion() {
        return ((bitField0_ & 0x00000002) == 0x00000002);
      }
      /**
       * <code>optional string currentVersion = 2;</code>
       *
       * <pre>
       * 用户当前版本
       * </pre>
       */
      public String getCurrentVersion() {
        Object ref = currentVersion_;
        if (!(ref instanceof String)) {
          com.google.protobuf.ByteString bs =
              (com.google.protobuf.ByteString) ref;
          String s = bs.toStringUtf8();
          if (bs.isValidUtf8()) {
            currentVersion_ = s;
          }
          return s;
        } else {
          return (String) ref;
        }
      }
      /**
       * <code>optional string currentVersion = 2;</code>
       *
       * <pre>
       * 用户当前版本
       * </pre>
       */
      public com.google.protobuf.ByteString
          getCurrentVersionBytes() {
        Object ref = currentVersion_;
        if (ref instanceof String) {
          com.google.protobuf.ByteString b = 
              com.google.protobuf.ByteString.copyFromUtf8(
                  (String) ref);
          currentVersion_ = b;
          return b;
        } else {
          return (com.google.protobuf.ByteString) ref;
        }
      }
      /**
       * <code>optional string currentVersion = 2;</code>
       *
       * <pre>
       * 用户当前版本
       * </pre>
       */
      public Builder setCurrentVersion(
          String value) {
        if (value == null) {
    throw new NullPointerException();
  }
  bitField0_ |= 0x00000002;
        currentVersion_ = value;
        onChanged();
        return this;
      }
      /**
       * <code>optional string currentVersion = 2;</code>
       *
       * <pre>
       * 用户当前版本
       * </pre>
       */
      public Builder clearCurrentVersion() {
        bitField0_ = (bitField0_ & ~0x00000002);
        currentVersion_ = getDefaultInstance().getCurrentVersion();
        onChanged();
        return this;
      }
      /**
       * <code>optional string currentVersion = 2;</code>
       *
       * <pre>
       * 用户当前版本
       * </pre>
       */
      public Builder setCurrentVersionBytes(
          com.google.protobuf.ByteString value) {
        if (value == null) {
    throw new NullPointerException();
  }
  bitField0_ |= 0x00000002;
        currentVersion_ = value;
        onChanged();
        return this;
      }

      // @@protoc_insertion_point(builder_scope:com.gowild.mobileclient.PhoneCheckVersionMsg)
    }

    static {
      defaultInstance = new PhoneCheckVersionMsg(true);
      defaultInstance.initFields();
    }

    // @@protoc_insertion_point(class_scope:com.gowild.mobileclient.PhoneCheckVersionMsg)
  }

  private static final com.google.protobuf.Descriptors.Descriptor
    internal_static_com_gowild_mobileclient_PhoneCheckVersionMsg_descriptor;
  private static
    com.google.protobuf.GeneratedMessage.FieldAccessorTable
      internal_static_com_gowild_mobileclient_PhoneCheckVersionMsg_fieldAccessorTable;

  public static com.google.protobuf.Descriptors.FileDescriptor
      getDescriptor() {
    return descriptor;
  }
  private static com.google.protobuf.Descriptors.FileDescriptor
      descriptor;
  static {
    String[] descriptorData = {
      "\n\027PhoneCheckVersion.proto\022\027com.gowild.mo" +
      "bileclient\"?\n\024PhoneCheckVersionMsg\022\017\n\007ap" +
      "pType\030\001 \001(\005\022\026\n\016currentVersion\030\002 \001(\tB8\n c" +
      "om.gowild.mobileclient.protocolB\024PhoneCh" +
      "eckVersionPro"
    };
    com.google.protobuf.Descriptors.FileDescriptor.InternalDescriptorAssigner assigner =
        new com.google.protobuf.Descriptors.FileDescriptor.    InternalDescriptorAssigner() {
          public com.google.protobuf.ExtensionRegistry assignDescriptors(
              com.google.protobuf.Descriptors.FileDescriptor root) {
            descriptor = root;
            return null;
          }
        };
    com.google.protobuf.Descriptors.FileDescriptor
      .internalBuildGeneratedFileFrom(descriptorData,
        new com.google.protobuf.Descriptors.FileDescriptor[] {
        }, assigner);
    internal_static_com_gowild_mobileclient_PhoneCheckVersionMsg_descriptor =
      getDescriptor().getMessageTypes().get(0);
    internal_static_com_gowild_mobileclient_PhoneCheckVersionMsg_fieldAccessorTable = new
      com.google.protobuf.GeneratedMessage.FieldAccessorTable(
        internal_static_com_gowild_mobileclient_PhoneCheckVersionMsg_descriptor,
        new String[] { "AppType", "CurrentVersion", });
  }

  // @@protoc_insertion_point(outer_class_scope)
}
