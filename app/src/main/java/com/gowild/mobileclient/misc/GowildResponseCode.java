/**
 * Copyright (C)  2016 深圳市狗尾草智能科技有限公司
 * GowildXB
 * GowildResponseCode.java
 */
package com.gowild.mobileclient.misc;

/**
 * @author Ji.Li
 * @version 1.0
 *          <p><strong>Features draft description.主要功能介绍</strong></p>
 * @since 2016/8/11 16:42
 */
public class GowildResponseCode {

    // ===========================================================
    // HostConstants
    // ===========================================================

    private static final String TAG = GowildResponseCode.class.getSimpleName();

    // ===========================================================
    // Static Fields
    // ===========================================================

    public static final short LOGIN_RESULT = 102;
    public static final short HRART_SEND = 103;
    public static final short LOGIN_OUT = 104;
    public static final short HRART_RECEVICE = 106;
    public static final short NETTY_CONNECT = 1000;
    //主界面会主动同步音乐故事信息
    public static final short ROBOT_INFO_SYNC = 202;
    public static final short ROBOT_RESULT = 206;
    public static final short ROBOT_BIND_RESULT = 208;
    public static final short STORY_CHANGE = 704;
    public static final short APPLIANCE_ADD_RESULT = 1402;
    public static final short APPLIANCE_DEL_RESULT = 1404;
    public static final short APPLIANCE_SYN_RESULT = 1406;
    public static final short APPLIANCE_CONTROL_RESULT = 1408;
    public static final short MUSIC_CHANGE = 804;
    public static final short START_AV_MONITOR_RESULT = 1102;
    public static final short QUIT_AV_MONITOR_RESULT = 1106;
    public static final short GET_AUTH_RESULT = 1104;
    public static final short CLOCK_RING=302;
    public static final short CLOCK_SYNC=304;
    //提醒响铃
    public static final short ALERT_RING = 502;
    public static final short ALERT_SYNC = 504;

    public static final int CODE_START_AV_MONITOR_SUCCESS = 1;
    public static final int CODE_START_AV_MONITOR_NOT_ONLINE = -1;
    public static final int CODE_START_AV_MONITOR_BUSY = -2;

    public static final int CODE_GET_AUTH_SUCCESS = 1;
    public static final int CODE_GET_AUTH_FAIL = -1;

    public static final int CODE_QUIT_AV_MONITOR_BY_ROBOT = 2;
    public static final int CODE_QUIT_AV_MONITOR_BY_APP = 1;
    public static final int CODE_QUIT_AV_MONITOR_BY_APP_FAIL = -1;

    //APP升级
    public static final short PHONE_UPGRADEINFO_RESULT = 1602;
    public static final short MACHINE_UPGRADEINFO_RESULT = 210;


    //音量同步,播放同步
    public static final short PLAY_CHANGE = 602;
    public static final short SOUND_CHANGE = 604;
    public static final short MUSIC_UNLIKE = 706;
    public static final short STORY_UNLIKE = 806;

    // ===========================================================
    // Fields
    // ===========================================================


    // ===========================================================
    // Constructors
    // ===========================================================


    // ===========================================================
    // Getter or Setter
    // ===========================================================


    // ===========================================================
    // Methods for/from SuperClass/Interfaces
    // ===========================================================


    // ===========================================================
    // Methods
    // ===========================================================


    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================

}
