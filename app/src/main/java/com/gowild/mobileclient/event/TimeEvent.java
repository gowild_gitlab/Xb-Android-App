package com.gowild.mobileclient.event;

/**
 * Created by ssywbj on 2016/10/13.
 */
public class TimeEvent {

    public String hour;
    public String minutes;
    public int index;

    public TimeEvent(String hour, String minutes, int index) {
        this.hour = hour;
        this.minutes = minutes;
        this.index = index;
    }
}
