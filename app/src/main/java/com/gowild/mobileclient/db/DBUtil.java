/**
 * Copyright (C)  2016 深圳市狗尾草智能科技有限公司
 * GowildXB
 * DBUtil.java
 */
package com.gowild.mobileclient.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.support.annotation.Nullable;

/**
 * @author temp
 * @since 2016/8/27 15:02
 * @version 1.0
 * <p><strong>Features draft description.主要功能介绍</strong></p>
 */
public class DBUtil {

    // ===========================================================
    // HostConstants
    // ===========================================================

    private static final String TAG = DBUtil.class.getSimpleName();

    private static DBHelper mHelper;

    public boolean init(Context context) {
        mHelper = DBHelper.getInstance(context);
        return true;
    }

    @Nullable
    public static Cursor query(String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        SQLiteDatabase db = mHelper.getWritableDatabase();
        Cursor c = db.query(DBHelper.TABLE_NAME, projection, selection, selectionArgs, null, null, sortOrder);
        return c;
    }




    @Nullable
    public static Uri insert(ContentValues values) {

        SQLiteDatabase db = mHelper.getWritableDatabase();
        db.insert(DBHelper.TABLE_NAME,  DBHelper.COLUMN_ID,  values);
        db.close();
        return null;
    }

    public static int delete(String selection, String[] selectionArgs) {

//        sqLiteDatabase.delete(DEMO_DB_NAME, "uid = ? ", new String[]{uid});

        SQLiteDatabase db = mHelper.getWritableDatabase();

        db.delete(DBHelper.TABLE_NAME,  selection,  selectionArgs);
        return 0;
    }

    public static int update(ContentValues values, String selection, String[] selectionArgs) {
        SQLiteDatabase db = mHelper.getWritableDatabase();
        int id = db.update(DBHelper.TABLE_NAME, values, selection, selectionArgs);
        db.close();
        if(id!=0) {
            return id;
        }
        return 0;
    }

    public static  void clearTable() {
        mHelper.clearTable();
    }
}
