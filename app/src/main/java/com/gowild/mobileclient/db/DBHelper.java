package com.gowild.mobileclient.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;

import com.gowild.mobile.libcommon.utils.Logger;

public class DBHelper extends SQLiteOpenHelper {

	private static final String TAG = DBHelper.class.getSimpleName();
	private static String DB_NAME = "gowildxb.db"; // 数据库名称
	public static String TABLE_NAME = "photo"; // 表名

    public static final String COLUMN_ID = "id";
    public static final String COLUMN_TIMESTAMP = "timeStamp";
    public static final String COLUMN_PHOTOURL = "photoUrl";
    public static final String COLUMN_CREATETIME = "createTime";

	public static final int VERSION = 1;
    private static DBHelper mHelper;

    private DBHelper(Context context, String name, CursorFactory factory,
					int version) {
		super(context, name, factory, version);
	}

	public static DBHelper getInstance(Context context) {
		if (mHelper == null) {
            synchronized (DBHelper.class) {
                if (mHelper == null) {
                    mHelper = new DBHelper(context, DB_NAME, null, VERSION);
                }
            }
		}
		return mHelper;
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		// Create table
		String sql = "CREATE TABLE " + TABLE_NAME + "("
				+ COLUMN_ID + " INTEGER PRIMARY KEY,"
                + COLUMN_TIMESTAMP + " INTEGER,"
                + COLUMN_PHOTOURL + " TEXT,"
				+ COLUMN_CREATETIME + " TEXT);";

		Logger.e(TAG, "create table");
		db.execSQL(sql); // 创建表
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		Logger.e(TAG, "update");
		// db.execSQL("ALTER TABLE "+ MyHelper.TABLE_NAME+" ADD sex TEXT");
		// //修改字段
	}

	@Override
	public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		super.onDowngrade(db, oldVersion, newVersion);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME + ";");
		onCreate(db);
	}

    public void clearTable() {
        SQLiteDatabase db = getWritableDatabase();
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME + ";");
        onCreate(db);
    }
}
