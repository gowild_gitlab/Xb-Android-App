/**
 * Copyright (C)  2016 深圳市狗尾草智能科技有限公司
 * GowildXB
 * GowildContentProvider.java
 */
package com.gowild.mobileclient.db;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.support.annotation.Nullable;

/**
 * @author temp
 * @since 2016/8/27 12:31
 * @version 1.0
 * <p><strong>Features draft description.主要功能介绍</strong></p>
 */
public class GowildContentProvider extends ContentProvider {

    // ===========================================================
    // HostConstants
    // ===========================================================

    private static final String TAG = GowildContentProvider.class.getSimpleName();

    private DBHelper mHelper;

    @Override
    public boolean onCreate() {
        mHelper = DBHelper.getInstance(getContext());
        return true;
    }

    @Nullable
    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        SQLiteDatabase db = mHelper.getWritableDatabase();
        Cursor c=db.query(DBHelper.TABLE_NAME,projection, selection, selectionArgs, null, null, sortOrder);
        return c;
    }

    @Nullable
    @Override
    public String getType(Uri uri) {
        return null;
    }

    @Nullable
    @Override
    public Uri insert(Uri uri, ContentValues values) {
        SQLiteDatabase db = mHelper.getWritableDatabase();
        db.insert(DBHelper.TABLE_NAME,  DBHelper.COLUMN_ID,  values);
        getContext().getContentResolver().notifyChange(uri, null);
        return null;
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        SQLiteDatabase db = mHelper.getWritableDatabase();
        db.delete(DBHelper.TABLE_NAME,  selection,  selectionArgs);
        getContext().getContentResolver().notifyChange(uri, null);
        return 0;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        SQLiteDatabase  db = mHelper.getWritableDatabase();
        int id = db.update(DBHelper.TABLE_NAME, values, selection, selectionArgs);
        if(id!=0) {
            return id;
        }
        getContext().getContentResolver().notifyChange(uri, null);
        return 0;
    }
}
