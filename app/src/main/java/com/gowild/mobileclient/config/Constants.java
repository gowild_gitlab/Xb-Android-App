/**
 * Copyright (C)  2016 深圳市狗尾草智能科技有限公司
 * GowildXB
 * HostConstants.java
 */
package com.gowild.mobileclient.config;

import com.gowild.mobileclient.application.GowildApplication;

/**
 * @author shuai. wang
 * @version 1.0
 *          <p><strong>Features draft description.主要功能介绍</strong></p>
 * @since 2016/7/18 18:05
 */
public class Constants {


    // ===========================================================
    // HostConstants
    // ===========================================================




    public static final int PORT = 6030;

    public static final String KEY_REGISTER       = "REGISTER";
    public static final String ACCESS_TOKEN       = "access_token";
    public static final String FORGOT_PASSWORD    = "FORGOT_PASSWORD";
    public static final String REFRESH_TOKEN      = "refresh_token";
    public static final String AUTHORIZATION      = "Authorization";
    public static final String AUTHORIZATION_DESC = "Basic Z293aWxkX2FwcF9jbGllbnQ6U1VrZSQjODNqZGlzbClESl8uLDMyNA==";

    public static final String TOKEN = "token";//用于忘记密码
    public static final String USER  = "user";//用于忘记密码
    public static final String SSID  = "ssid";

    public static final String INFOS = "infos";
    public static final String INFO  = "info";
    public static final String POS   = "pos";

    public static final String BPHOTO = "bPhoto";

    public static final String TYPE        = "type";
    public static final String ALBUM_ID    = "albumId";
    public static final String CONTENT     = "content";
    public static final String REMIND_TIME = "remindTime";
    public static final String REMIND_ID   = "remindId";
    public static final String ALARM_ID    = "alarmId";
    public static final String ALARM_FLAG  = "alarmFlag";
    public static final String HOUR        = "hour";
    public static final String MINUTE      = "minute";
    public static final String ENABLE      = "enabled";
    public static final String CYCLE_INDEX = "cycleIndex";
    public static final String SORT        = "sort";
    public static final String SEARCH_KEY  = "searchKey";
    public static final String CLASSIFY_ID = "classifyId";
    public static final String CATEGORY_ID = "categoryId";
    public static final String CLASSIFY    = "classify";
    public static final String PAGE_NUM    = "pageNum";
    public static final String PAGE_SIZE   = "pageSize";
    public static final String LOGIN_TYPE  = "LOGIN_TYPE";
    public static final String MUSIC_ID    = "musicId";
    public static final String STORY_ID    = "storyId";
    public static final String PLAY_STATUS = "playStatus";
    public static final String PLAY_TYPE   = "playType";

    //媒体播放状态:播放
    public static final int PLAY_STATUS_PLAY       = 1;
    //媒体播放状态:暂停
    public static final int PLAY_STATUS_PAUSE      = 2;
    //媒体播放状态:恢复(故事专属)
    public static final int PLAY_STATUS_RESUME     = 4;
    //媒体播放状态:停止
    public static final int PLAY_STATUS_STOP       = 3;
    //媒体播放状态:下一首
    public static final int PLAY_STATUS_NEXT       = 5;
    //媒体播放类型:未知
    public static final int PLAY_TYPE_OTHER        = 0;
    //媒体播放类型:收藏列表
    public static final int PLAY_TYPE_COLLECT_LIST = 1;
    //媒体播放类型:机器（未启用）
    public static final int PLAY_TYPE_ROBOT        = 2;

    //重新登录
    public static final String RELOGIN = "RELOGIN";
    //退出登录
    public static final String LOGOUT  = "LOGOUT";
    public static final String IS_PLAY = "isPlay";

    //成就
    public static final String PAGENUM  = "pageNum";
    public static final String PAGESIZE = "pageSize";

    //财富排行
    public static final String PETID = "petId";

    //个人中心
    public static final String KEY_PRE_NAME_ACCOUNT = "account";
    public static final String KEY_BIRTHDAY         = "birthday";
    public static final String KEY_ADDRESS          = "address";
    public static final String KEY_SEX              = "sex";
    public static final String KEY_HOBBY            = "hobby";
    public static final String KEY_STYLE            = "style";
    public static final String KEY_PHONE            = "phone";
    public static final String KEY_EMAIL            = "email";
    public static final String KEY_NICKNAME         = "nickname";

    //意见反馈
    public static final String KEY_FEEDBACK_PHONE   = "phone";
    public static final String KEY_FEEDBACK_CONTENT = "content";

    //修改密码
    public static final String KEY_USERNAME    = "username";
    public static final String KEY_PASSWORD    = "password";
    public static final String KEY_OLDPASSWORD = "oldPassword";

    //目录结构定义
    /**
     * Data的缓存目录
     */
    public static final String CACHE_DIR   = GowildApplication.context.getCacheDir().getAbsolutePath();
    /**
     * Data目录缓存图片
     */
    public static final String CACHE_IMAGE = CACHE_DIR + "img/";

    public static final String AV_MONITOR_CAPTURE_IMAGE_FILE_NAME = "picture";

    public static final String AV_MONITOR_CAPTURE_VIDEO_FILE_NAME = "video";

    public static final String AV_MONITOR_VIDEO_FILE_SUFFIX = ".mp4";

    //请求码
    public static final String SUCCESS               = "100";//成功
    public static final String CAPTCHA_ERROR_TYPE    = "302";//错误
    public static final String CAPTCHA_EXIT          = "2005";//已存在
    public static final String CAPTCHA_BUSY          = "2001";//请求太频繁
    public static final String CAPTCHA_ERROR         = "2003";//验证码错误
    public static final String CAPTCHA_ERROR_OUTTIME = "2002";//验证码超时

    public static final String USERNAME            = "username";
    public static final String ID                  = "id";
    public static final String ACCESS_TOKEN_NOBEAR = "access_token_nobear";
    public static final String EXPIRES_IN          = "expires_in";
    public static final String NEED_LUNCH          = "need_lunch";
    public static final String NEED_LUNCH_FLAG     = "need_lunch_flag";
    public static final String LEVEL               = "level";
    public static final String CLASS               = "class";
    public static final String ROBOT_ISBIND        = "robot_isonline";
    public static final String TITLE               = "1";
    public static final String WIFI_PASSWROD       = "wifi_passwrod";
    public static final String MAGICCODE           = "magicCode";

    public static final int    AV                = 0;
    public static final int    ALARM             = 1;//闹钟
    public static final int    HOME              = 2;//家电
    public static final int    MUSIC             = 3;//音乐
    public static final int    STORY             = 4;//故事
    public static final int    LOVER             = 5;
    public static final int    WEB               = 6;
    public static final int    ALARM_OUTLINE     = 7;//闹钟
    public static final String IS_CONNECT_SUCESS = "is_connect_sucess";
    public static final String ISFIST_LUNCH_IN   = "is_frist_in";
    public static final String ROBOT_RESULT      = "robot_result";
    public static final String FRIST_LUNCH       = "frist_lunch";
    public static final String PASSWORD          = "password";
    public static final String GRANT_TYPE        = "grant_type";
    public static final String BACK              = "back";

    /**
     * 机器已经连接
     */
    public static final int ROBOT_CONNECTION = 1;

    public static final String TASK         = "task";
    public static final String HONOR        = "honor";
    public static final String REGISTER     = "register";
    public static final String NET_ERROR    = "error";
    public static final String SHOWREDPOINT = "showredpoint";
    public static final String SKILL_NUM    = "skill_num";
    public static final String STARTTIME    = "starttime";
    public static final String ENDTIME = "endtime";


    public static final String STATSTICS = "statstics";
}
