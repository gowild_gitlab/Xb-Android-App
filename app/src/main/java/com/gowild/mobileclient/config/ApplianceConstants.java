/**
 * Copyright (C)  2016 深圳市狗尾草智能科技有限公司
 * GowildXB
 * ApplianceConstants.java
 */
package com.gowild.mobileclient.config;

/**
 * @author Zhangct
 * @version 1.0
 *          <p> 电器常量 </p>
 * @since 2016/8/20 17:08
 */
public interface ApplianceConstants {

    /**
     * 空调
     */
    public interface Air {
        //消息头
        // 家电定义
        public final int APPLIANCES_MAIN = 100; //同步数据消息头(包括手机端第一次请求同步数据和机器端返回数据，删除设备)
        public final int APPLIANCES_OPTION = 200; //空调匹配消息头（手机端匹配页面所有收发消息用到的消息头）
        public final int APPLIANCES_AIR = 300; //空调控制消息头（手机端空调控制页面发送数据用到的消息头）

        // 同步消息和删除设备（消息头是：100）
        public static final int MESSAGE_SYN = 0;//同步数据
        public static final int MESSAGE_DEL_ALL = 1; //删除整个设备（所有空调）
        public static final int MESSAGE_DEL = 2;//删除设备下的某个场景（比如 客厅空调）

        //家电匹配协议（消息头是：200）
        //消息类型
        public static final int MESSAGE_INIT = 0;// 刚进入配置页面状态
        public static final int MESSAGE_COMMAND = 1;//点击配置页面按钮指令状态
        public static final int MESSAGE_RETURN = 2;//退出配置页面
        public static final int MESSAGE_CONTINUE_MATCHING = 3;//重新回到配置页面发送继续匹配处理（主要处理正在匹配手机黑屏然后再进入继续匹配）
        public static final int MESSAGE_FINISH = 4;// 第一次匹配家电结束发送命令（目的是为了语音播报匹配完成）

        //按键key
        public final int KEY_START = 1;// 开始匹配|重新匹配
        public final int KEY_STOP = 2; //停止匹配
        public final int KEY_SAVE = 3;// 保存
        public final int KEY_NEXT = 4;// 下一组
        public final int KEY_PRE = 5;// 上一组


        // 家电控制（空调­­­­ 消息头：300）
        // 设备类型
//        public final int MESSAGE_SYN = 0;//同步数据
        public final int MESSAGE_CONTROL = 1; //控制设备
        // 状态指令：
        public final String COMMAND_STAEON = "STATEON";//开
        public final String COMMAND_STAEOFF = "STATEOFF"; //关
        // 动作指令
        public final String COMMAND_ADD = "ACTIONADD";//增加，调高，增大
        public final String COMMAND_REDUCE = "ACTIONREDUCE"; //减小， 调低
        public final String COMMAND_TO = "ACTIONTO"; //调到，改到，变到
        // 属性
        public final String COMMAND_SPEED = "SPEED";//风速
        public final String COMMAND_TEMP = "TEMP"; //温度
        public final String COMMAND_DIR = "DIR"; //风向
        //属性值
        public final String COMMAND_MAX = "MAX";//最大，最高
        public final String COMMAND_MIN = "MIN"; //最小，最低，最冷
        // 数字的话直接传阿拉伯数字就好了（16‐31这里就不做定义了）
        // 模式
        public final String COMMAND_REFRIGERATION = "REFRIGERATION";//制冷
        public final String COMMAND_HEATING = "HEATING"; //制热
        public final String COMMAND_BLAST = "BLAST"; //送风
        public final String COMMAND_DRY = "DRY"; //抽湿
        public final String COMMAND_AUTO = "AUTO"; //自动

        public final String COMMAND_SWING_LEFT_RIGHT = "LEFT_RIGHT"; //左右风
        public final String COMMAND_SWING_UP_DOWN = "UP_DOWN"; //上下风

    }

}
