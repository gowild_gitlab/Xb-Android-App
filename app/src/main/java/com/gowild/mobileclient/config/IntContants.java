/**
 * Copyright (C)  2016 深圳市狗尾草智能科技有限公司
 * GowildXB
 * IntContants.java
 */
package com.gowild.mobileclient.config;

import android.content.Context;

import com.gowild.mobileclient.R;

/**
 * @author temp
 * @version 1.0
 *          <p><strong>Features draft description.主要功能介绍</strong></p>
 * @since 2016/8/5 11:13
 */
public class IntContants {

    // ===========================================================
    // HostConstants
    // ===========================================================

    private static final String TAG = IntContants.class.getSimpleName();

    public static final int TYPE_MUSIC = 1;
    public static final int TYPE_STORY = 2;
    public static final int TYPE_ROBOT_SUCESS = 1;
    public static final int TYPE_ROBOT_OUTLINE = 2;
    public static final int TYPE_ROBOT_NO_ROBOT = 3;
    public static final int TYPE_ROBOT_ERROR = -1;
    public static final int TYPE_ROBOT_ERROR_HTTP = -2;


    public static int PHOTO_THUMB_WIDTH;

    public static int sCurrentResourceType;

    public static boolean isCurrentMusic() {
        return sCurrentResourceType == TYPE_MUSIC;
    }

    public static void init(Context context) {
        PHOTO_THUMB_WIDTH = context.getResources().getDimensionPixelSize(R.dimen.gowild_photo_item_width);
    }
}
