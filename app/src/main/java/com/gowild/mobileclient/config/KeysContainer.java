package com.gowild.mobileclient.config;

/**
 * Created by Administrator on 2016/4/6.
 */
public class KeysContainer {
    public static final String KEY_PRE_NAME_ACCOUNT = "account";
    public static final String KEY_USERNAME = "username";
    public static final String KEY_PASSWORD = "password";
    public static final String KEY_EMAIL = "email";
    public static final String KEY_NICKNAME = "nickname";
    public static final String KEY_BIRTHDAY = "birthday";
    public static final String KEY_ADDRESS = "address";
    public static final String KEY_SEX = "sex";
    public static final String KEY_HOBBY = "hobby";

    public static final int KEY_REQUEST_USERINFO = 1;

    public static final String KEY_ROBOT_VERSION = "robotVersion";
    public static final String KEY_ROBOT_VOLUME = "robotVolume";
    public static final String KEY_VOLUME="volume";
    public static final String KEY_TALK_SPEED = "talkSpeed";
    public static final String KEY_MAC = "mac";
    public static final String KEY_WIFI_NAME = "wifiName";
    public static final String KEY_WIFI_PASS = "wifiPass";
    public static final String KEY_ROBOT_BIND_TITLE = "robotBindTitle";

    public static final String IS4G = "is4G";
    public static final String SN   = "sn";

}
