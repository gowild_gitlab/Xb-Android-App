/**
 * Copyright (C)  2016 深圳市狗尾草智能科技有限公司
 * GowildXB
 * MediaConstants.java
 */
package com.gowild.mobileclient.config;

import com.gowild.mobileclient.protocol.MusicBothMsgProto;
import com.gowild.mobileclient.protocol.StoryBothMsgProto;

/**
 * @author ljd
 * @version 1.0
 *          <p><strong>Features draft description.主要功能介绍</strong></p>
 * @since 2016/8/13 16:37
 */
public class MediaConstants {

    // ===========================================================
    // HostConstants
    // ===========================================================

    private static final String TAG = MediaConstants.class.getSimpleName();

    // ===========================================================
    // Static Fields
    // ===========================================================
    public static final int PLAY_STATE_PLAY = 1;
    public static final int PLAY_STATE_STOP = 2;
    public static final int COLLECT=1;
    public static final int COLLECT_NO=2;

    // ===========================================================
    // Fields
    // ===========================================================


    public static MusicBothMsgProto.MusicAndAlbumInfoMsg mMusicMsg = null;

    public static StoryBothMsgProto.StoryAndAlbumInfoMsg mStoryMsg = null;

    public static int mCollect = -1;

    public static int mMusicCollect =-1;

    public static int mStoryCollect =-1;


    public static int mPlayState = PLAY_STATE_PLAY;

    // ===========================================================
    // Constructors
    // ===========================================================


    // ===========================================================
    // Getter or Setter
    // ===========================================================


    // ===========================================================
    // Methods for/from SuperClass/Interfaces
    // ===========================================================


    // ===========================================================
    // Methods
    // ===========================================================


    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================

}
