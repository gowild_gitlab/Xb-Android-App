/*
 * Copyright (C) 2015 iChano incorporation's Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.gowild.mobile.libviewer.utils;

public class Constants {
	public static final String STREAMER_STATE = "com.ichano.ex.viewer.streamer.online";
	public static final String SESSIONS_STATE = "com.ichano.ex.viewer.streamer.session";	
	public static final String VIDEO_MP4 = ".mp4";
	public static final String IMG_JPG = ".jpg";
	public static final String RECORD_VIDEO_PATH = "ViewerDemo/recordVideo";
	public static final String CAPTURE_IAMGE_PATH = "ViewerDemo/capture";
	public static final String LOCAL_ICON_PATH = "ViewerDemo/icon";
	public static final String LOCAL_CID_ICON_PATH = "ViewerDemo/cid_icon";
}
