package com.gowild.mobile.libviewer;

import android.content.Context;
import android.util.Log;

import com.ichano.rvs.viewer.constant.AuthState;
import com.ichano.rvs.viewer.constant.RemoteStreamerState;
import com.ichano.rvs.viewer.constant.RvsError;
import com.ichano.rvs.viewer.constant.StreamerConfigState;
import com.ichano.rvs.viewer.constant.StreamerInfoType;
import com.ichano.rvs.viewer.ui.ViewerInitHelper;

public class MyViewerHelper extends ViewerInitHelper {

	private static final String TAG = "MyViewerHelper";
	private static MyViewerHelper mViewer;
	private ViewerListener mViewerListener;
	private boolean mHasLogin = false;

	private boolean isLoginSuccess = false;

	public static MyViewerHelper getInstance() {
		if (null == mViewer) {
			Log.e(TAG,"must call initInstance before this method");
		}
		return mViewer;
	}

	public static void initInstance(Context applicationContext){
		if (null == mViewer) {
			mViewer = new MyViewerHelper(applicationContext);
		}
	}


	private MyViewerHelper(Context applicationContext) {
		super(applicationContext);
	}

	// 设置AppID，从网站注册获得
	@Override
	public String getAppID() {
		return "80a5ee4e8bc449f2b1430590de4aaf6e";
	}

	// 设置CompanyID，从网站注册获得
	@Override
	public String getCompanyID() {
		return "90c46877503f4f94b6d2f9b63832f68b";
	}

	// 设置CompanyKey，从网站注册获得
	@Override
	public String getCompanyKey() {
		return "1447295343318";
	}

	// 设置License，从网站注册获得
	@Override
	public String getLicense() {
		return "";
	}

	@Override
	public void onAuthResult(AuthState state, RvsError error) {
		if (AuthState.SUCCESS == state) {
			mHasLogin = true;
			//如果没有通知过，就进行通知，如果已经通知过一次，则跳过
			if(!isLoginSuccess) {
				isLoginSuccess = true;
				if (null != mViewerListener) {
					mViewerListener.onLoginResult(true);
					Log.d("onAuthResult", "onLoginResult:" + state );
				}
			}
		}else{
			mHasLogin = false;
		}
		Log.d("onAuthResult", "AuthState.SUCCESS:" + state );
//		Log.d("onAuthResult", "isLogined:" + isLogined );
	}

	@Override
	public void onRemoteStreamerStateChange(long streamerCID, RemoteStreamerState state, RvsError error) {
		Log.d(TAG, "onRemoteStreamerStateChange state" + state);
		if (state == RemoteStreamerState.CANUSE) {
			// 观看端和采集端连通, 可观看端实时视频
			mViewerListener.onRemoteStreamState(streamerCID,true);
		} else if (state == RemoteStreamerState.FAIL) {
			// 观看端和采集端没有连通
		}
//		Log.d("onStreamerPresenceState", "streamer:" + streamerCID + ",online state:" + state.toString());
	}

	@Override
	public void onStreamerConfigState(long streamerCID,
									  StreamerConfigState state) {
		Log.d(TAG, "onStreamerConfigState state" + state);

	}

	@Override
	public void onUpdateCID(long cid) {
		// mUserInfo.saveClientCid(cid);
	}

	public void setViewerListener(ViewerListener l) {
		mViewerListener = l;
	}

	public interface ViewerListener {
		public void onLoginResult(boolean success);
		public void onRemoteStreamState(long streamerCid,boolean session);
	}

	@Override
	public void onStreamerInfoUpdate(long streamerCID, StreamerInfoType infoType) {

	}

	@Override
	public boolean useIchanoUserSystem() {
		return false;
	}

	@Override
	public void login() {
		super.login();
		isLoginSuccess = false;
	}

	@Override
	public void logout() {
		super.logout();
		isLoginSuccess = false;
		mHasLogin = false;
	}

	public boolean hasLogin() {
		return mHasLogin;
	}

}
