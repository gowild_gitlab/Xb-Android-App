/**
 * Copyright (C)  2016 深圳市狗尾草智能科技有限公司
 * GowildNewXB
 * JsonRequestParams.java
 */
package com.gowild.mobile.libhttp;

import com.alibaba.fastjson.JSON;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.ResponseHandlerInterface;

import org.apache.http.HttpEntity;
import org.apache.http.entity.StringEntity;
import java.io.IOException;


/**
 * @author Ji.Li
 * @since 2016/7/11 11:03
 * @version 1.0
 * <p><strong>Features draft description.主要功能介绍</strong></p>
 */
@SuppressWarnings("unused")
public class JsonRequestParams extends RequestParams{

    // ===========================================================
    // Constants
    // ===========================================================

    private static final String TAG = JsonRequestParams.class.getSimpleName();

    // ===========================================================
    // Static Fields
    // ===========================================================

    // ===========================================================
    // Fields
    // ===========================================================

    private Object mObj;

    // ===========================================================
    // Constructors
    // ===========================================================

    public JsonRequestParams(Object obj){
        this.mObj = obj;
    }

    // ===========================================================
    // Getter or Setter
    // ===========================================================


    // ===========================================================
    // Methods for/from SuperClass/Interfaces
    // ===========================================================

    @Override
    public HttpEntity getEntity(ResponseHandlerInterface progressHandler) throws IOException {
        StringEntity entity = new StringEntity(convertObjectToJson(), contentEncoding);
        return entity;
    }

    public String convertObjectToJson() {
        if (this.mObj == null) {
            return null;
        } else {
            return JSON.toJSONString(this.mObj);
        }
    }

    // ===========================================================
    // Methods
    // ===========================================================

    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================

}
