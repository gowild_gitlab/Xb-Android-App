package com.gowild.mobile.libhttp;

import android.util.Log;

import com.alibaba.fastjson.JSON;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.apache.http.Header;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.ParameterizedType;


/**
 * @author JunDong Wang
 * @version 1.0
 *          <p><strong>接收Http返回的信息并且返回到上层</strong></p>
 * @since 2016/7/23 10:04
 */
@SuppressWarnings("unused")
public abstract class ObjectAsynHttpResponseHandler<T> extends AsyncHttpResponseHandler {

    private static final String TAG = ObjectAsynHttpResponseHandler.class.getSimpleName();

    @Override
    public final void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
        if (responseBody != null) {
            try {
                String text = new String(responseBody, "UTF-8");
                Log.d(TAG, "onSuccess() text = " + text);
                Class<T> clz = (Class<T>) (((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0]);
                T obj = JSON.parseObject(text, clz);
                onResponseSuccess(statusCode, headers, obj);
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
                onResponseFailure(statusCode, headers, null, e);
            }
        } else {
            onResponseSuccess(statusCode, headers, null);
        }
    }

    @Override
    public final void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
        onResponseFailure(statusCode, headers, null, error);
    }

    public abstract void onResponseSuccess(int statusCode, Header[] headers, T response);

    public abstract void onResponseFailure(int statusCode, Header[] headers, T response, Throwable error);

}
