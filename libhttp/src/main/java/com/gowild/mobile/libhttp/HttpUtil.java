/**
 * Copyright (C)  2016 深圳市狗尾草智能科技有限公司
 * GowildNewXB
 * HttpUtil.java
 */
package com.gowild.mobile.libhttp;

import android.content.Context;
import android.util.Log;

import com.gowild.mobile.libcommon.utils.CheckNetworkUtil;
import com.gowild.mobile.libcommon.utils.Logger;
import com.gowild.mobile.libcommon.utils.ToastUtils;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.apache.http.Header;
import org.apache.http.HttpEntity;

import java.io.IOException;


/**
 * @author Ji.Li
 * @version 1.0
 *          <p><strong>Features draft description.主要功能介绍</strong></p>
 * @since 2016/7/11 10:26
 */
@SuppressWarnings("unused")
public final class HttpUtil {

    // ===========================================================
    // Constants
    // ===========================================================

    private static final String TAG = HttpUtil.class.getSimpleName();
    private static final String CONTENT_TYPE_JSON = "application/json;charset=utf-8";
    private static final String CONTENT_TYPE_NORMAL = "application/x-www-form-urlencoded";

    // ===========================================================
    // Static Fields
    // ===========================================================

    private static volatile HttpUtil sInstance = null;

    // ===========================================================
    // Fields
    // ===========================================================

    private static AsyncHttpClient mHttpClient = new AsyncHttpClient();

    // ===========================================================
    // Constructors
    // ===========================================================

    private HttpUtil() {
        mHttpClient.setConnectTimeout(30000);
    }

    // ===========================================================
    // Getter or Setter
    // ===========================================================


    // ===========================================================
    // Methods for/from SuperClass/Interfaces
    // ===========================================================


    // ===========================================================
    // Methods
    // ===========================================================

    public static HttpUtil getInstance() {
        if (sInstance == null) {
            synchronized (HttpUtil.class) {
                if (sInstance == null) {
                    sInstance = new HttpUtil();
                }
            }
        }
        return sInstance;
    }

    public void post(Context context, String url, Object reqParams, AsyncHttpResponseHandler responseHandler) {
        if (!CheckNetworkUtil.isNetworkAvailable(context)) {
            responseHandler.onFailure(0, null, null,null);
            return;
        }

        JsonRequestParams params = new JsonRequestParams(reqParams);
        HttpEntity entity = null;
        try {
            entity = params.getEntity(responseHandler);
        } catch (IOException e) {
            if (responseHandler != null) {
                responseHandler.onFailure(0, null, null, e);
            }
            e.printStackTrace();
        }
        this.mHttpClient.post(context, url, entity, CONTENT_TYPE_JSON, responseHandler);
    }




    public void post(Context context, String url, String headKey, String headValue, RequestParams reqParams, AsyncHttpResponseHandler responseHandler) {
        Log.d(TAG, "url:" + url);
        if (!CheckNetworkUtil.isNetworkAvailable(context)) {
            ToastUtils.showCustom(context,context.getString(R.string.net_error),false);
            responseHandler.onFailure(0, null, null,null);
            return;
        }
        this.mHttpClient.removeAllHeaders();
        this.mHttpClient.addHeader(headKey, headValue);
        this.mHttpClient.post(context,url,null,reqParams,CONTENT_TYPE_NORMAL,responseHandler);
    }

    public void post(Context context, String url, Header[] headers, RequestParams reqParams, AsyncHttpResponseHandler responseHandler) {
        Logger.d(TAG, "url = " + url + "  " + "reqParams = " + reqParams);
        if (!CheckNetworkUtil.isNetworkAvailable(context)) {
            ToastUtils.showCustom(context,context.getString(R.string.net_error),false);
            responseHandler.onFailure(0, null, null,null);
            return;
        }
        this.mHttpClient.post(context, url, headers, reqParams, CONTENT_TYPE_NORMAL, responseHandler);
    }

    public void get(Context context, String url, Header[] headers, RequestParams requestParams, AsyncHttpResponseHandler responseHandler) {
        Logger.d(TAG, "url = " + url + "  " + "reqParams = " + requestParams);
        if (!CheckNetworkUtil.isNetworkAvailable(context)) {
            ToastUtils.showCustom(context,context.getString(R.string.net_error),false);
            responseHandler.onFailure(0, null, null,null);
            return;
        }
        this.mHttpClient.get(context, url, headers, requestParams, responseHandler);
    }

    public void cancelRequest(Context context) {
        this.mHttpClient.cancelRequests(context, true);
    }

    public void post(Context context, String url, RequestParams reqParams, AsyncHttpResponseHandler responseHandler) {
        if (!CheckNetworkUtil.isNetworkAvailable(context)) {
            ToastUtils.showCustom(context,context.getString(R.string.net_error),false);
            responseHandler.onFailure(0, null, null,null);
            return;
        }
        this.mHttpClient.post(context, url, reqParams, responseHandler);
    }
    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================

}
