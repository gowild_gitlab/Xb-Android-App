package com.gowild.gwais.libstatstics;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Bundle;

import java.util.Map;

/**
 * @author Ji.Li
 */
@SuppressWarnings("unused")
public class ReporterFacade {

	@SuppressWarnings("unused")
	private static final String TAG     = ReporterFacade.class.getSimpleName();
	
	private static final String APPKEY  = "UMENG_APPKEY";
	private static final String CHANNEL = "UMENG_CHANNEL";
	
	private static IReporter sReporter;
	
	private ReporterFacade() {}
	
	public static final void init(Context context) {
		sReporter = new Reporter();
		PackageManager pm = context.getPackageManager();
		String pkgName = context.getPackageName();
		ApplicationInfo info;
		try {
			info = pm.getApplicationInfo(pkgName, PackageManager.GET_META_DATA);
			Bundle bundle = info.metaData;
			String appKey = bundle.getString(APPKEY);
			String channel = bundle.getString(CHANNEL);
			IReporter reporter = new UMengReporter(context, appKey, channel,
					true, true);
			Reporter r = new Reporter();
			r.add(reporter);
			sReporter = reporter;
		} catch (NameNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	public static final void onResumeActivity(Context context) {
        sReporter.onResumeActivity(context);
	}

	public static final void onPauseActivity(Context context) {
        sReporter.onPauseActivity(context);
	}

	public static final void onEvent(Context context, String eventId) {
        sReporter.onEvent(context, eventId);
	}

	public static final void onEvent(Context context, String eventId,
			Map<String, String> values) {
        sReporter.onEvent(context, eventId, values);
	}

	public static final void onError(Context context, String error) {
        sReporter.onError(context, error);
	}

	public static final void onError(Context context, Throwable th) {
        sReporter.onError(context, th);
	}

}
