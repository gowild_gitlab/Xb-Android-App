package com.gowild.gwais.libstatstics;

import android.content.Context;

import java.util.List;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 *
 * @author Ji.Li
 *
 **/
class Reporter implements IReporter,IReporterManager{
	
	@SuppressWarnings("unused")
	private static final String TAG = Reporter.class.getSimpleName();
	
	private List<IReporter>     mReporters = null;

	public Reporter() {
		this.mReporters = new CopyOnWriteArrayList<>();
	}
	
	@Override
	public void onResumeActivity(Context context) {
		IReporter reporter = null;
		for (int i = 0, n = mReporters.size(); i < n; i++) {
			reporter = mReporters.get(i);
			reporter.onResumeActivity(context);
		}
	}

	@Override
	public void onPauseActivity(Context context) {
		IReporter reporter = null;
		for (int i = 0, n = mReporters.size(); i < n; i++) {
			reporter = mReporters.get(i);
			reporter.onPauseActivity(context);
		}
	}

	@Override
	public void onEvent(Context context, String eventId) {
		IReporter reporter = null;
		for (int i = 0, n = mReporters.size(); i < n; i++) {
			reporter = mReporters.get(i);
			reporter.onEvent(context, eventId);
		}
	}

	@Override
	public void onEvent(Context context, String eventId,
			Map<String, String> values) {
		IReporter reporter = null;
		for (int i = 0, n = mReporters.size(); i < n; i++) {
			reporter = mReporters.get(i);
			reporter.onEvent(context, eventId, values);
		}
	}

	@Override
	public void onError(Context context, String error) {
		IReporter reporter = null;
		for (int i = 0, n = mReporters.size(); i < n; i++) {
			reporter = mReporters.get(i);
			reporter.onError(context, error);
		}
	}

	@Override
	public void onError(Context context, Throwable th) {
		IReporter reporter = null;
		for (int i = 0, n = mReporters.size(); i < n; i++) {
			reporter = mReporters.get(i);
			reporter.onError(context, th);
		}
	}
	
	@Override
	public void onPageStart(String name) {
		IReporter reporter = null;
		for (int i = 0, n = mReporters.size(); i < n; i++) {
			reporter = mReporters.get(i);
			reporter.onPageStart(name);
		}
	}

	@Override
	public void onPageEnd(String name) {
		IReporter reporter = null;
		for (int i = 0, n = mReporters.size(); i < n; i++) {
			reporter = mReporters.get(i);
			reporter.onPageEnd(name);
		}
	}

	@Override
	public void add(IReporter repoter) {
		this.mReporters.add(repoter);
	}

	@Override
	public void remote(IReporter reporter) {
		this.mReporters.remove(reporter);
	}

	@Override
	public void clean() {
		this.mReporters.clear();
	}
}
