package com.gowild.gwais.libstatstics;

import android.content.Context;

import com.umeng.analytics.MobclickAgent;
import com.umeng.analytics.MobclickAgent.EScenarioType;
import com.umeng.analytics.MobclickAgent.UMAnalyticsConfig;

import java.util.Map;

/**
 * @author Ji.Li
 */
class UMengReporter implements IReporter {

    @SuppressWarnings("unused")
    private static final String TAG = UMengReporter.class.getSimpleName();

    public UMengReporter(Context context, String appKey, String channel,
                         boolean crashEnable, boolean checkDevice) {
        UMAnalyticsConfig config = new UMAnalyticsConfig(context, appKey,
                channel, EScenarioType.E_UM_NORMAL, crashEnable);
		//MobclickAgent.setDebugMode(BuildConfig.DEBUG);
        MobclickAgent.setDebugMode(true);
        MobclickAgent.setCheckDevice(checkDevice);
        MobclickAgent.startWithConfigure(config);
    }

    @Override
    public void onResumeActivity(Context context) {
        MobclickAgent.onResume(context);
    }

    @Override
    public void onPauseActivity(Context context) {
        MobclickAgent.onPause(context);
    }

    @Override
    public void onEvent(Context context, String event) {
        MobclickAgent.onEvent(context, event);
    }

    @Override
    public void onError(Context context, String error) {
        MobclickAgent.reportError(context, error);
    }

    @Override
    public void onError(Context context, Throwable th) {
        MobclickAgent.reportError(context, th);
    }

    @Override
    public void onEvent(Context context, String eventId,
                        Map<String, String> values) {
        MobclickAgent.onEvent(context, eventId, values);
    }

    @Override
    public void onPageStart(String name) {
        MobclickAgent.onPageStart(name);
    }

    @Override
    public void onPageEnd(String name) {
        MobclickAgent.onPageEnd(name);
    }
}
