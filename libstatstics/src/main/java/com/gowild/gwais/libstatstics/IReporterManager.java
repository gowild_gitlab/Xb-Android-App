package com.gowild.gwais.libstatstics;

/**
 * @author Ji.Li
 */
interface IReporterManager {

	public void add(IReporter repoter);

	public void remote(IReporter reporter);

	public void clean();
	
}
