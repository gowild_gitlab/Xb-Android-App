package com.gowild.gwais.libstatstics;

import android.content.Context;

import java.util.Map;

/**
 * @author Ji.Li
 */
interface IReporter {

	void onResumeActivity(Context context);

	void onPauseActivity(Context context);
	
	void onPageStart(String name);

	void onPageEnd(String name);

	void onEvent(Context context, String eventId);

	void onEvent(Context context, String eventId, Map<String, String> values);

	void onError(Context context, String error);

	void onError(Context context, Throwable th);
}
