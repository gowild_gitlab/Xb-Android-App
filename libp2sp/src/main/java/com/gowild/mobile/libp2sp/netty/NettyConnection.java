/**
 * Copyright (C)  2016 深圳市狗尾草智能科技有限公司
 * GO
 * NettyConnection.java
 */
package com.gowild.mobile.libp2sp.netty;

import com.gowild.mobile.libp2sp.core.Connection;
import com.gowild.mobile.libp2sp.core.Message;
import com.gowild.mobile.libp2sp.core.exception.NotConnectedException;

import java.io.IOException;

import io.netty.channel.Channel;

/**
 * @author Ji.Li
 * @version 1.0
 *          <p><strong>Features draft description.主要功能介绍</strong></p>
 * @since 2016/7/18 20:03
 */
@SuppressWarnings("unused")
public class NettyConnection extends Connection {

    // ===========================================================
    // HostConstants
    // ===========================================================

    private static final String TAG = NettyConnection.class.getSimpleName();

    // ===========================================================
    // Static Fields
    // ===========================================================

    // ===========================================================
    // Fields
    // ===========================================================

    private Channel mSession = null;

    // ===========================================================
    // Constructors
    // ===========================================================

    public NettyConnection(String hostAddress, int port, Channel session) {
        super(hostAddress, port);
        this.mSession = session;
    }

    // ===========================================================
    // Getter or Setter
    // ===========================================================


    // ===========================================================
    // Methods for/from SuperClass/Interfaces
    // ===========================================================

    @Override
    public void send(Message message) throws NotConnectedException {
        boolean isConnected = isConnected();
        if (isConnected) {
            Channel session = mSession;
            session.writeAndFlush(message);
        } else {
            throw new NotConnectedException();
        }
    }

    @Override
    public boolean isClosed() {
        return mSession.isOpen();
    }

    @Override
    public boolean isConnected() {
        if (mSession != null) {
            return mSession.isActive();
        } else {
            return false;
        }
    }

    @Override
    public void close() throws IOException {
        mSession.close();
    }

    // ===========================================================
    // Methods
    // ===========================================================

    public Channel getSession() {
        return mSession;
    }

    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================

}
