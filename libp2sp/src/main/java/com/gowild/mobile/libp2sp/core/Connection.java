/**
 * Copyright (C)  2016 深圳市狗尾草智能科技有限公司
 * GO
 * Connection.java
 */
package com.gowild.mobile.libp2sp.core;

/**
 * @author Ji.Li
 * @since 2016/7/19 20:38
 * @version 1.0
 * <p><strong>Features draft description.主要功能介绍</strong></p>
 */
@SuppressWarnings("unused")
public abstract class Connection implements IConnection{

    // ===========================================================
    // HostConstants
    // ===========================================================

    private static final String TAG = Connection.class.getSimpleName();

    // ===========================================================
    // Static Fields
    // ===========================================================

    // ===========================================================
    // Fields
    // ===========================================================

    private String mHostAddress;
    private int    mPort;

    // ===========================================================
    // Constructors
    // ===========================================================

    public Connection(final String hostAddress, final int port) {
        this.mHostAddress = hostAddress;
        this.mPort = port;
    }

    // ===========================================================
    // Getter or Setter
    // ===========================================================

    public String getHostAddress() {
        return mHostAddress;
    }

    public int getPort() {
        return mPort;
    }

    protected void setHostAddress(String address) {
        this.mHostAddress = address;
    }

    protected void setPort(int port) {
        this.mPort = port;
    }

    // ===========================================================
    // Methods for/from SuperClass/Interfaces
    // ===========================================================

    // ===========================================================
    // Methods
    // ===========================================================


    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================

}
