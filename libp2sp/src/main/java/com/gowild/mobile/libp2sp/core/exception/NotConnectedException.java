/**
 * Copyright (C)  2016 深圳市狗尾草智能科技有限公司
 * GO
 * NotConnectedException.java
 */
package com.gowild.mobile.libp2sp.core.exception;

import android.util.AndroidException;

/**
 * @author Ji.Li
 * @since 2016/7/19 14:20
 * @version 1.0
 * <p><strong>Features draft description.主要功能介绍</strong></p>
 */
@SuppressWarnings("unused")
public class NotConnectedException extends AndroidException{

    // ===========================================================
    // HostConstants
    // ===========================================================

    private static final String TAG = NotConnectedException.class.getSimpleName();

    private static final String MESSAGE = "TCP服务器未登录";

    // ===========================================================
    // Static Fields
    // ===========================================================

    // ===========================================================
    // Fields
    // ===========================================================


    // ===========================================================
    // Constructors
    // ===========================================================

    public NotConnectedException() {
        super(MESSAGE);
    }

    // ===========================================================
    // Getter or Setter
    // ===========================================================


    // ===========================================================
    // Methods for/from SuperClass/Interfaces
    // ===========================================================


    // ===========================================================
    // Methods
    // ===========================================================


    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================

}
