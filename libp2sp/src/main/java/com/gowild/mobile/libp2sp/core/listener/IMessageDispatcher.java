/**
 * Copyright (C)  2016 深圳市狗尾草智能科技有限公司
 * GowildNewXB
 * IMessageDispatcher.java
 */
package com.gowild.mobile.libp2sp.core.listener;

import com.gowild.mobile.libp2sp.core.Message;

/**
 * @author Ji.Li
 * @since 2016/7/21 14:12
 * @version 1.0
 * <p><strong>Features draft description.主要功能介绍</strong></p>
 */
@SuppressWarnings("unused")
public interface IMessageDispatcher {

    // ===========================================================
    // HostConstants
    // ===========================================================


    // ===========================================================
    // Methods
    // ===========================================================

    void dispatchMessage(Message msg);

    void dispatchExceptionCaught(Throwable th);

    void dispatchIsActive(boolean isActive);

}
