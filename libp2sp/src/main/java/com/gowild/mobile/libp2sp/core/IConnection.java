/**
 * Copyright (C)  2016 深圳市狗尾草智能科技有限公司
 * GO
 * IConnection.java
 */
package com.gowild.mobile.libp2sp.core;

import com.gowild.mobile.libp2sp.core.exception.NotConnectedException;
import com.gowild.mobile.libp2sp.core.listener.IMessageSender;

import java.io.IOException;

/**
 * @author Ji.Li
 * @since 2016/7/18 18:18
 * @version 1.0
 * <p><strong>Features draft description.主要功能介绍</strong></p>
 */
@SuppressWarnings("unused")
interface IConnection extends IMessageSender{

    // ===========================================================
    // HostConstants
    // ===========================================================


    // ===========================================================
    // Methods
    // ===========================================================

    boolean isClosed();

    boolean isConnected();

    void close() throws IOException;
}
