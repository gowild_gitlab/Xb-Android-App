/**
 * Copyright (C)  2016 深圳市狗尾草智能科技有限公司
 * GowildNewXB
 * IDataEncoder.java
 */
package com.gowild.mobile.libp2sp.core.listener;

/**
 * @author Ji.Li
 * @since 2016/7/22 16:57
 * @version 1.0
 * <p><strong>Features draft description.主要功能介绍</strong></p>
 */
@SuppressWarnings("unused")
public interface IDataEncoder<T> {

    // ===========================================================
    // HostConstants
    // ===========================================================


    // ===========================================================
    // Methods
    // ===========================================================

    byte[] parseData(T o);

}
