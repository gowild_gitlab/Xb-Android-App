/**
 * Copyright (C)  2016 深圳市狗尾草智能科技有限公司
 * GO
 * NettyClientManager.java
 */
package com.gowild.mobile.libp2sp.netty;

import com.gowild.mobile.libcommon.utils.Logger;
import com.gowild.mobile.libp2sp.HostConstants;
import com.gowild.mobile.libp2sp.core.HandlerManager;
import com.gowild.mobile.libp2sp.core.Message;
import com.gowild.mobile.libp2sp.core.exception.NotConnectedException;
import com.gowild.mobile.libp2sp.core.listener.IDataHandler;
import com.gowild.mobile.libp2sp.core.listener.IDataListener;
import com.gowild.mobile.libp2sp.core.listener.IDataListenerManager;
import com.gowild.mobile.libp2sp.core.listener.IHandlerManager;
import com.gowild.mobile.libp2sp.core.listener.IHeartBeatHandler;
import com.gowild.mobile.libp2sp.core.listener.IMessageDispatcher;
import com.gowild.mobile.libp2sp.core.listener.IMessageSender;

import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

import io.netty.bootstrap.Bootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.util.AttributeKey;

/**
 * @author Ji.Li
 * @version 1.0
 *          <p><str ong>Features draft description.主要功能介绍</strong></p>
 * @since 2016/7/19 21:14
 */
@SuppressWarnings("unused")
public final class NettyClientManager implements IMessageSender, IHandlerManager {

    // ===========================================================
    // HostConstants
    // ===========================================================

    private static final String TAG = NettyClientManager.class.getSimpleName();
    private static final int NUM_THREADS = 1;
    private static final int SCHEDULE_RECONNECT_SECONDS = 1;
    private static final long SEND_HEART_BEAT_PERIOD = 20 * 1000;
    public static final AttributeKey<NettyClientManager> ATTRIBUTE_NETTY_CLIENT = AttributeKey.valueOf("ATTRIBUTE_NETTY_CLIENT");

    // ===========================================================
    // Static Fields
    // ===========================================================

    private static volatile NettyClientManager sInstance;

    // ===========================================================
    // Fields
    // ===========================================================

    private String mHostAddress ;
    private int    mPort ;
    private NettyInitializer mInitializer;
    private NettyChannelFutureListener mFutureListener;
    private ScheduleConnectTask mScheduleConnectTask;
    private Bootstrap mBootStrap;
    private IMessageDispatcher mMessageDispatch;
    private IHandlerManager mHandlerManager;
    private SendHeartBeatTask mSendHeartBeatTask;
    private Timer mTimer;

    private IHeartBeatHandler mHeartBeatHandler;
    private NettyConnection mConnection;
    private boolean mIsConnected = false;
    private boolean mIsStartHeartBeat = false;
    private boolean mIsConnecting = false;
    private final Object  mLock = new Object();

    // ===========================================================
    // Constructors
    // ===========================================================

	private NettyClientManager() {
		this.mInitializer = new NettyInitializer();
		this.mFutureListener = new NettyChannelFutureListener();
		this.mScheduleConnectTask = new ScheduleConnectTask();
		EventLoopGroup loopGroup = new NioEventLoopGroup(NUM_THREADS);
        this.mBootStrap = new Bootstrap();
        mBootStrap.group(loopGroup).channel(NioSocketChannel.class).handler(mInitializer).option(ChannelOption.SO_REUSEADDR, false);

		this.mMessageDispatch = HandlerManager.getInstance();
		this.mHandlerManager = HandlerManager.getInstance();
		this.mSendHeartBeatTask = new SendHeartBeatTask();
		this.mTimer = new Timer(true);
	}

    // ===========================================================
    // Getter or Setter
    // ===========================================================

    // ===========================================================
    // Methods for/from SuperClass/Interfaces
    // ===========================================================

    // ===========================================================
    // Methods
    // ===========================================================

    public static NettyClientManager getInstance() {
        if (sInstance == null) {
            synchronized (NettyClientManager.class) {
                if (sInstance == null) {
                    sInstance = new NettyClientManager();
                }
            }
        }
        return sInstance;
    }

    public void init(String hostAddress,int port){
        this.mHostAddress = hostAddress;
        this.mPort = port;

    }

    public void connect() {
        synchronized (mLock) {
            if (mIsConnected) {
                return;
            }
            if (!mIsConnecting) {
                Logger.d(TAG, "TCP地址"+HostConstants.HOST+" = {" + mIsConnected + "," + mIsConnecting + "}");
                Logger.d(TAG,"HTTP 地址1"+HostConstants.BASE_URL_FORMAL);
                Logger.d(TAG,"HTTP 地址2"+HostConstants.TOKEN_BASE_URL);
                Logger.d(TAG,"HTML地址"+HostConstants.HOST_HTML);

                ChannelFuture channelFuture = this.mBootStrap.connect(HostConstants.HOST, HostConstants.PORT);
                channelFuture.removeListener(mFutureListener);
                channelFuture.addListener(mFutureListener);
                mIsConnecting = true;
            }
        }
    }

    public void close() {
        Logger.d(TAG, "close()");
        try {
            synchronized (mLock){
                if (mConnection != null) {
                    mConnection.close();
                    mIsConnected = false;
                    mIsConnecting = false;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void send(Message message) throws NotConnectedException {
        //检查了半天，是这两句加了就不能登录了！
//        if (mConnection == null || mConnection.isClosed()) {
//            return;
//        }

        Logger.d(TAG,"mConnection " + mConnection  + "========" + mConnection.isConnected());
        Logger.d(TAG,message.getCode() + "");
        if (mConnection != null && mConnection.isConnected()) {
            mConnection.send(message);
        } else {
            throw new NotConnectedException();
        }
    }

    @Override
    public void putHandler(short key, IDataHandler handler) {
        this.mHandlerManager.putHandler(key, handler);
    }

    @Override
    public void removeHandler(short key) {
        this.mHandlerManager.removeHandler(key);
    }

    @Override
    public IDataHandler getHandler(short key) {
        return this.mHandlerManager.getHandler(key);
    }

    public boolean addHandlerListener(short key, IDataListener listener) {
        IDataHandler handler = this.mHandlerManager.getHandler(key);
        if (handler != null) {
            IDataListenerManager manager = (IDataListenerManager) handler;
            manager.addListener(listener);
            return true;
        } else {
            return false;
        }
    }

    public void removeHandlerListener(short key, IDataListener listener) {
        IDataHandler handler = this.mHandlerManager.getHandler(key);
        if (handler != null) {
            IDataListenerManager manager = (IDataListenerManager) handler;
            manager.remoteListener(listener);
        }
    }

    public void setHeartBeatHandler(IHeartBeatHandler handler){
        this.mHeartBeatHandler = handler;
    }

    public void startHeartBeat() {
        Logger.d(TAG,"startHeartBeat()");
        if (!mIsStartHeartBeat) {
	        mTimer = new Timer();
	        mSendHeartBeatTask = new SendHeartBeatTask();
            mTimer.scheduleAtFixedRate(mSendHeartBeatTask, 0, SEND_HEART_BEAT_PERIOD);
            mIsStartHeartBeat = true;
        }
    }

    public void stopHeartBeat() {
        Logger.d(TAG, "stopHeartBeat()");
        if (mIsStartHeartBeat) {
	        mTimer.cancel();
	        mTimer = null;
            mSendHeartBeatTask.cancel();
	        mSendHeartBeatTask = null;
            mIsStartHeartBeat = false;
        }
    }

    public boolean isConnected() {
        if (mConnection != null && mConnection.isConnected()) {
            return true;
        }
        return false;
    }

    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================

    private class NettyChannelFutureListener implements ChannelFutureListener {

        @Override
        public void operationComplete(ChannelFuture channelFuture) throws Exception {
            boolean isSuccess = channelFuture.isSuccess();
            Channel channel = channelFuture.channel();

            Logger.d(TAG,"operationComplete() isSuccess = " + isSuccess);
            synchronized (mLock) {
                mIsConnecting = false;
                if (!isSuccess) {
                    mConnection = null;
                    if (channel != null) {
                        channel.close();
                        channel.eventLoop().schedule(mScheduleConnectTask, SCHEDULE_RECONNECT_SECONDS, TimeUnit.SECONDS);
                    }
                    mIsConnected = false;
                } else {
                    if (mConnection != null && !mConnection.isClosed()){
                        mConnection.close();
                    }
                    mConnection = new NettyConnection(HostConstants.HOST, HostConstants.PORT, channel);
//                    connect();
                    mIsConnected = true;
                }
            }
        }
    }

    private class ScheduleConnectTask implements Runnable {

        @Override
        public void run() {
            connect();
	        Logger.d(TAG,"ScheduleConnectTask()");
        }
    }

    private class SendHeartBeatTask extends TimerTask {

        @Override
        public void run() {
            if (mHeartBeatHandler != null) {
                mHeartBeatHandler.onSendHeartBeat(NettyClientManager.getInstance());
            }
        }
    }
}
