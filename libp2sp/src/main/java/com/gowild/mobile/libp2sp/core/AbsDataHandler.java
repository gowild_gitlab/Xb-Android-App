/**
 * Copyright (C)  2016 深圳市狗尾草智能科技有限公司
 * GowildNewXB
 * AbsDataHandler.java
 */
package com.gowild.mobile.libp2sp.core;

import com.gowild.mobile.libp2sp.core.listener.IDataHandler;
import com.gowild.mobile.libp2sp.core.listener.IDataListener;
import com.gowild.mobile.libp2sp.core.listener.IDataListenerManager;

import java.lang.ref.WeakReference;
import java.util.ConcurrentModificationException;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * @author Ji.Li
 * @version 1.0
 *          <p><strong>Features draft description.主要功能介绍</strong></p>
 * @since 2016/7/21 16:29
 */
@SuppressWarnings("unused")
public abstract class AbsDataHandler<T> implements IDataHandler, IDataListenerManager<T> {

    // ===========================================================
    // HostConstants
    // ===========================================================

    private static final String TAG = AbsDataHandler.class.getSimpleName();

    // ===========================================================
    // Static Fields
    // ===========================================================

    // ===========================================================
    // Fields
    // ===========================================================

    private List<WeakReference<IDataListener<T>>> mListeners = new CopyOnWriteArrayList<>();

    // ===========================================================
    // Constructors
    // ===========================================================


    // ===========================================================
    // Getter or Setter
    // ===========================================================


    // ===========================================================
    // Methods for/from SuperClass/Interfaces
    // ===========================================================

    @Override
    public void handleReceiveData(byte[] bytes) {
        T o = parseData(bytes);
        List<WeakReference<IDataListener<T>>> listeners = mListeners;
        IDataListener<T> tmp;
        for (WeakReference<IDataListener<T>> weak : listeners) {
            tmp = weak.get();
            if (tmp != null) {
                tmp.onReceiveData(o);
            }
        }
    }

    @Override
    public void handleActiveChanged(boolean isActive) {
        List<WeakReference<IDataListener<T>>> listeners = mListeners;
        IDataListener<T> tmp;
        for (WeakReference<IDataListener<T>> weak : listeners) {
            tmp = weak.get();
            if (tmp != null) {
                tmp.onActiveChanged(isActive);
            }
        }
    }

    @Override
    public void addListener(IDataListener<T> l) {
        if (l != null) {
            WeakReference<IDataListener<T>> weak = new WeakReference<>(l);
            mListeners.add(weak);
        }
    }

    @Override
    public void remoteListener(IDataListener<T> l) {
	    List<WeakReference<IDataListener<T>>> listeners = mListeners;
        IDataListener<T> tmp;
        try {
            for (WeakReference<IDataListener<T>> weak : listeners) {
                tmp = weak.get();
                if (tmp != null && l == tmp) {
                    listeners.remove(weak);
                } else if (tmp == null) {
                    listeners.remove(weak);
                }
            }
        } catch (ConcurrentModificationException e) {
            for (WeakReference<IDataListener<T>> weak : listeners) {
                tmp = weak.get();
                if (tmp != null && l == tmp) {
                    listeners.remove(weak);
                } else if (tmp == null) {
                    listeners.remove(weak);
                }
            }
        }
    }

    // ===========================================================
    // Methods
    // ===========================================================

    protected abstract T parseData(byte[] bytes);

    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================

}
