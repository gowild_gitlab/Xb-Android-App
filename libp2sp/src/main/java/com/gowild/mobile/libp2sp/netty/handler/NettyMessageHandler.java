/**
 * Copyright (C)  2016 深圳市狗尾草智能科技有限公司
 * GO
 * NettyMessageHandler.java
 */
package com.gowild.mobile.libp2sp.netty.handler;

import com.gowild.mobile.libp2sp.core.Message;
import com.gowild.mobile.libp2sp.core.listener.IMessageDispatcher;
import com.gowild.mobile.libp2sp.core.utils.SocketUtil;
import com.gowild.mobile.libp2sp.netty.NettyStrictCodecFactory;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.channel.socket.nio.NioSocketChannel;

/**
 * @author Ji.Li
 * @since 2016/7/19 15:24
 * @version 1.0
 * <p><strong>Features draft description.主要功能介绍</strong></p>
 */
@SuppressWarnings("unused")
public class NettyMessageHandler extends ChannelInboundHandlerAdapter {

    // ===========================================================
    // HostConstants
    // ===========================================================

    private static final String TAG = NettyMessageHandler.class.getSimpleName();
    private static final String INVALID_PARAM_EXCEPTION = "Not valid message param.";

    // ===========================================================
    // Static Fields
    // ===========================================================

    // ===========================================================
    // Fields
    // ===========================================================

    private final IMessageDispatcher mDispatcher;

    // ===========================================================
    // Constructors
    // ===========================================================

    public NettyMessageHandler(IMessageDispatcher dispatcher) {
        this.mDispatcher = dispatcher;
    }

	ChannelInboundHandlerAdapter mHandlerAdapter;

	public ChannelInboundHandlerAdapter getHeartBeat(){
		return mHandlerAdapter;
	}

    // ===========================================================
    // Getter or Setter
    // ===========================================================


    // ===========================================================
    // Methods for/from SuperClass/Interfaces
    // ===========================================================

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        super.exceptionCaught(ctx, cause);
        mDispatcher.dispatchExceptionCaught(cause);
        System.out.println(cause.toString());//netty出现了异常
        
    }

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        super.channelRead(ctx, msg);
        if (msg != null && msg instanceof Message) {
            Message message = (Message) msg;
            mDispatcher.dispatchMessage(message);
        } else {
            throw new IllegalArgumentException(INVALID_PARAM_EXCEPTION);
        }
    }

    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        super.channelActive(ctx);
        mDispatcher.dispatchIsActive(true);
    }

    @Override
    public void channelInactive(ChannelHandlerContext ctx) throws Exception {
        super.channelInactive(ctx);
        mDispatcher.dispatchIsActive(false);
//        NettyClientManager.getInstance().connect();
    }

    @Override
    public void channelRegistered(ChannelHandlerContext ctx) throws Exception {
        super.channelRegistered(ctx);
        NioSocketChannel nioSocketChannel = (NioSocketChannel) ctx.channel();
        nioSocketChannel.attr(NettyStrictCodecFactory.DECRYPTION_KEY).set(SocketUtil.copyDefaultKey());
        nioSocketChannel.attr(NettyStrictCodecFactory.ENCRYPTION_KEY).set(SocketUtil.copyDefaultKey());
    }

    @Override
    public void channelUnregistered(ChannelHandlerContext ctx) throws Exception {
        super.channelUnregistered(ctx);
    }



    // ===========================================================
    // Methods
    // ===========================================================


    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================

}
