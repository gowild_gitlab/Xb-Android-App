/**
 * Copyright (C)  2016 深圳市狗尾草智能科技有限公司
 * GowildNewXB
 * HandlerManager.java
 */
package com.gowild.mobile.libp2sp.core;

import android.util.Log;

import com.gowild.mobile.libp2sp.core.listener.IDataHandler;
import com.gowild.mobile.libp2sp.core.listener.IHandlerManager;
import com.gowild.mobile.libp2sp.core.listener.IMessageDispatcher;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Ji.Li
 * @version 1.0
 *          <p><strong>Features draft description.</strong></p>
 *          非线程安全
 * @since 2016/7/21 15:43
 */
@SuppressWarnings("unused")
public final class HandlerManager implements IMessageDispatcher, IHandlerManager {

    // ===========================================================
    // HostConstants
    // ===========================================================

    private static final String TAG = HandlerManager.class.getSimpleName();

    // ===========================================================
    // Static Fields
    // ===========================================================

    private static volatile HandlerManager sInstance;

    // ===========================================================
    // Fields
    // ===========================================================

    private Map<Short, IDataHandler> mContainer = null;

    // ===========================================================
    // Constructors
    // ===========================================================

    private HandlerManager() {
        this.mContainer = new HashMap<>();
    }

    // ===========================================================
    // Getter or Setter
    // ===========================================================


    // ===========================================================
    // Methods for/from SuperClass/Interfaces
    // ===========================================================

    @Override
    public void dispatchMessage(Message msg) {
        Log.d("NettyClientManager", "dispatchMessage() msgCode = " + msg.getCode());
        short msgCode = msg.getCode();
        IDataHandler handler = this.mContainer.get(msgCode);
        if (handler != null) {
            handler.handleReceiveData(msg.getBody());
        }
    }

    @Override
    public void dispatchExceptionCaught(Throwable th) {
        Log.d("NettyClientManager", "dispatchExceptionCaught()");
    }

    @Override
    public void dispatchIsActive(boolean isActive) {
        Collection<IDataHandler> collection = this.mContainer.values();
        for (IDataHandler handler : collection) {
            handler.handleActiveChanged(isActive);
        }
    }

    @Override
    public void putHandler(short key, IDataHandler handler) {
        this.mContainer.put(key, handler);
    }

    @Override
    public void removeHandler(short key) {
        this.mContainer.remove(key);
    }

    @Override
    public IDataHandler getHandler(short key) {
        return this.mContainer.get(key);
    }

    // ===========================================================
    // Methods
    // ===========================================================

    public static HandlerManager getInstance() {
        if (sInstance == null) {
            synchronized (HandlerManager.class) {
                if (sInstance == null) {
                    sInstance = new HandlerManager();
                }
            }
        }
        return sInstance;
    }

    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================

}
