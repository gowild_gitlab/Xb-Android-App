/**
 * Copyright (C)  2016 深圳市狗尾草智能科技有限公司
 * GowildNewXB
 * IHandlerManager.java
 */
package com.gowild.mobile.libp2sp.core.listener;

/**
 * @author Ji.Li
 * @version 1.0
 *          <p><strong>Features draft description.主要功能介绍</strong></p>
 * @since 2016/7/21 14:45
 */
@SuppressWarnings("unused")
public interface IHandlerManager {

    // ===========================================================
    // HostConstants
    // ===========================================================


    // ===========================================================
    // Methods
    // ===========================================================

    void putHandler(short key, IDataHandler handler);

    void removeHandler(short key);

    IDataHandler getHandler(short key);

}
