/**
 * Copyright (C)  2016 深圳市狗尾草智能科技有限公司
 * GO
 * NettyPlaintextStrictMessageDecoder.java
 */
package com.gowild.mobile.libp2sp.netty.codec;

import com.gowild.mobile.libp2sp.core.Message;

import java.net.InetSocketAddress;
import java.util.List;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ByteToMessageDecoder;

/**
 * @author dream.xie
 * @since 2016/7/19 11:07
 * @version 1.0
 * <p><strong>Features draft description.主要功能介绍</strong></p>
 */
@SuppressWarnings("unused")
public class NettyPlaintextStrictMessageDecoder extends ByteToMessageDecoder{

    // ===========================================================
    // HostConstants
    // ===========================================================

    private static final String TAG = NettyPlaintextStrictMessageDecoder.class.getSimpleName();

    // ===========================================================
    // Static Fields
    // ===========================================================

    // ===========================================================
    // Fields
    // ===========================================================


    // ===========================================================
    // Constructors
    // ===========================================================


    // ===========================================================
    // Getter or Setter
    // ===========================================================


    // ===========================================================
    // Methods for/from SuperClass/Interfaces
    // ===========================================================

    @Override
    protected void decode(ChannelHandlerContext ctx, ByteBuf in, List<Object> out) throws Exception {
        if (in.readableBytes() < 4) {
            return;
        }
        int header = in.readShort();
        int packetLength = in.readShort();
        // 预解密长度信息成功，回溯位置
        in.readerIndex(in.readerIndex() - 4);
        //如果不是标识头，发送给客户端说，断开连接
        if (header != Message.HEADER || packetLength < Message.HEAD_SIZE) {
            // 数据包长度错误，断开连接
            InetSocketAddress socketAddr = (InetSocketAddress) ctx.channel().remoteAddress();
            String ip = socketAddr.getAddress().getHostAddress();
//            LogUtil.error(String.format("IP为[%s]发送的消息头不对，断开连接.", ip));
            ctx.channel().close();
            return;
        }

        if (in.readableBytes() < packetLength) {
            // 数据长度不足，等待下次接收
            return;
        }
        // 读取数据并解密数据
        byte[] data = new byte[packetLength];
        in.getBytes(in.readerIndex(), data, 0, packetLength);
        in.readerIndex(in.readerIndex() + packetLength);
        Message packet = Message.parse(data);
        if (packet != null) {
            out.add(packet);
        }
        return;
    }

    // ===========================================================
    // Methods
    // ===========================================================


    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================

}
