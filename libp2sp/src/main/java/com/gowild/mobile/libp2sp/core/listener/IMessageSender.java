/**
 * Copyright (C)  2016 深圳市狗尾草智能科技有限公司
 * GowildNewXB
 * IMessageSender.java
 */
package com.gowild.mobile.libp2sp.core.listener;

import com.gowild.mobile.libp2sp.core.Message;
import com.gowild.mobile.libp2sp.core.exception.NotConnectedException;

/**
 * @author Ji.Li
 * @since 2016/7/22 17:51
 * @version 1.0
 * <p><strong>Features draft description.主要功能介绍</strong></p>
 */
@SuppressWarnings("unused")
public interface IMessageSender {

    // ===========================================================
    // HostConstants
    // ===========================================================


    // ===========================================================
    // Methods
    // ===========================================================

    void send(Message message) throws NotConnectedException;
}
