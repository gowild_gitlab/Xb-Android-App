/**
 * Copyright (C)  2016 深圳市狗尾草智能科技有限公司
 * GowildXB
 * HostConstants.java
 */
package com.gowild.mobile.libp2sp;

/**
 * @author liudu
 * @since 2017/4/8 14:34
 * @version 1.0
 * <p><strong>Features draft description.主要功能介绍</strong></p>
 */
public class HostConstants {

    // ===========================================================
    // HostConstants
    // ===========================================================

    private static final String TAG = HostConstants.class.getSimpleName();

    // ===========================================================
    // Static Fields
    // ===========================================================
    public static final int PORT = 6030;
    //IP修改，注意要同时修改TCP、HTTP、HTML5的地址

    //TCP
//    public static final String HOST = "uatxb.gowildweb.com"; //预发布
    //    public static final String HOST = "172.26.1.165"; //内网
        public static final String HOST = "xbweb.gowildweb.com"; //外网

    //HTML地址
        public static final String HOST_HTML  ="http://m.gowild.cn/gowild-xb-web/"; //正式环境
    //    public static final String HOST_HTML  ="http://172.25.1.52/#/libraries/dash";//内网环境
//    public static final String HOST_HTML  ="http://172.26.1.185:8080/#/libraries/dash";//预发布环境

    //HTTP地址
      //正式环境
        public static final String BASE_URL_FORMAL = "http://xbweb.gowildweb.com:6130/web/rest";
        public static final String TOKEN_BASE_URL = "http://xbweb.gowildweb.com:6110/passport";

    //测试环境
    //        public static final String BASE_URL_FORMAL = "http://172.26.1.165:9001/web/rest";
    //        public static final String TOKEN_BASE_URL = "http://172.26.1.165:9000/passport";

    //预发布01 古老版本
    //        public static final String BASE_URL_FORMAL = "http://xb.gowildtech.com:6130/web/rest";
    //        public static final String TOKEN_BASE_URL = "http://xb.gowildtech.com:6110/passport";

    //    预发布
//    public static final String BASE_URL_FORMAL = "http://uatxb.gowildweb.com:6130/web/rest";
//    public static final String TOKEN_BASE_URL = "http://uatxb.gowildweb.com:6110/passport";

}
