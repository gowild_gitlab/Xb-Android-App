/**
 * Copyright (C)  2016 深圳市狗尾草智能科技有限公司
 * GO
 * NettyInitializer.java
 */
package com.gowild.mobile.libp2sp.netty;

import android.util.Log;

import com.gowild.mobile.libp2sp.core.BaseConfig;
import com.gowild.mobile.libp2sp.core.HandlerManager;
import com.gowild.mobile.libp2sp.core.Message;
import com.gowild.mobile.libp2sp.netty.handler.NettyMessageHandler;

import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.ByteToMessageDecoder;
import io.netty.handler.codec.MessageToByteEncoder;

/**
 * @author Ji.Li
 * @version 1.0
 *          <p><strong>Features draft description.主要功能介绍</strong></p>
 * @since 2016/7/19 20:55
 */
@SuppressWarnings("unused")
public class NettyInitializer extends ChannelInitializer<SocketChannel> {

	// ===========================================================
	// HostConstants
	// ===========================================================

	private static final String TAG = NettyInitializer.class.getSimpleName();
	private static final int NUM_THREAD = 1;
	private static final int SO_RCVBUF_SIZE = 64 * 1024;  // 64K

	// ===========================================================
	// Static Fields
	// ===========================================================

	// ===========================================================
	// Fields
	// ===========================================================

	// ===========================================================
	// Constructors
	// ===========================================================

	public NettyInitializer() {
		super();
	}

	// ===========================================================
	// Getter or Setter
	// ===========================================================


	// ===========================================================
	// Methods for/from SuperClass/Interfaces
	// ===========================================================

	@Override
	protected void initChannel(SocketChannel socketChannel) throws Exception {
		Log.d(TAG, "initChannel()");
		boolean isCipherText = BaseConfig.IS_CHIPER_TEXT;
		MessageToByteEncoder<Message> encoder = NettyStrictCodecFactory.getEncoder(isCipherText);
		ByteToMessageDecoder decoder = NettyStrictCodecFactory.getDecoder(isCipherText);
		ChannelPipeline pipeLine = socketChannel.pipeline();
		pipeLine.addLast("encoder", encoder);
		pipeLine.addLast("decoder", decoder);
		pipeLine.addLast("handler", new NettyMessageHandler(HandlerManager.getInstance()));
	}
}

// ===========================================================
// Methods
// ===========================================================


// ===========================================================
// Inner and Anonymous Classes
// ===========================================================

