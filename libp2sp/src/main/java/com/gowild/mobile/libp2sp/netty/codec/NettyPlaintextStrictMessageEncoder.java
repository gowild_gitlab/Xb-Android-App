/**
 * Copyright (C)  2016 深圳市狗尾草智能科技有限公司
 * GO
 * NettyPlaintextStrictMessageEncoder.java
 */
package com.gowild.mobile.libp2sp.netty.codec;

import com.gowild.mobile.libp2sp.core.Message;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToByteEncoder;

/**
 * @author dream.xie
 * @since 2016/7/19 11:00
 * @version 1.0
 * <p><strong>Features draft description.主要功能介绍</strong></p>
 */
@SuppressWarnings("unused")
public class NettyPlaintextStrictMessageEncoder extends MessageToByteEncoder<Message>{

    // ===========================================================
    // HostConstants
    // ===========================================================

    private static final String TAG = NettyPlaintextStrictMessageEncoder.class.getSimpleName();

    // ===========================================================
    // Static Fields
    // ===========================================================

    // ===========================================================
    // Fields
    // ===========================================================


    // ===========================================================
    // Constructors
    // ===========================================================


    // ===========================================================
    // Getter or Setter
    // ===========================================================


    // ===========================================================
    // Methods for/from SuperClass/Interfaces
    // ===========================================================

    @Override
    protected void encode(ChannelHandlerContext channelHandlerContext, Message message, ByteBuf out) throws Exception {
        byte[] plainText = message.toByteBuffer().array();
        out.writeBytes(plainText);
    }

    // ===========================================================
    // Methods
    // ===========================================================


    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================

}
