/**
 * Copyright (C)  2016 深圳市狗尾草智能科技有限公司
 * GO
 * NettyCiphertextStrictMessageEncoder.java
 */
package com.gowild.mobile.libp2sp.netty.codec;

import com.gowild.mobile.libp2sp.core.Message;
import com.gowild.mobile.libp2sp.core.utils.SocketUtil;
import com.gowild.mobile.libp2sp.netty.NettyStrictCodecFactory;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToByteEncoder;

/**
 * @author Ji.Li
 * @since 2016/7/19 11:13
 * @version 1.0
 * <p><strong>Features draft description.主要功能介绍</strong></p>
 */
@SuppressWarnings("unused")
public class NettyCiphertextStrictMessageEncoder extends MessageToByteEncoder<Message>{

    // ===========================================================
    // HostConstants
    // ===========================================================

    private static final String TAG = NettyCiphertextStrictMessageEncoder.class.getSimpleName();

    // ===========================================================
    // Static Fields
    // ===========================================================

    // ===========================================================
    // Fields
    // ===========================================================


    // ===========================================================
    // Constructors
    // ===========================================================


    // ===========================================================
    // Getter or Setter
    // ===========================================================


    // ===========================================================
    // Methods for/from SuperClass/Interfaces
    // ===========================================================

    @Override
    protected void encode(final ChannelHandlerContext ctx, final Message message, final ByteBuf out) throws Exception {
        // 若存在不同线程给同一玩家发送数据的情况，因此加密过程需要同步处理
        byte[] plainText = message.toByteBuffer().array();
        //获取key
        int[] encryptKey = getKey(ctx);
        //加密过程
        byte[] cipherText = SocketUtil.encode(plainText, encryptKey);
        out.writeBytes(cipherText);
    }

    private int[] getKey(final ChannelHandlerContext ctx) {
        int[] key = ctx.channel().attr(NettyStrictCodecFactory.ENCRYPTION_KEY).get();
        return key;
    }

    // ===========================================================
    // Methods
    // ===========================================================


    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================

}
