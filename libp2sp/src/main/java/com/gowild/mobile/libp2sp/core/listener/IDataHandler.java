/**
 * Copyright (C)  2016 深圳市狗尾草智能科技有限公司
 * GowildNewXB
 * IDataHandler.java
 */
package com.gowild.mobile.libp2sp.core.listener;

/**
 * @author dell
 * @since 2016/7/21 14:47
 * @version 1.0
 * <p><strong>Features draft description.主要功能介绍</strong></p>
 */
@SuppressWarnings("unused")
public interface IDataHandler {

    // ===========================================================
    // HostConstants
    // ===========================================================

    // ===========================================================
    // Methods
    // ===========================================================

    void handleReceiveData(byte[] bytes);

    void handleActiveChanged(boolean isActive);

}
