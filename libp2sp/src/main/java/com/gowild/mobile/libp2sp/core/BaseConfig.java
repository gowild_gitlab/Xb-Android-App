/**
 * Copyright (C)  2016 深圳市狗尾草智能科技有限公司
 * GO
 * BaseConfig.java
 */
package com.gowild.mobile.libp2sp.core;

/**
 * @author Ji.Li
 * @version 1.0
 *          <p><strong>Features draft description.主要功能介绍</strong></p>
 * @since 2016/7/19 21:44
 */
@SuppressWarnings("unused")
public final class BaseConfig {

    // ===========================================================
    // HostConstants
    // ===========================================================

    private static final String TAG = BaseConfig.class.getSimpleName();

    public static final  boolean IS_CHIPER_TEXT = true;

    // ===========================================================
    // Static Fields
    // ===========================================================

    // ===========================================================
    // Fields
    // ===========================================================

    // ===========================================================
    // Constructors
    // ===========================================================

    private BaseConfig() {
    }

    // ===========================================================
    // Getter or Setter
    // ===========================================================


    // ===========================================================
    // Methods for/from SuperClass/Interfaces
    // ===========================================================


    // ===========================================================
    // Methods
    // ===========================================================

    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================

}
